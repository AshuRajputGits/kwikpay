package com.uk.recharge.kwikpay.myaccount.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.catering.models.CateringCompleteOrdersMO;
import com.uk.recharge.kwikpay.catering.models.ProductDistributorDetailsMO;
import com.uk.recharge.kwikpay.catering.models.ProductSelectedDetailsMO;
import com.uk.recharge.kwikpay.catering.models.SubProductSelectedMO;
import com.uk.recharge.kwikpay.myaccount.MyTransactionSingleton;
import com.uk.recharge.kwikpay.myaccount.adapters.CateringOrdersPagerAdapter;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 11/5/2018.
 */

public class CateringTransactionFragment extends Fragment {

    private Context context;
    private SharedPreferences preference = null;
    private ViewPager viewPager = null;
    //    private Button onGoingOrderButton, pastOrderButton;
    private TextView onGoingOrderButton, pastOrderButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.catering_transaction_fragment, container, false);
        preference = getActivity().getSharedPreferences("KP_Preference", Context.MODE_PRIVATE);
        onGoingOrderButton = view.findViewById(R.id.onGoingOrderButton);
        pastOrderButton = view.findViewById(R.id.pastOrderButton);
        onGoingOrderButton.setOnClickListener(onClickListener);
        pastOrderButton.setOnClickListener(onClickListener);
        setUpTabModes(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.onGoingOrderButton) {
                viewPager.setCurrentItem(0);
                onGoingOrderButton.setBackground(getResources().getDrawable(R.drawable.catering_selected_tab));
                pastOrderButton.setBackground(getResources().getDrawable(R.drawable.catering_unselected_tab));

            } else if (v.getId() == R.id.pastOrderButton) {
                viewPager.setCurrentItem(1);
                pastOrderButton.setBackground(getResources().getDrawable(R.drawable.catering_selected_tab));
                onGoingOrderButton.setBackground(getResources().getDrawable(R.drawable.catering_unselected_tab));
            }
        }
    };

    private void setUpTabModes(View view) {
        viewPager = view.findViewById(R.id.cateringViewPager);
        /*TabLayout tabLayout = view.findViewById(R.id.cateringTabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("OnGoing order"));
        tabLayout.addTab(tabLayout.newTab().setText("Past order"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);*/

        viewPager.setOffscreenPageLimit(2);

        /*viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });*/
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            Log.e("CatFrag", "CateringTransactionFragment call API");
            callCateringAllOrderDetailsAPI();
        } else {
            Log.e("CatFrag", "CateringTransactionFragment NOT TO DO ANYTHING BOSSSSS!!");
        }
    }

    private void callCateringAllOrderDetailsAPI() {
        KPApplication.getInstance().getUtilityInstance().showProgressDialog(getActivity());
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "CATERING_ORDER_BY_USER_ID");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {
                                    parseCateringTransactionDetails(childJson);
                                } else
                                    ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                            } else
                                ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(context))
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(context))
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    void parseCateringTransactionDetails(JSONObject productResponse) {
        try {
            Object genericObject = productResponse.get("ORDER_LIST");
            JSONArray jsonArray = null;

            if (genericObject instanceof JSONObject) {
                final JSONObject jsonOBJECT = (JSONObject) genericObject;
                jsonArray = new JSONArray();
                jsonArray.put(jsonOBJECT);
            } else if (genericObject instanceof JSONArray)
                jsonArray = (JSONArray) genericObject;

            if (jsonArray != null && jsonArray.length() > 0) {

                ArrayList<CateringCompleteOrdersMO> onGoingCateringCompleteOrdersMOList = new ArrayList<>();
                ArrayList<CateringCompleteOrdersMO> pastCateringCompleteOrdersMOList = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {

                    CateringCompleteOrdersMO cateringCompleteOrdersMO = new CateringCompleteOrdersMO();
                    cateringCompleteOrdersMO.setCateringOnGoingOrder(jsonArray.getJSONObject(i).getString("ONGOING_ORDER"));
                    cateringCompleteOrdersMO.setCateringReadyStatus(jsonArray.getJSONObject(i).getString("catering_ready_status"));
                    cateringCompleteOrdersMO.setCateringAcceptedStatus(jsonArray.getJSONObject(i).getString("catering_accepted_status"));
                    cateringCompleteOrdersMO.setCateringDeliveryStatus(jsonArray.getJSONObject(i).getString("catering_delivery_status"));
                    cateringCompleteOrdersMO.setCateringCancelStatus(jsonArray.getJSONObject(i).getString("catering_cancel_status"));
                    cateringCompleteOrdersMO.setCateringTxnDateTime(jsonArray.getJSONObject(i).getString("TRANSACTION_DATETIME"));
                    cateringCompleteOrdersMO.setCateringOrderId(jsonArray.getJSONObject(i).getString("ORDER_ID"));
                    cateringCompleteOrdersMO.setCateringTotalItems(jsonArray.getJSONObject(i).getString("ITEMS"));
                    cateringCompleteOrdersMO.setCateringServiceFee(jsonArray.getJSONObject(i).getString("SERVICE_FEE"));
                    cateringCompleteOrdersMO.setCateringServiceName(jsonArray.getJSONObject(i).getString("SERVICENAME"));
                    cateringCompleteOrdersMO.setCateringOperatorName(jsonArray.getJSONObject(i).getString("OPERATORNAME"));
                    cateringCompleteOrdersMO.setCateringPaymentAmount(jsonArray.getJSONObject(i).getString("PAYMENTAMOUNT"));
                    cateringCompleteOrdersMO.setCateringPaymentStatus(jsonArray.getJSONObject(i).getString("PAYMENTSTATUS"));
                    cateringCompleteOrdersMO.setCateringOperatorCode(jsonArray.getJSONObject(i).getString("OPERATORCODE"));
                    cateringCompleteOrdersMO.setCateringCountryCode(jsonArray.getJSONObject(i).getString("COUNTRYCODE"));
                    cateringCompleteOrdersMO.setCateringDisplayCurrencyCode(jsonArray.getJSONObject(i).getString("DISPLAYCURRENCYCODE"));
                    cateringCompleteOrdersMO.setCateringPaymentCurrencyCode(jsonArray.getJSONObject(i).getString("PAYMENTCURRENCYCODE"));
//                    cateringCompleteOrdersMO.setCateringCo(jsonArray.getJSONObject(i).getString("PARTYCODE"));
                    cateringCompleteOrdersMO.setCateringDiscountAmount(jsonArray.getJSONObject(i).getString("DISCOUNT_AMOUNT"));
                    cateringCompleteOrdersMO.setCateringCpnSrNum(jsonArray.getJSONObject(i).getString("CPNSRNUM"));
                    cateringCompleteOrdersMO.setCateringCpnValue(jsonArray.getJSONObject(i).getString("CPNVALUE"));
                    cateringCompleteOrdersMO.setCateringUniqueTxnId(jsonArray.getJSONObject(i).getString("CPNUNIQUETRANSID"));
                    cateringCompleteOrdersMO.setCateringProcessingFee(jsonArray.getJSONObject(i).getString("PROCESSINGFEE"));

                    //--------------DISTRIBUTOR OBJECT DETAILS PARSING-------------------------------//
                    JSONObject distributorJsonObject = jsonArray.getJSONObject(i).getJSONObject("distributor_details");
                    ProductDistributorDetailsMO distributorDetailsMO = new ProductDistributorDetailsMO();
                    distributorDetailsMO.setMerchantName(distributorJsonObject.getString("MERCHANT_NAME"));
                    distributorDetailsMO.setDistributorHeadId(distributorJsonObject.optString("distributor_head_id"));
                    distributorDetailsMO.setDistributorName(distributorJsonObject.getString("distributor_name"));
                    distributorDetailsMO.setDistributorLogo(distributorJsonObject.getString("logo_img"));
                    distributorDetailsMO.setDistributorAddress(distributorJsonObject.getString("ADDRESS_ONE") + " " +
                            distributorJsonObject.getString("CITY") + " " + distributorJsonObject.getString("POSTCODE"));
                    distributorDetailsMO.setDistributorCity(distributorJsonObject.getString("CITY"));
                    distributorDetailsMO.setDistributorLocationNumber(distributorJsonObject.getString("LOCATION_NUMBER"));
                    distributorDetailsMO.setDistributorLocationName(distributorJsonObject.getString("LOCATION_NAME"));

                    cateringCompleteOrdersMO.setProductDistributorDetails(distributorDetailsMO);

                    //-------------*****end DISTRIBUTOR OBJECT DETAILS PARSING ******-------------------------------//

                    //--------------PRODUCTS AND SUB PRODUCTS OBJECT DETAILS PARSING-------------------------------//
                    Object genericObject2 = jsonArray.getJSONObject(i).get("products");
                    JSONArray jsonArray2 = null;

                    if (genericObject2 instanceof JSONObject) {
                        final JSONObject jsonOBJECT = (JSONObject) genericObject2;
                        jsonArray2 = new JSONArray();
                        jsonArray2.put(jsonOBJECT);
                    } else if (genericObject2 instanceof JSONArray)
                        jsonArray2 = (JSONArray) genericObject2;

                    if (jsonArray2 != null && jsonArray2.length() > 0) {

                        List<ProductSelectedDetailsMO> productsList = new ArrayList<>();

                        for (int j = 0; j < jsonArray2.length(); j++) {
                            ProductSelectedDetailsMO productSelectedDetailsMO = new ProductSelectedDetailsMO();
                            productSelectedDetailsMO.setBrandName(jsonArray2.getJSONObject(j).getString("brand_name"));
                            productSelectedDetailsMO.setCategoryType(jsonArray2.getJSONObject(j).getString("category_type"));
                            productSelectedDetailsMO.setUnitPrice(jsonArray2.getJSONObject(j).getString("unit_price"));
                            productSelectedDetailsMO.setProductPurchaseCount(jsonArray2.getJSONObject(j).getString("product_purchase_count"));
                            productSelectedDetailsMO.setProductDeliverCount(jsonArray2.getJSONObject(j).getString("product_deliver_count"));

                            if (jsonArray2.getJSONObject(j).has("sub_products")) {
                                Object subProdGenericObj = jsonArray2.getJSONObject(j).get("sub_products");
                                JSONArray subProdJsonArray = null;

                                if (subProdGenericObj instanceof JSONObject) {
                                    final JSONObject jsonOBJECT = (JSONObject) subProdGenericObj;
                                    subProdJsonArray = new JSONArray();
                                    subProdJsonArray.put(jsonOBJECT);
                                } else if (subProdGenericObj instanceof JSONArray)
                                    subProdJsonArray = (JSONArray) subProdGenericObj;

                                if (subProdJsonArray != null && subProdJsonArray.length() > 0) {
                                    List<SubProductSelectedMO> subProductSelectedMOList = new ArrayList<>();
                                    for (int k = 0; k < subProdJsonArray.length(); k++) {

                                        try {
                                            SubProductSelectedMO subProdMO = new SubProductSelectedMO();
                                            subProdMO.setSubProdBrandName(subProdJsonArray.getJSONObject(k).getString("brand_name"));
                                            subProdMO.setSubProdUnitPrice(subProdJsonArray.getJSONObject(k).getString("unit_price"));
                                            subProdMO.setSubProdCategoryType(subProdJsonArray.getJSONObject(k).getString("category_type"));
                                            subProdMO.setSubProdPurchaseCount(subProdJsonArray.getJSONObject(k).getString("product_purchase_count"));
                                            subProdMO.setSubProdDispenseCount(subProdJsonArray.getJSONObject(k).getString("product_dispensed_count"));
                                            subProductSelectedMOList.add(subProdMO);

                                        } catch (Exception e) {
//                                            Crashlytics.logException(e);
                                            e.printStackTrace();
                                        }
                                    }
                                    productSelectedDetailsMO.setSubProductSelectedMOList(subProductSelectedMOList);
                                }
                            }
                            productsList.add(productSelectedDetailsMO);
                        }
                        cateringCompleteOrdersMO.setProductSelectedDetailsMOList(productsList);
                    }

                    String onGoingOrderStatus = jsonArray.getJSONObject(i).optString("ONGOING_ORDER");
                    if (onGoingOrderStatus != null && onGoingOrderStatus.equalsIgnoreCase("YES"))
                        onGoingCateringCompleteOrdersMOList.add(cateringCompleteOrdersMO);
                    else if (onGoingOrderStatus != null && onGoingOrderStatus.equalsIgnoreCase("NO"))
                        pastCateringCompleteOrdersMOList.add(cateringCompleteOrdersMO);
                }
                if (viewPager != null) {

                    //SETTING DEFAULT BG IMAGES TO 'ONGOING AND PAST TABS'
                    onGoingOrderButton.setBackground(getResources().getDrawable(R.drawable.catering_selected_tab));
                    pastOrderButton.setBackground(getResources().getDrawable(R.drawable.catering_unselected_tab));

                    //setting above value to MyTransaction Singleton class to access app level
                    MyTransactionSingleton.getInstance().setCateringOnGoingCompleteOrderList(onGoingCateringCompleteOrdersMOList);
                    MyTransactionSingleton.getInstance().setCateringPastCompleteOrderList(pastCateringCompleteOrdersMOList);
                    viewPager.setAdapter(new CateringOrdersPagerAdapter(getActivity().getSupportFragmentManager()));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
