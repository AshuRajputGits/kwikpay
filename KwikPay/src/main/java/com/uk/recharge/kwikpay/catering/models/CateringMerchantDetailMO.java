package com.uk.recharge.kwikpay.catering.models;

/**
 * Created by Ashu Rajput on 11/3/2018.
 */

public class CateringMerchantDetailMO {
    private String merchantLogoUrls;
    private String merchantName;
    private String locationNumber;
    private String locationName;
    private String merchantId;

    public String getMerchantLogoUrls() {
        return merchantLogoUrls;
    }

    public void setMerchantLogoUrls(String merchantLogoUrls) {
        this.merchantLogoUrls = merchantLogoUrls;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
