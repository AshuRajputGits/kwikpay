package com.uk.recharge.kwikpay;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidation;
import com.ar.library.validation.FieldValidationWithMessage;
import com.uk.recharge.kwikpay.myaccount.MyTransactionActivity;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.uk.recharge.kwikpay.utilities.CustomDialogBox;

public class ContactUs extends Activity implements OnItemSelectedListener,AsyncRequestListenerViaPost
{
	private Spinner chooseReasonSpinner;
	private TextView complainScreenTV,reasonLabel,nameLabel,mobNumberLabel,emailLabel,commentLabel;
	private ArrayAdapter<String> chooseReasonAdapter;
	private LinearLayout queryfeedbackLL;
	private String[] reasonsArray={"Select","Contact us","Report issue"};
	private EditText queryFeedbackName, queryFeedbackMobno, queryFeedbackEmail, queryFeedbackComment;
	private SharedPreferences preferences;
	private int apiHitPosition=0;
	private boolean checkAPICall = false,controlNetOffExit=false;

//	private View separatorLine;
//	private TextView cartItemCounter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactus_mainscreen);
		settingIds();

		// SETTING ADAPTER AND DATA TO THE CHOOSE REASON SPINNER
		chooseReasonAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textsize,reasonsArray);
		chooseReasonAdapter.setDropDownViewResource(R.layout.single_row_spinner);
		chooseReasonSpinner.setAdapter(chooseReasonAdapter);
		chooseReasonSpinner.setOnItemSelectedListener(this);

		Bundle extras = getIntent().getExtras();
		if(extras !=null)
		{
			if(extras.containsKey("RAISE_TICKET_SCREEN"))
			{
				chooseReasonSpinner.setSelection(1);
				queryFeedbackName.setText(extras.getString("RAISE_TICKET_NAME"));
				queryFeedbackMobno.setText(extras.getString("RAISE_TICKET_MOBILE_NUMBER"));
				queryFeedbackEmail.setText(extras.getString("RAISE_TICKET_EMAIL_ID"));

				queryFeedbackName.setEnabled(false);
				queryFeedbackMobno.setEnabled(false);
				queryFeedbackEmail.setEnabled(false);

				checkAPICall= true;
			}
		}

		if(preferences.getString("KP_USER_ID", "").equals("0"))
		{
		}
		else
		{
			if(!checkAPICall)
			{
				apiHitPosition = 1;
				new LoadResponseViaPost(ContactUs.this, formViewContactUs() , true).execute("");
			}
		}

		//RESTRICTING ' IN COMMENT FIELD
		//		FieldValidationWithMessage.blockMyCharacter(queryFeedbackComment, "@!#^()/:+;-_");

		//SETTING SCREEN NAMES TO APPS FLYER
		try{
			EventsLoggerSingleton.getInstance().setCommonEventLogs(this,getResources().getString(R.string.SCREEN_NAME_CONTACT_US));
			/*String eventName=getResources().getString(R.string.SCREEN_NAME_CONTACT_US);
			AppsFlyerLib.trackEvent(this, eventName, null);
			EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
			((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/

		}catch (Exception e){e.printStackTrace();}
	}

	private void settingIds() 
	{
		// TODO Auto-generated method stub
		preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);	
		chooseReasonSpinner=(Spinner)findViewById(R.id.selectContactUsSpinner);
		complainScreenTV=(TextView)findViewById(R.id.contactUs_ComplaintScreen);
		queryfeedbackLL=(LinearLayout)findViewById(R.id.queryfeedback_ContactLayout);
		reasonLabel=(TextView)findViewById(R.id.contactUs_ChooseYourReason);
		nameLabel=(TextView)findViewById(R.id.queryfeedback_NameLabel);
		mobNumberLabel=(TextView)findViewById(R.id.queryfeedback_MobNumbLabel);
		emailLabel=(TextView)findViewById(R.id.queryfeedback_MailIdLabel);
		commentLabel=(TextView)findViewById(R.id.queryfeedback_CommentLabel);

		reasonLabel.setText(Html.fromHtml("Choose Your Reason"+"<font color='#ff0000'>*"));
		nameLabel.setText(Html.fromHtml("Name"+"<font color='#ff0000'>*"));
		mobNumberLabel.setText(Html.fromHtml("Mobile Number"+"<font color='#ff0000'>*"));
		emailLabel.setText(Html.fromHtml("Email Address"+"<font color='#ff0000'>*"));
		commentLabel.setText(Html.fromHtml("Comments/Feedback"+"<font color='#ff0000'>*"));

		queryFeedbackName = (EditText)findViewById(R.id.queryfeedback_NameET);
		queryFeedbackMobno = (EditText)findViewById(R.id.queryfeedback_MobNumbET);
		queryFeedbackEmail = (EditText)findViewById(R.id.queryfeedback_MailIdET);
		queryFeedbackComment = (EditText)findViewById(R.id.queryfeedback_CommentET);
		
		TextView headerTitle=(TextView)findViewById(R.id.mainHeaderTitleName);
		headerTitle.setText("Contact us ");

//		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);
//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);

//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(ContactUs.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME
		/*DBQueryMethods database=new DBQueryMethods(ContactUs.this);
		try
		{
			database.open();
			TextView headerBelowTV=(TextView)findViewById(R.id.headerBelowTextPart2);
			headerBelowTV.setText(database.getSubHeaderTitles("Contact us"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}*/

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		final InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		switch(position)
		{
		case 0:
			hideMyAllViews();
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			break;
		case 1:
			complainScreenTV.setVisibility(View.GONE);
//			queryfeedbackLL.setVisibility(View.VISIBLE); // COMMENTING AS PER NEW REQUIREMENT
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			
			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("message/rfc822");
			i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"care@kwikpay.com"});
//			i.putExtra(Intent.EXTRA_EMAIL  , "care@kwikpay.co.uk");
			i.putExtra(Intent.EXTRA_SUBJECT, "contact us @kwikpay");
			i.putExtra(Intent.EXTRA_TEXT   , "");
			try 
			{
			    startActivity(Intent.createChooser(i, "Send mail..."));
			} catch (android.content.ActivityNotFoundException ex) {
//			    Toast.makeText(MyActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
			}
			
			/*if(!preferences.getString("KP_USER_ID", "0").equals("0"))
			{
				queryFeedbackComment.setText("");
				FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackComment);
			}
			else
			{
				// COMMENTING AS PER NEW REQUIREMENT
				queryFeedbackName.setText("");
				queryFeedbackMobno.setText("");
				queryFeedbackEmail.setText("");
				queryFeedbackComment.setText("");
				
				FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackName);
				FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackMobno);
				FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackEmail);
				FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackComment);
			}*/
			
			break;
			
		case 2:
			complainScreenTV.setVisibility(View.VISIBLE);
			queryfeedbackLL.setVisibility(View.GONE);
			settingAndClickingHeading();
			
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

			break;
		default:
			break;

		}
	}

	public String formViewContactUs() {
		JSONObject jsonobj = new JSONObject();

		try {

			jsonobj.put("APISERVICE", "KPVIEWCONTACTUS");
			jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
			jsonobj.put("DEVICEOS", "Android");
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("USERID", preferences.getString("KP_USER_ID", ""));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();

	}

	public String formJSONParam() {
		JSONObject jsonobj = new JSONObject();

		try {
			String trimmedComment = queryFeedbackComment.getText().toString().replaceAll(" +"," ").trim();
			jsonobj.put("APISERVICE", "KPSAVECONTACTUS");
			jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
			jsonobj.put("DEVICEOS", "Android");
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("EMAIL", queryFeedbackEmail.getText().toString());
			jsonobj.put("NAME", queryFeedbackName.getText().toString());
			jsonobj.put("MOBILENUMBER", queryFeedbackMobno.getText().toString());
			jsonobj.put("COMMENT", trimmedComment);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	public void queryfeedbackOnClickMethod(View v)
	{
		if(apiHitPosition==1 || checkAPICall)
		{
			if(!FieldValidationWithMessage.commentIsValid(queryFeedbackComment.getText().toString(),queryFeedbackComment).equals("SUCCESS"))
			{
				ARCustomToast.showToast(ContactUs.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			}
			else
			{
				FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackComment);
				controlNetOffExit=true;
				apiHitPosition = 2;
				new LoadResponseViaPost(ContactUs.this, formJSONParam() , true).execute("");
			}
			
		}
		else
		{
			if(queryFeedbackkValidation())
			{
				controlNetOffExit=true;
				apiHitPosition = 2;
				new LoadResponseViaPost(ContactUs.this, formJSONParam() , true).execute("");
			}
		}
		
		
		//		ContactUs.this.finish();
	}

	public void hideMyAllViews()
	{
		complainScreenTV.setVisibility(View.GONE);
		queryfeedbackLL.setVisibility(View.GONE);
	}

	public void settingAndClickingHeading()
	{
		Spanned tempClickHereStr=Html.fromHtml("Please select the transaction in my account section and raise a ticket. We endeavour to revert within 48 hours. Go to <i>My Transaction.</i> ");
		SpannableString ss = new SpannableString(tempClickHereStr);

		ClickableSpan clickableSpan = new ClickableSpan() 
		{
			@Override
			public void onClick(View textView) 
			{
				if(preferences.getString("KP_USER_ID", "").equals("0"))
				{
					Intent moveToLoginIntent=new Intent(ContactUs.this, LoginScreen.class);
					startActivity(moveToLoginIntent);
					hideMyAllViews();
					chooseReasonSpinner.setSelection(0);
				}
				else
				{
//					Intent moveToMyAccIntent=new Intent(ContactUs.this, MyTransaction.class);
					Intent moveToMyAccIntent=new Intent(ContactUs.this, MyTransactionActivity.class);
					startActivity(moveToMyAccIntent);
					hideMyAllViews();
					chooseReasonSpinner.setSelection(0);
				}
			}
		};

		ss.setSpan(clickableSpan, 118 , 133, Spannable.SPAN_INTERMEDIATE);
		ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.darkGreen)), 118, 133, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		complainScreenTV.setText(ss);
		complainScreenTV.setMovementMethod(LinkMovementMethod.getInstance());

	}

	public boolean queryFeedbackkValidation()
	{
		if(!FieldValidationWithMessage.nameIsValid(queryFeedbackName.getText().toString(), queryFeedbackName).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackMobno);
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackEmail);
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackComment);

			ARCustomToast.showToast(ContactUs.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}

		else if(!FieldValidationWithMessage.mobNumberIsValidWithoutLeadingZero(queryFeedbackMobno.getText().toString(), queryFeedbackMobno).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackName);
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackEmail);
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackComment);

			ARCustomToast.showToast(ContactUs.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}
		
		else if(!FieldValidation.emailAddressIsValid(queryFeedbackEmail.getText().toString(), queryFeedbackEmail))
		{
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackName);
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackMobno);
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackComment);


			ARCustomToast.showToast(ContactUs.this, "please enter a valid email id", Toast.LENGTH_LONG);
			return false;
		}

		else if(!FieldValidationWithMessage.commentIsValid(queryFeedbackComment.getText().toString(),queryFeedbackComment).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackName);
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackMobno);
			FieldValidationWithMessage.removeAllErrorIcons(queryFeedbackEmail);


			ARCustomToast.showToast(ContactUs.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}
		else
		{
			return true;
		}


	}

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub
		//		Log.e("FeedBackResponse", loadedString);

		if( loadedString!=null && !loadedString.equals(""))
		{
			if(!loadedString.equals("Exception"))
			{
				if(apiHitPosition==1)
				{
					try 
					{
						JSONObject view_feedback_json = new JSONObject(loadedString);
						JSONObject view_feedback_json_response = view_feedback_json.getJSONObject(ProjectConstant.RESPONSE_TAG);

						if(view_feedback_json_response.has(ProjectConstant.API_RESPONSE_CODE))
						{
							if(view_feedback_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
							{
								if(view_feedback_json_response.has("USERNAME") 
										&& view_feedback_json_response.has("MOBILENUMBER")
										&& view_feedback_json_response.has("EMAILID"))
								{
									queryFeedbackName.setText(view_feedback_json_response.getString("USERNAME"));
									queryFeedbackEmail.setText(view_feedback_json_response.getString("EMAILID"));
									queryFeedbackMobno.setText(view_feedback_json_response.getString("MOBILENUMBER"));

									queryFeedbackName.setEnabled(false);
									queryFeedbackEmail.setEnabled(false);
									queryFeedbackMobno.setEnabled(false);

								}
							}
							else
							{
								if(view_feedback_json_response.has("DESCRIPTION"))
								{
									ARCustomToast.showToast(ContactUs.this, view_feedback_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
								}

							}
						}

					}	

					catch (JSONException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();

						if(!preferences.getString("KP_USER_ID", "0").equals("0"))
						{
							queryFeedbackName.setEnabled(false);
							queryFeedbackEmail.setEnabled(false);
							queryFeedbackMobno.setEnabled(false);
						}

					}
				}

				if(apiHitPosition==2)
				{
					try 
					{
						JSONObject feedback_json = new JSONObject(loadedString);
						JSONObject feedback_json_response = feedback_json.getJSONObject(ProjectConstant.RESPONSE_TAG);

						if(feedback_json_response.has(ProjectConstant.API_RESPONSE_CODE))
						{
							if(feedback_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
							{
								if(feedback_json_response.has(ProjectConstant.TICKET_ID))
								{
									Intent i = new Intent(ContactUs.this,CustomDialogBox.class);
									i.putExtra("MSG_KEY", feedback_json_response.getString("DESCRIPTION"));
									startActivity(i);
									ContactUs.this.finish();
								}
							}
							else
							{
								if(feedback_json_response.has("DESCRIPTION"))
								{
									ARCustomToast.showToast(ContactUs.this, feedback_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
								}

							}
						}

					}	

					catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			else
			{
				if(!controlNetOffExit)
				ContactUs.this.finish();
				ARCustomToast.showToast(ContactUs.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
			}
		}
		else
		{
			if(!controlNetOffExit)
			ContactUs.this.finish();
			ARCustomToast.showToast(ContactUs.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}

	}
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preferences!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preferences.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);
		}
		
		}
	}*/

}
