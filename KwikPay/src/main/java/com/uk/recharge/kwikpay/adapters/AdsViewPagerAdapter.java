package com.uk.recharge.kwikpay.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;

/**
 * Created by Ashu Rajput on 05-02-2017.
 */

public class AdsViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private String[] bannerImageUrls;

    public AdsViewPagerAdapter(Context context, String moduleType) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //Clearing previous stored URLs
        bannerImageUrls = new String[0];
        try {
            String receivedUrls = "";
            if (moduleType != null && moduleType.equalsIgnoreCase("HomeScreen"))
                receivedUrls = mContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE).getString("LANDING_PAGE_BANNERS", "");
            else if (moduleType != null && moduleType.equalsIgnoreCase("KwikCharge"))
                receivedUrls = mContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE).getString("KWIKCHARGE_BANNERS", "");
            else if (moduleType != null && moduleType.equalsIgnoreCase("GameService"))
                receivedUrls = mContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE).getString("GAME_SERVICE_BANNERS", "");
            else if (moduleType != null && moduleType.equalsIgnoreCase("TopUpCharge"))
                receivedUrls = mContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE).getString("TOPUP_BANNERS", "");
            else if (moduleType != null && moduleType.equalsIgnoreCase("GamingArcade"))
                receivedUrls = mContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE).getString("GAMING_HOME_BANNERS", "");
            else if (moduleType != null && moduleType.equalsIgnoreCase("KwikVend"))
                receivedUrls = mContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE).getString("KV_HOME_BANNERS", "");
            else if (moduleType != null && moduleType.equalsIgnoreCase("CateringMerchantDetails"))
                receivedUrls = mContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE).getString("CAT_MERCHANT_BANNERS", "");
            bannerImageUrls = receivedUrls.split("\\|");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        if (bannerImageUrls != null && bannerImageUrls.length > 0)
            return bannerImageUrls.length;
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.view_pager_item, container, false);
        ImageView bannerAdsIV = itemView.findViewById(R.id.bannerAdsIV);
//        setDynamicWidthHeightToImageView(bannerAdsIV);
        try {
            Glide.with(mContext).load(bannerImageUrls[position]).into(bannerAdsIV);
            container.addView(itemView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void setDynamicWidthHeightToImageView(ImageView singleBannerIV) {

        try {
            if (singleBannerIV != null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();

                Activity activity = (Activity) mContext;
                activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                ViewGroup.LayoutParams layoutParams = singleBannerIV.getLayoutParams();
                layoutParams.height = ((displayMetrics.widthPixels) / 2) + ((displayMetrics.widthPixels) / 6);
                singleBannerIV.setLayoutParams(layoutParams);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
