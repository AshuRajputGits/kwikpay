package com.uk.recharge.kwikpay.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

public class CountrySpinnerAdapter extends BaseAdapter 
{
	Activity context;

	ArrayList<HashMap<String, String>> spinnerArrayList;

	public CountrySpinnerAdapter(Activity context,ArrayList<HashMap<String, String>> spinnerArrayList) 
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		this.spinnerArrayList = spinnerArrayList;
	}

	public int getCount() 
	{
		// TODO Auto-generated method stub
		return spinnerArrayList.size();
	}

	public Object getItem(int position) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder 
	{
		TextView countryName,countryPostalCode;

		ViewHolder(View view)
		{
			countryName = (TextView) view.findViewById(R.id.countryNameTV);
			countryPostalCode = (TextView) view.findViewById(R.id.countryPostalCodeTV);
		}

	}

	@SuppressWarnings("static-access")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View row=convertView;
		ViewHolder holder;

		if(row==null)
		{
			LayoutInflater inflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row=inflater.inflate(R.layout.single_country_spinner, parent, false);
			holder=new ViewHolder(row);
			row.setTag(holder);
		}

		else
		{
			holder=(ViewHolder)row.getTag();
		}
		holder.countryName.setText(spinnerArrayList.get(position).get(ProjectConstant.AG_NAME));
		holder.countryPostalCode.setText("+"+spinnerArrayList.get(position).get(ProjectConstant.AG_COUNTRY_CODE));

		return row;
	}

}
