package com.uk.recharge.kwikpay.kwikvend.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductDetailsMO;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 7/10/2018.
 */

public class KVProductAdapter extends RecyclerView.Adapter<KVProductAdapter.KVProductViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<KVProductDetailsMO> kvProductDetailsMOArrayList = null;
    private Context context;
    private ProductSelectionListener listener = null;
    private String currencyCode = "";
    private int maximumVendCapacity = 0;
    private int numberOfProductSelectedByUser = 1;

    public KVProductAdapter(Context context, ArrayList<KVProductDetailsMO> kvProductDetailsMOArrayList) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.kvProductDetailsMOArrayList = kvProductDetailsMOArrayList;
        this.listener = (ProductSelectionListener) context;
        try {
            currencyCode = KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getCurrencySign();
            if (KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getMaxVendCapacity() != null &&
                    !KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getMaxVendCapacity().equals("")) {
                maximumVendCapacity = Integer.parseInt(KVProductDetailsSingleton.getInstance().
                        getKvProductOthersDetailsMO().getMaxVendCapacity());

                Log.e("maximumVendCapacity", "Cap " + maximumVendCapacity);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public KVProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.kv_product_row, parent, false);
        return new KVProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KVProductViewHolder holder, int position) {

        if (kvProductDetailsMOArrayList.get(position).getProductSelected()) {
            holder.kvProductPrice.setTextColor(context.getResources().getColor(R.color.lightGreen));
            holder.kvProductName.setTextColor(context.getResources().getColor(R.color.lightGreen));
            holder.productLayout.setBackgroundResource(R.drawable.kvproduct_selected_border);
            holder.kvProductSelectedTickIcon.setVisibility(View.VISIBLE);
        } else {
            holder.kvProductPrice.setTextColor(context.getResources().getColor(R.color.darkGreyColor));
            holder.kvProductName.setTextColor(context.getResources().getColor(R.color.darkGreyColor));
            holder.productLayout.setBackgroundResource(R.drawable.kvproduct_default_border);
            holder.kvProductSelectedTickIcon.setVisibility(View.GONE);
        }
        holder.kvProductPrice.setText(Utility.fromHtml(currencyCode) + String.valueOf(kvProductDetailsMOArrayList.get(position).getProductPrice()));
        holder.kvProductName.setText(kvProductDetailsMOArrayList.get(position).getProductName());
        try {
            Glide.with(context).load(kvProductDetailsMOArrayList.get(position).getProductURL()).into(holder.kvProductImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return kvProductDetailsMOArrayList.size();
    }

    public class KVProductViewHolder extends RecyclerView.ViewHolder {

        private TextView kvProductPrice;
        private TextView kvProductName;
        private ImageView kvProductInfoIcon;
        private ImageView kvProductSelectedTickIcon;
        private ImageView kvProductImage;
        private RelativeLayout productLayout;

        public KVProductViewHolder(View itemView) {
            super(itemView);

            kvProductPrice = itemView.findViewById(R.id.kvProductPrice);
            kvProductName = itemView.findViewById(R.id.kvProductName);
            kvProductInfoIcon = itemView.findViewById(R.id.kvProductInfoIcon);
            kvProductSelectedTickIcon = itemView.findViewById(R.id.kvProductSelectedTickIcon);
            kvProductImage = itemView.findViewById(R.id.kvProductImage);
            productLayout = itemView.findViewById(R.id.productLayout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("number", "numberOfProductSelectedByUser " + numberOfProductSelectedByUser);
                    if (kvProductDetailsMOArrayList.get(getLayoutPosition()).getProductSelected()) {
                        kvProductDetailsMOArrayList.get(getLayoutPosition()).setProductSelected(false);
                        kvProductDetailsMOArrayList.get(getLayoutPosition()).setProductQuantity(1);
                        numberOfProductSelectedByUser = numberOfProductSelectedByUser - 1;
                    } else {
                        if (numberOfProductSelectedByUser <= maximumVendCapacity) {
                            kvProductDetailsMOArrayList.get(getLayoutPosition()).setProductSelected(true);
                            numberOfProductSelectedByUser = numberOfProductSelectedByUser + 1;
                        } else
                            listener.onMaxProductSelected();
                    }
                    KVProductAdapter.this.notifyDataSetChanged();
                }
            });

            kvProductInfoIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onInfoButtonClicked(getLayoutPosition());
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public interface ProductSelectionListener {
        void onMaxProductSelected();

        void onInfoButtonClicked(int position);
    }

    public ArrayList<KVProductDetailsMO> getSelectedProductsFromRV() {
        return kvProductDetailsMOArrayList;
    }

}
