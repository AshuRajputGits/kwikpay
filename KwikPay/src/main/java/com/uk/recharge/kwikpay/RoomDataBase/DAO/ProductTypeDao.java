package com.uk.recharge.kwikpay.RoomDataBase.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.uk.recharge.kwikpay.RoomDataBase.Models.ProductType;

import java.util.List;

@Dao
public interface ProductTypeDao {

    @Query("SELECT * FROM ProductType")
    List<ProductType> getAll();

    @Query("SELECT * FROM ProductType where parent_id=:parent_id")
    List<ProductType> getAll(String parent_id);

    @Insert
    void insert(List<ProductType> list);

    @Query("delete from ProductType")
    void clear();

    @Query("SELECT COUNT(*) from ProductType")
    int count();

}
