package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidation;
import com.ar.library.validation.FieldValidationWithMessage;
import com.ar.library.validation.TempFieldValidation;
import com.cm.hybridmessagingsdk.HybridMessaging;
import com.cm.hybridmessagingsdk.listener.OnRegistrationListener;
import com.cm.hybridmessagingsdk.listener.OnVerificationStatus;
import com.cm.hybridmessagingsdk.util.Registration;
import com.cm.hybridmessagingsdk.util.VerificationStatus;
import com.uk.recharge.kwikpay.myaccount.MyTransactionActivity;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile extends Activity implements OnClickListener, AsyncRequestListenerViaPost {
    private TextView otpNumberLabel, otpResendButton;
    private TextView editProfileChangePassword;
    private SharedPreferences preferences;
    private int flag = 0;
    private String promotionCoupon = "0";
    private EditText myprofileFName, myprofileEmail, myprofileMobNo, otpNumberEditText;
    private CheckBox myprofilePromotion;
    private boolean isEditProfileBtnClicked = false, isOtpVerifiedSuccessfully = false;
    private Button otpProceedButton, editProfileProceedButton;
    private String apiResponseMobileNumber = "", formatedMobileNumberForCMSdk = "";
    private ProgressBar editProgressBar;
    private TextView footerFavouriteTxnBtn, footerMyTxnBtn;
    private String receivedOTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile);
        settingIds();

        flag = 1;
        new LoadResponseViaPost(EditProfile.this, formJSONParam(), true).execute("");
    }

    private String formJSONParam() {
        JSONObject jsonobj = new JSONObject();

        try {
            jsonobj.put("APISERVICE", "KPVIEWPROFILE");
            jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
            jsonobj.put("DEVICEOS", "Android");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("USERID", preferences.getString("KP_USER_ID", "0"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @SuppressLint("NewApi")
    private void settingIds() {
        preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        TextView headerTitle = findViewById(R.id.mainHeaderTitleName);
        headerTitle.setText("My profile ");

        // SETTING THE ID'S FOR THE OTP RELATED FIELD
        otpNumberLabel = findViewById(R.id.editProfileMobNoOtpLabel);
        otpNumberEditText = findViewById(R.id.editProfileMobNumbOtp);
        otpResendButton = findViewById(R.id.editProfileResendOTP);
        otpProceedButton = findViewById(R.id.editProfileProceedButtonWithOTP);
        editProfileProceedButton = findViewById(R.id.editProfileWithoutOTPButton);

        otpNumberLabel.setText(Html.fromHtml("Enter verification code  here" + "<font color='#ff0000'>*"));
        editProgressBar = findViewById(R.id.editProfileProgressBar);
        footerFavouriteTxnBtn = findViewById(R.id.footerFavouriteBtn);
        footerMyTxnBtn = findViewById(R.id.footerMyTxnBtn);
        editProfileChangePassword = findViewById(R.id.editProfileChangePasswordButton);

        TextView myEditButton = findViewById(R.id.footerEditProfileBtn);
        myEditButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_profile_icon, 0, 0);
        myEditButton.setTextColor(getResources().getColor(R.color.darkGreen));

        //SETTING THE IMAGE AND COLOR ON TEXT ON BUTTON PROGRAMMATICALLY
        myprofilePromotion = findViewById(R.id.redeemCouponCheckBox);

        // EDIT TEXT SETTING ID'S
        myprofileFName = findViewById(R.id.editProfileFirstname);
        myprofileEmail = findViewById(R.id.editProfileMalID);
        myprofileMobNo = findViewById(R.id.editProfileMobNumb);

        //SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);

        CustomSlidingDrawer csd = new CustomSlidingDrawer(EditProfile.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();
        footerFavouriteTxnBtn.setOnClickListener(this);
        footerMyTxnBtn.setOnClickListener(this);
        editProfileChangePassword.setOnClickListener(this);
        otpResendButton.setOnClickListener(this);

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_PROFILE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editProfileSubmitButton(View editButton) {
        if (EditProfileValidation()) {
            if (apiResponseMobileNumber.equals(myprofileMobNo.getText().toString())) {
                flag = 2;
                isEditProfileBtnClicked = true;
                new LoadResponseViaPost(EditProfile.this, formEditProfile(), true).execute("");
            } else {
                try {
                    /*HybridMessaging.resetLogin();
                    formatedMobileNumberForCMSdk = "0044" + myprofileMobNo.getText().toString().substring(1);
                    registerNewUserFromCMSdk(formatedMobileNumberForCMSdk);
                    Thread.sleep(500);
                    showOTPLayout();*/

                    callingGetOTPApi();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void callingGetOTPApi() {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "KP_GENERATE_OTP");
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("MOBILE", myprofileMobNo.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {
                                    if (childJson.has("OTP")) {
                                        receivedOTP = childJson.getString("OTP");
                                        showOTPLayout();
                                    }
                                } else {
                                    ARCustomToast.showToast(EditProfile.this, childJson.optString("DESCRIPTION"));
                                }
                            } else {
                                ARCustomToast.showToast(EditProfile.this, childJson.optString("DESCRIPTION"));
                            }
                        }

                    } catch (Exception e) {
                        ARCustomToast.showToast(EditProfile.this, "Invalid response, parsing error!!!");
                    }
                } else {
                    if (Utility.isInternetAvailable(EditProfile.this))
                        ARCustomToast.showToast(EditProfile.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(EditProfile.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(EditProfile.this))
                    ARCustomToast.showToast(EditProfile.this, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(EditProfile.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void registerNewUserFromCMSdk(String msisdn) {
        try {
            //USE THIS METHOD IF YOU WANT TO REGISTER NEW USER[FIRST TIME]
            HybridMessaging.registerNewUser(msisdn, new OnRegistrationListener() {
                @Override
                public void onReceivedRegistration(Registration registrationResponse) {
                    // TODO Auto-generated method stub
                    try {
                        if (registrationResponse.getStatus().toString().equalsIgnoreCase("WaitingForPin")) {
//							showOTPLayout();
                        } else if (registrationResponse.getStatus().toString().equalsIgnoreCase("Verified")) {
                            //If number is already verified
                            flag = 2;
                            isEditProfileBtnClicked = true;
                            new LoadResponseViaPost(EditProfile.this, formEditProfile(), true).execute("");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable arg0) {
                }
            });
        } catch (Exception ee) {

        }
    }

    public void editProfileOTPConfirmMethod(View otpNumberView) {
        if (EditProfileValidation()) {
            if (otpNumberEditText.getText().toString().isEmpty()) {
                otpNumberEditText.requestFocus();
                otpNumberEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                ARCustomToast.showToast(EditProfile.this, "Please enter a valid OTP", Toast.LENGTH_LONG);
            } else if (otpNumberEditText.getText().toString().length() < 4) {
                otpNumberEditText.requestFocus();
                otpNumberEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                ARCustomToast.showToast(EditProfile.this, "Please enter a valid OTP", Toast.LENGTH_LONG);
            } else if (!otpNumberEditText.getText().toString().equals(receivedOTP)) {
                otpNumberEditText.requestFocus();
                otpNumberEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                ARCustomToast.showToast(EditProfile.this, "Last pin verification failed");
            } else {
                otpNumberEditText.requestFocus();
                otpNumberEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                /*if (isOtpVerifiedSuccessfully) {
                    flag = 2;
                    isEditProfileBtnClicked = true;
                    new LoadResponseViaPost(EditProfile.this, formEditProfile(), true).execute("");
                } else {
                    editProgressBar.setVisibility(View.VISIBLE);
                    confirmReceivedOTP(otpNumberEditText.getText().toString());
                }*/

                flag = 2;
                isEditProfileBtnClicked = true;
                new LoadResponseViaPost(EditProfile.this, formEditProfile(), true).execute("");
            }
        }
    }

    private void showOTPLayout() {
        myprofileMobNo.clearFocus();
        otpNumberEditText.requestFocus();
        otpNumberLabel.setVisibility(View.VISIBLE);
        otpNumberEditText.setVisibility(View.VISIBLE);
        otpResendButton.setVisibility(View.VISIBLE);
        otpProceedButton.setVisibility(View.VISIBLE);
        editProfileProceedButton.setVisibility(View.GONE);
    }

    private void hideOTPLayout() {
        otpNumberLabel.setVisibility(View.GONE);
        otpNumberEditText.setVisibility(View.GONE);
        otpResendButton.setVisibility(View.GONE);
        otpProceedButton.setVisibility(View.GONE);
        editProfileProceedButton.setVisibility(View.VISIBLE);
    }

    private void resendOTP(String enteredMsisdn) {
        HybridMessaging.requestNewVerificationPin(enteredMsisdn, new OnRegistrationListener() {

            @Override
            public void onReceivedRegistration(Registration resendRegResponse) {
                // TODO Auto-generated method stub
//				Log.i("", "CM SDK response of RESEND ReceivedData "+resendRegResponse.getStatus());
                editProgressBar.setVisibility(View.GONE);
//				ARCustomToast.showToast(EditProfile.this, resendRegResponse.getStatus().toString(), Toast.LENGTH_LONG);
            }

            @Override
            public void onError(Throwable error) {
                // TODO Auto-generated method stub
                editProgressBar.setVisibility(View.GONE);
//				Log.i("", "CM SDK response of verification "+error);
            }
        });
    }

    private void confirmReceivedOTP(String verificationCode) {
        HybridMessaging.registerUserByPincode(verificationCode, new OnVerificationStatus() {
            @Override
            public void onVerificationStatus(VerificationStatus verificationStatus) {
                if (verificationStatus.toString().equalsIgnoreCase("Verified")) {
                    isOtpVerifiedSuccessfully = true;
                    otpResendButton.setVisibility(View.INVISIBLE);
                    otpProceedButton.setEnabled(false);

                    editProgressBar.setVisibility(View.GONE);

                    SharedPreferences.Editor otpEditor = preferences.edit();
                    otpEditor.putBoolean("isUserVerifiedFromCM", true);
                    otpEditor.putString("VerifiedMobNoViaCM", myprofileMobNo.getText().toString());
                    otpEditor.apply();

                    flag = 2;
                    isEditProfileBtnClicked = true;
                    new LoadResponseViaPost(EditProfile.this, formEditProfile(), true).execute("");
                } else {
                    editProgressBar.setVisibility(View.GONE);
                    ARCustomToast.showToast(EditProfile.this, "Last pin verification failed", Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onError(Throwable verificationError) {
                editProgressBar.setVisibility(View.GONE);
            }
        });
    }

    public String formEditProfile() {
        JSONObject jsonobj = new JSONObject();
        if (myprofilePromotion.isChecked()) {
            promotionCoupon = "1";
        }
        String trimmedFirstName = myprofileFName.getText().toString().replaceAll(" +", " ").trim();

        try {
            jsonobj.put("APISERVICE", "KPEDITPROFILE");
            jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
            jsonobj.put("DEVICEOS", "Android");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("EMAILID", myprofileEmail.getText().toString());
            jsonobj.put("MOBILENUMBER", myprofileMobNo.getText().toString());
            jsonobj.put("FIRSTNAME", trimmedFirstName);
            jsonobj.put("USERID", preferences.getString("KP_USER_ID", ""));
            jsonobj.put("RECEIVEPROMOTIONAL", promotionCoupon);

            jsonobj.put("GENDER", "");
            jsonobj.put("MIDNAME", "");
            jsonobj.put("LASTNAME", "");
            jsonobj.put("TITLE", "");
            jsonobj.put("DOB", "");
            jsonobj.put("ADDRESSONE", "");
            jsonobj.put("ADDRESSTWO", "");
            jsonobj.put("ADDRESSTHREE", "");
            jsonobj.put("POSTALTOWN", "");
            jsonobj.put("POSTALCODE", "");
            jsonobj.put("COUNTRY", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.footerFavouriteBtn) {
            startActivity(new Intent(EditProfile.this, FavouriteScreen.class));
        } else if (v.getId() == R.id.footerMyTxnBtn) {
            startActivity(new Intent(EditProfile.this, MyTransactionActivity.class));
        } else if (v.getId() == R.id.editProfileChangePasswordButton) {
            startActivity(new Intent(EditProfile.this, ChangePassword.class));
        } else if (v.getId() == R.id.editProfileResendOTP) {
            /*editProgressBar.setVisibility(View.VISIBLE);
            resendOTP(formatedMobileNumberForCMSdk);*/
            callingGetOTPApi();
        }
    }

    public boolean EditProfileValidation() {
        if (!FieldValidationWithMessage.firstNameIsValid(myprofileFName.getText().toString(), myprofileFName).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(myprofileMobNo);
            FieldValidationWithMessage.removeAllErrorIcons(myprofileEmail);
            ARCustomToast.showToast(EditProfile.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
            return false;
        } else if (!TempFieldValidation.mobNumberIsValidWithoutLeadingZero(myprofileMobNo.getText().toString(), myprofileMobNo).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(myprofileFName);
            FieldValidationWithMessage.removeAllErrorIcons(myprofileEmail);
            ARCustomToast.showToast(EditProfile.this, TempFieldValidation.getErrorMessage(), Toast.LENGTH_LONG);
            return false;
        } else if (!(myprofileMobNo.getText().toString().startsWith("07"))) {
            myprofileMobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
            FieldValidationWithMessage.removeAllErrorIcons(myprofileFName);
            FieldValidationWithMessage.removeAllErrorIcons(myprofileEmail);
            ARCustomToast.showToast(EditProfile.this, "please enter a valid 11 digit mobile no. starting with 07", Toast.LENGTH_LONG);
            return false;
        } else if (myprofileMobNo.getText().toString().length() < 11) {
            myprofileMobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
            FieldValidationWithMessage.removeAllErrorIcons(myprofileFName);
            FieldValidationWithMessage.removeAllErrorIcons(myprofileEmail);
            ARCustomToast.showToast(EditProfile.this, "please enter a valid 11 digit mobile no. starting with 07", Toast.LENGTH_LONG);
            return false;
        } else if (!FieldValidation.emailAddressIsValid(myprofileEmail.getText().toString(), myprofileEmail)) {
            FieldValidationWithMessage.removeAllErrorIcons(myprofileFName);
            FieldValidationWithMessage.removeAllErrorIcons(myprofileMobNo);
            ARCustomToast.showToast(EditProfile.this, "please enter a valid email id", Toast.LENGTH_LONG);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("") && !loadedString.equals("Exception")) {
            if (flag == 1) {
                try {
                    JSONObject view_profile_json = new JSONObject(loadedString);
                    JSONObject view_profile_json_response = view_profile_json.getJSONObject(ProjectConstant.RESPONSE_TAG);

                    if (view_profile_json_response.has(ProjectConstant.API_RESPONSE_CODE)) {
                        if (view_profile_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                            if (view_profile_json_response.has("FIRSTNAME") && view_profile_json_response.has("MIDNAME")
                                    && view_profile_json_response.has("LASTNAME")) {
                                myprofileFName.setText(view_profile_json_response.getString("FIRSTNAME"));
//								myprofileMName.setText(view_profile_json_response.getString("MIDNAME"));
//								myprofileLName.setText(view_profile_json_response.getString("LASTNAME"));
                            }

                            if (view_profile_json_response.has("EMAILID")) {
                                myprofileEmail.setText(view_profile_json_response.getString("EMAILID"));
                                if (!myprofileEmail.equals("")) {
                                    myprofileEmail.setEnabled(false);
                                }
                            }

                            if (view_profile_json_response.has("MOBILENUMBER")) {
                                myprofileMobNo.setText(view_profile_json_response.getString("MOBILENUMBER"));
                                apiResponseMobileNumber = view_profile_json_response.getString("MOBILENUMBER");
                                if (!myprofileMobNo.equals("")) {
                                    // myprofileMobNo.setEnabled(false);
                                }
                            }
                            if (view_profile_json_response.has("RECEIVEPROMOTIONAL")) {
                                if (view_profile_json_response.getString("RECEIVEPROMOTIONAL").equals("1"))
                                    myprofilePromotion.setChecked(true);
                                else if (view_profile_json_response.getString("RECEIVEPROMOTIONAL").equals("0"))
                                    myprofilePromotion.setChecked(false);
                            }
                        } else {
                            if (view_profile_json_response.has("DESCRIPTION"))
                                ARCustomToast.showToast(EditProfile.this, view_profile_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (flag == 2) {
                try {
                    JSONObject view_profile_json = new JSONObject(loadedString);
                    JSONObject view_profile_json_response = view_profile_json.getJSONObject(ProjectConstant.RESPONSE_TAG);

                    if (view_profile_json_response.has(ProjectConstant.API_RESPONSE_CODE)) {
                        if (view_profile_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                            if (view_profile_json_response.has("USERNAME")) {
                                SharedPreferences.Editor editProfileEditor = preferences.edit();
                                editProfileEditor.putString("KP_USER_NAME", view_profile_json_response.getString("USERNAME")).commit();
                            }

                            hideOTPLayout();
                            // NEED TO IMPLEMENT WHERE TO REDRIECT AFTER SUCCESSFULL SUBMITION OF EDIT PROFILE.
                            ARCustomToast.showToast(EditProfile.this, "successfully updated your profile.", Toast.LENGTH_LONG);
                            EditProfile.this.finish();
                        } else {
                            if (view_profile_json_response.has("DESCRIPTION")) {
                                ARCustomToast.showToast(EditProfile.this, view_profile_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } else {
            if (!isEditProfileBtnClicked)
                EditProfile.this.finish();
            ARCustomToast.showToast(EditProfile.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }
}
