package com.uk.recharge.kwikpay.catering;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.R;

public class CateringReceiptScreen extends AppCompatActivity {

    private String receivedOrderId = "";
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catering_receipt_screen);

        init();

        bundle = getIntent().getExtras();

        if (bundle != null) {
            if (bundle.containsKey("PG_APP_TXN_ID_KEY"))
                receivedOrderId = bundle.getString("PG_APP_TXN_ID_KEY");
            if (bundle.containsKey("IS_PAYMENT_SUCCESS")) {
                boolean isPaymentSuccess = bundle.getBoolean("IS_PAYMENT_SUCCESS");
                if (isPaymentSuccess)
                    findViewById(R.id.kcReceiptSuccessLayout).setVisibility(View.VISIBLE);
                else
                    findViewById(R.id.kcReceiptFailureLayout).setVisibility(View.VISIBLE);
            }
        }
    }

    private void init() {

        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);

        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(CateringReceiptScreen.this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        findViewById(R.id.backbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bundle.containsKey("IS_PAYMENT_SUCCESS")) {
                    boolean isPaymentSuccess = bundle.getBoolean("IS_PAYMENT_SUCCESS");
                    if (isPaymentSuccess) {
                        Intent intent = new Intent(CateringReceiptScreen.this, HomeScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
                finish();
            }
        });

        findViewById(R.id.ReceiptSuccessButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CateringReceiptScreen.this, CateringOrderReceipt.class);
                intent.putExtra("PG_APP_TXN_ID_KEY", receivedOrderId);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.kcReceiptRetryButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CateringReceiptScreen.this, MyOrders.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
