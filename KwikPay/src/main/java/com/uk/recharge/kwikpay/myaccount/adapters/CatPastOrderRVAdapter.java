package com.uk.recharge.kwikpay.myaccount.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.catering.CateringViewDetails;
import com.uk.recharge.kwikpay.catering.models.CateringCompleteOrdersMO;
import com.uk.recharge.kwikpay.myaccount.MyTransactionSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 11/5/2018.
 */

public class CatPastOrderRVAdapter extends RecyclerView.Adapter<CatPastOrderRVAdapter.CatBothOrdersViewHolder> {

    private LayoutInflater layoutInflater = null;
    //1: OnGoing Order , 2: Past Order
    ArrayList<CateringCompleteOrdersMO> cateringPastCompleteOrderList = null;
    private Context context = null;

    public CatPastOrderRVAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        cateringPastCompleteOrderList = MyTransactionSingleton.getInstance().getCateringPastCompleteOrderList();
    }

    @NonNull
    @Override
    public CatBothOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cat_ongoing_post_order_row, parent, false);
        return new CatBothOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CatBothOrdersViewHolder holder, int position) {

        try {
            holder.transactionDateTime.setText(cateringPastCompleteOrderList.get(position).getCateringTxnDateTime());
            holder.transactionOrderID.setText(cateringPastCompleteOrderList.get(position).getCateringOrderId());
            holder.transactionRCAmount.setText(cateringPastCompleteOrderList.get(position).getCateringPaymentAmount());
            holder.transactionRCAmount.setText(Utility.fromHtml(cateringPastCompleteOrderList.get(position).getCateringDisplayCurrencyCode()) +
                    cateringPastCompleteOrderList.get(position).getCateringPaymentAmount());
            holder.transactionOrderItemsCount.setText("Items : " + cateringPastCompleteOrderList.get(position).getCateringTotalItems());
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        /*try {
            if (cateringPastCompleteOrderList.get(position).getCateringCancelStatus().equals("1")) {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderCancelled));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusCancel));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_red));
            } else if (cateringPastCompleteOrderList.get(position).getCateringDeliveryStatus().equals("1")) {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderDelivered));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusReadyOrPrep));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_green));
            } else if (cateringPastCompleteOrderList.get(position).getCateringReadyStatus().equals("1")) {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderReadyForCollect));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusReadyOrPrep));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_green));
            } else if (cateringPastCompleteOrderList.get(position).getCateringAcceptedStatus().equals("1")) {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderUnderPrep));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusUnderPrep));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_blue));
            } else {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderPlaced));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusPlaced));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_orange));
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }*/

        holder.transactionOrderStatus.setVisibility(View.INVISIBLE);

        try {
            Glide.with(context)
                    .load(cateringPastCompleteOrderList.get(position).getProductDistributorDetails().getDistributorLogo())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(holder.operatorImages);
        } catch (Exception e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (cateringPastCompleteOrderList != null && cateringPastCompleteOrderList.size() > 0) {
            return cateringPastCompleteOrderList.size();
        }
        return 0;
    }

    public class CatBothOrdersViewHolder extends RecyclerView.ViewHolder {

        private TextView transactionOrderID, transactionDateTime, transactionRCAmount,
                transactionOrderItemsCount, transactionOrderStatus;
        private ImageView operatorImages;

        public CatBothOrdersViewHolder(View itemView) {
            super(itemView);
            transactionOrderID = itemView.findViewById(R.id.transactionOrderID);
            transactionDateTime = itemView.findViewById(R.id.transactionDateTime);
            transactionRCAmount = itemView.findViewById(R.id.transactionRCAmount);
            transactionOrderItemsCount = itemView.findViewById(R.id.transactionOrderItemsCount);
            transactionOrderStatus = itemView.findViewById(R.id.transactionOrderStatus);
            operatorImages = itemView.findViewById(R.id.operatorImages);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, CateringViewDetails.class)
                            .putExtra("SELECTED_ORDER_TXN", cateringPastCompleteOrderList.get(getLayoutPosition())));
                }
            });
        }
    }
}
