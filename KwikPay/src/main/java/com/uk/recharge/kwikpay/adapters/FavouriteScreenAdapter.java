package com.uk.recharge.kwikpay.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

public class FavouriteScreenAdapter extends BaseAdapter 
{
	Activity context;

	ArrayList<HashMap<String, String>> favouriteList;

	public FavouriteScreenAdapter(Activity context,ArrayList<HashMap<String, String>> arrayListMap) 
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		this.favouriteList = arrayListMap;
	}

	public int getCount() 
	{
		// TODO Auto-generated method stub
		return favouriteList.size();
	}

	public Object getItem(int position) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder 
	{
//		TextView nickNameTV,serviceProviderTV,descriptionTV;
		TextView serviceProviderTV,favAmount;
//		Button favRepeatRC;

		ViewHolder(View view)
		{
			serviceProviderTV = (TextView) view.findViewById(R.id.favouriteServiceProvider);
			favAmount = (TextView) view.findViewById(R.id.favouriteAmount);
//			favRepeatRC=(Button)view.findViewById(R.id.favouriteRepeatRCBtn);
//			nickNameTV = (TextView) view.findViewById(R.id.favouriteNickName);
//			descriptionTV = (TextView) view.findViewById(R.id.favouriteDescription);
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View row=convertView;
		ViewHolder holder;

		if(row==null)
		{
			LayoutInflater inflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row=inflater.inflate(R.layout.singlefavourite_row, parent, false);
			holder=new ViewHolder(row);
			row.setTag(holder);
		}

		else
		{
			holder=(ViewHolder)row.getTag();
		}

		if(position%2==0)
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewEvenColor));
		}
		else
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewOddColor));
		}
//		holder.nickNameTV.setText(favouriteList.get(position).get(ProjectConstant.FAV_NICKNAME));
		holder.serviceProviderTV.setText(favouriteList.get(position).get(ProjectConstant.FAV_SUBSCRIBERNUM)+"\n"+
										 favouriteList.get(position).get(ProjectConstant.FAV_OPCODE));
		holder.favAmount.setText(favouriteList.get(position).get(ProjectConstant.FAV_AMOUNT));

		return row;
	}

}
