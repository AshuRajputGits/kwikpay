
package com.uk.recharge.kwikpay;

import java.text.DecimalFormat;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.appsflyer.AppsFlyerLib;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

public class ViewDetailScreen extends Activity implements OnClickListener
{
//	private TextView backBtnTV;
//	private LinearLayout backBtnTV;
//	TextView headerBelowTV,backBtnTV,headerFavBtn,headerTicketBtn;
//	TextView headerProfileBtn,headerChangePWBtn,cartItemCounter;
//	View separatorLine;
//	LinearLayout myTransitionLinearLayout;
	private HashMap<String,String> receivedViewDetailHashMap;
	private TextView valueDOT,valueOrderId,valueServiceType,valueServiceProvider,valueMobNumber,valueTopUpAmt,valueCouponAmt,
	valueAmountPaid, valuePinNumber, valuePinExpiryDate, valueCouponSerialNumber;
	private Button raiseTicketBtn,backBtnTV;
	private LinearLayout pinNumberLayout,pinExpiryDateLayout,couponAmountLayout,couponSrNumberLayout;
	private boolean setHeader=false;
//	private SharedPreferences preference;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		Bundle receivedBundle=getIntent().getExtras();
		if(receivedBundle!=null)
		{
			if(receivedBundle.containsKey("RECEIPT_TYPE"))
			{
				if(receivedBundle.getInt("RECEIPT_TYPE")==1)
				{
					setHeader=true;
					setContentView(R.layout.view_detail_screen_etopup);
				}
				else if(receivedBundle.getInt("RECEIPT_TYPE")==2)
				{
					setHeader=true;
					setContentView(R.layout.view_detail_screen_pintopup);
				}
				else if(receivedBundle.getInt("RECEIPT_TYPE")==0)
					setContentView(R.layout.view_detail_screen);
				
				settingIDs();
				
				if(receivedBundle.containsKey("TRANSACTION_VIEW_DETAIL"))
				{
					try
					{
						receivedViewDetailHashMap = (HashMap<String, String>) receivedBundle.getSerializable("TRANSACTION_VIEW_DETAIL");

						valueDOT.setText(receivedViewDetailHashMap.get(ProjectConstant.TXN_TRANSACTIONDATETIME));
						valueOrderId.setText(receivedViewDetailHashMap.get(ProjectConstant.TXN_ORDERID));
						valueServiceType.setText(receivedViewDetailHashMap.get(ProjectConstant.TXN_SERVICENAME));
						valueServiceProvider.setText(receivedViewDetailHashMap.get(ProjectConstant.TXN_OPERATORNAME));
						valueMobNumber.setText(receivedViewDetailHashMap.get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
						
						if(receivedViewDetailHashMap.containsKey(ProjectConstant.TXN_DISPLAYCURRENCYCODE))
						{
							valueTopUpAmt.setText(Html.fromHtml(receivedViewDetailHashMap.get(ProjectConstant.TXN_DISPLAYCURRENCYCODE))  +" "+ 
									getAmountInTwoDecimal(receivedViewDetailHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT)));
							
							String couponAmountRedeemed=receivedViewDetailHashMap.get("CPNVALUE");
							if(couponAmountRedeemed.equals("") || couponAmountRedeemed.equalsIgnoreCase("N/A") || isDoubleValueIsZero(couponAmountRedeemed))
								couponAmountLayout.setVisibility(View.GONE);
							else
								valueCouponAmt.setText(Html.fromHtml(receivedViewDetailHashMap.get(ProjectConstant.TXN_DISPLAYCURRENCYCODE)) +" "+couponAmountRedeemed);
						}
						else
						{
							valueTopUpAmt.setText(Html.fromHtml("&pound;")+" "+getAmountInTwoDecimal(receivedViewDetailHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT)));
							
							if(receivedViewDetailHashMap.containsKey("CPNVALUE"))
							{
								String couponAmountRedeemed=receivedViewDetailHashMap.get("CPNVALUE");
								if(couponAmountRedeemed.equals("") || couponAmountRedeemed.equalsIgnoreCase("N/A") || isValueZero(couponAmountRedeemed))
									couponAmountLayout.setVisibility(View.GONE);
								else
									valueCouponAmt.setText(Html.fromHtml("&pound;") +" "+couponAmountRedeemed);
							}
							else 
								couponAmountLayout.setVisibility(View.GONE);
						}
						
						// CHECKING VALUES FOR COUPON CODE USED
						if(receivedViewDetailHashMap.containsKey("CPNSRNUM"))
						{
							String couponCodeUsed=receivedViewDetailHashMap.get("CPNSRNUM");
							if(couponCodeUsed.equals("") || couponCodeUsed.equalsIgnoreCase("N/A") || isValueZero(couponCodeUsed))
								couponSrNumberLayout.setVisibility(View.GONE);
							else
								valueCouponSerialNumber.setText(couponCodeUsed);
						}
						else
						{
							couponSrNumberLayout.setVisibility(View.GONE);
						}
						
						
						if(receivedViewDetailHashMap.containsKey(ProjectConstant.TXN_PAYMENTCURRENCYCODE))
						{
							valueAmountPaid.setText(Html.fromHtml(receivedViewDetailHashMap.get(ProjectConstant.TXN_PAYMENTCURRENCYCODE)) + " " +
									receivedViewDetailHashMap.get(ProjectConstant.TXN_PAYMENTAMOUNT));
						}
						else 
						{
							valueAmountPaid.setText(Html.fromHtml("&pound;")+ " " +	receivedViewDetailHashMap.get(ProjectConstant.TXN_PAYMENTAMOUNT));
						}
						
						// PIN NUMBER 
						if(receivedViewDetailHashMap.containsKey("RECHARGEPIN"))
						{
							String pinNumber=receivedViewDetailHashMap.get("RECHARGEPIN");
							if(pinNumber.equals("") || pinNumber.equalsIgnoreCase("N/A") || isValueZero(pinNumber))
								pinNumberLayout.setVisibility(View.GONE);
							else
								valuePinNumber.setText(pinNumber);
						}
						else
						{
							pinNumberLayout.setVisibility(View.GONE);
						}
						
						// PIN EXPIRY DATE
						if(receivedViewDetailHashMap.containsKey("RECHARGEPINEXPIRYDATE"))
						{
							String pinExpiryDate=receivedViewDetailHashMap.get("RECHARGEPINEXPIRYDATE");
							if(pinExpiryDate.equals("") || pinExpiryDate.equalsIgnoreCase("N/A"))
								pinExpiryDateLayout.setVisibility(View.GONE);
							else 
								valuePinExpiryDate.setText(pinExpiryDate);
						}
						else
						{
							pinExpiryDateLayout.setVisibility(View.GONE);
						}
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}

		try
		{
			EventsLoggerSingleton.getInstance().setCommonEventLogs(this,getResources().getString(R.string.SCREEN_NAME_VIEW_DETAILS));
			/*String eventName=getResources().getString(R.string.SCREEN_NAME_VIEW_DETAILS);
			AppsFlyerLib.trackEvent(this, eventName, null);
			EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
			((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/

		}catch (Exception e){e.printStackTrace();}
	}

	//CHECKING INTEGER VALUE AS ZERO OR NON ZERO
	private boolean isValueZero(String value)
	{
		try
		{
			if(Integer.parseInt(value)==0)
				return true;
			else
				return false;
			
		}catch(Exception e)
		{
			return false;
		}
	}
	
	//CHECKING DOUBLE VALUE AS ZERO OR NON ZERO
	private boolean isDoubleValueIsZero(String value)
	{
		try
		{
			Double d=Double.parseDouble(value);
			int i=d.intValue();
			if(i==0)
				return true;
			else
				return false;
			
		}catch(Exception e)
		{
			return false;
		}
	}
	
	public static String getAmountInTwoDecimal(String amountValue) 
	{
		String amountString = "";
		try
		{
			Double d1 = Double.valueOf(amountValue);
			DecimalFormat dec = new DecimalFormat("0.00");
			amountString = dec.format(d1);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return amountString;
	}
	private void settingIDs() 
	{
		// TODO Auto-generated method stub
		/*headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
		headerBelow_ClassTV.setText("Transaction Details");*/
		
//		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);
//		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);
		
//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

		if(setHeader)
		{
			TextView headerTitle=(TextView)findViewById(R.id.mainHeaderTitleName);
			headerTitle.setText("Receipt");
		}
		
//		myTransitionLinearLayout=(LinearLayout)findViewById(R.id.myTransitionHeaderLayout);
//		myTransitionLinearLayout.setVisibility(View.VISIBLE);
		
//		backBtnTV=(TextView)findViewById(R.id.viewDetails_BackButtonTV);
		backBtnTV=(Button)findViewById(R.id.viewDetails_BackButtonTV);
		raiseTicketBtn=(Button)findViewById(R.id.viewDetail_RaiseTicketBtn);

		// IDs of all four buttons
//		headerFavBtn=(TextView)findViewById(R.id.header_FavouriteBtn);
//		headerProfileBtn=(TextView)findViewById(R.id.header_MyProfileBtn);
//		headerChangePWBtn=(TextView)findViewById(R.id.header_ChangePWBtn);
//		headerTicketBtn=(TextView)findViewById(R.id.header_MyTicketBtn);

		valueDOT=(TextView)findViewById(R.id.viewDetail_DOTTV);
		valueOrderId=(TextView)findViewById(R.id.viewDetail_OrderIdTV);
		valueServiceType=(TextView)findViewById(R.id.viewDetail_ServiceTypeTV);
		valueServiceProvider=(TextView)findViewById(R.id.viewDetail_ServiceProviderTV);
		valueMobNumber=(TextView)findViewById(R.id.viewDetail_MobNumTV);
		valueTopUpAmt=(TextView)findViewById(R.id.viewDetail_TopUpAmountTV);
		valueCouponAmt=(TextView)findViewById(R.id.viewDetail_CouponAmtTV);
		valueAmountPaid=(TextView)findViewById(R.id.viewDetail_AmountPaidTV);
		valuePinNumber = (TextView)findViewById(R.id.viewDetail_PinNumberTV);
		valuePinExpiryDate = (TextView)findViewById(R.id.viewDetail_PinExpiryDateTV);
		valueCouponSerialNumber = (TextView)findViewById(R.id.viewDetail_CouponSrNumTV);
		
		pinNumberLayout=(LinearLayout)findViewById(R.id.viewDetail_PinNumberLayout);
		pinExpiryDateLayout=(LinearLayout)findViewById(R.id.viewDetail_PinExpiryLayout);
		couponAmountLayout=(LinearLayout)findViewById(R.id.viewDetail_CouponAmountRedeemedLayout);
		couponSrNumberLayout=(LinearLayout)findViewById(R.id.viewDetail_CouponSrNumberLayout);
		
		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(ViewDetailScreen.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		backBtnTV.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ViewDetailScreen.this.finish();
			}
		});


//		headerFavBtn.setOnClickListener(this);
//		headerProfileBtn.setOnClickListener(this);
//		headerChangePWBtn.setOnClickListener(this);
//		headerTicketBtn.setOnClickListener(this);

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME

		/*DBQueryMethods database=new DBQueryMethods(ViewDetailScreen.this);
		try
		{
			database.open();
			TextView headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
			headerBelow_ClassTV.setText(database.getSubHeaderTitles("View details"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}*/
		
		raiseTicketBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				Intent raiseTicketIntent = new Intent(ViewDetailScreen.this,RaiseTicket.class);
				raiseTicketIntent.putExtra("RAISETICKET_ORDERID", receivedViewDetailHashMap.get(ProjectConstant.TXN_ORDERID));
				raiseTicketIntent.putExtra("RAISETICKET_SERVICE_PROVIDER", receivedViewDetailHashMap.get(ProjectConstant.TXN_OPERATORNAME));
				raiseTicketIntent.putExtra("RAISETICKET_DOT", receivedViewDetailHashMap.get(ProjectConstant.TXN_TRANSACTIONDATETIME));

				startActivity(raiseTicketIntent);	
			}
		});

	}

	@Override
	public void onClick(View v) 
	{
		// TODO Auto-generated method stub
		/*if(v.getId()==R.id.header_FavouriteBtn)
		{
			startActivity(new Intent(ViewDetailScreen.this,FavouriteScreen.class));
		}
		if(v.getId()==R.id.header_MyProfileBtn)
		{
			startActivity(new Intent(ViewDetailScreen.this,EditProfile.class));
		}
		if(v.getId()==R.id.header_ChangePWBtn)
		{
			startActivity(new Intent(ViewDetailScreen.this,ChangePassword.class));
		}
		if(v.getId()==R.id.header_MyTicketBtn)
		{
			startActivity(new Intent(ViewDetailScreen.this,MyTicket.class));
		}*/

	}
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preference!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preference.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}
		
		}
	}*/


}
