package com.uk.recharge.kwikpay;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.adapters.MyTicketAdapter;
import com.uk.recharge.kwikpay.database.DBQueryMethods;
import com.uk.recharge.kwikpay.utilities.CustomDialogBox;

@SuppressLint("NewApi")
public class MyTicket extends Activity implements OnClickListener,AsyncRequestListenerViaPost
{
	ListView myTicketListView;
	MyTicketAdapter myTicketAdapter;
	ArrayList<HashMap<String, String>> myTicketArrayList;
	HashMap<String, String> myTicketHashMap;
//	View separatorLine;
	TextView myTicketBtn,headerFavBtn,cartItemCounter;
//	TextView headerProfileBtn,headerChangePWBtn;
	SharedPreferences preference;
	boolean isRadioOn=false;
	JSONArray myTicketJsonArray;
	int apiHitPosition=0;
	String selectedSubsNumber="",selectedServiceProvider="",selectedDOT="",selectedOrderId="",selectedAmount="",selectedTOP="",
			selectedComment="",descriptionMsg="",selectedQueryTypeId="";
//	LinearLayout myTransitionLinearLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.myticket_screen);

		settingIDs();

		apiHitPosition=1;
		new LoadResponseViaPost(MyTicket.this, formMyTicketJSON(), true).execute("");

		myTicketListView.setOnItemClickListener(new AdapterView.OnItemClickListener() 
		{
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) 
			{
				isRadioOn=true;
				selectedSubsNumber=myTicketArrayList.get(position).get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER);
				selectedServiceProvider=myTicketArrayList.get(position).get(ProjectConstant.TXN_OPERATORNAME);
				selectedDOT=myTicketArrayList.get(position).get(ProjectConstant.TXN_TRANSACTIONDATETIME);
				selectedOrderId=myTicketArrayList.get(position).get(ProjectConstant.TXN_ORDERID);
				selectedAmount=myTicketArrayList.get(position).get(ProjectConstant.TXN_PAYMENTAMOUNT);
				selectedTOP=myTicketArrayList.get(position).get(ProjectConstant.TICKET_TYPEOFPROBLEM);
				selectedComment=myTicketArrayList.get(position).get(ProjectConstant.TICKET_TICKETDESC);
				selectedQueryTypeId=myTicketArrayList.get(position).get(ProjectConstant.TXN_QUERYTYPEID);
			}
		});

	}

	private void settingIDs() 
	{
		// TODO Auto-generated method stub
		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);
		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);

		myTicketListView=(ListView)findViewById(R.id.myticketListView);

		/*headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
		headerBelow_ClassTV.setText("My Ticket");*/
//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

		headerFavBtn=(TextView)findViewById(R.id.header_FavouriteBtn);
//		headerProfileBtn=(TextView)findViewById(R.id.header_MyProfileBtn);
//		headerChangePWBtn=(TextView)findViewById(R.id.header_ChangePWBtn);
		
//		myTransitionLinearLayout=(LinearLayout)findViewById(R.id.myTransitionHeaderLayout);
//		myTransitionLinearLayout.setVisibility(View.VISIBLE);

		//SETTING THE IMAGE AND COLOR ON TEXT ON BUTTON PROGRAMMATICALLY 

		myTicketBtn=(TextView)findViewById(R.id.header_MyTicketBtn);
//		myTicketBtn.setBackground(getResources().getDrawable(R.drawable.menu_header_btn_active));
//		myTicketBtn.setBackgroundResource(R.drawable.menu_header_btn_active);
		myTicketBtn.setTextColor(getResources().getColor(R.color.darkGreen));


		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(MyTicket.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage,cartIconLayout);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		headerFavBtn.setOnClickListener(this);
//		headerProfileBtn.setOnClickListener(this);
//		headerChangePWBtn.setOnClickListener(this);

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME

		DBQueryMethods database=new DBQueryMethods(MyTicket.this);
		try
		{
			database.open();
			TextView headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
			headerBelow_ClassTV.setText(database.getSubHeaderTitles("My tickets"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}
	}

	private void showFavouriteList(ArrayList<HashMap<String, String>> arrayListMap) 
	{
		// TODO Auto-generated method stub
		myTicketAdapter = new MyTicketAdapter(MyTicket.this, arrayListMap) 
		{
			@Override
			public View getView(int position, View convertView, ViewGroup parent) 
			{
				View row = super.getView(position, convertView, parent);
				return row;
			}
		};
	}

	public void esclateOnClickMethod(View btnView)
	{
		switch(btnView.getId())
		{
		case R.id.myticket_EsclateBtn:

			if(isRadioOn)
			{
				apiHitPosition=2;
				new LoadResponseViaPost(MyTicket.this, formEscalateJSON(), true).execute("");
			}
			else
			{
				ARCustomToast.showToast(MyTicket.this,"please select the transaction to escalate", Toast.LENGTH_LONG);
			}

			break;
		case R.id.myticket_BackBtn:

			MyTicket.this.finish();

			break;
		}

	}

	@Override
	public void onClick(View v) 
	{
		// TODO Auto-generated method stub
		if(v.getId()==R.id.header_FavouriteBtn)
		{
			startActivity(new Intent(MyTicket.this,FavouriteScreen.class));
		}
		/*if(v.getId()==R.id.header_MyProfileBtn)
		{
			startActivity(new Intent(MyTicket.this,EditProfile.class));
		}
		if(v.getId()==R.id.header_ChangePWBtn)
		{
			startActivity(new Intent(MyTicket.this,ChangePassword.class));
		}*/

	}

	// Expensive operation method
	public static void setListViewHeightBasedOnChildren(ListView listView) 
	{
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		int totalHeight=0;
		View view = null;

		for (int i = 0; i < listAdapter.getCount(); i++) 
		{
			view = listAdapter.getView(i, view, listView); 
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.MATCH_PARENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	// FORMING MY TICKET JSON TO GET TICKETS
	public String formMyTicketJSON()
	{
		try
		{
			JSONObject json = new JSONObject();
			json.put("APISERVICE", "KPRAISETICKETDETAIL");
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("DEVICEOS", "ANDROID");
			json.put("SOURCENAME", "KPAPP");
			json.put("USERID",  preference.getString("KP_USER_ID", "0"));

			return json.toString();
		}catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	// FORMING MY TICKET JSON TO GET TICKETS
	public String formEscalateJSON()
	{
		try
		{
			JSONObject json = new JSONObject();
			json.put("APISERVICE", "KPESCALATE");
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("DEVICEOS", "ANDROID");
			json.put("SOURCENAME", "KPAPP");
			json.put("NAME", preference.getString("KP_USER_NAME", ""));
			json.put("CONTACTMOBILENUMBER", preference.getString("KP_USER_MOBILE_NUMBER", ""));
			json.put("EMAIL", preference.getString("KP_USER_EMAIL_ID", ""));
			json.put("COMMENT", selectedComment);
			json.put("MOBILENUMBER",selectedSubsNumber);
			json.put("QUERYTYPEID", selectedQueryTypeId);
			json.put("SERVICEPROVIDER",selectedServiceProvider );
			json.put("DATEOFTRANSACTION",selectedDOT );
			json.put("ORDERID", selectedOrderId);
			json.put("AMOUNT", selectedAmount);
			json.put("TYPEOFPROBLEM", selectedTOP);

			return json.toString();
		}catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub

		//		Log.e("", "Api response "+loadedString);

		if(loadedString!=null && !loadedString.equals(""))
		{
			if(!loadedString.equals("Exception"))
			{
				if(apiHitPosition==1)
				{
					if(parseMyTicketDetails(loadedString))
					{
						showFavouriteList(myTicketArrayList);
						myTicketListView.setAdapter(myTicketAdapter);
						setListViewHeightBasedOnChildren(myTicketListView);	
					}

				}

				if(apiHitPosition==2)
				{
					if(parseEscalateDetails(loadedString))
					{
						Intent escalteIntent = new Intent(MyTicket.this,CustomDialogBox.class);
						escalteIntent.putExtra("MSG_KEY", descriptionMsg);
						escalteIntent.putExtra("KP_SCREEN_IDENTIFIER", "MYTICKET_ESCALATE");
						startActivity(escalteIntent);
					}

				}

			}
			else
			{
				MyTicket.this.finish();
				ARCustomToast.showToast(MyTicket.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
			}
		}
		else
		{
			MyTicket.this.finish();
			ARCustomToast.showToast(MyTicket.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}
	}

	// PARSING THE MY TICKET DATA COMING FROM API

	public boolean parseMyTicketDetails(String myTransactionResponse)
	{
		JSONObject jsonObject,parentResponseObject,childResponseObject;

		try 
		{
			jsonObject=new JSONObject(myTransactionResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				ARCustomToast.showToast(MyTicket.this,"No Reponse Tag", Toast.LENGTH_LONG);
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
			{
				if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
				{
					if(parentResponseObject.has(ProjectConstant.TICKET_RAISETICKETDETAIL))
					{
						myTicketArrayList=new ArrayList<HashMap<String,String>>();
						//JSONArray favJsonArray=parentResponseObject.getJSONArray(ProjectConstant.FAV_FAVOURITES);

						Object object = parentResponseObject.get(ProjectConstant.TICKET_RAISETICKETDETAIL);

						// CHECKING WHETHER THE DATA INSIDE FAVOURITES TAG IS OBJECT OR JSON OBJECT

						if(object instanceof JSONObject)
						{
							final JSONObject jsonOBJECT = (JSONObject)object;
							myTicketJsonArray = new JSONArray();
							myTicketJsonArray.put(jsonOBJECT);
						}
						else if (object instanceof JSONArray)
						{
							myTicketJsonArray = (JSONArray)object;
						}


						for(int i=0;i<myTicketJsonArray.length();i++)
						{
							childResponseObject=myTicketJsonArray.getJSONObject(i);

							if (!childResponseObject.isNull(ProjectConstant.TICKET_TICKETSTATUS)
									&& !childResponseObject.isNull(ProjectConstant.TXN_TRANSACTIONDATETIME) 
									&& !childResponseObject.isNull(ProjectConstant.TXN_SERVICEID)
									&& !childResponseObject.isNull(ProjectConstant.TXN_OPERATORNAME)
									&& !childResponseObject.isNull(ProjectConstant.TICKET_TICKETNO)
									&& !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTAMOUNT)
									&& !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTSTATUS) 
									&& !childResponseObject.isNull(ProjectConstant.TXN_SERVICENAME)
									&& !childResponseObject.isNull(ProjectConstant.TXN_COUNTRYCODE)
									&& !childResponseObject.isNull(ProjectConstant.TXN_ORDERID)
									&& !childResponseObject.isNull(ProjectConstant.TXN_OPERATORCODE)
									&& !childResponseObject.isNull(ProjectConstant.TXN_RECHARGESTATUS) 
									&& !childResponseObject.isNull(ProjectConstant.TXN_RECHARGEAMOUNT)
									&& !childResponseObject.isNull(ProjectConstant.TICKET_QUERYTYPE)
									&& !childResponseObject.isNull(ProjectConstant.TICKET_TICKETDESC)
									&& !childResponseObject.isNull(ProjectConstant.TICKET_TYPEOFPROBLEM)
									&& !childResponseObject.isNull(ProjectConstant.TXN_SUBSCRIPTIONNUMBER)
									&& !childResponseObject.isNull(ProjectConstant.TXN_QUERYTYPEID) ) 
							{
								myTicketHashMap=new HashMap<String, String>();
								myTicketHashMap.put(ProjectConstant.TICKET_TICKETSTATUS, childResponseObject.getString(ProjectConstant.TICKET_TICKETSTATUS));
								myTicketHashMap.put(ProjectConstant.TXN_TRANSACTIONDATETIME, childResponseObject.getString(ProjectConstant.TXN_TRANSACTIONDATETIME));
								myTicketHashMap.put(ProjectConstant.TXN_SERVICEID, childResponseObject.getString(ProjectConstant.TXN_SERVICEID));
								myTicketHashMap.put(ProjectConstant.TXN_OPERATORNAME, childResponseObject.getString(ProjectConstant.TXN_OPERATORNAME));
								myTicketHashMap.put(ProjectConstant.TICKET_TICKETNO, childResponseObject.getString(ProjectConstant.TICKET_TICKETNO));
								myTicketHashMap.put(ProjectConstant.TXN_PAYMENTAMOUNT, childResponseObject.getString(ProjectConstant.TXN_PAYMENTAMOUNT));
								myTicketHashMap.put(ProjectConstant.TXN_PAYMENTSTATUS, childResponseObject.getString(ProjectConstant.TXN_PAYMENTSTATUS));
								myTicketHashMap.put(ProjectConstant.TXN_SERVICENAME, childResponseObject.getString(ProjectConstant.TXN_SERVICENAME));
								myTicketHashMap.put(ProjectConstant.TXN_COUNTRYCODE, childResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE));
								myTicketHashMap.put(ProjectConstant.TXN_ORDERID, childResponseObject.getString(ProjectConstant.TXN_ORDERID));
								myTicketHashMap.put(ProjectConstant.TXN_OPERATORCODE, childResponseObject.getString(ProjectConstant.TXN_OPERATORCODE));
								myTicketHashMap.put(ProjectConstant.TXN_RECHARGESTATUS, childResponseObject.getString(ProjectConstant.TXN_RECHARGESTATUS));
								myTicketHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, childResponseObject.getString(ProjectConstant.TXN_RECHARGEAMOUNT));
								myTicketHashMap.put(ProjectConstant.TICKET_QUERYTYPE, childResponseObject.getString(ProjectConstant.TICKET_QUERYTYPE));
								myTicketHashMap.put(ProjectConstant.TICKET_TICKETDESC, childResponseObject.getString(ProjectConstant.TICKET_TICKETDESC));
								myTicketHashMap.put(ProjectConstant.TICKET_TYPEOFPROBLEM, childResponseObject.getString(ProjectConstant.TICKET_TYPEOFPROBLEM));
								myTicketHashMap.put(ProjectConstant.TXN_SUBSCRIPTIONNUMBER, childResponseObject.getString(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
								myTicketHashMap.put(ProjectConstant.TXN_QUERYTYPEID, childResponseObject.getString(ProjectConstant.TXN_QUERYTYPEID));

								myTicketArrayList.add(myTicketHashMap);

							}
						}

					}

				}

				else
				{
					if(parentResponseObject.has("DESCRIPTION"))
					{
						ARCustomToast.showToast(MyTicket.this,parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						return false;
					}

				}

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}

	// PARSING THE ESCALATION DATA COMING FROM API

	public boolean parseEscalateDetails(String escalationResponse)
	{
		JSONObject jsonObject,parentResponseObject;

		try 
		{
			jsonObject=new JSONObject(escalationResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				ARCustomToast.showToast(MyTicket.this,"No Reponse Tag", Toast.LENGTH_LONG);
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
			{
				if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
				{

					if(parentResponseObject.has(ProjectConstant.ESCALATE_TICKETDATETIME) 
							&& parentResponseObject.has(ProjectConstant.ESCALATE_TICKETID))
					{
						//						Log.e("", "MyTicket if response code 0");

						descriptionMsg=parentResponseObject.getString(ProjectConstant.ESCALATE_TICKETID)+" "+ 
								parentResponseObject.getString(ProjectConstant.ESCALATE_TICKETDATETIME);
					}

				}

				else
				{
					if(parentResponseObject.has("DESCRIPTION"))
					{
						ARCustomToast.showToast(MyTicket.this,parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						return false;
					}
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preference!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preference.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}
		
		}
	}*/


}

