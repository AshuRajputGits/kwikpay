package com.uk.recharge.kwikpay.catering.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ashu Rajput on 11/5/2018.
 */

public class CateringCompleteOrdersMO implements Serializable {

    private String cateringOnGoingOrder;
    private String cateringReadyStatus;
    private String cateringAcceptedStatus;
    private String cateringDeliveryStatus;
    private String cateringCancelStatus;
    private String cateringTxnDateTime;
    private String cateringOrderId;
    private String cateringTotalItems;
    private String cateringServiceFee;
    private String cateringServiceName;
    private String cateringOperatorName;
    private String cateringPaymentAmount;
    private String cateringPaymentStatus;
    private String cateringServiceId;
    private String cateringOperatorCode;
    private String cateringCountryCode;
    private String cateringDiscountAmount;
    private String cateringDisplayCurrencyCode;
    private String cateringRechargeCurrencyCode;
    private String cateringPaymentCurrencyCode;
    private String cateringCpnSrNum;
    private String cateringCpnValue;
    private String cateringUniqueTxnId;
    private String cateringProcessingFee;

    private ProductDistributorDetailsMO productDistributorDetails;
    private List<ProductSelectedDetailsMO> productSelectedDetailsMOList;

    public String getCateringOnGoingOrder() {
        return cateringOnGoingOrder;
    }

    public void setCateringOnGoingOrder(String cateringOnGoingOrder) {
        this.cateringOnGoingOrder = cateringOnGoingOrder;
    }

    public String getCateringReadyStatus() {
        return cateringReadyStatus;
    }

    public void setCateringReadyStatus(String cateringReadyStatus) {
        this.cateringReadyStatus = cateringReadyStatus;
    }

    public String getCateringAcceptedStatus() {
        return cateringAcceptedStatus;
    }

    public void setCateringAcceptedStatus(String cateringAcceptedStatus) {
        this.cateringAcceptedStatus = cateringAcceptedStatus;
    }

    public String getCateringDeliveryStatus() {
        return cateringDeliveryStatus;
    }

    public void setCateringDeliveryStatus(String cateringDeliveryStatus) {
        this.cateringDeliveryStatus = cateringDeliveryStatus;
    }

    public String getCateringCancelStatus() {
        return cateringCancelStatus;
    }

    public void setCateringCancelStatus(String cateringCancelStatus) {
        this.cateringCancelStatus = cateringCancelStatus;
    }

    public String getCateringTxnDateTime() {
        return cateringTxnDateTime;
    }

    public void setCateringTxnDateTime(String cateringTxnDateTime) {
        this.cateringTxnDateTime = cateringTxnDateTime;
    }

    public String getCateringOrderId() {
        return cateringOrderId;
    }

    public void setCateringOrderId(String cateringOrderId) {
        this.cateringOrderId = cateringOrderId;
    }

    public String getCateringTotalItems() {
        return cateringTotalItems;
    }

    public void setCateringTotalItems(String cateringTotalItems) {
        this.cateringTotalItems = cateringTotalItems;
    }

    public String getCateringServiceFee() {
        return cateringServiceFee;
    }

    public void setCateringServiceFee(String cateringServiceFee) {
        this.cateringServiceFee = cateringServiceFee;
    }

    public String getCateringServiceName() {
        return cateringServiceName;
    }

    public void setCateringServiceName(String cateringServiceName) {
        this.cateringServiceName = cateringServiceName;
    }

    public String getCateringOperatorName() {
        return cateringOperatorName;
    }

    public void setCateringOperatorName(String cateringOperatorName) {
        this.cateringOperatorName = cateringOperatorName;
    }

    public String getCateringPaymentAmount() {
        return cateringPaymentAmount;
    }

    public void setCateringPaymentAmount(String cateringPaymentAmount) {
        this.cateringPaymentAmount = cateringPaymentAmount;
    }

    public String getCateringPaymentStatus() {
        return cateringPaymentStatus;
    }

    public void setCateringPaymentStatus(String cateringPaymentStatus) {
        this.cateringPaymentStatus = cateringPaymentStatus;
    }

    public String getCateringServiceId() {
        return cateringServiceId;
    }

    public void setCateringServiceId(String cateringServiceId) {
        this.cateringServiceId = cateringServiceId;
    }

    public String getCateringOperatorCode() {
        return cateringOperatorCode;
    }

    public void setCateringOperatorCode(String cateringOperatorCode) {
        this.cateringOperatorCode = cateringOperatorCode;
    }

    public String getCateringCountryCode() {
        return cateringCountryCode;
    }

    public void setCateringCountryCode(String cateringCountryCode) {
        this.cateringCountryCode = cateringCountryCode;
    }

    public String getCateringDiscountAmount() {
        return cateringDiscountAmount;
    }

    public void setCateringDiscountAmount(String cateringDiscountAmount) {
        this.cateringDiscountAmount = cateringDiscountAmount;
    }

    public String getCateringDisplayCurrencyCode() {
        return cateringDisplayCurrencyCode;
    }

    public void setCateringDisplayCurrencyCode(String cateringDisplayCurrencyCode) {
        this.cateringDisplayCurrencyCode = cateringDisplayCurrencyCode;
    }

    public String getCateringRechargeCurrencyCode() {
        return cateringRechargeCurrencyCode;
    }

    public void setCateringRechargeCurrencyCode(String cateringRechargeCurrencyCode) {
        this.cateringRechargeCurrencyCode = cateringRechargeCurrencyCode;
    }

    public String getCateringPaymentCurrencyCode() {
        return cateringPaymentCurrencyCode;
    }

    public void setCateringPaymentCurrencyCode(String cateringPaymentCurrencyCode) {
        this.cateringPaymentCurrencyCode = cateringPaymentCurrencyCode;
    }

    public String getCateringCpnSrNum() {
        return cateringCpnSrNum;
    }

    public void setCateringCpnSrNum(String cateringCpnSrNum) {
        this.cateringCpnSrNum = cateringCpnSrNum;
    }

    public String getCateringCpnValue() {
        return cateringCpnValue;
    }

    public void setCateringCpnValue(String cateringCpnValue) {
        this.cateringCpnValue = cateringCpnValue;
    }

    public String getCateringUniqueTxnId() {
        return cateringUniqueTxnId;
    }

    public void setCateringUniqueTxnId(String cateringUniqueTxnId) {
        this.cateringUniqueTxnId = cateringUniqueTxnId;
    }

    public String getCateringProcessingFee() {
        return cateringProcessingFee;
    }

    public void setCateringProcessingFee(String cateringProcessingFee) {
        this.cateringProcessingFee = cateringProcessingFee;
    }

    public ProductDistributorDetailsMO getProductDistributorDetails() {
        return productDistributorDetails;
    }

    public void setProductDistributorDetails(ProductDistributorDetailsMO productDistributorDetails) {
        this.productDistributorDetails = productDistributorDetails;
    }

    public List<ProductSelectedDetailsMO> getProductSelectedDetailsMOList() {
        return productSelectedDetailsMOList;
    }

    public void setProductSelectedDetailsMOList(List<ProductSelectedDetailsMO> productSelectedDetailsMOList) {
        this.productSelectedDetailsMOList = productSelectedDetailsMOList;
    }
}
