package com.uk.recharge.kwikpay.kwikcharge;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.adapters.AdsViewPagerAdapter;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog.LocationIdListener;
import com.uk.recharge.kwikpay.kwikcharge.models.ConnectorListMO;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.kwikcharge.models.RegistrationNumbersMO;
import com.uk.recharge.kwikpay.kwikcharge.qrscanner.BarcodeScannerActivity;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;
import com.uk.recharge.kwikpay.singleton.RegNoDetailsSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;

/**
 * Created by Ashu Rajput on 9/17/2017.
 */

public class ScanAndLocationActivity extends AppCompatActivity implements LocationIdListener, AsyncRequestListenerViaPost {

    private static final int REQUEST_SCANNER = 9;
    private static final int REQUEST_CAMERA_RESULT = 10;
    private static final int REQUEST_LOCATION_RESULT = 11;
    private AutoScrollViewPager mViewPager;
    private SharedPreferences preference;
    int apiHitPosition = 0;
    private JSONArray regNoJsonArray;
    //    private boolean showAddressMessage = false;
    private LocationIdMO apiReceivedLocationChargerDetailsModel = null;
    private double latitude = 0.0, longitude = 0.0;
    private boolean isCallingDirectlyFromHome = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_location_activity);

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(ScanAndLocationActivity.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        AdsViewPagerAdapter adsViewPagerAdapter = new AdsViewPagerAdapter(ScanAndLocationActivity.this, "KwikCharge");
        mViewPager = findViewById(R.id.viewPagerBanner);
        mViewPager.startAutoScroll(4500);
        mViewPager.setInterval(4500);
        mViewPager.setAdapter(adsViewPagerAdapter);

        findViewById(R.id.kcGeoLocationTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grantLocationPermission();
            }
        });

        findViewById(R.id.scanQRCodeTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkRTPForCamera();
            }
        });

        findViewById(R.id.enterLocationId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showAddressMessage = false;
                DialogFragment dialog = new CustomDialog();
                dialog.show(getSupportFragmentManager(), "LocationIdDialog");
                dialog.setCancelable(false);
            }
        });

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("HOME_SCANNED_QR")) {
                String scannedQRLocationID = getIntent().getExtras().getString("HOME_SCANNED_QR", "");
                if (scannedQRLocationID != null && !scannedQRLocationID.equals("")) {
                    apiHitPosition = 1;
                    isCallingDirectlyFromHome = true;
                    new LoadResponseViaPost(ScanAndLocationActivity.this, formLocationIdJson(scannedQRLocationID), true).execute("");
                }
            }
        }
    }

    private void checkRTPForCamera() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                    startQRScanActivity();
                else {
                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA))
                        Toast.makeText(this, "No Permission to use the Camera services", Toast.LENGTH_SHORT).show();
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, REQUEST_CAMERA_RESULT);
                }
            } else
                startQRScanActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void grantLocationPermission() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (ContextCompat.checkSelfPermission(ScanAndLocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(ScanAndLocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION))
                        Toast.makeText(this, "Please accept location permission, to use map service", Toast.LENGTH_SHORT).show();

                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_RESULT);
                } else
                    startMapScreen();
            } else
                startMapScreen();
        } catch (Exception e) {
        }
    }

    private void startQRScanActivity() {
        startActivityForResult(new Intent(ScanAndLocationActivity.this, BarcodeScannerActivity.class), REQUEST_SCANNER);
    }

    private void startMapScreen() {

        if (isGPSEnabled(this)) {
            startLocationUpdates();
            startActivity(new Intent(this, KCMapsActivity.class)
                    .putExtra("LATITUDE", latitude)
                    .putExtra("LONGITUDE", longitude));
        } else
            gpsDialogBox();
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationParams params = LocationParams.NAVIGATION;
            SmartLocation.with(this).location()
                    .config(params)
                    .start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Cannot run application because camera service permission have not been granted", Toast.LENGTH_SHORT).show();
                } else {
                    startQRScanActivity();
                }
                break;

            case REQUEST_LOCATION_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Cannot run application because location permission have not been granted", Toast.LENGTH_SHORT).show();
                else
                    startMapScreen();
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    public void getLocationId(String locationID) {
        if (locationID != null && !locationID.isEmpty()) {
            apiHitPosition = 1;
            new LoadResponseViaPost(ScanAndLocationActivity.this, formLocationIdJson(locationID), true).execute("");
        } else {
            if (apiReceivedLocationChargerDetailsModel != null) {
                if (preference.getBoolean("IS_KC_TNC_CHECKED", false)) {
                    if (preference.getString("KP_USER_ID", "").equals("0"))
                        startActivity(new Intent(ScanAndLocationActivity.this, ElectricBlueAcNoScreen.class));
                    else {
                        apiHitPosition = 2;
                        KwikChargeDetailsSingleton.getInstance().setLocationIdMO(apiReceivedLocationChargerDetailsModel);
                        new LoadResponseViaPost(this, formRegNumberJSON(), true).execute("");
                    }
                } else {
                    KwikChargeDetailsSingleton.getInstance().setLocationIdMO(apiReceivedLocationChargerDetailsModel);
                    startActivity(new Intent(ScanAndLocationActivity.this, KCCompanyDetails.class));
                    addressBuilder = null;
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_SCANNER) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String locationNumber = data.getStringExtra("QR_RESULT");
                    if (locationNumber != null && locationNumber.contains("-")) {
                        apiHitPosition = 1;
                        new LoadResponseViaPost(ScanAndLocationActivity.this,
                                formLocationIdJson(locationNumber.split("-")[1]), true).execute("");
                    }
                    else
                        ARCustomToast.showToast(this, getString(R.string.qr_error_msg));
                }
            }
        }
    }

    //FORM LOCATION ID JSON PARAMETERS
    public String formLocationIdJson(String locationNumber) {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KP_OPR_BY_LOCATION_NUM");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("DEVICEOS", "ANDROID");
            json.put("LOCATION_NUMBER", locationNumber);
            json.put("USERID", preference.getString("KP_USER_ID", "0"));

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //CREATING JSON PARAMETERS FOR REGISTERED Location NUMBER
    public String formRegNumberJSON() {
        JSONObject jsonobj = new JSONObject();
        try {
            String receivedLocationNumber = "";
            try {
                receivedLocationNumber = KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getLocationNumber();
            } catch (Exception e) {
            }
            jsonobj.put("APISERVICE", "KP_REG_NUM_BY_USERID");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("OP_CODE", "ELEC");
            jsonobj.put("SERVICEID", "4");
            jsonobj.put("LOCATION_NUMBER", receivedLocationNumber);
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void onRequestComplete(String loadedString) {
//        Log.e("Response", "My Response afterRequest" + loadedString);

        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 1) {
                    boolean parseResult = parseLocationIdResponse(loadedString);
                    if (!parseResult && isCallingDirectlyFromHome)
                        finish();
                } else if (apiHitPosition == 2) {
                    boolean parseResult = parseRegisteredNumberJson(loadedString);
                    if (!parseResult && isCallingDirectlyFromHome)
                        finish();
                }
            } else {
                if (!isCallingDirectlyFromHome) {
                    if (DeviceNetConnectionDetector.checkDataConnWifiMobile(ScanAndLocationActivity.this))
                        ARCustomToast.showToast(ScanAndLocationActivity.this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
                    else
                        ARCustomToast.showToast(ScanAndLocationActivity.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
                } else
                    finish();
            }

        } else {
            if (DeviceNetConnectionDetector.checkDataConnWifiMobile(ScanAndLocationActivity.this))
                ARCustomToast.showToast(ScanAndLocationActivity.this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
            else
                ARCustomToast.showToast(ScanAndLocationActivity.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    // LOGIC FOR PARSING LOCATION ID RESPONSE FROM SERVER
    public boolean parseLocationIdResponse(String response) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(response);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(ScanAndLocationActivity.this, "No Response Tag", Toast.LENGTH_LONG);
                return false;
            }
            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);
            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    LocationIdMO model = new LocationIdMO();
                    model.setUserName(parentResponseObject.optString("USERNAME"));
                    model.setOperatorName(parentResponseObject.optString("OP_NAME"));
                    model.setOperatorLogo(parentResponseObject.optString("OP_LOGO"));
                    model.setOperatorCode(parentResponseObject.optString("OP_CODE"));
                    model.setLocationNumber(parentResponseObject.optString("LOCATION_NUMBER"));
                    model.setProviderTnCUrl(parentResponseObject.optString("PROVIDER_TERM"));
                    model.setAddressOne(parentResponseObject.optString("ADDRESS_ONE"));
                    model.setAddressTwo(parentResponseObject.optString("ADDRESS_TWO"));
                    model.setAddressThree(parentResponseObject.optString("ADDRESS_THREE"));
                    model.setCity(parentResponseObject.optString("CITY"));
                    model.setCountry(parentResponseObject.optString("COUNTRY"));
                    model.setPostCode(parentResponseObject.optString("POSTCODE"));
                    model.setLatitude(parentResponseObject.optString("LATITUDE"));
                    model.setLongitude(parentResponseObject.optString("LONGITUDE"));
                    model.setConnector1Desc(parentResponseObject.optString("CONNECTOR_1_DESC"));
                    model.setConnector2Desc(parentResponseObject.optString("CONNECTOR_2_DESC"));
                    model.setConnector3Desc(parentResponseObject.optString("CONNECTOR_3_DESC"));
                    model.setCompanyDescription(parentResponseObject.optString("DESCRIPTION"));

                    model.setInterimScreenFlow(parentResponseObject.optString("INTERIM_SCREEN_FLOW"));
                    model.setInterimScreenText(parentResponseObject.optString("INTERIM_SCREEN_TEXT"));

                    /*model.setInterimScreenFlow("1");
                    model.setInterimScreenText("Please select the connector | Connect one end of the cable into your vehicle, then plug the other end into the charger | Press START button against the selected connector");*/

                    if (parentResponseObject.has("CONNECTOR_LIST")) {
                        ArrayList<ConnectorListMO> connectorListMOArrayList = null;
                        JSONArray connectorListArray = parentResponseObject.getJSONArray("CONNECTOR_LIST");
                        if (connectorListArray != null && connectorListArray.length() > 0) {
                            connectorListMOArrayList = new ArrayList<>();

                            for (byte b = 0; b < connectorListArray.length(); b++) {
                                JSONObject connectorJsonObject = connectorListArray.getJSONObject(b);

                                ConnectorListMO connectorListMO = new ConnectorListMO();
                                if (connectorJsonObject.has("ConnectorType"))
                                    connectorListMO.setConnectorType(connectorJsonObject.getString("ConnectorType"));
                                if (connectorJsonObject.has("LocalConnectorNumber"))
                                    connectorListMO.setLocalConnectorNumber(connectorJsonObject.getString("LocalConnectorNumber"));
                                if (connectorJsonObject.has("ConnectorStatus"))
                                    connectorListMO.setConnectorStatus(connectorJsonObject.getString("ConnectorStatus"));
                                if (connectorJsonObject.has("Connector_Display_Name"))
                                    connectorListMO.setConnectorName(connectorJsonObject.getString("Connector_Display_Name"));
                                if (connectorJsonObject.has("ConnectorLogo"))
                                    connectorListMO.setConnectorLogoURL(connectorJsonObject.getString("ConnectorLogo"));
                                if (connectorJsonObject.has("Connector_Description"))
                                    connectorListMO.setConnectorDescription(connectorJsonObject.getString("Connector_Description"));

                                connectorListMOArrayList.add(connectorListMO);
                            }
                            model.setConnectorListMOList(connectorListMOArrayList);
                        }
                    }
                    confirmAddressFromUserDialog(model);
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(ScanAndLocationActivity.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean parseRegisteredNumberJson(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    LocationIdMO receivedLocationDetailsMO = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();

                    receivedLocationDetailsMO.setOperatorCode(childJsonObj.optString("OP_CODE"));
                    receivedLocationDetailsMO.setServiceId(childJsonObj.optString("SERVICEID"));
                    receivedLocationDetailsMO.setAC(childJsonObj.optString("AC"));
                    receivedLocationDetailsMO.setDC(childJsonObj.optString("DC"));
                    receivedLocationDetailsMO.setCHADEMO(childJsonObj.optString("CHADEMO"));

                    receivedLocationDetailsMO.setConnector1Desc(childJsonObj.optString("CONNECTOR_1_DESC"));
                    receivedLocationDetailsMO.setConnector2Desc(childJsonObj.optString("CONNECTOR_2_DESC"));
                    receivedLocationDetailsMO.setConnector3Desc(childJsonObj.optString("CONNECTOR_3_DESC"));

                    receivedLocationDetailsMO.setAddressOne(childJsonObj.optString("ADDRESS_ONE"));
                    receivedLocationDetailsMO.setAddressTwo(childJsonObj.optString("ADDRESS_TWO"));
                    receivedLocationDetailsMO.setAddressThree(childJsonObj.optString("ADDRESS_THREE"));
                    receivedLocationDetailsMO.setCountry(childJsonObj.optString("COUNTRY"));
                    receivedLocationDetailsMO.setCity(childJsonObj.optString("CITY"));
                    receivedLocationDetailsMO.setPostCode(childJsonObj.optString("POSTCODE"));
                    receivedLocationDetailsMO.setUserPostCode(childJsonObj.optString("USER_POSTCODE"));
                    receivedLocationDetailsMO.setRegistrationNumber(childJsonObj.optString("REGISTRATION_NUMBER"));
                    receivedLocationDetailsMO.setRecentMake(childJsonObj.optString("RECENT_MAKE"));
                    receivedLocationDetailsMO.setRecentModel(childJsonObj.optString("RECENT_MODEL"));
                    receivedLocationDetailsMO.setRecentYear(childJsonObj.optString("RECENT_YEAR"));
                    receivedLocationDetailsMO.setRecentBatteryType(childJsonObj.optString("RECENT_BATTERY_TYPE"));
                    receivedLocationDetailsMO.setRecentChargerType(childJsonObj.optString("RECENT_CHARGER_TYPE"));
                    receivedLocationDetailsMO.setRecentMaxAmount(childJsonObj.optString("RECENT_MAX_AMOUNT"));
                    receivedLocationDetailsMO.setRecentMaxTime(childJsonObj.optString("RECENT_MAX_TIME"));
                    receivedLocationDetailsMO.setRecentRate(childJsonObj.optString("RECENT_RATE"));

                    KwikChargeDetailsSingleton.getInstance().setLocationIdMO(receivedLocationDetailsMO);

                    if (childJsonObj.has("REG_NUMS")) {
                        Object object = childJsonObj.get("REG_NUMS");

                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            regNoJsonArray = new JSONArray();
                            regNoJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray) {
                            regNoJsonArray = (JSONArray) object;
                        }

                        if (regNoJsonArray != null && regNoJsonArray.length() > 0) {

                            ArrayList<RegistrationNumbersMO> regNoArrayList = new ArrayList<>(regNoJsonArray.length());

                            for (byte b = 0; b < regNoJsonArray.length(); b++) {

                                RegistrationNumbersMO regNoModel = new RegistrationNumbersMO();
                                regNoModel.setRegistrationNo(regNoJsonArray.getJSONObject(b).optString("REG_NUM"));
                                regNoModel.setMake(regNoJsonArray.getJSONObject(b).optString("MAKE"));
                                regNoModel.setModel(regNoJsonArray.getJSONObject(b).optString("MODEL"));
                                regNoModel.setChargerType(regNoJsonArray.getJSONObject(b).optString("CHARGER_TYPE"));
                                regNoModel.setBatteryType(regNoJsonArray.getJSONObject(b).optString("BATTERY_TYPE"));
                                regNoModel.setYear(regNoJsonArray.getJSONObject(b).optString("YEAR"));
                                regNoModel.setMaxAmount(regNoJsonArray.getJSONObject(b).optString("MAX_AMOUNT"));
                                regNoModel.setMaxTime(regNoJsonArray.getJSONObject(b).optString("MAX_TIME"));
                                regNoModel.setMaxRate(regNoJsonArray.getJSONObject(b).optString("RATE"));

                                regNoArrayList.add(regNoModel);
                            }
                            RegNoDetailsSingleton.getInstance().setRegNoList(regNoArrayList);
                        }
                    }
                    startActivity(new Intent(this, KwikChargeOrderScreen.class));
                    if (isCallingDirectlyFromHome)
                        finish();

                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        startActivity(new Intent(this, VehicleDetailScreen.class));
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            KwikChargeDetailsSingleton.getInstance().clearLocationDetails();
        } catch (Exception e) {
        }
    }

    StringBuilder addressBuilder;

    private void confirmAddressFromUserDialog(final LocationIdMO model) {

        addressBuilder = new StringBuilder();
        addressBuilder.append("Are you at ");
        addressBuilder.append("\"");
        addressBuilder.append(model.getAddressOne());
        addressBuilder.append(", ");
        addressBuilder.append(model.getAddressTwo());
        addressBuilder.append(" ");
        addressBuilder.append(model.getAddressThree());
        addressBuilder.append("\"?");

        DialogFragment dialog = new CustomDialog();
        dialog.show(getSupportFragmentManager(), "LocationIdDialog");
        Bundle bundle = new Bundle();
        bundle.putString("Charger_Address", addressBuilder.toString());
        bundle.putBoolean("ShowAddressDialog", true);
        dialog.setArguments(bundle);
        dialog.setCancelable(false);
        apiReceivedLocationChargerDetailsModel = model;
    }

    public static boolean isGPSEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    private void gpsDialogBox() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("Please enable your GPS from setting");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    dialog.dismiss();
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(ScanAndLocationActivity.this, "Setting activity not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.show();
    }
}
