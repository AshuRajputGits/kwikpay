package com.uk.recharge.kwikpay.kwikcharge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.adapter.RegistrationNumberAdapter;
import com.uk.recharge.kwikpay.kwikcharge.adapter.RegistrationNumberAdapter.ChangeRegNumberListener;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.kwikcharge.models.RegistrationNumbersMO;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;
import com.uk.recharge.kwikpay.singleton.RegNoDetailsSingleton;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 10/8/2017.
 */

public class KCRegistrationNumberDetails extends AppCompatActivity {

//    private ArrayList<RegistrationNumbersMO> regNoModelList=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_reg_no_details);

        findViewById(R.id.kcChargeDifferentCardButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KCRegistrationNumberDetails.this, VehicleDetailScreen.class));
            }
        });

        setUpRegNoRecyclerView();
    }

    private void setUpRegNoRecyclerView() {

        RecyclerView registrationNoDetailsRV = (RecyclerView) findViewById(R.id.registrationNoDetailsRV);
        final ArrayList<RegistrationNumbersMO> regNoModelList = RegNoDetailsSingleton.getInstance().getRegNoList();
        if (regNoModelList != null && regNoModelList.size() > 0) {
            RegistrationNumberAdapter adapter = new RegistrationNumberAdapter(this, regNoModelList, new ChangeRegNumberListener() {
                @Override
                public void onRegisterNumberSelect(int position) {
                    sendDataBackToOrderScreen(regNoModelList.get(position));
                }
            });
            registrationNoDetailsRV.setAdapter(adapter);
            registrationNoDetailsRV.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    private void sendDataBackToOrderScreen(RegistrationNumbersMO regNoModelList) {

        LocationIdMO receivedModel = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();
        if (receivedModel != null && regNoModelList != null) {
            receivedModel.setRegistrationNumber(regNoModelList.getRegistrationNo());
            receivedModel.setRecentModel(regNoModelList.getModel());
            receivedModel.setRecentChargerType(regNoModelList.getChargerType());
            receivedModel.setRecentBatteryType(regNoModelList.getBatteryType());
            receivedModel.setRecentYear(regNoModelList.getYear());
            receivedModel.setRecentMaxAmount(regNoModelList.getMaxAmount());
            receivedModel.setRecentMaxTime(regNoModelList.getMaxTime());
            receivedModel.setRecentRate(regNoModelList.getMaxRate());

            KwikChargeDetailsSingleton.getInstance().setLocationIdMO(receivedModel);
            setResult(Activity.RESULT_OK, new Intent());
            finish();
        }
    }
}
