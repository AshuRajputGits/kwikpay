package com.uk.recharge.kwikpay.gamingarcade;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.gamingarcade.models.PricingOptionsMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 7/6/2018.
 */

public class GamingProductPricingAdapter extends RecyclerView.Adapter<GamingProductPricingAdapter.GamingPricingViewHolder> {

    private ArrayList<PricingOptionsMO> pricingOptionsMOArrayList = null;
    private Context context;
    private LayoutInflater layoutInflater = null;
    private GamingPricingItemListener listener;

    public GamingProductPricingAdapter(Context context, ArrayList<PricingOptionsMO> pricingOptionsMOArrayList) {
        this.context = context;
        this.pricingOptionsMOArrayList = pricingOptionsMOArrayList;
        this.layoutInflater = LayoutInflater.from(context);
        listener = (GamingPricingItemListener) context;
    }

    @NonNull
    @Override
    public GamingPricingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.game_service_row, parent, false);
        return new GamingPricingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GamingPricingViewHolder holder, int position) {
        try {
            Glide.with(context)
                    .load(R.drawable.default_operator_logo)
                    .into(holder.gameServiceLogo);
//            .placeholder(R.drawable.default_operator_logo)

        } catch (Exception e) {
            e.printStackTrace();
        }

        String planAmountWithCurrencySymbol = pricingOptionsMOArrayList.get(position).getPlanDesc() + "\n"
                + Utility.fromHtml("\u00a3") + pricingOptionsMOArrayList.get(position).getPlanAmount();

        holder.gameServiceDescription.setText(planAmountWithCurrencySymbol);
        holder.gameServiceDescription.setTextColor(context.getResources().getColor(R.color.lightGreen));
    }

    @Override
    public int getItemCount() {
        return pricingOptionsMOArrayList.size();
    }

    public class GamingPricingViewHolder extends RecyclerView.ViewHolder {

        private ImageView gameServiceLogo;
        private TextView gameServiceDescription;

        public GamingPricingViewHolder(View itemView) {
            super(itemView);
            gameServiceLogo = itemView.findViewById(R.id.gameServiceLogo);
            gameServiceDescription = itemView.findViewById(R.id.gameServiceDescription);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRowItemSelection(getLayoutPosition(), 1);
                }
            });
        }
    }

    public interface GamingPricingItemListener {
        void onRowItemSelection(int position, int type);
    }

}
