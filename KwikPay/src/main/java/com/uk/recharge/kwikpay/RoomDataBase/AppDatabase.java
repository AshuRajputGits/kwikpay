package com.uk.recharge.kwikpay.RoomDataBase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.uk.recharge.kwikpay.RoomDataBase.DAO.IngredientDao;
import com.uk.recharge.kwikpay.RoomDataBase.DAO.IngredientTypeDao;
import com.uk.recharge.kwikpay.RoomDataBase.DAO.ProductDao;
import com.uk.recharge.kwikpay.RoomDataBase.DAO.ProductTypeDao;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Product;
import com.uk.recharge.kwikpay.RoomDataBase.Models.ProductType;


@Database(entities = {Product.class, ProductType.class, Ingredient.class, IngredientType.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ProductDao productDao();

    public abstract ProductTypeDao productTypeDao();

    public abstract IngredientDao ingredientDao();

    public abstract IngredientTypeDao ingredientTypeDao();


}
