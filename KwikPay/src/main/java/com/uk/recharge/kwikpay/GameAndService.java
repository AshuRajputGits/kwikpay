package com.uk.recharge.kwikpay;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.urbanairship.UAirship;

public class GameAndService extends Activity implements AsyncRequestListenerViaPost
{
	private LinearLayout sixGreenBtnLayout,secondHalfSixBtnLayout;
	private Spinner chooseServiceSpinner;
	private String[] chooseServiceArray;
	private ArrayAdapter<String> chooseServiceAdapter;
	private Button greenButton1,greenButton2,greenButton3,greenButton4,greenButton5,greenButton6;
	private Bitmap operatorImageBitmap;
	private SharedPreferences preference;
	private int apiHitPosition=0;
	private String noOfProducts="",selectedOperatorFromSpinner="",selectedOperatorIdFromSpinner="",cartDeleteMsg="";
	private ArrayList<HashMap<String,String>> serviceArrayList,topUpPlansArrayList;
	private HashMap<String,String> serviceHashMap,topUpHashMap,payNowHashMap;
	private TextView greenTextView1,greenTextView2,greenTextView3,greenTextView4,greenTextView5,greenTextView6;
	private LinearLayout greenBtnLayout1,greenBtnLayout2,greenBtnLayout3,greenBtnLayout4,greenBtnLayout5,greenBtnLayout6,gameServiceProductLayout;
	private ImageView chooseProductOnFlyIV;
//	private ImageView verifiedPaymentBanner;
	private boolean controlNetOffExit=false;
    private TextView productDescriptionLink;
    private String viewProductDetailURL;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.gaming_service_screen);
		settingIdsNew();
		apiHitPosition=1;

		new LoadResponseViaPost(GameAndService.this, formDigitalService(), true).execute("");

		chooseServiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) 
			{
				// TODO Auto-generated method stub
				hideChooseTopUpLayout();
				hideSelectedTopUP();
				setDefaultValues();
				
				if(position>0)
				{
					noOfProducts="";
					position--;
					
					controlNetOffExit=true;
					
					selectedOperatorFromSpinner=serviceArrayList.get(position).get(ProjectConstant.API_OPCODE);
					selectedOperatorIdFromSpinner=serviceArrayList.get(position).get(ProjectConstant.API_OPID);
					
					// CHECKING OPERATOR ACTIVE FLAG
					if(serviceArrayList.get(position).get(ProjectConstant.API_OPACTIVEFLAG).equals("0")
						||serviceArrayList.get(position).get(ProjectConstant.API_OPCIRCLEACTIVEFLAG).equals("0")
						  ||serviceArrayList.get(position).get(ProjectConstant.API_OPTEMPACTIVEFLAG).equals("0"))
					{
						ARCustomToast.showToast(GameAndService.this,"this operator is temporarily unavailable. please try after sometime.", Toast.LENGTH_LONG);
					}
					else
					{
						apiHitPosition=2;
						new LoadResponseViaPost(GameAndService.this, formTopUpProductJson() , true).execute("");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		// COMMENTING AS PER NEW REQUIREMENT 
		/*topupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) 
			{
				// TODO Auto-generated method stub
				hideSelectedTopUP();
				if(position>0)
				{
					position--;
					String selectedTopUpAbove=Html.fromHtml(topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
							topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYAMT);
					String selectedTopUpBelow=Html.fromHtml(topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
							topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_PAYMENTAMT);

					showSelectedTopUP(selectedTopUpAbove,selectedTopUpBelow,position);
					createHashMapForOrderDetail(position);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});*/

		//SETTING SCREEN NAMES TO APPS FLYER
		try{
			EventsLoggerSingleton.getInstance().setCommonEventLogs(this,getResources().getString(R.string.SCREEN_NAME_GAMES_AND_SERVICES));
			/*String eventName=getResources().getString(R.string.SCREEN_NAME_GAMES_AND_SERVICES);
			AppsFlyerLib.trackEvent(this, eventName, null);
			EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
			((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/

		}catch (Exception e){e.printStackTrace();}

        productDescriptionLink.setOnClickListener(viewClickListener);

		try {
			if (preference.getString("URBAN_AIRSHIP_CHANNEL_ID", "").equals("")) {
				preference.edit().putString("URBAN_AIRSHIP_CHANNEL_ID", UAirship.shared().getPushManager().getChannelId()).apply();
			}
		} catch (Exception e) {
		}

	}

    private View.OnClickListener viewClickListener= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(GameAndService.this,CommonStaticPages.class).putExtra("STATIC_PAGE_NAME","PDT_DESCRIPTION_PAGE").putExtra("VIEW_PDT_URL",viewProductDetailURL));
        }
    };

	private void settingIdsNew() 
	{
		// TODO Auto-generated method stub
		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);
		chooseServiceSpinner=(Spinner)findViewById(R.id.gameservice_ChooseServiceSpinner);
		gameServiceProductLayout=(LinearLayout)findViewById(R.id.gameservice_ChooseTopUPLabelLayout);
		chooseProductOnFlyIV=(ImageView)findViewById(R.id.gameservice_onFlyOperatorImage);
		/*
        verifiedPaymentBanner=(ImageView)findViewById(R.id.gameservice_VerifiedImage);
		selectedTopUpLayout=(LinearLayout)findViewById(R.id.gameservice_ChoosedTopupLayout);
		topupSpinner=(Spinner)findViewById(R.id.gameservice_SelectTopUpSpinner);
		selectedOperatorOnFly=(ImageView)findViewById(R.id.gameservice_onFlySelectedOprImage);
		paynowButton=(Button)findViewById(R.id.gameServiceProceedButton);

		// SETTING IDs FOR CHOOSED TOPUP EITHER FROM 6 BUTTONS OR DROP DOWN
		choosedAboveTV=(TextView)findViewById(R.id.gameservice_SelectedAboveValue0);
		choosedBelowTV=(TextView)findViewById(R.id.gameservice_SelectedBelowValue0);*/

		sixGreenBtnLayout=(LinearLayout)findViewById(R.id.sixbuttonLayout);
		secondHalfSixBtnLayout=(LinearLayout)findViewById(R.id.customTopUpSecondLayout);
        productDescriptionLink=(TextView)findViewById(R.id.gameservice_ProductDescriptionLink);

		//SETTING IDs FOR SIX GREEN BUTTONS AND LINEAR LAYOUT

		greenBtnLayout1=(LinearLayout)findViewById(R.id.greenBox1);
		greenBtnLayout2=(LinearLayout)findViewById(R.id.greenBox2);
		greenBtnLayout3=(LinearLayout)findViewById(R.id.greenBox3);
		greenBtnLayout4=(LinearLayout)findViewById(R.id.greenBox4);
		greenBtnLayout5=(LinearLayout)findViewById(R.id.greenBox5);
		greenBtnLayout6=(LinearLayout)findViewById(R.id.greenBox6);

		// SETTING THE IDs FOR SIX GREEN BUTTONS

		greenButton1=(Button)findViewById(R.id.topupAboveValue1);
		greenButton2=(Button)findViewById(R.id.topupAboveValue2);
		greenButton3=(Button)findViewById(R.id.topupAboveValue3);
		greenButton4=(Button)findViewById(R.id.topupAboveValue4);
		greenButton5=(Button)findViewById(R.id.topupAboveValue5);
		greenButton6=(Button)findViewById(R.id.topupAboveValue6);

		// SETTING THE IDs FOR SIX GREEN TEXT VIEWS
		greenTextView1=(TextView)findViewById(R.id.topupBelowValue1);
		greenTextView2=(TextView)findViewById(R.id.topupBelowValue2);
		greenTextView3=(TextView)findViewById(R.id.topupBelowValue3);
		greenTextView4=(TextView)findViewById(R.id.topupBelowValue4);
		greenTextView5=(TextView)findViewById(R.id.topupBelowValue5);
		greenTextView6=(TextView)findViewById(R.id.topupBelowValue6);

		//SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
		CustomSlidingDrawer csd=new CustomSlidingDrawer(GameAndService.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();
	}

	public void gameServicePayNow(View payNowView)
	{	
		// COMMENTING AS PER NEW REQUIREMENT 
		/*if(payNowView.getId()==R.id.gameServiceProceedButton)
		{
			controlNetOffExit=true;
			if(preference.getString("KP_USER_ID", "").equals("0"))
			{
				Intent logInIntent=new Intent(GameAndService.this,LoginScreen.class);
				logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
				startActivity(logInIntent);
			}
			else
			{
				// Create the Values
				Map<String,Object> event = new HashMap<String,Object>();
				event.put(AFInAppEventParameterName.PRICE,9.99);
				event.put(AFInAppEventParameterName.CONTENT_TYPE,"GameAndServices");		        
				event.put(AFInAppEventParameterName.CURRENCY,"GBP");
				event.put(AFInAppEventParameterName.QUANTITY,1);

				AppsFlyerLib.trackEvent(GameAndService.this, AFInAppEventType.ADD_PAYMENT_INFO,event);

				Intent payNowIntent=new Intent(GameAndService.this,OrderDetails.class);
				payNowIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
				startActivity(payNowIntent);
			}
		}*/

	}

	//HIDING AND SHOWING CHOOSE YOUR TOP UP LAYOUT VIEWS
	public void showChooseTopUpLayout(String noOfProduct)
	{
		gameServiceProductLayout.setVisibility(View.VISIBLE);
		sixGreenBtnLayout.setVisibility(View.VISIBLE);

        // ENABLING OR SHOWING "PRODUCT DESCRIPTION LINK"
        productDescriptionLink.setVisibility(View.VISIBLE);
		
		/*
		if(Integer.parseInt(noOfProduct)>6)
		{
			sixGreenBtnLayout.setVisibility(View.GONE);
			// COMMENTING AS PER NEW REQUIREMENT 
//			topupSpinner.setVisibility(View.VISIBLE);
		}
		else
		{
			sixGreenBtnLayout.setVisibility(View.VISIBLE);
			// COMMENTING AS PER NEW REQUIREMENT 
//			topupSpinner.setVisibility(View.GONE);
		}
*/
	}

	public void hideChooseTopUpLayout()
	{
		gameServiceProductLayout.setVisibility(View.GONE);
		sixGreenBtnLayout.setVisibility(View.GONE);
        productDescriptionLink.setVisibility(View.GONE);
		// COMMENTING AS PER NEW REQUIREMENT 
//		topupSpinner.setVisibility(View.GONE);
	}

	//	HIDING AND SHOWING CHOOSED OR SELECTED TOP UP LAYOUT VIEWS

	// COMMENTING AS PER NEW REQUIREMENT 
	/*
	 public void showSelectedTopUP(String abovePrice,String belowPrice,int productPos)
	{
		selectedTopUpLayout.setVisibility(View.VISIBLE);
		
		//SELECTED ABOVE PRODUCT VALUE
		if(abovePrice.length()>7)
		{
			choosedAboveTV.setText(Html.fromHtml(topUpPlansArrayList.get(productPos).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH))+"\n"+
					topUpPlansArrayList.get(productPos).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
		}
		else
		{
			choosedAboveTV.setText(abovePrice);
		}

		//SELECTED BELOW PRODUCT VALUE
		if(belowPrice.length()>7)
		{
			choosedBelowTV.setText(Html.fromHtml(topUpPlansArrayList.get(productPos).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +"\n"+
					topUpPlansArrayList.get(productPos).get(ProjectConstant.TOPUP_API_PAYMENTAMT));
		}
		else
		{
			choosedBelowTV.setText(belowPrice);
		}

		if(operatorImageBitmap!=null)
			selectedOperatorOnFly.setImageBitmap(operatorImageBitmap);
		else
			selectedOperatorOnFly.setImageDrawable(getResources().getDrawable(R.drawable.default_operator));


		paynowButton.setVisibility(View.VISIBLE);
		verifiedPaymentBanner.setVisibility(View.VISIBLE);
		paynowButton.setFocusable(true);
		paynowButton.setFocusableInTouchMode(true);
		paynowButton.requestFocus();
	}*/

	public void hideSelectedTopUP()
	{
		// COMMENTING AS PER NEW REQUIREMENT
//        verifiedPaymentBanner.setVisibility(View.GONE);
//		selectedTopUpLayout.setVisibility(View.GONE);
//		paynowButton.setVisibility(View.GONE);
	}

	public void hideAllLayouts()
	{
		gameServiceProductLayout.setVisibility(View.GONE);
		sixGreenBtnLayout.setVisibility(View.GONE);
        productDescriptionLink.setVisibility(View.GONE);

        // COMMENTING AS PER NEW REQUIREMENT
//        verifiedPaymentBanner.setVisibility(View.GONE);
//		topupSpinner.setVisibility(View.GONE);
//		selectedTopUpLayout.setVisibility(View.GONE);
//		paynowButton.setVisibility(View.GONE);
		
	}

	// SETTING DEFAULT VALUES OF SIX GREEN BUTTONS
	public void setDefaultValues()
	{
		greenBtnLayout1.setVisibility(View.VISIBLE);
		greenBtnLayout2.setVisibility(View.VISIBLE);
		greenBtnLayout3.setVisibility(View.VISIBLE);
		greenBtnLayout4.setVisibility(View.VISIBLE);
		greenBtnLayout5.setVisibility(View.VISIBLE);
		greenBtnLayout6.setVisibility(View.VISIBLE);
		
		greenButton1.setBackgroundResource(R.drawable.topup_button);
		greenButton1.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton2.setBackgroundResource(R.drawable.topup_button);
		greenButton2.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton3.setBackgroundResource(R.drawable.topup_button);
		greenButton3.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton4.setBackgroundResource(R.drawable.topup_button);
		greenButton4.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton5.setBackgroundResource(R.drawable.topup_button);
		greenButton5.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton6.setBackgroundResource(R.drawable.topup_button);
		greenButton6.setTextColor(getResources().getColor(R.color.whiteColor));
	}
	
	//FORM DIGITAL SERVICE JSON PARAMETERS
	public String formDigitalService()
	{
		try
		{
			JSONObject json = new JSONObject();
			json.put("APISERVICE", "KPDIGITALSERVICES");
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("DEVICEOS", "ANDROID");
			json.put("SOURCENAME", "KPAPP");
			json.put("USERID",  preference.getString("KP_USER_ID", "0"));
			json.put("SERVICEID", "3");
			json.put("PUSH_TOKEN", preference.getString("URBAN_AIRSHIP_CHANNEL_ID", ""));
			json.put("APPNAME", "KWIKPAY");

			return json.toString();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	//FORM PLAN PRODUCT JSON PARAMETERS
	public String formTopUpProductJson()
	{
		try
		{
			JSONObject json = new JSONObject();
			json.put("APISERVICE", "KPDIGITALSERVICESPRODUCTS");
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("SOURCENAME", "KPAPP");
			json.put("DEVICEOS", "ANDROID");
			json.put("USERID", preference.getString("KP_USER_ID", "0"));
			json.put("OPERATORCODE", selectedOperatorFromSpinner);
			json.put("SERVICEID", "3");

			return json.toString();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	//FORM PAY NOW JSON PARAMETERS
/*	public String formPayNowJson()
	{
		try
		{
			JSONObject json = new JSONObject();
			json.put("APISERVICE", "KPGETRECHARGEAMOUNT");
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("SOURCENAME", "KPAPP");
			json.put("DEVICEOS", "ANDROID");
			json.put("OPERATORCODE", selectedOperatorFromSpinner);
			json.put("COUNTRYCODE", "UNITEDKINGDOM");
			json.put("DISPLAYAMOUNT", payNowDisplayAmount);
			json.put("PAYMENTAMOUNT", payNowPaymentAmount);
			json.put("SERVICEID", "3");

			return json.toString();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}*/

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub
		if(loadedString!=null && !loadedString.equals(""))
		{
			if(!loadedString.equals("Exception"))
			{
				if(apiHitPosition==1)
				{
					if(parseMyServiceData(loadedString))
					{
						chooseServiceArray=new String[serviceArrayList.size()+1];
						chooseServiceArray[0]="Choose Your Service";

						for(int i=0;i<serviceArrayList.size();i++)
						{
							chooseServiceArray[i+1]=serviceArrayList.get(i).get(ProjectConstant.API_OPNAME);
						}

						chooseServiceAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textsize,chooseServiceArray);
						chooseServiceAdapter.setDropDownViewResource(R.layout.single_row_spinner);
						chooseServiceSpinner.setAdapter(chooseServiceAdapter);
					}
				}

				if(apiHitPosition==2)
				{
					if(parseMyTopUpData(loadedString))
					{
						showChooseTopUpLayout(noOfProducts);
						String imageUrl="https://kwikpay.co.uk/kwikpay/kwikpay/Image/appimages/"+selectedOperatorFromSpinner+"APP.png";
						new ShowOperatorImageAsync().execute(imageUrl);

						if(Integer.parseInt(noOfProducts)>6)
						{
							handleSixGreenButton(6);
							
							// COMMENTING AS PER NEW REQUIREMENT : implement the functionality of "more" topups
							
							/*topUpPlansArray=new String[topUpPlansArrayList.size()+1];
							topUpPlansArray[0]="Choose Your TopUp";
							for(int i=0;i<topUpPlansArrayList.size();i++)
							{
								topUpPlansArray[i+1]=Html.fromHtml(topUpPlansArrayList.get(i).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH))+" "
										+topUpPlansArrayList.get(i).get(ProjectConstant.TOPUP_API_DISPLAYAMT) +" - "
										+Html.fromHtml(topUpPlansArrayList.get(i).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH))+" "
										+topUpPlansArrayList.get(i).get(ProjectConstant.TOPUP_API_PAYMENTAMT);
							}

							chooseTopupAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textsize,topUpPlansArray);
							chooseTopupAdapter.setDropDownViewResource(R.layout.single_row_spinner);
							topupSpinner.setAdapter(chooseTopupAdapter);*/
						}

						else
						{
							//Logic for Handling six buttons
							handleSixGreenButton(Integer.parseInt(noOfProducts));
						}
					}
				}

				/*if(apiHitPosition==3)
				{
					if(parseMyPaymentData(loadedString))
					{
						if(preference.getString("KP_USER_ID", "").equals("0"))
						{
							Intent logInIntent=new Intent(GameAndService.this,LoginScreen.class);
							logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
							startActivity(logInIntent);
						}
						else
						{
							Intent payNowIntent=new Intent(GameAndService.this,OrderDetails.class);
							payNowIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
							startActivity(payNowIntent);
						}
					}
				}*/

			}
			else
			{
				if(!controlNetOffExit)
				GameAndService.this.finish();
				
				if(DeviceNetConnectionDetector.checkDataConnWifiMobile(GameAndService.this))
					ARCustomToast.showToast(GameAndService.this,getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
				else
					ARCustomToast.showToast(GameAndService.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
			}
		}
		else
		{
			if(!controlNetOffExit)
			GameAndService.this.finish();
			
			if(DeviceNetConnectionDetector.checkDataConnWifiMobile(GameAndService.this))
				ARCustomToast.showToast(GameAndService.this,getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
			else
				ARCustomToast.showToast(GameAndService.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}
	}

	// LOGIC FOR PARSING GAME AND SERVICES FROM WEBSERVICE 
	public boolean parseMyServiceData(String operatorResponse)
	{
		JSONObject jsonObject,parentResponseObject,childResponseObject;
		try 
		{
			jsonObject=new JSONObject(operatorResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				ARCustomToast.showToast(GameAndService.this, "No Reponse Tag", Toast.LENGTH_LONG);
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
			{
				if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
				{
					if(parentResponseObject.has(ProjectConstant.GAMESERVICE_KPDIGITALSERVICE))
					{
						JSONArray topupJsonArray=parentResponseObject.getJSONArray(ProjectConstant.GAMESERVICE_KPDIGITALSERVICE);
						serviceArrayList=new ArrayList<>();

						for(int i=0;i<topupJsonArray.length();i++)
						{
							childResponseObject=topupJsonArray.getJSONObject(i);

							if (!childResponseObject.isNull(ProjectConstant.API_OPNAME)
									&& !childResponseObject.isNull(ProjectConstant.API_OPTEMPACTIVEFLAG) 
									&& !childResponseObject.isNull(ProjectConstant.API_OPID)
									&& !childResponseObject.isNull(ProjectConstant.API_OPIMGPATH)
									&& !childResponseObject.isNull(ProjectConstant.API_OPCODE)
									&& !childResponseObject.isNull(ProjectConstant.API_OPACTIVEFLAG) 
									&& !childResponseObject.isNull(ProjectConstant.API_OPCIRCLEACTIVEFLAG)) 
							{
								serviceHashMap=new HashMap<>();
								serviceHashMap.put(ProjectConstant.API_OPNAME, childResponseObject.getString(ProjectConstant.API_OPNAME));
								serviceHashMap.put(ProjectConstant.API_OPTEMPACTIVEFLAG, childResponseObject.getString(ProjectConstant.API_OPTEMPACTIVEFLAG));
								serviceHashMap.put(ProjectConstant.API_OPID, childResponseObject.getString(ProjectConstant.API_OPID));
								serviceHashMap.put(ProjectConstant.API_OPIMGPATH, childResponseObject.getString(ProjectConstant.API_OPIMGPATH));
								serviceHashMap.put(ProjectConstant.API_OPCODE, childResponseObject.getString(ProjectConstant.API_OPCODE));
								serviceHashMap.put(ProjectConstant.API_OPACTIVEFLAG, childResponseObject.getString(ProjectConstant.API_OPACTIVEFLAG));
								serviceHashMap.put(ProjectConstant.API_OPCIRCLEACTIVEFLAG, childResponseObject.getString(ProjectConstant.API_OPCIRCLEACTIVEFLAG));

								serviceArrayList.add(serviceHashMap);
							}
						}
					}
				}

				else
				{
					if(parentResponseObject.has("DESCRIPTION"))
					{
						ARCustomToast.showToast(GameAndService.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						return false;
					}
				}

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;

	}

	// LOGIC FOR PARSING TOPUP PLANS FROM WEBSERVICE 
	public boolean parseMyTopUpData(String operatorResponse)
	{
		JSONObject jsonObject,parentResponseObject,childResponseObject;
		try 
		{
			jsonObject=new JSONObject(operatorResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				ARCustomToast.showToast(GameAndService.this, "No Reponse Tag", Toast.LENGTH_LONG);
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
			{
				if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
				{
					if(parentResponseObject.has(ProjectConstant.TOPUP_API_PRODUCTS))
					{
						JSONArray topupJsonArray=parentResponseObject.getJSONArray(ProjectConstant.TOPUP_API_PRODUCTS);
						topUpPlansArrayList=new ArrayList<>();

						noOfProducts=String.valueOf(topupJsonArray.length());

						for(int i=0;i<topupJsonArray.length();i++)
						{
							childResponseObject=topupJsonArray.getJSONObject(i);

							if (!childResponseObject.isNull(ProjectConstant.TOPUP_API_DISPLAYAMT)
									&& !childResponseObject.isNull(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID)
									&& !childResponseObject.isNull(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)
									&& !childResponseObject.isNull(ProjectConstant.TOPUP_API_RECHARGEAMT)
									&& !childResponseObject.isNull(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)
									&& !childResponseObject.isNull(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID)
									&& !childResponseObject.isNull(ProjectConstant.TOPUP_API_PAYMENTAMT)
									&& !childResponseObject.isNull(ProjectConstant.TOPUP_API_PAYMENTCURRENCYAGID)
									&& !childResponseObject.isNull(ProjectConstant.TOPUP_API_RECHARGECURRENCYIMGPATH))
							{
								topUpHashMap=new HashMap<>();

								topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYAMT, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYAMT));
								topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
								topUpHashMap.put(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH));
								topUpHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH));

								// ADDING THESE TWO EXTRA AS IT CONTAINING AT SO MANY PLACES IN CLASS [KEEPING IT]
								topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH));
								topUpHashMap.put(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH));

								topUpHashMap.put(ProjectConstant.PAYNOW_RECHARGEAMOUNT, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGEAMT));
								topUpHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
								topUpHashMap.put(ProjectConstant.TOPUP_API_PAYMENTAMT, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTAMT));
								topUpHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYAGID));
								topUpHashMap.put(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYIMGPATH));
								topUpHashMap.put(ProjectConstant.PAYNOW_OPERATORNAME, childResponseObject.getString(ProjectConstant.PAYNOW_OPERATORNAME));
								topUpHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, childResponseObject.getString("PAYMENTPROCESSINGFEE"));
								topUpHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, childResponseObject.getString("PAYMENTSERVICEFEE"));
								topUpHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, childResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION));

								topUpPlansArrayList.add(topUpHashMap);
							}
						}
					}

                    if(parentResponseObject.has("URLTERMS"))
                    {
                        ProjectConstant.tncURL=parentResponseObject.getString("URLTERMS");
                    }
                    if(parentResponseObject.has("URLVIEWPRODUCTDETAIL"))
                    {
                        viewProductDetailURL=parentResponseObject.getString("URLVIEWPRODUCTDETAIL");
                    }
                    if(parentResponseObject.has("URLPROVIDERHELP"))
                    {
                        ProjectConstant.providerDetailsURL=parentResponseObject.getString("URLPROVIDERHELP");
                    }
				}

				else
				{
					if(parentResponseObject.has("DESCRIPTION"))
					{
						ARCustomToast.showToast(GameAndService.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						return false;
					}
				}

			}
			
			if(parentResponseObject.has("CARTMSG"))
			{
				cartDeleteMsg=parentResponseObject.getString("CARTMSG");
				ProjectConstant.CART_INFO_MESSAGE=parentResponseObject.getString("CARTMSG");
			}
			

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;

	}

	// HANDLING SIX GREEN BUTTON AND THEIR VISIBILITY
	public void handleSixGreenButton(int numberOfPdts)
	{
		if(numberOfPdts>3)
		{
			LinearLayout layout = (LinearLayout) findViewById(R.id.customTopUpSecondLayout);
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)layout.getLayoutParams();
			params.setMargins(0, 15, 0, 50);
			layout.setLayoutParams(params);
		}
		
		switch(numberOfPdts)
		{
		case 1:
			greenBtnLayout2.setVisibility(View.INVISIBLE);
			greenBtnLayout3.setVisibility(View.INVISIBLE);
			secondHalfSixBtnLayout.setVisibility(View.GONE);
			greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			break;

		case 2:
			greenBtnLayout3.setVisibility(View.INVISIBLE);
			secondHalfSixBtnLayout.setVisibility(View.GONE);
			greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			break;
		case 3:

			secondHalfSixBtnLayout.setVisibility(View.GONE);
			greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTAMT));


			break;
		case 4:
			greenBtnLayout5.setVisibility(View.INVISIBLE);
			greenBtnLayout6.setVisibility(View.INVISIBLE);

			greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTAMT));
			break;

		case 5:
			greenBtnLayout6.setVisibility(View.INVISIBLE);

			greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton5.setText(Html.fromHtml(topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView5.setText(Html.fromHtml(topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			break;

		case 6:

			greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton5.setText(Html.fromHtml(topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView5.setText(Html.fromHtml(topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			greenButton6.setText(Html.fromHtml(topUpPlansArrayList.get(5).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(5).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			greenTextView6.setText(Html.fromHtml(topUpPlansArrayList.get(5).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
					topUpPlansArrayList.get(5).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

			break;
		}
	}

	public void sixGreenButtonClick(View sixBtnView)
	{
		greenButton1.setBackgroundResource(R.drawable.topup_button);
		greenButton1.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton2.setBackgroundResource(R.drawable.topup_button);
		greenButton2.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton3.setBackgroundResource(R.drawable.topup_button);
		greenButton3.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton4.setBackgroundResource(R.drawable.topup_button);
		greenButton4.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton5.setBackgroundResource(R.drawable.topup_button);
		greenButton5.setTextColor(getResources().getColor(R.color.whiteColor));
		greenButton6.setBackgroundResource(R.drawable.topup_button);
		greenButton6.setTextColor(getResources().getColor(R.color.whiteColor));
		
		LinearLayout secondGreenBtnLayout = (LinearLayout) findViewById(R.id.customTopUpSecondLayout);
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)secondGreenBtnLayout.getLayoutParams();
		params.setMargins(0, 0, 0, 0);
		secondGreenBtnLayout.setLayoutParams(params);

		switch (sixBtnView.getId())
		{
		case R.id.topupAboveValue1:
//			showSelectedTopUP(greenButton1.getText().toString(),greenTextView1.getText().toString(),0); // COMMENTING AS PER NEW REQUIREMENT 

			greenButton1.setBackgroundResource(R.drawable.topup_button_hover);
			greenButton1.setTextColor(getResources().getColor(R.color.darkGreen));

			createHashMapForOrderDetail(0);

			break;
		case R.id.topupAboveValue2:
//			showSelectedTopUP(greenButton2.getText().toString(),greenTextView2.getText().toString(),1); // COMMENTING AS PER NEW REQUIREMENT 

			greenButton2.setBackgroundResource(R.drawable.topup_button_hover);
			greenButton2.setTextColor(getResources().getColor(R.color.darkGreen));

			createHashMapForOrderDetail(1);

			break;
		case R.id.topupAboveValue3:
//			showSelectedTopUP(greenButton3.getText().toString(),greenTextView3.getText().toString(),2); // COMMENTING AS PER NEW REQUIREMENT 

			greenButton3.setBackgroundResource(R.drawable.topup_button_hover);
			greenButton3.setTextColor(getResources().getColor(R.color.darkGreen));

			createHashMapForOrderDetail(2);

			break;
		case R.id.topupAboveValue4:
//			showSelectedTopUP(greenButton4.getText().toString(),greenTextView4.getText().toString(),3); // COMMENTING AS PER NEW REQUIREMENT 

			greenButton4.setBackgroundResource(R.drawable.topup_button_hover);
			greenButton4.setTextColor(getResources().getColor(R.color.darkGreen));

			createHashMapForOrderDetail(3);

			break;
		case R.id.topupAboveValue5:
//			showSelectedTopUP(greenButton5.getText().toString(),greenTextView5.getText().toString(),4); // COMMENTING AS PER NEW REQUIREMENT 

			greenButton5.setBackgroundResource(R.drawable.topup_button_hover);
			greenButton5.setTextColor(getResources().getColor(R.color.darkGreen));

			createHashMapForOrderDetail(4);

			break;

		case R.id.topupAboveValue6:
//			showSelectedTopUP(greenButton6.getText().toString(),greenTextView6.getText().toString(),5); // COMMENTING AS PER NEW REQUIREMENT 

			greenButton6.setBackgroundResource(R.drawable.topup_button_hover);
			greenButton6.setTextColor(getResources().getColor(R.color.darkGreen));

			createHashMapForOrderDetail(5);

			break;

		default:
			break;
		}
	}

	public class ShowOperatorImageAsync extends AsyncTask<String, Void, Bitmap>
	{
		@Override
		protected Bitmap doInBackground(String... urlString) {
			// TODO Auto-generated method stub
			try 
			{
				URL url = new URL(urlString[0]);
				HttpURLConnection imageConnection = (HttpURLConnection)url.openConnection();
				imageConnection.setConnectTimeout(7000);
				imageConnection.setReadTimeout(7000);
				imageConnection.setInstanceFollowRedirects(true);
				operatorImageBitmap = BitmapFactory.decodeStream(imageConnection.getInputStream());

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				return operatorImageBitmap=null;
				
			}
			return operatorImageBitmap;
		}


		@Override
		protected void onPostExecute(Bitmap result) 
		{
			// TODO Auto-generated method stub
			if(result!=null)
				chooseProductOnFlyIV.setImageBitmap(result);
			else
				chooseProductOnFlyIV.setImageDrawable(getResources().getDrawable(R.drawable.default_operator));
		}

	}

	// LOGIC FOR PARSING PAYMENT DATA AFTER CLICKING PAY NOW BUTTON
	/*public boolean parseMyPaymentData(String payNowResponse)
	{
		JSONObject jsonObject,parentResponseObject;
		try 
		{
			jsonObject=new JSONObject(payNowResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				ARCustomToast.showToast(GameAndService.this, "No Reponse Tag", Toast.LENGTH_LONG);
				return false;
			}

			else
			{
				parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

				if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
				{
					if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
					{
						if(parentResponseObject.has(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE) 
								&& parentResponseObject.has(ProjectConstant.PAYNOW_OPERATORNAME)
								&& parentResponseObject.has(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE)
								&& parentResponseObject.has(ProjectConstant.PAYNOW_PROCESSINGFEE)
								&& parentResponseObject.has(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE)
								&& parentResponseObject.has(ProjectConstant.PAYNOW_RECHARGEAMOUNT)
								&& parentResponseObject.has(ProjectConstant.PAYNOW_SERVICEFEE) 
								&& parentResponseObject.has(ProjectConstant.PAYNOW_PLANDESCRIPTION) 
								&& parentResponseObject.has(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID) )
						{

							payNowHashMap=new HashMap<String, String>();
							payNowHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE, parentResponseObject.getString(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE));
							payNowHashMap.put(ProjectConstant.PAYNOW_OPERATORNAME, parentResponseObject.getString(ProjectConstant.PAYNOW_OPERATORNAME));
							payNowHashMap.put(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE, parentResponseObject.getString(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE));
							payNowHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_PROCESSINGFEE));
							payNowHashMap.put(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE, parentResponseObject.getString(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE));
							payNowHashMap.put(ProjectConstant.PAYNOW_RECHARGEAMOUNT, parentResponseObject.getString(ProjectConstant.PAYNOW_RECHARGEAMOUNT));
							payNowHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_SERVICEFEE));
							payNowHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, parentResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION));
							
							payNowHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, parentResponseObject.getString(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID));
							payNowHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, selectedRCCurrencyAGID);
							payNowHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID,selectedDisplayCurrencyAGID );
							
							payNowHashMap.put(ProjectConstant.PAYNOW_MOBILENUMBER, "N/A");
							payNowHashMap.put(ProjectConstant.TOPUP_API_DISPLAYAMT, payNowDisplayAmount);
							payNowHashMap.put(ProjectConstant.TOPUP_API_PAYMENTAMT, payNowPaymentAmount);
							payNowHashMap.put(ProjectConstant.API_OPCODE, selectedOperatorFromSpinner);
							payNowHashMap.put("SERVICEID", "3");
							payNowHashMap.put(ProjectConstant.TXN_COUNTRYCODE, "UNITEDKINGDOM");
							payNowHashMap.put(ProjectConstant.API_OPID, selectedOperatorIdFromSpinner);
							payNowHashMap.put("AGID", "44");
						}
					}
					else
					{
						if(parentResponseObject.has("DESCRIPTION"))
						{
							ARCustomToast.showToast(GameAndService.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
							return false;
						}
					}
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}*/
	
	public void createHashMapForOrderDetail(int position)
	{
		try
		{
			payNowHashMap=new HashMap<>();
			payNowHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE));
			payNowHashMap.put(ProjectConstant.PAYNOW_OPERATORNAME, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_OPERATORNAME));
			payNowHashMap.put(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE,topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE));
			payNowHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PROCESSINGFEE));
			payNowHashMap.put(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE));
			payNowHashMap.put(ProjectConstant.PAYNOW_RECHARGEAMOUNT,topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGEAMOUNT));
			payNowHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE,topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_SERVICEFEE));
			payNowHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PLANDESCRIPTION));
			payNowHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID));
			
			payNowHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
			payNowHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID,topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
			payNowHashMap.put(ProjectConstant.TOPUP_API_DISPLAYAMT, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			payNowHashMap.put(ProjectConstant.TOPUP_API_PAYMENTAMT, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_PAYMENTAMT));
			
			payNowHashMap.put(ProjectConstant.PAYNOW_MOBILENUMBER, "N/A");
			payNowHashMap.put(ProjectConstant.API_OPCODE, selectedOperatorFromSpinner);
			payNowHashMap.put("SERVICEID", "3");
			payNowHashMap.put(ProjectConstant.TXN_COUNTRYCODE, "UNITEDKINGDOM");
			payNowHashMap.put(ProjectConstant.API_OPID, selectedOperatorIdFromSpinner);
			payNowHashMap.put("AGID", "44");
			payNowHashMap.put("CARTMSG", cartDeleteMsg);
			
			controlNetOffExit=true;
			if(preference.getString("KP_USER_ID", "").equals("0"))
//			if(preference.getString("KP_USER_ID", "").equals("rem"))
			{
				Intent logInIntent=new Intent(GameAndService.this,LoginScreen.class);
				logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
				startActivity(logInIntent);
			}
			else
			{
				// Create the Values
				Map<String,Object> event = new HashMap<>();
				event.put(AFInAppEventParameterName.PRICE,topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGEAMOUNT));
				event.put(AFInAppEventParameterName.CONTENT_TYPE,"GameAndServices");		        
				event.put(AFInAppEventParameterName.CURRENCY,"GBP");
				event.put(AFInAppEventParameterName.QUANTITY,1);

				AppsFlyerLib.trackEvent(GameAndService.this, AFInAppEventType.ADD_PAYMENT_INFO,event);

				Intent payNowIntent=new Intent(GameAndService.this,OrderDetails.class);
				payNowIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
				startActivity(payNowIntent);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	
	}

	@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		/*if(preference!=null && cartItemCounter!=null)
		{
			//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
			if((preference.getString("CART_ITEM_COUNTER", "0").equals("0")))
				cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
			else
				cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}*/
	}
}
