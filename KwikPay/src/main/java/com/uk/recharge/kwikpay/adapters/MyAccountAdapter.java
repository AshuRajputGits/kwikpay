package com.uk.recharge.kwikpay.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

public class MyAccountAdapter extends BaseAdapter 
{
	Activity context;
	ArrayList<HashMap<String, String>> myAccountArrayList;
	String amountPaid="";

	public MyAccountAdapter(Activity context,ArrayList<HashMap<String, String>> myAccountArrayList) 
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		this.myAccountArrayList = myAccountArrayList;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return myAccountArrayList.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder 
	{
		TextView tvServiceProvider,tvAmountPaid;
//		ImageView arrowIV;

		ViewHolder(View view)
		{
			tvServiceProvider = (TextView) view.findViewById(R.id.myAccountServiceProvider);
			tvAmountPaid = (TextView) view.findViewById(R.id.myAccountAmtPaid);
//			arrowIV=(ImageView)view.findViewById(R.id.myTxnArrowBtn);
//			tvDOT = (TextView) view.findViewById(R.id.myAccountDOT);
//			tvPaymentStatus = (TextView) view.findViewById(R.id.myAccountPaymentstatus);
		}

	}

	@SuppressWarnings("static-access")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View row=convertView;
		ViewHolder holder;

		if(row==null)
		{
			LayoutInflater inflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row=inflater.inflate(R.layout.singlemyaccount_row, parent, false);
			holder=new ViewHolder(row);
			row.setTag(holder);
		}

		else
		{
			holder=(ViewHolder)row.getTag();
		}

		// Ashu... Setting Background color to list view at odd and even position
		if(position%2==0)
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewEvenColor));
		else
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewOddColor));


//		holder.tvDOT.setText(datetimeSeparator(myAccountArrayList.get(position).get(ProjectConstant.TXN_TRANSACTIONDATETIME)));

		String TXN_STATUS="";
		
		if(myAccountArrayList.get(position).get(ProjectConstant.TXN_PAYMENTSTATUS).equals("SUCCESSFUL"))
			TXN_STATUS=myAccountArrayList.get(position).get(ProjectConstant.TXN_RECHARGESTATUS);
		else
			TXN_STATUS=myAccountArrayList.get(position).get(ProjectConstant.TXN_PAYMENTSTATUS);

		holder.tvServiceProvider.setText(myAccountArrayList.get(position).get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER)+"\n" +
										 myAccountArrayList.get(position).get(ProjectConstant.TXN_OPERATORNAME));

		holder.tvAmountPaid.setText(Html.fromHtml(myAccountArrayList.get(position).get(ProjectConstant.TXN_PAYMENTCURRENCYCODE))+" " +
				myAccountArrayList.get(position).get(ProjectConstant.TXN_PAYMENTAMOUNT)+"\n"+TXN_STATUS) ;
				
		return row;

	}

	//METHOD FOR SPLITING DATE AND TIME WITH SPACE
	/*private String datetimeSeparator(String DOT)
	{
		String finalDOT="";
		if(DOT.contains(" "))
		{
			try
			{
				String[] dotArray = DOT.split(" ");
				finalDOT=dotArray[0]+"\n"+dotArray[1];
				return finalDOT;
				
			}catch(ArrayIndexOutOfBoundsException arrayIndexException){
				return finalDOT="";
			}
			catch(Exception exception)
			{
				return finalDOT="";
			}
		}
		else
		{
			return finalDOT="";
		}
	}*/
	
	//METHOD FOR SPLITING ORDER ID WITH HYPHEN(-)
	/*private String orderIdSeperator(String orderId)
	{
		if(orderId.contains("-"))
		{
			String[] orderIdString = orderId.split("-");
			return orderIdString[0];
		}
		else
		{
			return orderId;
		}
	}*/


}

