package com.uk.recharge.kwikpay.gamingarcade.models;

/**
 * Created by Ashu Rajput on 7/6/2018.
 */

public class PricingOptionsMO {

    private String planID;
    private String planAmount;
    private String planDesc;
    private String planCredit;
    private String serviceFee;
    private String processingFee;

    public String getPlanID() {
        return planID;
    }

    public void setPlanID(String planID) {
        this.planID = planID;
    }

    public String getPlanAmount() {
        return planAmount;
    }

    public void setPlanAmount(String planAmount) {
        this.planAmount = planAmount;
    }

    public String getPlanDesc() {
        return planDesc;
    }

    public void setPlanDesc(String planDesc) {
        this.planDesc = planDesc;
    }

    public String getPlanCredit() {
        return planCredit;
    }

    public void setPlanCredit(String planCredit) {
        this.planCredit = planCredit;
    }

    public String getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(String serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(String processingFee) {
        this.processingFee = processingFee;
    }
}
