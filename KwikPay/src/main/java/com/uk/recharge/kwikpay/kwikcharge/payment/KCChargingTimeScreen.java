package com.uk.recharge.kwikpay.kwikcharge.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.kccharger.KCHoldingScreen;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;

/**
 * Created by Ashu Rajput on 10/25/2017.
 */

public class KCChargingTimeScreen extends AppCompatActivity {

    CountDownTimer countDownTimer = null;
    private SharedPreferences preference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_charging_time_screen);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        int maxIntChargingTime = 1;
        try {
            String maxChargingTime = KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getRecentMaxTime();
            Double time = Double.parseDouble(maxChargingTime);
            maxIntChargingTime = time.intValue() * 60;

        } catch (Exception e) {
            e.printStackTrace();
        }

        TextView chargeTimerTV = findViewById(R.id.chargeTimerTV);
        initiateChargingTimer(maxIntChargingTime, chargeTimerTV);

        findViewById(R.id.kcStopChargingButton).setOnClickListener(onClickListener);
        findViewById(R.id.headerHomeBtn).setOnClickListener(onClickListener);

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.kcStopChargingButton) {
                callKCHoldingScreen();
//                new LoadResponseViaPost(KCChargingTimeScreen.this, formKCStopChargingJson(), true).execute("");
            } else if (v.getId() == R.id.headerHomeBtn) {
                try {
                    if (countDownTimer != null)
                        countDownTimer.cancel();
                } catch (Exception e) {
                }

                removeInformationFromPrefs();

                Intent intent = new Intent(KCChargingTimeScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    };

    public void initiateChargingTimer(int Seconds, final TextView tv) {

        countDownTimer = new CountDownTimer(Seconds * 1000 + 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                //Commenting as per new requirement
                /*int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                int hours = minutes / 60;
                seconds = seconds % 60;
                tv.setText(String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));*/
            }

            public void onFinish() {
                callKCHoldingScreen();
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
    }

    /*public String formKCStopChargingJson() {
        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("APISERVICE", "KC_STOP_CHARGE");
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
            jsonobj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("LOCATION_NUMBER", preference.getString("TEMP_LOCATION_NUMBER", ""));
            jsonobj.put("DEVICE_DB_ID", preference.getString("TEMP_DEVICE_DB_ID", ""));
            jsonobj.put("LOCAL_CONNECTOR_NUMBER", preference.getString("TEMP_LOCAL_CONNECTOR_NUMBER", ""));
            jsonobj.put("APP_TXN_ID", preference.getString("TEMP_APP_TXN_ID", ""));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }*/


   /* @Override
    public void onRequestComplete(String loadedString) {
        if (!loadedString.equals("") && !loadedString.equals("Exception")) {
            parseStopChargingDetails(loadedString);
        } else {
            ARCustomToast.showToast(KCChargingTimeScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    private void parseStopChargingDetails(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    if (countDownTimer != null)
                        countDownTimer.cancel();

//                    removeInformationFromPrefs();

                    KCReceiptMO kcReceiptMO = new KCReceiptMO();
                    kcReceiptMO.setRegistrationNumber(childJsonObj.optString("REGISTRATION_NUMBER"));
                    kcReceiptMO.setLocationName(childJsonObj.optString("LOCATIONNAME"));
                    kcReceiptMO.setDuration(childJsonObj.optString("DURATION"));
                    kcReceiptMO.setTotalEnergy(childJsonObj.optString("TOTAL_ENERGY"));
                    kcReceiptMO.setServiceFee(childJsonObj.optString("SERVICEFEE"));
                    kcReceiptMO.setMaxTime(childJsonObj.optString("MAX_TIME"));
                    kcReceiptMO.setRate(childJsonObj.optString("RATE"));

//                    startActivity(new Intent(KCChargingTimeScreen.this, KCHoldingScreen.class).putExtra("KC_RECEIPT_DETAILS_MO", kcReceiptMO));

                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        ARCustomToast.showToast(KCChargingTimeScreen.this, childJsonObj.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (Exception e) {
            ARCustomToast.showToast(KCChargingTimeScreen.this, "Parsing exception", Toast.LENGTH_LONG);
        }
    }*/

    private void callKCHoldingScreen() {
        startActivity(new Intent(KCChargingTimeScreen.this, KCHoldingScreen.class));
    }

    private void removeInformationFromPrefs() {
        try {
            SharedPreferences.Editor editor = preference.edit();
            editor.remove("TEMP_DEVICE_DB_ID");
            editor.remove("TEMP_LOCAL_CONNECTOR_NUMBER");
            editor.remove("TEMP_APP_TXN_ID");
            editor.apply();
        } catch (Exception e) {
        }
    }

}
