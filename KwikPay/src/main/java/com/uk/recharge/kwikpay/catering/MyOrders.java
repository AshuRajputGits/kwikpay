package com.uk.recharge.kwikpay.catering;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.validation.FieldValidation;
import com.ar.library.validation.FieldValidationWithMessage;
import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.BillingAddressScreen;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.LoginScreen;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.ProductType;
import com.uk.recharge.kwikpay.adapters.catering.CartProductsAdapter;
import com.uk.recharge.kwikpay.adapters.catering.RecommendationsAdapter;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.uk.recharge.kwikpay.catering.CateringCart.cartProductId;
import static com.uk.recharge.kwikpay.catering.CateringCart.cartProducts;
import static com.uk.recharge.kwikpay.catering.CateringCart.cartTotal;
import static com.uk.recharge.kwikpay.catering.CateringCart.resolveConflicts;
import static com.uk.recharge.kwikpay.catering.CateringCart.setPaymentTotal;
import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;

public class MyOrders extends AppCompatActivity implements LoadResponseViaPost.AsyncRequestListenerViaPost {

    private static final int BILLING_ADDRESS_REQUEST_CODE = 300;
    public static TextView billamount, itemtotal;
    public static RecyclerView recomdation_recycler_view;
    public static RecyclerView cart_items_recycler_view;
    static String couponAmount = "0.00", couponTxnId = "", couponSrNo = "";
    protected boolean isAddressFound = false;
    SharedPreferences preference;
    CartProductsAdapter cartProductsAdapter;
    EditText couponBoxET1, couponBoxET2, couponBoxET3;
    int apiHitPosition = 0;
    String transactionFee = "", totalAmountPayable = "", couponCode = "", infoIconDisplayFlag = "", infoIconDisplayMsg = "";
    Context context;
    LinearLayout reedemCouponBoxesLL, apply_coupon_layout;
    AppDatabase appDatabase;
    boolean apply_coupon_flag = false;
    private RecommendationsAdapter mAdapter;
    private ImageView reedemCouponGoTV;
    private String directPaymentScreenFlag = "";
    private boolean isSuccessfullCouponCase = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);

        context = this;
        initializeViewIdAndUI();

        apply_coupon_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (apply_coupon_flag)
                    reedemCouponBoxesLL.setVisibility(View.GONE);
                else
                    reedemCouponBoxesLL.setVisibility(View.VISIBLE);
                apply_coupon_flag = !apply_coupon_flag;
            }
        });

        apiHitPosition = 8;
        LoadResponseViaPost asyncObject8 = new LoadResponseViaPost(MyOrders.this, formConfirmAddressJSON(), true);
        asyncObject8.execute("");

        setPaymentTotal(itemtotal, billamount);
        getCartProduct();

        reedemCouponGoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (!FieldValidation.couponFieldIsValid(couponBoxET1.getText().toString(), couponBoxET1, couponBoxET1.getText().toString().length(), 1)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    ARCustomToast.showToast(MyOrders.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else if (!FieldValidation.couponFieldIsValid(couponBoxET2.getText().toString(), couponBoxET2, couponBoxET2.getText().toString().length(), 2)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    ARCustomToast.showToast(MyOrders.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else if (!FieldValidation.couponFieldIsValid(couponBoxET3.getText().toString(), couponBoxET3, couponBoxET3.getText().toString().length(), 3)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    ARCustomToast.showToast(MyOrders.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    apiHitPosition = 4;
                    new LoadResponseViaPost(MyOrders.this, formRedeemCouponJSON(), true).execute("");
                }
            }
        });
    }

    private void initializeViewIdAndUI() {
        appDatabase = Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        billamount = findViewById(R.id.billamount);
        itemtotal = findViewById(R.id.itemtotal);
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        couponBoxET1 = findViewById(R.id.redeemCouponCheckBoxET1);
        couponBoxET2 = findViewById(R.id.redeemCouponCheckBoxET2);
        couponBoxET3 = findViewById(R.id.redeemCouponCheckBoxET3);
        apply_coupon_layout = findViewById(R.id.apply_coupon_layout);
        reedemCouponBoxesLL = findViewById(R.id.reedemCouponBoxesLayouts);
        reedemCouponGoTV = findViewById(R.id.reedemCouponGoTV);
        ListView mDrawerList = findViewById(R.id.left_drawer);

        CustomSlidingDrawer csd = new CustomSlidingDrawer(MyOrders.this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        TextView distributorDetails = findViewById(R.id.Disributordetails);
        distributorDetails.setText(Utility.fromHtml("<font color='#00581e'>" + preference.getString("DISTRIBUTOR_NAME", "name") + "<br />" +
                "<medium><medium><font color='#8dc600'>" + preference.getString("DISTRIBUTOR_ADDRESS", "address") + "</medium></medium>"));
        ImageView distributorImageView = findViewById(R.id.disributorimg);

        Glide.with(this)
                .load(preference.getString("DISTRIBUTOR_IMAGE", "image"))
                .placeholder(R.drawable.default_operator_logo)
                .into(distributorImageView);

        recomdation_recycler_view = findViewById(R.id.recommendationsrecycler_view);
        cart_items_recycler_view = findViewById(R.id.recycler_view);

        findViewById(R.id.backbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.pay_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartProducts.isEmpty()) {
                    Toast.makeText(context, "Cart is Empty!", Toast.LENGTH_SHORT).show();
                } else {
                    //     confirmPaymentProcess();
                    if (isSuccessfullCouponCase) // COMING HERE AFTER ENTERING AND VALIDATING THE CORRECT COUPON FROM API @ GO BUTTON
                    {
                        isSuccessfullCouponCase = false;
                        apiHitPosition = 5;
                        new LoadResponseViaPost(MyOrders.this, formLockCouponJson(), true).execute("");
                    }
                    createHashMapForOrderDetail();
                }
            }
        });

    }

    private void getCartProduct() {
        resolveConflicts(appDatabase);
        List<ProductType> productTypeList = appDatabase.productTypeDao().getAll();
        List<ProductType> recommendationsProductList = new ArrayList<>();

        for (ProductType productType : productTypeList) {
            if (!cartProductId.contains(productType.getProduct_id())) {
                recommendationsProductList.add(productType);
            }
        }

        mAdapter = new RecommendationsAdapter(recommendationsProductList, context);
        recomdation_recycler_view.setItemAnimator(new DefaultItemAnimator());
        recomdation_recycler_view.setAdapter(mAdapter);

        cartProductsAdapter = new CartProductsAdapter(context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        cart_items_recycler_view.setLayoutManager(mLayoutManager);
        cart_items_recycler_view.setItemAnimator(new DefaultItemAnimator());
        cart_items_recycler_view.setAdapter(cartProductsAdapter);
    }

    public void createHashMapForOrderDetail() {
        try {

            //CHECKING WHETHER USER ALREADY LOGED IN OR NOT
            if (preference.getString("KP_USER_ID", "").equals("0")) {
                Intent logInIntent = new Intent(MyOrders.this, LoginScreen.class);
                logInIntent.putExtra("IS_COMING_FROM_CATERING", "IS_COMING_FROM_CATERING");
                //      logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
                startActivity(logInIntent);
            } else {
                // Create the Values
                Map<String, Object> event = new HashMap<>();
                //     event.put(AFInAppEventParameterName.PRICE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGEAMOUNT));
                event.put(AFInAppEventParameterName.CONTENT_TYPE, "TopUpYourPhone");
                event.put(AFInAppEventParameterName.CURRENCY, "GBP");
                event.put(AFInAppEventParameterName.QUANTITY, 1);

                AppsFlyerLib.trackEvent(MyOrders.this, AFInAppEventType.ADD_PAYMENT_INFO, event);
                AppsFlyerLib.trackEvent(MyOrders.this, "TopUp", null);

                Intent payNowIntent = new Intent(MyOrders.this, CateringPaymentOptions.class);
                payNowIntent.putExtra("IS_COMING_FROM_CATERING", "IS_COMING_FROM_CATERING");
                startActivity(payNowIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartProduct();
        setPaymentTotal(itemtotal, billamount);
    }

    //FORM REDEEM COUPON JSON PARAM
    private String formRedeemCouponJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPCPNVALIDATION");
            json.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("PARTYCODE", couponBoxET1.getText().toString().toUpperCase(Locale.getDefault()));
            json.put("OPERATORID", "3GEV");
            json.put("CPNCODE", couponBoxET2.getText().toString().toUpperCase(Locale.getDefault()) + couponBoxET3.getText().toString().toUpperCase(Locale.getDefault()));
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            //TODO : set couponpayment amount

            //   json.put("PAYMENTAMT", couponPaymentAmount);
            json.put("PAYMENTAMT", cartTotal);
            json.put("PROCESSINGFEE", "0.00");
            json.put("SERVICEFEE", "0.20");
            json.put("SERVICEID", "1");
            json.put("AGID", "44");
            json.put("COUNTRYCODE", "UNITEDKINGDOM");
            json.put("MOBILENUMBER", "NA");


            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    //FORM LOCK COUPON JSON REQUEST PARAMETERS [ApiHitPosition==5]
    private String formLockCouponJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPCPNLOCKSTATUS");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("CPNSERIALNUMBER", couponSrNo);
            json.put("UNIQUEID", couponTxnId);
            json.put("ISLOCK", "Y");

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void onRequestComplete(String loadedString) {
        if (apiHitPosition == 4) {
            if (parseCouponDetails(loadedString)) {
                //reedemCouponBoxesLL.setVisibility(View.GONE);
                //   couponCheckBox.setVisibility(View.GONE);
            }
        } else if (apiHitPosition == 5) {
            parseLockCouponResoponse(loadedString);
        } else if (apiHitPosition == 8) {
//                    apiHitPosition=0;
            // NOTHING TO DO WITH THE RESPONSE OF THIS API... IT IS TRIGGERED TO SEND INFO TO SERVER
            parseAddressConfirmStatus(loadedString);
        }
    }

    //LOGIC FOR PARSING THE COUPON RECEIVED DATA [apiHitPosition==4]
    public boolean parseCouponDetails(String couponResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(couponResponse);
            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(MyOrders.this, "No Response Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has("COUPONCODE"))
                        couponCode = parentResponseObject.getString("COUPONCODE");
                    if (parentResponseObject.has(ProjectConstant.CART_CPNSRNO))
                        couponSrNo = parentResponseObject.getString(ProjectConstant.CART_CPNSRNO);
                    if (parentResponseObject.has("CPNAMT"))
                        couponAmount = parentResponseObject.getString("CPNAMT");
                    if (parentResponseObject.has("PGAMT"))
                        totalAmountPayable = parentResponseObject.getString("PGAMT");
                    if (parentResponseObject.has("INFOICONDISPLAYFLAG"))
                        infoIconDisplayFlag = parentResponseObject.getString("INFOICONDISPLAYFLAG");
                    if (parentResponseObject.has("INFOICONDISPLAYMSG"))
                        infoIconDisplayMsg = parentResponseObject.getString("INFOICONDISPLAYMSG");
                    if (parentResponseObject.has("COUPONTXNID"))
                        couponTxnId = parentResponseObject.getString("COUPONTXNID");

                    isSuccessfullCouponCase = true;

                   /* if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE))
                        couponDiscountAmount.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE)) + " " + couponAmount);*/
                    /*if (receivedPayNowDetailHashMap.containsKey("DISPLAYAMOUNTCURRENCYCODE"))
                        couponDiscountAmount.setText(Html.fromHtml(receivedPayNowDetailHashMap.get("DISPLAYAMOUNTCURRENCYCODE")) + " " + couponAmount);*/

                    totalAmountPayable = paymentConverter(transactionFee, totalAmountPayable);
                    billamount.setText(String.format("£ %s", totalAmountPayable));
                    itemtotal.setText(String.format("£ %s", totalAmountPayable));
                    //   totAmtPayableValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE)) + " " + totalAmountPayable);

                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(MyOrders.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(MyOrders.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //LOGIC FOR PARSING THE LOCK COUPON RESPONSE [apiHitPosition==5]
    public boolean parseLockCouponResoponse(String deleteTxnResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(deleteTxnResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(MyOrders.this, "No Response Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    return true;
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(MyOrders.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @SuppressLint("DefaultLocale")
    public String paymentConverter(String amount1, String amount2) {
        if (amount1.equals(""))
            amount1 = "0.00";
        if (amount2.equals(""))
            amount2 = "0.00";

        try {
            Float floatAmt1 = Float.parseFloat(amount1);
            Float floatAmt2 = Float.parseFloat(amount2);
            floatAmt1 = floatAmt1 + floatAmt2;
            return String.format("%.2f", floatAmt1);
        } catch (Exception e) {
        }
        return "0.00";
    }

    //FORMING JSON REQUEST PARAMETERS FOR CONFIRM USER IS ENTERS IN ORDER SCREEN[ApiHitPosition==8]
    private String formConfirmAddressJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KP_GETADDRESS_BY_CONFIRMDETAIL");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("OPERATORCODE", "CATERING");
            json.put("SERVICEID", "6");
            json.put("COUNTRYCODE", "UNITEDKINGDOM");
            json.put("PAYMENTAMT", cartTotal);
            json.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("SUBSCRIBERNUM", "NA");

//            Log.e("Pooja Data", "confirmdetail" + json.toString());

            return json.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void parseAddressConfirmStatus(String response) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(response);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(MyOrders.this, "No Response Tag", Toast.LENGTH_LONG);
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has("DIRECT_PAYMENT_SCREEN")) {
                        directPaymentScreenFlag = parentResponseObject.optString("DIRECT_PAYMENT_SCREEN");

                        SharedPreferences.Editor prefEditor = preference.edit();
                        if (parentResponseObject.has("IS_POSTCODE_DEFINED") && !parentResponseObject.getString("IS_POSTCODE_DEFINED").isEmpty())
                            prefEditor.putBoolean("IS_KP_USER_POSTCODE_DEFINED", Boolean.parseBoolean(parentResponseObject.optString("IS_POSTCODE_DEFINED")));
                        if (parentResponseObject.has("IS_EMAIL_VERIFIED") && !parentResponseObject.getString("IS_EMAIL_VERIFIED").isEmpty())
                            prefEditor.putBoolean("IS_KP_USER_EMAIL_VERIFIED", Boolean.parseBoolean(parentResponseObject.optString("IS_EMAIL_VERIFIED")));
                        prefEditor.apply();

                        if (directPaymentScreenFlag.equals("0")) {
                            // IF FLAG=1: ADDRESS FOUND IN THE D.B.
                            // IF FLAG=0: ADDRESS NOT FOUND IN THE D.B.
                            if (parentResponseObject.optString("ADDRESSRESPONSECODE").equals("1")) {
                                //      addressPostalCodeTV.setText("Address : " + parentResponseObject.optString("POSTALCODE"));
                                isAddressFound = true;
                            }
                        }
                    }

                } else {
                    if (parentResponseObject.has("DESCRIPTION"))
                        ARCustomToast.showToast(MyOrders.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void payNowButtonProcessing() {
        Intent payNowIntent = new Intent(MyOrders.this, CateringPaymentOptions.class);
        payNowIntent.putExtra("IS_COMING_FROM_CATERING", "IS_COMING_FROM_CATERING");
        startActivity(payNowIntent);
    }

    public void confirmPaymentProcess() {
        if (directPaymentScreenFlag.equals("1")) {
            payNowButtonProcessing();
        } else if (directPaymentScreenFlag.equals("0")) {
            if (isAddressFound)
                payNowButtonProcessing();
            else {
                Intent billingIntent = new Intent(MyOrders.this, BillingAddressScreen.class);
                startActivityForResult(billingIntent, BILLING_ADDRESS_REQUEST_CODE);
            }
        }
    }
}

