package com.uk.recharge.kwikpay.services;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.uk.recharge.kwikpay.BillingAddressScreen;
import com.uk.recharge.kwikpay.ContactUs;
import com.uk.recharge.kwikpay.EditProfile;
import com.uk.recharge.kwikpay.FavouriteScreen;
import com.uk.recharge.kwikpay.GameAndService;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.InternationalTopUp;
import com.uk.recharge.kwikpay.LoginScreen;
import com.uk.recharge.kwikpay.SignUpScreen;
import com.uk.recharge.kwikpay.TopUpYourPhone;
import com.uk.recharge.kwikpay.myaccount.MyTransactionActivity;

/**
 * An activity that creates and launches a task stack from a deep link uri.
 * <p/>
 * The activity will operate on any URI that it is started with, only parsing
 * the URI's path and query parameters. URI filtering should be defined in the
 * AndroidManifest.xml for the ParseDeepLinkActivity entry by defining an intent
 * filter.
 * <p/>
 * Handles URLs of the following syntax:
 * <url> := vnd.urbanairship.sample://deeplink/<deep-link>
 * <deep-link> := home | preferences | inbox | inbox?<message_id>
 * <message-id> := A rich push message ID
 * <p/>
 * Examples:
 * <p/>
 * // Deep link to inbox
 * vnd.urbanairship.sample://deeplink/inbox
 * <p/>
 * // Deep link to home
 * vnd.urbanairship.sample://deeplink/home
 * <p/>
 * // Deep link to preferences
 * vnd.urbanairship.sample://deeplink/preferences
 * <p/>
 * // Deep link to the message 'VJO7DpCEQ2i7LdqOxLbFxw'
 * vnd.urbanairship.sample://deeplink/inbox?message_id=VJO7DpCEQ2i7LdqOxLbFxw
 */
public class ParseDeepLinkActivity extends AppCompatActivity {

    private static final String TAG = "ParseDeepLinkActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Log.e(TAG, "OnCreate Method of Deep Linking ");

        // Parse the deep link
        String deepLink = getDeepLink();
//        Log.e(TAG, "OnCreate Method of Deep Linking  Value : " + deepLink);

        if (deepLink == null) {
            finish();
            return;
        }

        switch (deepLink) {
            case "/HomeScreen":
                startActivity(new Intent(this, HomeScreen.class));
                break;
            case "/TopUpYourPhone":
                startActivity(new Intent(this, TopUpYourPhone.class));
                break;
            case "/InternationalTopUp":
                startActivity(new Intent(this, InternationalTopUp.class));
                break;
            case "/GameAndService":
                startActivity(new Intent(this, GameAndService.class));
                break;
            case "/LoginScreen":
                startActivity(new Intent(this, LoginScreen.class));
                break;
            case "/MyTransaction":
                startActivity(new Intent(this, MyTransactionActivity.class));
                break;
            case "/FavouriteScreen":
                startActivity(new Intent(this, FavouriteScreen.class));
                break;
            case "/Profile":
                startActivity(new Intent(this, EditProfile.class));
                break;
            case "/ContactUs":
                startActivity(new Intent(this, ContactUs.class));
                break;
            case "/BillingAddressScreen":
                startActivity(new Intent(this, BillingAddressScreen.class));
                break;
            case "/SignUpScreen":
                startActivity(new Intent(this, SignUpScreen.class));
                break;

            /*case "/inbox":
                // Check for an optional Message ID
                String messageId = getDeepLinkQueryParameter("message_id");
                if (messageId != null && messageId.length() > 0) {
                    UAirship.shared().getInbox().startMessageActivity(messageId);
                } else {
                    UAirship.shared().getInbox().startInboxActivity();
                }
                break;*/

            default:
                startActivity(new Intent(this, HomeScreen.class));
                break;
        }

        finish();
    }

    @Nullable
    private String getDeepLink() {
        Intent intent = getIntent();
        if (intent != null && intent.getData() != null) {
            return intent.getData().getPath();
        }

        return null;
    }

    private String getDeepLinkQueryParameter(String key) {
        Intent intent = getIntent();
        if (intent != null && intent.getData() != null) {
            return intent.getData().getQueryParameter(key);
        }

        return null;
    }
}
