package com.uk.recharge.kwikpay.catering.models;

import java.io.Serializable;

/**
 * Created by Ashu Rajput on 11/5/2018.
 */

public class ProductDistributorDetailsMO implements Serializable{

    private String merchantName;
    private String distributorHeadId;
    private String distributorName;
    private String distributorLogo;
    private String distributorAddress;
    private String distributorPostCode;
    private String distributorLocationNumber;
    private String distributorLocationName;
    private String distributorCity;

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorLogo() {
        return distributorLogo;
    }

    public void setDistributorLogo(String distributorLogo) {
        this.distributorLogo = distributorLogo;
    }

    public String getDistributorAddress() {
        return distributorAddress;
    }

    public void setDistributorAddress(String distributorAddress) {
        this.distributorAddress = distributorAddress;
    }

    public String getDistributorPostCode() {
        return distributorPostCode;
    }

    public void setDistributorPostCode(String distributorPostCode) {
        this.distributorPostCode = distributorPostCode;
    }

    public String getDistributorLocationNumber() {
        return distributorLocationNumber;
    }

    public void setDistributorLocationNumber(String distributorLocationNumber) {
        this.distributorLocationNumber = distributorLocationNumber;
    }

    public String getDistributorLocationName() {
        return distributorLocationName;
    }

    public void setDistributorLocationName(String distributorLocationName) {
        this.distributorLocationName = distributorLocationName;
    }

    public String getDistributorCity() {
        return distributorCity;
    }

    public void setDistributorCity(String distributorCity) {
        this.distributorCity = distributorCity;
    }

    public String getDistributorHeadId() {
        return distributorHeadId;
    }

    public void setDistributorHeadId(String distributorHeadId) {
        this.distributorHeadId = distributorHeadId;
    }
}
