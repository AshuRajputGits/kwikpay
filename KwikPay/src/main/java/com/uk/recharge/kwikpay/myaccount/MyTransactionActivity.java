package com.uk.recharge.kwikpay.myaccount;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.EditProfile;
import com.uk.recharge.kwikpay.FavouriteScreen;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 12/20/2017.
 */

public class MyTransactionActivity extends KPSuperClass implements AsyncRequestListenerViaPost {

    private JSONArray txnJsonArray;
    private ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.my_transaction_activity);
        createMenuDrawer(this);
        turnHomeButtonToBackButton();
        setHeaderScreenName("My Order");

        setUpIds();
        setUpTabModes();

        //Clearing previously stored array list of all modules data if exists
        MyTransactionSingleton.getInstance().clearAllArrayLists();

        new LoadResponseViaPost(this, formMyTransactionJSON(), true).execute("");

        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_MY_ACCOUNT));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.my_transaction_activity;
    }

    private void setUpIds() {

        viewPager = findViewById(R.id.pager);
        TextView myTransactionButton = findViewById(R.id.footerMyTxnBtn);
        myTransactionButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_transaction_icon, 0, 0);
        myTransactionButton.setTextColor(getResources().getColor(R.color.darkGreen));

        findViewById(R.id.footerEditProfileBtn).setOnClickListener(onClickListener);
        findViewById(R.id.footerFavoriteBtn).setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.footerFavoriteBtn)
                startActivity(new Intent(MyTransactionActivity.this, FavouriteScreen.class));
            else if (v.getId() == R.id.footerEditProfileBtn)
                startActivity(new Intent(MyTransactionActivity.this, EditProfile.class));
        }
    };

    public String formMyTransactionJSON() {
        try {
            SharedPreferences preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPTRANSACTIONHISTORY");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setUpTabModes() {

        TabLayout tabLayout = findViewById(R.id.tab_layout);
//        tabLayout.addTab(tabLayout.newTab().setText("All"));
        tabLayout.addTab(tabLayout.newTab().setText("Mobile"));
        tabLayout.addTab(tabLayout.newTab().setText("Digital"));
        tabLayout.addTab(tabLayout.newTab().setText("EV Charge"));
        tabLayout.addTab(tabLayout.newTab().setText("Eat & Drink"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager.setOffscreenPageLimit(4);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (parseMyTransactionDetails(loadedString)) {
                    if (viewPager != null)
                        viewPager.setAdapter(new MyTransactionPagerAdapter(getSupportFragmentManager()));
                }
            } else {
                this.finish();
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
            this.finish();
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    //PARSING TRANSACTION DETAILS DATA
    public boolean parseMyTransactionDetails(String myTransactionResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(myTransactionResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has(ProjectConstant.TXN_TRANSACTIONDETAIL)) {

                        // CHECKING WHETHER THE DATA INSIDE FAVOURITES TAG IS OBJECT OR JSON OBJECT
                        Object object = parentResponseObject.get(ProjectConstant.TXN_TRANSACTIONDETAIL);

                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            txnJsonArray = new JSONArray();
                            txnJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray) {
                            txnJsonArray = (JSONArray) object;
                        }

                        MyTransactionSingleton myTransactionSingleton = MyTransactionSingleton.getInstance();
//                        ArrayList<MyTransactionsMO> allTransactionList = new ArrayList<>();
                        ArrayList<MyTransactionsMO> mobileTransactionList = new ArrayList<>();
                        ArrayList<MyTransactionsMO> digitalTransactionList = new ArrayList<>();
                        ArrayList<MyTransactionsMO> kvChargeTransactionList = new ArrayList<>();

                        for (int i = 0; i < txnJsonArray.length(); i++) {
                            childResponseObject = txnJsonArray.getJSONObject(i);

                            MyTransactionsMO transactionsMO = new MyTransactionsMO();
                            transactionsMO.setTransactionDateTime(childResponseObject.optString(ProjectConstant.TXN_TRANSACTIONDATETIME));
                            transactionsMO.setTransactionServiceId(childResponseObject.optString(ProjectConstant.TXN_SERVICEID));
                            transactionsMO.setTransactionPaymentCurrencyCode(childResponseObject.optString(ProjectConstant.TXN_PAYMENTCURRENCYCODE));
                            transactionsMO.setTransactionRechargeCurrencyCode(childResponseObject.optString(ProjectConstant.TXN_RECHARGECURRENCYCODE));
                            transactionsMO.setTransactionOperatorName(childResponseObject.optString(ProjectConstant.TXN_OPERATORNAME));
                            transactionsMO.setTransactionPaymentAmount(childResponseObject.optString(ProjectConstant.TXN_PAYMENTAMOUNT));
                            transactionsMO.setTransactionPaymentStatus(childResponseObject.optString(ProjectConstant.TXN_PAYMENTSTATUS));
                            transactionsMO.setTransactionServiceName(childResponseObject.optString(ProjectConstant.TXN_SERVICENAME));
                            transactionsMO.setTransactionCountryCode(childResponseObject.optString(ProjectConstant.TXN_COUNTRYCODE));
                            transactionsMO.setTransactionDisplayAmount(childResponseObject.optString(ProjectConstant.TXN_DISPLAYAMOUNT));
                            transactionsMO.setTransactionOrderId(childResponseObject.optString(ProjectConstant.TXN_ORDERID));
                            transactionsMO.setTransactionOperatorCode(childResponseObject.optString(ProjectConstant.TXN_OPERATORCODE));
                            transactionsMO.setTransactionDisplayCurrencyCode(childResponseObject.optString(ProjectConstant.TXN_DISPLAYCURRENCYCODE));
                            transactionsMO.setTransactionRechargeStatus(childResponseObject.optString(ProjectConstant.TXN_RECHARGESTATUS));
                            transactionsMO.setTransactionRechargeAmount(childResponseObject.optString(ProjectConstant.TXN_RECHARGEAMOUNT));
                            transactionsMO.setTransactionSubscriptionNumber(childResponseObject.optString(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
                            transactionsMO.setTransactionFee(childResponseObject.optString("SERVICEFEE"));
                            transactionsMO.setTransactionRCPin(childResponseObject.optString("RECHARGEPIN"));
                            transactionsMO.setTransactionRCPinExpiryDate(childResponseObject.optString("RECHARGEPINEXPIRYDATE"));
                            transactionsMO.setTransactionCouponSrNumber(childResponseObject.optString("CPNSRNUM"));
                            transactionsMO.setTransactionCouponValue(childResponseObject.optString("CPNVALUE"));
                            transactionsMO.setTransactionDiscountAmount(childResponseObject.optString("DISCOUNT_AMOUNT"));

                            transactionsMO.setTransactionTotalEnergy(childResponseObject.optString("TOTAL_ENERGY"));
                            transactionsMO.setTransactionCostOfEnergy(childResponseObject.optString("COST_OF_ENERGY"));
                            transactionsMO.setTransactionRegNumber(childResponseObject.optString("REGISTRATION_NUMBER"));
                            transactionsMO.setTransactionLocationName(childResponseObject.optString("LOCATIONNAME"));
                            transactionsMO.setTransactionChargerId(childResponseObject.optString("CHARGERID"));
                            transactionsMO.setImageUrl(childResponseObject.optString("IMG_URL"));
                            if (childResponseObject.has("PLANDESCRIPTION"))
                                transactionsMO.setTransactionPlanDesc(childResponseObject.optString("PLANDESCRIPTION"));

//                            allTransactionList.add(transactionsMO);

                            if (childResponseObject.optString(ProjectConstant.TXN_SERVICEID).equals("1") ||
                                    childResponseObject.optString(ProjectConstant.TXN_SERVICEID).equals("2"))
                                mobileTransactionList.add(transactionsMO);
                            else if (childResponseObject.optString(ProjectConstant.TXN_SERVICEID).equals("3"))
                                digitalTransactionList.add(transactionsMO);
                            else if (childResponseObject.optString(ProjectConstant.TXN_SERVICEID).equals("4"))
                                kvChargeTransactionList.add(transactionsMO);
                        }

//                        myTransactionSingleton.setAllTransactionList(allTransactionList);
                        myTransactionSingleton.setMobileTransactionList(mobileTransactionList);
                        myTransactionSingleton.setDigitalTransactionList(digitalTransactionList);
                        myTransactionSingleton.setKvChargeTransactionList(kvChargeTransactionList);

                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, parentResponseObject.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
