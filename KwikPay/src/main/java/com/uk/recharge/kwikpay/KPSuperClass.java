package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.adapters.AdsViewPagerAdapter;
import com.uk.recharge.kwikpay.adapters.HeaderMenuAdapter;
import com.uk.recharge.kwikpay.myaccount.MyTransactionActivity;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by Ashu Rajput on 7/10/2018.
 */

public abstract class KPSuperClass extends AppCompatActivity {

    public ImageView homeButtonImage;
    public ImageView headerMenuImage;
    DrawerLayout mDrawerLayout;
    ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private Activity activityContext = null;
    private boolean isHomeButtonSwitchedToBackButton = false;
    protected SharedPreferences sharedPreferences;
    private AutoScrollViewPager viewPagerBanner = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        initiateSliderMenu();
    }

    public void initiateSliderMenu() {
        homeButtonImage = findViewById(R.id.headerHomeBtn);
        headerMenuImage = findViewById(R.id.headerMenuBtn);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerList = findViewById(R.id.left_drawer);
        sharedPreferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);
    }

    protected abstract int getLayoutResourceId();

    public void createMenuDrawer(final Activity activityContext) {

        this.activityContext = activityContext;

        if (activityContext instanceof KPSuperClass) {
            mDrawerLayout.setScrimColor(activityContext.getResources().getColor(android.R.color.transparent));
        } else {
            Drawable shadow = ContextCompat.getDrawable(activityContext, R.drawable.drawer_shadow);
            mDrawerLayout.setDrawerShadow(shadow, GravityCompat.START);
        }

        HeaderMenuAdapter adapter = new HeaderMenuAdapter(activityContext);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(activityContext, mDrawerLayout, R.drawable.menu_icon, R.string.drawer1, R.string.drawer2) {
            public void onDrawerClosed(View view) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

            public void onDrawerOpened(View drawerView) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        headerMenuImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    final InputMethodManager imm = (InputMethodManager) activityContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(arg0.getWindowToken(), 0);

                    mDrawerLayout.openDrawer(mDrawerList);
                    mDrawerLayout.bringToFront();
                }
            }
        });

        if (homeButtonImage != null) {
            homeButtonImage.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("InlinedApi")
                @Override
                public void onClick(View v) {
                    if (isHomeButtonSwitchedToBackButton) {
                        activityContext.finish();
                    } else {
                        Intent intent = new Intent(activityContext, HomeScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activityContext.startActivity(intent);
                        activityContext.finish();
                    }
                }
            });
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @SuppressLint("InlinedApi")
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    if (activityContext instanceof HomeScreen)
                        mDrawerLayout.closeDrawers();
                    else {
                        Intent intent = new Intent(activityContext, HomeScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activityContext.startActivity(intent);
                        activityContext.finish();
                    }
                    break;

                case 1:

                    if (sharedPreferences.getString("KP_USER_ID", "").equals("0")) {
                        ProjectConstant.ISITCOMINGFROM_MYACCOUNT = true;
                        activityContext.startActivity(new Intent(activityContext, LoginScreen.class));
                    } else {
                        if (activityContext instanceof MyTransactionActivity)
                            mDrawerLayout.closeDrawers();
                        else {
                            activityContext.startActivity(new Intent(activityContext, MyTransactionActivity.class));
                        }
                    }
                    break;

                case 2:
                    if (activityContext instanceof CommonStaticPages)
                        mDrawerLayout.closeDrawers();
                    else {
                        activityContext.startActivity(new Intent(activityContext, CommonStaticPages.class).putExtra("STATIC_PAGE_NAME", "HOWITWORK_PAGE"));
                    }
                    break;

                case 3:
                    if (activityContext instanceof ContactUs)
                        mDrawerLayout.closeDrawers();
                    else {
                        activityContext.startActivity(new Intent(activityContext, ContactUs.class));
                    }
                    break;

                case 4:
                    if (activityContext instanceof TermsOfUsage)
                        mDrawerLayout.closeDrawers();
                    else {
                        activityContext.startActivity(new Intent(activityContext, TermsOfUsage.class));
                    }
                    break;

                case 5:
                    if (activityContext instanceof AboutUs)
                        mDrawerLayout.closeDrawers();
                    else {
                        Intent aboutUsIntent = new Intent(activityContext, AboutUs.class);
                        aboutUsIntent.putExtra("SCREEN_NAME", "");
                        activityContext.startActivity(aboutUsIntent);
                    }
                    break;

                case 6:
                    if (activityContext instanceof CommonStaticPages)
                        mDrawerLayout.closeDrawers();
                    else {
                        Intent privacyPolicy = new Intent(activityContext, CommonStaticPages.class);
                        privacyPolicy.putExtra("STATIC_PAGE_NAME", "PRIVACY_POLICY");
                        activityContext.startActivity(privacyPolicy);
                    }
                    break;

                default:
                    break;
            }

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    public void initiateAppsBanner(String moduleType) {
        try {
            viewPagerBanner = findViewById(R.id.viewPagerBanner);
            AdsViewPagerAdapter adsViewPagerAdapter = new AdsViewPagerAdapter(this, moduleType);
            viewPagerBanner.startAutoScroll(4500);
            viewPagerBanner.setInterval(4500);
            viewPagerBanner.setAdapter(adsViewPagerAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initiateHomeAppsBanner(String moduleType) {
        try {
            viewPagerBanner = findViewById(R.id.viewPagerBanner);
            AdsViewPagerAdapter adsViewPagerAdapter = new AdsViewPagerAdapter(this, moduleType);
            viewPagerBanner.startAutoScroll(5000);
            viewPagerBanner.setInterval(5000);

            viewPagerBanner.setClipToPadding(false);
//            viewPagerBanner.setPadding(0,0,100,0);
            viewPagerBanner.setPageMargin(-150);
            viewPagerBanner.setAdapter(adsViewPagerAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHeaderScreenName(String screenName) {
        try {
            TextView headerScreeName = findViewById(R.id.mainHeaderTitleName);
            headerScreeName.setText(screenName);
        } catch (Exception e) {
        }
    }

    public void turnHomeButtonToBackButton() {
        isHomeButtonSwitchedToBackButton = true;
    }

    public void disableHomeAndMenuIcon() {
        homeButtonImage.setEnabled(false);
        headerMenuImage.setEnabled(false);
    }

    public void enableHomeAndMenuIcon() {
        homeButtonImage.setEnabled(true);
        headerMenuImage.setEnabled(true);
    }

    //Note: Always call below method after calling "initiateAppsBanner()"
    public void setDynamicWidthHeightToView() {

        if (viewPagerBanner != null) {
            try {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                ViewGroup.LayoutParams layoutParams = viewPagerBanner.getLayoutParams();
                layoutParams.height = ((displayMetrics.widthPixels) / 2) + ((displayMetrics.widthPixels) / 6);
                viewPagerBanner.setLayoutParams(layoutParams);
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }

    //Note: Always call below method after calling "initiateAppsBanner()"
    public void setDynamicWidthHeightToHomeView() {

        if (viewPagerBanner != null) {
            try {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                ViewGroup.LayoutParams layoutParams = viewPagerBanner.getLayoutParams();
                layoutParams.height = ((displayMetrics.widthPixels) / 2) + ((displayMetrics.widthPixels) / 6) - 150;
                viewPagerBanner.setLayoutParams(layoutParams);
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }

    public void setDynamicWidthHeightToImageView(ImageView singleBannerIV) {

        try {
            if (singleBannerIV != null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

                ViewGroup.LayoutParams layoutParams = singleBannerIV.getLayoutParams();
                layoutParams.height = ((displayMetrics.widthPixels) / 2) + ((displayMetrics.widthPixels) / 6);
//                layoutParams.height = ((displayMetrics.widthPixels) / 2) ;
                singleBannerIV.setLayoutParams(layoutParams);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
