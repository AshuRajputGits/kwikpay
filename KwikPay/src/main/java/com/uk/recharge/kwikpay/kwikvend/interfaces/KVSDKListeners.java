package com.uk.recharge.kwikpay.kwikvend.interfaces;

/**
 * Created by Ashu Rajput on 10/19/2018.
 */

public interface KVSDKListeners {

    void onGettingFailureResponse(String failureResponse);

    void onGettingProductList(String productList);

    void onGettingSequenceNo(String sequenceNo);

    void onGettingDispenseStatus(String dispenseStatus);

}
