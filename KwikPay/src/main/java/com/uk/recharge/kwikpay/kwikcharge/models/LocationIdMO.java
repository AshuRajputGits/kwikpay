package com.uk.recharge.kwikpay.kwikcharge.models;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 10/6/2017.
 */

public class LocationIdMO {

    private String userName;
    private String operatorName;
    private String operatorLogo;
    private String operatorCode;
    private String locationNumber;
    private String addressOne;
    private String addressTwo;
    private String addressThree;
    private String country;
    private String city;
    private String postCode;
    private String latitude;
    private String longitude;

    // User and Operator related information
    private String serviceId;
    private String AC;
    private String DC;
    private String CHADEMO;
    private String userPostCode;
    private String registrationNumber;
    private String recentMake;
    private String recentModel;
    private String recentYear;
    private String recentBatteryType;
    private String recentChargerType;
    private String recentMaxAmount;
    private String recentMaxTime;
    private String recentRate;
    private String connectorType;
    private String connector1Desc;
    private String connector2Desc;
    private String connector3Desc;
    private String connectorDescSelected;
    private String providerTnCUrl;
//    private String connectorImageType;
    private String companyDescription;
    private String authorisingAmount;
    private String serviceFee;
    private String evRateScreenBanner;
    private String selectedConnectorName;
    private String selectedConnectorURL;

    private String interimScreenFlow;
    private String interimScreenText;

    private ArrayList<ConnectorListMO> connectorListMOList;

    //------------TEMPORARY GETTER AND SETTER USED IN KC CHARGING TIME SCREEN CLASS--------------//
    private String tempDeviceDBID;
    private String tempLocalConnectorNumber;
    private String tempAppTxnId;

    public String getEvRateScreenBanner() {
        return evRateScreenBanner;
    }

    public void setEvRateScreenBanner(String evRateScreenBanner) {
        this.evRateScreenBanner = evRateScreenBanner;
    }

    /*public String getConnectorImageType() {
        return connectorImageType;
    }

    public void setConnectorImageType(String connectorImageType) {
        this.connectorImageType = connectorImageType;
    }*/

    public String getProviderTnCUrl() {
        return providerTnCUrl;
    }

    public void setProviderTnCUrl(String providerTnCUrl) {
        this.providerTnCUrl = providerTnCUrl;
    }

    public String getConnectorDescSelected() {
        return connectorDescSelected;
    }

    public void setConnectorDescSelected(String connectorDescSelected) {
        this.connectorDescSelected = connectorDescSelected;
    }

    public String getConnector1Desc() {
        return connector1Desc;
    }

    public void setConnector1Desc(String connector1Desc) {
        this.connector1Desc = connector1Desc;
    }

    public String getConnector2Desc() {
        return connector2Desc;
    }

    public void setConnector2Desc(String connector2Desc) {
        this.connector2Desc = connector2Desc;
    }

    public String getConnector3Desc() {
        return connector3Desc;
    }

    public void setConnector3Desc(String connector3Desc) {
        this.connector3Desc = connector3Desc;
    }

    public String getConnectorType() {
        return connectorType;
    }

    public void setConnectorType(String connectorType) {
        this.connectorType = connectorType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOperatorLogo() {
        return operatorLogo;
    }

    public void setOperatorLogo(String operatorLogo) {
        this.operatorLogo = operatorLogo;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public String getAddressOne() {
        return addressOne;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public String getAddressTwo() {
        return addressTwo;
    }

    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    public String getAddressThree() {
        return addressThree;
    }

    public void setAddressThree(String addressThree) {
        this.addressThree = addressThree;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getAC() {
        return AC;
    }

    public void setAC(String AC) {
        this.AC = AC;
    }

    public String getDC() {
        return DC;
    }

    public void setDC(String DC) {
        this.DC = DC;
    }

    public String getCHADEMO() {
        return CHADEMO;
    }

    public void setCHADEMO(String CHADEMO) {
        this.CHADEMO = CHADEMO;
    }

    public String getUserPostCode() {
        return userPostCode;
    }

    public void setUserPostCode(String userPostCode) {
        this.userPostCode = userPostCode;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRecentMake() {
        return recentMake;
    }

    public void setRecentMake(String recentMake) {
        this.recentMake = recentMake;
    }

    public String getRecentModel() {
        return recentModel;
    }

    public void setRecentModel(String recentModel) {
        this.recentModel = recentModel;
    }

    public String getRecentYear() {
        return recentYear;
    }

    public void setRecentYear(String recentYear) {
        this.recentYear = recentYear;
    }

    public String getRecentBatteryType() {
        return recentBatteryType;
    }

    public void setRecentBatteryType(String recentBatteryType) {
        this.recentBatteryType = recentBatteryType;
    }

    public String getRecentChargerType() {
        return recentChargerType;
    }

    public void setRecentChargerType(String recentChargerType) {
        this.recentChargerType = recentChargerType;
    }

    public String getRecentMaxAmount() {
        return recentMaxAmount;
    }

    public void setRecentMaxAmount(String recentMaxAmount) {
        this.recentMaxAmount = recentMaxAmount;
    }

    public String getRecentMaxTime() {
        return recentMaxTime;
    }

    public void setRecentMaxTime(String recentMaxTime) {
        this.recentMaxTime = recentMaxTime;
    }

    public String getRecentRate() {
        return recentRate;
    }

    public void setRecentRate(String recentRate) {
        this.recentRate = recentRate;
    }


    public String getTempDeviceDBID() {
        return tempDeviceDBID;
    }

    public void setTempDeviceDBID(String tempDeviceDBID) {
        this.tempDeviceDBID = tempDeviceDBID;
    }

    public String getTempLocalConnectorNumber() {
        return tempLocalConnectorNumber;
    }

    public void setTempLocalConnectorNumber(String tempLocalConnectorNumber) {
        this.tempLocalConnectorNumber = tempLocalConnectorNumber;
    }

    public String getTempAppTxnId() {
        return tempAppTxnId;
    }

    public void setTempAppTxnId(String tempAppTxnId) {
        this.tempAppTxnId = tempAppTxnId;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public String getAuthorisingAmount() {
        return authorisingAmount;
    }

    public void setAuthorisingAmount(String authorisingAmount) {
        this.authorisingAmount = authorisingAmount;
    }

    public String getInterimScreenFlow() {
        return interimScreenFlow;
    }

    public void setInterimScreenFlow(String interimScreenFlow) {
        this.interimScreenFlow = interimScreenFlow;
    }

    public String getInterimScreenText() {
        return interimScreenText;
    }

    public void setInterimScreenText(String interimScreenText) {
        this.interimScreenText = interimScreenText;
    }

    public ArrayList<ConnectorListMO> getConnectorListMOList() {
        return connectorListMOList;
    }

    public void setConnectorListMOList(ArrayList<ConnectorListMO> connectorListMOList) {
        this.connectorListMOList = connectorListMOList;
    }

    public String getSelectedConnectorName() {
        return selectedConnectorName;
    }

    public void setSelectedConnectorName(String selectedConnectorName) {
        this.selectedConnectorName = selectedConnectorName;
    }

    public String getSelectedConnectorURL() {
        return selectedConnectorURL;
    }

    public void setSelectedConnectorURL(String selectedConnectorURL) {
        this.selectedConnectorURL = selectedConnectorURL;
    }

    public String getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(String serviceFee) {
        this.serviceFee = serviceFee;
    }
}
