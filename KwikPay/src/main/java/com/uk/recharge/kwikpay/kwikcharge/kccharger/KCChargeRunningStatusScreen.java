package com.uk.recharge.kwikpay.kwikcharge.kccharger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ashu Rajput on 12/17/2017.
 */

public class KCChargeRunningStatusScreen extends AppCompatActivity implements AsyncRequestListenerViaPost {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_charge_running_status_screen);

        findViewById(R.id.kcTimeFailurePressStartButton).setOnClickListener(onClickListener);
        findViewById(R.id.kcTimeFailureCancelChargeButton).setOnClickListener(onClickListener);
        findViewById(R.id.headerHomeBtn).setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.kcTimeFailurePressStartButton) {
                new LoadResponseViaPost(KCChargeRunningStatusScreen.this, formKCStartChargingJson(), true).execute("");
            } else if (v.getId() == R.id.kcTimeFailureCancelChargeButton) {
                goToHomeScreen();
            } else if (v.getId() == R.id.headerHomeBtn) {
                goToHomeScreen();
            }

        }
    };

    private void goToHomeScreen() {
        Intent intent = new Intent(KCChargeRunningStatusScreen.this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public String formKCStartChargingJson() {
        JSONObject jsonObj = new JSONObject();
        SharedPreferences preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        try {
            jsonObj.put("APISERVICE", "KC_START_CHARGE");
            jsonObj.put("USERID", preference.getString("KP_USER_ID", ""));
            jsonObj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonObj.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonObj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonObj.put("LOCATION_NUMBER", preference.getString("TEMP_LOCATION_NUMBER", ""));
            jsonObj.put("DEVICE_DB_ID", preference.getString("TEMP_DEVICE_DB_ID", ""));
            jsonObj.put("LOCAL_CONNECTOR_NUMBER", preference.getString("TEMP_LOCAL_CONNECTOR_NUMBER", ""));
            jsonObj.put("APP_TXN_ID", preference.getString("TEMP_APP_TXN_ID", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj.toString();
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                parseStartChargingDetails(loadedString);
            } else {
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    private void parseStartChargingDetails(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    startActivity(new Intent(this, KCChargeStepProcessScreen.class)
                            .putExtra("START_STATUS_TIMER_IN_SECONDS", childJsonObj.optString("START_STATUS_TIMER_IN_SECONDS")));
                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, childJsonObj.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (Exception e) {
            ARCustomToast.showToast(this, "Parsing exception", Toast.LENGTH_LONG);
        }
    }
}
