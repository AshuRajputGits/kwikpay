package com.uk.recharge.kwikpay.myaccount;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.OrderDetails;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RaiseTicket;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Ashu Rajput on 12/23/2017.
 */

public class MyTransactionViewDetails extends AppCompatActivity implements AsyncRequestListenerViaPost {

    private MyTransactionsMO myTransactionsMO = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mytransaction_view_details);
        setUpIds();
    }

    private void setUpIds() {

        TextView txnDetailsRepeatButton = findViewById(R.id.txnDetailsRepeatButton);
        TextView txnMobileOrRegNo = findViewById(R.id.txnDetailsMobileNum);
        TextView txnPinOrTotEnergy = findViewById(R.id.txnDetailsPinNum);
        TextView txnDetailsPlanDesc = findViewById(R.id.txnDetailsPlanDesc);

        findViewById(R.id.headerBackButton).setOnClickListener(onClickListener);
        findViewById(R.id.txnDetailsContactButton).setOnClickListener(onClickListener);
        txnDetailsRepeatButton.setOnClickListener(onClickListener);

        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("SELECTED_TRANSACTION")) {
                myTransactionsMO = (MyTransactionsMO) receivedBundle.getSerializable("SELECTED_TRANSACTION");
                if (myTransactionsMO != null) {

                    ((TextView) findViewById(R.id.txnDetailsOrderID)).setText("Order Number\n" + myTransactionsMO.getTransactionOrderId());
                    try {
                        String[] timeSplitter = myTransactionsMO.getTransactionDateTime().split(" ");
                        ((TextView) findViewById(R.id.txnDetailsDateTime)).setText(timeSplitter[0] + "\n" + timeSplitter[1]);
                    } catch (Exception e) {
                    }
                    ((TextView) findViewById(R.id.txnDetailsOperatorName)).setText(myTransactionsMO.getTransactionOperatorName());
                    ((TextView) findViewById(R.id.txnDetailsRCAmount)).setText(Utility.fromHtml(myTransactionsMO.getTransactionRechargeCurrencyCode())
                            + myTransactionsMO.getTransactionRechargeAmount());
                    ((TextView) findViewById(R.id.txnDetailsTransactionFee)).setText(Utility.fromHtml(myTransactionsMO.getTransactionDisplayCurrencyCode())
                            + myTransactionsMO.getTransactionFee());
                    ((TextView) findViewById(R.id.txnDetailsDiscount)).setText(Utility.fromHtml(myTransactionsMO.getTransactionDisplayCurrencyCode())
                            + myTransactionsMO.getTransactionDiscountAmount());
                    ((TextView) findViewById(R.id.txnDetailsTotalAmount)).setText(Utility.fromHtml(myTransactionsMO.getTransactionPaymentCurrencyCode())
                            + myTransactionsMO.getTransactionPaymentAmount());

                    ImageView txnDetailsRCStatusIcon = findViewById(R.id.txnDetailsRCStatusIcon);

                    if (myTransactionsMO.getTransactionRechargeStatus().equalsIgnoreCase("SUCCESSFUL"))
                        txnDetailsRCStatusIcon.setImageResource(R.drawable.green_tick_small);
                    else if (myTransactionsMO.getTransactionRechargeStatus().equalsIgnoreCase("FAILURE"))
                        txnDetailsRCStatusIcon.setImageResource(R.drawable.red_cross);
                    else
                        txnDetailsRCStatusIcon.setImageResource(R.drawable.exclamation);

                    ImageView txnDetailsOperatorLogo = findViewById(R.id.txnDetailsOperatorLogo);
                    try {
                        Glide.with(this).load(myTransactionsMO.getImageUrl())
                                .placeholder(R.drawable.default_operator_logo)
                                .into(txnDetailsOperatorLogo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    TextView txnRepeatRCLabel = findViewById(R.id.txnRepeatRCLabel);
                    TextView txnEVRepeatRCDetails = findViewById(R.id.txnEVRepeatRCDetails);

                    if (!myTransactionsMO.getTransactionRCPin().isEmpty() && !myTransactionsMO.getTransactionRCPin().equalsIgnoreCase("N/A")) {
                        txnPinOrTotEnergy.setVisibility(View.VISIBLE);
                        txnPinOrTotEnergy.setText("Pin:    " + myTransactionsMO.getTransactionRCPin());
                    }

                    if (myTransactionsMO.getTransactionServiceId().equals("4")) {

                        txnMobileOrRegNo.setText("Registration: " + myTransactionsMO.getTransactionRegNumber());
                        txnPinOrTotEnergy.setText("Total energy consumed: " + myTransactionsMO.getTransactionTotalEnergy() + " kWh");

                        txnRepeatRCLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.location_icon, 0, 0, 0);
                        txnRepeatRCLabel.setText("Charger Location");

                        String evDetails = "ID: " + myTransactionsMO.getTransactionChargerId() + "\n" + myTransactionsMO.getTransactionLocationName();
                        txnEVRepeatRCDetails.setText(evDetails);
                        txnDetailsRepeatButton.setVisibility(View.GONE);
                    } else if (myTransactionsMO.getTransactionServiceId().equals("3")) {
                        txnMobileOrRegNo.setVisibility(View.GONE);
                    } else {
                        txnMobileOrRegNo.setText("Mobile: " + myTransactionsMO.getTransactionSubscriptionNumber());
                    }

                    if (myTransactionsMO.getTransactionPlanDesc() != null && !myTransactionsMO.getTransactionPlanDesc().isEmpty()) {
                        txnDetailsPlanDesc.setText(Html.fromHtml(myTransactionsMO.getTransactionPlanDesc()));
                        txnDetailsPlanDesc.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.headerBackButton)
                MyTransactionViewDetails.this.finish();
            else if (v.getId() == R.id.txnDetailsRepeatButton) {
                new LoadResponseViaPost(MyTransactionViewDetails.this, formRepeatRechargeJSON(), true).execute("");
            } else if (v.getId() == R.id.txnDetailsContactButton) {

                if (myTransactionsMO != null) {
                    Intent raiseTicketIntent = new Intent(MyTransactionViewDetails.this, RaiseTicket.class);
                    raiseTicketIntent.putExtra("RAISETICKET_ORDERID", myTransactionsMO.getTransactionOrderId());
                    raiseTicketIntent.putExtra("RAISETICKET_SERVICE_PROVIDER", myTransactionsMO.getTransactionOperatorName());
                    raiseTicketIntent.putExtra("RAISETICKET_DOT", myTransactionsMO.getTransactionDateTime());
                    startActivity(raiseTicketIntent);
                }
            }
        }
    };


    public String formRepeatRechargeJSON() {
        try {
            SharedPreferences preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPREPEATRECHARGE");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            if (myTransactionsMO != null)
                json.put("ORDERID", myTransactionsMO.getTransactionOrderId());
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {

        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (myTransactionsMO != null && myTransactionsMO.getTransactionServiceId().equals("4")) {
                    parseKCRepeatRechargeDetails(loadedString);
                } else {
                    parseRepeatRechargeDetail(loadedString);
                }
            } else
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        } else
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
    }

    private void parseKCRepeatRechargeDetails(String loadedString) {

        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(loadedString);
            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }

        } catch (JSONException e) {
        }
    }

    //PARSING REPEAT RECHARGE DETAILS DATA
    public boolean parseRepeatRechargeDetail(String repeatRCResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(repeatRCResponse);
            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    HashMap<String, String> repeatRechargeHashMap = new HashMap<>();

                    if (parentResponseObject.has(ProjectConstant.TXN_DISPLAYAMOUNT))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, parentResponseObject.getString(ProjectConstant.TXN_DISPLAYAMOUNT).toString());
                    else {
                        if (myTransactionsMO != null)
                            repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, myTransactionsMO.getTransactionDisplayAmount());
                    }

                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID).toString());
                    if (parentResponseObject.has("DISPLAYAMOUNTCURRENCYCODE"))
                        repeatRechargeHashMap.put("DISPLAYAMOUNTCURRENCYCODE", parentResponseObject.getString("DISPLAYAMOUNTCURRENCYCODE").toString());
                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGEAMT))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGEAMT, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGEAMT).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_SERVICEID))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICEID, parentResponseObject.getString(ProjectConstant.TXN_SERVICEID).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_OPERATORNAME))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_OPERATORNAME, parentResponseObject.getString(ProjectConstant.TXN_OPERATORNAME).toString());
                    if (parentResponseObject.has("EMAILID"))
                        repeatRechargeHashMap.put("EMAILID", parentResponseObject.getString("EMAILID").toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PROCESSINGFEE))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_PROCESSINGFEE).toString());
                    if (parentResponseObject.has("PGAMT"))
                        repeatRechargeHashMap.put("PGAMT", parentResponseObject.getString("PGAMT").toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_PAYMENTCURRENCYCODE))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, parentResponseObject.getString(ProjectConstant.TXN_PAYMENTCURRENCYCODE).toString());

                    if (parentResponseObject.has(ProjectConstant.TXN_SERVICENAME))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICENAME, parentResponseObject.getString(ProjectConstant.TXN_SERVICENAME).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_COUNTRYCODE))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_COUNTRYCODE, parentResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE).toString());
                    if (parentResponseObject.has("MOBILENUMBER"))
                        repeatRechargeHashMap.put("MOBILENUMBER", parentResponseObject.getString("MOBILENUMBER").toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_SERVICEFEE))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_SERVICEFEE).toString());
                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID).toString());
                    if (parentResponseObject.has(ProjectConstant.API_OPCODE))
                        repeatRechargeHashMap.put(ProjectConstant.API_OPCODE, parentResponseObject.getString(ProjectConstant.API_OPCODE).toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PLANDESCRIPTION))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, parentResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION).toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, parentResponseObject.getString(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID).toString());

                    if (parentResponseObject.has("URLTERMS"))
                        ProjectConstant.tncURL = parentResponseObject.getString("URLTERMS");
                    if (parentResponseObject.has("URLPROVIDERHELP"))
                        ProjectConstant.providerDetailsURL = parentResponseObject.getString("URLPROVIDERHELP");

                    if (parentResponseObject.has("RECHARGEPIN"))
                        repeatRechargeHashMap.put("RECHARGEPIN", parentResponseObject.getString("RECHARGEPIN").toString());
                    if (parentResponseObject.has("RECHARGEPINEXPIRYDATE"))
                        repeatRechargeHashMap.put("RECHARGEPINEXPIRYDATE", parentResponseObject.getString("RECHARGEPINEXPIRYDATE").toString());
                    if (parentResponseObject.has("CPNSRNUM"))
                        repeatRechargeHashMap.put("CPNSRNUM", parentResponseObject.getString("CPNSRNUM").toString());
                    if (parentResponseObject.has("CPNVALUE"))
                        repeatRechargeHashMap.put("CPNVALUE", parentResponseObject.getString("CPNVALUE").toString());

                    if(parentResponseObject.getString(ProjectConstant.TXN_SERVICEID).equals("3")){
                        if (parentResponseObject.has("PRODUCT_IMG"))
                            repeatRechargeHashMap.put(ProjectConstant.API_OPIMGPATH, parentResponseObject.getString("PRODUCT_IMG").toString());
                    }else
                    {
                        if (parentResponseObject.has("IMG_URL"))
                            repeatRechargeHashMap.put(ProjectConstant.API_OPIMGPATH, parentResponseObject.getString("IMG_URL").toString());
                    }

                    repeatRechargeHashMap.put(ProjectConstant.API_OPID, "");
                    repeatRechargeHashMap.put("AGID", "");
                    repeatRechargeHashMap.put("OPERATORPRODUCTCODE", parentResponseObject.optString("OPERATORPRODUCTCODE"));
                    if (myTransactionsMO != null)
                        repeatRechargeHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, myTransactionsMO.getTransactionRechargeAmount());

                    //STARTING ORDER DETAIL SCREEN FROM VIEW DETAIL SCREEN
                    Intent payNowIntent = new Intent(this, OrderDetails.class);
                    payNowIntent.putExtra("PAYNOW_DETAILS_VIA_RR_HASHMAP", repeatRechargeHashMap);
                    startActivity(payNowIntent);

                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            return false;
        }
        return true;
    }

}
