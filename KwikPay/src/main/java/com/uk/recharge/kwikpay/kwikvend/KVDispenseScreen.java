package com.uk.recharge.kwikpay.kwikvend;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikvend.interfaces.KVSDKListeners;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductDetailsMO;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;
import com.vendekin.vendekinlib.VendekinSdk;
import com.vendekin.vendekinlib.models.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashu Rajput on 7/13/2018.
 */

public class KVDispenseScreen extends KPSuperClass implements KVSDKListeners {

    private String[] receivedSeqArray;
    int productDispenseCounter = 0;
    private VendekinSdk vendekinSdk = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        setHeaderScreenName("Your vend");
        setOrderDataToVendekinSDK();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.kv_dispense_screen;
    }

    private void setOrderDataToVendekinSDK() {

        String generatedTxnId = getIntent().getExtras().getString("PG_APP_TXN_ID_KEY", "");

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this, "Processing items,please wait...");
        try {
            if (vendekinSdk != null)
                vendekinSdk.setOrderData(createListProduct(), generatedTxnId,
                        KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getTotalProductAmount(),
                        KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getTotalCartAmount(),
                        getSharedPreferences("KP_Preference", MODE_PRIVATE).getString("KP_USER_ID", ""),
                        "0");
            else {
                Log.e("KwikVend", "Getting null values of VENDEKIN SDK in Dispense Screen----");
            }

        } catch (Exception e) {
            KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
            Crashlytics.logException(e);
        }
    }

    private List<Product> createListProduct() {

        List<Product> productList = new ArrayList<>();
        ArrayList<KVProductDetailsMO> productDetailList = KVProductDetailsSingleton.getInstance().getKvProductDetailsMOList();

        if (productDetailList != null && productDetailList.size() > 0) {

            for (KVProductDetailsMO models : productDetailList) {
                Product product = new Product();
                product.setAddedCount(models.getProductQuantity());
                product.setAmount(String.valueOf(models.getProductPrice()));
                product.setCore_price(models.getProductCorePrice());
                product.setProductId(models.getProductId());
                product.setProduct_qty(models.getProductQuantity());
                productList.add(product);
            }
        }
        return productList;
    }

    /*@Override
    protected void onGettingVendProductList(String s, String s1, String s2, String s3) {

    }

    @Override
    protected void onGettingFailureResponse(String failResponse) {
        Log.e("VendKinLogs", "onSeq Failure -- " + failResponse);
    }*/

    /*@Override
    protected void onGettingSequenceArray(String sequenceNo) {
    }*/

    @Override
    public void onGettingFailureResponse(String failureResponse) {

    }

    @Override
    public void onGettingProductList(String productList) {

    }

    @Override
    public void onGettingSequenceNo(String sequenceNo) {
        Log.e("VendKinLogs", "onSeq -- " + sequenceNo);
        if (sequenceNo != null && !sequenceNo.isEmpty()) {
            KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
            try {
                Gson gson = new Gson();
                receivedSeqArray = gson.fromJson(sequenceNo, String[].class);
                productDispenseProcessor();
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }

    @Override
    public void onGettingDispenseStatus(String dispenseStatus) {
        Log.e("VendKinLogs", "onDispenseStatus -- " + dispenseStatus);
        productDispenseProcessor();
    }

    /*@Override
    protected void onGettingDispenseStatus(String dispenseStatus) {

    }*/

    private void productDispenseProcessor() {

        Log.e("VendKinLogs", "Dispense Counter = " + productDispenseCounter + ", Total Length" + receivedSeqArray.length);

        if (productDispenseCounter < receivedSeqArray.length) {
            try {
                productDispenseCounter = productDispenseCounter + 1;
                vendekinSdk.dispense(receivedSeqArray[productDispenseCounter - 1]);
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        } else {
            vendekinSdk.disconnect();
            Log.e("VendKinLogs", "Dispense Counter = ALL DONE Transaction");
        }
    }
}
