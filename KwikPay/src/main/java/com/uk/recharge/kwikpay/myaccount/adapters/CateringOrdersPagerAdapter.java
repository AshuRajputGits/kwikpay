package com.uk.recharge.kwikpay.myaccount.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.uk.recharge.kwikpay.myaccount.fragments.CatOnGoingOrderFragment;
import com.uk.recharge.kwikpay.myaccount.fragments.CatPastOrderFragment;

/**
 * Created by Ashu Rajput on 11/5/2018.
 */

public class CateringOrdersPagerAdapter extends FragmentStatePagerAdapter {

    public CateringOrdersPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            CatOnGoingOrderFragment catOnGoingOrderFragment = new CatOnGoingOrderFragment();
            return catOnGoingOrderFragment;
        } else if (position == 1) {
            CatPastOrderFragment catPostOrderFragment = new CatPastOrderFragment();
            return catPostOrderFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}

