package com.uk.recharge.kwikpay.adapters.catering;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.catering.CustomizeOrder;
import com.uk.recharge.kwikpay.models.CartProduct;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import static com.uk.recharge.kwikpay.catering.CateringCart.cartProductId;
import static com.uk.recharge.kwikpay.catering.CateringCart.cartProducts;
import static com.uk.recharge.kwikpay.catering.CateringCart.logCart;
import static com.uk.recharge.kwikpay.catering.CateringCart.setPaymentTotal;
import static com.uk.recharge.kwikpay.catering.CateringCart.updateCartTotal;
import static com.uk.recharge.kwikpay.catering.MyOrders.billamount;
import static com.uk.recharge.kwikpay.catering.MyOrders.itemtotal;
import static com.uk.recharge.kwikpay.catering.RestoMenuList.cartCountTextView;
import static com.uk.recharge.kwikpay.catering.RestoMenuList.cartTotalTextView;
import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;
import static com.uk.recharge.kwikpay.utilities.Utility.ParseFloat;
import static com.uk.recharge.kwikpay.utilities.Utility.ParseInteger;

public class CartProductsAdapter extends RecyclerView.Adapter<CartProductsAdapter.MyViewHolder> {
    private AppDatabase appDatabase;
    private Context context;

    public CartProductsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cart_products, parent, false);

        appDatabase = Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final CartProduct cartProduct = cartProducts.get(position);
        final List<Ingredient> ingredientList = appDatabase.ingredientDao().getAll(cartProduct.getCategory());

        holder.product_name.setText(cartProduct.getBrand_name());
        holder.prod_quantity.setText(String.valueOf(cartProduct.getSelected_quantity()));
        holder.product_price.setText(getPrice(cartProduct));
        holder.product_description.setText(getDescription(cartProduct));


        if (ingredientList.isEmpty())
            holder.customize.setVisibility(View.GONE);

        holder.add_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addQty(position, holder.add_quantity, holder);

            }
        });

        holder.remove_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeQty(position, holder.remove_quantity, holder);


            }
        });


        if (ingredientList.isEmpty())
            holder.customize.setVisibility(View.GONE);

        holder.customize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ingredientList.isEmpty()) {
                    Intent intent = new Intent(context, CustomizeOrder.class);
                    intent.putExtra("parent_id", cartProduct.getCategory());
                    intent.putExtra("product_type", cartProduct.getBrand_name());
                    intent.putExtra("id", cartProduct.getId());
                    intent.putExtra("killScreen", true);
                    context.startActivity(intent);
                }


            }
        });

        //   holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return cartProducts.size();
    }


    private String getPrice(CartProduct cartProduct) {

        float total_price = ParseFloat(cartProduct.getUnit_price()) * cartProduct.getSelected_quantity();

        List<Ingredient> ingredientList = cartProduct.getIngredientList();

        HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();

        for (final Ingredient ingredient : ingredientList) {

            List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
            List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());

            for (IngredientType ingredientType : ingredientTypeList) {
                int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                if (checked == 1) {
                    total_price += ParseFloat(ingredientType.getUnit_price()) * cartProduct.getSelected_quantity();
                }
            }
        }

        return "£ " + new DecimalFormat("##.00").format(total_price);
    }


    private String getDescription(CartProduct cartProduct) {
        StringBuilder description = new StringBuilder();


        List<Ingredient> ingredientList = cartProduct.getIngredientList();

        HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();

        for (final Ingredient ingredient : ingredientList) {

            List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
            List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());

            for (IngredientType ingredientType : ingredientTypeList) {
                int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                if (checked == 1) {
                    if (description.length() != 0)
                        description.append(",");


                    description.append(ingredientType.getName());


                }
            }
        }

        return description.toString();
    }


    private void addQty(int position, ImageButton add_quantity, final MyViewHolder holder) {

        add_quantity.setEnabled(false);

        CartProduct cartProduct = cartProducts.get(position);

        int qty = cartProduct.getSelected_quantity();
        int maxQty = ParseInteger(cartProduct.getAvailable_quantity());

        if (qty < maxQty) {

            cartProduct.setSelected_quantity(qty + 1);

        } else {

            Toast.makeText(context, context.getResources().getString(R.string.max_qty), Toast.LENGTH_SHORT).show();

        }

        notifyItemChanged(position);
        notifyDataSetChanged();

        updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
        setPaymentTotal(itemtotal, billamount);


        holder.product_price.setText(getPrice(cartProduct));

        logCart();

        add_quantity.setEnabled(true);

    }

    private void removeQty(int position, ImageButton remove_quantity, final MyViewHolder holder) {

        remove_quantity.setEnabled(false);

        CartProduct cartProduct = cartProducts.get(position);
        int qty = cartProduct.getSelected_quantity();
        if (qty == 1) {
            if (cartProducts.isEmpty()) {
                Toast.makeText(context, context.getResources().getString(R.string.cart_is_empty), Toast.LENGTH_SHORT).show();
            }
            cartProductId.remove(position);
            cartProducts.remove(position);
            notifyItemRemoved(position);
            notifyDataSetChanged();

        } else {
            cartProduct.setSelected_quantity(qty - 1);
            notifyItemChanged(position);
        }

        updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
        setPaymentTotal(itemtotal, billamount);


        holder.product_price.setText(getPrice(cartProduct));

        logCart();

        remove_quantity.setEnabled(true);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView product_name, prod_quantity, product_price, product_description, customize;
        ImageButton remove_quantity, add_quantity;

        MyViewHolder(View view) {
            super(view);
            product_name = view.findViewById(R.id.lblListItem);
            add_quantity = view.findViewById(R.id.add_quantity);
            remove_quantity = view.findViewById(R.id.remove_quantity);
            prod_quantity = view.findViewById(R.id.prod_quantity);
            product_price = view.findViewById(R.id.product_price);
            product_description = view.findViewById(R.id.product_description);
            customize = view.findViewById(R.id.customize);
        }
    }
}
