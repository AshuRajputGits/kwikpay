package com.uk.recharge.kwikpay;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.myaccount.MyTransactionViewDetails;
import com.uk.recharge.kwikpay.myaccount.MyTransactionsMO;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ReceiptScreen extends Activity implements AsyncRequestListenerViaPost, OnClickListener {
    private ArrayList<HashMap<String, String>> receiptArrayList;
    private HashMap<String, String> receiptHashMap;
    private SharedPreferences preference;
    private JSONArray receiptJsonArray;
    private String receivedTxnId_OrderId = "", receivedPaymentStatus = "", receivedImageStatus = "";
    private TextView failureReceiptMessage, failureReceiptButton;
    private ImageView statusImageView;

    private TextView successfullReceiptButton, successfullReceiptTopLabel, successfullReceiptMobnoOrPin;
    private TextView successfullBelowMessage, successfullDetailBtn;
    //	private LinearLayout failureReceiptLayout;
    private ScrollView failureReceiptLayout;
    private ScrollView successReceiptLayout;
    private RatingBar ratingBar;
    private int receiptType = 0;
    private TextView ratingBarLabelId, failureInfoMsg1, failureInfoMsg2;
    private TextView failureReceiptMobnoOrPin;
    private String apiDisplayMessage = "", operatorServiceId = "";
    private LinearLayout receiptRatingLayout, receiptCrowdFundingLayout;
    private String apiReceiptFlag = "";
    private TextView crowdFundingLinkTV;
    private TextView paymentFailedHeaderLabel;
    //    private String receivedTransactionID = "";
    int apiHitPosition = 0;
    private JSONArray txnJsonArray;
    private MyTransactionsMO transactionsMO = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        try {
            if (!isTaskRoot()) {
                final Intent intent = getIntent();
                if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {
                    finish();
                    return;
                }
            }
        } catch (Exception e) {
        }

        setContentView(R.layout.receipt_screen);
        settingIDs();

        Bundle getBundle = getIntent().getExtras();
        if (getBundle != null) {
            if (getBundle.containsKey("PG_APP_TXN_ID_KEY"))
                receivedTxnId_OrderId = getBundle.getString("PG_APP_TXN_ID_KEY");
        }

        apiHitPosition = 1;
        new LoadResponseViaPost(ReceiptScreen.this, formReceiptJSON(), true).execute("");

        successfullReceiptButton.setOnClickListener(this);
        successfullDetailBtn.setOnClickListener(this);
        failureReceiptButton.setOnClickListener(this);

        ratingBarLabelId = findViewById(R.id.ratingBarLabelId);
        ratingBarLabelId.setOnClickListener(this);
        crowdFundingLinkTV.setOnClickListener(this);

        ratingBar = findViewById(R.id.ratingBarId);
        ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                // TODO Auto-generated method stub
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.uk.recharge.kwikpay")));
            }
        });

        int txnCounter = preference.getInt("transactionNumber", 1);

        SharedPreferences.Editor receiptScreenEditor = preference.edit();
        receiptScreenEditor.putString("CART_ITEM_COUNTER", "0");
        receiptScreenEditor.putInt("transactionNumber", txnCounter + 1);
        receiptScreenEditor.apply();

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int height = displayMetrics.heightPixels;

        if (height < 805) {
            LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 60, 0, 0);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            ratingBarLabelId.setLayoutParams(params);
        }
    }

    private void settingIDs() {
        // TODO Auto-generated method stub
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        statusImageView = findViewById(R.id.mainHeaderTickErrorIcons);
        statusImageView.setVisibility(View.VISIBLE);
        successReceiptLayout = findViewById(R.id.success_receipt_layout);
        failureReceiptLayout = findViewById(R.id.failure_receipt_layout);
        failureReceiptMessage = findViewById(R.id.receipt_Failure_CaseMsg);
        failureReceiptButton = findViewById(R.id.receipt_Failure_CaseBtn);
        successfullReceiptButton = findViewById(R.id.receipt_success_show_receiptbtn);
        successfullReceiptTopLabel = findViewById(R.id.receipt_success_toplabel);
        successfullReceiptMobnoOrPin = findViewById(R.id.receipt_success_mobile_pin_value);
        failureReceiptMobnoOrPin = findViewById(R.id.receipt_failure_mobile_or_pin_value);
        successfullBelowMessage = findViewById(R.id.receipt_success_message);
        successfullDetailBtn = findViewById(R.id.receipt_success_show_details);
        failureInfoMsg1 = findViewById(R.id.failure_receipt_info_msg1);
        failureInfoMsg2 = findViewById(R.id.failure_receipt_info_msg2);
        crowdFundingLinkTV = findViewById(R.id.receiptCrowdfundingLink);
        paymentFailedHeaderLabel = findViewById(R.id.receipt_payment_failure_header_title);

        receiptCrowdFundingLayout = findViewById(R.id.receiptCrowdFundingLayout);
        receiptRatingLayout = findViewById(R.id.receiptRatingLayout);

        //SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(ReceiptScreen.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
    }

    public String formReceiptJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPAPPTRANSACTIONSTATUS");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("ORDERID", receivedTxnId_OrderId);
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            return json.toString();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {
        // TODO Auto-generated method stub
//		Log.e("", "JUDO PG  Receipt "+loadedString);

        int statusFlag = 0;

        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 1) {
                    if (parseMyTicketDetails(loadedString)) {
                        if (receivedPaymentStatus.equalsIgnoreCase("T")) {
                            if (receiptArrayList.size() > 0) {
                                if (receiptArrayList.get(0).get(ProjectConstant.TXN_RECHARGESTATUS).equals("T")) {
                                    statusFlag = 1; // PAYMENT STATUS= SUCCESS,RECHARGE STATUS= SUCCESS
                                } else {
                                    statusFlag = 2; // PAYMENT STATUS= SUCCESS,RECHARGE STATUS= FAIL
                                }
                            }
                        } else {
                            statusFlag = 3; // PAYMENT STATUS= FAILED
                        }

                        try {
                            String eventName = "";
                            if (receivedImageStatus.contains("SUCCESS"))
                                statusImageView.setBackgroundResource(R.drawable.receipt_success_icon);
                            else
                                statusImageView.setBackgroundResource(R.drawable.receipt_error_icon);

                            if (statusFlag == 1) {
                                eventName = getResources().getString(R.string.SCREEN_NAME_RECEIPT_SUCCESS);
                                successReceiptLayout.setVisibility(View.VISIBLE);
                                if (receiptArrayList.size() > 0) {
                                    if (receiptArrayList.get(0).get("RECHARGEREQUESTTYPE").equalsIgnoreCase("Top-up")) {
                                        String successfulTopUpText = "Your top up for " + "<b>" + receiptHashMap.get(ProjectConstant.TXN_OPERATORNAME) + "</b> is successful on mobile no.";
                                        successfullReceiptTopLabel.setText(Html.fromHtml(successfulTopUpText));
                                        successfullBelowMessage.setVisibility(View.GONE);
                                        successfullDetailBtn.setVisibility(View.GONE);
                                        successfullReceiptMobnoOrPin.setText(receiptArrayList.get(0).get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
                                        receiptType = 1;
                                    } else {
                                        String successfulPinText = "The PIN for top up of your " + "<b>" + receiptHashMap.get(ProjectConstant.TXN_OPERATORNAME) + "</b> is";
                                        successfullReceiptTopLabel.setText(Html.fromHtml(successfulPinText));
                                        successfullReceiptMobnoOrPin.setText(receiptArrayList.get(0).get(ProjectConstant.RECEIPT_RECHARGEPIN));
                                        receiptType = 2;

                                        if (operatorServiceId.equals("3"))
                                            successfullBelowMessage.setText(apiDisplayMessage);
                                    }

                                    if (apiReceiptFlag.equals("1"))
                                        receiptRatingLayout.setVisibility(View.VISIBLE);
                                    else if (apiReceiptFlag.equals("2"))
                                        receiptCrowdFundingLayout.setVisibility(View.VISIBLE);
                                }
                            } else if (statusFlag == 2) {
                                eventName = getResources().getString(R.string.SCREEN_NAME_RECEIPT_FAILURE);
                                String message = "";
                                if (receiptArrayList.get(0).get("RECHARGEREQUESTTYPE").equalsIgnoreCase("Top-up")) {
                               /* message="This top up could not be processed as the mobile number "+
                                        receiptHashMap.get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER)+ " was rejected by the operator "+
                                        receiptHashMap.get(ProjectConstant.TXN_OPERATORNAME)+" for mismatch. Please choose the correct operator and retry.\n\n"+
                                        "Any payment taken from your account has been reversed";*/

                                    if (operatorServiceId.equals("3")) {
                                        message = "Your top up for " + receiptHashMap.get(ProjectConstant.TXN_OPERATORNAME) +
                                                " could not be processed due to technical reasons";
                                    } else {
                                        message = "Your top up for " + receiptHashMap.get(ProjectConstant.TXN_OPERATORNAME) +
                                                " is rejected by the operator for the mobile no.";
                                        failureInfoMsg1.setVisibility(View.VISIBLE);
                                    }
                                    failureReceiptMessage.setText(message);
                                    failureReceiptMobnoOrPin.setText(receiptHashMap.get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
                                } else {
                                    message = "Your transaction has failed due to a technical reasons, no payment has been taken from your account. In case of any deduction, full refund will be processed immediately";
                                    failureReceiptMessage.setText(message);
                                }
                                failureReceiptLayout.setVisibility(View.VISIBLE);
                            } else if (statusFlag == 3) {
                                eventName = getResources().getString(R.string.SCREEN_NAME_RECEIPT_FAILURE);
                                String message = "Your payment was declined by your card issuer. Please get in touch with your bank and retry for a super fast top up experience.\n" +
                                        "\n" +
                                        "Alternatively you could use a different payment mechanism.";

                            /*if(receiptArrayList.get(0).get("RECHARGEREQUESTTYPE").equalsIgnoreCase("Top-up"))
                            {
                                message="Oops Your payment was declined by your card issuer. Please get in touch with your bank and retry for a super fast top up experience.\n" +
                                        "\n" +
                                        "Alternatively you could use a different payment mechanisms.";
                            }
                            else
                            {
                                message="Your payment failed please use alternate mode of payment";
                            }*/

                                failureReceiptLayout.setVisibility(View.VISIBLE);
                                paymentFailedHeaderLabel.setVisibility(View.VISIBLE);
                                failureReceiptMessage.setText(message);
                                failureReceiptButton.setVisibility(View.GONE);
                                failureInfoMsg1.setVisibility(View.GONE);
                                failureInfoMsg2.setVisibility(View.GONE);
                            }

                            try {
                                EventsLoggerSingleton.getInstance().setCommonEventLogs(this, eventName);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (apiHitPosition == 2) {
                    parseMyTransactionDetails(loadedString);
                }

            } else {
                ARCustomToast.showToast(ReceiptScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
            ARCustomToast.showToast(ReceiptScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    //*************************************************************************//
    // USE BELOW METHOD TO DETERMINE WHETHER PIN IS NUMBER OR STRING TYPE      //
    //*************************************************************************//
    /*private boolean isPinIsNumber(String pinString)
    {
		try
		{
			Integer.parseInt(pinString);
			return true;
		}catch(Exception e)
		{
			return false;
		}
	}*/

    // PARSING THE RECEIPT SCREEN DATA COMING FROM API
    private boolean parseMyTicketDetails(String myTransactionResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;

        try {
            jsonObject = new JSONObject(myTransactionResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(ReceiptScreen.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has(ProjectConstant.TXN_PAYMENTSTATUS))
                        receivedPaymentStatus = parentResponseObject.getString(ProjectConstant.TXN_PAYMENTSTATUS);
                    if (parentResponseObject.has("IMAGE"))
                        receivedImageStatus = parentResponseObject.getString("IMAGE");

                    if (parentResponseObject.has(ProjectConstant.RECEIPT_TRANSACTIONLIST)) {
                        receiptArrayList = new ArrayList<>();
                        Object object = parentResponseObject.get(ProjectConstant.RECEIPT_TRANSACTIONLIST);

                        // CHECKING WHETHER THE DATA INSIDE FAVOURITES TAG IS OBJECT OR JSON OBJECT
                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            receiptJsonArray = new JSONArray();
                            receiptJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray) {
                            receiptJsonArray = (JSONArray) object;
                        }

                        for (int i = 0; i < receiptJsonArray.length(); i++) {
                            childResponseObject = receiptJsonArray.getJSONObject(i);

                            if (!childResponseObject.isNull(ProjectConstant.RECEIPT_USEREMAIL)
                                    && !childResponseObject.isNull(ProjectConstant.RECEIPT_TRANSACTIONDATETIME)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_SERVICEID)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_OPERATORNAME)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTAMOUNT)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTSTATUS)
                                    && !childResponseObject.isNull(ProjectConstant.RECEIPT_RECHARGEPIN)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_SERVICENAME)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_ORDERID)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_RECHARGESTATUS)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_RECHARGEAMOUNT)
                                    && !childResponseObject.isNull(ProjectConstant.RECEIPT_USERNAME)
                                    && !childResponseObject.isNull(ProjectConstant.RECEIPT_RECHARGEREQUESTTYPE)
                                    && !childResponseObject.isNull(ProjectConstant.RECEIPT_VENDORDESC)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_SUBSCRIPTIONNUMBER)
                                    && !childResponseObject.isNull(ProjectConstant.RECEIPT_USERID)) {
                                receiptHashMap = new HashMap<>();
                                receiptHashMap.put(ProjectConstant.RECEIPT_USEREMAIL, childResponseObject.getString(ProjectConstant.RECEIPT_USEREMAIL));
                                receiptHashMap.put(ProjectConstant.RECEIPT_TRANSACTIONDATETIME, childResponseObject.getString(ProjectConstant.RECEIPT_TRANSACTIONDATETIME));
                                receiptHashMap.put(ProjectConstant.TXN_SERVICEID, childResponseObject.getString(ProjectConstant.TXN_SERVICEID));
                                receiptHashMap.put(ProjectConstant.TXN_OPERATORNAME, childResponseObject.getString(ProjectConstant.TXN_OPERATORNAME));
                                receiptHashMap.put(ProjectConstant.TXN_PAYMENTAMOUNT, childResponseObject.getString(ProjectConstant.TXN_PAYMENTAMOUNT));
                                receiptHashMap.put(ProjectConstant.TXN_PAYMENTSTATUS, childResponseObject.getString(ProjectConstant.TXN_PAYMENTSTATUS));
                                receiptHashMap.put(ProjectConstant.RECEIPT_RECHARGEPIN, childResponseObject.getString(ProjectConstant.RECEIPT_RECHARGEPIN));
                                receiptHashMap.put(ProjectConstant.TXN_SERVICENAME, childResponseObject.getString(ProjectConstant.TXN_SERVICENAME));
                                receiptHashMap.put(ProjectConstant.TXN_ORDERID, childResponseObject.getString(ProjectConstant.TXN_ORDERID));
                                receiptHashMap.put(ProjectConstant.TXN_RECHARGESTATUS, childResponseObject.getString(ProjectConstant.TXN_RECHARGESTATUS));

//								String rechargeAmount=convertAmountToDigit();
                                receiptHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, childResponseObject.getString(ProjectConstant.TXN_RECHARGEAMOUNT));

                                receiptHashMap.put(ProjectConstant.RECEIPT_USERNAME, childResponseObject.getString(ProjectConstant.RECEIPT_USERNAME));
                                receiptHashMap.put(ProjectConstant.RECEIPT_RECHARGEREQUESTTYPE, childResponseObject.getString(ProjectConstant.RECEIPT_RECHARGEREQUESTTYPE));
                                receiptHashMap.put(ProjectConstant.RECEIPT_VENDORDESC, childResponseObject.getString(ProjectConstant.RECEIPT_VENDORDESC));
                                receiptHashMap.put(ProjectConstant.TXN_SUBSCRIPTIONNUMBER, childResponseObject.getString(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
                                receiptHashMap.put(ProjectConstant.RECEIPT_USERID, childResponseObject.getString(ProjectConstant.RECEIPT_USERID));
                                receiptHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, childResponseObject.getString(ProjectConstant.TXN_DISPLAYAMOUNT));

                                receiptArrayList.add(receiptHashMap);
                                operatorServiceId = childResponseObject.optString(ProjectConstant.TXN_SERVICEID);
                            }

                            if (childResponseObject.has("DISPLAY_TEXT"))
                                apiDisplayMessage = childResponseObject.getString("DISPLAY_TEXT");
                            if (childResponseObject.has("RECEIPT_FLAG")) {
                                apiReceiptFlag = childResponseObject.getString("RECEIPT_FLAG");
                            }
                        }
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(ReceiptScreen.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        /*if(view.getId()==R.id.receiptScreen_FailureMsgBtn)
        {
			Intent homeIntent = new Intent(ReceiptScreen.this,HomeScreen.class);
			homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(homeIntent);
			ReceiptScreen.this.finish();
		}*/
        if (view.getId() == R.id.receipt_success_show_receiptbtn) {
            showUserTransactionReceipt(receiptType);
        } else if (view.getId() == R.id.receipt_Failure_CaseBtn) {
            showUserTransactionReceipt(receiptType);
        } else if (view.getId() == R.id.receipt_success_show_details) {
//			String detailURL="http://46.37.168.7:8080/kwikpay/kwikpay/apphtml/operators.html";
//        	startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(detailURL)));
            Intent detailIntent = new Intent(ReceiptScreen.this, AboutUs.class);
            detailIntent.putExtra("SCREEN_NAME", "RECEIPT_DETAILS");
            startActivity(detailIntent);
        } else if (view.getId() == R.id.ratingBarLabelId) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.uk.recharge.kwikpay")));
        } else if (view.getId() == R.id.receiptCrowdfundingLink) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://kwikpay.com/crowdfunding-customers/")));
        }

    }

    //************************************************************************************************//
    // BELOW METHOD IS USED TO SHOW USER TRANSACTION RECEIPT IN SUCCESSFULL (PAYMENT AND RECHARGE OR) //
    // FAILURE (PAYMENT AND RECHARGE)																  //
    // RECEIPT TYPE DENOTES WHAT TYPE OF RECEIPT NEED TO SHOW										  //
    // 0: FAILURE RECEIPT
    // 1: E-TOPUP RECEIPT
    // 2: PIN-TOPUP
    //************************************************************************************************//
    private void showUserTransactionReceipt(int receiptType) {
        /*Intent receiptIntent = new Intent(ReceiptScreen.this, ViewDetailScreen.class);
        receiptIntent.putExtra("TRANSACTION_VIEW_DETAIL", receiptHashMap);
        receiptIntent.putExtra("RECEIPT_TYPE", receiptType);
        startActivity(receiptIntent);*/
        if (transactionsMO == null) {
            apiHitPosition = 2;
            new LoadResponseViaPost(this, formMyTransactionJSON(), true).execute("");
        } else {
            startActivity(new Intent(this, MyTransactionViewDetails.class).putExtra("SELECTED_TRANSACTION", transactionsMO));
        }
    }

    public String formMyTransactionJSON() {
        try {
            JSONObject json = new JSONObject();

            json.put("APISERVICE", "KPTRANSACTIONHISTORY");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("SERVICEID", operatorServiceId);
            json.put("APP_TXN_ID", receivedTxnId_OrderId);
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //PARSING TRANSACTION DETAILS DATA
    public void parseMyTransactionDetails(String myTransactionResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(myTransactionResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has(ProjectConstant.TXN_TRANSACTIONDETAIL)) {

                        // CHECKING WHETHER THE DATA INSIDE FAVOURITES TAG IS OBJECT OR JSON OBJECT
                        Object object = parentResponseObject.get(ProjectConstant.TXN_TRANSACTIONDETAIL);

                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            txnJsonArray = new JSONArray();
                            txnJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray) {
                            txnJsonArray = (JSONArray) object;
                        }

                        for (int i = 0; i < txnJsonArray.length(); i++) {
                            childResponseObject = txnJsonArray.getJSONObject(i);

                            transactionsMO = new MyTransactionsMO();
                            transactionsMO.setTransactionDateTime(childResponseObject.optString(ProjectConstant.TXN_TRANSACTIONDATETIME));
                            transactionsMO.setTransactionServiceId(childResponseObject.optString(ProjectConstant.TXN_SERVICEID));
                            transactionsMO.setTransactionPaymentCurrencyCode(childResponseObject.optString(ProjectConstant.TXN_PAYMENTCURRENCYCODE));
                            transactionsMO.setTransactionRechargeCurrencyCode(childResponseObject.optString(ProjectConstant.TXN_RECHARGECURRENCYCODE));
                            transactionsMO.setTransactionOperatorName(childResponseObject.optString(ProjectConstant.TXN_OPERATORNAME));
                            transactionsMO.setTransactionPaymentAmount(childResponseObject.optString(ProjectConstant.TXN_PAYMENTAMOUNT));
                            transactionsMO.setTransactionPaymentStatus(childResponseObject.optString(ProjectConstant.TXN_PAYMENTSTATUS));
                            transactionsMO.setTransactionServiceName(childResponseObject.optString(ProjectConstant.TXN_SERVICENAME));
                            transactionsMO.setTransactionCountryCode(childResponseObject.optString(ProjectConstant.TXN_COUNTRYCODE));
                            transactionsMO.setTransactionDisplayAmount(childResponseObject.optString(ProjectConstant.TXN_DISPLAYAMOUNT));
                            transactionsMO.setTransactionOrderId(childResponseObject.optString(ProjectConstant.TXN_ORDERID));
                            transactionsMO.setTransactionOperatorCode(childResponseObject.optString(ProjectConstant.TXN_OPERATORCODE));
                            transactionsMO.setTransactionDisplayCurrencyCode(childResponseObject.optString(ProjectConstant.TXN_DISPLAYCURRENCYCODE));
                            transactionsMO.setTransactionRechargeStatus(childResponseObject.optString(ProjectConstant.TXN_RECHARGESTATUS));
                            transactionsMO.setTransactionRechargeAmount(childResponseObject.optString(ProjectConstant.TXN_RECHARGEAMOUNT));
                            transactionsMO.setTransactionSubscriptionNumber(childResponseObject.optString(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
                            transactionsMO.setTransactionFee(childResponseObject.optString("SERVICEFEE"));
                            transactionsMO.setTransactionRCPin(childResponseObject.optString("RECHARGEPIN"));
                            transactionsMO.setTransactionRCPinExpiryDate(childResponseObject.optString("RECHARGEPINEXPIRYDATE"));
                            transactionsMO.setTransactionCouponSrNumber(childResponseObject.optString("CPNSRNUM"));
                            transactionsMO.setTransactionCouponValue(childResponseObject.optString("CPNVALUE"));
                            transactionsMO.setTransactionDiscountAmount(childResponseObject.optString("DISCOUNT_AMOUNT"));

                            transactionsMO.setTransactionTotalEnergy(childResponseObject.optString("TOTAL_ENERGY"));
                            transactionsMO.setTransactionCostOfEnergy(childResponseObject.optString("COST_OF_ENERGY"));
                            transactionsMO.setTransactionRegNumber(childResponseObject.optString("REGISTRATION_NUMBER"));
                            transactionsMO.setTransactionLocationName(childResponseObject.optString("LOCATIONNAME"));
                            transactionsMO.setTransactionChargerId(childResponseObject.optString("CHARGERID"));
                            transactionsMO.setImageUrl(childResponseObject.optString("IMG_URL"));
                            if(childResponseObject.has("PLANDESCRIPTION"))
                                transactionsMO.setTransactionPlanDesc(childResponseObject.optString("PLANDESCRIPTION"));

                            startActivity(new Intent(this, MyTransactionViewDetails.class)
                                    .putExtra("SELECTED_TRANSACTION", transactionsMO));

                        }
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, parentResponseObject.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}