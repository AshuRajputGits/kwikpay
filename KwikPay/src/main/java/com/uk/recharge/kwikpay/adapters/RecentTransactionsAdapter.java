package com.uk.recharge.kwikpay.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.models.RecentTransactionsModel;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 15-03-2017.
 */

public class RecentTransactionsAdapter extends RecyclerView.Adapter<RecentTransactionsAdapter.RecentTransactionViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<RecentTransactionsModel> recentTransactionsModelArrayList;
    private RecentTransactionListener listener;
    private View view;
    private Context context;

    public RecentTransactionsAdapter(Context context, ArrayList<RecentTransactionsModel> recentTransactionsModelArrayList,
                                     RecentTransactionListener listener) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.recentTransactionsModelArrayList = recentTransactionsModelArrayList;
        this.listener = listener;
    }

    @Override
    public RecentTransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = layoutInflater.inflate(R.layout.recent_transaction_row, parent, false);
        RecentTransactionViewHolder holder = new RecentTransactionViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecentTransactionViewHolder holder, int position) {
        try {
            holder.recentTxnMobileNumber.setText(recentTransactionsModelArrayList.get(position).getMobileNumber());
            holder.recentTxnAmount.setText(Html.fromHtml(recentTransactionsModelArrayList.get(position).getDisplayCurrencyCode() + " " +
                    recentTransactionsModelArrayList.get(position).getTopUpAmount()));
            holder.recentTxnOperator.setText(recentTransactionsModelArrayList.get(position).getOperatorName());

            if (view != null && context != null) {
                if (position % 2 == 0)
                    view.setBackgroundColor(context.getResources().getColor(R.color.listViewOddColor));
                else
                    view.setBackgroundColor(context.getResources().getColor(R.color.listViewEvenColor));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (recentTransactionsModelArrayList.size() > 3)
            return 3;
        return recentTransactionsModelArrayList.size();
    }

    public class RecentTransactionViewHolder extends RecyclerView.ViewHolder {
        TextView recentTxnMobileNumber, recentTxnAmount, recentTxnOperator;

        public RecentTransactionViewHolder(View itemView) {
            super(itemView);
            recentTxnMobileNumber = (TextView) itemView.findViewById(R.id.recentTxnMobileNumber);
            recentTxnAmount = (TextView) itemView.findViewById(R.id.recentTxnAmount);
            recentTxnOperator = (TextView) itemView.findViewById(R.id.recentTxnOperator);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.recentTxnSelected(getLayoutPosition());
                }
            });
        }
    }

    public interface RecentTransactionListener {
        void recentTxnSelected(int position);
    }
}
