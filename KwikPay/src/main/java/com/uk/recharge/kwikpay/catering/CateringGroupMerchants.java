package com.uk.recharge.kwikpay.catering;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Product;
import com.uk.recharge.kwikpay.RoomDataBase.Models.ProductType;
import com.uk.recharge.kwikpay.adapters.catering.CateringMerchantAdapter;
import com.uk.recharge.kwikpay.adapters.catering.CateringMerchantAdapter.CateringMerchantSelectListener;
import com.uk.recharge.kwikpay.catering.cart.RestaurantMenuCartScreen;
import com.uk.recharge.kwikpay.catering.models.CateringMerchantDetailMO;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.utilities.DividerItemDecoration;
import com.uk.recharge.kwikpay.utilities.PreferenceHelper;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;

/**
 * Created by Ashu Rajput on 11/3/2018.
 */

public class CateringGroupMerchants extends KPSuperClass implements CateringMerchantSelectListener {

    private Context context;
    private SharedPreferences preference;
    private PreferenceHelper mPreferenceHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        turnHomeButtonToBackButton();
        setUpResourceIds();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.catering_group_merchants;
    }

    private void setUpResourceIds() {
        this.context = CateringGroupMerchants.this;
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        mPreferenceHelper = new PreferenceHelper(this);

        ImageView singleBannerIV = findViewById(R.id.singleBannerIV);
        setDynamicWidthHeightToImageView(singleBannerIV);

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("LOC_NUMBER"))
                callCateringMerchantByLocNum(receivedBundle.getString("LOC_NUMBER", ""));
        }
    }

    private void callCateringMerchantByLocNum(String locationNumber) {
        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "CATERING_MERCHANTS_BY_LOCATION_NUM");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("DEVICEOS", "ANDROID");
            json.put("LOCATION_NUMBER", locationNumber);
            json.put("USERID", sharedPreferences.getString("KP_USER_ID", "0"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {

                                    //Initiating Banner and setting height of banner dynamically
                                    preference.edit().putString("CAT_MERCHANT_BANNERS", childJson.optString("MERCHANT_BANNER")).apply();
                                    initiateAppsBanner("CateringMerchantDetails");
                                    setDynamicWidthHeightToView();

                                    Object genericObject = childJson.get("distributor_details");
                                    JSONArray jsonArray = null;

                                    if (genericObject instanceof JSONObject) {
                                        final JSONObject jsonOBJECT = (JSONObject) genericObject;
                                        jsonArray = new JSONArray();
                                        jsonArray.put(jsonOBJECT);
                                    } else if (genericObject instanceof JSONArray)
                                        jsonArray = (JSONArray) genericObject;

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        ArrayList<CateringMerchantDetailMO> merchantDetailList = new ArrayList<>(jsonArray.length());

                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            CateringMerchantDetailMO model = new CateringMerchantDetailMO();
                                            model.setMerchantName(jsonArray.getJSONObject(i).optString("MERCHANT_NAME"));
                                            model.setMerchantLogoUrls(jsonArray.getJSONObject(i).optString("logo_img"));
                                            model.setLocationNumber(jsonArray.getJSONObject(i).optString("LOCATION_NUMBER"));
                                            merchantDetailList.add(model);
                                        }
                                        updateMerchantRV(merchantDetailList);
                                    }

                                } else
                                    ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                            } else
                                ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(context))
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(context))
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void updateMerchantRV(ArrayList<CateringMerchantDetailMO> merchantDetailList) {
        RecyclerView cateringGroupMerchantsRV = findViewById(R.id.cateringGroupMerchantsRV);
        cateringGroupMerchantsRV.setLayoutManager(new LinearLayoutManager(CateringGroupMerchants.this));
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider));
        cateringGroupMerchantsRV.addItemDecoration(dividerItemDecoration);
        cateringGroupMerchantsRV.setAdapter(new CateringMerchantAdapter(this, merchantDetailList, this));
    }

    @Override
    public void onSelectingMerchant(String merchantLocationNumber) {
        callCateringProductByLocationNumAPI(merchantLocationNumber);
    }

    private void callCateringProductByLocationNumAPI(final String qrCODE) {
        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "CATERING_PRODUCTS_BY_LOCATION_NUM");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("DEVICEOS", "ANDROID");
            json.put("LOCATION_NUMBER", qrCODE);
            json.put("USERID", sharedPreferences.getString("KP_USER_ID", "0"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {

                                    JSONObject distributor_details = childJson.getJSONObject("distributor_details");

                                    SharedPreferences.Editor editor = preference.edit();
                                    editor.putString("DISTRIBUTOR_NAME", distributor_details.getString("distributor_name"));
                                    editor.putString("DISTRIBUTOR_ADDRESS", distributor_details.optString("ADDRESS_ONE") + " " + distributor_details.optString("CITY")
                                            + " " + distributor_details.optString("COUNTRY") + " " + distributor_details.optString("POSTCODE"));
                                    editor.putString("DISTRIBUTOR_IMAGE", distributor_details.getString("logo_img"));
                                    editor.putString("MERCHANT_TERM_BANNER", distributor_details.getString("MERCHANT_TERM_BANNER"));
                                    editor.apply();

                                    ProjectConstant.tncURL = distributor_details.getString("URLTERMS");
                                    saveProductData(childJson);

                                    mPreferenceHelper.addString("locationid", qrCODE);
                                    mPreferenceHelper.addString("servicefee", childJson.getString("SERVICE_FEE"));
                                } else
                                    ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                            } else
                                ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(context))
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(context))
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    void saveProductData(JSONObject productResponse) {
        AppDatabase appDatabase;
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        appDatabase.productDao().clear();
        appDatabase.productTypeDao().clear();
        appDatabase.ingredientDao().clear();
        appDatabase.ingredientTypeDao().clear();
        try {
            JSONArray productData = productResponse.getJSONArray("products");
            JSONArray IngredientData = null;
            if (productResponse.has("sub_products"))
                IngredientData = productResponse.getJSONArray("sub_products");
            List<Product> productList = new ArrayList<>();
            List<String> stringArrayList = new ArrayList<>();
            List<ProductType> productTypeList = new ArrayList<>();
            List<Ingredient> ingredientList = new ArrayList<>();
            List<IngredientType> ingredientTypeList = new ArrayList<>();

            for (int i = 0; i < productData.length(); i++) {
                String newProduct = productData.getJSONObject(i).getString("category");

                if (!stringArrayList.contains(newProduct)) {
                    Product product = new Product();
                    product.setName(newProduct);
                    productList.add(product);
                    stringArrayList.add(newProduct);
                    Log.d("ProductInMerchant", "Add Product " + product.getName());
                }

                ProductType productType = new ProductType();

                productType.setParent_id(newProduct);
                productType.setCategory_action(productData.getJSONObject(i).getString("category_action"));
                productType.setSize(productData.getJSONObject(i).getString("size"));
                productType.setProduct_id(productData.getJSONObject(i).getString("product_id"));
                productType.setProduct_description(productData.getJSONObject(i).getString("product_description"));
                productType.setImage(productData.getJSONObject(i).getString("image"));
                productType.setCore_price(productData.getJSONObject(i).getString("core_price"));
                productType.setAvailable_quantity(productData.getJSONObject(i).getString("available_quantity"));
                productType.setName(productData.getJSONObject(i).getString("brand_name"));
                productType.setUnit_price(productData.getJSONObject(i).getString("unit_price"));

                Log.d("ProductInMerchant", "Add productType " + productType.getName());

                productTypeList.add(productType);
            }
            stringArrayList.clear();

            if (IngredientData != null && IngredientData.length() > 0) {
                for (int i = 0; i < IngredientData.length(); i++) {
                    String newIngredient = IngredientData.getJSONObject(i).getString("sub_category");

                    if (!stringArrayList.contains(newIngredient)) {
                        Ingredient ingredient = new Ingredient();
                        ingredient.setName(newIngredient);
                        ingredient.setCategory_action(IngredientData.getJSONObject(i).getString("category_action"));
                        ingredient.setParent_id(IngredientData.getJSONObject(i).getString("category"));
                        ingredientList.add(ingredient);
                        stringArrayList.add(newIngredient);

                        Log.d("ProductInMerchant", "Add ingredient " + ingredient.getName());
                    }

                    IngredientType ingredientType = new IngredientType();

                    ingredientType.setParent_id(newIngredient);
                    ingredientType.setCategory_action(IngredientData.getJSONObject(i).getString("category_action"));
                    ingredientType.setSize(IngredientData.getJSONObject(i).getString("size"));
                    ingredientType.setProduct_id(IngredientData.getJSONObject(i).getString("product_id"));
                    ingredientType.setProduct_description(IngredientData.getJSONObject(i).getString("product_description"));
                    ingredientType.setImage(IngredientData.getJSONObject(i).getString("image"));
                    ingredientType.setCore_price(IngredientData.getJSONObject(i).getString("core_price"));
                    ingredientType.setAvailable_quantity(IngredientData.getJSONObject(i).getString("available_quantity"));
                    ingredientType.setName(IngredientData.getJSONObject(i).getString("brand_name"));
                    ingredientType.setUnit_price(IngredientData.getJSONObject(i).getString("unit_price"));
                    ingredientTypeList.add(ingredientType);
                    Log.d("ProductInMerchant", "Add ingredientType " + ingredientType.getName());
                }
            }
            appDatabase.productDao().insert(productList);
            appDatabase.productTypeDao().insert(productTypeList);
            if (ingredientList != null && ingredientList.size() > 0)
                appDatabase.ingredientDao().insert(ingredientList);
            if (ingredientTypeList != null && ingredientTypeList.size() > 0)
                appDatabase.ingredientTypeDao().insert(ingredientTypeList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (preference.getBoolean("IS_CATERING_TNC_ACCEPTED", false))
            startActivity(new Intent(context, RestaurantMenuCartScreen.class));
//            startActivity(new Intent(context, RestoMenuList.class));
        else
            startActivity(new Intent(context, CateringTnCScreen.class));
    }
}
