package com.uk.recharge.kwikpay.catering;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RaiseTicket;
import com.uk.recharge.kwikpay.catering.models.CateringCompleteOrdersMO;
import com.uk.recharge.kwikpay.catering.models.ProductSelectedDetailsMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.List;

/**
 * Created by Ashu Rajput on 11/8/2018.
 */

public class CateringViewDetails extends KPSuperClass {

    private CateringCompleteOrdersMO cateringCompleteOrdersMO = null;
    private LinearLayout catProductDynamicLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        setHeaderScreenName("View Details");
        turnHomeButtonToBackButton();
        setResourceIdAndGetDataFromMO();

        findViewById(R.id.txnDetailsContactButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cateringCompleteOrdersMO != null) {
                    Intent raiseTicketIntent = new Intent(CateringViewDetails.this, RaiseTicket.class);
                    raiseTicketIntent.putExtra("RAISETICKET_ORDERID", cateringCompleteOrdersMO.getCateringOrderId());
                    raiseTicketIntent.putExtra("RAISETICKET_SERVICE_PROVIDER", cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorName());
                    raiseTicketIntent.putExtra("RAISETICKET_DOT", cateringCompleteOrdersMO.getCateringTxnDateTime());
                    startActivity(raiseTicketIntent);
                }
            }
        });

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.catering_view_details;
    }

    private void setResourceIdAndGetDataFromMO() {

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("SELECTED_ORDER_TXN"))
                cateringCompleteOrdersMO = (CateringCompleteOrdersMO) receivedBundle.getSerializable("SELECTED_ORDER_TXN");
        }

        TextView cateringLocationMarker = findViewById(R.id.cateringLocationMarker);
        TextView cateringLocIdAddress = findViewById(R.id.cateringLocIdAddress);
        catProductDynamicLayout = findViewById(R.id.catProductDynamicLayout);

        ImageView txnDetailsOperatorLogo = findViewById(R.id.txnDetailsOperatorLogo);
        try {
            Glide.with(this).load(cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorLogo())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(txnDetailsOperatorLogo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Setting operator or merchant name
        ((TextView) findViewById(R.id.txnDetailsOperatorName)).setText(cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorName());

        //Setting order number and order date time
        ((TextView) findViewById(R.id.txnDetailsOrderID)).setText("Order Number\n" + cateringCompleteOrdersMO.getCateringOrderId());
        try {
            String[] timeSplitter = cateringCompleteOrdersMO.getCateringTxnDateTime().split(" ");
            ((TextView) findViewById(R.id.txnDetailsDateTime)).setText(timeSplitter[0] + "\n" + timeSplitter[1]);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        /*ImageView txnDetailsRCStatusIcon = findViewById(R.id.txnDetailsRCStatusIcon);
        if (myTransactionsMO.getTransactionRechargeStatus().equalsIgnoreCase("SUCCESSFUL"))
            txnDetailsRCStatusIcon.setImageResource(R.drawable.green_tick_small);
        else if (myTransactionsMO.getTransactionRechargeStatus().equalsIgnoreCase("FAILURE"))
            txnDetailsRCStatusIcon.setImageResource(R.drawable.red_cross);
        else
            txnDetailsRCStatusIcon.setImageResource(R.drawable.exclamation);*/

        String currencySymbol = cateringCompleteOrdersMO.getCateringDisplayCurrencyCode();

        TextView txnDetailsTransactionFee = findViewById(R.id.txnDetailsTransactionFee);
        txnDetailsTransactionFee.setText(Utility.fromHtml(currencySymbol) + cateringCompleteOrdersMO.getCateringServiceFee());
        TextView txnDetailsDiscount = findViewById(R.id.txnDetailsDiscount);
        txnDetailsDiscount.setText(Utility.fromHtml(currencySymbol) + cateringCompleteOrdersMO.getCateringDiscountAmount());
        TextView txnDetailsTotalAmount = findViewById(R.id.txnDetailsTotalAmount);
        txnDetailsTotalAmount.setText(Utility.fromHtml(currencySymbol) + cateringCompleteOrdersMO.getCateringPaymentAmount());

        //CREATING DYNAMIC UI FOR PRODUCTS DISPLAY:
        createAndUpdateDynamicProducts(currencySymbol);

        //-----------------------------------------------------------//
        cateringLocationMarker.setCompoundDrawablesWithIntrinsicBounds(R.drawable.location_icon, 0, 0, 0);
        cateringLocationMarker.setText(cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorName());

        String locationDetails = "ID: " + cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorLocationNumber() + "\n" +
                cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorLocationName() +
                cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorCity();
        cateringLocIdAddress.setText(locationDetails);

    }

    private void createAndUpdateDynamicProducts(String currencySymbol) {

        List<ProductSelectedDetailsMO> productSelectedDetailsMOList = cateringCompleteOrdersMO.getProductSelectedDetailsMOList();
        if (productSelectedDetailsMOList != null && productSelectedDetailsMOList.size() > 0) {

            if (productSelectedDetailsMOList != null && productSelectedDetailsMOList.size() != 0) {
                int indexLength = productSelectedDetailsMOList.size();

                TextView productName[] = new TextView[indexLength];
                TextView productPrice[] = new TextView[indexLength];

                LayoutParams productNameLayoutParam = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.5f);
                LayoutParams productAmountLayoutParam = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.5f);

                for (byte b = 0; b < productSelectedDetailsMOList.size(); b++) {
                    LinearLayout parent = new LinearLayout(this);
                    parent.setOrientation(LinearLayout.HORIZONTAL);
                    LayoutParams innerLayout = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                    innerLayout.setMargins(10, 4, 10, 4);
                    parent.setLayoutParams(innerLayout);

                    // CREATING STRENGTH TYPE TEXT VIEW
                    productName[b] = new TextView(this);
                    productName[b].setTextSize(13.5f);
                    productName[b].setLayoutParams(productNameLayoutParam);
                    productName[b].setText(productSelectedDetailsMOList.get(b).getBrandName());

                    // CREATING AUTHORIZED STRENGTH TEXT VIEW
                    productPrice[b] = new TextView(this);
                    productPrice[b].setTextSize(13.5f);
                    productPrice[b].setGravity(Gravity.CENTER);
                    productPrice[b].setLayoutParams(productAmountLayoutParam);
                    productPrice[b].setText(Utility.fromHtml(currencySymbol) + productSelectedDetailsMOList.get(b).getUnitPrice());

                    parent.addView(productName[b]);
                    parent.addView(productPrice[b]);

                    catProductDynamicLayout.addView(parent);
                }
            }
        }
    }
}
