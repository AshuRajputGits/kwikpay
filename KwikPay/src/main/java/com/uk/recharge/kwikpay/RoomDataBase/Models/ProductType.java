package com.uk.recharge.kwikpay.RoomDataBase.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class ProductType {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    private int Id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "product_id")
    private String product_id;

    @ColumnInfo(name = "product_description")
    private String product_description;

    @ColumnInfo(name = "image")
    private String image;

    @ColumnInfo(name = "core_price")
    private String core_price;

    @ColumnInfo(name = "available_quantity")
    private String available_quantity;

    @ColumnInfo(name = "size")
    private String size;

    @ColumnInfo(name = "unit_price")
    private String unit_price;

    @ColumnInfo(name = "category_action")
    private String category_action;

    @ColumnInfo(name = "parent_id")
    private String parent_id;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCore_price() {
        return core_price;
    }

    public void setCore_price(String core_price) {
        this.core_price = core_price;
    }

    public String getAvailable_quantity() {
        return available_quantity;
    }

    public void setAvailable_quantity(String available_quantity) {
        this.available_quantity = available_quantity;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getCategory_action() {
        return category_action;
    }

    public void setCategory_action(String category_action) {
        this.category_action = category_action;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
