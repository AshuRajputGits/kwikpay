package com.uk.recharge.kwikpay.database;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.uk.recharge.kwikpay.ProjectConstant;

public class DBQueryMethods {

	SQLiteDatabase sqliteDB = null;
	DatabaseTables dbTables = null;
	HashMap<String, String> appTableVersionHashMap,topupgameserviceHashMap;

	public DBQueryMethods(Context context) 
	{
		dbTables = new DatabaseTables(context);
	}
	public void open() throws SQLException 
	{
		try
		{
			sqliteDB = dbTables.getWritableDatabase();
		}catch(Exception e)
		{
			Log.e("", "SQL Exception 1");
			e.printStackTrace();
		}
	}

	public void close() 
	{
		try
		{
			sqliteDB.close();
		}catch(Exception e)
		{
			Log.e("", "SQL Exception 2");
			e.printStackTrace();
		}
		
	}

	//METHOD FOR INSERTING OPERATOR DETAILS IN OPERATOR TABLE
	public boolean insertIntoOperatorTable(ArrayList<HashMap<String, String>> operatorArrayList) 
	{
		sqliteDB.beginTransaction();
		sqliteDB.execSQL("delete from " +ProjectConstant.DB_OPERATOR_TABLENAME);
		for (HashMap<String, String> operatorHashMap : operatorArrayList) 
		{
			ContentValues cv = new ContentValues();
			cv.put(ProjectConstant.OPERATOR_ID, operatorHashMap.get(ProjectConstant.OPERATOR_ID));
			cv.put(ProjectConstant.OP_OPERATOR_ID,operatorHashMap.get(ProjectConstant.OP_OPERATOR_ID_TAG));
			cv.put(ProjectConstant.OPERATOR_ABBR,operatorHashMap.get(ProjectConstant.OPERATOR_ABBR));
			cv.put(ProjectConstant.OPERATOR_HLR_CODE,operatorHashMap.get(ProjectConstant.OPERATOR_HLR_CODE));
			cv.put(ProjectConstant.OPERATOR_NAME,operatorHashMap.get(ProjectConstant.OPERATOR_NAME));
			cv.put(ProjectConstant.OPERATOR_ENABLE,operatorHashMap.get(ProjectConstant.OPERATOR_ENABLE));
			cv.put(ProjectConstant.OPERATOR_CF_ACTIVE_FLAG,operatorHashMap.get(ProjectConstant.OPERATOR_CF_ACTIVE_FLAG));

			try 
			{
				sqliteDB.insertOrThrow(ProjectConstant.DB_OPERATOR_TABLENAME , null, cv);
			} catch (SQLException e) 
			{
				sqliteDB.endTransaction();
				return false;
			}
		}
		sqliteDB.setTransactionSuccessful();
		sqliteDB.endTransaction();
		return true;
	}

	//METHOD FOR INSERTING AREA GROUP DETAILS IN AREA GROUP TABLE
	public boolean insertIntoAreaGroupTable(ArrayList<HashMap<String, String>> areaGroupArrayList) 
	{
		sqliteDB.beginTransaction();
		sqliteDB.execSQL("delete from " +ProjectConstant.DB_AG_TABLENAME);
		for (HashMap<String, String> areaGroupHashMap : areaGroupArrayList) 
		{
			ContentValues cv = new ContentValues();
			cv.put(ProjectConstant.AG_ID, areaGroupHashMap.get(ProjectConstant.AG_ID));
			cv.put(ProjectConstant.AG_ENABLE,areaGroupHashMap.get(ProjectConstant.AG_ENABLE));
			cv.put(ProjectConstant.AG_GROUP_CODE,areaGroupHashMap.get(ProjectConstant.AG_GROUP_CODE));
			cv.put(ProjectConstant.AG_NAME,areaGroupHashMap.get(ProjectConstant.AG_NAME));
			cv.put(ProjectConstant.AG_CURRENCY, areaGroupHashMap.get(ProjectConstant.AG_CURRENCY));
			cv.put(ProjectConstant.AG_PREFIX_ALLOW,areaGroupHashMap.get(ProjectConstant.AG_PREFIX_ALLOW));
			cv.put(ProjectConstant.AG_COUNTRY_CODE,areaGroupHashMap.get(ProjectConstant.AG_COUNTRY_CODE));
			cv.put(ProjectConstant.AG_SUBS_TERM_USED,areaGroupHashMap.get(ProjectConstant.AG_SUBS_TERM_USED));
			cv.put(ProjectConstant.AG_SUBS_DATA_TYPE, areaGroupHashMap.get(ProjectConstant.AG_SUBS_DATA_TYPE));
			cv.put(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO,areaGroupHashMap.get(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO));
			cv.put(ProjectConstant.AG_SUBS_MIN_LENGTH,areaGroupHashMap.get(ProjectConstant.AG_SUBS_MIN_LENGTH));
			cv.put(ProjectConstant.AG_SUBS_MAX_LENGTH,areaGroupHashMap.get(ProjectConstant.AG_SUBS_MAX_LENGTH));
			cv.put(ProjectConstant.AG_PREFIX_LENGTH, areaGroupHashMap.get(ProjectConstant.AG_PREFIX_LENGTH));
			cv.put(ProjectConstant.AG_SECOND_VALIDATION,areaGroupHashMap.get(ProjectConstant.AG_SECOND_VALIDATION));
			cv.put(ProjectConstant.AG_CF_ACTIVE_FLAG,areaGroupHashMap.get(ProjectConstant.AG_CF_ACTIVE_FLAG));

			try 
			{
				sqliteDB.insertOrThrow(ProjectConstant.DB_AG_TABLENAME , null, cv);
			} catch (SQLException e) 
			{
				sqliteDB.endTransaction();
				return false;
			}
		}
		sqliteDB.setTransactionSuccessful();
		sqliteDB.endTransaction();
		return true;
	}

	//METHOD FOR INSERTING OPERATOR AREA DETAILS IN OPERATOR AREA TABLE
	public boolean insertIntoOprAreaTable(ArrayList<HashMap<String, String>> operatorAreaArrayList) 
	{
		sqliteDB.beginTransaction();
		sqliteDB.execSQL("delete from " +ProjectConstant.DB_OP_AREA_TABLENAME);
		for (HashMap<String, String> oprAreaHashMap : operatorAreaArrayList) 
		{
			ContentValues cv = new ContentValues();
			cv.put(ProjectConstant.OA_ID, oprAreaHashMap.get(ProjectConstant.OA_ID));
			cv.put(ProjectConstant.OA_AG_ID,oprAreaHashMap.get(ProjectConstant.OA_AG_ID));
			cv.put(ProjectConstant.OA_OS_ID,oprAreaHashMap.get(ProjectConstant.OA_OS_ID));
			cv.put(ProjectConstant.OA_CONFIGURED, oprAreaHashMap.get(ProjectConstant.OA_CONFIGURED));
			cv.put(ProjectConstant.OA_HLR_CODE,oprAreaHashMap.get(ProjectConstant.OA_HLR_CODE));
			cv.put(ProjectConstant.OA_ENABLE,oprAreaHashMap.get(ProjectConstant.OA_ENABLE));
			cv.put(ProjectConstant.OA_REQ_TYPE,oprAreaHashMap.get(ProjectConstant.OA_REQ_TYPE));
			cv.put(ProjectConstant.OA_LOGO,oprAreaHashMap.get(ProjectConstant.OA_LOGO));
			cv.put(ProjectConstant.OA_CF_ACTIVE_FLAG,oprAreaHashMap.get(ProjectConstant.OA_CF_ACTIVE_FLAG));

			try 
			{
				sqliteDB.insertOrThrow(ProjectConstant.DB_OP_AREA_TABLENAME , null, cv);
			} catch (SQLException e) 
			{
				sqliteDB.endTransaction();
				return false;
			}
		}
		sqliteDB.setTransactionSuccessful();
		sqliteDB.endTransaction();
		return true;
	}

	//METHOD FOR INSERTING OPERATOR SERVICE DETAILS IN OPERATOR SERVICE TABLE
	public boolean insertIntoOprServiceTable(ArrayList<HashMap<String, String>> operatorServiceArrayList) 
	{
		sqliteDB.beginTransaction();
		sqliteDB.execSQL("delete from " +ProjectConstant.DB_OP_SERVICE_TABLENAME);
		for (HashMap<String, String> oprServiceHashMap : operatorServiceArrayList) 
		{
			ContentValues cv = new ContentValues();
			cv.put(ProjectConstant.OS_ID, oprServiceHashMap.get(ProjectConstant.OS_ID));
			cv.put(ProjectConstant.OS_OP_ID,oprServiceHashMap.get(ProjectConstant.OS_OP_ID));
			cv.put(ProjectConstant.OS_SERVICE_ID,oprServiceHashMap.get(ProjectConstant.OS_SERVICE_ID));
			cv.put(ProjectConstant.OS_OP_LOGO,oprServiceHashMap.get(ProjectConstant.OS_OP_LOGO));
			cv.put(ProjectConstant.OS_ENABLE,oprServiceHashMap.get(ProjectConstant.OS_ENABLE));
			cv.put(ProjectConstant.OS_IS_PLAN_RANGE,oprServiceHashMap.get(ProjectConstant.OS_IS_PLAN_RANGE));
			cv.put(ProjectConstant.OS_CF_ACTIVE_FLAG,oprServiceHashMap.get(ProjectConstant.OS_CF_ACTIVE_FLAG));

			try 
			{
				sqliteDB.insertOrThrow(ProjectConstant.DB_OP_SERVICE_TABLENAME , null, cv);
			} catch (SQLException e) 
			{
				sqliteDB.endTransaction();
				return false;
			}
		}
		sqliteDB.setTransactionSuccessful();
		sqliteDB.endTransaction();
		return true;
	}

	//METHOD FOR INSERTING APP TABLE VERSION DETAILS IN APP TABLE VERSION TABLE
	public boolean insertIntoAppTableVersion(ArrayList<HashMap<String, String>> appTableVersionArrayList) 
	{
		sqliteDB.beginTransaction();
		sqliteDB.execSQL("delete from " +ProjectConstant.DB_APP_TABLE_VERSION_TABLENAME);
		for (HashMap<String, String> appTableVersionHashMap : appTableVersionArrayList) 
		{
			ContentValues cv = new ContentValues();
			cv.put(ProjectConstant.DB_HOW_TO_USE_REVISED, appTableVersionHashMap.get(ProjectConstant.DB_HOW_TO_USE_REVISED));
			cv.put(ProjectConstant.DB_OPERATOR_TABLENAME, appTableVersionHashMap.get(ProjectConstant.DB_OPERATOR_TABLENAME));
			cv.put(ProjectConstant.DB_OP_AREA_TABLENAME, appTableVersionHashMap.get(ProjectConstant.DB_OP_AREA_TABLENAME));
			cv.put(ProjectConstant.DB_AG_TABLENAME, appTableVersionHashMap.get(ProjectConstant.DB_AG_TABLENAME));
			cv.put(ProjectConstant.DB_OP_SERVICE_TABLENAME, appTableVersionHashMap.get(ProjectConstant.DB_OP_SERVICE_TABLENAME));
			cv.put(ProjectConstant.DB_TERMS_OF_USAGE_REVISED, appTableVersionHashMap.get(ProjectConstant.DB_TERMS_OF_USAGE_REVISED));
			cv.put(ProjectConstant.DB_HOW_IT_WORKS_REVISED, appTableVersionHashMap.get(ProjectConstant.DB_HOW_IT_WORKS_REVISED));
			cv.put(ProjectConstant.DB_ABOUTUS_REVISED, appTableVersionHashMap.get(ProjectConstant.DB_ABOUTUS_REVISED));
			cv.put(ProjectConstant.DB_KP_PHI_TABLENAME, appTableVersionHashMap.get(ProjectConstant.DB_KP_PHI_TABLENAME));

			try 
			{
				sqliteDB.insertOrThrow(ProjectConstant.DB_APP_TABLE_VERSION_TABLENAME , null, cv);
			} catch (SQLException e) 
			{
				sqliteDB.endTransaction();
				return false;
			}
		}
		sqliteDB.setTransactionSuccessful();
		sqliteDB.endTransaction();
		return true;
	}

	//METHOD FOR INSERTING PAGEWISE HEADER INFO DETAILS IN PAGEWISE HEADER INFO TABLE
	public boolean insertIntoPHITable(ArrayList<HashMap<String, String>> phiArrayList) 
	{
		sqliteDB.beginTransaction();
		sqliteDB.execSQL("delete from " +ProjectConstant.DB_KP_PHI_TABLENAME);
		for (HashMap<String, String> phiHashMap : phiArrayList) 
		{
			ContentValues cv = new ContentValues();
			cv.put(ProjectConstant.KP_PHI, phiHashMap.get(ProjectConstant.KP_PHI));
			cv.put(ProjectConstant.KP_PHI_HEADER_CONTENT,phiHashMap.get(ProjectConstant.KP_PHI_HEADER_CONTENT));
			cv.put(ProjectConstant.KP_PHI_SCREEN_NAME,phiHashMap.get(ProjectConstant.KP_PHI_SCREEN_NAME));

			try 
			{
				sqliteDB.insertOrThrow(ProjectConstant.DB_KP_PHI_TABLENAME , null, cv);
			} catch (SQLException e) 
			{
				sqliteDB.endTransaction();
				return false;
			}
		}
		sqliteDB.setTransactionSuccessful();
		sqliteDB.endTransaction();
		return true;
	}

	//METHOD FOR INSERTING CART DETAILS IN SAVE CART TABLE
	public boolean insertIntoSaveCartTable(ArrayList<HashMap<String, String>> saveCartArrayList) 
	{
		sqliteDB.beginTransaction();
		sqliteDB.execSQL("delete from " +ProjectConstant.DB_SAVE_CART);
		for (HashMap<String, String> saveCartHashMap : saveCartArrayList) 
		{
			ContentValues cv = new ContentValues();
			cv.put(ProjectConstant.CART_CARTID, saveCartHashMap.get(ProjectConstant.CART_CARTID));
			cv.put(ProjectConstant.CART_MOBILENUMBER,saveCartHashMap.get(ProjectConstant.CART_MOBILENUMBER));
			cv.put(ProjectConstant.TXN_RECHARGEAMOUNT,saveCartHashMap.get(ProjectConstant.TXN_RECHARGEAMOUNT));
			cv.put(ProjectConstant.TXN_DISPLAYAMOUNT,saveCartHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT));
			cv.put(ProjectConstant.CART_NETPGAMOUNT, saveCartHashMap.get(ProjectConstant.CART_NETPGAMOUNT));
			cv.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID,saveCartHashMap.get(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
			cv.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID,saveCartHashMap.get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
			cv.put(ProjectConstant.TOPUP_API_PAYMENTCURRENCYAGID,saveCartHashMap.get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYAGID));
			cv.put(ProjectConstant.CART_CPNTXNID, saveCartHashMap.get(ProjectConstant.CART_CPNTXNID));
			cv.put(ProjectConstant.CART_CPNSRNO,saveCartHashMap.get(ProjectConstant.CART_CPNSRNO));
			cv.put(ProjectConstant.CART_CPNAMOUNT,saveCartHashMap.get(ProjectConstant.CART_CPNAMOUNT));
			cv.put(ProjectConstant.CART_CPNMESSAGE,saveCartHashMap.get(ProjectConstant.CART_CPNMESSAGE));
			cv.put(ProjectConstant.TXN_SERVICEID, saveCartHashMap.get(ProjectConstant.TXN_SERVICEID));
			cv.put(ProjectConstant.TXN_OPERATORCODE,saveCartHashMap.get(ProjectConstant.TXN_OPERATORCODE));
			cv.put(ProjectConstant.TXN_COUNTRYCODE,saveCartHashMap.get(ProjectConstant.TXN_COUNTRYCODE));
			cv.put(ProjectConstant.CART_DISCOUNTAMOUNT,saveCartHashMap.get(ProjectConstant.CART_DISCOUNTAMOUNT));
			cv.put(ProjectConstant.CART_PGDISCOUNTAMOUNT,saveCartHashMap.get(ProjectConstant.CART_PGDISCOUNTAMOUNT));
			cv.put(ProjectConstant.PG_PGSOID,saveCartHashMap.get(ProjectConstant.PG_PGSOID));
			cv.put(ProjectConstant.PG_MOPID,saveCartHashMap.get(ProjectConstant.PG_MOPID));
			cv.put(ProjectConstant.APP_SESSIONID,saveCartHashMap.get(ProjectConstant.APP_SESSIONID));
			cv.put(ProjectConstant.PAYNOW_PROCESSINGFEE, saveCartHashMap.get(ProjectConstant.PAYNOW_PROCESSINGFEE));
			cv.put(ProjectConstant.PAYNOW_SERVICEFEE,saveCartHashMap.get(ProjectConstant.PAYNOW_SERVICEFEE));
			cv.put(ProjectConstant.PAYNOW_PLANDESCRIPTION,saveCartHashMap.get(ProjectConstant.PAYNOW_PLANDESCRIPTION));
			cv.put(ProjectConstant.TXN_OPERATORNAME,saveCartHashMap.get(ProjectConstant.TXN_OPERATORNAME));
			cv.put(ProjectConstant.CART_INSERTIONDATE, saveCartHashMap.get(ProjectConstant.CART_INSERTIONDATE));
			cv.put(ProjectConstant.CART_UPDATEDDATE,saveCartHashMap.get(ProjectConstant.CART_UPDATEDDATE));
			cv.put(ProjectConstant.TXN_DISPLAYCURRENCYCODE,saveCartHashMap.get(ProjectConstant.TXN_DISPLAYCURRENCYCODE));
			cv.put(ProjectConstant.TXN_RECHARGECURRENCYCODE,saveCartHashMap.get(ProjectConstant.TXN_RECHARGECURRENCYCODE));
			cv.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE,saveCartHashMap.get(ProjectConstant.TXN_PAYMENTCURRENCYCODE));

			try 
			{
				sqliteDB.insertOrThrow(ProjectConstant.DB_SAVE_CART , null, cv);
			} catch (SQLException e) 
			{
				sqliteDB.endTransaction();
				return false;
			}
		}
		sqliteDB.setTransactionSuccessful();
		sqliteDB.endTransaction();
		return true;
	}

	//METHOD FOR GETTING THE DETAILS OF ALL COUNTRY LIST
	public ArrayList<HashMap<String, String>> getCountryList() 
	{
		ArrayList<HashMap<String, String>> countryArrayList = new ArrayList<HashMap<String, String>>();
		final String cur = "select distinct ag.ag_group_code, ag.ag_name, ag.ag_currency, ag.ag_country_code, ag.ag_subcr_min_len, ag.ag_subcr_max_len, ag.ag_subcr_allow_leading_zero, ag.ag_second_validation from area_group ag INNER JOIN operator_area oa ON ag.ag_id = oa.ag_id and oa.cf_active_flag = '1' and oa.oa_enable ='1' INNER JOIN opr_service os ON oa.os_id = os.os_id and os.cf_active_flag = '1' and os.os_enable = '1' and os.service_id = '2' Where ag.cf_active_flag = '1' and ag.ag_enable = '1'order by ag.ag_name";
		HashMap<String, String> countryHashMap;
		try
		{
			Cursor cursor = sqliteDB.rawQuery(cur, null);
			if (cursor.getCount() > 0) 
			{
				cursor.moveToFirst();
				do 
				{
					countryHashMap = new HashMap<String, String>();
					countryHashMap.put(ProjectConstant.AG_GROUP_CODE, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_GROUP_CODE)));
					countryHashMap.put(ProjectConstant.AG_NAME, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_NAME)));
					countryHashMap.put(ProjectConstant.AG_CURRENCY, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_CURRENCY)));
					countryHashMap.put(ProjectConstant.AG_COUNTRY_CODE, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_COUNTRY_CODE)));
					countryHashMap.put(ProjectConstant.AG_SUBS_MIN_LENGTH, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SUBS_MIN_LENGTH)));
					countryHashMap.put(ProjectConstant.AG_SUBS_MAX_LENGTH, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SUBS_MAX_LENGTH)));
					countryHashMap.put(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO)));
					countryHashMap.put(ProjectConstant.AG_SECOND_VALIDATION, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SECOND_VALIDATION)));

					countryArrayList.add(countryHashMap);

				} while (cursor.moveToNext());
			}

			cursor.close(); // that's important too, otherwise you're gonna leak cursor
		}catch(IllegalStateException ise)
		{

		}

		return countryArrayList;
	}

	//METHOD FOR GETTING THE DETAILS OF APP TABLE VERSION
	public HashMap<String, String> getAppTableVersionData() 
	{
		final String cur = "SELECT * FROM "+ProjectConstant.DB_APP_TABLE_VERSION_TABLENAME;
		try
		{
			Cursor cursor = sqliteDB.rawQuery(cur, null);
			if (cursor.getCount() > 0) 
			{
				cursor.moveToFirst();
				do 
				{
					appTableVersionHashMap = new HashMap<String, String>();
					appTableVersionHashMap.put(ProjectConstant.DB_HOW_TO_USE_REVISED, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_HOW_TO_USE_REVISED))); // [0 index]
					appTableVersionHashMap.put(ProjectConstant.DB_OPERATOR_TABLENAME, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_OPERATOR_TABLENAME))); // [1 index]
					appTableVersionHashMap.put(ProjectConstant.DB_OP_AREA_TABLENAME, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_OP_AREA_TABLENAME)));   // [2 index]
					appTableVersionHashMap.put(ProjectConstant.DB_AG_TABLENAME, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_AG_TABLENAME)));			   // [3 index]
					appTableVersionHashMap.put(ProjectConstant.DB_OP_SERVICE_TABLENAME, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_OP_SERVICE_TABLENAME)));//[4 index]
					appTableVersionHashMap.put(ProjectConstant.DB_TERMS_OF_USAGE_REVISED, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_TERMS_OF_USAGE_REVISED))); //[5 index]
					appTableVersionHashMap.put(ProjectConstant.DB_HOW_IT_WORKS_REVISED, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_HOW_IT_WORKS_REVISED))); // [6 index]
					appTableVersionHashMap.put(ProjectConstant.DB_ABOUTUS_REVISED, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_ABOUTUS_REVISED)));	// [7 index]
					appTableVersionHashMap.put(ProjectConstant.DB_KP_PHI_TABLENAME, cursor.getString(cursor.getColumnIndex(ProjectConstant.DB_KP_PHI_TABLENAME)));  // [8 index]

				} while (cursor.moveToNext());
			}

			cursor.close(); // that's important too, otherwise you're gonna leak cursor
		}catch(IllegalStateException ise)
		{
		}
		return appTableVersionHashMap;
	}

	//METHOD FOR GETTING THE DETAILS OF MAX LENGTH AND OTHER DETAILS FOR TOPUP AND GAME AND SERVICE
	public HashMap<String, String> getDetailsForTopUpAndGameService() 
	{
		final String cur = "select ag_id , ag_enable , ag_group_code , ag_name , ag_currency , ag_prefix_allow , ag_country_code ,ag_subcriber_term_used ,ag_subcr_data_type , ag_subcr_allow_leading_zero ,ag_subcr_min_len , ag_subcr_max_len , ag_prefix_len , ag_second_validation from area_group WHERE cf_active_flag = '1' and ag_enable = '1' and ag_group_code= 'UNITEDKINGDOM'";
		try
		{
			Cursor cursor = sqliteDB.rawQuery(cur, null);
			if (cursor.getCount() > 0) 
			{
				cursor.moveToFirst();
				do 
				{
					topupgameserviceHashMap = new HashMap<String, String>();
					topupgameserviceHashMap.put(ProjectConstant.AG_ID, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_ID))); // [0 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_ENABLE, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_ENABLE))); // [1 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_GROUP_CODE, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_GROUP_CODE)));   // [2 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_NAME, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_NAME)));			   // [3 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_CURRENCY, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_CURRENCY)));//[4 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_PREFIX_ALLOW, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_PREFIX_ALLOW))); //[5 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_COUNTRY_CODE, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_COUNTRY_CODE))); // [6 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_SUBS_TERM_USED, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SUBS_TERM_USED)));	// [7 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_SUBS_DATA_TYPE, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SUBS_DATA_TYPE)));  // [8 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO)));  // [8 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_SUBS_MIN_LENGTH, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SUBS_MIN_LENGTH)));  // [9 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_SUBS_MAX_LENGTH, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SUBS_MAX_LENGTH)));  // [10 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_PREFIX_LENGTH, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_PREFIX_LENGTH)));  // [11 index]
					topupgameserviceHashMap.put(ProjectConstant.AG_SECOND_VALIDATION, cursor.getString(cursor.getColumnIndex(ProjectConstant.AG_SECOND_VALIDATION)));  // [12 index]

				} while (cursor.moveToNext());
			}
			cursor.close(); // that's important too, otherwise you're gonna leak cursor
		}catch(IllegalStateException ise)
		{
			Log.e("", "Splash Work hash Exception ");
		}
		return topupgameserviceHashMap;
	}

	//METHOD FOR GETTING THE DETAILS OF APP TABLE VERSION
	public ArrayList<HashMap<String, String>> getSaveCartTransactions() 
	{
		ArrayList<HashMap<String, String>> cartArrayList = new ArrayList<HashMap<String, String>>();

		final String cur = "SELECT * FROM "+ProjectConstant.DB_SAVE_CART;
		try
		{
			Cursor cursor = sqliteDB.rawQuery(cur, null);
			if (cursor.getCount() > 0) 
			{
				cursor.moveToFirst();
				do 
				{
					appTableVersionHashMap = new HashMap<String, String>();
					appTableVersionHashMap.put(ProjectConstant.CART_CARTID, cursor.getString(cursor.getColumnIndex(ProjectConstant.CART_CARTID))); 
					appTableVersionHashMap.put(ProjectConstant.TXN_OPERATORNAME, cursor.getString(cursor.getColumnIndex(ProjectConstant.TXN_OPERATORNAME))); 
					appTableVersionHashMap.put(ProjectConstant.CART_MOBILENUMBER, cursor.getString(cursor.getColumnIndex(ProjectConstant.CART_MOBILENUMBER)));  
					appTableVersionHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, cursor.getString(cursor.getColumnIndex(ProjectConstant.TXN_DISPLAYAMOUNT)));	
					appTableVersionHashMap.put(ProjectConstant.CART_DISCOUNTAMOUNT, cursor.getString(cursor.getColumnIndex(ProjectConstant.CART_DISCOUNTAMOUNT)));
					appTableVersionHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, cursor.getString(cursor.getColumnIndex(ProjectConstant.PAYNOW_PLANDESCRIPTION))); 
					appTableVersionHashMap.put(ProjectConstant.TXN_DISPLAYCURRENCYCODE, cursor.getString(cursor.getColumnIndex(ProjectConstant.TXN_DISPLAYCURRENCYCODE)));
					appTableVersionHashMap.put(ProjectConstant.CART_CPNSRNO, cursor.getString(cursor.getColumnIndex(ProjectConstant.CART_CPNSRNO))); 
					appTableVersionHashMap.put(ProjectConstant.CART_CPNTXNID, cursor.getString(cursor.getColumnIndex(ProjectConstant.CART_CPNTXNID)));
					appTableVersionHashMap.put(ProjectConstant.TXN_OPERATORCODE, cursor.getString(cursor.getColumnIndex(ProjectConstant.TXN_OPERATORCODE))); 
					appTableVersionHashMap.put(ProjectConstant.TXN_SERVICEID, cursor.getString(cursor.getColumnIndex(ProjectConstant.TXN_SERVICEID)));
					appTableVersionHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, cursor.getString(cursor.getColumnIndex(ProjectConstant.PAYNOW_PROCESSINGFEE))); 
					appTableVersionHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, cursor.getString(cursor.getColumnIndex(ProjectConstant.PAYNOW_SERVICEFEE)));
					appTableVersionHashMap.put(ProjectConstant.CART_NETPGAMOUNT, cursor.getString(cursor.getColumnIndex(ProjectConstant.CART_NETPGAMOUNT))); 
					appTableVersionHashMap.put(ProjectConstant.TXN_COUNTRYCODE, cursor.getString(cursor.getColumnIndex(ProjectConstant.TXN_COUNTRYCODE)));
					appTableVersionHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, cursor.getString(cursor.getColumnIndex(ProjectConstant.TXN_RECHARGEAMOUNT))); 
					appTableVersionHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, cursor.getString(cursor.getColumnIndex(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID)));
					appTableVersionHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, cursor.getString(cursor.getColumnIndex(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID))); 
					appTableVersionHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, cursor.getString(cursor.getColumnIndex(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID)));
					appTableVersionHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, cursor.getString(cursor.getColumnIndex(ProjectConstant.TXN_PAYMENTCURRENCYCODE)));
					
					cartArrayList.add(appTableVersionHashMap);

				} while (cursor.moveToNext());
			}

			cursor.close(); // that's important too, otherwise you're gonna leak cursor
		}catch(IllegalStateException ise)
		{
		}
		return cartArrayList;
	}

	//METHOD FOR GETTING WHETHER APP TABLE VERSION IS EMPTY OR NOT
	public boolean isAppVersionTableEmpty()
	{
		long rows=0;
		try
		{
			sqliteDB.beginTransaction();
			SQLiteStatement s = sqliteDB.compileStatement("SELECT COUNT(*) FROM "+ProjectConstant.DB_APP_TABLE_VERSION_TABLENAME );
			rows = s.simpleQueryForLong();
			sqliteDB.endTransaction();
		}catch(Exception e)
		{
			sqliteDB.endTransaction();
		}
		if(rows>0)
			return false;
		else
			return true;

	}

	//METHOD FOR GETTING KP SUB HEADER TITLES
	public String getSubHeaderTitles(String screenName)
	{
		String headerTitle="",query="";
		try
		{
			sqliteDB.beginTransaction();

			query="SELECT KP_PHI_HEADER_CONTENT FROM KP_PAGEWISE_HEADER_INFO WHERE KP_PHI_SCREEN_NAME='" +screenName +"'";

			SQLiteStatement s = sqliteDB.compileStatement(query);
			headerTitle= s.simpleQueryForString();
			sqliteDB.endTransaction();
			
		}catch(Exception e)
		{
			sqliteDB.endTransaction();
		}

		return headerTitle;
	}

	//METHOD FOR GETTING KP SUB HEADER TITLES
	public String getOpABBR(String opCode)
	{
		String opABBR="",query="";
		
		try
		{
			sqliteDB.beginTransaction();
			query="SELECT OP_ABBR FROM OPERATOR WHERE OP_CODE='" +opCode +"'";
			SQLiteStatement s = sqliteDB.compileStatement(query);
			opABBR= s.simpleQueryForString();
			sqliteDB.endTransaction();
			
		}catch(Exception e)
		{
			sqliteDB.endTransaction();
		}

		return opABBR;
	}

	//METHOD FOR DELETING CART TRANSACTION
	public boolean deleteCartTransaction(String cartID)
	{
		String deleteQuery="DELETE FROM " +ProjectConstant.DB_SAVE_CART+" WHERE CARTID='"+cartID+"';";

//		Log.e("", "cart delete query "+deleteQuery);

		try
		{
			sqliteDB.execSQL(deleteQuery);

		}catch(Exception e)
		{
			return false;
		}

		return true;
	}

	//METHOD FOR DELETING ALL RECORDS AND TRANSACTION FROM CART TABLE
	public boolean deleteAllCartTableRecords()
	{
		String deleteQuery="DELETE FROM " +ProjectConstant.DB_SAVE_CART;
		try
		{
			sqliteDB.execSQL(deleteQuery);
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

}
