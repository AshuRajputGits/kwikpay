package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.uk.recharge.kwikpay.catering.CateringReceiptScreen;
import com.uk.recharge.kwikpay.gamingarcade.payments.GamingReceipt;
import com.uk.recharge.kwikpay.gamingarcade.payments.GamingSuccessFailure;
import com.uk.recharge.kwikpay.kwikcharge.receipt.KCSuccessFailureScreen;
import com.uk.recharge.kwikpay.kwikvend.KVProductSelection;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class PaymentGatewayPage extends Activity {
    private ProgressDialog mProgressDialog;
    private WebView webView;
    private String receivedUrlParams = "", receivedTxnID = "";
    private LinearLayout parentLinearLayout;
    private String pgBaseUrl = "", pgIntermediateUrl1 = "", pgIntermediateUrl2 = "", pgIntermediateUrl3 = "";
    private String kcSuccessURL = "", kcFailureURL = "";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.paymentgateway_screen);

        webView = findViewById(R.id.pg_WebView);
        parentLinearLayout = findViewById(R.id.webviewpgLinearLayout);

        pgBaseUrl = getResources().getString(R.string.pgBaseUrl);
        pgIntermediateUrl1 = getResources().getString(R.string.pgIntermediateUrl1);
        pgIntermediateUrl2 = getResources().getString(R.string.pgIntermediateUrl2);
        pgIntermediateUrl3 = getResources().getString(R.string.pgIntermediateUrl3);
        kcSuccessURL = getResources().getString(R.string.kcSuccessURL);
        kcFailureURL = getResources().getString(R.string.kcFailureURL);

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("PG_JSON_PARAM"))
                receivedUrlParams = receivedBundle.getString("PG_JSON_PARAM");
            if (receivedBundle.containsKey("PG_APP_TXN_ID_KEY"))
                receivedTxnID = receivedBundle.getString("PG_APP_TXN_ID_KEY");
        }

        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.clearHistory();

        CustomWebViewClient webViewClient = new CustomWebViewClient(PaymentGatewayPage.this);
        webView.setWebViewClient(webViewClient);

        String postData = null;
        try {
            postData = "param=" + URLEncoder.encode(receivedUrlParams, "UTF-8");
//            Log.e("Webview", "PG FullURL" + pgBaseUrl + postData);
//            Log.e("Webview", "PGJson " + receivedUrlParams);
//            Log.e("Webview", "PG Final Url  " + postData);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        webView.postUrl(pgBaseUrl, postData.getBytes());

//		webView.postUrl(pgBaseUrl,EncodingUtils.getBytes("param="+receivedUrlParams, "text/html"));

    }

    public class CustomWebViewClient extends WebViewClient {
        public Context aboutUsContext;

        public CustomWebViewClient(Context aboutUsContext) {
            this.aboutUsContext = aboutUsContext;
            mProgressDialog = new ProgressDialog(aboutUsContext);
            mProgressDialog.setMessage("loading please wait...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            try {
                if (!isFinishing()) {
                    mProgressDialog.show();
                }
            } catch (Exception exception) {
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            try {

                if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                //TOPUP,INTERNATIONAL AND GAME-SERVICES CALL BACK URLS
                if (url.equals(pgIntermediateUrl1))
                    callingReceiptScreen();
                else if (url.equals(pgIntermediateUrl2))
                    callingReceiptScreen();
                else if (url.equals(pgIntermediateUrl3))
                    callingReceiptScreen();

                    //KWIKCHARGE CALL BACK URLS
                else if (url.equals(kcSuccessURL))
                    callingKCReceiptScreen(true);
                else if (url.equals("http://46.37.168.7:8080/kppg/pg-app-rspn-kwikcharge-pgsuccess.jspx"))
                    callingKCReceiptScreen(true);
                else if (url.equals("http://46.37.168.5:8080/kppg/pg-app-rspn-kwikcharge-pgsuccess.jspx"))
                    callingKCReceiptScreen(true);
                else if (url.equals(kcFailureURL))
                    callingKCReceiptScreen(false);
                else if (url.equals("http://46.37.168.7:8080/kppg/pg-app-rspn-kwikcharge-pgfailed.jspx"))
                    callingKCReceiptScreen(false);
                else if (url.equals("http://46.37.168.5:8080/kppg/pg-app-rspn-kwikcharge-pgfailed.jspx"))
                    callingKCReceiptScreen(false);

                    //KWIK VEND CALL BACK URLS
                else if (url.equals("http://46.37.168.7:8080/kppg/pg-app-rspn-kwikvend-pgsuccess.jspx"))
                    callingKwikVendReceiptScreen(true);
                else if (url.equals("http://46.37.168.7:8080/kppg/pg-app-rspn-kwikvend-pgfailed.jspx"))
                    callingKwikVendReceiptScreen(false);

                    //CATERING CALL BACK URLS
                else if (url.equals("http://46.37.168.7:8080/kppg/pg-app-rspn-catering-pgsuccess.jspx"))
                    callingCateringReceiptScreen(true);
                else if (url.equals("http://46.37.168.5:8080/kppg/pg-app-rspn-catering-pgsuccess.jspx"))
                    callingCateringReceiptScreen(true);
                else if (url.equals("http://46.37.168.7:8080/kppg/pg-app-rspn-catering-pgfailed.jspx"))
                    callingCateringReceiptScreen(false);
                else if (url.equals("http://46.37.168.5:8080/kppg/pg-app-rspn-catering-pgfailed.jspx"))
                    callingCateringReceiptScreen(false);

                    //GAMING CALL BACK URLS
                else if (url.equals("http://46.37.168.7:8080/kppg/pg-app-rspn-gaming-pgsuccess.jspx"))
                    callingGamingReceiptScreen(true);
                else if (url.equals("http://46.37.168.7:8080/kppg/pg-app-rspn-gaming-pgfailed.jspx"))
                    callingGamingReceiptScreen(false);
                else if (url.equals("http://46.37.168.5:8080/kppg/pg-app-rspn-gaming-pgsuccess.jspx"))
                    callingGamingReceiptScreen(true);
                else if (url.equals("http://46.37.168.5:8080/kppg/pg-app-rspn-gaming-pgfailed.jspx"))
                    callingGamingReceiptScreen(false);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

		/*@Override
        public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) 
		{
			super.onReceivedError(view, errorCode, description, failingUrl);
		}*/

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {

            /*Log.e("SSlError","Error--- : "+error.getPrimaryError());
            String message="";

            switch (error.getPrimaryError()) {
                case SslError.SSL_UNTRUSTED:
                    message = "The certificate authority is not trusted.";
                    break;
                case SslError.SSL_EXPIRED:
                    message = "The certificate has expired.";
                    break;
                case SslError.SSL_IDMISMATCH:
                    message = "The certificate Hostname mismatch.";
                    break;
                case SslError.SSL_NOTYETVALID:
                    message = "The certificate is not yet valid.";
                    break;
                case SslError.SSL_INVALID:
                    message = "The certificate is not NOT valid.";
                    break;
            }

            Log.e("SSlError","Error : "+message);*/

            try {
                final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentGatewayPage.this);
                builder.setMessage("There is an error loading the SSL certificate do you want to continue anyway");
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            } catch (Exception e) {
            }
        }
    }

    private void callingReceiptScreen() {
        parentLinearLayout.removeAllViews();
        setContentView(R.layout.payment_gif_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent receiptIntent = new Intent(PaymentGatewayPage.this, ReceiptScreen.class);
                receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedTxnID);
                startActivity(receiptIntent);
                PaymentGatewayPage.this.finish();
            }
        }, 35000);
    }

    private void callingKCReceiptScreen(final boolean paymentStatus) {

        Intent receiptIntent = new Intent(PaymentGatewayPage.this, KCSuccessFailureScreen.class);
        receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedTxnID);
        receiptIntent.putExtra("IS_PAYMENT_SUCCESS", paymentStatus);
        startActivity(receiptIntent);
        PaymentGatewayPage.this.finish();
    }

    private void callingCateringReceiptScreen(final boolean paymentStatus) {

        Intent receiptIntent = new Intent(PaymentGatewayPage.this, CateringReceiptScreen.class);
        receiptIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedTxnID);
        receiptIntent.putExtra("IS_PAYMENT_SUCCESS", paymentStatus);
        startActivity(receiptIntent);
        PaymentGatewayPage.this.finish();
    }

    private void callingGamingReceiptScreen(final boolean paymentStatus) {
        if (paymentStatus) {
            startActivity(new Intent(this, GamingReceipt.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            PaymentGatewayPage.this.finish();
        } else {
            Intent receiptIntent = new Intent(this, GamingSuccessFailure.class);
            receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedTxnID);
            receiptIntent.putExtra("IS_PAYMENT_SUCCESS", paymentStatus);
            startActivity(receiptIntent);
            PaymentGatewayPage.this.finish();
        }
    }

    private void callingKwikVendReceiptScreen(final boolean paymentStatus) {
//        Intent receiptIntent = new Intent(this, KVDispenseScreen.class);
        Intent receiptIntent = new Intent(this, KVProductSelection.class);
        receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedTxnID);
        receiptIntent.putExtra("IS_PAYMENT_SUCCESS", paymentStatus);
        receiptIntent.putExtra("WelcomeBackPageAfterPayment", true);
        startActivity(receiptIntent);
        PaymentGatewayPage.this.finish();
    }

    @Override
    public void onBackPressed() {
    }
}
