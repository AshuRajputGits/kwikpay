package com.uk.recharge.kwikpay.intro;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;

/**
 * Created by Ashu Rajput on 01-02-2018.
 */

public class IntroScreenAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private int[] introScreenImages = {R.drawable.introscreen1,
            R.drawable.introscreen2,
            R.drawable.introscreen3,
            R.drawable.introscreen4,
            R.drawable.introscreen5,
            R.drawable.introscreen6};

    public IntroScreenAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return introScreenImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.view_pager_item, container, false);
        ImageView bannerAdsIV = itemView.findViewById(R.id.bannerAdsIV);
        try {
            Glide.with(mContext).load(introScreenImages[position]).into(bannerAdsIV);
            container.addView(itemView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
