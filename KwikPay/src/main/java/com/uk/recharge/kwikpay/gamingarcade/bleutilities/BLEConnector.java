package com.uk.recharge.kwikpay.gamingarcade.bleutilities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Ashu Rajput on 11/19/2018.
 */

public class BLEConnector {
    private BLEInterface bleInterface;
    private String macAddress;
    private BluetoothAdapter mBtAdapter;
    private Activity context;
    public final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
//            BluetoothDevice device = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            BluetoothDevice device = intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            if ("android.bluetooth.device.action.FOUND".equals(action)) {
                if (device.getBondState() != 12) {
                    Log.e("Device", device.getAddress().toString());
                    if (device.getAddress().equalsIgnoreCase(BLEConnector.this.macAddress)) {
                        BLEConnector.this.destroyReceiver();
                        BLEConnector.this.bleInterface.onConnected(device);
                        return;
                    }
                }
            } else if (!action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                int state;
                if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                    state = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -2147483648);
                    int prevState = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", -2147483648);
                    Log.e("STATE", String.valueOf(state));
                    if (state == 12) {
                    }

                    if (state == 12 && prevState == 11) {
                        Log.e("AT", "Paired");
                    } else if (state == 10 && prevState == 12) {
                        Log.e("AT", "incorrect");
                    }
                } else if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                    state = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -2147483648);
                    Log.e("STATE_AD", String.valueOf(state));
                } else if (action.equals("android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED")) {
                    state = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -2147483648);
                    Log.e("STATE_3", String.valueOf(state));
                }
            }

            if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
            }
        }
    };

    public BLEConnector(Activity context, String macAddress, BLEInterface bleInterface) {
        this.context = context;
        this.bleInterface = bleInterface;
        this.macAddress = macAddress;
    }

    public void startScanning() {
        Log.e("KPBLE", this.macAddress);
        IntentFilter filter = new IntentFilter("android.bluetooth.device.action.FOUND");
        this.context.registerReceiver(this.mReceiver, filter);
        filter = new IntentFilter("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
        this.context.registerReceiver(this.mReceiver, filter);
        filter = new IntentFilter("android.bluetooth.device.action.PAIRING_REQUEST");
        this.context.registerReceiver(this.mReceiver, filter);
        this.mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = this.mBtAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            Iterator var3 = pairedDevices.iterator();
            while (var3.hasNext()) {
                BluetoothDevice device = (BluetoothDevice) var3.next();
                if (device.getAddress().equalsIgnoreCase(this.macAddress)) {
                    Log.e("Found", "At Pairing");
                    this.destroyReceiver();
                    this.bleInterface.onBLEAlreadyPaired(device);
                    break;
                }
            }
        }

        this.doDiscovery();
    }

    public void destroyReceiver() {
        if (this.mBtAdapter != null) {
            this.mBtAdapter.cancelDiscovery();
        }

        this.context.unregisterReceiver(this.mReceiver);
    }

    private void doDiscovery() {
        this.mBtAdapter.startDiscovery();
    }
}
