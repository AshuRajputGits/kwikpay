package com.uk.recharge.kwikpay.gamingarcade;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.adapters.AdsViewPagerAdapter;
import com.uk.recharge.kwikpay.gamingarcade.GamingProductPricingAdapter.GamingPricingItemListener;
import com.uk.recharge.kwikpay.gamingarcade.models.GameMachineInfoMO;
import com.uk.recharge.kwikpay.gamingarcade.payments.GamingSuccessFailure;
import com.uk.recharge.kwikpay.singleton.GamingInfoSingleton;
import com.uk.recharge.kwikpay.utilities.DividerItemDecoration;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by Ashu Rajput on 7/6/2018.
 */

public class GamingProductPricing extends AppCompatActivity implements GamingPricingItemListener {

    private AutoScrollViewPager mViewPager;
    private SharedPreferences preference;
    private RecyclerView gamingProductRV = null;
    private TextView operatorBannerDescription;
    //    private TextView  productInfoLink;
//    private String viewProductDetailURL = "";
    private GameMachineInfoMO gameMachineInfoMO = null;
    private TextView watchVideoIcon;
    private LinearLayout gamingAddMoreCreditLayout;
    private TextView gamingMoreCreditReqTV;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_service_screen);
        setResourcesIds();
        gameMachineInfoMO = GamingInfoSingleton.getInstance().getGameMachineInfoMO();
        updateUIAndRV();
    }

    private void setResourcesIds() {

        mViewPager = findViewById(R.id.viewPagerBanner);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        gamingProductRV = findViewById(R.id.game_service_operators_rv);
        operatorBannerDescription = findViewById(R.id.operatorBannerDescription);
//        productInfoLink = findViewById(R.id.productInfoLink);
        findViewById(R.id.singleBottomBanner).setVisibility(View.GONE);
        watchVideoIcon = findViewById(R.id.watchVideoIcon);
        gamingAddMoreCreditLayout = findViewById(R.id.gamingAddMoreCreditLayout);
        gamingMoreCreditReqTV = findViewById(R.id.gamingMoreCreditReqTV);

        //SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();
    }

    private void updateUIAndRV() {
        if (gameMachineInfoMO != null) {

            preference.edit().putString("GAME_SERVICE_BANNERS", gameMachineInfoMO.getMachineLogo()).apply();

            AdsViewPagerAdapter adsViewPagerAdapter = new AdsViewPagerAdapter(this, "GameService");
            mViewPager.startAutoScroll(4500);
            mViewPager.setInterval(4500);
            mViewPager.setAdapter(adsViewPagerAdapter);

            operatorBannerDescription.setVisibility(View.VISIBLE);
//        productInfoLink.setVisibility(View.VISIBLE);

            watchVideoIcon.setVisibility(View.VISIBLE);
            watchVideoIcon.setOnClickListener(onClickListener);

            if (preference.getString("KP_USER_ID", "").equals("0")) {
                operatorBannerDescription.setText(gameMachineInfoMO.getMachineBannerDesc());
            } else {
                operatorBannerDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                operatorBannerDescription.setText("Available Credit - " + gameMachineInfoMO.getAmusementAvailableCredit());
                operatorBannerDescription.setGravity(Gravity.CENTER);
                gamingAddMoreCreditLayout.setVisibility(View.VISIBLE);
                gamingMoreCreditReqTV.setText("This game play requires " + gameMachineInfoMO.getCost() + " credits");
                findViewById(R.id.gamingPlayNowButton).setOnClickListener(onClickListener);
                findViewById(R.id.gamingAddMoreCreditButton).setOnClickListener(onClickListener);
            }

            gamingProductRV.setLayoutManager(new LinearLayoutManager(this));
            gamingProductRV.setAdapter(new GamingProductPricingAdapter(this, gameMachineInfoMO.getPricingOptionsMOList()));
            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider));
            gamingProductRV.addItemDecoration(dividerItemDecoration);

        }
    }

    @Override
    public void onRowItemSelection(int position, int type) {

        GamingInfoSingleton.getInstance().getGameMachineInfoMO().setSelectedPosition(position);
//        startActivity(new Intent(this, GamingOrderDetails.class).putExtra("PRODUCT_POS", position));
        startActivity(new Intent(this, GamingOrderDetails.class));
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.watchVideoIcon) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(gameMachineInfoMO.getMachineVideoURL())));
            } else if (v.getId() == R.id.gamingPlayNowButton) {
                startActivity(new Intent(GamingProductPricing.this, GamingSuccessFailure.class)
                        .putExtra("IS_DIRECT_PLAY_NOW", true));
            } else if (v.getId() == R.id.gamingAddMoreCreditButton) {

            }
        }
    };

}
