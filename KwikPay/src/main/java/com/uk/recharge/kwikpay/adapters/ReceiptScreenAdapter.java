package com.uk.recharge.kwikpay.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

public class ReceiptScreenAdapter extends BaseAdapter 
{
	Activity context;
	ArrayList<HashMap<String, String>> myReceiptArrayList;

	public ReceiptScreenAdapter(Activity context,ArrayList<HashMap<String, String>> myReceiptArrayList) 
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		this.myReceiptArrayList = myReceiptArrayList;

	}

	public int getCount() {
		// TODO Auto-generated method stub
		return myReceiptArrayList.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder 
	{
		TextView txnId, serviceDetails,pin,remarks;

		ViewHolder(View view)
		{
			txnId = (TextView) view.findViewById(R.id.receipt_TxnId);
			serviceDetails = (TextView) view.findViewById(R.id.receipt_ServiceDetails);
			pin = (TextView) view.findViewById(R.id.receipt_Pin);
			remarks = (TextView) view.findViewById(R.id.receipt_Remarks);
		}

	}

	@SuppressWarnings("static-access")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View row=convertView;
		ViewHolder holder;

		if(row==null)
		{
			LayoutInflater inflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row=inflater.inflate(R.layout.single_receipt_row, parent, false);
			holder=new ViewHolder(row);
			row.setTag(holder);
		}

		else
		{
			holder=(ViewHolder)row.getTag();
		}

		// Ashu... Setting Background color to list view at odd and even position
		if(position%2==0)
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewEvenColor));
		}
		else
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewOddColor));
		}

//		String amount=myReceiptArrayList.get(position).get(ProjectConstant.TXN_ORDERID +"/"+"\n"+ProjectConstant.TXN_PAYMENTAMOUNT);
		holder.txnId.setText(myReceiptArrayList.get(position).get(ProjectConstant.TXN_ORDERID )+"/"+"\n\n"
							+myReceiptArrayList.get(position).get(ProjectConstant.TXN_PAYMENTAMOUNT ));
		holder.serviceDetails.setText(myReceiptArrayList.get(position).get(ProjectConstant.TXN_SERVICENAME)+"/"+"\n\n"+
							myReceiptArrayList.get(position).get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
		holder.pin.setText(myReceiptArrayList.get(position).get(ProjectConstant.RECEIPT_RECHARGEPIN));
		holder.remarks.setText(myReceiptArrayList.get(position).get(ProjectConstant.RECEIPT_VENDORDESC));
		
		//SETTING AND IDENTIFYING COLOR OF TEXT TO RED IN CASE OF RECHARGE IS FAIL
		if(!myReceiptArrayList.get(position).get(ProjectConstant.TXN_RECHARGESTATUS).equalsIgnoreCase("T"))
		{
			holder.txnId.setTextColor(context.getResources().getColor(R.color.redColor));
			holder.serviceDetails.setTextColor(context.getResources().getColor(R.color.redColor));
			holder.pin.setTextColor(context.getResources().getColor(R.color.redColor));
			holder.remarks.setTextColor(context.getResources().getColor(R.color.redColor));
		}

		return row;

	}


}

