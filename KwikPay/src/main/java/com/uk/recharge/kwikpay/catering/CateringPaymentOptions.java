package com.uk.recharge.kwikpay.catering;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.PaymentGatewayPage;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.models.CartProduct;
import com.uk.recharge.kwikpay.nativepg.PayPalPaymentScreen;
import com.uk.recharge.kwikpay.nativepg.UpdateJUDOPaymentGateway;
import com.uk.recharge.kwikpay.utilities.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import static com.uk.recharge.kwikpay.catering.CateringCart.cartProducts;
import static com.uk.recharge.kwikpay.catering.MyOrders.couponAmount;
import static com.uk.recharge.kwikpay.catering.MyOrders.couponSrNo;
import static com.uk.recharge.kwikpay.catering.MyOrders.couponTxnId;
import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;

public class CateringPaymentOptions extends Activity implements AsyncRequestListenerViaPost {

    Context context;
    AppDatabase appDatabase;
    PreferenceHelper mPreferencehelper;
    private boolean isVisaChoose = false;
    private int apiHitPosition = 0;
    private SharedPreferences preference;
    private String pgAppTxnId, pgType;
    //    private TextView maximumAuthorizedAmountTV;
    private String apiPGName = "";
    private HashMap<String, String> nativePGDataHashMap;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewID = v.getId();
            if (viewID == R.id.paypal_PG_Button) {
                isVisaChoose = false;
                callPaymentRelatedApis();

            } else if (viewID == R.id.judo_PG_Button) {
                isVisaChoose = true;
                callPaymentRelatedApis();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_payment_options);
        init();
    }

    private void init() {
        context = this;
        appDatabase = Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        findViewById(R.id.paypal_PG_Button).setOnClickListener(onClickListener);
        findViewById(R.id.judo_PG_Button).setOnClickListener(onClickListener);
//        ((TextView) findViewById(R.id.mainHeaderTitleName)).setText(getString(R.string.pay_with));

        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        mPreferencehelper = new PreferenceHelper(this);

        /*findViewById(R.id.backbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
    }

    private void callPaymentRelatedApis() {
        apiHitPosition = 1;
        new LoadResponseViaPost(CateringPaymentOptions.this, formTransactionIdJSON(), true).execute("");
    }

    private String formTransactionIdJSON() {
        try {
            JSONObject json = new JSONObject();

            json.put("APISERVICE", "KPPAYMENTGATEWAYAPPTXNID");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);

            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("SOURCE", "KPAPP");
            json.put("OPABBR", "CATERING");
            if (isVisaChoose) {
                json.put("MOPID", "18");
                json.put("PGSOID", "160");
            } else {
                json.put("MOPID", "22");
                json.put("PGSOID", "164");
            }

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {
        Log.e("Response", "Order Response " + loadedString);
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {

                if (apiHitPosition == 1) {

                    if (parseAppTxnIdDetails(loadedString)) {
                        if (!pgAppTxnId.equals("") && !pgType.equals("")) {
                            if (pgType.equals("NATIVE")) {
                                apiHitPosition = 2;
                                new LoadResponseViaPost(CateringPaymentOptions.this, formPaymentGatewayJSON("KPGETPGDETAILS", false)).execute("");
                            } else {
                                Intent pgIntent = (new Intent(CateringPaymentOptions.this, PaymentGatewayPage.class));
                                pgIntent.putExtra("PG_JSON_PARAM", formPaymentGatewayJSON("WSGetCotrolAndData", true));
                                pgIntent.putExtra("PG_APP_TXN_ID_KEY", pgAppTxnId);
                                startActivity(pgIntent);
                                finish();
                            }
                        }
                    }
                } else if (apiHitPosition == 2) {
                    if (parseReceivedPGDetail(loadedString)) {
                        if (apiPGName.equals("JUDOPG")) {
                            Intent pgIntent = (new Intent(CateringPaymentOptions.this, UpdateJUDOPaymentGateway.class));
                            pgIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                            startActivity(pgIntent);
                        } else if (apiPGName.equalsIgnoreCase("PAYPAL")) {
                            Intent paypalIntent = new Intent(CateringPaymentOptions.this, PayPalPaymentScreen.class);
                            paypalIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                            startActivity(paypalIntent);
                        } else if (apiPGName.equals("COUPONPG")) {
                            Intent couponPgIntent = (new Intent(CateringPaymentOptions.this, UpdateJUDOPaymentGateway.class));
                            couponPgIntent.putExtra("NATIVE_PG_COUPON", nativePGDataHashMap);
                            couponPgIntent.putExtra("NATIVE_PG_COUPON2", "COUPON_DATA");
                            startActivity(couponPgIntent);
                        } else {
                            Toast.makeText(CateringPaymentOptions.this, "Payment Gateway not defined", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        }
    }

    // LOGIC FOR PARSING PG APP TRANSACTION ID FROM WEBSERVICE [apiHitPosition==1]
    public boolean parseAppTxnIdDetails(String operatorResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(operatorResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(CateringPaymentOptions.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.RESPONSE_MSG_TAG)) {
                if (parentResponseObject.getString(ProjectConstant.RESPONSE_MSG_TAG).equals("SUCCESS")) {
                    if (parentResponseObject.has(ProjectConstant.PG_APP_TXN_ID) && parentResponseObject.has("PGTYPE")) {
                        pgAppTxnId = parentResponseObject.getString(ProjectConstant.PG_APP_TXN_ID);
                        pgType = parentResponseObject.getString("PGTYPE");
                    }
                } else {
                    ARCustomToast.showToast(CateringPaymentOptions.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                    return false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // FORMING JSON FOR GETTING PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEBVIEW
    public String formPaymentGatewayJSON(String apiServiceName, boolean isWebView) {
        try {
            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            json.put("iscart", "YES");
            json.put("os", "Android");

            if (isVisaChoose) {
                json.put("mopid", "18");
                json.put("pgsoid", "160");
            } else {
                json.put("mopid", "22");
                json.put("pgsoid", "164");
            }
            json.put("apptransactionid", pgAppTxnId);
            json.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
            json.put("browser", "webview");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("sessionid", preference.getString("SESSION_ID", ""));
            else
                json.put("sessionid", ProjectConstant.SESSIONID);
            json.put("source", "KPAPP");
            json.put("username", preference.getString("KP_USER_NAME", ""));
            json.put("userid", preference.getString("KP_USER_ID", "0"));
            json.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("servicefee", mPreferencehelper.getString("servicefee"));

            if (!isWebView)
                json.put("pgType", pgType);


            json.put("kprequest", getProductJSONArray(apiServiceName));

            Log.e("KP", "kprequest" + json.toString());

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    private JSONArray getProductJSONArray(String apiServiceName) {

        Log.d("KP", "In payment product list size" + String.valueOf(cartProducts.size()));
        JSONArray productJsonArray = new JSONArray();
        int i = 1;
        for (CartProduct cartProduct : cartProducts) {
            try {
                for (int j = 0; j < cartProduct.getSelected_quantity(); j++) {
                    JSONObject mainProductJSONObject = new JSONObject();
                    mainProductJSONObject.put("giftreceiver", "||");
                    mainProductJSONObject.put("machinecode", mPreferencehelper.getString("locationid"));
                    mainProductJSONObject.put("appname", "KWIKPAY");
                    mainProductJSONObject.put("apiservice", apiServiceName);
                    mainProductJSONObject.put("username", preference.getString("KP_USER_NAME", ""));
                    mainProductJSONObject.put("userid", preference.getString("KP_USER_ID", "0"));
                    mainProductJSONObject.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));
                    mainProductJSONObject.put("os", "Android");
                    mainProductJSONObject.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
                    mainProductJSONObject.put("browser", "webview");
                    mainProductJSONObject.put("cpntxnid", couponTxnId);
                    mainProductJSONObject.put("cpnamount", couponAmount);
                    mainProductJSONObject.put("cpnsrno", couponSrNo);
                    mainProductJSONObject.put("discountamount", couponAmount);
                    mainProductJSONObject.put("pgdiscountamount", "0");
                    mainProductJSONObject.put("source", "KPAPP");

                    if (isVisaChoose) {
                        mainProductJSONObject.put("mopid", "18");
                        mainProductJSONObject.put("pgsoid", "160");
                    } else {
                        mainProductJSONObject.put("mopid", "22");
                        mainProductJSONObject.put("pgsoid", "164");
                    }
                    if (ProjectConstant.SESSIONID.equals(""))
                        mainProductJSONObject.put("sessionid", preference.getString("SESSION_ID", ""));
                    else
                        mainProductJSONObject.put("sessionid", ProjectConstant.SESSIONID);

                    mainProductJSONObject.put("mobilenumber", "NA");
                    mainProductJSONObject.put("rechargeamount", "0.00");
                    mainProductJSONObject.put("rechargecurrencyagid", "42");
                    mainProductJSONObject.put("displaycurrencyagid", "42");
                    mainProductJSONObject.put("paymentcurrencyagid", "42");
                    mainProductJSONObject.put("countrycode", "UNITEDKINGDOM");
                    mainProductJSONObject.put("serviceid", "6");
                    mainProductJSONObject.put("operatorcode", "CATERING");
                    mainProductJSONObject.put("processingfee", "0.00");
                    mainProductJSONObject.put("servicefee", "0.00");

                    mainProductJSONObject.put("productcode", cartProduct.getProduct_id());
                    mainProductJSONObject.put("productname", cartProduct.getBrand_name());
                    mainProductJSONObject.put("displayamount", cartProduct.getUnit_price());
                    mainProductJSONObject.put("netpgamount", cartProduct.getUnit_price());
                    mainProductJSONObject.put("categorytype", "MAIN");
                    mainProductJSONObject.put("parentproductcode", String.valueOf(i) + "-" + cartProduct.getProduct_id());

                    productJsonArray.put(mainProductJSONObject);

                    List<Ingredient> ingredientList = cartProduct.getIngredientList();

                    HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();

                    for (final Ingredient ingredient : ingredientList) {

                        List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
                        List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());

                        for (IngredientType ingredientType : ingredientTypeList) {
                            int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                            if (checked == 1) {

                                Log.d("machinecode", mPreferencehelper.getString("locationid"));
                                JSONObject subProductJSONObject = new JSONObject();

                                subProductJSONObject.put("giftreceiver", "||");

                                subProductJSONObject.put("machinecode", mPreferencehelper.getString("locationid"));
                                subProductJSONObject.put("appname", "KWIKPAY");
                                subProductJSONObject.put("apiservice", apiServiceName);
                                subProductJSONObject.put("username", preference.getString("KP_USER_NAME", ""));
                                subProductJSONObject.put("userid", preference.getString("KP_USER_ID", "0"));
                                subProductJSONObject.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));
                                subProductJSONObject.put("os", "Android");
                                subProductJSONObject.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
                                subProductJSONObject.put("browser", "webview");
                                subProductJSONObject.put("cpntxnid", "0");
                                subProductJSONObject.put("cpnamount", "0");
                                subProductJSONObject.put("cpnsrno", "0");
                                subProductJSONObject.put("source", "KPAPP");
                                subProductJSONObject.put("discountamount", "0");
                                subProductJSONObject.put("pgdiscountamount", "0");
                                if (isVisaChoose) {
                                    subProductJSONObject.put("mopid", "18");
                                    subProductJSONObject.put("pgsoid", "160");
                                } else {
                                    subProductJSONObject.put("mopid", "22");
                                    subProductJSONObject.put("pgsoid", "164");
                                }
                                if (ProjectConstant.SESSIONID.equals(""))
                                    subProductJSONObject.put("sessionid", preference.getString("SESSION_ID", ""));
                                else
                                    subProductJSONObject.put("sessionid", ProjectConstant.SESSIONID);

                                subProductJSONObject.put("mobilenumber", "NA");
                                subProductJSONObject.put("rechargeamount", "0.00");
                                subProductJSONObject.put("rechargecurrencyagid", "42");
                                subProductJSONObject.put("displaycurrencyagid", "42");
                                subProductJSONObject.put("paymentcurrencyagid", "42");
                                subProductJSONObject.put("countrycode", "UNITEDKINGDOM");
                                subProductJSONObject.put("serviceid", "6");
                                subProductJSONObject.put("operatorcode", "CATERING");
                                subProductJSONObject.put("processingfee", "0.00");
                                subProductJSONObject.put("servicefee", "0.00");

                                subProductJSONObject.put("productcode", ingredientType.getProduct_id());
                                subProductJSONObject.put("productname", ingredientType.getName());
                                subProductJSONObject.put("displayamount", ingredientType.getUnit_price());
                                subProductJSONObject.put("netpgamount", ingredientType.getUnit_price());
                                subProductJSONObject.put("categorytype", "ADD-ON");
                                subProductJSONObject.put("parentproductcode", String.valueOf(i) + "-" + cartProduct.getProduct_id());

                                productJsonArray.put(subProductJSONObject);
                            }
                        }
                    }
                    i++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.e("KP", " productJsonArray " + String.valueOf(productJsonArray));
        return productJsonArray;
    }

    //LOGIC FOR PARSING THE RECEIVED PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEB VIEW [apiHitPosition==3]
    public boolean parseReceivedPGDetail(String pgDetails) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(pgDetails);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(CateringPaymentOptions.this, "No Response Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    nativePGDataHashMap = new HashMap<>();
                    nativePGDataHashMap.put("PAYMENTREFERENCE", parentResponseObject.optString("PAYMENTREFERENCE"));
                    nativePGDataHashMap.put("JUDOID", parentResponseObject.optString("JUDOID"));
                    nativePGDataHashMap.put("CONSUMERREFERENCE", parentResponseObject.optString("CONSUMERREFERENCE"));
                    nativePGDataHashMap.put("CHECKSUMKEY", parentResponseObject.optString("CHECKSUMKEY"));
                    nativePGDataHashMap.put("ORDERID", parentResponseObject.optString("ORDERID"));
                    nativePGDataHashMap.put("TOKENPAYREFERENCE", parentResponseObject.optString("TOKENPAYREFERENCE"));
                    nativePGDataHashMap.put("SECRETKEY", parentResponseObject.optString("SECRETKEY"));
                    nativePGDataHashMap.put("TOKEN", parentResponseObject.optString("TOKEN"));
                    apiPGName = parentResponseObject.optString("PGNAME");

                    /*

        if (receivedModel != null)
                        nativePGDataHashMap.put("TOTALAMOUNTPAYABLE", receivedModel.getRecentMaxAmount());

                        */

                    nativePGDataHashMap.put("PGNAME", apiPGName);
                    nativePGDataHashMap.put("APPTXNID", pgAppTxnId);

                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(CateringPaymentOptions.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    private String convertToTwoDecimals(String totalPayableAmount) {

        String convertedValues = "";
        double totalPaymentAmountDouble = 0.00;
        double finalTotalPaymentAmount;
        try {
            if (totalPayableAmount != null && !totalPayableAmount.isEmpty()) {
                totalPaymentAmountDouble = Double.parseDouble(totalPayableAmount);
            }
            finalTotalPaymentAmount = 0.20 + totalPaymentAmountDouble;
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            convertedValues = decimalFormat.format(finalTotalPaymentAmount);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedValues;
    }

}
