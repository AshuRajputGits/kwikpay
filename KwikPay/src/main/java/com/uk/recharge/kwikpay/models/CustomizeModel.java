package com.uk.recharge.kwikpay.models;

public class CustomizeModel {
    private String sub_category;
    private String category_action;
    private String product_id;
    private String brand_name;
    private String unit_price;
    private String core_price;
    private String category;

    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    public String getCategory_action() {
        return category_action;
    }

    public void setCategory_action(String category_action) {
        this.category_action = category_action;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getCore_price() {
        return core_price;
    }

    public void setCore_price(String core_price) {
        this.core_price = core_price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
