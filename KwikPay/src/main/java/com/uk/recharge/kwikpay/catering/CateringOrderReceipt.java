package com.uk.recharge.kwikpay.catering;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.models.CartProduct;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import static com.uk.recharge.kwikpay.catering.CateringCart.cartProducts;
import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;
import static com.uk.recharge.kwikpay.utilities.Utility.ParseFloat;

public class CateringOrderReceipt extends AppCompatActivity {

    TableLayout tableLayout;
    Context context;
    AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catering_order_receipt);

        init();

        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(getIntent().getStringExtra("PG_APP_TXN_ID_KEY"), BarcodeFormat.QR_CODE, 400, 400);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            ((ImageView) findViewById(R.id.img_result_qr)).setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private void init() {

        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        tableLayout = findViewById(R.id.table_main);

        context = this;

        appDatabase = Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(CateringOrderReceipt.this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        View th = LayoutInflater.from(CateringOrderReceipt.this).inflate(R.layout.table_header_ls, null, false);

        tableLayout.addView(th);

        int i = 1;

        Log.d("cart products size", String.valueOf(cartProducts.size()));

        for (CartProduct cartProduct : cartProducts) {

            View tr = LayoutInflater.from(context).inflate(R.layout.table_row, null, false);

            View separator = new View(context);
            separator.setPadding(10, 10, 10, 10);
            separator.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
            separator.setBackgroundColor(Color.GRAY);

            tableLayout.addView(separator);

            TextView txtsrno = tr.findViewById(R.id.txtsrno);
            TextView txtitem = tr.findViewById(R.id.product);
            TextView product_description = tr.findViewById(R.id.product_description);
            TextView txtquantity = tr.findViewById(R.id.qty);
            TextView txtamount = tr.findViewById(R.id.txtamount);

            txtsrno.setText(String.valueOf(i));
            txtitem.setText(cartProduct.getBrand_name());
            txtquantity.setText(String.valueOf(cartProduct.getSelected_quantity()));
            txtamount.setText(getPrice(cartProduct));
            product_description.setText(getDescription(cartProduct));
            tableLayout.addView(tr);
            i++;
        }

        findViewById(R.id.backbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    private String getPrice(CartProduct cartProduct) {

        float total_price = ParseFloat(cartProduct.getUnit_price()) * cartProduct.getSelected_quantity();
        List<Ingredient> ingredientList = cartProduct.getIngredientList();
        HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();
        for (final Ingredient ingredient : ingredientList) {
            List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
            List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());
            for (IngredientType ingredientType : ingredientTypeList) {
                int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                if (checked == 1) {
                    total_price += ParseFloat(ingredientType.getUnit_price()) * cartProduct.getSelected_quantity();
                }
            }
        }
        return "£ " + new DecimalFormat("##.00").format(total_price);
    }

    private String getDescription(CartProduct cartProduct) {
        StringBuilder description = new StringBuilder();
        List<Ingredient> ingredientList = cartProduct.getIngredientList();
        HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();
        for (final Ingredient ingredient : ingredientList) {
            List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
            List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());

            for (IngredientType ingredientType : ingredientTypeList) {
                int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                if (checked == 1) {
                    if (description.length() != 0)
                        description.append(",");
                    description.append(ingredientType.getName());
                }
            }
        }
        return description.toString();
    }
}
