package com.uk.recharge.kwikpay.gameservice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.adapters.AdsViewPagerAdapter;
import com.uk.recharge.kwikpay.gameservice.GameServiceAdapter.GameServiceItemListener;
import com.uk.recharge.kwikpay.utilities.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by Ashu Rajput on 1/15/2018.
 */

public class GameServicesScreen extends AppCompatActivity implements AsyncRequestListenerViaPost, GameServiceItemListener {

    private AutoScrollViewPager mViewPager;
    private SharedPreferences preference;
    private RecyclerView gameServiceRecyclerView = null;
    private ArrayList<HashMap<String, String>> serviceArrayList = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_service_screen);
        setResourcesIds();
        new LoadResponseViaPost(this, formDigitalService(), true).execute("");
    }

    private void setResourcesIds() {

        mViewPager = findViewById(R.id.viewPagerBanner);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        gameServiceRecyclerView = findViewById(R.id.game_service_operators_rv);

        //SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();
    }

    //FORM DIGITAL SERVICE JSON PARAMETERS
    public String formDigitalService() {
        try {

            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPDIGITALSERVICES");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("SERVICEID", "3");
            json.put("PUSH_TOKEN", preference.getString("URBAN_AIRSHIP_CHANNEL_ID", ""));
            json.put("APPNAME", "KWIKPAY");

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                parseMyServiceData(loadedString);
            } else {
                if (DeviceNetConnectionDetector.checkDataConnWifiMobile(this))
                    ARCustomToast.showToast(this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
                else
                    ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);

                finish();
            }
        } else {
            if (DeviceNetConnectionDetector.checkDataConnWifiMobile(this))
                ARCustomToast.showToast(this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
            else
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);

            finish();
        }
    }

    // LOGIC FOR PARSING GAME AND SERVICES FROM WEBSERVICE
    public void parseMyServiceData(String operatorResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(operatorResponse);
            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(this, "No Response Tag", Toast.LENGTH_LONG);
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    if (parentResponseObject.has("GAMES_BANNERS")) {
                        preference.edit().putString("GAME_SERVICE_BANNERS", parentResponseObject.getString("GAMES_BANNERS")).apply();
                        AdsViewPagerAdapter adsViewPagerAdapter = new AdsViewPagerAdapter(this, "GameService");
                        mViewPager.startAutoScroll(4500);
                        mViewPager.setInterval(4500);
                        mViewPager.setAdapter(adsViewPagerAdapter);
                    }

                    if (parentResponseObject.has(ProjectConstant.GAMESERVICE_KPDIGITALSERVICE)) {
                        HashMap<String, String> serviceHashMap;
                        JSONArray topupJsonArray = parentResponseObject.getJSONArray(ProjectConstant.GAMESERVICE_KPDIGITALSERVICE);
                        serviceArrayList = new ArrayList<>();

                        for (int i = 0; i < topupJsonArray.length(); i++) {
                            childResponseObject = topupJsonArray.getJSONObject(i);

                            serviceHashMap = new HashMap<>();
                            serviceHashMap.put(ProjectConstant.API_OPNAME, childResponseObject.optString(ProjectConstant.API_OPNAME));
                            serviceHashMap.put(ProjectConstant.API_OPTEMPACTIVEFLAG, childResponseObject.optString(ProjectConstant.API_OPTEMPACTIVEFLAG));
                            serviceHashMap.put(ProjectConstant.API_OPID, childResponseObject.optString(ProjectConstant.API_OPID));
                            serviceHashMap.put(ProjectConstant.API_OPIMGPATH, childResponseObject.optString(ProjectConstant.API_OPIMGPATH));
                            serviceHashMap.put(ProjectConstant.API_OPCODE, childResponseObject.optString(ProjectConstant.API_OPCODE));
                            serviceHashMap.put(ProjectConstant.API_OPACTIVEFLAG, childResponseObject.optString(ProjectConstant.API_OPACTIVEFLAG));
                            serviceHashMap.put(ProjectConstant.API_OPCIRCLEACTIVEFLAG, childResponseObject.optString(ProjectConstant.API_OPCIRCLEACTIVEFLAG));
                            serviceHashMap.put("OP_DESC", childResponseObject.optString("OP_DESC"));
                            serviceArrayList.add(serviceHashMap);

                            gameServiceRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                            gameServiceRecyclerView.setAdapter(new GameServiceAdapter(this, serviceArrayList));
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider));
                            gameServiceRecyclerView.addItemDecoration(dividerItemDecoration);
                        }
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRowItemSelection(int position, int type) {

        startActivity(new Intent(this, GameServiceProducts.class)
                .putExtra("OPERATOR_ID", serviceArrayList.get(position).get(ProjectConstant.API_OPID))
                .putExtra("OPERATOR_CODE", serviceArrayList.get(position).get(ProjectConstant.API_OPCODE)));

//                .putExtra("OPERATOR_SELECTED_HASHMAP", serviceArrayList.get(position))

    }
}
