package com.uk.recharge.kwikpay.gameservice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.CommonStaticPages;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.LoginScreen;
import com.uk.recharge.kwikpay.OrderDetails;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.adapters.AdsViewPagerAdapter;
import com.uk.recharge.kwikpay.gameservice.GameServiceAdapter.GameServiceItemListener;
import com.uk.recharge.kwikpay.utilities.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by Ashu Rajput on 22-01-2018.
 */

public class GameServiceProducts extends AppCompatActivity implements AsyncRequestListenerViaPost, GameServiceItemListener {

    private AutoScrollViewPager mViewPager;
    private SharedPreferences preference;
    private RecyclerView gameServiceRecyclerView = null;
    private String selectedOperatorCode = "", selectedOperatorID = "";
    //    private HashMap<String, String> receivedOperatorHashMap = null;
    private ArrayList<HashMap<String, String>> topUpPlansArrayList = null;
    private TextView operatorBannerDescription, productInfoLink;
    private String viewProductDetailURL = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_service_screen);

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("OPERATOR_CODE"))
                selectedOperatorCode = getIntent().getStringExtra("OPERATOR_CODE");
            if (receivedBundle.containsKey("OPERATOR_ID"))
                selectedOperatorID = getIntent().getStringExtra("OPERATOR_ID");

           /* if (receivedBundle.containsKey("OPERATOR_SELECTED_HASHMAP"))
                receivedOperatorHashMap = (HashMap<String, String>) receivedBundle.getSerializable("OPERATOR_SELECTED_HASHMAP");*/
        }

        setResourcesIds();
        new LoadResponseViaPost(this, formTopUpProductJson(), true).execute("");

        operatorBannerDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(GameServiceProducts.this, CommonStaticPages.class)
                        .putExtra("STATIC_PAGE_NAME", "PDT_DESCRIPTION_PAGE")
                        .putExtra("VIEW_PDT_URL", viewProductDetailURL));
            }
        });

        productInfoLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(GameServiceProducts.this, CommonStaticPages.class)
                        .putExtra("STATIC_PAGE_NAME", "PDT_DESCRIPTION_PAGE")
                        .putExtra("VIEW_PDT_URL", viewProductDetailURL));
            }
        });
    }

    private void setResourcesIds() {

        mViewPager = findViewById(R.id.viewPagerBanner);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        gameServiceRecyclerView = findViewById(R.id.game_service_operators_rv);
        operatorBannerDescription = findViewById(R.id.operatorBannerDescription);
        productInfoLink = findViewById(R.id.productInfoLink);
        findViewById(R.id.singleBottomBanner).setVisibility(View.GONE);

        //SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();
    }

    //FORM PLAN PRODUCT JSON PARAMETERS
    public String formTopUpProductJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPDIGITALSERVICESPRODUCTS");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("OPERATORCODE", selectedOperatorCode);
            json.put("SERVICEID", "3");

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                parseResponseOnProductSelection(loadedString);
            } else {
                if (DeviceNetConnectionDetector.checkDataConnWifiMobile(this))
                    ARCustomToast.showToast(this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
                else
                    ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);

                finish();
            }
        } else {
            if (DeviceNetConnectionDetector.checkDataConnWifiMobile(this))
                ARCustomToast.showToast(this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
            else
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);

            finish();
        }
    }

    // LOGIC FOR PARSING TOP_UP PLANS FROM WEBSERVICE
    public boolean parseResponseOnProductSelection(String operatorResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(operatorResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(this, "No Response Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);
            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    if (parentResponseObject.has("PROVIDER_BANNER")) {
                        preference.edit().putString("GAME_SERVICE_BANNERS", parentResponseObject.getString("PROVIDER_BANNER")).apply();
                        AdsViewPagerAdapter adsViewPagerAdapter = new AdsViewPagerAdapter(this, "GameService");
                        mViewPager.startAutoScroll(4500);
                        mViewPager.setInterval(4500);
                        mViewPager.setAdapter(adsViewPagerAdapter);
                    }

                    if (parentResponseObject.has("PROVIDER_BANNER_DESC")) {
                        if (!parentResponseObject.optString("PROVIDER_BANNER_DESC").isEmpty()) {
                            operatorBannerDescription.setVisibility(View.VISIBLE);
                            productInfoLink.setVisibility(View.VISIBLE);
                            operatorBannerDescription.setText(parentResponseObject.getString("PROVIDER_BANNER_DESC"));
                        }
                    }

                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_PRODUCTS)) {
                        JSONArray topupJsonArray = parentResponseObject.getJSONArray(ProjectConstant.TOPUP_API_PRODUCTS);
                        topUpPlansArrayList = new ArrayList<>();
                        HashMap<String, String> topUpHashMap;

                        for (int i = 0; i < topupJsonArray.length(); i++) {
                            childResponseObject = topupJsonArray.getJSONObject(i);

                            topUpHashMap = new HashMap<>();
                            topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYAMT, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYAMT));
                            topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
                            topUpHashMap.put(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH));
                            topUpHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH));

                            // ADDING THESE TWO EXTRA AS IT CONTAINING AT SO MANY PLACES IN CLASS [KEEPING IT]
                            topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH));
                            topUpHashMap.put(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH));

                            topUpHashMap.put(ProjectConstant.PAYNOW_RECHARGEAMOUNT, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGEAMT));
                            topUpHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
                            topUpHashMap.put(ProjectConstant.TOPUP_API_PAYMENTAMT, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTAMT));
                            topUpHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYAGID));
                            topUpHashMap.put(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYIMGPATH));
                            topUpHashMap.put(ProjectConstant.PAYNOW_OPERATORNAME, childResponseObject.getString(ProjectConstant.PAYNOW_OPERATORNAME));
                            topUpHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, childResponseObject.getString("PAYMENTPROCESSINGFEE"));
                            topUpHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, childResponseObject.getString("PAYMENTSERVICEFEE"));
                            topUpHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, childResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION));
                            topUpHashMap.put(ProjectConstant.API_OPIMGPATH, childResponseObject.optString("PRODUCT_IMG"));
                            topUpHashMap.put("OPERATORPRODUCTCODE", childResponseObject.optString("OPERATORPRODUCTCODE"));
                            topUpPlansArrayList.add(topUpHashMap);

                            gameServiceRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                            gameServiceRecyclerView.setAdapter(new GameServiceAdapter(this, topUpPlansArrayList, true));
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider));
                            gameServiceRecyclerView.addItemDecoration(dividerItemDecoration);
                        }
                    }

                    if (parentResponseObject.has("URLTERMS"))
                        ProjectConstant.tncURL = parentResponseObject.getString("URLTERMS");
                    if (parentResponseObject.has("URLVIEWPRODUCTDETAIL"))
                        viewProductDetailURL = parentResponseObject.getString("URLVIEWPRODUCTDETAIL");
                    if (parentResponseObject.has("URLPROVIDERHELP"))
                        ProjectConstant.providerDetailsURL = parentResponseObject.getString("URLPROVIDERHELP");

                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onRowItemSelection(int position, int type) {
        createHashMapForOrderDetail(position);
    }

    public void createHashMapForOrderDetail(int position) {
        try {
            HashMap<String, String> payNowHashMap = new HashMap<>();
            payNowHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE));
            payNowHashMap.put(ProjectConstant.PAYNOW_OPERATORNAME, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_OPERATORNAME));
            payNowHashMap.put(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE));
            payNowHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PROCESSINGFEE));
            payNowHashMap.put(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE));
            payNowHashMap.put(ProjectConstant.PAYNOW_RECHARGEAMOUNT, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGEAMOUNT));
            payNowHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_SERVICEFEE));
            payNowHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PLANDESCRIPTION));
            payNowHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID));

            payNowHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
            payNowHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
            payNowHashMap.put(ProjectConstant.TOPUP_API_DISPLAYAMT, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
            payNowHashMap.put(ProjectConstant.TOPUP_API_PAYMENTAMT, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_PAYMENTAMT));
            payNowHashMap.put(ProjectConstant.API_OPIMGPATH, topUpPlansArrayList.get(position).get(ProjectConstant.API_OPIMGPATH));

            payNowHashMap.put(ProjectConstant.PAYNOW_MOBILENUMBER, "N/A");
            payNowHashMap.put(ProjectConstant.API_OPCODE, selectedOperatorCode);
            payNowHashMap.put(ProjectConstant.API_OPID, selectedOperatorID);
            payNowHashMap.put(ProjectConstant.TXN_COUNTRYCODE, "UNITEDKINGDOM");
            payNowHashMap.put("SERVICEID", "3");
            payNowHashMap.put("AGID", "44");
            payNowHashMap.put("CARTMSG", "");
            payNowHashMap.put("OPERATORPRODUCTCODE", topUpPlansArrayList.get(position).get("OPERATORPRODUCTCODE"));

//            controlNetOffExit = true;
            if (preference.getString("KP_USER_ID", "").equals("0")) {
                Intent logInIntent = new Intent(this, LoginScreen.class);
                logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
                startActivity(logInIntent);
            } else {
                Map<String, Object> event = new HashMap<>();
                event.put(AFInAppEventParameterName.PRICE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGEAMOUNT));
                event.put(AFInAppEventParameterName.CONTENT_TYPE, "GameAndServices");
                event.put(AFInAppEventParameterName.CURRENCY, "GBP");
                event.put(AFInAppEventParameterName.QUANTITY, 1);

                AppsFlyerLib.trackEvent(this, AFInAppEventType.ADD_PAYMENT_INFO, event);

                Intent payNowIntent = new Intent(this, OrderDetails.class);
                payNowIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
                startActivity(payNowIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
