package com.uk.recharge.kwikpay.kwikcharge;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.BillingAddressScreen;
import com.uk.recharge.kwikpay.EmailVerificationScreen;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog.LocationIdListener;
import com.uk.recharge.kwikpay.kwikcharge.models.ConnectorListMO;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.kwikcharge.payment.KwikChargePaymentOptions;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 9/27/2017.
 */

public class KwikChargeOrderScreen extends AppCompatActivity implements AsyncRequestListenerViaPost, LocationIdListener {

    private TextView kcOrderACButton, kcOrderDCButton, kcOrderChademoButton;
    private TextView kcRegistrationNo, kcMaxTimeSession, kcMaxAmount, kcRateKWpH;
    private TextView addressPostalCodeTV;
    private SharedPreferences preference;
    private static final int CHANGE_REGISTRATION_CODE = 4;
    private static final int EMAIL_VERIFICATION_REQUEST_CODE = 5;
    private static final int BILLING_ADDRESS_REQUEST_CODE = 6;
    private static final int EDIT_BILLING_ADDRESS_REQUEST_CODE = 7;
    private LocationIdMO receivedModel;
    //    private String defaultConnectorType = "";
    //    private String connectorType = "";
    private int selectedConnectorPosition = -1;
    protected boolean isAddressFound = false;
    private String directPaymentScreenFlag = "";
    private int apiHitPosition = 0;
    private String deviceDBID = "", authorisingAmount = "0", serviceFee = "0.00";
//    private boolean isType2Available = false, isCCSAvailable = false, isChademoAvailable = false;
//    private String type2ConnectorNumber = "", ccsConnectorNumber = "", chademoConnectorNumber = "";
//    private boolean isType2ConnectorNumberFound = false;
//    private boolean isChademoCCSConnectorNumberFound = false;

    //new Development
    private ImageView chargerConnectorOption1IV;
    private ImageView chargerConnectorOption2IV;
    private ImageView chargerConnectorOption3IV;

    private LinearLayout chargerConnector1Layout;
    private LinearLayout chargerConnector2Layout;
    private LinearLayout chargerConnector3Layout;

    private RelativeLayout chargerConnector1RelLayout;
    private RelativeLayout chargerConnector2RelLayout;
    private RelativeLayout chargerConnector3RelLayout;

    private ImageView redDot1;
    private ImageView redDot2;
    private ImageView redDot3;

    private ArrayList<ConnectorListMO> receivedConnectorMOList = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kwik_charge_order_screen);
        setResourceIds();

        //INTRODUCING NEW API, TO GET THE STATUS OF ADDRESS ALREADY EXIST IN CENTRAL D.B. OR NOT
        apiHitPosition = 1;
        LoadResponseViaPost asyncObject = new LoadResponseViaPost(KwikChargeOrderScreen.this, formConfirmAddressJSON(), true);
        asyncObject.execute("");
        asyncObject = null;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
//        connectorType = "";
        selectedConnectorPosition = -1;
        updateKCOrderUI();
        setDefaultSelectionToChargerTypes();

        //CALLING BELOW API TO SENT RECENT DATA TO SERVER FOR UPDATE
        apiHitPosition = 2;
        new LoadResponseViaPost(KwikChargeOrderScreen.this, formCheckConnectorDeviceJSON(), true).execute("");
    }

    private String formConfirmAddressJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KP_GETADDRESS_BY_CONFIRMDETAIL");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("OPERATORCODE", "ELECTRICBLUE");
            json.put("SERVICEID", "4");
            json.put("COUNTRYCODE", "");
            json.put("PAYMENTAMT", "");
            json.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("SUBSCRIBERNUM", "");
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setResourceIds() {

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        findViewById(R.id.editRegNoTV).setOnClickListener(onClickListener);
        findViewById(R.id.kcOrderProceedButton).setOnClickListener(onClickListener);

        kcOrderACButton = findViewById(R.id.kcOrderACButton);
        kcOrderDCButton = findViewById(R.id.kcOrderDCButton);
        kcOrderChademoButton = findViewById(R.id.kcOrderChademoButton);
        addressPostalCodeTV = findViewById(R.id.addressPostalCodeTV);

       /* kcOrderACButton.setOnClickListener(onClickListener);
        kcOrderDCButton.setOnClickListener(onClickListener);
        kcOrderChademoButton.setOnClickListener(onClickListener);*/

        findViewById(R.id.addressEditButton).setOnClickListener(onClickListener);
        findViewById(R.id.headerHomeBtn).setOnClickListener(onClickListener);

        kcRegistrationNo = findViewById(R.id.kcRegistrationNo);
        kcMaxTimeSession = findViewById(R.id.kcMaxTimeSession);
        kcMaxAmount = findViewById(R.id.kcMaxAmount);
        kcRateKWpH = findViewById(R.id.kcRateKWpH);

        chargerConnectorOption1IV = findViewById(R.id.chargerConnectorOption1IV);
        chargerConnectorOption2IV = findViewById(R.id.chargerConnectorOption2IV);
        chargerConnectorOption3IV = findViewById(R.id.chargerConnectorOption3IV);

        chargerConnector1Layout = findViewById(R.id.chargerConnector1Layout);
        chargerConnector2Layout = findViewById(R.id.chargerConnector2Layout);
        chargerConnector3Layout = findViewById(R.id.chargerConnector3Layout);

        chargerConnector1RelLayout = findViewById(R.id.chargerConnector1RelLayout);
        chargerConnector2RelLayout = findViewById(R.id.chargerConnector2RelLayout);
        chargerConnector3RelLayout = findViewById(R.id.chargerConnector3RelLayout);

        redDot1 = findViewById(R.id.redDot1);
        redDot2 = findViewById(R.id.redDot2);
        redDot3 = findViewById(R.id.redDot3);

        chargerConnector1Layout.setOnClickListener(onClickListener);
        chargerConnector2Layout.setOnClickListener(onClickListener);
        chargerConnector3Layout.setOnClickListener(onClickListener);

        updateKCOrderUI();
    }

    private void updateKCOrderUI() {
        try {
            receivedModel = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();
            kcRegistrationNo.setText(receivedModel.getRegistrationNumber());
            kcMaxTimeSession.setText(receivedModel.getRecentMaxTime() + " minutes");
            kcMaxAmount.setText(Html.fromHtml("\u00a3") + " " + receivedModel.getRecentMaxAmount());
            kcRateKWpH.setText(Html.fromHtml("\u00a3") + " " + receivedModel.getRecentRate());
            addressPostalCodeTV.setText(receivedModel.getPostCode());

            receivedConnectorMOList = receivedModel.getConnectorListMOList();

            if (receivedConnectorMOList != null && receivedConnectorMOList.size() > 0) {
                for (byte b = 0; b < receivedConnectorMOList.size(); b++) {
                    if (b == 0) {
                        chargerConnector1Layout.setVisibility(View.VISIBLE);
                        kcOrderACButton.setText(receivedConnectorMOList.get(b).getConnectorName());
                        try {
                            Glide.with(this).load(receivedConnectorMOList.get(b).getConnectorLogoURL()).into(chargerConnectorOption1IV);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (!receivedConnectorMOList.get(b).getConnectorStatus().equalsIgnoreCase("Available")) {
//                            chargerConnector1Layout.setAlpha(0.5f);
                            chargerConnectorOption1IV.setAlpha(0.5f);
                            kcOrderACButton.setAlpha(0.5f);
                            redDot1.setVisibility(View.VISIBLE);
                        }

                    } else if (b == 1) {
                        chargerConnector2Layout.setVisibility(View.VISIBLE);
                        kcOrderDCButton.setText(receivedConnectorMOList.get(b).getConnectorName());
                        try {
                            Glide.with(this).load(receivedConnectorMOList.get(b).getConnectorLogoURL()).into(chargerConnectorOption2IV);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (!receivedConnectorMOList.get(b).getConnectorStatus().equalsIgnoreCase("Available")) {
//                            chargerConnector2Layout.setAlpha(0.5f);
                            chargerConnectorOption2IV.setAlpha(0.5f);
                            kcOrderDCButton.setAlpha(0.5f);
                            redDot2.setVisibility(View.VISIBLE);
                        }

                    } else if (b == 2) {
                        chargerConnector3Layout.setVisibility(View.VISIBLE);
                        kcOrderChademoButton.setText(receivedConnectorMOList.get(b).getConnectorName());
                        try {
                            Glide.with(this).load(receivedConnectorMOList.get(b).getConnectorLogoURL()).into(chargerConnectorOption3IV);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (!receivedConnectorMOList.get(b).getConnectorStatus().equalsIgnoreCase("Available")) {
//                            chargerConnector3Layout.setAlpha(0.5f);
                            chargerConnectorOption3IV.setAlpha(0.5f);
                            kcOrderChademoButton.setAlpha(0.5f);
                            redDot3.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            /*if (receivedModel.getAC() != null && receivedModel.getAC().equals("1"))
                setSelectorOnChargingTypeTV(1);
            if (receivedModel.getDC() != null && receivedModel.getDC().equals("1"))
                setSelectorOnChargingTypeTV(2);
            if (receivedModel.getCHADEMO() != null && receivedModel.getCHADEMO().equals("1"))
                setSelectorOnChargingTypeTV(3);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();

            if (viewId == R.id.editRegNoTV) {
                startActivityForResult(new Intent(KwikChargeOrderScreen.this, KCRegistrationNumberDetails.class), CHANGE_REGISTRATION_CODE);
            } else if (viewId == R.id.kcOrderProceedButton) {
                if (preference.getBoolean("IS_KP_USER_EMAIL_VERIFIED", false) || preference.getBoolean("EMAIL_IS_VERIFIED", false)) {
                    finalCallForPaymentProcess();
                } else
                    startActivityForResult(new Intent(KwikChargeOrderScreen.this, EmailVerificationScreen.class), EMAIL_VERIFICATION_REQUEST_CODE);

            } else if (viewId == R.id.chargerConnector1Layout) {
                if (receivedConnectorMOList != null && receivedConnectorMOList.get(0).getConnectorStatus().equalsIgnoreCase("Available"))
                    setSelectorOnChargingTypeTV(1);
            } else if (viewId == R.id.chargerConnector2Layout) {
                if (receivedConnectorMOList != null && receivedConnectorMOList.get(1).getConnectorStatus().equalsIgnoreCase("Available"))
                    setSelectorOnChargingTypeTV(2);
            } else if (viewId == R.id.chargerConnector3Layout) {
                if (receivedConnectorMOList != null && receivedConnectorMOList.get(2).getConnectorStatus().equalsIgnoreCase("Available"))
                    setSelectorOnChargingTypeTV(3);
            } else if (viewId == R.id.addressEditButton) {
                startActivityForResult(new Intent(KwikChargeOrderScreen.this, BillingAddressScreen.class), EDIT_BILLING_ADDRESS_REQUEST_CODE);
            } else if (viewId == R.id.headerHomeBtn) {
                Intent intent = new Intent(KwikChargeOrderScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    };

    private void finalCallForPaymentProcess() {
        if (directPaymentScreenFlag.equals("1")) {
            initiatePaymentProcess();
        } else if (directPaymentScreenFlag.equals("0")) {
//            if (isAddressFound)
            if (preference.getBoolean("IS_KP_USER_POSTCODE_DEFINED", false))
                initiatePaymentProcess();
            else {
                Intent billingIntent = new Intent(KwikChargeOrderScreen.this, BillingAddressScreen.class);
                startActivityForResult(billingIntent, BILLING_ADDRESS_REQUEST_CODE);
            }
        } else
            initiatePaymentProcess();
    }

    /*  type 1: AC
        type 2: DC
        type 3: CHADEMO */

    private void setSelectorOnChargingTypeTV(int type) {
        if (type == 1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                /*kcOrderACButton.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape, null));
                kcOrderDCButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape, null));
                kcOrderChademoButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape, null));*/

                chargerConnector1RelLayout.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape, null));
                chargerConnector2RelLayout.setBackgroundResource(0);
                chargerConnector3RelLayout.setBackgroundResource(0);

            } else {
                /*kcOrderACButton.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape));
                kcOrderDCButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));
                kcOrderChademoButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));*/

                chargerConnector1RelLayout.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape));
                chargerConnector2RelLayout.setBackgroundResource(0);
                chargerConnector3RelLayout.setBackgroundResource(0);
            }
            /*kcOrderACButton.setTextColor(getResources().getColor(R.color.whiteColor));
            kcOrderDCButton.setTextColor(getResources().getColor(R.color.black));
            kcOrderChademoButton.setTextColor(getResources().getColor(R.color.black));*/
//            connectorType = "AC";
            selectedConnectorPosition = 0;
//            defaultConnectorType = receivedModel.getConnector1Desc();
        } else if (type == 2) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                /*kcOrderDCButton.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape, null));
                kcOrderACButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape, null));
                kcOrderChademoButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape, null));*/

                chargerConnector1RelLayout.setBackgroundResource(0);
                chargerConnector2RelLayout.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape, null));
                chargerConnector3RelLayout.setBackgroundResource(0);

            } else {
                /*kcOrderDCButton.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape));
                kcOrderACButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));
                kcOrderChademoButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));*/

                chargerConnector1RelLayout.setBackgroundResource(0);
                chargerConnector2RelLayout.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape));
                chargerConnector3RelLayout.setBackgroundResource(0);

            }
            /*kcOrderDCButton.setTextColor(getResources().getColor(R.color.whiteColor));
            kcOrderACButton.setTextColor(getResources().getColor(R.color.black));
            kcOrderChademoButton.setTextColor(getResources().getColor(R.color.black));*/

//            connectorType = "DC";
            selectedConnectorPosition = 1;
//            defaultConnectorType = receivedModel.getConnector2Desc();
        } else if (type == 3) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                /*kcOrderChademoButton.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape, null));
                kcOrderDCButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape, null));
                kcOrderACButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape, null));*/

                chargerConnector1RelLayout.setBackgroundResource(0);
                chargerConnector2RelLayout.setBackgroundResource(0);
                chargerConnector3RelLayout.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape, null));

            } else {
                /*kcOrderChademoButton.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape));
                kcOrderACButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));
                kcOrderDCButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));*/

                chargerConnector1RelLayout.setBackgroundResource(0);
                chargerConnector2RelLayout.setBackgroundResource(0);
                chargerConnector3RelLayout.setBackground(getResources().getDrawable(R.drawable.selected_oval_shape));
            }
            /*kcOrderChademoButton.setTextColor(getResources().getColor(R.color.whiteColor));
            kcOrderACButton.setTextColor(getResources().getColor(R.color.black));
            kcOrderDCButton.setTextColor(getResources().getColor(R.color.black));*/
//            connectorType = "CHADEMO";
            selectedConnectorPosition = 2;
//            defaultConnectorType = receivedModel.getConnector3Desc();
        }
    }

    private void setDefaultSelectionToChargerTypes() {
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        } else {
            kcOrderChademoButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));
            kcOrderACButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));
            kcOrderDCButton.setBackground(getResources().getDrawable(R.drawable.default_oval_shape));
        }
        kcOrderACButton.setTextColor(getResources().getColor(R.color.black));
        kcOrderDCButton.setTextColor(getResources().getColor(R.color.black));
        kcOrderChademoButton.setTextColor(getResources().getColor(R.color.black));*/

        chargerConnector1RelLayout.setBackgroundResource(0);
        chargerConnector2RelLayout.setBackgroundResource(0);
        chargerConnector3RelLayout.setBackgroundResource(0);
    }

    private void initiatePaymentProcess() {
        /*if (connectorType != null && connectorType.isEmpty())
            ARCustomToast.showToast(KwikChargeOrderScreen.this, "Please select connector type", Toast.LENGTH_LONG);
        else if (connectorType != null && connectorType.equals("AC") && !isType2Available)
            ARCustomToast.showToast(KwikChargeOrderScreen.this, "Type2 connector is currently unavailable", Toast.LENGTH_LONG);
        else if (connectorType != null && connectorType.equals("DC") && !isCCSAvailable)
            ARCustomToast.showToast(KwikChargeOrderScreen.this, "CCS connector is currently unavailable", Toast.LENGTH_LONG);
        else if (connectorType != null && connectorType.equals("CHADEMO") && !isChademoAvailable)
            ARCustomToast.showToast(KwikChargeOrderScreen.this, "CHAdeMO connector is currently unavailable", Toast.LENGTH_LONG);
        else
            confirmConnectorFromUserDialog();*/

        if (selectedConnectorPosition == -1) {
            ARCustomToast.showToast(KwikChargeOrderScreen.this, "Please select connector type", Toast.LENGTH_LONG);
        } else {
            apiHitPosition = 2;
            LoadResponseViaPost asyncObject = new LoadResponseViaPost(KwikChargeOrderScreen.this, formCheckConnectorDeviceJSON(), true);
            asyncObject.execute("");
            asyncObject = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHANGE_REGISTRATION_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                updateKCOrderUI();
            }
        } else if (requestCode == EDIT_BILLING_ADDRESS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("REFRESH_POSTAL_CODE")) {
                    addressPostalCodeTV.setText(data.getStringExtra("REFRESH_POSTAL_CODE"));
//                    isAddressFound = true;
                }
            }
        } else if (requestCode == BILLING_ADDRESS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    isAddressFound = true;
                    preference.edit().putBoolean("IS_KP_USER_POSTCODE_DEFINED", true).apply();
                    initiatePaymentProcess();
                }
            }
        } else if (requestCode == EMAIL_VERIFICATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    finalCallForPaymentProcess();
                }
            }
        }
    }

    @Override
    public void onRequestComplete(String loadedString) {

//        Log.e("Response", "Response : " + loadedString);

        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 1) {
                    parseAddressConfirmStatus(loadedString);
                } else if (apiHitPosition == 2) {
                    parseConnectorDeviceDetails(loadedString);
                }
            }
        }
    }

    private String formCheckConnectorDeviceJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KC_DEVICE_STATUS");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            if (receivedModel != null) {
                json.put("LOCATION_NUMBER", receivedModel.getLocationNumber());
                json.put("REGISTRATIONNUMBER", receivedModel.getRegistrationNumber());
                json.put("RATE", receivedModel.getRecentRate());
                json.put("MAXTIME", receivedModel.getRecentMaxTime());
                json.put("MAXAMOUNT", receivedModel.getRecentMaxAmount());
                json.put("MAKE", receivedModel.getRecentMake());
                json.put("MODEL", receivedModel.getRecentModel());
                json.put("BATTERYTYPE", receivedModel.getRecentBatteryType());
                json.put("CHARGERTYPE", "High");
            }
            json.put("DEVICEOS", "ANDROID");
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //*******************************************************************************************//
    // LOGIC FOR PARSING THE RESPONSE OF ADDRESS CONFIRMATION                                    //
    //*******************************************************************************************//
    private void parseAddressConfirmStatus(String response) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(response);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(KwikChargeOrderScreen.this, "No Response Tag", Toast.LENGTH_LONG);
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has("DIRECT_PAYMENT_SCREEN")) {
                        directPaymentScreenFlag = parentResponseObject.optString("DIRECT_PAYMENT_SCREEN");

                        SharedPreferences.Editor prefEditor = preference.edit();
                        if (parentResponseObject.has("IS_POSTCODE_DEFINED") && !parentResponseObject.getString("IS_POSTCODE_DEFINED").isEmpty())
                            prefEditor.putBoolean("IS_KP_USER_POSTCODE_DEFINED", Boolean.parseBoolean(parentResponseObject.optString("IS_POSTCODE_DEFINED")));
                        if (parentResponseObject.has("IS_EMAIL_VERIFIED") && !parentResponseObject.getString("IS_EMAIL_VERIFIED").isEmpty())
                            prefEditor.putBoolean("IS_KP_USER_EMAIL_VERIFIED", Boolean.parseBoolean(parentResponseObject.optString("IS_EMAIL_VERIFIED")));
                        prefEditor.apply();

                        if (directPaymentScreenFlag.equals("0")) {
                            // IF FLAG=1: ADDRESS FOUND IN THE D.B.
                            // IF FLAG=0: ADDRESS NOT FOUND IN THE D.B.
                            if (parentResponseObject.optString("ADDRESSRESPONSECODE").equals("1")) {
//                                addressLayout.setVisibility(View.VISIBLE);
//                                addressPostalCodeTV.setText(parentResponseObject.optString("POSTALCODE"));
                                isAddressFound = true;
                            }
                        }
                    }

                } else {
                    if (parentResponseObject.has("DESCRIPTION"))
                        ARCustomToast.showToast(KwikChargeOrderScreen.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //*******************************************************************************************//
    // LOGIC FOR PARSING THE RESPONSE OF CONNECTOR DEVICE DETAILS                                //
    //*******************************************************************************************//
    private void parseConnectorDeviceDetails(String response) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(response);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(KwikChargeOrderScreen.this, "No Response Tag", Toast.LENGTH_LONG);
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    deviceDBID = parentResponseObject.optString("DEVICE_DB_ID");
                    authorisingAmount = parentResponseObject.optString("AUTHORISING_AMOUNT");
                    serviceFee = parentResponseObject.optString("SERVICEFEE");

                    JSONArray connectorListArray = parentResponseObject.getJSONArray("CONNECTOR_LIST");
                    if (connectorListArray != null && connectorListArray.length() > 0) {

                        JSONObject connectorJsonObject = connectorListArray.getJSONObject(selectedConnectorPosition);
                        if (connectorJsonObject.optString("ConnectorStatus").equalsIgnoreCase("Available")) {
                            confirmConnectorFromUserDialog();
                        } else {
                            String statusMsg = connectorJsonObject.optString("ConnectorType") + " connector is currently unavailable";
                            ARCustomToast.showToast(KwikChargeOrderScreen.this, statusMsg, Toast.LENGTH_LONG);
                        }

                        /*for (byte b = 0; b < connectorListArray.length(); b++) {
                            JSONObject connectorJsonObject = connectorListArray.getJSONObject(b);
                            if (connectorJsonObject.optString("ConnectorType").contains("62196-2") && connectorJsonObject.optString("LocalConnectorNumber").equals("1")) {
                                if (connectorJsonObject.optString("ConnectorStatus").equalsIgnoreCase("Available")) {
                                    isType2Available = true;
                                    type2ConnectorNumber = connectorJsonObject.optString("LocalConnectorNumber");
                                    isType2ConnectorNumberFound = true;
                                }

                            } else if (connectorJsonObject.optString("ConnectorType").contains("62196-2") && connectorJsonObject.optString("LocalConnectorNumber").equals("2")) {
                                if (connectorJsonObject.optString("ConnectorStatus").equalsIgnoreCase("Available")) {
                                    isType2Available = true;
                                    if (!isType2ConnectorNumberFound)
                                        type2ConnectorNumber = connectorJsonObject.optString("LocalConnectorNumber");
                                }

                            } else if ((connectorJsonObject.optString("ConnectorType").contains("ChaDeMo") || connectorJsonObject.optString("ConnectorType").contains("ComboCCS"))
                                    && connectorJsonObject.optString("LocalConnectorNumber").equals("1")) {
                                if (connectorJsonObject.optString("ConnectorStatus").equalsIgnoreCase("Available")) {
                                    isCCSAvailable = isChademoAvailable = true;
                                    ccsConnectorNumber = chademoConnectorNumber = connectorJsonObject.optString("LocalConnectorNumber");
                                    isChademoCCSConnectorNumberFound = true;
                                }

                            } else if ((connectorJsonObject.optString("ConnectorType").contains("ChaDeMo") || connectorJsonObject.optString("ConnectorType").contains("ComboCCS"))
                                    && connectorJsonObject.optString("LocalConnectorNumber").equals("2")) {
                                if (connectorJsonObject.optString("ConnectorStatus").equalsIgnoreCase("Available")) {

                                    isCCSAvailable = isChademoAvailable = true;
                                    if (!isChademoCCSConnectorNumberFound)
                                        ccsConnectorNumber = chademoConnectorNumber = connectorJsonObject.optString("LocalConnectorNumber");
                                }
                            }
                        }*/
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION"))
                        ARCustomToast.showToast(KwikChargeOrderScreen.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void confirmConnectorFromUserDialog() {
        try {
            String connectorMessage = receivedConnectorMOList.get(selectedConnectorPosition).getConnectorDescription() + "\n" + "Is this correct?";
            DialogFragment dialog = new CustomDialog();
            dialog.show(getSupportFragmentManager(), "LocationIdDialog");
            Bundle bundle = new Bundle();
            bundle.putString("Charger_Address", connectorMessage);
            bundle.putBoolean("ShowAddressDialog", true);
            dialog.setArguments(bundle);
            dialog.setCancelable(false);
        } catch (Exception e) {
        }
    }

    @Override
    public void getLocationId(String locationID) {

        LocationIdMO finalChargerLocationModel = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();

        /*if (connectorType.equals("AC")) {
            finalChargerLocationModel.setConnectorDescSelected(finalChargerLocationModel.getConnector1Desc());
            finalChargerLocationModel.setConnectorImageType("TYPE2");
            finalChargerLocationModel.setTempLocalConnectorNumber(type2ConnectorNumber);
        } else if (connectorType.equals("DC")) {
            finalChargerLocationModel.setConnectorDescSelected(finalChargerLocationModel.getConnector2Desc());
            finalChargerLocationModel.setConnectorImageType("CCS");
            finalChargerLocationModel.setTempLocalConnectorNumber(ccsConnectorNumber);
        } else if (connectorType.equals("CHADEMO")) {
            finalChargerLocationModel.setConnectorDescSelected(finalChargerLocationModel.getConnector3Desc());
            finalChargerLocationModel.setConnectorImageType("CHADEMO");
            finalChargerLocationModel.setTempLocalConnectorNumber(chademoConnectorNumber);
        }*/

        finalChargerLocationModel.setSelectedConnectorName(receivedConnectorMOList.get(selectedConnectorPosition).getConnectorName());
        finalChargerLocationModel.setSelectedConnectorURL(receivedConnectorMOList.get(selectedConnectorPosition).getConnectorLogoURL());
        finalChargerLocationModel.setConnectorType(receivedConnectorMOList.get(selectedConnectorPosition).getConnectorType());
        finalChargerLocationModel.setAuthorisingAmount(authorisingAmount);
        finalChargerLocationModel.setServiceFee(serviceFee);
        finalChargerLocationModel.setTempLocalConnectorNumber(receivedConnectorMOList.get(selectedConnectorPosition).getLocalConnectorNumber());
        finalChargerLocationModel.setTempDeviceDBID(deviceDBID);

        KwikChargeDetailsSingleton.getInstance().setLocationIdMO(finalChargerLocationModel);
        startActivity(new Intent(KwikChargeOrderScreen.this, KwikChargePaymentOptions.class));
    }
}
