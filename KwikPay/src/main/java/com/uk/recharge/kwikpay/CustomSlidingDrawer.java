package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.uk.recharge.kwikpay.adapters.HeaderMenuAdapter;
import com.uk.recharge.kwikpay.myaccount.MyTransactionActivity;

@SuppressWarnings("deprecation")
public class CustomSlidingDrawer {
    private Activity activityContext;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    //	private ArrayAdapter<String> adapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private ImageView headerMenuImage, homeButtonImage;
    private boolean killMyPage = false;
//    LinearLayout cartIconLayout;
    SharedPreferences preferences;

    public CustomSlidingDrawer(Activity activityContext, DrawerLayout mDrawerLayout,
                               ListView mDrawerList, ImageView headerMenuImage, ImageView homeButtonImage) {
        this.activityContext = activityContext;
        this.mDrawerLayout = mDrawerLayout;
        this.mDrawerList = mDrawerList;
        this.headerMenuImage = headerMenuImage;
        this.homeButtonImage = homeButtonImage;
    }

    // CONSTRUCTOR WITH CART, HOME IMAGE AND MENU BUTTON

    public CustomSlidingDrawer(Activity activityContext, DrawerLayout mDrawerLayout,
                               ListView mDrawerList, ImageView headerMenuImage, ImageView homeButtonImage, LinearLayout cartIconLayout) {
        this.activityContext = activityContext;
        this.mDrawerLayout = mDrawerLayout;
        this.mDrawerList = mDrawerList;
        this.headerMenuImage = headerMenuImage;
        this.homeButtonImage = homeButtonImage;
//        this.cartIconLayout = cartIconLayout;
    }

    // CONSTRUCTOR WITH CART, HOME IMAGE, MENU BUTTON AND BOOLEAN VALUE TO KILL THE CURRENT ACTIVITY IF FROM MENU

    public CustomSlidingDrawer(Activity activityContext, DrawerLayout mDrawerLayout,
                               ListView mDrawerList, ImageView headerMenuImage, ImageView homeButtonImage, LinearLayout cartIconLayout, boolean killMyPage) {
        this.activityContext = activityContext;
        this.mDrawerLayout = mDrawerLayout;
        this.mDrawerList = mDrawerList;
        this.headerMenuImage = headerMenuImage;
        this.homeButtonImage = homeButtonImage;
        this.killMyPage = killMyPage;
    }


    public void createMenuDrawer() {
        // TODO Auto-generated method stub

        preferences = activityContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE);

        if (activityContext instanceof HomeScreen) {
            mDrawerLayout.setScrimColor(activityContext.getResources().getColor(android.R.color.transparent));
        } else {
            Drawable shadow = ContextCompat.getDrawable(activityContext, R.drawable.drawer_shadow);
            mDrawerLayout.setDrawerShadow(shadow, GravityCompat.START);

			/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Drawable shadow=ContextCompat.getDrawable(activityContext, R.drawable.drawer_shadow);
				mDrawerLayout.setDrawerShadow(shadow, GravityCompat.START);
			} else {
				Drawable shadow=activityContext.getResources().getDrawable(R.drawable.drawer_shadow);
				mDrawerLayout.setDrawerShadow(shadow, GravityCompat.START);
			}*/

        }

        HeaderMenuAdapter adapter = new HeaderMenuAdapter(activityContext);

        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(activityContext,
                mDrawerLayout, R.drawable.menu_icon, R.string.drawer1, R.string.drawer2) {
            public void onDrawerClosed(View view) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

            public void onDrawerOpened(View drawerView) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        headerMenuImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    final InputMethodManager imm = (InputMethodManager) activityContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(arg0.getWindowToken(), 0);

                    mDrawerLayout.openDrawer(mDrawerList);
                    mDrawerLayout.bringToFront();
                }
            }
        });

        if (homeButtonImage != null) {
            homeButtonImage.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("InlinedApi")
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(activityContext, HomeScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activityContext.startActivity(intent);
                    activityContext.finish();

                    ProjectConstant.ISLOGINPAGE_ACTIVE = false;
                }
            });
        }

        /*if (cartIconLayout != null) {
            cartIconLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    activityContext.startActivity(new Intent(activityContext, MyCartScreen.class));
                }
            });
        }*/

    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @SuppressLint("InlinedApi")
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:

                    Intent intent = new Intent(activityContext, HomeScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activityContext.startActivity(intent);
                    activityContext.finish();

                    if (killMyPage)
                        activityContext.finish();
                    break;

			/*case 1:
                if(preferences.getString("KP_USER_ID", "").equals("0"))
				{
					if(ProjectConstant.ISLOGINPAGE_ACTIVE)
					{
						
					}
					else
					{
						ProjectConstant.ISITCOMINGFROM_MYACCOUNT=true;
						activityContext.startActivity(new Intent(activityContext,LoginScreen.class));
					}
					
				}
				else
				{
					ProjectConstant.MY_TRANSACTION_COUNTER++;
					ProjectConstant.MY_ACCOUNT_COUNTER=ProjectConstant.HOWITWORKS_COUNTER=ProjectConstant.CONTACTUS_COUNTER=0;
					ProjectConstant.TOU_COUNTER=ProjectConstant.ABOUTUS=0;
					
					activityContext.startActivity(new Intent(activityContext,MyTransaction.class));
					if(killMyPage)
					{
						if(ProjectConstant.MY_TRANSACTION_COUNTER>1)
							activityContext.finish();
					}
				}
				
				break;*/

                case 1:

                    if (preferences.getString("KP_USER_ID", "").equals("0")) {
                        if (ProjectConstant.ISLOGINPAGE_ACTIVE) {

                        } else {
                            ProjectConstant.ISITCOMINGFROM_MYACCOUNT = true;
                            activityContext.startActivity(new Intent(activityContext, LoginScreen.class));
                        }
                    } else {
                        ProjectConstant.MY_ACCOUNT_COUNTER++;
                        ProjectConstant.MY_TRANSACTION_COUNTER = ProjectConstant.HOWITWORKS_COUNTER = ProjectConstant.CONTACTUS_COUNTER = 0;
                        ProjectConstant.TOU_COUNTER = ProjectConstant.ABOUTUS = 0;

//                        activityContext.startActivity(new Intent(activityContext, MyTransaction.class));
                        activityContext.startActivity(new Intent(activityContext, MyTransactionActivity.class));
                        if (killMyPage) {
                            if (ProjectConstant.MY_ACCOUNT_COUNTER > 1)
                                activityContext.finish();
                        }
                    }

                    break;

                case 2:
                    ProjectConstant.HOWITWORKS_COUNTER++;
                    ProjectConstant.MY_TRANSACTION_COUNTER = ProjectConstant.MY_ACCOUNT_COUNTER = ProjectConstant.CONTACTUS_COUNTER = 0;
                    ProjectConstant.TOU_COUNTER = ProjectConstant.ABOUTUS = 0;

                    activityContext.startActivity(new Intent(activityContext, CommonStaticPages.class).putExtra("STATIC_PAGE_NAME", "HOWITWORK_PAGE"));

                    if (killMyPage) {
                        if (ProjectConstant.HOWITWORKS_COUNTER > 1)
                            activityContext.finish();
                    }
                    break;

                case 3:
                    ProjectConstant.CONTACTUS_COUNTER++;
                    ProjectConstant.MY_TRANSACTION_COUNTER = ProjectConstant.MY_ACCOUNT_COUNTER = ProjectConstant.HOWITWORKS_COUNTER = 0;
                    ProjectConstant.TOU_COUNTER = ProjectConstant.ABOUTUS = 0;
                    activityContext.startActivity(new Intent(activityContext, ContactUs.class));

                    if (killMyPage) {
                        if (ProjectConstant.CONTACTUS_COUNTER > 1)
                            activityContext.finish();
                    }

                    break;

                case 4:
                    ProjectConstant.TOU_COUNTER++;
                    ProjectConstant.MY_TRANSACTION_COUNTER = ProjectConstant.MY_ACCOUNT_COUNTER = ProjectConstant.HOWITWORKS_COUNTER = 0;
                    ProjectConstant.CONTACTUS_COUNTER = ProjectConstant.ABOUTUS = 0;
                    activityContext.startActivity(new Intent(activityContext, TermsOfUsage.class));

                    if (killMyPage) {
                        if (ProjectConstant.TOU_COUNTER > 1)
                            activityContext.finish();
                    }

                    break;

                case 5:

                    ProjectConstant.ABOUTUS++;
                    ProjectConstant.MY_TRANSACTION_COUNTER = ProjectConstant.MY_ACCOUNT_COUNTER = ProjectConstant.HOWITWORKS_COUNTER = 0;
                    ProjectConstant.CONTACTUS_COUNTER = ProjectConstant.TOU_COUNTER = 0;

                    Intent aboutUsIntent = new Intent(activityContext, AboutUs.class);
                    aboutUsIntent.putExtra("SCREEN_NAME", "");
                    activityContext.startActivity(aboutUsIntent);

                    if (killMyPage) {
                        if (ProjectConstant.ABOUTUS > 1)
                            activityContext.finish();
                    }

                    break;

                case 6:
                    if (activityContext instanceof CommonStaticPages)
                        mDrawerLayout.closeDrawers();
                    else {
                        Intent privacyPolicy = new Intent(activityContext, CommonStaticPages.class);
                        privacyPolicy.putExtra("STATIC_PAGE_NAME", "PRIVACY_POLICY");
                        activityContext.startActivity(privacyPolicy);
                    }
                    break;

                default:
                    break;
            }

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            mDrawerLayout.closeDrawer(mDrawerList);

            mDrawerList.setItemChecked(position, true);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }
}
