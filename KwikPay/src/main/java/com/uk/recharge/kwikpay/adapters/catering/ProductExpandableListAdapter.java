package com.uk.recharge.kwikpay.adapters.catering;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Product;
import com.uk.recharge.kwikpay.RoomDataBase.Models.ProductType;
import com.uk.recharge.kwikpay.catering.CustomizeOrder;
import com.uk.recharge.kwikpay.models.CartProduct;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.uk.recharge.kwikpay.catering.CateringCart.cartProductId;
import static com.uk.recharge.kwikpay.catering.CateringCart.cartProducts;
import static com.uk.recharge.kwikpay.catering.CateringCart.logCart;
import static com.uk.recharge.kwikpay.catering.CateringCart.setQTY;
import static com.uk.recharge.kwikpay.catering.CateringCart.updateCartTotal;
import static com.uk.recharge.kwikpay.catering.CateringCart.updateCartVisibility;
import static com.uk.recharge.kwikpay.catering.RestoMenuList.cartCountTextView;
import static com.uk.recharge.kwikpay.catering.RestoMenuList.cartLayout;
import static com.uk.recharge.kwikpay.catering.RestoMenuList.cartTotalTextView;
import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;
import static com.uk.recharge.kwikpay.utilities.Utility.ParseInteger;
import static com.uk.recharge.kwikpay.utilities.Utility.randomUUID;

public class ProductExpandableListAdapter extends BaseExpandableListAdapter {

    private float total_price = 0;
    private Context _context;
    private AppDatabase appDatabase;
    private List<Product> productList;
    private HashMap<Integer, List<ProductType>> productTypePerProductList;

    public ProductExpandableListAdapter(Context context, List<Product> productList,
                                        HashMap<Integer, List<ProductType>> productTypePerProductList) {
        this._context = context;
        this.productList = productList;
        this.productTypePerProductList = productTypePerProductList;

        appDatabase = Room.databaseBuilder(_context.getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.productTypePerProductList.get(groupPosition)
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        final ProductType productType = (ProductType) getChild(groupPosition, childPosition);

        final List<Ingredient> ingredientList = appDatabase.ingredientDao().getAll(productType.getParent_id());

        //and then in your tv display data
        //   yourTextView.setText(teamData.getGroupid());
        TextView txtListChild = convertView.findViewById(R.id.lblListItem);
        TextView price = convertView.findViewById(R.id.price);
        final TextView prod_quantity = convertView.findViewById(R.id.prod_quantity);
        final TextView customize = convertView.findViewById(R.id.customize);
        TextView product_description = convertView.findViewById(R.id.product_description);
        final ImageView addToCartImageView = convertView.findViewById(R.id.addtocart);
        final LinearLayout add_remove_item = convertView.findViewById(R.id.add_remove_item);
        final TextView blank_text = convertView.findViewById(R.id.blanktext);
        ImageButton remove_quantity = convertView.findViewById(R.id.remove_quantity);
        ImageButton add_quantity = convertView.findViewById(R.id.add_quantity);

        if (ingredientList.isEmpty())
            customize.setVisibility(View.GONE);

        if (cartProductId.contains(productType.getProduct_id())) {
            add_remove_item.setVisibility(View.VISIBLE);
            addToCartImageView.setVisibility(View.GONE);
            blank_text.setVisibility(View.GONE);
        } else {
            addToCartImageView.setImageResource(R.drawable.addsign);
        }

        txtListChild.setText(productType.getName());
        price.setText(getPrice(productType));
        product_description.setText(productType.getProduct_description());
        prod_quantity.setText(setQTY(productType.getProduct_id()));

        addToCartImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Add to Cart ", "Product ID : " + productType.getProduct_id());
                CartProduct cartProduct = new CartProduct();
                cartProduct.setId(randomUUID());
                cartProduct.setCategory(productType.getParent_id());
                cartProduct.setBrand_name(productType.getName());
                cartProduct.setAvailable_quantity(productType.getAvailable_quantity());
                cartProduct.setCategory_action(productType.getCategory_action());
                cartProduct.setCore_price(productType.getCore_price());
                cartProduct.setImage(productType.getImage());
                cartProduct.setProduct_description(productType.getProduct_description());
                cartProduct.setProduct_id(productType.getProduct_id());
                cartProduct.setSelected_quantity(1);
                cartProduct.setUnit_price(productType.getUnit_price());
                cartProduct.setSize(productType.getSize());
                cartProduct.setIngredientList(ingredientList);
                cartProduct.setViewCheckedPerIngredientList(getDefaultMapping(ingredientList));
                cartProductId.add(productType.getProduct_id());
                cartProducts.add(cartProduct);
                add_remove_item.setVisibility(View.VISIBLE);
                addToCartImageView.setVisibility(View.GONE);
                blank_text.setVisibility(View.GONE);

                prod_quantity.setText(setQTY(productType.getProduct_id()));
                updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
                updateCartVisibility(cartLayout);
                logCart();
                Toast.makeText(_context, "Added To Cart", Toast.LENGTH_SHORT).show();
                if (!ingredientList.isEmpty()) {
                    Intent intent = new Intent(_context, CustomizeOrder.class);
                    intent.putExtra("parent_id", productType.getParent_id());
                    intent.putExtra("product_type", productType.getName());
                    intent.putExtra("id", cartProduct.getId());
                    _context.startActivity(intent);
                }
            }
        });

        customize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Add to Cart ", "Product ID : " + productType.getProduct_id());
                CartProduct cartProduct = new CartProduct();
                cartProduct.setId(randomUUID());
                cartProduct.setCategory(productType.getParent_id());
                cartProduct.setBrand_name(productType.getName());
                cartProduct.setAvailable_quantity(productType.getAvailable_quantity());
                cartProduct.setCategory_action(productType.getCategory_action());
                cartProduct.setCore_price(productType.getCore_price());
                cartProduct.setImage(productType.getImage());
                cartProduct.setProduct_description(productType.getProduct_description());
                cartProduct.setProduct_id(productType.getProduct_id());
                cartProduct.setSelected_quantity(1);
                cartProduct.setUnit_price(productType.getUnit_price());
                cartProduct.setSize(productType.getSize());
                cartProduct.setIngredientList(ingredientList);
                cartProduct.setViewCheckedPerIngredientList(getDefaultMapping(ingredientList));
                cartProductId.add(productType.getProduct_id());
                cartProducts.add(cartProduct);
                add_remove_item.setVisibility(View.VISIBLE);
                addToCartImageView.setVisibility(View.GONE);
                blank_text.setVisibility(View.GONE);
                prod_quantity.setText(setQTY(productType.getProduct_id()));
                updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
                updateCartVisibility(cartLayout);
                logCart();
                Toast.makeText(_context, "Added To Cart", Toast.LENGTH_SHORT).show();
                if (!ingredientList.isEmpty()) {
                    Intent intent = new Intent(_context, CustomizeOrder.class);
                    intent.putExtra("parent_id", productType.getParent_id());
                    intent.putExtra("product_type", productType.getName());
                    intent.putExtra("id", cartProduct.getId());
                    _context.startActivity(intent);
                }
            }


        });

        add_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Add to Cart ", "Product ID : " + productType.getProduct_id());
                CartProduct cartProduct = new CartProduct();
                cartProduct.setId(randomUUID());
                cartProduct.setCategory(productType.getParent_id());
                cartProduct.setBrand_name(productType.getName());
                cartProduct.setAvailable_quantity(productType.getAvailable_quantity());
                cartProduct.setCategory_action(productType.getCategory_action());
                cartProduct.setCore_price(productType.getCore_price());
                cartProduct.setImage(productType.getImage());
                cartProduct.setProduct_description(productType.getProduct_description());
                cartProduct.setProduct_id(productType.getProduct_id());
                cartProduct.setSelected_quantity(1);
                cartProduct.setUnit_price(productType.getUnit_price());
                cartProduct.setSize(productType.getSize());
                cartProduct.setIngredientList(ingredientList);
                cartProduct.setViewCheckedPerIngredientList(getDefaultMapping(ingredientList));
                cartProductId.add(productType.getProduct_id());
                cartProducts.add(cartProduct);
                add_remove_item.setVisibility(View.VISIBLE);
                addToCartImageView.setVisibility(View.GONE);
                blank_text.setVisibility(View.GONE);
                prod_quantity.setText(setQTY(productType.getProduct_id()));
                updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
                updateCartVisibility(cartLayout);
                logCart();

                Toast.makeText(_context, "Added To Cart", Toast.LENGTH_SHORT).show();

                if (!ingredientList.isEmpty()) {
                    Intent intent = new Intent(_context, CustomizeOrder.class);
                    intent.putExtra("parent_id", productType.getParent_id());
                    intent.putExtra("product_type", productType.getName());
                    intent.putExtra("id", cartProduct.getId());
                    _context.startActivity(intent);
                }
            }

        });

        remove_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int count = ParseInteger(setQTY(productType.getProduct_id()));

                if (count == 1) {
                    add_remove_item.setVisibility(View.GONE);
                    addToCartImageView.setVisibility(View.VISIBLE);
                    blank_text.setVisibility(View.VISIBLE);
                } else {
                    add_remove_item.setVisibility(View.VISIBLE);
                    addToCartImageView.setVisibility(View.GONE);
                    blank_text.setVisibility(View.GONE);
                }

                int index = 0;
                for (String product_id : cartProductId) {
                    if (product_id.equals(productType.getProduct_id())) {
                        index = cartProductId.indexOf(product_id);
                        break;
                    }
                }

                cartProductId.remove(index);
                cartProducts.remove(index);
                Log.d("Removed From Cart ", "Product ID : " + productType.getProduct_id());
                prod_quantity.setText(setQTY(productType.getProduct_id()));
                updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
                updateCartVisibility(cartLayout);
                logCart();
                Toast.makeText(_context, "Removed From Cart", Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }

    private String getPrice(ProductType productType) {

        if (cartProductId.isEmpty() || !cartProductId.contains(productType.getProduct_id())) {
            float total = Float.parseFloat(productType.getUnit_price());
            final List<Ingredient> ingredientList = appDatabase.ingredientDao().getAll(productType.getParent_id());
            for (final Ingredient ingredient : ingredientList) {
                List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
                int index = 0;
                if (ingredient.getCategory_action().equals("MULTIPLE_SELECTION"))
                    continue;

                for (IngredientType ingredientType : ingredientTypeList) {
                    if (index == 0) {
                        index++;
                        float unit_price = Float.parseFloat(ingredientType.getUnit_price());
                        total += unit_price;
                    } else {
                        break;
                    }
                }
            }
            return "£ " + new DecimalFormat("##.00").format(total);

        } else {
            int index = 0;
            for (String product_id : cartProductId) {
                if (product_id.equals(productType.getProduct_id())) {
                    index = cartProductId.indexOf(product_id);
                    break;
                }
            }

            CartProduct cartProduct = cartProducts.get(index);
            float total_price = 0;
            total_price = Float.parseFloat(cartProduct.getUnit_price());
            List<Ingredient> ingredientList = cartProduct.getIngredientList();
            HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();
            for (final Ingredient ingredient : ingredientList) {
                List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
                List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());
                for (IngredientType ingredientType : ingredientTypeList) {
                    int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                    if (checked == 1) {
                        total_price += Float.parseFloat(ingredientType.getUnit_price());
                    }
                }
            }
            return "£ " + new DecimalFormat("##.00").format(total_price);
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.productTypePerProductList.get(groupPosition)
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.productList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.productList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Product product = (Product) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        ImageView img = convertView.findViewById(R.id.img);

        if (isExpanded) {
            img.setImageResource(R.drawable.chevrons);
            img.setRotation(90);
        } else {
            img.setImageResource(R.drawable.chevrons);
            img.setRotation(0);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        //   lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(product.getName());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private HashMap<String, List<Integer>> getDefaultMapping(List<Ingredient> ingredientList) {
        HashMap<String, List<Integer>> viewCheckedPerIngredientList = new HashMap<>();
        for (final Ingredient ingredient : ingredientList) {
            List<Integer> idList = new ArrayList<>();
            if (ingredient.getCategory_action().equals("MULTIPLE_SELECTION")) {
                List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
                for (int index = 0; index < ingredientTypeList.size(); index++)
                    idList.add(0);

            } else {
                List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
                for (int index = 0; index < ingredientTypeList.size(); index++) {
                    if (index == 0)
                        idList.add(1);
                    idList.add(0);
                }
            }
            viewCheckedPerIngredientList.put(ingredient.getName(), idList);
        }

        return viewCheckedPerIngredientList;
    }
}