package com.uk.recharge.kwikpay;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.JobIntentService;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.model.DeviceAppInformation;
import com.ar.library.validation.FieldValidationWithMessage;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.uk.recharge.kwikpay.catering.MyOrders;
import com.uk.recharge.kwikpay.gamingarcade.GamingProductPricing;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.kwikcharge.models.RegistrationNumbersMO;
import com.uk.recharge.kwikpay.kwikvend.KVProductCartScreen;
import com.uk.recharge.kwikpay.services.SendEmailForVerificationService;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;
import com.uk.recharge.kwikpay.singleton.RegNoDetailsSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

//import com.google.android.gms.auth.api.Auth;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public class LoginScreen extends FragmentActivity implements AsyncRequestListenerViaPost, GoogleApiClient.OnConnectionFailedListener {
    private TextView emailAddressLabel, passwordLabel;
    private EditText loginEmailID, loginPassword;
    private SharedPreferences preferences;
    private static final int SIGNUP_REQUEST_CODE = 102;
    private static final int REQUEST_APP_SETTINGS = 103;
    private HashMap<String, String> receivedPayNowDetailHashMap;
    private boolean isHashMapFulled = false;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private String fb_gpName = "", fb_gpGender = "", fb_gpId = "", fb_gpEmailId = "", fb_gpAgentCode = "", fb_gpAccessToken = "";
    private int apiHitPosition = 0;

    // GOOGLE PLUS CLASS VARIABLE
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 9010;
    private static final int MOBILE_OTP_REQUEST_CODE = 100;
    private boolean isThirdPartyLoginExecuted = false;
    private SignInButton googlePlusLoginBtn;
    private TextView login_SignUpTV;
    private boolean isComingFromElectricBlueScreen = false;
    private boolean isComingFromPayAsYouGo = false, isComingFromCatering = false, isComingFromKVScreen = false,
            isComingFromGamingScreen = false;
    private JSONArray regNoJsonArray;
//    private String receivedLocationId = "";

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

//		FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.loginscreen);
        setUpIds();

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("IS_COMING_FROM_CATERING")) {
                isComingFromCatering = true;
            }
            if (receivedBundle.containsKey("PAYNOW_DETAILS_HASHMAP")) {
                isHashMapFulled = true;
                receivedPayNowDetailHashMap = (HashMap<String, String>) receivedBundle.getSerializable("PAYNOW_DETAILS_HASHMAP");
            } else if (receivedBundle.containsKey("IS_COMING_FROM_ELEC_BLUE")) {
                if (receivedBundle.getBoolean("IS_COMING_FROM_ELEC_BLUE", false)) {
                    hideLoginButtonsLayout();
                    isComingFromElectricBlueScreen = true;
                }
            } else if (receivedBundle.containsKey("COMING_FROM_PAY_AS_GO")) {
                if (receivedBundle.getBoolean("COMING_FROM_PAY_AS_GO", false)) {
                    isComingFromPayAsYouGo = true;
                }
            } else if (receivedBundle.containsKey("IS_COMING_FROM_KV_SCREEN")) {
                if (receivedBundle.getBoolean("IS_COMING_FROM_KV_SCREEN", false)) {
                    isComingFromKVScreen = true;
                }
            } else if (receivedBundle.containsKey("IS_COMING_FROM_GAMING_SCREEN")) {
                if (receivedBundle.getBoolean("IS_COMING_FROM_GAMING_SCREEN", false)) {
                    isComingFromGamingScreen = true;
                }
            }
        }

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // TODO Auto-generated method stub
                                try {

                                    fb_gpId = object.optString("id").toString();
                                    fb_gpGender = object.optString("gender").toString();

                                    if (fb_gpGender.equals("male"))
                                        fb_gpGender = "M";
                                    else if (fb_gpGender.equals("female"))
                                        fb_gpGender = "F";

                                    fb_gpEmailId = object.optString("email").toString();
                                    fb_gpName = object.optString("name").toString();
                                    fb_gpAgentCode = "FACEBOOK";

                                    AccessToken token = AccessToken.getCurrentAccessToken();
                                    if (token == null)
                                        fb_gpAccessToken = fb_gpId;
                                    else
                                        fb_gpAccessToken = token.getToken().toString();

//                                    Log.e("FBGP", "TokenFB  " + fb_gpAccessToken);

                                    //SETTING SCREEN NAMES TO APPS FLYER
                                    try {
                                        EventsLoggerSingleton.getInstance().setCommonEventLogs(LoginScreen.this, "after facebook logged in_ANDROID");
                                      /*  String eventName = "AfterLoginFacebook";
                                        AppsFlyerLib.trackEvent(LoginScreen.this, eventName, null);
                                        EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
                                        ((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    isThirdPartyLoginExecuted = true;
                                    startActivityForResult(new Intent(LoginScreen.this, OTPVerificationScreen.class).putExtra("LOGIN_OTP_CALLER", true)
                                            .putExtra("LOGIN_OTP_COMING_FROM_FB", true), MOBILE_OTP_REQUEST_CODE);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });

        getRunTimeIMEINumber();
    }

    /*private void getRunTimeAdvertisementID() {

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.KITKAT) {
            getRunTimeIMEINumber();
        } else {
            AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    AdvertisingIdClient.Info idInfo = null;
                    try {
                        idInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String advertId = null;
                    try {
                        advertId = idInfo.getId();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    return advertId;
                }

                @Override
                protected void onPostExecute(String advertId) {
                    Toast.makeText(getApplicationContext(), "Check ID", Toast.LENGTH_SHORT).show();
                    Log.e("AdvertiseId", "AdvertiseId" + advertId);
                }

            };
            task.execute();
        }
    }*/

    private void hideLoginButtonsLayout() {
        loginButton.setVisibility(View.GONE);
        googlePlusLoginBtn.setVisibility(View.GONE);
        login_SignUpTV.setVisibility(View.GONE);
    }

   /* private void showInfoMessageForRTP()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("We need this permission");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setCancelable(false);
        builder.show();
    }*/

    private void getRunTimeIMEINumber() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},PERMISSIONS_REQUEST_READ_PHONE_STATE);

                // FIRST WE NEED TO INFORM USER WHY WE NEED THIS PERMISSION HENCE SHOWING DIALOG BOX.
                openRTPInfoDialog();
            } else {
                getDeviceIMEI();
            }
        }
    }

    private void getDeviceIMEI() {
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        SharedPreferences.Editor editor = preferences.edit();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        editor.putString("IMEI_NUMBER_KEY", tm.getDeviceId()).apply();
    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.login_SignUpTV) {
                Intent loginIntent = new Intent(LoginScreen.this, SignUpScreen.class);
                startActivityForResult(loginIntent, SIGNUP_REQUEST_CODE);
            } else if (id == R.id.login_ForgotPW) {
                startActivity(new Intent(LoginScreen.this, ForgetPasswordScreen.class));
            } else if (id == R.id.googlePlusLoginBtn) {
                startGoogleSignIn();
            }
        }
    };

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SIGNUP_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    if (ProjectConstant.ISITCOMINGFROM_MYACCOUNT) {
                        ProjectConstant.ISITCOMINGFROM_MYACCOUNT = false;
                        LoginScreen.this.finish();
                    } else {
                        if (isComingFromElectricBlueScreen || isComingFromPayAsYouGo) {
                            apiHitPosition = 3;
                            new LoadResponseViaPost(LoginScreen.this, formRegNumberJSON(), true).execute("");
                        } else if (isComingFromCatering) {
                            startActivity(new Intent(LoginScreen.this, MyOrders.class));
                            LoginScreen.this.finish();
                        } else if (isComingFromKVScreen) {
                            startActivity(new Intent(LoginScreen.this, KVProductCartScreen.class));
                            LoginScreen.this.finish();
                        } else if (isComingFromGamingScreen) {
                            startActivity(new Intent(LoginScreen.this, GamingProductPricing.class));
                            LoginScreen.this.finish();
                        } else {
                            Intent signUpIntent = new Intent(LoginScreen.this, OrderDetails.class);
                            signUpIntent.putExtra("PAYNOW_DETAILS_HASHMAP", receivedPayNowDetailHashMap);
                            startActivity(signUpIntent);
                            LoginScreen.this.finish();
                        }
                    }
                }
            }
        }

        //MOBILE OTP VERIFICATION CALL BACK METHOD
        else if (requestCode == MOBILE_OTP_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    if (isThirdPartyLoginExecuted) {
                        isThirdPartyLoginExecuted = false;
                        apiHitPosition = 2;
                        new LoadResponseViaPost(LoginScreen.this, formFbGpJSON(), true).execute("");
                    } else {
                        // Executing normal login process
                        apiHitPosition = 1;
                        new LoadResponseViaPost(LoginScreen.this, formJSONParam(), true).execute("");
                    }
                }
            }
            if (resultCode == RESULT_CANCELED) {
//                ProjectConstant.ISITCOMINGFROM_MYACCOUNT=false;
//                LoginScreen.this.finish();
            }
        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        else if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else if (requestCode == REQUEST_APP_SETTINGS) {
            // Need to handle, whether user granted the permission or not...
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(LoginScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                LoginScreen.this.finish();
            } else {
                getDeviceIMEI();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDeviceIMEI();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginScreen.this, Manifest.permission.READ_PHONE_STATE)) {
                //Show permission explanation dialog...
//                openRTPInfoDialog();

                //Need to handle, what to do if user denies permission very first.
                // RE- DIRECTING USER TO HOME SCREEN, IF CHOOSE "NEVER ASK AGAIN" WITH DENY
                Intent intent = new Intent(LoginScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                LoginScreen.this.finish();
            } else {
                //Never ask again selected, or device policy prohibits the app from having that permission.
                //So, disable that feature, or fall back to another situation...

                openNeverAskedSelectedDialog();

                // RE- DIRECTING USER TO HOME SCREEN, IF CHOOSE "NEVER ASK AGAIN" WITH DENY
                /*Intent intent = new Intent(LoginScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                LoginScreen.this.finish();*/
            }
        }
    }

    private void openRTPInfoDialog() {
        new AlertDialog.Builder(LoginScreen.this)
//                .setMessage("Kwikpay needs permission to access the device information for processing any payment. We will never use this information for any other purpose")
                .setMessage(getResources().getString(R.string.CONSENT_SCREEN_MESSAGE))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(LoginScreen.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void openNeverAskedSelectedDialog() {
        new AlertDialog.Builder(LoginScreen.this)
                .setMessage(getResources().getString(R.string.CONSENT_SCREEN_MESSAGE))
                .setPositiveButton("open setting", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        try {
                            Intent appSettingIntent = new Intent();
                            appSettingIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            appSettingIntent.setData(Uri.fromParts("package:", getPackageName(), null));
                            startActivityForResult(appSettingIntent, REQUEST_APP_SETTINGS);
                        } catch (Exception e) {
                            LoginScreen.this.finish();
                        }
//                        ActivityCompat.requestPermissions(LoginScreen.this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
                    }
                })
                /*.setNegativeButton("open setting", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent appSettingIntent = new Intent();
                        appSettingIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        appSettingIntent.setData(Uri.fromParts("package:", getPackageName(), null));
                        startActivityForResult(appSettingIntent, REQUEST_APP_SETTINGS);
                    }
                })*/
                .setCancelable(false)
                .show();
    }

    //IMPLEMENT IN ORDER TO CUSTOMIZE FACEBOOK BUTTON, A LITTLE BIT
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onPostCreate(savedInstanceState);

        float fbIconScale = 1.45F;
        Drawable drawable = LoginScreen.this.getResources().getDrawable(com.facebook.R.drawable.com_facebook_button_icon);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * fbIconScale), (int) (drawable.getIntrinsicHeight() * fbIconScale));
        loginButton.setCompoundDrawables(drawable, null, null, null);
        loginButton.setCompoundDrawablePadding(LoginScreen.this.getResources().
                getDimensionPixelSize(R.dimen.fb_margin_override_textpadding));
        loginButton.setPadding(LoginScreen.this.getResources().getDimensionPixelSize(R.dimen.fb_margin_override_lr), LoginScreen.this.getResources().getDimensionPixelSize(R.dimen.fb_margin_override_top), 0, LoginScreen.this.getResources().getDimensionPixelSize(R.dimen.fb_margin_override_bottom));

    }

    private void setUpIds() {
        // TODO Auto-generated method stub
        preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        emailAddressLabel = findViewById(R.id.loginscreen_EmailTV);
        passwordLabel = findViewById(R.id.loginscreen_PasswordTV);

        emailAddressLabel.setText(Html.fromHtml("Email Address" + "<font color='#ff0000'>*"));
        passwordLabel.setText(Html.fromHtml("Password" + "<font color='#ff0000'>*"));

        loginEmailID = findViewById(R.id.loginId);
        loginPassword = findViewById(R.id.loginPW);

        //@Facebook Initialization
        loginButton = (LoginButton) findViewById(R.id.fb_login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SETTING SCREEN NAMES TO APPS FLYER
                try {
                    EventsLoggerSingleton.getInstance().setCommonEventLogs(LoginScreen.this, "before login facebook_ANDROID");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        findViewById(R.id.login_ForgotPW).setOnClickListener(viewClickListener);
        login_SignUpTV = findViewById(R.id.login_SignUpTV);
        login_SignUpTV.setOnClickListener(viewClickListener);
        googlePlusLoginBtn = findViewById(R.id.googlePlusLoginBtn);
        googlePlusLoginBtn.setOnClickListener(viewClickListener);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("704843277723-qrherkpgtfpo0drid1tuunkouhcivkca.apps.googleusercontent.com")
//                .requestServerAuthCode("704843277723-qrherkpgtfpo0drid1tuunkouhcivkca.apps.googleusercontent.com")
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = ((ImageView) findViewById(R.id.headerHomeBtn));
        ImageView headerMenuImage = (ImageView) findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);

        CustomSlidingDrawer csd = new CustomSlidingDrawer(LoginScreen.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();
        //END OF SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;

        ProjectConstant.ISLOGINPAGE_ACTIVE = true;
        //RESTRICTING ' IN PASSWORD FIELD
        FieldValidationWithMessage.blockMyCharacter(loginPassword, "'");
    }

    public void loginProceedMethod(View v) {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        if (!FieldValidationWithMessage.emailAddressIsValid(loginEmailID.getText().toString(), loginEmailID)) {
            loginPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            ARCustomToast.showToast(LoginScreen.this, "please enter a valid email id", Toast.LENGTH_LONG);
        } else if (loginPassword.getText().toString().isEmpty()) {
            loginPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
            FieldValidationWithMessage.removeAllErrorIcons(loginEmailID);
            ARCustomToast.showToast(LoginScreen.this, "please enter password", Toast.LENGTH_LONG);
        } else {
            apiHitPosition = 1;
            new LoadResponseViaPost(LoginScreen.this, formJSONParam(), true).execute("");

            // IF USER'S MOBILE NUMBER IS VERIFIED FROM CM-SDK
            /*if(preferences.getBoolean("isUserVerifiedFromCM", false))
            {
            }
            else
            {
                isThirdPartyLoginExecuted=false;
                startActivityForResult(new Intent(LoginScreen.this, OTPVerificationScreen.class).putExtra("LOGIN_OTP_CALLER",true), MOBILE_OTP_REQUEST_CODE);
            }*/

        }
    }

    //CREATING JSON PARAMETERS FOR APP LOGIN
    public String formJSONParam() {
        JSONObject jsonobj = new JSONObject();

        String trimmedPassword = loginPassword.getText().toString().replaceAll(" +", " ").trim();
        try {
            jsonobj.put("APISERVICE", "KPUSERLOGIN");
            jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
            jsonobj.put("DEVICEOS", "Android");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("EMAILID", loginEmailID.getText().toString());
            jsonobj.put("PASSWORD", trimmedPassword);

            jsonobj.put("PUSH_TOKEN", preferences.getString("URBAN_AIRSHIP_CHANNEL_ID", ""));

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    //CREATING JSON PARAMETERS FOR THIRD PARTY LOGINS: FACEBOOK AND GOOGLE PLUS
    public String formFbGpJSON() {
        JSONObject jsonobj = new JSONObject();
        try {
            DeviceAppInformation deviceAppInformation = new DeviceAppInformation(LoginScreen.this);

            jsonobj.put("APISERVICE", "KPTHIRDPARTYLOGIN");
            jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
            jsonobj.put("DEVICEOS", "Android");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("EMAIL", fb_gpEmailId);
            jsonobj.put("NAME", fb_gpName);
            jsonobj.put("FIRST_NAME", fb_gpName);
            jsonobj.put("LAST_NAME", "");
            jsonobj.put("THIRDPARTYID", fb_gpId);
            jsonobj.put("GENDER", fb_gpGender);
            jsonobj.put("AGENT_CODE", fb_gpAgentCode);
            jsonobj.put("THIRD_PARTY_ID", fb_gpId);
            jsonobj.put("APPNAME", "KWIKPAY");

            //ADDING NEW EXTRA TAGS, FOR DEVICE INFORMATION
            jsonobj.put("TOKAN", fb_gpAccessToken);
            jsonobj.put("MOBILE", preferences.getString("VerifiedMobNoViaCM", ""));
            jsonobj.put("BRAND", deviceAppInformation.getDeviceBrand());
            jsonobj.put("MODEL", deviceAppInformation.getDeviceModel());
            jsonobj.put("MANUFACTURER", deviceAppInformation.getDeviceManufacturer());
            jsonobj.put("DEVICE", deviceAppInformation.getDevice());
            jsonobj.put("OSVERSION", deviceAppInformation.getDeviceOSVersion());
            jsonobj.put("SCREENSIZE", deviceAppInformation.getDeviceScreenSize());
            jsonobj.put("RESOLUTION", deviceAppInformation.getDeviceResolution());
            jsonobj.put("PRODUCT", deviceAppInformation.getDeviceProduct());
            jsonobj.put("IPADDRESS", deviceAppInformation.getIpAddress());
            jsonobj.put("CELLID", "");
            jsonobj.put("PUSH_TOKEN", preferences.getString("URBAN_AIRSHIP_CHANNEL_ID", ""));

            deviceAppInformation = null;

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    //CREATING JSON PARAMETERS FOR REGISTERED MOBILE NUMBER
    public String formRegNumberJSON() {
        JSONObject jsonobj = new JSONObject();
        try {
            String receivedLocationNumber = "";
            try {
                receivedLocationNumber = KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getLocationNumber();
            } catch (Exception e) {
            }
            jsonobj.put("APISERVICE", "KP_REG_NUM_BY_USERID");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("OP_CODE", "ELEC");
            jsonobj.put("SERVICEID", "4");
            jsonobj.put("LOCATION_NUMBER", receivedLocationNumber);
            jsonobj.put("USERID", preferences.getString("KP_USER_ID", ""));
//            jsonobj.put("USERID", "979");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (!loadedString.equals("") && !loadedString.equals("Exception")) {
            if (apiHitPosition == 1)
                parseAPPLoginJson(loadedString);
            else if (apiHitPosition == 2)
                parseTHIRDpartyLoginJson(loadedString);
            else if (apiHitPosition == 3)
                parseRegisteredNumberJson(loadedString);
        } else {
            ARCustomToast.showToast(LoginScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }

    }

    public boolean parseAPPLoginJson(String jsonString) {
        try {
            JSONObject loginJsonObject = new JSONObject(jsonString);
            JSONObject signup_json_response = loginJsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (signup_json_response.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (signup_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (signup_json_response.has("USERNAME") && signup_json_response.has("USERID") && signup_json_response.has("EMAILID")) {
                        SharedPreferences.Editor loginPrefEdit = preferences.edit();
                        loginPrefEdit.putString("KP_USER_NAME", signup_json_response.getString("USERNAME"));
                        loginPrefEdit.putString("KP_USER_ID", signup_json_response.getString("USERID"));
                        loginPrefEdit.putString("KP_USER_EMAIL_ID", signup_json_response.getString("EMAILID"));

                        try {
                            if (signup_json_response.has("IS_POSTCODE_DEFINED") && !signup_json_response.getString("IS_POSTCODE_DEFINED").isEmpty())
                                loginPrefEdit.putBoolean("IS_KP_USER_POSTCODE_DEFINED", Boolean.parseBoolean(signup_json_response.optString("IS_POSTCODE_DEFINED")));
                            if (signup_json_response.has("IS_MOBILE_VERIFIED") && !signup_json_response.getString("IS_MOBILE_VERIFIED").isEmpty())
                                loginPrefEdit.putBoolean("IS_MOBILE_VERIFIED", Boolean.parseBoolean(signup_json_response.optString("IS_MOBILE_VERIFIED")));

                            if (signup_json_response.has("IS_EMAIL_VERIFIED") && !signup_json_response.getString("IS_EMAIL_VERIFIED").isEmpty()) {
                                loginPrefEdit.putBoolean("IS_KP_USER_EMAIL_VERIFIED", Boolean.parseBoolean(signup_json_response.optString("IS_EMAIL_VERIFIED")));

                                if (!Boolean.parseBoolean(signup_json_response.optString("IS_EMAIL_VERIFIED"))) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        Intent sendEmailFromBG = new Intent();
                                        sendEmailFromBG.putExtra("isComingFromKPLoginScreen", true);
                                        JobIntentService.enqueueWork(LoginScreen.this, SendEmailForVerificationService.class, 1002, sendEmailFromBG);
                                    } else {
                                        Intent sendEmailFromBG = new Intent(LoginScreen.this, SendEmailForVerificationService.class);
                                        sendEmailFromBG.putExtra("isComingFromKPLoginScreen", true);
                                        startService(sendEmailFromBG);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                        }

                        loginPrefEdit.apply();

                        //SETTING SCREEN NAMES TO APPS FLYER
                        try {
                            EventsLoggerSingleton.getInstance().setCommonEventLogs(LoginScreen.this, getResources().getString(R.string.SCREEN_NAME_LOGIN_KP));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        ProjectConstant.ISLOGINPAGE_ACTIVE = false;

                        if (ProjectConstant.ISITCOMINGFROM_MYACCOUNT) {
                            ProjectConstant.ISITCOMINGFROM_MYACCOUNT = false;
                            LoginScreen.this.finish();
                        } else {
                            if (isComingFromElectricBlueScreen || isComingFromPayAsYouGo) {
                                apiHitPosition = 3;
                                new LoadResponseViaPost(LoginScreen.this, formRegNumberJSON(), true).execute("");
                            } else if (isComingFromCatering) {
                                startActivity(new Intent(LoginScreen.this, MyOrders.class));
                                LoginScreen.this.finish();
                            } else if (isComingFromKVScreen) {
                                startActivity(new Intent(LoginScreen.this, KVProductCartScreen.class));
                                LoginScreen.this.finish();
                            } else if (isComingFromGamingScreen) {
                                startActivity(new Intent(LoginScreen.this, GamingProductPricing.class));
                                LoginScreen.this.finish();
                            } else {
                                Intent logInIntent = new Intent(LoginScreen.this, OrderDetails.class);
                                logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", receivedPayNowDetailHashMap);
                                startActivity(logInIntent);
                                LoginScreen.this.finish();
                            }
                        }
                    }
                } else if (signup_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("2")) {
                    if (signup_json_response.has("USERNAME") && signup_json_response.has("USERID") && signup_json_response.has("EMAILID")) {
                        Intent changeTempPWIntent = new Intent(LoginScreen.this, ChangeTempPassword.class);
                        changeTempPWIntent.putExtra("OLD_TEMP_PW", loginPassword.getText().toString());
                        changeTempPWIntent.putExtra("TEMP_USERID", signup_json_response.getString("USERID"));
                        if (isHashMapFulled)
                            changeTempPWIntent.putExtra("PAYNOW_DETAILS_HASHMAP", receivedPayNowDetailHashMap);
                        startActivity(changeTempPWIntent);

                        LoginScreen.this.finish();
                    }
                } else {
                    if (signup_json_response.has("DESCRIPTION")) {
                        ARCustomToast.showToast(LoginScreen.this, signup_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean parseTHIRDpartyLoginJson(String jsonString) {
        try {
            JSONObject loginJsonObject = new JSONObject(jsonString);
            JSONObject fbgpLoginObject = loginJsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (fbgpLoginObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (fbgpLoginObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (fbgpLoginObject.has("USERNAME") && fbgpLoginObject.has("USERID") && fbgpLoginObject.has("EMAIL")) {
                        SharedPreferences.Editor loginPrefEdit = preferences.edit();
                        loginPrefEdit.putString("KP_USER_NAME", fbgpLoginObject.getString("USERNAME"));
                        loginPrefEdit.putString("KP_USER_ID", fbgpLoginObject.getString("USERID"));
                        loginPrefEdit.putString("KP_USER_EMAIL_ID", fbgpLoginObject.getString("EMAIL"));
                        loginPrefEdit.putBoolean("EMAIL_IS_VERIFIED", true);
                        loginPrefEdit.apply();

                        ProjectConstant.ISLOGINPAGE_ACTIVE = false;

                        //SETTING SCREEN NAMES TO APPS FLYER
                        try {
                            String eventName = "";
                            if (fb_gpAgentCode.equals("GOOGLE"))
                                eventName = getResources().getString(R.string.SCREEN_NAME_LOGIN_GP);
                            else if (fb_gpAgentCode.equals("FACEBOOK"))
                                eventName = getResources().getString(R.string.SCREEN_NAME_LOGIN_FB);

                            EventsLoggerSingleton.getInstance().setCommonEventLogs(LoginScreen.this, eventName);

                            /*AppsFlyerLib.trackEvent(this, eventName, null);
                            EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
                            ((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (ProjectConstant.ISITCOMINGFROM_MYACCOUNT) {
                            ProjectConstant.ISITCOMINGFROM_MYACCOUNT = false;
                            LoginScreen.this.finish();
                        } else {
                            if (isComingFromElectricBlueScreen || isComingFromPayAsYouGo) {
                                apiHitPosition = 3;
                                new LoadResponseViaPost(LoginScreen.this, formRegNumberJSON(), true).execute("");
                            } else if (isComingFromCatering) {
                                startActivity(new Intent(LoginScreen.this, MyOrders.class));
                                LoginScreen.this.finish();
                            } else if (isComingFromKVScreen) {
                                startActivity(new Intent(LoginScreen.this, KVProductCartScreen.class));
                                LoginScreen.this.finish();
                            } else if (isComingFromGamingScreen) {
                                startActivity(new Intent(LoginScreen.this, GamingProductPricing.class));
                                LoginScreen.this.finish();
                            } else {
                                Intent logInIntent = new Intent(LoginScreen.this, OrderDetails.class);
                                logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", receivedPayNowDetailHashMap);
                                startActivity(logInIntent);
                                LoginScreen.this.finish();
                            }
                        }
                    }
                } else {
                    if (fbgpLoginObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(LoginScreen.this, fbgpLoginObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            return false;
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean parseRegisteredNumberJson(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    LocationIdMO receivedLocationDetailsMO = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();

                    receivedLocationDetailsMO.setOperatorCode(childJsonObj.optString("OP_CODE"));
                    receivedLocationDetailsMO.setServiceId(childJsonObj.optString("SERVICEID"));
                    receivedLocationDetailsMO.setAC(childJsonObj.optString("AC"));
                    receivedLocationDetailsMO.setDC(childJsonObj.optString("DC"));
                    receivedLocationDetailsMO.setCHADEMO(childJsonObj.optString("CHADEMO"));
                    receivedLocationDetailsMO.setConnector1Desc(childJsonObj.optString("CONNECTOR_1_DESC"));
                    receivedLocationDetailsMO.setConnector2Desc(childJsonObj.optString("CONNECTOR_2_DESC"));
                    receivedLocationDetailsMO.setConnector3Desc(childJsonObj.optString("CONNECTOR_3_DESC"));
                    receivedLocationDetailsMO.setAddressOne(childJsonObj.optString("ADDRESS_ONE"));
                    receivedLocationDetailsMO.setAddressTwo(childJsonObj.optString("ADDRESS_TWO"));
                    receivedLocationDetailsMO.setAddressThree(childJsonObj.optString("ADDRESS_THREE"));
                    receivedLocationDetailsMO.setCountry(childJsonObj.optString("COUNTRY"));
                    receivedLocationDetailsMO.setCity(childJsonObj.optString("CITY"));
                    receivedLocationDetailsMO.setPostCode(childJsonObj.optString("POSTCODE"));
                    receivedLocationDetailsMO.setUserPostCode(childJsonObj.optString("USER_POSTCODE"));
                    receivedLocationDetailsMO.setRegistrationNumber(childJsonObj.optString("REGISTRATION_NUMBER"));
                    receivedLocationDetailsMO.setRecentMake(childJsonObj.optString("RECENT_MAKE"));
                    receivedLocationDetailsMO.setRecentModel(childJsonObj.optString("RECENT_MODEL"));
                    receivedLocationDetailsMO.setRecentYear(childJsonObj.optString("RECENT_YEAR"));
                    receivedLocationDetailsMO.setRecentBatteryType(childJsonObj.optString("RECENT_BATTERY_TYPE"));
                    receivedLocationDetailsMO.setRecentChargerType(childJsonObj.optString("RECENT_CHARGER_TYPE"));
                    receivedLocationDetailsMO.setRecentMaxAmount(childJsonObj.optString("RECENT_MAX_AMOUNT"));
                    receivedLocationDetailsMO.setRecentMaxTime(childJsonObj.optString("RECENT_MAX_TIME"));
                    receivedLocationDetailsMO.setRecentRate(childJsonObj.optString("RECENT_RATE"));

                    KwikChargeDetailsSingleton.getInstance().setLocationIdMO(receivedLocationDetailsMO);

                    Object object = childJsonObj.get("REG_NUMS");

                    if (object instanceof JSONObject) {
                        final JSONObject jsonOBJECT = (JSONObject) object;
                        regNoJsonArray = new JSONArray();
                        regNoJsonArray.put(jsonOBJECT);
                    } else if (object instanceof JSONArray) {
                        regNoJsonArray = (JSONArray) object;
                    }

                    if (regNoJsonArray != null && regNoJsonArray.length() > 0) {

                        ArrayList<RegistrationNumbersMO> regNoArrayList = new ArrayList<>(regNoJsonArray.length());

                        for (byte b = 0; b < regNoJsonArray.length(); b++) {

                            RegistrationNumbersMO regNoModel = new RegistrationNumbersMO();
                            regNoModel.setRegistrationNo(regNoJsonArray.getJSONObject(b).optString("REG_NUM"));
                            regNoModel.setMake(regNoJsonArray.getJSONObject(b).optString("MAKE"));
                            regNoModel.setModel(regNoJsonArray.getJSONObject(b).optString("MODEL"));
                            regNoModel.setChargerType(regNoJsonArray.getJSONObject(b).optString("CHARGER_TYPE"));
                            regNoModel.setBatteryType(regNoJsonArray.getJSONObject(b).optString("BATTERY_TYPE"));
                            regNoModel.setYear(regNoJsonArray.getJSONObject(b).optString("YEAR"));
                            regNoModel.setMaxAmount(regNoJsonArray.getJSONObject(b).optString("MAX_AMOUNT"));
                            regNoModel.setMaxTime(regNoJsonArray.getJSONObject(b).optString("MAX_TIME"));
                            regNoModel.setMaxRate(regNoJsonArray.getJSONObject(b).optString("RATE"));

                            regNoArrayList.add(regNoModel);
                        }
                        RegNoDetailsSingleton.getInstance().setRegNoList(regNoArrayList);

                        setResult(Activity.RESULT_OK, new Intent());
                        finish();
                    }
                } else {
                    Intent intent = new Intent(LoginScreen.this, HomeScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    LoginScreen.this.finish();
                }
            }
        } catch (JSONException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        ProjectConstant.ISLOGINPAGE_ACTIVE = ProjectConstant.ISITCOMINGFROM_MYACCOUNT = false;
        super.onBackPressed();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void startGoogleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /*private void startGoogleSignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
    }*/

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            fb_gpAccessToken = acct.getIdToken();
            if (fb_gpAccessToken == null)
                fb_gpAccessToken = "";

            fb_gpName = acct.getDisplayName();
            fb_gpId = acct.getId();
            fb_gpEmailId = acct.getEmail();
            fb_gpGender = "";
            fb_gpAgentCode = "GOOGLE";

            isThirdPartyLoginExecuted = true;
            startActivityForResult(new Intent(LoginScreen.this, OTPVerificationScreen.class).putExtra("LOGIN_OTP_CALLER", true), MOBILE_OTP_REQUEST_CODE);
//            apiHitPosition=2;
//            new LoadResponseViaPost(LoginScreen.this, formFbGpJSON() , true).execute("");

        } else {
            ARCustomToast.showToast(LoginScreen.this, "Your google account authentication failed!", Toast.LENGTH_LONG);
            // Signed out, show unauthenticated UI.
            // updateUI(false);
        }
    }

}
