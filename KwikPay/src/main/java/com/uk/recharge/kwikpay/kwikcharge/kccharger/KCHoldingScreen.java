package com.uk.recharge.kwikpay.kwikcharge.kccharger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.KCReceiptScreen;
import com.uk.recharge.kwikpay.kwikcharge.models.KCReceiptMO;

import org.json.JSONException;
import org.json.JSONObject;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by IOPS on 2/17/2018.
 */

public class KCHoldingScreen extends AppCompatActivity implements AsyncRequestListenerViaPost {

    private SharedPreferences preference;
    KCReceiptMO kcReceiptMO = null;
    private GifImageView kcHoldingCircularProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_holding_screen);

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        kcHoldingCircularProgressBar = findViewById(R.id.kcHoldingCircularProgressBar);

        new LoadResponseViaPost(this, formKCStopChargingJson(), false).execute("");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                removeInformationFromPrefs();
                if (kcReceiptMO != null)
                    startActivity(new Intent(KCHoldingScreen.this, KCReceiptScreen.class).putExtra("KC_RECEIPT_DETAILS_MO", kcReceiptMO));
            }
        }, 30000);
    }

    public String formKCStopChargingJson() {
        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("APISERVICE", "KC_STOP_CHARGE");
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
            jsonobj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("LOCATION_NUMBER", preference.getString("TEMP_LOCATION_NUMBER", ""));
            jsonobj.put("DEVICE_DB_ID", preference.getString("TEMP_DEVICE_DB_ID", ""));
            jsonobj.put("LOCAL_CONNECTOR_NUMBER", preference.getString("TEMP_LOCAL_CONNECTOR_NUMBER", ""));
            jsonobj.put("APP_TXN_ID", preference.getString("TEMP_APP_TXN_ID", ""));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (!loadedString.equals("") && !loadedString.equals("Exception")) {
            parseStopChargingDetails(loadedString);
        } else {
            hideProgressBarAndExitClass();
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    private void parseStopChargingDetails(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    kcReceiptMO = new KCReceiptMO();
                    kcReceiptMO.setRegistrationNumber(childJsonObj.optString("REGISTRATION_NUMBER"));
                    kcReceiptMO.setLocationName(childJsonObj.optString("LOCATIONNAME"));
                    kcReceiptMO.setDuration(childJsonObj.optString("DURATION"));
                    kcReceiptMO.setTotalEnergy(childJsonObj.optString("TOTAL_ENERGY"));
                    kcReceiptMO.setServiceFee(childJsonObj.optString("SERVICEFEE"));
                    kcReceiptMO.setMaxTime(childJsonObj.optString("MAX_TIME"));
                    kcReceiptMO.setRate(childJsonObj.optString("RATE"));

                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        hideProgressBarAndExitClass();
                        ARCustomToast.showToast(this, childJsonObj.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (Exception e) {
            hideProgressBarAndExitClass();
            ARCustomToast.showToast(this, "Parsing exception", Toast.LENGTH_LONG);
        }
    }

    private void removeInformationFromPrefs() {
        try {
            SharedPreferences.Editor editor = preference.edit();
            editor.remove("TEMP_DEVICE_DB_ID");
            editor.remove("TEMP_LOCAL_CONNECTOR_NUMBER");
            editor.remove("TEMP_APP_TXN_ID");
            editor.apply();
        } catch (Exception e) {
        }
    }

    private void hideProgressBarAndExitClass() {
        kcHoldingCircularProgressBar.setVisibility(View.INVISIBLE);
        finish();
    }

}
