package com.uk.recharge.kwikpay.gamingarcade.payments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.gamingarcade.models.GameMachineInfoMO;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.GamingInfoSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 7/10/2018.
 */

public class GamingReceipt extends AppCompatActivity {

    private TextView totalTicketsTV;
    private TextView mickeyMouseView;
    private TextView ratingStars;
    private TextView availableBalanceTV;
    private Button gamePlayAgainButton;
    private String availableCredit = "", availableTicket = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gaming_success_failure);
        settingResourceIDs();
    }

    private void settingResourceIDs() {

        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        totalTicketsTV = findViewById(R.id.availableCreditTV);
        mickeyMouseView = findViewById(R.id.creditAddedSuccessMsg);
        ratingStars = findViewById(R.id.pressStartMachineMsgTV);
        availableBalanceTV = findViewById(R.id.creditRequireMsgTV);

        gamePlayAgainButton = findViewById(R.id.gamingStartGameButton);
        gamePlayAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToHomeScreen();
            }
        });

        findViewById(R.id.headerBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToHomeScreen();
            }
        });

        callStartEndGameAPI("0", true);
    }

    private void callStartEndGameAPI(String ticketEarned, final boolean isCallingFirstTime) {
        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            SharedPreferences preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
            GameMachineInfoMO gameMachineInfoMO = GamingInfoSingleton.getInstance().getGameMachineInfoMO();
//            int productPosition = gameMachineInfoMO.getSelectedPosition();

            json.put("APISERVICE", "GAME_MANAGE_BALANCE_AND_TICKETS");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));

            json.put("MACHINE_NUMBER", gameMachineInfoMO.getMachineNumber());
            json.put("OP_CODE", gameMachineInfoMO.getOperatorCode());
            json.put("OP_ID", gameMachineInfoMO.getOperatorID());
            json.put("APP_TXN_ID", preference.getString("GAMING_TXN_ID", ""));
            json.put("GAME_COST", gameMachineInfoMO.getCost());
            json.put("TICKET_EARNED", ticketEarned); // Kept always blank 'While starting the game'
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {

                                    if (childJson.has("AMUSEMENT_AVAILABLE_CREDIT"))
                                        availableCredit = childJson.getString("AMUSEMENT_AVAILABLE_CREDIT");
                                    if (childJson.has("AMUSEMENT_AVAILABLE_TICKET"))
                                        availableTicket = childJson.getString("AMUSEMENT_AVAILABLE_TICKET");

                                    if (isCallingFirstTime) {

                                        updateReceiptScreenUI(availableTicket, availableCredit);

                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                callStartEndGameAPI(availableTicket, false);
                                            }
                                        }, 3000);

                                    } else {
                                        updateReceiptScreenUI(availableTicket, availableCredit);
                                    }

                                } else {
                                    ARCustomToast.showToast(GamingReceipt.this, childJson.optString("DESCRIPTION"));
                                }
                            } else {
                                ARCustomToast.showToast(GamingReceipt.this, childJson.optString("DESCRIPTION"));
                            }
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(GamingReceipt.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(GamingReceipt.this))
                        ARCustomToast.showToast(GamingReceipt.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(GamingReceipt.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                ARCustomToast.showToast(GamingReceipt.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void updateReceiptScreenUI(String ticketEarned, String availableBalance) {
        Log.e("ReceiptScreen", "Coming to update the UI after second time calling API");

        totalTicketsTV.setText("Total Tickets : " + 0);
        mickeyMouseView.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.mickey_car, 0, 0);
        mickeyMouseView.setText("");

        ratingStars.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.gaming_stars, 0, 0);
        ratingStars.setText("Ticket Earned " + ticketEarned);
        ratingStars.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);

        availableBalanceTV.setText("Available Balance : " + availableBalance);

        gamePlayAgainButton.setText("Play Again");
    }

    private void redirectToHomeScreen() {
        Intent intent = new Intent(GamingReceipt.this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}