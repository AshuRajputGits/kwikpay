package com.uk.recharge.kwikpay.kwikvend.payments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.PaymentGatewayPage;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikvend.KVProductSelection;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductDetailsMO;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductOthersDetailsMO;
import com.uk.recharge.kwikpay.nativepg.PayPalPaymentScreen;
import com.uk.recharge.kwikpay.nativepg.UpdateJUDOPaymentGateway;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 7/13/2018.
 */

public class KVPaymentOptions extends KPSuperClass {

    private boolean isVisaChoose = false;
    private String pgAppTxnId, pgType;
    private TextView maximumAuthorizedAmountTV;
    private String apiPGName = "";
    private HashMap<String, String> nativePGDataHashMap;
    //    private String totalCartAmount = "";
    private KVProductOthersDetailsMO receivedModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        setHeaderScreenName("Pay with");
        setResourceIds();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.kc_payment_options;
    }

    private void setResourceIds() {

        findViewById(R.id.processingFeesMessage).setVisibility(View.VISIBLE);
        maximumAuthorizedAmountTV = findViewById(R.id.maximumAuthorizedAmountTV);
        maximumAuthorizedAmountTV.setVisibility(View.VISIBLE);
        findViewById(R.id.pgIntermediateScreenId).setVisibility(View.VISIBLE);
        findViewById(R.id.kcPaymentNote).setVisibility(View.VISIBLE);
        findViewById(R.id.paypal_PG_Button).setOnClickListener(onClickListener);
        findViewById(R.id.judo_PG_Button).setOnClickListener(onClickListener);

        receivedModel = KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO();

        maximumAuthorizedAmountTV.setText("Maximum amount to be authorised " + Utility.fromHtml("\u00a3") + receivedModel.getTotalCartAmount());

        /*Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("totalProductAmount")) {

            }
            if (receivedBundle.containsKey("totalCartAmount")) {
                totalCartAmount = receivedBundle.getString("totalCartAmount", "");
                maximumAuthorizedAmountTV.setText("Maximum amount to be authorised " + Utility.fromHtml("\u00a3") + totalCartAmount);
            }
        }*/
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewID = v.getId();
            if (viewID == R.id.paypal_PG_Button)
                isVisaChoose = false;
            else if (viewID == R.id.judo_PG_Button)
                isVisaChoose = true;
            callGetPaymentTransactionIdAPI();
        }
    };

    private void callGetPaymentTransactionIdAPI() {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "KPPAYMENTGATEWAYAPPTXNID");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", sharedPreferences.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);

            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("SOURCE", "KPAPP");
            json.put("OPABBR", "KWIKVEND");
            if (isVisaChoose) {
                json.put("MOPID", sharedPreferences.getString("PG_MOP_ID", "18"));
                json.put("PGSOID", sharedPreferences.getString("PG_SO_ID", "160"));
            } else {
                json.put("MOPID", sharedPreferences.getString("PG_MOP_ID1", "22"));
                json.put("PGSOID", sharedPreferences.getString("PG_SO_ID1", "164"));
            }

        } catch (Exception e) {
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has(ProjectConstant.RESPONSE_MSG_TAG)) {
                                if (childJson.getString(ProjectConstant.RESPONSE_MSG_TAG).equals("SUCCESS")) {
                                    if (childJson.has(ProjectConstant.PG_APP_TXN_ID) && childJson.has("PGTYPE")) {
                                        pgAppTxnId = childJson.getString(ProjectConstant.PG_APP_TXN_ID);
                                        pgType = childJson.getString("PGTYPE");

                                        if (!pgAppTxnId.equals("") && !pgType.equals("")) {
                                            if (pgType.equals("NATIVE")) {
                                                callNativePGAPI();
                                            } else {

                                                Intent pgIntent = (new Intent(KVPaymentOptions.this, PaymentGatewayPage.class));
                                                pgIntent.putExtra("PG_JSON_PARAM", formPaymentGatewayJSON("WSGetCotrolAndData", true));
                                                pgIntent.putExtra("PG_APP_TXN_ID_KEY", pgAppTxnId);
                                                startActivity(pgIntent);
                                                finish();

//                                                callingKwikVendReceiptScreen(true);
                                            }
                                        }
                                    }
                                } else {
                                    ARCustomToast.showToast(KVPaymentOptions.this, childJson.optString("DESCRIPTION"));
                                }
                            }
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(KVPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(KVPaymentOptions.this))
                        ARCustomToast.showToast(KVPaymentOptions.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(KVPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                ARCustomToast.showToast(KVPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void callNativePGAPI() {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(formPaymentGatewayJSON("KPGETPGDETAILS", false)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has(ProjectConstant.API_RESPONSE_CODE)) {
                                if (childJson.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                                    nativePGDataHashMap = new HashMap<>();
                                    nativePGDataHashMap.put("PAYMENTREFERENCE", childJson.optString("PAYMENTREFERENCE"));
                                    nativePGDataHashMap.put("JUDOID", childJson.optString("JUDOID"));
                                    nativePGDataHashMap.put("CONSUMERREFERENCE", childJson.optString("CONSUMERREFERENCE"));
                                    nativePGDataHashMap.put("CHECKSUMKEY", childJson.optString("CHECKSUMKEY"));
                                    nativePGDataHashMap.put("ORDERID", childJson.optString("ORDERID"));
                                    nativePGDataHashMap.put("TOKENPAYREFERENCE", childJson.optString("TOKENPAYREFERENCE"));
                                    nativePGDataHashMap.put("SECRETKEY", childJson.optString("SECRETKEY"));
                                    nativePGDataHashMap.put("TOKEN", childJson.optString("TOKEN"));
                                    apiPGName = childJson.optString("PGNAME");

//                                    NOTE: made changes here to get Max AMOUNT for the gaming
                                   /* if (receivedModel != null)
                                        nativePGDataHashMap.put("TOTALAMOUNTPAYABLE", receivedModel.getRecentMaxAmount());*/

                                    nativePGDataHashMap.put("PGNAME", apiPGName);
                                    nativePGDataHashMap.put("APPTXNID", pgAppTxnId);

                                    if (apiPGName.equals("JUDOPG")) {
                                        Intent pgIntent = (new Intent(KVPaymentOptions.this, UpdateJUDOPaymentGateway.class));
                                        pgIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                                        startActivity(pgIntent);
                                    } else if (apiPGName.equalsIgnoreCase("PAYPAL")) {
                                        Intent paypalIntent = new Intent(KVPaymentOptions.this, PayPalPaymentScreen.class);
                                        paypalIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                                        startActivity(paypalIntent);
                                    } else if (apiPGName.equals("COUPONPG")) {
                                        Intent couponPgIntent = (new Intent(KVPaymentOptions.this, UpdateJUDOPaymentGateway.class));
                                        couponPgIntent.putExtra("NATIVE_PG_COUPON", nativePGDataHashMap);
                                        couponPgIntent.putExtra("NATIVE_PG_COUPON2", "COUPON_DATA");
                                        startActivity(couponPgIntent);
                                    } else {
                                        Toast.makeText(KVPaymentOptions.this, "Payment Gateway not defined", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    if (childJson.has("DESCRIPTION")) {
                                        ARCustomToast.showToast(KVPaymentOptions.this, childJson.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(KVPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(KVPaymentOptions.this))
                        ARCustomToast.showToast(KVPaymentOptions.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(KVPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                ARCustomToast.showToast(KVPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });

    }

    // FORMING JSON FOR GETTING PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEBVIEW
    public String formPaymentGatewayJSON(String apiServiceName, boolean isWebView) {
        try {
            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            json.put("iscart", "YES");
            json.put("os", "Android");

            if (isVisaChoose) {
                json.put("mopid", sharedPreferences.getString("PG_MOP_ID", "18"));
                json.put("pgsoid", sharedPreferences.getString("PG_SO_ID", "160"));
            } else {
                json.put("mopid", sharedPreferences.getString("PG_MOP_ID1", "22"));
                json.put("pgsoid", sharedPreferences.getString("PG_SO_ID1", "164"));
            }
            json.put("apptransactionid", pgAppTxnId);
            json.put("ipaddress", sharedPreferences.getString("USER_PUBLIC_IP", "0.0.0.0"));
            json.put("browser", "webView");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("sessionid", sharedPreferences.getString("SESSION_ID", ""));
            else
                json.put("sessionid", ProjectConstant.SESSIONID);
            json.put("source", "KPAPP");
            json.put("username", sharedPreferences.getString("KP_USER_NAME", ""));
            json.put("userid", sharedPreferences.getString("KP_USER_ID", "0"));
            json.put("emailid", sharedPreferences.getString("KP_USER_EMAIL_ID", ""));
            json.put("servicefee", "0.35");

            if (!isWebView)
                json.put("pgType", pgType);

            //Getting selected product list by user to create json array for payment process
            ArrayList<KVProductDetailsMO> kvProductDetailsMOList = KVProductDetailsSingleton.getInstance().getKvProductDetailsMOList();
            if (kvProductDetailsMOList != null & kvProductDetailsMOList.size() > 0) {
                for (int i = 0; i < kvProductDetailsMOList.size(); i++) {
                    JSONObject childJsonObject = new JSONObject();

                    childJsonObject.put("appname", "KWIKPAY");
                    childJsonObject.put("apiservice", apiServiceName);
                    childJsonObject.put("username", sharedPreferences.getString("KP_USER_NAME", ""));
                    childJsonObject.put("userid", sharedPreferences.getString("KP_USER_ID", "0"));
                    childJsonObject.put("emailid", sharedPreferences.getString("KP_USER_EMAIL_ID", ""));
                    childJsonObject.put("os", "Android");
                    childJsonObject.put("ipaddress", sharedPreferences.getString("USER_PUBLIC_IP", "0.0.0.0"));
                    childJsonObject.put("browser", "webView");
                    childJsonObject.put("cpntxnid", "0");
                    childJsonObject.put("cpnamount", "0");
                    childJsonObject.put("cpnsrno", "0");
                    childJsonObject.put("source", "KPAPP");
                    childJsonObject.put("discountamount", "0.0");
                    childJsonObject.put("pgdiscountamount", "0.0");
                    childJsonObject.put("giftreceiver", "||");

                    if (isVisaChoose) {
                        childJsonObject.put("mopid", sharedPreferences.getString("PG_MOP_ID", "18"));
                        childJsonObject.put("pgsoid", sharedPreferences.getString("PG_SO_ID", "160"));
                    } else {
                        childJsonObject.put("mopid", sharedPreferences.getString("PG_MOP_ID1", "22"));
                        childJsonObject.put("pgsoid", sharedPreferences.getString("PG_SO_ID1", "164"));
                    }

                    if (ProjectConstant.SESSIONID.equals(""))
                        childJsonObject.put("sessionid", sharedPreferences.getString("SESSION_ID", ""));
                    else
                        childJsonObject.put("sessionid", ProjectConstant.SESSIONID);

                    childJsonObject.put("mobilenumber", "NA");
                    childJsonObject.put("rechargeamount", "0.0");
                    childJsonObject.put("rechargecurrencyagid", "42");
                    childJsonObject.put("displaycurrencyagid", "42");
                    childJsonObject.put("paymentcurrencyagid", "42");
                    childJsonObject.put("countrycode", "UNITEDKINGDOM");
                    childJsonObject.put("processingfee", "0.02");
                    childJsonObject.put("servicefee", "0.00");

                    //Note: Add extra payment parameters for KwikVend API
                    try {
                        childJsonObject.put("displayamount", String.format("%.2f", kvProductDetailsMOList.get(i).getProductPrice()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    childJsonObject.put("serviceid", "5");
                    childJsonObject.put("operatorcode", "KWIKVEND");
                    childJsonObject.put("productpurchasecount", "" + kvProductDetailsMOList.size());
                    childJsonObject.put("netpgamount", receivedModel.getTotalProductAmount());
                    childJsonObject.put("productcode", kvProductDetailsMOList.get(i).getProductId());
                    childJsonObject.put("machinecode", receivedModel.getMachineType());
//                    childJsonObject.put("operatorname", "KWIKVEND");
                    childJsonObject.put("productname", kvProductDetailsMOList.get(i).getProductName());

                    jsonArray.put(childJsonObject);
                }
            }
            json.put("kprequest", jsonArray);
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*private void callingKwikVendReceiptScreen(final boolean paymentStatus) {
        Intent receiptIntent = new Intent(this, KVProductSelection.class);
        receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        receiptIntent.putExtra("PG_APP_TXN_ID_KEY", pgAppTxnId);
        receiptIntent.putExtra("IS_PAYMENT_SUCCESS", paymentStatus);
        receiptIntent.putExtra("WelcomeBackPageAfterPayment", true);
        startActivity(receiptIntent);
        KVPaymentOptions.this.finish();
    }*/

}
