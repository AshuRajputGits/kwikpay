package com.uk.recharge.kwikpay.nativepg;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.ReceiptScreen;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.HashMap;

public class PayPalPaymentScreen extends Activity implements AsyncRequestListenerViaPost {
    HashMap<String, String> receivedNativePGData;
//        private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;

    //KP neeraj client ID
//    private static final String CONFIG_CLIENT_ID = "ARRUUcIle0MuopU9sSRFkrYBjjJRJEmVgqK-bW813aP3wynJSs43yQuCHYbWg2X-AtEAPZ_EOCVS2vq4";  //SANDBOX-
    private static final String CONFIG_CLIENT_ID = "ASr7GYiZm58PKDoDcYf8O2Vvyc61Vr-II2BMaXWPxFAb4SKccc63tpxLuIjzZUNGuLvznnycKclvXCm_"; //LIVE

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PAYPAL_OTP = 3;
    private String receivedAppTxnId = "", receivedAppOrderId = "", receivedAppAmt = "", authCode = "", metaDataID = "", statusTag = "",
            paypalPaymentTime = "", paypalPaymentState = "", paypalPaymentAuthId = "", paypalReceivedEmailId = "";
    long delayTimeInterval = 0;
    static SharedPreferences preference;
    private int nextProcess = 0;
    private String paypalErrorMessage = "";
    Dialog dialog;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .acceptCreditCards(false);

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("NATIVE_PG_HASHMAP_KEY")) {
                receivedNativePGData = (HashMap<String, String>) receivedBundle.getSerializable("NATIVE_PG_HASHMAP_KEY");
                receivedAppTxnId = receivedNativePGData.get("APPTXNID").toString();
                receivedAppOrderId = receivedNativePGData.get("ORDERID").toString();
                receivedAppAmt = receivedNativePGData.get("TOTALAMOUNTPAYABLE").toString();
            }
        }

        singlePayment();

    }

    public void futurePayment() {
        Intent intent = new Intent(PayPalPaymentScreen.this, PayPalFuturePaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
    }

    public void singlePayment() {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(receivedAppAmt), "GBP", "Kwikpay Topup", PayPalPayment.PAYMENT_INTENT_AUTHORIZE);
        Intent ppIntent = new Intent(PayPalPaymentScreen.this, PaymentActivity.class);
        ppIntent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        ppIntent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(ppIntent, REQUEST_CODE_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
//						Log.i("Full Result ","SDK reponse "+ confirm.toJSONObject().toString(4));
                        //Log.i("", confirm.getPayment().toJSONObject().toString(4));
                        //Log.i("", "KP_PP POP "+confirm.getProofOfPayment().toJSONObject().getString("id"));

                        //paypalPaymentID=confirm.getProofOfPayment().toJSONObject().getString("id");
                        paypalPaymentTime = confirm.getProofOfPayment().toJSONObject().getString("create_time");
                        //paypalPaymentState=confirm.getProofOfPayment().toJSONObject().getString("state");
                        if (confirm.getProofOfPayment().toJSONObject().getString("state").equalsIgnoreCase("approved"))
                            paypalPaymentState = "Success";
                        paypalPaymentAuthId = confirm.getProofOfPayment().toJSONObject().getString("authorization_id");

                        new LoadResponseViaPost(PayPalPaymentScreen.this, getTimerValueJson(true), true).execute("");

                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                callingReceiptScreen();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                callingReceiptScreen();
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        authCode = auth.getAuthorizationCode();
                        metaDataID = PayPalConfiguration.getClientMetadataId(this);
                        // sendAuthorizationToServer(auth);
                        statusTag = "success";
                        new LoadResponseViaPost(PayPalPaymentScreen.this, getTimerValueJson(false), true).execute("");

                    } catch (Exception e) {
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                statusTag = "cancelled";
                new LoadResponseViaPost(PayPalPaymentScreen.this, getTimerValueJson(false), true).execute("");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
            }
        } else if (requestCode == REQUEST_CODE_PAYPAL_OTP) {
            setContentView(R.layout.payment_gif_screen);
            if (resultCode == Activity.RESULT_OK) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        callingReceiptScreen();
                    }
                }, delayTimeInterval);
            }
        }

    }

    //FORM TIMER VALUE JSON PARAMETERS FOR BOTH SINGLE PAYMENT AND THE FUTURE PAYMENT
    public String getTimerValueJson(boolean isSinglePayment) {
        try {
            String formEncryptData = "KPAPP" + "|" + receivedAppAmt + "|" + "" + "|" + receivedAppOrderId +
                    "|" + receivedAppTxnId + "|" + "Success" + "|" + "ozj5Fvn3skB51EnNK3lTqoqo+p4JxtHrzjIoyncH57o=";

            String checkSumString = encryptInSHA256(formEncryptData.trim());

            JSONObject json = new JSONObject();

            json.put("APISERVICE", "KPSAVEPGRESPONSE");
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KP_AND");
            json.put("APPTRANSACTIONID", receivedAppTxnId);
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("PGNAME", "PAYPAL");

            if (isSinglePayment)
                json.put("REFERENCE", paypalPaymentAuthId);
            else
                json.put("REFERENCE", authCode);

            json.put("AMOUNT", receivedAppAmt);
            json.put("APPEARSONSTATEMENTAS", "");
            json.put("CARDDETAILS", "");
            json.put("CARDLASTFOUR", "");
            json.put("CARDTOKEN", "");
            json.put("CARDTYPE", "");
            json.put("CONSUMER", "");
            json.put(" n ", "");

            if (isSinglePayment)
                json.put("CREATEDAT", paypalPaymentTime);
            else
                json.put("CREATEDAT", "");

            json.put("CURRENCY", "GBP");
            json.put("ENDDATE", "");
            json.put("JUDOID", "");
            json.put("MERCHANTNAME", "kwikpay");
            json.put("MESSAGE", "");
            json.put("NETAMOUNT", receivedAppAmt);
            json.put("ORIGINALAMOUNT", receivedAppAmt);
            json.put("ORIGINALRECEIPTID", "");
            json.put("PARTNERSERVICEFEE", "");
            json.put("RAWRECEIPT", "");

            if (isSinglePayment)
                json.put("RECEIPTID", paypalPaymentAuthId);
            else
                json.put("RECEIPTID", metaDataID);

            json.put("REFUNDS", "");
            json.put("RESULT", "Success");
            json.put("TYPE", "payment");
            json.put("CONSUMERREFERENCE", "");
            json.put("PAYMENTREFERENCE", "");

            if (isSinglePayment)
                json.put("STATUS", paypalPaymentState);
            else
                json.put("STATUS", statusTag);

            json.put("ORDERID", receivedAppOrderId);
            json.put("CHECKSUM ", checkSumString);

            //			Log.e("","Auth save pg "+json.toString());
            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //		super.onBackPressed();
    }

    @Override
    public void onRequestComplete(String loadedString) {
        // TODO Auto-generated method stub
//		Log.e("", "PayPal Requested Result "+loadedString);

        if (loadedString != null && !loadedString.equals("")) {
            if (parsePPPaymentResponse(loadedString)) {
                // IF PAYPAL OTP VERIFICATION IS NOT REQUIRED AND PROCEEDING NORMAL FLOW
                if (nextProcess == 1) {
                    setContentView(R.layout.payment_gif_screen);
                    nextProcess = 0;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            callingReceiptScreen();
                        }
                    }, delayTimeInterval);
                }

                // IF PAYPAL OTP VERIFICATION IS REQUIRED, OPENING NEW PAYPAL OTP SCREEN
                else if (nextProcess == 2) {
                    nextProcess = 0;
                    Intent ppOtpIntent = new Intent(PayPalPaymentScreen.this, PayPalOTPVerification.class);
                    ppOtpIntent.putExtra("PP_EMAIL_ID", paypalReceivedEmailId);
                    ppOtpIntent.putExtra("PP_ORDER_ID", receivedAppOrderId);
                    startActivityForResult(ppOtpIntent, REQUEST_CODE_PAYPAL_OTP);
                } else if (nextProcess == 3) {
                    showPayPalErrorDialog();
                }
            } else {
                paypalErrorMessage = getResources().getString(R.string.common_checkNetConnection);
                showPayPalErrorDialog();
            }
        } else {
            ARCustomToast.showToast(PayPalPaymentScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    //LOGIC FOR PARSING THE RECEIVED DATA FROM PAYPAL
    public boolean parsePPPaymentResponse(String judoResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(judoResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(PayPalPaymentScreen.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    nextProcess = 1;
                    delayTimeInterval = 40000;

                    //UNCOMMENT BELOW CODE, IF WANT DYNAMIC TIME OF WAITING TIME FROM API FOR PAYPAL

					/*if(parentResponseObject.has("WAITINGTIME"))
                    {
						nextProcess=1;
						delayTimeInterval=Long.parseLong(parentResponseObject.getString("WAITINGTIME"));
					}*/
                } else {
                    if (parentResponseObject.has("OTPRESPONSECODE")) {
                        if (parentResponseObject.getString("OTPRESPONSECODE").equals("1")) {
                            // INITIATE PAYPAL OTP VERIFICATION
                            nextProcess = 2;

                            if (parentResponseObject.has("PAYPALEMAIL"))
                                paypalReceivedEmailId = parentResponseObject.getString("PAYPALEMAIL");

                            delayTimeInterval = 40000;
                            //UNCOMMENT BELOW CODE, IF WANT DYNAMIC TIME OF WAITING TIME FROM API FOR PAYPAL
                            /*if(parentResponseObject.has("WAITINGTIME"))
                                delayTimeInterval=Long.parseLong(parentResponseObject.getString("WAITINGTIME"));*/
                        } else {
                            // GETTING ERROR RESPONSE FROM API,IF "RESPONSECODE=1 && OTPRESPONSECODE=0"
                            if (parentResponseObject.has("DESCRIPTION")) {
                                nextProcess = 3;
                                paypalErrorMessage = parentResponseObject.getString("DESCRIPTION");
                            }
                        }
                    } else {
                        ARCustomToast.showToast(PayPalPaymentScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            delayTimeInterval = 0;
            return false;
        }
        return true;

    }

    private static String encryptInSHA256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    // CALLING THIS METHOD FOR PROCEEDING FURTHER TO RECEIPT SCREEN WITH ORDER ID OR TXN ID
    private void callingReceiptScreen() {
        Intent receiptIntent = new Intent(PayPalPaymentScreen.this, ReceiptScreen.class);
        receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedAppTxnId);
        startActivity(receiptIntent);

        PayPalPaymentScreen.this.finish();
    }

    private void showPayPalErrorDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(paypalErrorMessage);
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                Intent homeIntent = new Intent(PayPalPaymentScreen.this, HomeScreen.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                PayPalPaymentScreen.this.finish();
            }
        });
        builder.setCancelable(false);
        dialog = builder.show();
    }
}
