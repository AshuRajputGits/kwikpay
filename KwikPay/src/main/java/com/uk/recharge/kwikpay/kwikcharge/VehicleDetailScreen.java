package com.uk.recharge.kwikpay.kwikcharge;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.models.ConnectorListMO;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.kwikcharge.models.RegistrationNumbersMO;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Ashu Rajput on 9/26/2017.
 */

public class VehicleDetailScreen extends AppCompatActivity implements AsyncRequestListenerViaPost {

    private EditText registrationNumberET;
    private SharedPreferences preference;
    private JSONArray vehicleJsonArray;
    private Spinner vehicleMakeSpinner, vehicleModelSpinner, vehicleYearSpinner, vehicleBatteryTypeSpinner;
    private String defaultRegMake = "", defaultRegModel = "", defaultRegYear = "", defaultRegEngineType = "",
            defaultRegBodyShape = "", defaultRegBatteryType = "";
    private String defaultRate = "0.0", enteredRegNo = "";
    private String receivedOpCode = "", receivedLocationNumber = "", receivedServiceId = "";
    private Set<String> makeHashSet = null, modelHashSet = null, batteryTypeHashSet = null;
    private ArrayList<RegistrationNumbersMO> modelList = null;
    //    private boolean isSettingDefaultValue = false;
    private LinearLayout vehicleSpinnerLayout;
    private TextView selectYourVehicleMessage;
    private String evRateScreenBanner = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_vehicle_screen);

        setResourceIds();

        registrationNumberET = findViewById(R.id.registrationNumberET);

        registrationNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() == 7 && count > 0) {
//                    vehicleMakeSpinner.setSelection(0);
                    cleanBatteryModelsAndSetDefaultValues();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if (s.toString().length() == 7) {
                    hideKeyboard(registrationNumberET);
                    new LoadResponseViaPost(VehicleDetailScreen.this, formRegNumberJSON(s.toString()), true).execute("");
                }*/
            }
        });

        vehicleMakeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {

//                    Log.e("RegNo", "Pos " + position);
//                    filterModelYearBatteryTypeSpinner(modelList.get(position).getMakeId());
                    filterModelYearBatteryTypeSpinner(parent.getItemAtPosition(position).toString());
                }
                modelHashSet = batteryTypeHashSet = null;
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        findViewById(R.id.headerHomeBtn).setOnClickListener(onClickListener);
        findViewById(R.id.selectVehicleProceedButton).setOnClickListener(onClickListener);
        findViewById(R.id.vehicleRegistrationGoButton).setOnClickListener(onClickListener);

        try {
            registrationNumberET.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(7)});
        } catch (Exception e) {
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if (viewId == R.id.headerHomeBtn) {
                Intent intent = new Intent(VehicleDetailScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            } else if (viewId == R.id.vehicleRegistrationGoButton) {
                validateVehicleRegNumberOnGoButton();
            } else if (viewId == R.id.selectVehicleProceedButton) {
                validateVehicleEntryDetails();
            }
        }
    };

    private void setResourceIds() {
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        selectYourVehicleMessage = (TextView) findViewById(R.id.selectYourVehicleMessage);
        vehicleSpinnerLayout = (LinearLayout) findViewById(R.id.vehicleSpinnerLayout);
        vehicleMakeSpinner = (Spinner) findViewById(R.id.vehicleMakeSpinner);
        vehicleModelSpinner = (Spinner) findViewById(R.id.vehicleModelSpinner);
        vehicleYearSpinner = (Spinner) findViewById(R.id.vehicleYearSpinner);
        vehicleBatteryTypeSpinner = (Spinner) findViewById(R.id.vehicleBatteryTypeSpinner);

        findViewById(R.id.headerHomeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VehicleDetailScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

    }

    private void validateVehicleEntryDetails() {
        if (registrationNumberET.getText().toString().isEmpty() || registrationNumberET.getText().toString().trim().equals(""))
            ARCustomToast.showToast(this, "Please enter valid registration number", Toast.LENGTH_LONG);
        else if (makeHashSet == null)
            ARCustomToast.showToast(this, "Please enter valid registration number to select make", Toast.LENGTH_LONG);
        else if (vehicleMakeSpinner != null && vehicleMakeSpinner.getSelectedItemPosition() == 0)
            ARCustomToast.showToast(this, "Please select valid make", Toast.LENGTH_LONG);
        else if (vehicleModelSpinner != null && vehicleModelSpinner.getSelectedItemPosition() == 0)
            ARCustomToast.showToast(this, "Please select valid model", Toast.LENGTH_LONG);
        else if (vehicleBatteryTypeSpinner != null && vehicleBatteryTypeSpinner.getSelectedItemPosition() == 0)
            ARCustomToast.showToast(this, "Please select valid battery type", Toast.LENGTH_LONG);
        else {
            getFinalSelectedValueFromSpinners();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(VehicleDetailScreen.this, ElectricChargeMiniDetails.class));
                }
            }, 500);
        }
    }

    private void validateVehicleRegNumberOnGoButton() {
        if (registrationNumberET.getText().toString().isEmpty() || registrationNumberET.getText().toString().trim().equals(""))
            ARCustomToast.showToast(this, "Please enter valid registration number", Toast.LENGTH_LONG);
        else {
            hideKeyboard(registrationNumberET);
            new LoadResponseViaPost(VehicleDetailScreen.this, formRegNumberJSON(registrationNumberET.getText().toString()), true).execute("");
        }
    }

    private void cleanBatteryModelsAndSetDefaultValues() {
        selectYourVehicleMessage.setVisibility(View.GONE);
        vehicleSpinnerLayout.setVisibility(View.GONE);
        String emptyArray[] = {};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_textsize, emptyArray);
        vehicleMakeSpinner.setAdapter(adapter);
        vehicleModelSpinner.setAdapter(adapter);
        vehicleBatteryTypeSpinner.setAdapter(adapter);
        makeHashSet = modelHashSet = batteryTypeHashSet = null;
    }

    public String formRegNumberJSON(String registrationNumber) {
        JSONObject jsonobj = new JSONObject();
        try {
            String receivedLocationNumber = "";
            try {
                receivedLocationNumber = KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getLocationNumber();
            } catch (Exception e) {
            }

            jsonobj.put("APISERVICE", "KP_REG_NUM_INFO");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("OP_CODE", "ELEC");
            jsonobj.put("SERVICEID", "4");
            jsonobj.put("LOCATION_NUMBER", receivedLocationNumber);
            jsonobj.put("REGISTRATION_NUMBER", registrationNumber);
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
//            jsonobj.put("USERID", "979");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void onRequestComplete(String loadedString) {
        Log.e("OnRequest", "Response " + loadedString);
        if (!loadedString.equals("") && !loadedString.equals("Exception")) {
            parseVehicleDetailsJson(loadedString);
        } else {
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    private boolean parseVehicleDetailsJson(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    enteredRegNo = childJsonObj.optString("REGISTRATION_NUMBER");
                    receivedOpCode = childJsonObj.optString("OP_CODE");
                    receivedLocationNumber = childJsonObj.optString("LOCATION_NUMBER");
                    receivedServiceId = childJsonObj.optString("SERVICEID");
                    defaultRegMake = childJsonObj.optString("REG_MAKE");
                    defaultRegModel = childJsonObj.optString("REG_MODEL");
                    defaultRegBatteryType = childJsonObj.optString("REG_BATTERY_TYPE");
                    defaultRegYear = childJsonObj.optString("REG_YEAR");
                    defaultRegEngineType = childJsonObj.optString("REG_ENGINETYPE");
                    defaultRegBodyShape = childJsonObj.optString("REG_BODYSHAPE");
                    defaultRate = childJsonObj.optString("RATE");
                    evRateScreenBanner = childJsonObj.optString("EV_RATE_SCREEN_BANNER");
                    Object object = childJsonObj.get("DATA");

                    if (object instanceof JSONObject) {
                        final JSONObject jsonOBJECT = (JSONObject) object;
                        vehicleJsonArray = new JSONArray();
                        vehicleJsonArray.put(jsonOBJECT);
                    } else if (object instanceof JSONArray) {
                        vehicleJsonArray = (JSONArray) object;
                    }

                    if (vehicleJsonArray != null && vehicleJsonArray.length() > 0) {

                        modelList = new ArrayList<>(vehicleJsonArray.length());
                        makeHashSet = new LinkedHashSet<>();
                        makeHashSet.add("Select Make");

                        for (int b = 0; b < vehicleJsonArray.length(); b++) {
                            RegistrationNumbersMO regNoModel = new RegistrationNumbersMO();
                            regNoModel.setMake(vehicleJsonArray.getJSONObject(b).optString("MAKE"));
                            regNoModel.setMakeId(vehicleJsonArray.getJSONObject(b).optString("MAKE_ID"));
                            regNoModel.setModel(vehicleJsonArray.getJSONObject(b).optString("MODEL"));
                            regNoModel.setModelId(vehicleJsonArray.getJSONObject(b).optString("MODEL_ID"));
                            regNoModel.setBatteryType(vehicleJsonArray.getJSONObject(b).optString("BATTERY_TYPE"));
                            regNoModel.setBatteryTypeId(vehicleJsonArray.getJSONObject(b).optString("BATTERY_TYPE_ID"));
                            regNoModel.setMaxTime(vehicleJsonArray.getJSONObject(b).optString("MAXTIME"));
                            modelList.add(regNoModel);

                            makeHashSet.add(vehicleJsonArray.getJSONObject(b).optString("MAKE"));
                        }
                        initializeSpinnersValue();

                        selectYourVehicleMessage.setVisibility(View.VISIBLE);
                        vehicleSpinnerLayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, childJsonObj.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void initializeSpinnersValue() {
        ArrayAdapter<String> makeAdapter = null;

        if (makeHashSet != null) {
            makeAdapter = new ArrayAdapter<>(this, R.layout.spinner_textsize, addHashSetToList(makeHashSet));
            makeAdapter.setDropDownViewResource(R.layout.single_row_spinner);
            vehicleMakeSpinner.setAdapter(makeAdapter);
            try {
                if (defaultRegMake != null && !defaultRegMake.isEmpty()) {
                    Iterator<String> itr = makeHashSet.iterator();
                    int counter = 0;
                    while (itr.hasNext()) {
                        if (defaultRegMake.equalsIgnoreCase(itr.next())) {
                            break;
                        }
                        counter = counter + 1;
                    }
                    vehicleMakeSpinner.setSelection(counter);
                    defaultRegMake = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void filterModelYearBatteryTypeSpinner(String selectedMakeId) {

        modelHashSet = new LinkedHashSet<>();
        batteryTypeHashSet = new LinkedHashSet<>();
        modelHashSet.add("Select Model");
        batteryTypeHashSet.add("Select Battery");

        for (RegistrationNumbersMO regNoMO : modelList) {
            if (regNoMO.getMake().equals(selectedMakeId)) {
                modelHashSet.add(regNoMO.getModel());
                batteryTypeHashSet.add(regNoMO.getBatteryType());
            }
        }
        if (modelHashSet.size() > 1 && batteryTypeHashSet.size() > 1)
            updateModelYearBatteryTypeSpinner();
    }

    private void updateModelYearBatteryTypeSpinner() {
        ArrayAdapter<String> makeAdapter = null;
        makeAdapter = new ArrayAdapter<>(this, R.layout.spinner_textsize, addHashSetToList(modelHashSet));
        makeAdapter.setDropDownViewResource(R.layout.single_row_spinner);
        vehicleModelSpinner.setAdapter(makeAdapter);

        try {
            if (defaultRegModel != null && !defaultRegModel.isEmpty()) {
                Iterator<String> itr = modelHashSet.iterator();
                int counter = 0;
                while (itr.hasNext()) {
                    if (defaultRegModel.equalsIgnoreCase(itr.next())) {
                        break;
                    }
                    counter = counter + 1;
                }
                vehicleModelSpinner.setSelection(counter);
                defaultRegModel = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        makeAdapter = new ArrayAdapter<>(this, R.layout.spinner_textsize, addHashSetToList(batteryTypeHashSet));
        makeAdapter.setDropDownViewResource(R.layout.single_row_spinner);
        vehicleBatteryTypeSpinner.setAdapter(makeAdapter);

        try {
            if (defaultRegBatteryType != null && !defaultRegBatteryType.isEmpty()) {
                Iterator<String> itr = batteryTypeHashSet.iterator();
                int counter = 0;
                while (itr.hasNext()) {
                    if (defaultRegBatteryType.equalsIgnoreCase(itr.next())) {
                        break;
                    }
                    counter = counter + 1;
                }
                vehicleBatteryTypeSpinner.setSelection(counter);
                defaultRegBatteryType = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized List<String> addHashSetToList(Set<String> hashSet) {
        List<String> listOfValues = new ArrayList<>();
        listOfValues.addAll(hashSet);
        return listOfValues;
    }

    private void getFinalSelectedValueFromSpinners() {
        for (RegistrationNumbersMO registrationNumbersMO : modelList) {
            if (vehicleMakeSpinner.getSelectedItem().toString().equalsIgnoreCase(registrationNumbersMO.getMake()) &&
                    vehicleModelSpinner.getSelectedItem().toString().equalsIgnoreCase(registrationNumbersMO.getModel()) &&
                    vehicleBatteryTypeSpinner.getSelectedItem().toString().equalsIgnoreCase(registrationNumbersMO.getBatteryType())) {

                LocationIdMO detailModel = new LocationIdMO();

                //GETTING AND SETTING CONNECTOR LIST RECEIVED FROM "KP_OPR_BY_LOCATION_NUM" api
                ArrayList<ConnectorListMO> storedConnectorList = KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getConnectorListMOList();
                if (storedConnectorList != null && storedConnectorList.size() > 0)
                    detailModel.setConnectorListMOList(storedConnectorList);

                detailModel.setRegistrationNumber(enteredRegNo);
                detailModel.setOperatorCode(receivedOpCode);
                detailModel.setLocationNumber(receivedLocationNumber);
                detailModel.setServiceId(receivedServiceId);
                detailModel.setRecentMake(registrationNumbersMO.getMake());
                detailModel.setRecentModel(registrationNumbersMO.getModel());
                detailModel.setRecentBatteryType(registrationNumbersMO.getBatteryType());
                detailModel.setRecentMaxTime(registrationNumbersMO.getMaxTime());
                detailModel.setRecentRate(defaultRate);
                detailModel.setRecentYear("");
                detailModel.setEvRateScreenBanner(evRateScreenBanner);
                try {
                    detailModel.setConnector1Desc(KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getConnector1Desc());
                    detailModel.setConnector2Desc(KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getConnector2Desc());
                    detailModel.setConnector3Desc(KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getConnector3Desc());
                } catch (Exception e) {
                }
//                detailModel.setRecentChargerType("");
                double calculatedMaxAmount = 0.0;
                try {
                    calculatedMaxAmount = (Double.parseDouble(defaultRate) * Double.parseDouble(registrationNumbersMO.getMaxTime()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                detailModel.setRecentMaxAmount("" + calculatedMaxAmount);
                KwikChargeDetailsSingleton.getInstance().setLocationIdMO(detailModel);
                break;
            }
        }
    }

    private void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
