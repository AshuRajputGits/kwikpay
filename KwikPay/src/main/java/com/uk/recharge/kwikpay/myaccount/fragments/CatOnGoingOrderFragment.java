package com.uk.recharge.kwikpay.myaccount.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.myaccount.adapters.CatOnGoingRVAdapter;

/**
 * Created by Ashu Rajput on 11/5/2018.
 */

public class CatOnGoingOrderFragment extends Fragment {

    private RecyclerView cateringOrderRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cat_ongoing_fragment, container, false);
        cateringOrderRecyclerView = view.findViewById(R.id.cateringOrderRecyclerView);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        cateringOrderRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        cateringOrderRecyclerView.setAdapter(new CatOnGoingRVAdapter(getActivity()));
    }
}
