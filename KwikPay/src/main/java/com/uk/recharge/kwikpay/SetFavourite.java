package com.uk.recharge.kwikpay;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidationWithMessage;
import com.uk.recharge.kwikpay.database.DBQueryMethods;

public class SetFavourite extends Activity implements OnClickListener, AsyncRequestListenerViaPost
{
	private EditText nickNameET;
	private TextView valueServiceProvider,valueDescription;
//	TextView headerProfileBtn,headerChangePWBtn,headerFavBtn,headerTicketBtn,cartItemCounter,headerBelow_ClassTV;
//	View separatorLine;
	private SharedPreferences preferences;
	private HashMap<String,String> receivedFavDetailHashMap;
//	private LinearLayout myTransitionLinearLayout;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setfavourite_screen);

		settingIds();

		Bundle receivedBundle=getIntent().getExtras();
		if(receivedBundle!=null)
		{
			if(receivedBundle.containsKey("MYAC_FAV_HASHMAP"))
			{
				receivedFavDetailHashMap = (HashMap<String, String>) receivedBundle.getSerializable("MYAC_FAV_HASHMAP");

				valueServiceProvider.setText(receivedFavDetailHashMap.get(ProjectConstant.TXN_OPERATORNAME));
				valueDescription.setText(Html.fromHtml(receivedFavDetailHashMap.get(ProjectConstant.TXN_PAYMENTCURRENCYCODE))+" " +
						receivedFavDetailHashMap.get(ProjectConstant.TXN_PAYMENTAMOUNT));
			}
		}

	}


	public String formJSONParam() {
		JSONObject jsonobj = new JSONObject();
		try 
		{
			String trimmedNickName = nickNameET.getText().toString().replaceAll(" +"," ").trim();
			jsonobj.put("APISERVICE", "KPSAVEFAVOURITE");
			jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
			jsonobj.put("DEVICEOS", "Android");
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("USERID",preferences.getString("KP_USER_ID", "0"));
			jsonobj.put("NICKNAME", trimmedNickName);
			jsonobj.put("AMOUNT", receivedFavDetailHashMap.get(ProjectConstant.TXN_PAYMENTAMOUNT));
			jsonobj.put("ORDERID",receivedFavDetailHashMap.get(ProjectConstant.TXN_ORDERID));
			jsonobj.put("SUBSCRIBERNUM", receivedFavDetailHashMap.get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
			jsonobj.put("OPERATORCODE",receivedFavDetailHashMap.get(ProjectConstant.TXN_OPERATORCODE));
			jsonobj.put("SERVICEID", receivedFavDetailHashMap.get(ProjectConstant.TXN_SERVICEID));
			jsonobj.put("COUNTRYCODE",receivedFavDetailHashMap.get(ProjectConstant.TXN_COUNTRYCODE));
			jsonobj.put("FAVOURITEENABLEFLAG","1");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();

	}

	private void settingIds() 
	{
		// TODO Auto-generated method stub

		preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);	

		nickNameET=(EditText)findViewById(R.id.setFav_NickNameET);
		valueServiceProvider=(TextView)findViewById(R.id.setFav_ServiceProviderTV);
		valueDescription=(TextView)findViewById(R.id.setFav_DescriptionTV);

//		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);
//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);
		
//		myTransitionLinearLayout=(LinearLayout)findViewById(R.id.myTransitionHeaderLayout);
//		myTransitionLinearLayout.setVisibility(View.VISIBLE);

		// IDs of all four buttons
//		headerFavBtn=(TextView)findViewById(R.id.header_FavouriteBtn);
//		headerProfileBtn=(TextView)findViewById(R.id.header_MyProfileBtn);
//		headerChangePWBtn=(TextView)findViewById(R.id.header_ChangePWBtn);
//		headerTicketBtn=(TextView)findViewById(R.id.header_MyTicketBtn);

		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(SetFavourite.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

//		headerFavBtn.setOnClickListener(this);
//		headerProfileBtn.setOnClickListener(this);
//		headerChangePWBtn.setOnClickListener(this);
//		headerTicketBtn.setOnClickListener(this);

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME

		DBQueryMethods database=new DBQueryMethods(SetFavourite.this);
		try
		{
			database.open();
			TextView headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
			headerBelow_ClassTV.setText(database.getSubHeaderTitles("Set as favourites"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}

	}

	public void favouriteCancelOrSetFav(View setFavView)
	{

		switch (setFavView.getId()) 
		{
		case R.id.setFav_CancelBtn:

			SetFavourite.this.finish();

			break;

		case R.id.setFav_SetFavBtn:

			if(!FieldValidationWithMessage.nameIsValidNickName(nickNameET.getText().toString(), nickNameET).equals("SUCCESS"))
			{
				ARCustomToast.showToast(SetFavourite.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			}
			else
			{
				new LoadResponseViaPost(SetFavourite.this, formJSONParam() , true).execute("");
			}

			break;

		default:
			break;
		}

	}

	@Override
	public void onClick(View v) 
	{
		// TODO Auto-generated method stub
		/*if(v.getId()==R.id.header_FavouriteBtn)
		{
			startActivity(new Intent(SetFavourite.this,FavouriteScreen.class));
		}
		if(v.getId()==R.id.header_MyProfileBtn)
		{
			startActivity(new Intent(SetFavourite.this,EditProfile.class));
		}
		if(v.getId()==R.id.header_ChangePWBtn)
		{
			startActivity(new Intent(SetFavourite.this,ChangePassword.class));
		}
		if(v.getId()==R.id.header_MyTicketBtn)
		{
			startActivity(new Intent(SetFavourite.this,MyTicket.class));
		}*/

	}


	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub

		if(loadedString!=null && !loadedString.equals(""))
		{
			if(!loadedString.equals("Exception"))
			{
				try 
				{
					JSONObject set_fav_json = new JSONObject(loadedString);
					JSONObject set_fav_json_response = set_fav_json.getJSONObject(ProjectConstant.RESPONSE_TAG);

					if(set_fav_json_response.has(ProjectConstant.API_RESPONSE_CODE))
					{
						if(set_fav_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
						{
							Intent redirectToFav=new Intent(SetFavourite.this,FavouriteScreen.class);
							startActivity(redirectToFav);
							SetFavourite.this.finish();
						}	
						else
						{
							if(set_fav_json_response.has("DESCRIPTION"))
							{
								ARCustomToast.showToast(SetFavourite.this,set_fav_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
							}

						}
					}

				}	

				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				ARCustomToast.showToast(SetFavourite.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
			}

		}
		
		else
		{
			ARCustomToast.showToast(SetFavourite.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}
	}
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preferences!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preferences.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}
		
		}
	}*/
	
}
