package com.uk.recharge.kwikpay.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

public class MyCartAdapter extends BaseAdapter
{
	Activity context;
	ArrayList<HashMap<String, String>> myCartArrayList;
	
	String cartTxtId="";
	CartAdapterListener cartListner;

	public MyCartAdapter(Activity context,ArrayList<HashMap<String, String>> myCartArrayList) 
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		cartListner=(CartAdapterListener) context;
		this.myCartArrayList = myCartArrayList;
		
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return myCartArrayList.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder 
	{
		TextView tvServProvMobNo,tvTopUpAmtnDiscountAmt, tvTxnFeeTPA;
		TextView deleteBtn;
		ViewHolder(View view)
		{
			tvServProvMobNo = (TextView) view.findViewById(R.id.mycartServProMobNo);
			tvTopUpAmtnDiscountAmt = (TextView) view.findViewById(R.id.mycartTopupAmntDiscoutAmt);
			tvTxnFeeTPA = (TextView) view.findViewById(R.id.mycartTxnFeeTAP);
			deleteBtn = (TextView) view.findViewById(R.id.mycart_DeleteTrans);
		}

	}

	@SuppressWarnings("static-access")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		View row=convertView;
		ViewHolder holder;

		if(row==null)
		{
			LayoutInflater inflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row=inflater.inflate(R.layout.single_mycart_row, parent, false);
			holder=new ViewHolder(row);
			row.setTag(holder);
		}

		else
		{
			holder=(ViewHolder)row.getTag();
		}

		// Ashu... Setting Background color to list view at odd and even position
		if(position%2==0)
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewEvenColor));
		}
		else
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewOddColor));
		}

		holder.tvServProvMobNo.setText(myCartArrayList.get(position).get(ProjectConstant.TXN_OPERATORNAME)+"\n"+
				myCartArrayList.get(position).get(ProjectConstant.CART_MOBILENUMBER));
		
		String discountAmount="\n"+Html.fromHtml(myCartArrayList.get(position).get(ProjectConstant.TXN_PAYMENTCURRENCYCODE))+
				  myCartArrayList.get(position).get(ProjectConstant.CART_DISCOUNTAMOUNT);
		
		/*if(myCartArrayList.get(position).get(ProjectConstant.CART_DISCOUNTAMOUNT).equals(""))
		{
			discountAmount="";
		}
		else
		{
			discountAmount="\n"+Html.fromHtml(myCartArrayList.get(position).get(ProjectConstant.TXN_PAYMENTCURRENCYCODE))+
					  myCartArrayList.get(position).get(ProjectConstant.CART_DISCOUNTAMOUNT);
		}*/

		holder.tvTopUpAmtnDiscountAmt.setText(Html.fromHtml(myCartArrayList.get(position).get(ProjectConstant.TXN_DISPLAYCURRENCYCODE))+
				myCartArrayList.get(position).get(ProjectConstant.TXN_DISPLAYAMOUNT)+discountAmount );
		
		String transactionFee=paymentConverter(myCartArrayList.get(position).get(ProjectConstant.PAYNOW_PROCESSINGFEE), 
							  myCartArrayList.get(position).get(ProjectConstant.PAYNOW_SERVICEFEE) );
		
		holder.tvTxnFeeTPA.setText(Html.fromHtml(myCartArrayList.get(position).get(ProjectConstant.TXN_PAYMENTCURRENCYCODE))+transactionFee +"\n"+
				Html.fromHtml(myCartArrayList.get(position).get(ProjectConstant.TXN_PAYMENTCURRENCYCODE))+
				myCartArrayList.get(position).get(ProjectConstant.CART_NETPGAMOUNT));

		holder.deleteBtn.setTag(position);
		holder.deleteBtn.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View view) 
			{

				String pos = view.getTag().toString();
				int deletePos = Integer.parseInt(pos);
				
				cartTxtId=myCartArrayList.get(deletePos).get(ProjectConstant.CART_CARTID);
				
				cartListner.cartDeleteRequestComplete(cartTxtId,deletePos);

				/*String pos = view.getTag().toString();
				int deletePos = Integer.parseInt(pos);
				myCartArrayList.remove(deletePos);
				MyCartAdapter.this.notifyDataSetChanged();*/
			}
		});

		return row;

	}

	public interface CartAdapterListener
	{
		public void cartDeleteRequestComplete(String cartId,int deletePosition);
	}
	
	@SuppressLint("DefaultLocale")
	public String paymentConverter(String amount1,String amount2)
	{
		if(amount1.equals(""))
			amount1="0.00";
		if(amount2.equals(""))
			amount2="0.00";
		try
		{
			Float floatAmt1=Float.parseFloat(amount1);
			Float floatAmt2=Float.parseFloat(amount2);

			floatAmt1=floatAmt1+floatAmt2;

			return String.format("%.2f", floatAmt1);

		}catch(Exception e)
		{

		}

		return "0.00";
	}


}
