package com.uk.recharge.kwikpay.myaccount.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.myaccount.MyTransactionSingleton;
import com.uk.recharge.kwikpay.myaccount.MyTransactionViewDetails;
import com.uk.recharge.kwikpay.myaccount.MyTransactionsMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 23-12-2017.
 */

public class DigitalTransactionAdapter extends RecyclerView.Adapter<DigitalTransactionAdapter.DigitalTransactionViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<MyTransactionsMO> digitalTransactionList = null;
    private Context context = null;

    public DigitalTransactionAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        digitalTransactionList = MyTransactionSingleton.getInstance().getDigitalTransactionList();
        this.context = context;
    }

    @Override
    public DigitalTransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.my_transactions_row, parent, false);
        return new DigitalTransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DigitalTransactionViewHolder holder, int position) {
        holder.transactionRCAmount.setText(Utility.fromHtml(digitalTransactionList.get(position).getTransactionDisplayCurrencyCode())
                + digitalTransactionList.get(position).getTransactionDisplayAmount());
        if (digitalTransactionList.get(position).getTransactionPaymentStatus().equalsIgnoreCase("SUCCESSFUL")) {
            holder.transactionRechargeStatus.setText(digitalTransactionList.get(position).getTransactionRechargeStatus());
        } else {
            holder.transactionRechargeStatus.setText(digitalTransactionList.get(position).getTransactionPaymentStatus());
            holder.transactionRechargeStatus.setTextColor(Color.RED);
        }
        holder.transactionRechargeType.setText(digitalTransactionList.get(position).getTransactionOperatorName());
        holder.transactionDateTime.setText(digitalTransactionList.get(position).getTransactionDateTime());
        holder.transactionOrderID.setText("Order No " + digitalTransactionList.get(position).getTransactionOrderId());

        try {
            Glide.with(context).load(digitalTransactionList.get(position).getImageUrl())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(holder.operatorImages);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (digitalTransactionList != null && digitalTransactionList.size() > 0)
            return digitalTransactionList.size();
        return 0;
    }

    public class DigitalTransactionViewHolder extends RecyclerView.ViewHolder {

        TextView transactionRCAmount;
        TextView transactionRechargeStatus;
        TextView transactionRechargeType;
        TextView transactionDateTime;
        TextView transactionOrderID;
        ImageView operatorImages;

        public DigitalTransactionViewHolder(View itemView) {
            super(itemView);
            transactionRCAmount = itemView.findViewById(R.id.transactionRCAmount);
            transactionRechargeStatus = itemView.findViewById(R.id.transactionRechargeStatus);
            transactionRechargeType = itemView.findViewById(R.id.transactionRechargeType);
            transactionDateTime = itemView.findViewById(R.id.transactionDateTime);
            transactionOrderID = itemView.findViewById(R.id.transactionOrderID);
            operatorImages = itemView.findViewById(R.id.operatorImages);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, MyTransactionViewDetails.class)
                            .putExtra("SELECTED_TRANSACTION", digitalTransactionList.get(getLayoutPosition())));
                }
            });
        }
    }

}
