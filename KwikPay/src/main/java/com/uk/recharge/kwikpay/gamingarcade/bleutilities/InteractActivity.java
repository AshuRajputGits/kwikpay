package com.uk.recharge.kwikpay.gamingarcade.bleutilities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.uk.recharge.kwikpay.R;


public class InteractActivity extends AppCompatActivity {

    public static final String EXTRA_DEVICE_ADDRESS = "mAddress";
    private final GattClient mGattClient = new GattClient();
    private TextView bleDataPreviewer;
    private EditText writeDataET;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interact_activity);

        String address = getIntent().getStringExtra(EXTRA_DEVICE_ADDRESS);
        mGattClient.onCreate(this, address, new GattClient.OnCounterReadListener() {
            @Override
            public void onCounterRead(final int value) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        mButton.setText(Integer.toString(value));
                        bleDataPreviewer.setText("BLE Data value size " + value);
                    }
                });
            }

            @Override
            public void onConnected(final boolean success) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (success)
                            bleDataPreviewer.setText("BLE is ready to write data");
                        else
                            bleDataPreviewer.setText("Error!!, unable to write or read data");

                        if (!success) {
                            Toast.makeText(InteractActivity.this, "Connection error", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }

            @Override
            public void onCharacterResponseRead(String response) {
                bleDataPreviewer.setText("BLE Data value response " + response);
            }
        });

        bleDataPreviewer = findViewById(R.id.bleDataPreviewer);
        writeDataET = findViewById(R.id.writeDataET);

        findViewById(R.id.readDataFromBLE).setOnClickListener(onClickListener);
        findViewById(R.id.writeDataFromBLE).setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.writeDataFromBLE) {
                if (writeDataET.getText().toString().isEmpty() && !writeDataET.getText().toString().trim().equals(""))
                    Toast.makeText(InteractActivity.this, "Enter writable data", Toast.LENGTH_LONG).show();
                else
                    mGattClient.writeInteractor(writeDataET.getText().toString());
            } else if (v.getId() == R.id.readDataFromBLE) {
                bleDataPreviewer.setText("BLE Data " + mGattClient.readData());
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGattClient.onDestroy();
    }
}
