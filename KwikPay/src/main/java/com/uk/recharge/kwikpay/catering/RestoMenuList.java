package com.uk.recharge.kwikpay.catering;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Product;
import com.uk.recharge.kwikpay.RoomDataBase.Models.ProductType;
import com.uk.recharge.kwikpay.adapters.catering.ProductExpandableListAdapter;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.uk.recharge.kwikpay.catering.CateringCart.cartProductId;
import static com.uk.recharge.kwikpay.catering.CateringCart.cartProducts;
import static com.uk.recharge.kwikpay.catering.CateringCart.cartTotal;
import static com.uk.recharge.kwikpay.catering.CateringCart.updateCartTotal;
import static com.uk.recharge.kwikpay.catering.CateringCart.updateCartVisibility;
import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;

public class RestoMenuList extends KPSuperClass {

    public static TextView cartCountTextView;
    public static LinearLayout cartLayout;
    public static TextView cartTotalTextView;
    public static ImageView imgbase;

    ProductExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    SharedPreferences preference;

    AppDatabase appDatabase;
    List<Product> productList = new ArrayList<>();
    HashMap<Integer, List<ProductType>> productTypePerProductList = new HashMap<>();
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createMenuDrawer(this);
        init();

        productList = appDatabase.productDao().getAll();

        for (Product product : productList) {
            List<ProductType> productTypeList = appDatabase.productTypeDao().getAll(product.getName());
            productTypePerProductList.put(productList.indexOf(product), productTypeList);
        }

        findViewById(R.id.backbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartProducts.isEmpty())
                    Toast.makeText(context, "Cart is Empty!", Toast.LENGTH_SHORT).show();
                else {
                    Intent intent = new Intent(RestoMenuList.this, MyOrders.class);
                    startActivity(intent);
                }
            }
        });

        updateCartVisibility(cartLayout);
//        Log.d("Pooja data", preference.getString("DISTRIBUTOR_NAME", "name"));
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_resto_menu_list;
    }

    private void init() {
        context = this;
        expListView = findViewById(R.id.lvExp);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        cartLayout = findViewById(R.id.cart_layout);
        imgbase = findViewById(R.id.imgbase);
        cartCountTextView = findViewById(R.id.cart_count);
        cartTotalTextView = findViewById(R.id.cart_total);

        TextView Distributordetails = findViewById(R.id.Disributordetails);
        Distributordetails.setText(Utility.fromHtml("<font color='#00581e'>" + preference.getString("DISTRIBUTOR_NAME", "name") + "<br />" +
                "<medium><medium><font color='#8cd600'>" + preference.getString("DISTRIBUTOR_ADDRESS", "address") + "</medium></medium>"));
        ImageView disributorimg = findViewById(R.id.disributorimg);

        Glide.with(this)
                .load(preference.getString("DISTRIBUTOR_IMAGE", "image"))
                .placeholder(R.drawable.default_operator_logo)
                .into(disributorimg);

        initCart();

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

    }

    private void initCart() {
        cartProductId = new ArrayList<>();
        cartProducts = new ArrayList<>();
        cartTotal = "0.00";
    }

    @Override
    protected void onResume() {
        super.onResume();
        initProducts();
        updateCartVisibility(cartLayout);
    }

    void initProducts() {
        updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
        listAdapter = new ProductExpandableListAdapter(this, productList, productTypePerProductList);
        expListView.setAdapter(listAdapter);
    }

}
