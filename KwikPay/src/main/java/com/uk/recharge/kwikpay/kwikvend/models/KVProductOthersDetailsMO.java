package com.uk.recharge.kwikpay.kwikvend.models;

/**
 * Created by Ashu Rajput on 10/1/2018.
 */

public class KVProductOthersDetailsMO {

    private String merchantId;
    private String qrCode;
    private String machineType;
    private String maxVendCapacity;
    private String totalProductAmount;
    private String totalCartAmount;
    private String countryName;
    private String currencyName;
    private String currencySign;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getMaxVendCapacity() {
        return maxVendCapacity;
    }

    public void setMaxVendCapacity(String maxVendCapacity) {
        this.maxVendCapacity = maxVendCapacity;
    }

    public String getTotalProductAmount() {
        return totalProductAmount;
    }

    public void setTotalProductAmount(String totalProductAmount) {
        this.totalProductAmount = totalProductAmount;
    }

    public String getTotalCartAmount() {
        return totalCartAmount;
    }

    public void setTotalCartAmount(String totalCartAmount) {
        this.totalCartAmount = totalCartAmount;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencySign() {
        return currencySign;
    }

    public void setCurrencySign(String currencySign) {
        this.currencySign = currencySign;
    }
}
