package com.uk.recharge.kwikpay.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.AsyncTask;

import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.database.DBQueryMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DataDownloadApiParser extends AsyncTask<Void, Void, String> {
    DataDownloadListener listener;
    Context activityContext;
    String apiResponseToParse = "", howtouseValue = "", howitworksValue = "", aboutusValue = "", termsofusageValue = "";
    private String pgSoId = "", mopId = "", pgSoId1 = "", mopId1 = "", LANDING_PAGE_BANNERS = "";

    private ProgressDialog mProgressDialog;

    ArrayList<HashMap<String, String>> operatorAreaArrayList, areaGroupArrayList, operatorServiceArrayList, appTableVersionArrayList,
            operatorArrayList, phiArrayList;
    HashMap<String, String> operatorAreaHashMap, areaGroupHashMap, operatorServiceHashMap, appTabVersionHashMap, operatorHashMap, phiHashMap;

    DBQueryMethods database;

    SharedPreferences preference;

    public DataDownloadApiParser(Context activityContext, String apiResponseToParse) {
        this.activityContext = activityContext;
        this.apiResponseToParse = apiResponseToParse;
        listener = (DataDownloadListener) activityContext;
        database = new DBQueryMethods(activityContext);

        mProgressDialog = new ProgressDialog(activityContext);
        mProgressDialog.setMessage(" Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        /*if(mProgressDialog!=null)
            mProgressDialog.show();*/
    }

    @Override
    protected String doInBackground(Void... params) {
        // TODO Auto-generated method stub
        if (parseMyResponse(apiResponseToParse))
            return "PARSED";
        else
            return "NOT PARSED";

    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub

        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.cancel();

        listener.parsingResponse(result);
    }

    public boolean parseMyResponse(String apiResponseToParse) {
        if (apiResponseToParse != null && !apiResponseToParse.equals("")) {
            JSONObject jsonObject, parentResponseObject, childResponseObject;

            try {
                database.open();
                jsonObject = new JSONObject(apiResponseToParse);

                if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                    return false;
                }

                parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

                // INDIVIDUAL TAGS UNDER PARENT TAG ie KPRESPONSE

                if (parentResponseObject.has(ProjectConstant.DB_HOW_TO_USE_REVISED))
                    howtouseValue = parentResponseObject.getString(ProjectConstant.DB_HOW_TO_USE_REVISED);
                if (parentResponseObject.has(ProjectConstant.DB_TERMS_OF_USAGE_REVISED))
                    termsofusageValue = parentResponseObject.getString(ProjectConstant.DB_TERMS_OF_USAGE_REVISED);
                if (parentResponseObject.has(ProjectConstant.DB_HOW_IT_WORKS_REVISED))
                    howitworksValue = parentResponseObject.getString(ProjectConstant.DB_HOW_IT_WORKS_REVISED);
                if (parentResponseObject.has(ProjectConstant.DB_ABOUTUS_REVISED))
                    aboutusValue = parentResponseObject.getString(ProjectConstant.DB_ABOUTUS_REVISED);

                if (parentResponseObject.has(ProjectConstant.PG_MOPID))
                    mopId = parentResponseObject.getString(ProjectConstant.PG_MOPID);
//					ProjectConstant.PG_MOP_ID_VALUE=parentResponseObject.getString(ProjectConstant.PG_MOPID);
                if (parentResponseObject.has("PGSOIG"))
                    pgSoId = parentResponseObject.getString("PGSOIG");
//					ProjectConstant.PG_SO_ID_VALUE=parentResponseObject.getString("PGSOIG");
                if (parentResponseObject.has("MOPID1"))
                    mopId1 = parentResponseObject.getString("MOPID1");
//					ProjectConstant.PG_MOP_ID_VALUE1=parentResponseObject.getString("MOPID1");
                if (parentResponseObject.has("PGSOIG1"))
                    pgSoId1 = parentResponseObject.getString("PGSOIG1");
//					ProjectConstant.PG_SO_ID_VALUE1=parentResponseObject.getString("PGSOIG1");

                LANDING_PAGE_BANNERS = parentResponseObject.optString("LANDING_PAGE_BANNERS");

                // PARSING OPERATOR TABLE
                if (parentResponseObject.has(ProjectConstant.OPERATOR_TAG)) {
                    JSONArray operatorJsonArray = parentResponseObject.getJSONArray(ProjectConstant.OPERATOR_TAG);
                    operatorArrayList = new ArrayList<>();

                    for (int i = 0; i < operatorJsonArray.length(); i++) {
                        childResponseObject = operatorJsonArray.getJSONObject(i);

                        if (!childResponseObject.isNull(ProjectConstant.OPERATOR_ID)
                                && !childResponseObject.isNull(ProjectConstant.OP_OPERATOR_ID_TAG)
                                && !childResponseObject.isNull(ProjectConstant.OPERATOR_ABBR)
                                && !childResponseObject.isNull(ProjectConstant.OPERATOR_HLR_CODE)
                                && !childResponseObject.isNull(ProjectConstant.OPERATOR_NAME)
                                && !childResponseObject.isNull(ProjectConstant.OPERATOR_ENABLE)
                                && !childResponseObject.isNull(ProjectConstant.OPERATOR_CF_ACTIVE_FLAG)) {
                            operatorHashMap = new HashMap<String, String>();
                            operatorHashMap.put(ProjectConstant.OPERATOR_ID, childResponseObject.getString(ProjectConstant.OPERATOR_ID));
                            operatorHashMap.put(ProjectConstant.OP_OPERATOR_ID_TAG, childResponseObject.getString(ProjectConstant.OP_OPERATOR_ID_TAG));
                            operatorHashMap.put(ProjectConstant.OPERATOR_ABBR, childResponseObject.getString(ProjectConstant.OPERATOR_ABBR));
                            operatorHashMap.put(ProjectConstant.OPERATOR_HLR_CODE, childResponseObject.getString(ProjectConstant.OPERATOR_HLR_CODE));
                            operatorHashMap.put(ProjectConstant.OPERATOR_NAME, childResponseObject.getString(ProjectConstant.OPERATOR_NAME));
                            operatorHashMap.put(ProjectConstant.OPERATOR_ENABLE, childResponseObject.getString(ProjectConstant.OPERATOR_ENABLE));
                            operatorHashMap.put(ProjectConstant.OPERATOR_CF_ACTIVE_FLAG, childResponseObject.getString(ProjectConstant.OPERATOR_CF_ACTIVE_FLAG));

                            operatorArrayList.add(operatorHashMap);
                        }
                    }

                    if (!database.insertIntoOperatorTable(operatorArrayList)) {
                        return false;
                    } else {
                    }
                }

                // PARSING OPERATOR AREA TABLE

                if (parentResponseObject.has(ProjectConstant.OPERATOR_AREA_TAG)) {
                    JSONArray opAreaJsonArray = parentResponseObject.getJSONArray(ProjectConstant.OPERATOR_AREA_TAG);
                    operatorAreaArrayList = new ArrayList<>();

                    for (int i = 0; i < opAreaJsonArray.length(); i++) {
                        childResponseObject = opAreaJsonArray.getJSONObject(i);

                        if (!childResponseObject.isNull(ProjectConstant.OA_ID)
                                && !childResponseObject.isNull(ProjectConstant.OA_AG_ID)
                                && !childResponseObject.isNull(ProjectConstant.OA_OS_ID)
                                && !childResponseObject.isNull(ProjectConstant.OA_CONFIGURED)
                                && !childResponseObject.isNull(ProjectConstant.OA_HLR_CODE)
                                && !childResponseObject.isNull(ProjectConstant.OA_ENABLE)
                                && !childResponseObject.isNull(ProjectConstant.OA_REQ_TYPE)
                                && !childResponseObject.isNull(ProjectConstant.OA_LOGO)
                                && !childResponseObject.isNull(ProjectConstant.OA_CF_ACTIVE_FLAG)) {
                            operatorAreaHashMap = new HashMap<String, String>();
                            operatorAreaHashMap.put(ProjectConstant.OA_ID, childResponseObject.getString(ProjectConstant.OA_ID));
                            operatorAreaHashMap.put(ProjectConstant.OA_AG_ID, childResponseObject.getString(ProjectConstant.OA_AG_ID));
                            operatorAreaHashMap.put(ProjectConstant.OA_OS_ID, childResponseObject.getString(ProjectConstant.OA_OS_ID));
                            operatorAreaHashMap.put(ProjectConstant.OA_CONFIGURED, childResponseObject.getString(ProjectConstant.OA_CONFIGURED));
                            operatorAreaHashMap.put(ProjectConstant.OA_HLR_CODE, childResponseObject.getString(ProjectConstant.OA_HLR_CODE));
                            operatorAreaHashMap.put(ProjectConstant.OA_ENABLE, childResponseObject.getString(ProjectConstant.OA_ENABLE));
                            operatorAreaHashMap.put(ProjectConstant.OA_REQ_TYPE, childResponseObject.getString(ProjectConstant.OA_REQ_TYPE));
                            operatorAreaHashMap.put(ProjectConstant.OA_LOGO, childResponseObject.getString(ProjectConstant.OA_LOGO));
                            operatorAreaHashMap.put(ProjectConstant.OA_CF_ACTIVE_FLAG, childResponseObject.getString(ProjectConstant.OA_CF_ACTIVE_FLAG));

                            operatorAreaArrayList.add(operatorAreaHashMap);
                        }
                    }
                    if (!database.insertIntoOprAreaTable(operatorAreaArrayList)) {
                        return false;
                    } else {
                    }
                }

                // PARSING AREA GROUP TABLE
                if (parentResponseObject.has(ProjectConstant.AREA_GROUP_TAG)) {
                    JSONArray areaGroupJsonArray = parentResponseObject.getJSONArray(ProjectConstant.AREA_GROUP_TAG);
                    areaGroupArrayList = new ArrayList<>();

                    for (int i = 0; i < areaGroupJsonArray.length(); i++) {
                        childResponseObject = areaGroupJsonArray.getJSONObject(i);

                        if (!childResponseObject.isNull(ProjectConstant.AG_ID)
                                && !childResponseObject.isNull(ProjectConstant.AG_ENABLE)
                                && !childResponseObject.isNull(ProjectConstant.AG_GROUP_CODE)
                                && !childResponseObject.isNull(ProjectConstant.AG_NAME)
                                && !childResponseObject.isNull(ProjectConstant.AG_CURRENCY)
                                && !childResponseObject.isNull(ProjectConstant.AG_PREFIX_ALLOW)
                                && !childResponseObject.isNull(ProjectConstant.AG_COUNTRY_CODE)
                                && !childResponseObject.isNull(ProjectConstant.AG_SUBS_TERM_USED)
                                && !childResponseObject.isNull(ProjectConstant.AG_SUBS_DATA_TYPE)
                                && !childResponseObject.isNull(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO)
                                && !childResponseObject.isNull(ProjectConstant.AG_SUBS_MIN_LENGTH)
                                && !childResponseObject.isNull(ProjectConstant.AG_SUBS_MAX_LENGTH)
                                && !childResponseObject.isNull(ProjectConstant.AG_PREFIX_LENGTH)
                                && !childResponseObject.isNull(ProjectConstant.AG_SECOND_VALIDATION)
                                && !childResponseObject.isNull(ProjectConstant.AG_CF_ACTIVE_FLAG)) {
                            areaGroupHashMap = new HashMap<String, String>();
                            areaGroupHashMap.put(ProjectConstant.AG_ID, childResponseObject.getString(ProjectConstant.AG_ID));
                            areaGroupHashMap.put(ProjectConstant.AG_ENABLE, childResponseObject.getString(ProjectConstant.AG_ENABLE));
                            areaGroupHashMap.put(ProjectConstant.AG_GROUP_CODE, childResponseObject.getString(ProjectConstant.AG_GROUP_CODE));
                            areaGroupHashMap.put(ProjectConstant.AG_NAME, childResponseObject.getString(ProjectConstant.AG_NAME));
                            areaGroupHashMap.put(ProjectConstant.AG_CURRENCY, childResponseObject.getString(ProjectConstant.AG_CURRENCY));
                            areaGroupHashMap.put(ProjectConstant.AG_PREFIX_ALLOW, childResponseObject.getString(ProjectConstant.AG_PREFIX_ALLOW));
                            areaGroupHashMap.put(ProjectConstant.AG_COUNTRY_CODE, childResponseObject.getString(ProjectConstant.AG_COUNTRY_CODE));
                            areaGroupHashMap.put(ProjectConstant.AG_SUBS_TERM_USED, childResponseObject.getString(ProjectConstant.AG_SUBS_TERM_USED));
                            areaGroupHashMap.put(ProjectConstant.AG_SUBS_DATA_TYPE, childResponseObject.getString(ProjectConstant.AG_SUBS_DATA_TYPE));
                            areaGroupHashMap.put(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO, childResponseObject.getString(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO));
                            areaGroupHashMap.put(ProjectConstant.AG_SUBS_MIN_LENGTH, childResponseObject.getString(ProjectConstant.AG_SUBS_MIN_LENGTH));
                            areaGroupHashMap.put(ProjectConstant.AG_SUBS_MAX_LENGTH, childResponseObject.getString(ProjectConstant.AG_SUBS_MAX_LENGTH));
                            areaGroupHashMap.put(ProjectConstant.AG_PREFIX_LENGTH, childResponseObject.getString(ProjectConstant.AG_PREFIX_LENGTH));
                            areaGroupHashMap.put(ProjectConstant.AG_SECOND_VALIDATION, childResponseObject.getString(ProjectConstant.AG_SECOND_VALIDATION));
                            areaGroupHashMap.put(ProjectConstant.AG_CF_ACTIVE_FLAG, childResponseObject.getString(ProjectConstant.AG_CF_ACTIVE_FLAG));

                            areaGroupArrayList.add(areaGroupHashMap);
                        }
                    }
                    if (!database.insertIntoAreaGroupTable(areaGroupArrayList)) {
                        return false;
                    } else {
                    }
                }

                // PARSING OPERATOR SERVICE TABLE
                if (parentResponseObject.has(ProjectConstant.DB_OP_SERVICE_TABLENAME)) {
                    JSONArray opServiceJsonArray = parentResponseObject.getJSONArray(ProjectConstant.DB_OP_SERVICE_TABLENAME);
                    operatorServiceArrayList = new ArrayList<>();

                    for (int i = 0; i < opServiceJsonArray.length(); i++) {
                        childResponseObject = opServiceJsonArray.getJSONObject(i);

                        if (!childResponseObject.isNull(ProjectConstant.OS_ID)
                                && !childResponseObject.isNull(ProjectConstant.OS_OP_ID)
                                && !childResponseObject.isNull(ProjectConstant.OS_SERVICE_ID)
                                && !childResponseObject.isNull(ProjectConstant.OS_OP_LOGO)
                                && !childResponseObject.isNull(ProjectConstant.OS_ENABLE)
                                && !childResponseObject.isNull(ProjectConstant.OS_IS_PLAN_RANGE)
                                && !childResponseObject.isNull(ProjectConstant.OS_CF_ACTIVE_FLAG)) {
                            operatorServiceHashMap = new HashMap<String, String>();
                            operatorServiceHashMap.put(ProjectConstant.OS_ID, childResponseObject.getString(ProjectConstant.OS_ID));
                            operatorServiceHashMap.put(ProjectConstant.OS_OP_ID, childResponseObject.getString(ProjectConstant.OS_OP_ID));
                            operatorServiceHashMap.put(ProjectConstant.OS_SERVICE_ID, childResponseObject.getString(ProjectConstant.OS_SERVICE_ID));
                            operatorServiceHashMap.put(ProjectConstant.OS_OP_LOGO, childResponseObject.getString(ProjectConstant.OS_OP_LOGO));
                            operatorServiceHashMap.put(ProjectConstant.OS_ENABLE, childResponseObject.getString(ProjectConstant.OS_ENABLE));
                            operatorServiceHashMap.put(ProjectConstant.OS_IS_PLAN_RANGE, childResponseObject.getString(ProjectConstant.OS_IS_PLAN_RANGE));
                            operatorServiceHashMap.put(ProjectConstant.OS_CF_ACTIVE_FLAG, childResponseObject.getString(ProjectConstant.OS_CF_ACTIVE_FLAG));

                            operatorServiceArrayList.add(operatorServiceHashMap);
                        }

                    }

                    if (!database.insertIntoOprServiceTable(operatorServiceArrayList)) {
                        return false;
                    }
                }

                // PARSING PAGE WISE HEADER INFO TABLE
                if (parentResponseObject.has(ProjectConstant.DB_KP_PHI_TABLENAME)) {
                    JSONArray phiJsonArray = parentResponseObject.getJSONArray(ProjectConstant.DB_KP_PHI_TABLENAME);
                    phiArrayList = new ArrayList<>();

                    for (int i = 0; i < phiJsonArray.length(); i++) {
                        childResponseObject = phiJsonArray.getJSONObject(i);

                        if (!childResponseObject.isNull(ProjectConstant.KP_PHI)
                                && !childResponseObject.isNull(ProjectConstant.KP_PHI_HEADER_CONTENT)
                                && !childResponseObject.isNull(ProjectConstant.KP_PHI_SCREEN_NAME)) {
                            phiHashMap = new HashMap<>();
                            phiHashMap.put(ProjectConstant.KP_PHI, childResponseObject.getString(ProjectConstant.KP_PHI));
                            phiHashMap.put(ProjectConstant.KP_PHI_HEADER_CONTENT, childResponseObject.getString(ProjectConstant.KP_PHI_HEADER_CONTENT));
                            phiHashMap.put(ProjectConstant.KP_PHI_SCREEN_NAME, childResponseObject.getString(ProjectConstant.KP_PHI_SCREEN_NAME));
                            phiArrayList.add(phiHashMap);
                        }
                    }

                    if (!database.insertIntoPHITable(phiArrayList)) {
                        return false;
                    }
                }


                // PARSING APP TABLE VERSION TABLE
                if (parentResponseObject.has(ProjectConstant.DB_APP_TABLE_VERSION_TABLENAME)) {
                    childResponseObject = parentResponseObject.getJSONObject(ProjectConstant.DB_APP_TABLE_VERSION_TABLENAME);
                    appTableVersionArrayList = new ArrayList<>();

                    if (!childResponseObject.isNull(ProjectConstant.DB_HOW_TO_USE_REVISED)
                            && !childResponseObject.isNull(ProjectConstant.DB_OPERATOR_TABLENAME)
                            && !childResponseObject.isNull(ProjectConstant.DB_OP_AREA_TABLENAME)
                            && !childResponseObject.isNull(ProjectConstant.DB_AG_TABLENAME)
                            && !childResponseObject.isNull(ProjectConstant.DB_OP_SERVICE_TABLENAME)
                            && !childResponseObject.isNull(ProjectConstant.DB_TERMS_OF_USAGE_REVISED)
                            && !childResponseObject.isNull(ProjectConstant.DB_HOW_IT_WORKS_REVISED)
                            && !childResponseObject.isNull(ProjectConstant.DB_ABOUTUS_REVISED)
                            && !childResponseObject.isNull(ProjectConstant.DB_KP_PHI_TABLENAME)) {
                        appTabVersionHashMap = new HashMap<>();
                        appTabVersionHashMap.put(ProjectConstant.DB_HOW_TO_USE_REVISED, childResponseObject.getString(ProjectConstant.DB_HOW_TO_USE_REVISED));
                        appTabVersionHashMap.put(ProjectConstant.DB_OPERATOR_TABLENAME, childResponseObject.getString(ProjectConstant.DB_OPERATOR_TABLENAME));
                        appTabVersionHashMap.put(ProjectConstant.DB_OP_AREA_TABLENAME, childResponseObject.getString(ProjectConstant.DB_OP_AREA_TABLENAME));
                        appTabVersionHashMap.put(ProjectConstant.DB_AG_TABLENAME, childResponseObject.getString(ProjectConstant.DB_AG_TABLENAME));
                        appTabVersionHashMap.put(ProjectConstant.DB_OP_SERVICE_TABLENAME, childResponseObject.getString(ProjectConstant.DB_OP_SERVICE_TABLENAME));
                        appTabVersionHashMap.put(ProjectConstant.DB_TERMS_OF_USAGE_REVISED, childResponseObject.getString(ProjectConstant.DB_TERMS_OF_USAGE_REVISED));
                        appTabVersionHashMap.put(ProjectConstant.DB_HOW_IT_WORKS_REVISED, childResponseObject.getString(ProjectConstant.DB_HOW_IT_WORKS_REVISED));
                        appTabVersionHashMap.put(ProjectConstant.DB_ABOUTUS_REVISED, childResponseObject.getString(ProjectConstant.DB_ABOUTUS_REVISED));
                        appTabVersionHashMap.put(ProjectConstant.DB_KP_PHI_TABLENAME, childResponseObject.getString(ProjectConstant.DB_KP_PHI_TABLENAME));

                        appTableVersionArrayList.add(appTabVersionHashMap);
                    }

                    if (!database.insertIntoAppTableVersion(appTableVersionArrayList)) {
                        return false;
                    }
                }


            } catch (JSONException e) {
                return false;
            } catch (SQLException e) {
                return false;
            } catch (Exception e) {
                return false;
            } finally {
                database.close();
                preference = activityContext.getSharedPreferences("KP_Preference", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preference.edit();
                editor.putString(ProjectConstant.DB_HOW_TO_USE_REVISED, howtouseValue);
                editor.putString(ProjectConstant.DB_HOW_IT_WORKS_REVISED, howitworksValue);
                editor.putString(ProjectConstant.DB_TERMS_OF_USAGE_REVISED, termsofusageValue);
                editor.putString(ProjectConstant.DB_ABOUTUS_REVISED, aboutusValue);

                editor.putString("PG_MOP_ID", mopId);
                editor.putString("PG_SO_ID", pgSoId);
                editor.putString("PG_MOP_ID1", mopId1);
                editor.putString("PG_SO_ID1", pgSoId1);
                editor.putString("LANDING_PAGE_BANNERS", LANDING_PAGE_BANNERS);
                editor.apply();
            }
        }
        return true;
    }

    public interface DataDownloadListener {
        void parsingResponse(String parseResponse);
    }

}
