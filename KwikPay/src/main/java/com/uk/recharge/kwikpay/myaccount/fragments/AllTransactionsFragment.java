package com.uk.recharge.kwikpay.myaccount.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.myaccount.adapters.AllTransactionsAdapter;

/**
 * Created by Ashu Rajput on 12/20/2017.
 */

public class AllTransactionsFragment extends Fragment {

    RecyclerView transactionRV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transactions_fragment, container, false);
        Log.e("Fragment---", "All");

        transactionRV = view.findViewById(R.id.transactionRecyclerView);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        transactionRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        transactionRV.setAdapter(new AllTransactionsAdapter(getActivity()));

    }
}
