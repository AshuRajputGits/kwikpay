package com.uk.recharge.kwikpay.RoomDataBase.Models;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Ingredient {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    private int Id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "category_action")
    private String category_action;

    @ColumnInfo(name = "parent_id")
    private String parent_id;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_action() {
        return category_action;
    }

    public void setCategory_action(String category_action) {
        this.category_action = category_action;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
