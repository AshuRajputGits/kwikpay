package com.uk.recharge.kwikpay.services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.uk.recharge.kwikpay.ProjectConstant;
import com.urbanairship.AirshipReceiver;
import com.urbanairship.actions.ActionValue;
import com.urbanairship.actions.DeepLinkAction;
import com.urbanairship.push.PushMessage;

import java.util.Map;

/**
 * Created by Ashu Rajput on 25-04-2017.
 */

public class KPAirshipReceiver extends AirshipReceiver {

    private static final String TAG = "SampleAirshipReceiver";

    @Override
    protected void onChannelCreated(@NonNull Context context, @NonNull String channelId) {
//        Log.i(TAG, "Channel created. Channel Id:" + channelId + ".");
    }

    @Override
    protected void onChannelUpdated(@NonNull Context context, @NonNull String channelId) {
//        Log.i(TAG, "Channel updated. Channel Id:" + channelId + ".");
    }

    @Override
    protected void onChannelRegistrationFailed(Context context) {
//        Log.i(TAG, "Channel registration failed.");
    }

    @Override
    protected void onPushReceived(@NonNull Context context, @NonNull PushMessage message, boolean notificationPosted) {
//        Log.i(TAG, "Received push message. Alert: " + message.getAlert() + ". posted notification: " + notificationPosted);
    }

    @Override
    protected void onNotificationPosted(@NonNull Context context, @NonNull NotificationInfo notificationInfo) {
//        Log.i(TAG, "Notification posted. Alert: " + notificationInfo.getMessage().getAlert() + ". NotificationId: " + notificationInfo.getNotificationId());
    }

    @Override
    protected boolean onNotificationOpened(@NonNull Context context, @NonNull NotificationInfo notificationInfo) {
//        Log.i(TAG, "Notification opened. Alert: " + notificationInfo.getMessage().getAlert() + ". NotificationId: " + notificationInfo.getNotificationId());

        Map<String, ActionValue> actions = notificationInfo.getMessage().getActions();
        if (actions.containsKey(DeepLinkAction.DEFAULT_REGISTRY_NAME) || actions.containsKey(DeepLinkAction.DEFAULT_REGISTRY_SHORT_NAME)) {
            // Just return true so SDK does not auto launch the main activity
            return true;
        }

        // Return false here to allow Urban Airship to auto launch the launcher activity
        return false;
    }

    @Override
    protected boolean onNotificationOpened(@NonNull Context context, @NonNull NotificationInfo notificationInfo, @NonNull ActionButtonInfo actionButtonInfo) {
        Log.i(TAG, "Notification action button opened. Button ID: " + actionButtonInfo.getButtonId() + ". NotificationId: " + notificationInfo.getNotificationId());

        Map<String, ActionValue> actions = notificationInfo.getMessage().getActions();
        if (actions.containsKey(DeepLinkAction.DEFAULT_REGISTRY_NAME) || actions.containsKey(DeepLinkAction.DEFAULT_REGISTRY_SHORT_NAME)) {
            // Just return true so SDK does not auto launch the main activity
            return true;
        }

        // Return false here to allow Urban Airship to auto launch the launcher
        // activity for foreground notification action buttons
        return false;
    }

    @Override
    protected void onNotificationDismissed(@NonNull Context context, @NonNull NotificationInfo notificationInfo) {
        Log.i(TAG, "Notification dismissed. Alert: " + notificationInfo.getMessage().getAlert() + ". Notification ID: " + notificationInfo.getNotificationId());
    }
}
