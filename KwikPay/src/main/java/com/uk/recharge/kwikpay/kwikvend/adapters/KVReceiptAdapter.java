package com.uk.recharge.kwikpay.kwikvend.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductDetailsMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 10/24/2018.
 */

public class KVReceiptAdapter extends RecyclerView.Adapter<KVReceiptAdapter.KVReceiptViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<KVProductDetailsMO> kvProductDetailsMOList = null;
    private Context context;

    public KVReceiptAdapter(Context context, ArrayList<KVProductDetailsMO> kvProductDetailsMOList) {
        layoutInflater = LayoutInflater.from(context);
        this.kvProductDetailsMOList = kvProductDetailsMOList;
        this.context = context;
    }

    @NonNull
    @Override
    public KVReceiptViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.kv_receipt_single_row, parent, false);
        return new KVReceiptViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KVReceiptViewHolder holder, int pos) {

        try {
            if (kvProductDetailsMOList.get(pos).getModelType() != null &&
                    kvProductDetailsMOList.get(pos).getModelType().equalsIgnoreCase("KVReceiptPlainMO")) {
                holder.kvReceiptProductTV.setText(kvProductDetailsMOList.get(pos).getProductName());
                holder.kvReceiptAmountTV.setText(kvProductDetailsMOList.get(pos).getProductCorePrice());
                holder.kvReceiptStatusIV.setVisibility(View.INVISIBLE);
            } else {
                holder.kvReceiptProductTV.setText(kvProductDetailsMOList.get(pos).getProductName());
                holder.kvReceiptQuantityTV.setText(kvProductDetailsMOList.get(pos).getProductDispensedCount());
                holder.kvReceiptAmountTV.setText(Utility.fromHtml(kvProductDetailsMOList.get(pos).getPaymentCurrencyCode())
                        + kvProductDetailsMOList.get(pos).getProductCorePrice());

                try {
                    int productDispenseCount = 0, productPurchaseCount = 0;

                    if (kvProductDetailsMOList.get(pos).getProductDispensedCount() != null && !kvProductDetailsMOList.get(pos).getProductDispensedCount().trim().equals(""))
                        productDispenseCount = Integer.parseInt(kvProductDetailsMOList.get(pos).getProductDispensedCount());
                    if (kvProductDetailsMOList.get(pos).getProductPurchaseCount() != null && !kvProductDetailsMOList.get(pos).getProductPurchaseCount().trim().equals(""))
                        productPurchaseCount = Integer.parseInt(kvProductDetailsMOList.get(pos).getProductPurchaseCount());

                    if (productDispenseCount == 0)
                        holder.kvReceiptStatusIV.setImageResource(R.drawable.red_cross);
                    else if (productDispenseCount == productPurchaseCount)
                        holder.kvReceiptStatusIV.setImageResource(R.drawable.green_tick_small);
                    else if (productDispenseCount != 0 && (productDispenseCount < productPurchaseCount))
                        holder.kvReceiptStatusIV.setImageResource(R.drawable.exclamation);

                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
            }

            if (pos % 2 == 0)
                holder.kvProductRowLayout.setBackgroundColor(context.getResources().getColor(R.color.listViewEvenColor));
            else
                holder.kvProductRowLayout.setBackgroundColor(context.getResources().getColor(R.color.listViewOddColor));

        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    @Override
    public int getItemCount() {
        return kvProductDetailsMOList.size();
    }

    public class KVReceiptViewHolder extends RecyclerView.ViewHolder {

        private TextView kvReceiptProductTV, kvReceiptQuantityTV, kvReceiptAmountTV;
        private ImageView kvReceiptStatusIV;
        private LinearLayout kvProductRowLayout;

        public KVReceiptViewHolder(View itemView) {
            super(itemView);
            kvReceiptProductTV = itemView.findViewById(R.id.kvReceiptProductTV);
            kvReceiptQuantityTV = itemView.findViewById(R.id.kvReceiptQuantityTV);
            kvReceiptAmountTV = itemView.findViewById(R.id.kvReceiptAmountTV);
            kvReceiptStatusIV = itemView.findViewById(R.id.kvReceiptStatusIV);
            kvProductRowLayout = itemView.findViewById(R.id.kvProductRowLayout);
        }
    }
}
