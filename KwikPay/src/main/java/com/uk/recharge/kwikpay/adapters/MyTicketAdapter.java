package com.uk.recharge.kwikpay.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

public class MyTicketAdapter extends BaseAdapter 
{
	Activity context;
	ArrayList<HashMap<String, String>> myTicketList;

	public MyTicketAdapter(Activity context,ArrayList<HashMap<String, String>> myTicketList) 
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		this.myTicketList = myTicketList;

	}

	public int getCount() {
		// TODO Auto-generated method stub
		return myTicketList.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder 
	{
		TextView tvDOT, tvTicketNo,tvServiceProviderMobNo,tvQueryType;

		ViewHolder(View view)
		{
			tvDOT = (TextView) view.findViewById(R.id.myticketDOT);
			tvTicketNo = (TextView) view.findViewById(R.id.myticketTicketNo);
			tvServiceProviderMobNo = (TextView) view.findViewById(R.id.myticketServiceProMobNo);
			tvQueryType = (TextView) view.findViewById(R.id.myticketQueryType);
		}

	}

	@SuppressWarnings("static-access")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View row=convertView;
		ViewHolder holder;

		if(row==null)
		{

			LayoutInflater inflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row=inflater.inflate(R.layout.singlemyticket_row, parent, false);

			holder=new ViewHolder(row);
			row.setTag(holder);

		}

		else
		{
			holder=(ViewHolder)row.getTag();
		}

		// Ashu... Setting Background color to list view at odd and even position
		if(position%2==0)
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewEvenColor));
		}
		else
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewOddColor));
		}

		holder.tvDOT.setText(datetimeSeparator(myTicketList.get(position).get(ProjectConstant.TXN_TRANSACTIONDATETIME)) );
		holder.tvTicketNo.setText(myTicketList.get(position).get(ProjectConstant.TICKET_TICKETNO));
		holder.tvServiceProviderMobNo.setText(myTicketList.get(position).get(ProjectConstant.TXN_OPERATORNAME)+"/"+"\n"+
				myTicketList.get(position).get(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
		holder.tvQueryType.setText(myTicketList.get(position).get(ProjectConstant.TICKET_QUERYTYPE));


		return row;

	}

	//METHOD FOR SPLITING DATE AND TIME WITH SPACE
	public String datetimeSeparator(String DOT)
	{
		String finalDOT="";
		if(DOT.contains(" "))
		{
			try
			{
				String[] dotArray = DOT.split(" ");
				finalDOT=dotArray[0]+"\n"+dotArray[1];
				return finalDOT;

			}catch(ArrayIndexOutOfBoundsException arrayIndexException){
				return finalDOT="";
			}
			catch(Exception exception)
			{
				return finalDOT="";
			}
		}
		else
		{
			return DOT;
		}
	}


}
