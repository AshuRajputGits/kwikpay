package com.uk.recharge.kwikpay.kwikvend;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.BillingAddressScreen;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.LoginScreen;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikvend.adapters.KVProductCartAdapter;
import com.uk.recharge.kwikpay.kwikvend.adapters.KVProductCartAdapter.UpdateTotalsListener;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductOthersDetailsMO;
import com.uk.recharge.kwikpay.kwikvend.payments.KVPaymentOptions;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

/**
 * Created by Ashu Rajput on 7/11/2018.
 */

public class KVProductCartScreen extends KPSuperClass implements UpdateTotalsListener {

    private KVProductCartAdapter kvProductCartAdapter = null;
    private TextView kvCartTotalPdtQty, kvCartTotalPdtAmount;
    private TextView processingFeeValue, totalAmountValue;
    private Button kvProductCartProceedButton;
    private TextView kvContinueShoppingTV;
    private String totalCartAmount = "0", totalProductAmount = "0", currencySign = "";
    private static final int BILLING_ADDRESS_REQUEST_CODE = 7;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        settingResourceIds();
        setHeaderScreenName("Your cart");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.kv_product_cart;
    }

    private void settingResourceIds() {
        kvCartTotalPdtQty = findViewById(R.id.kvCartTotalPdtQty);
        kvCartTotalPdtAmount = findViewById(R.id.kvCartTotalPdtAmount);
        processingFeeValue = findViewById(R.id.processingFeeValue);
        totalAmountValue = findViewById(R.id.totalAmountValue);
        kvProductCartProceedButton = findViewById(R.id.kvProductCartProceedButton);
        kvContinueShoppingTV = findViewById(R.id.kvContinueShoppingTV);

        currencySign = KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getCurrencySign();
        processingFeeValue.setText(currencySign + "0.2");

        RecyclerView kvProductCartRV = findViewById(R.id.kvProductCartRV);
        kvProductCartRV.setLayoutManager(new LinearLayoutManager(this));
        kvProductCartAdapter = new KVProductCartAdapter(this, currencySign);
        kvProductCartRV.setAdapter(kvProductCartAdapter);

        kvProductCartProceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getSharedPreferences("KP_Preference", MODE_PRIVATE).getString("KP_USER_ID", "").equals("0"))
                    startActivity(new Intent(KVProductCartScreen.this, LoginScreen.class)
                            .putExtra("IS_COMING_FROM_KV_SCREEN", true));
                else if (!getSharedPreferences("KP_Preference", MODE_PRIVATE).getBoolean("IS_KP_USER_POSTCODE_DEFINED", false)) {
                    Intent billingIntent = new Intent(KVProductCartScreen.this, BillingAddressScreen.class);
                    startActivityForResult(billingIntent, BILLING_ADDRESS_REQUEST_CODE);
                } else
                    proceedForPaymentProcess();
            }
        });

        kvContinueShoppingTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KVProductCartScreen.this.finish();
            }
        });
    }

    @Override
    public void getTotalQuantityAmount(int productQty, float productTotalAmount) {
        try {
            kvCartTotalPdtQty.setText("" + productQty);
            kvCartTotalPdtAmount.setText(Utility.fromHtml(currencySign) + "" + productTotalAmount);
            totalProductAmount = String.valueOf(productTotalAmount);

            //Cart Amount = Total Pdt amount + Processing Amount
            totalCartAmount = String.format("%.02f", productTotalAmount + 0.2);
            totalAmountValue.setText(Utility.fromHtml(currencySign) + "" + totalCartAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showMessageToUI(String message) {
        ARCustomToast.showToast(KVProductCartScreen.this, message, Toast.LENGTH_LONG);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        proceedForPaymentProcess();
    }

    private void proceedForPaymentProcess() {
        if (kvProductCartAdapter != null)
            kvProductCartAdapter.setIfModifiedProductsFromCartToSingleton();

        //Getting and setting the "Others" product details info into model
        KVProductOthersDetailsMO receivedModel = KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO();
        receivedModel.setTotalProductAmount(totalProductAmount);
        receivedModel.setTotalCartAmount(totalCartAmount);
        KVProductDetailsSingleton.getInstance().setKvProductOthersDetailsMO(receivedModel);
        startActivity(new Intent(KVProductCartScreen.this, KVPaymentOptions.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BILLING_ADDRESS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    getSharedPreferences("KP_Preference", MODE_PRIVATE).edit().putBoolean("IS_KP_USER_POSTCODE_DEFINED", true).apply();
                    proceedForPaymentProcess();
                }
            }
        }
    }
}
