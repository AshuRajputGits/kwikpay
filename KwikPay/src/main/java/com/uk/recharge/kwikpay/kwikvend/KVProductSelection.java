package com.uk.recharge.kwikpay.kwikvend;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ar.library.ARCustomToast;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikvend.adapters.KVProductAdapter;
import com.uk.recharge.kwikpay.kwikvend.adapters.KVProductAdapter.ProductSelectionListener;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductDetailsMO;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductOthersDetailsMO;
import com.uk.recharge.kwikpay.kwikvend.payments.KVReceiptScreen;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;
import com.vendekin.vendekinlib.CallBack;
import com.vendekin.vendekinlib.VendekinSdk;
import com.vendekin.vendekinlib.models.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by Ashu Rajput on 7/10/2018.
 */

public class KVProductSelection extends KPSuperClass implements ProductSelectionListener, CallBack {

    private ArrayList<KVProductDetailsMO> list = null;
    private KVProductAdapter kvProductAdapter = null;
    private RecyclerView kvProductRV;
    private Button kvProductProceedButton;
    private VendekinSdk vendekinSdk = null;
    private String[] receivedSeqArray;
    private int productDispenseCounter = 0;

    //----------------Dispense Screen Views --------------------//
    private RelativeLayout productDispenseParentLayout;
    private TextView dispenseProductName;
    private ImageView dispenseProductImage;
    private ArrayList<KVProductDetailsMO> singleDispenseProductList = null;
    private boolean isDispensingStart = false;
    private GifImageView dispenseGIFImage;
    private ArrayList<String> dispensedProductStatusList = null;
    private String kvOrderTransactionId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createMenuDrawer(this);
        setHeaderScreenName("Select your item");

        initViewIds();
        initializeVendSDK();

        kvProductProceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (kvProductAdapter != null) {
                    ArrayList<KVProductDetailsMO> modifiedProductList = kvProductAdapter.getSelectedProductsFromRV();
                    ArrayList<KVProductDetailsMO> selectedProductList = new ArrayList<>();
                    if (modifiedProductList != null && modifiedProductList.size() > 0) {

                        for (KVProductDetailsMO models : modifiedProductList) {
                            if (models.getProductSelected()) {
                                selectedProductList.add(models);
                            }
                        }
                        if (selectedProductList != null && selectedProductList.size() > 0) {
                            KVProductDetailsSingleton.getInstance().clearKVProductList();
                            KVProductDetailsSingleton.getInstance().setKvProductDetailsMOList(selectedProductList);
                            startActivity(new Intent(KVProductSelection.this, KVProductCartScreen.class));
                        } else {
                            ARCustomToast.showToast(KVProductSelection.this, "Please select at least one product");
                        }
                    }
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.kv_product_selection;
    }

    private void initViewIds() {

        kvProductRV = findViewById(R.id.kvProductRV);
        productDispenseParentLayout = findViewById(R.id.productDispenseParentLayout);
        kvProductProceedButton = findViewById(R.id.kvProductProceedButton);
        dispenseProductName = findViewById(R.id.dispenseProductName);
        dispenseProductImage = findViewById(R.id.dispenseProductImage);
        dispenseGIFImage = findViewById(R.id.dispenseGIFImage);
    }

    private void initializeVendSDK() {

        //-------Clearing previous selected product from list--------//
        KVProductDetailsSingleton.getInstance().clearKVProductList();
        //-----------------------------------------------------------//

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this, "Loading items,please wait...");

        try {
            vendekinSdk = new VendekinSdk(this, KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getMerchantId(),
                    KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getQrCode(), false, this);
            vendekinSdk.start();
        } catch (Exception e) {
            KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
            Crashlytics.logException(e);
        }
    }

    @Override
    public void onMaxProductSelected() {
        ARCustomToast.showToast(KVProductSelection.this, "You have selected the maximum allowed vend capacity");
    }

    @Override
    public void onInfoButtonClicked(int position) {
        try {
            DialogFragment dialog = new KVProductDetailDialog();
            dialog.show(getSupportFragmentManager(), "KVProductDialog");
            Bundle productBundle = new Bundle();
            productBundle.putString("ProdName", list.get(position).getProductName());
            productBundle.putFloat("ProdPrice", list.get(position).getProductPrice());
            productBundle.putString("ProdImageURL", list.get(position).getProductURL());
            dialog.setArguments(productBundle);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    //-----------------------Vendekin SDK Callback---------------------------------//
    @Override
    public void onFailure(String errorMessage) {
        KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

        Log.e("VendKinLogs", "Failure -- " + errorMessage);
        ARCustomToast.showToast(this, errorMessage);
        this.finish();
    }

    @Override
    public void onProductList(String productList, String countryName, String currencyName, String currencySign) {

//        Log.e("VendKinLogs", "Success -- " + productList);
        KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
        if (productList != null) {
            try {
                JSONObject parentObject = new JSONObject(productList);
//                String status = parentObject.optString("status");

                KVProductOthersDetailsMO receivedModel = KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO();
                receivedModel.setMachineType(parentObject.optString("machine_type"));
                receivedModel.setMaxVendCapacity(parentObject.optString("max_vend_capacity"));
                receivedModel.setCountryName(countryName);
                receivedModel.setCurrencyName(currencyName);
                receivedModel.setCurrencySign(currencySign);

                KVProductDetailsSingleton.getInstance().setKvProductOthersDetailsMO(receivedModel);

                if (parentObject.has("products")) {
                    JSONArray jsonArray = parentObject.getJSONArray("products");
                    if (jsonArray != null && jsonArray.length() > 0) {
                        list = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            KVProductDetailsMO model = new KVProductDetailsMO();
                            model.setProductId(jsonArray.getJSONObject(i).optString("product_id"));
                            model.setProductName(jsonArray.getJSONObject(i).optString("brand_name"));

                            if (!jsonArray.getJSONObject(i).optString("unit_price").isEmpty())
                                model.setProductPrice(Float.parseFloat(jsonArray.getJSONObject(i).optString("unit_price")));

                            if (!jsonArray.getJSONObject(i).optString("available_quantity").isEmpty())
                                model.setProductAvailableQuantity(Integer.parseInt(jsonArray.getJSONObject(i).optString("available_quantity")));

                            model.setProductCorePrice(jsonArray.getJSONObject(i).optString("core_price"));
                            model.setProductSize(jsonArray.getJSONObject(i).optString("size"));
                            model.setProductSubBrand(jsonArray.getJSONObject(i).optString("subbrand_name"));
                            model.setProductURL(jsonArray.getJSONObject(i).optString("image"));
                            model.setProductDescription("Description " + i);

                            //Adding additional entities for implementing local logic:
                            model.setProductSelected(false);
                            model.setProductQuantity(1);

                            list.add(model);
                        }
                    }
                }

                kvProductRV.setLayoutManager(new GridLayoutManager(this, 2));
                kvProductAdapter = new KVProductAdapter(this, list);
                kvProductRV.setAdapter(kvProductAdapter);
            } catch (Exception e) {
                e.printStackTrace();
                Crashlytics.logException(e);
            }
        }
    }

    @Override
    public void onSequenceString(String sequenceNo) {
        Log.e("VendKinLogs", "KP Receiving onSeqString -- " + sequenceNo);
        if (sequenceNo != null && !sequenceNo.isEmpty()) {
            KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
            productDispenseParentLayout.setVisibility(View.VISIBLE);
            try {
                //----------Init status list array---------//
                dispensedProductStatusList = new ArrayList<>();
                //-----------------------------------------//

                Gson gson = new Gson();
                receivedSeqArray = gson.fromJson(sequenceNo, String[].class);
                productDispenseProcessor();
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }

    @Override
    public void onDispenseStatus(String dispenseResponse) {
        Log.e("VendKinLogs", "onDispenseStatus -- " + dispenseResponse + " Count: " + productDispenseCounter);
        dispensedProductStatusList.add(dispenseResponse);

        /*if (productDispenseCounter < receivedSeqArray.length) {
            nextProductDispenseDialog();
        } else {
            Log.e("VendKinLogs", "Dispense Counter = ALL DONE Transaction-------->>>");
            enableHomeAndMenuIcon();
            dispenseProductName.setText("Dispensed.\nThank you for your business");
            dispenseProductImage.setVisibility(View.INVISIBLE);
            dispenseGIFImage.setVisibility(View.INVISIBLE);

            vendekinSdk.disconnect();
            callKVReceiptScreen();
        }*/

        if (productDispenseCounter < receivedSeqArray.length)
            Log.e("VendKinLogs", "Wait!! Product is there to dispense");
        else
            voilaAllProductDispensed();

    }

    @Override
    public void onNextVend(String nextStringValue) {
        Log.e("VendKinLogs", "nextStringValue -- " + nextStringValue);
        try {
            if (productDispenseCounter < receivedSeqArray.length) {
                nextProductDispenseDialog();
            } else {
//                voilaAllProductDispensed();
            }

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    @Override
    public void onMachineErrorMessage(String errorMessage) {
//        KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
        Log.e("VendKinLogs", "Failure -- " + errorMessage);
        ARCustomToast.showToast(this, errorMessage);
    }
    //-----------------------Vendekin SDK Callback[END]----------------------------//

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent != null) {
            Bundle receivedBundle = intent.getExtras();
            if (receivedBundle != null) {
                if (receivedBundle.containsKey("WelcomeBackPageAfterPayment")) {
                    if (vendekinSdk != null) {

                        disableHomeAndMenuIcon();
                        isDispensingStart = true;
                        setHeaderScreenName("Dispense");

                        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this, "Processing items,please wait...");
                        kvProductRV.setVisibility(View.GONE);
                        kvProductProceedButton.setVisibility(View.GONE);
                        try {
                            if (vendekinSdk != null) {
                                kvOrderTransactionId = receivedBundle.getString("PG_APP_TXN_ID_KEY", "");
                                vendekinSdk.setOrderData(createListProduct(), kvOrderTransactionId,
                                        KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getTotalProductAmount(),
                                        KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getTotalCartAmount(),
                                        getSharedPreferences("KP_Preference", MODE_PRIVATE).getString("KP_USER_ID", ""),
                                        "0");
                            } else
                                ARCustomToast.showToast(KVProductSelection.this, "Machine SDK not initialized, Please try again");

                        } catch (Exception e) {
                            KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                            Crashlytics.logException(e);
                        }
                    }
                }
            }
        }
    }

    /*
    1. Creating Product List(KV SDK MO)-> To send in setOrderData()
    2. Creating list of single product list
    */
    private List<Product> createListProduct() {
        List<Product> productList = null;
        try {
            productList = new ArrayList<>();
            singleDispenseProductList = new ArrayList<>();
            ArrayList<KVProductDetailsMO> productDetailList = KVProductDetailsSingleton.getInstance().getKvProductDetailsMOList();
            if (productDetailList != null && productDetailList.size() > 0) {
                for (KVProductDetailsMO models : productDetailList) {
                    Product product = new Product();
                    product.setAddedCount(models.getProductQuantity());
                    product.setAmount(String.valueOf(models.getProductPrice()));
                    product.setCore_price(models.getProductCorePrice());
                    product.setProductId(models.getProductId());
                    product.setProduct_qty(models.getProductQuantity());
                    productList.add(product);

                    if (models.getProductQuantity() > 1) {
                        for (int i = 0; i < models.getProductQuantity(); i++) {
                            KVProductDetailsMO singleMO = new KVProductDetailsMO();
                            singleMO.setProductURL(models.getProductURL());
                            singleMO.setProductName(models.getProductName());
                            singleDispenseProductList.add(singleMO);
                        }
                    } else {
                        KVProductDetailsMO singleMO = new KVProductDetailsMO();
                        singleMO.setProductURL(models.getProductURL());
                        singleMO.setProductName(models.getProductName());
                        singleDispenseProductList.add(singleMO);
                    }
                }
            }

            Log.e("VendKinLogs", "Product SIngle List size " + singleDispenseProductList.size());

        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        return productList;
    }

    private void productDispenseProcessor() {

        Log.e("VendKinLogs", "Dispense Counter = " + productDispenseCounter + ", Total Length" + receivedSeqArray.length);

        if (productDispenseCounter < receivedSeqArray.length) {
            try {
                productDispenseCounter = productDispenseCounter + 1;

                //Setting product name and product image w.r.t URL
                try {
                    if (KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getMachineType() == "2")
                        dispenseProductName.setText("Dispensing  " + singleDispenseProductList.get(productDispenseCounter - 1).getProductName()
                                + " Please make sure you keep cup at the dispensing area.");
                    else
                        dispenseProductName.setText("Your " + singleDispenseProductList.get(productDispenseCounter - 1).getProductName() + " is being dispensed");

                    Glide.with(this).load(singleDispenseProductList.get(productDispenseCounter - 1).getProductURL()).into(dispenseProductImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                vendekinSdk.dispense(receivedSeqArray[productDispenseCounter - 1]);
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }

    private void nextProductDispenseDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        if (KVProductDetailsSingleton.getInstance().getKvProductOthersDetailsMO().getMachineType() == "2")
            dialog.setMessage("Replace the cup for your next product and click below");
        else
            dialog.setMessage("Dispense next product");

        dialog.setPositiveButton("Next", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    dialog.dismiss();
                    dispenseGIFImage.setVisibility(View.VISIBLE);
                    productDispenseProcessor();
                } catch (Exception e) {
                }
            }
        });

        AlertDialog alert = dialog.create();
        alert.getWindow().setGravity(Gravity.BOTTOM);
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.show();
    }

    private void callKVReceiptScreen() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(KVProductSelection.this, KVReceiptScreen.class);
                intent.putExtra("DISPENSE_STATUS_LIST", dispensedProductStatusList);
                intent.putExtra("KV_ORDER_TXN_ID", kvOrderTransactionId);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }

    private void voilaAllProductDispensed() {
        Log.e("VendKinLogs", "Dispense Counter = ALL DONE Transaction-------->>>");
        enableHomeAndMenuIcon();
        dispenseProductName.setText("Dispensed.\nThank you for your business");
        dispenseProductImage.setVisibility(View.INVISIBLE);
        dispenseGIFImage.setVisibility(View.INVISIBLE);
        vendekinSdk.disconnect();
        callKVReceiptScreen();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (vendekinSdk != null) {
                vendekinSdk.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    @Override
    public void onBackPressed() {
        if (!isDispensingStart) {
            super.onBackPressed();
        }
    }
}
