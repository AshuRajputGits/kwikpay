package com.uk.recharge.kwikpay.myaccount.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.myaccount.MyTransactionSingleton;
import com.uk.recharge.kwikpay.myaccount.MyTransactionViewDetails;
import com.uk.recharge.kwikpay.myaccount.MyTransactionsMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 23-12-2017.
 */

public class EVChargeTransactionAdapter extends RecyclerView.Adapter<EVChargeTransactionAdapter.EVTransactionViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<MyTransactionsMO> evTransactionList = null;
    private Context context = null;

    public EVChargeTransactionAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        evTransactionList = MyTransactionSingleton.getInstance().getKvChargeTransactionList();
        this.context = context;
    }

    @Override
    public EVTransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.my_transactions_row, parent, false);
        return new EVTransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EVTransactionViewHolder holder, int position) {

        holder.transactionRCAmount.setText(Utility.fromHtml(evTransactionList.get(position).getTransactionDisplayCurrencyCode())
                + evTransactionList.get(position).getTransactionDisplayAmount());
        if (evTransactionList.get(position).getTransactionPaymentStatus().equalsIgnoreCase("SUCCESSFUL")) {
            holder.transactionRechargeStatus.setText(evTransactionList.get(position).getTransactionRechargeStatus());
        } else {
            holder.transactionRechargeStatus.setText(evTransactionList.get(position).getTransactionPaymentStatus());
            holder.transactionRechargeStatus.setTextColor(Color.RED);
        }

        holder.transactionRechargeType.setText(evTransactionList.get(position).getTransactionOperatorName());
        holder.transactionDateTime.setText(evTransactionList.get(position).getTransactionDateTime());
        holder.transactionOrderID.setText("Order No " + evTransactionList.get(position).getTransactionOrderId());

        try {
            Glide.with(context).load(evTransactionList.get(position).getImageUrl())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(holder.operatorImages);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (evTransactionList != null && evTransactionList.size() > 0)
            return evTransactionList.size();
        return 0;
    }

    public class EVTransactionViewHolder extends RecyclerView.ViewHolder {

        TextView transactionRCAmount;
        TextView transactionRechargeStatus;
        TextView transactionRechargeType;
        TextView transactionDateTime;
        TextView transactionOrderID;
        ImageView operatorImages;

        public EVTransactionViewHolder(View itemView) {
            super(itemView);
            transactionRCAmount = itemView.findViewById(R.id.transactionRCAmount);
            transactionRechargeStatus = itemView.findViewById(R.id.transactionRechargeStatus);
            transactionRechargeType = itemView.findViewById(R.id.transactionRechargeType);
            transactionDateTime = itemView.findViewById(R.id.transactionDateTime);
            transactionOrderID = itemView.findViewById(R.id.transactionOrderID);
            operatorImages = itemView.findViewById(R.id.operatorImages);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, MyTransactionViewDetails.class)
                            .putExtra("SELECTED_TRANSACTION", evTransactionList.get(getLayoutPosition())));
                }
            });
        }
    }

}
