package com.uk.recharge.kwikpay.kwikcharge.models;

/**
 * Created by Ashu Rajput on 1/20/2018.
 */

public class ConnectorListMO {

    private String connectorType;
    private String connectorStatus;
    private String localConnectorNumber;
    private String connectorLogoURL;
    private String connectorName;
    private String connectorDescription;

    public String getConnectorType() {
        return connectorType;
    }

    public void setConnectorType(String connectorType) {
        this.connectorType = connectorType;
    }

    public String getConnectorStatus() {
        return connectorStatus;
    }

    public void setConnectorStatus(String connectorStatus) {
        this.connectorStatus = connectorStatus;
    }

    public String getLocalConnectorNumber() {
        return localConnectorNumber;
    }

    public void setLocalConnectorNumber(String localConnectorNumber) {
        this.localConnectorNumber = localConnectorNumber;
    }

    public String getConnectorLogoURL() {
        return connectorLogoURL;
    }

    public void setConnectorLogoURL(String connectorLogoURL) {
        this.connectorLogoURL = connectorLogoURL;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getConnectorDescription() {
        return connectorDescription;
    }

    public void setConnectorDescription(String connectorDescription) {
        this.connectorDescription = connectorDescription;
    }
}
