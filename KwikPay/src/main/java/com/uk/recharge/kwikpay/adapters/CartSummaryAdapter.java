package com.uk.recharge.kwikpay.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uk.recharge.kwikpay.R;

public class CartSummaryAdapter extends BaseAdapter 
{
	Activity context;
	ArrayList<HashMap<String, String>> cartSummaryArrayList;

	public CartSummaryAdapter(Activity context,ArrayList<HashMap<String, String>> cartSummaryArrayList) 
	{
		// TODO Auto-generated constructor stub
		this.context = context;
		this.cartSummaryArrayList = cartSummaryArrayList;

	}

	public int getCount() {
		// TODO Auto-generated method stub
		return cartSummaryArrayList.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder 
	{
		TextView serviceProviderTV, mobNoTV,topUpTV,totAmtTV;

		ViewHolder(View view)
		{
			serviceProviderTV = (TextView) view.findViewById(R.id.cartSummary_ServProviderTV);
			mobNoTV = (TextView) view.findViewById(R.id.cartSummary_MobNoTV);
			topUpTV = (TextView) view.findViewById(R.id.cartSummary_TopupTV);
			totAmtTV = (TextView) view.findViewById(R.id.cartSummary_TotAmtTV);
		}

	}

	@SuppressWarnings("static-access")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View row=convertView;
		ViewHolder holder;

		if(row==null)
		{
			LayoutInflater inflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row=inflater.inflate(R.layout.single_summary_row, parent, false);
			holder=new ViewHolder(row);
			row.setTag(holder);
		}

		else
		{
			holder=(ViewHolder)row.getTag();
		}

		// Ashu... Setting Background color to list view at odd and even position
		if(position%2==0)
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewEvenColor));
		}
		else
		{
			row.setBackgroundColor(context.getResources().getColor(R.color.listViewOddColor));
		}

		holder.serviceProviderTV.setText(cartSummaryArrayList.get(position).get("SERVICE_PROVIDER"));
		holder.mobNoTV.setText(cartSummaryArrayList.get(position).get("MOB_NUMBER"));
		holder.topUpTV.setText(cartSummaryArrayList.get(position).get("TOPUP"));
		holder.totAmtTV.setText(cartSummaryArrayList.get(position).get("TOTAL_AMOUNT"));

		return row;

	}


}
