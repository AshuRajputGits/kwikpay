package com.uk.recharge.kwikpay.gamingarcade;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.validation.FieldValidation;
import com.ar.library.validation.FieldValidationWithMessage;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.BillingAddressScreen;
import com.uk.recharge.kwikpay.CommonStaticPages;
import com.uk.recharge.kwikpay.EmailVerificationScreen;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.gamingarcade.models.GameMachineInfoMO;
import com.uk.recharge.kwikpay.gamingarcade.payments.GamingPaymentOptions;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.GamingInfoSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONObject;

import java.text.DecimalFormat;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 7/7/2018.
 */

public class GamingOrderDetails extends KPSuperClass {

    private SharedPreferences preference;
    private LinearLayout addressLayout, sendAsGiftLayout;
    private ImageView txnDetailsOperatorLogo;
    private TextView serviceProviderValue, mobNumberValue, topUpOptionValue,
            transactionFeeValue, totAmtPayableValue, couponDiscountAmount;
    private ImageView reedemCouponGoTV;
    private EditText sendGiftEmailAddress, sendGiftMobileNumber, sendGiftRecipientName;
    //    private ImageView txnDetailsRCStatusIcon;
//    private TextView orderDetailPlanDescription;
//    private String operatorProductCode = "";
    //    private TextView orderDetailCountryOrPinTV;
    private EditText couponBoxET1, couponBoxET2, couponBoxET3;
    private CheckBox couponCheckBox, tncCheckBox, sendGiftCheckBox;
    //    private TextView headerTitle;
    private LinearLayout reedemCouponBoxesLL;
    private boolean isTermConditionAccepted = false;
    protected boolean isAddressFound = false;
    private String directPaymentScreenFlag = "";

    private static final int EMAIL_VERIFICATION_REQUEST_CODE = 20;
    private static final int BILLING_ADDRESS_REQUEST_CODE = 30;
    private static final int TERM_CONDITION_CODE = 40;
    private static final int EDIT_BILLING_ADDRESS_REQUEST_CODE = 45;

    private GameMachineInfoMO gameMachineInfoMO = null;
    private int productPosition = 0;
    private LinearLayout gamingCreditAmountLayout;
    private TextView gamingCreditAmount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createMenuDrawer(this);
        setHeaderScreenName("Your Order");

        gameMachineInfoMO = GamingInfoSingleton.getInstance().getGameMachineInfoMO();

        if (gameMachineInfoMO != null)
            productPosition = gameMachineInfoMO.getSelectedPosition();

        settingIds();
        updateOrderDetailsUI();
        callConfirmAddressAPI();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.order_details_card_view;
    }

    private void settingIds() {
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        addressLayout = findViewById(R.id.addressLayout);
        sendAsGiftLayout = findViewById(R.id.sendAsGiftLayout);
        serviceProviderValue = findViewById(R.id.orderDetail_ServiceProviderTV);
        mobNumberValue = findViewById(R.id.orderDetail_MobNumberTV);
        topUpOptionValue = findViewById(R.id.orderDetail_TopUpOptionTV);
        transactionFeeValue = findViewById(R.id.orderDetail_TransFeeTV);
        totAmtPayableValue = findViewById(R.id.orderDetail_TotAmountTV);
        couponDiscountAmount = findViewById(R.id.orderDetail1_CouponAmountTV);
        gamingCreditAmountLayout = findViewById(R.id.gamingCreditAmountLayout);
        gamingCreditAmount = findViewById(R.id.gamingCreditAmount);
        reedemCouponBoxesLL = findViewById(R.id.reedemCouponBoxesLayouts);
        //        addressPostalCodeTV = findViewById(R.id.addressPostalCodeTV);
//        withoutCouponLayout = findViewById(R.id.orderDetail_WithoutCoupon);
//        orderDetailCountryOrPinTV = findViewById(R.id.orderDetailCountryOrPinTV);
//        txnDetailsRCStatusIcon = findViewById(R.id.txnDetailsRCStatusIcon);
//        orderDetailPlanDescription = findViewById(R.id.orderDetailPlanDescription);

        couponBoxET1 = findViewById(R.id.redeemCouponCheckBoxET1);
        couponBoxET2 = findViewById(R.id.redeemCouponCheckBoxET2);
        couponBoxET3 = findViewById(R.id.redeemCouponCheckBoxET3);

        reedemCouponGoTV = findViewById(R.id.reedemCouponGoTV);
        couponCheckBox = findViewById(R.id.redeemCouponCheckBox);
        sendGiftCheckBox = findViewById(R.id.sendGiftCheckBox);
        tncCheckBox = findViewById(R.id.tncCheckBox);
        txnDetailsOperatorLogo = findViewById(R.id.txnDetailsOperatorLogo);
        sendGiftEmailAddress = findViewById(R.id.sendGiftEmailAddress);
        sendGiftRecipientName = findViewById(R.id.sendGiftRecipientName);
        sendGiftMobileNumber = findViewById(R.id.sendGiftMobileNumber);

        findViewById(R.id.tncMessageWebView).setOnClickListener(viewClickListener);
        findViewById(R.id.addressEditButton).setOnClickListener(viewClickListener);
        findViewById(R.id.orderDetailCheckOutBtn).setOnClickListener(viewClickListener);
        findViewById(R.id.headerBackButton).setOnClickListener(viewClickListener);
        reedemCouponGoTV.setOnClickListener(viewClickListener);

        couponCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    reedemCouponBoxesLL.setVisibility(View.VISIBLE);
                } else {
                    reedemCouponBoxesLL.setVisibility(View.GONE);
                    couponBoxET1.setText("");
                    couponBoxET2.setText("");
                    couponBoxET3.setText("");
                }
            }
        });

        sendGiftCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    FieldValidationWithMessage.removeAllErrorIcons(sendGiftEmailAddress);
                    FieldValidationWithMessage.removeAllErrorIcons(sendGiftMobileNumber);
                    sendGiftEmailAddress.setVisibility(View.VISIBLE);
                    sendGiftMobileNumber.setVisibility(View.VISIBLE);
                    sendGiftRecipientName.setVisibility(View.VISIBLE);
                } else {
                    sendGiftEmailAddress.setVisibility(View.GONE);
                    sendGiftMobileNumber.setVisibility(View.GONE);
                    sendGiftRecipientName.setVisibility(View.GONE);
                    sendGiftEmailAddress.setText("");
                    sendGiftMobileNumber.setText("");
                    sendGiftRecipientName.setText("");
                }
            }
        });
    }

    private void updateOrderDetailsUI() {
        try {
            addressLayout.setVisibility(View.GONE);
            sendAsGiftLayout.setVisibility(View.GONE);
            serviceProviderValue.setText(gameMachineInfoMO.getMachineName());
            String machinePlanAmount = gameMachineInfoMO.getPricingOptionsMOList().get(productPosition).getPlanAmount();
            topUpOptionValue.setText(Utility.fromHtml("\u00a3") + machinePlanAmount);
            gamingCreditAmountLayout.setVisibility(View.VISIBLE);

            gamingCreditAmount.setText(gameMachineInfoMO.getPricingOptionsMOList().get(productPosition).getPlanCredit());
            String machineServiceFee = gameMachineInfoMO.getPricingOptionsMOList().get(productPosition).getServiceFee();
            transactionFeeValue.setText(Utility.fromHtml("\u00a3") + machineServiceFee);
            couponDiscountAmount.setText(Utility.fromHtml("\u00a3") + "0.00");

            if (machinePlanAmount != null && !machinePlanAmount.isEmpty() && machineServiceFee != null && !machineServiceFee.isEmpty()) {
                try {
                    Double totalAmount = Double.parseDouble(machinePlanAmount) + Double.parseDouble(machineServiceFee);
                    DecimalFormat decimalFormat = new DecimalFormat("#.##");
                    totAmtPayableValue.setText(Utility.fromHtml("\u00a3") + decimalFormat.format(totalAmount));
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.logException(e);
                }
            }

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.reedemCouponGoTV) {
                final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (!FieldValidation.couponFieldIsValid(couponBoxET1.getText().toString(), couponBoxET1, couponBoxET1.getText().toString().length(), 1)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    ARCustomToast.showToast(GamingOrderDetails.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else if (!FieldValidation.couponFieldIsValid(couponBoxET2.getText().toString(), couponBoxET2, couponBoxET2.getText().toString().length(), 2)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    ARCustomToast.showToast(GamingOrderDetails.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else if (!FieldValidation.couponFieldIsValid(couponBoxET3.getText().toString(), couponBoxET3, couponBoxET3.getText().toString().length(), 3)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    ARCustomToast.showToast(GamingOrderDetails.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
//                    apiHitPosition = 4;
//                    new LoadResponseViaPost(GamingOrderDetails.this, formRedeemCouponJSON(), true).execute("");
                }
            } else if (id == R.id.tncCheckBox) {
                if (tncCheckBox.isChecked())
                    isTermConditionAccepted = true;
                else
                    isTermConditionAccepted = false;

            } else if (id == R.id.addressEditButton) {
                startActivityForResult(new Intent(GamingOrderDetails.this, BillingAddressScreen.class),
                        EDIT_BILLING_ADDRESS_REQUEST_CODE);
            } else if (id == R.id.tncMessageWebView) {
                startActivityForResult(new Intent(GamingOrderDetails.this, CommonStaticPages.class)
                        .putExtra("STATIC_PAGE_NAME", "TNC_PAGE"), TERM_CONDITION_CODE);
            } else if (id == R.id.headerBackButton) {
                finish();
            } else if (id == R.id.orderDetailCheckOutBtn) {
                confirmPaymentProcess();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BILLING_ADDRESS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    isAddressFound = true;
                    preference.edit().putBoolean("IS_KP_USER_POSTCODE_DEFINED", true).apply();
                    initiatePaymentProcess();
                }
            }
        } else if (requestCode == EMAIL_VERIFICATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    finalCallForPaymentProcess();
                }
            }
        }
    }

    public void confirmPaymentProcess() {
        if (sendGiftCheckBox.isChecked() && sendGiftRecipientName.getText().toString().trim().isEmpty()) {
            ARCustomToast.showToast(this, "please enter a valid name", Toast.LENGTH_LONG);
        } else if (sendGiftCheckBox.isChecked() && sendGiftEmailAddress.getText().toString().trim().isEmpty()) {
            ARCustomToast.showToast(this, "please enter a valid email address", Toast.LENGTH_LONG);
        } else if (sendGiftCheckBox.isChecked() && !FieldValidation.emailAddressIsValid(sendGiftEmailAddress.getText().toString(), sendGiftEmailAddress)) {
            ARCustomToast.showToast(this, "please enter a valid email address", Toast.LENGTH_LONG);
        } else if (sendGiftCheckBox.isChecked() && sendGiftMobileNumber.getText().toString().isEmpty()) {
            ARCustomToast.showToast(this, "please enter a valid mobile number", Toast.LENGTH_LONG);
        } else if (!tncCheckBox.isChecked() && !isTermConditionAccepted) {
            ARCustomToast.showToast(this, "please accept the terms & condition", Toast.LENGTH_LONG);
        } else {
            // IS USER HAD ALREADY VERIFIED THEIR EMAIL ID-- Uncomment once testing is done, comment billing intent above
            if (preference.getBoolean("IS_KP_USER_EMAIL_VERIFIED", false) || preference.getBoolean("EMAIL_IS_VERIFIED", false)) {
                finalCallForPaymentProcess();
            } else
                startActivityForResult(new Intent(GamingOrderDetails.this, EmailVerificationScreen.class), EMAIL_VERIFICATION_REQUEST_CODE);
        }
    }

    private void finalCallForPaymentProcess() {
        if (directPaymentScreenFlag.equals("1")) {
            initiatePaymentProcess();
        } else if (directPaymentScreenFlag.equals("0")) {
//            if (isAddressFound)
            if (preference.getBoolean("IS_KP_USER_POSTCODE_DEFINED", false))
                initiatePaymentProcess();
            else {
                Intent billingIntent = new Intent(GamingOrderDetails.this, BillingAddressScreen.class);
                startActivityForResult(billingIntent, BILLING_ADDRESS_REQUEST_CODE);
            }
        } else
            initiatePaymentProcess();
    }

    private void initiatePaymentProcess() {
        startActivity(new Intent(this, GamingPaymentOptions.class));
    }

    private void callConfirmAddressAPI() {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "KP_GETADDRESS_BY_CONFIRMDETAIL");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("OPERATORCODE", "GAMMING");
            json.put("SERVICEID", "7");
            json.put("COUNTRYCODE", "");
            json.put("PAYMENTAMT", "");
            json.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("SUBSCRIBERNUM", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJson.has("DIRECT_PAYMENT_SCREEN")) {
                                directPaymentScreenFlag = childJson.getString("DIRECT_PAYMENT_SCREEN");

                                if (childJson.has("IS_POSTCODE_DEFINED") && !childJson.getString("IS_POSTCODE_DEFINED").isEmpty())
                                    preference.edit().putBoolean("IS_KP_USER_POSTCODE_DEFINED", Boolean.parseBoolean(childJson.getString("IS_POSTCODE_DEFINED"))).apply();
                                if (childJson.has("IS_EMAIL_VERIFIED") && !childJson.getString("IS_EMAIL_VERIFIED").isEmpty())
                                    preference.edit().putBoolean("IS_KP_USER_EMAIL_VERIFIED", Boolean.parseBoolean(childJson.getString("IS_EMAIL_VERIFIED"))).apply();

                                if (directPaymentScreenFlag.equals("0")) {
                                    // IF FLAG=1: ADDRESS FOUND IN THE D.B.
                                    // IF FLAG=0: ADDRESS NOT FOUND IN THE D.B.
                                    if (childJson.optString("ADDRESSRESPONSECODE").equals("1")) {
                                        isAddressFound = true;
                                    }
                                }
                            } else {
                                ARCustomToast.showToast(GamingOrderDetails.this, childJson.optString("DESCRIPTION"));
                            }
                        }

                    } catch (Exception e) {
                        ARCustomToast.showToast(GamingOrderDetails.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(GamingOrderDetails.this))
                        ARCustomToast.showToast(GamingOrderDetails.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(GamingOrderDetails.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                ARCustomToast.showToast(GamingOrderDetails.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

}
