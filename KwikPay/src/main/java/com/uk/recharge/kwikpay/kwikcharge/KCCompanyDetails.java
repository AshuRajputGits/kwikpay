package com.uk.recharge.kwikpay.kwikcharge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.CommonStaticPages;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.kwikcharge.models.RegistrationNumbersMO;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;
import com.uk.recharge.kwikpay.singleton.RegNoDetailsSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 9/18/2017.
 */

public class KCCompanyDetails extends AppCompatActivity implements AsyncRequestListenerViaPost {

    private TextView kcCompanyName, kcCompanyUserName;
    private SharedPreferences preference;
    private JSONArray regNoJsonArray;
    private CheckBox kcTNCCheckBox;
    private static final int TERM_CONDITION_CODE = 3;
    private boolean isTermConditionAccepted = false;
    private ImageView kcOperatorLogoIV;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kwik_charge_company_details);

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        kcCompanyName = findViewById(R.id.kcCompanyName);
        kcCompanyUserName = findViewById(R.id.kcCompanyUserName);
        kcTNCCheckBox = findViewById(R.id.kcTNCCheckBox);
        kcOperatorLogoIV = findViewById(R.id.kcOperatorLogoIV);

        /*Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("QR_RESULT")) {
                kcCompanyName.setText(receivedBundle.getString("QR_RESULT"));
            }
        }*/

        findViewById(R.id.companyDetailsProceedButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!kcTNCCheckBox.isChecked() && !isTermConditionAccepted) {
                    ARCustomToast.showToast(KCCompanyDetails.this, "please accept the terms & condition", Toast.LENGTH_LONG);
                } else {
                    if (preference.getString("KP_USER_ID", "0").equals("0")) {
                        startActivity(new Intent(KCCompanyDetails.this, ElectricBlueAcNoScreen.class));
                    } else {
                        new LoadResponseViaPost(KCCompanyDetails.this, formRegNumberJSON(), true).execute("");
                    }
                }
            }
        });

        findViewById(R.id.kcTNCCheckBox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (kcTNCCheckBox.isChecked()) {
                    startActivityForResult(new Intent(KCCompanyDetails.this, CommonStaticPages.class)
                            .putExtra("STATIC_PAGE_NAME", "EB_TNC_PAGE"),TERM_CONDITION_CODE);
//                    startActivityForResult(new Intent(KCCompanyDetails.this, CommonStaticPages.class).putExtra("STATIC_PAGE_NAME", "TNC_PAGE"), TERM_CONDITION_CODE);
                } else
                    isTermConditionAccepted = false;
            }
        });

        updateKCCompanyDetailsUI();
    }

    private void updateKCCompanyDetailsUI() {
        LocationIdMO locationNumModel = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();
        if (locationNumModel != null) {
            try {
                ProjectConstant.tncURL = locationNumModel.getProviderTnCUrl();
                String operatorLogoURL = getResources().getString(R.string.operatorLogoBaseUrl) + locationNumModel.getOperatorLogo() + ".png";
                Glide.with(this)
                        .load(operatorLogoURL)
                        .placeholder(R.drawable.default_operator_logo)
                        .into(kcOperatorLogoIV);

                String companyDescription = "<small><small>" + locationNumModel.getCompanyDescription() + "</small></small>";
                kcCompanyName.setText(locationNumModel.getOperatorName() + "\n" + Utility.fromHtml(companyDescription));

                String tncMessage = "I accept the <i><u>terms &amp; conditions</u></i> of " + locationNumModel.getOperatorName();
                kcTNCCheckBox.setText(Utility.fromHtml(tncMessage));

                if (!preference.getString("KP_USER_NAME", "").equals("")) {
                    kcCompanyUserName.setText("Welcome " + preference.getString("KP_USER_NAME", "") + "!");
                    kcCompanyUserName.setVisibility(View.VISIBLE);
                    kcTNCCheckBox.setChecked(true);
                }
            } catch (Exception e) {
            }
        }
    }

    //CREATING JSON PARAMETERS FOR REGISTERED Location NUMBER
    public String formRegNumberJSON() {
        JSONObject jsonobj = new JSONObject();
        try {
            String receivedLocationNumber = "";
            try {
                receivedLocationNumber = KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getLocationNumber();
            } catch (Exception e) {
            }
            jsonobj.put("APISERVICE", "KP_REG_NUM_BY_USERID");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("OP_CODE", "ELEC");
            jsonobj.put("SERVICEID", "4");
            jsonobj.put("LOCATION_NUMBER", receivedLocationNumber);
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
//            jsonobj.put("USERID", "979");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void onRequestComplete(String loadedString) {

        Log.e("OnRequest", "Response " + loadedString);
        if (!loadedString.equals("") && !loadedString.equals("Exception")) {
            parseRegisteredNumberJson(loadedString);
        } else {
            ARCustomToast.showToast(KCCompanyDetails.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    public boolean parseRegisteredNumberJson(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    LocationIdMO receivedLocationDetailsMO = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();

                    receivedLocationDetailsMO.setOperatorCode(childJsonObj.optString("OP_CODE"));
                    receivedLocationDetailsMO.setServiceId(childJsonObj.optString("SERVICEID"));
                    receivedLocationDetailsMO.setAC(childJsonObj.optString("AC"));
                    receivedLocationDetailsMO.setDC(childJsonObj.optString("DC"));
                    receivedLocationDetailsMO.setCHADEMO(childJsonObj.optString("CHADEMO"));

                    receivedLocationDetailsMO.setConnector1Desc(childJsonObj.optString("CONNECTOR_1_DESC"));
                    receivedLocationDetailsMO.setConnector2Desc(childJsonObj.optString("CONNECTOR_2_DESC"));
                    receivedLocationDetailsMO.setConnector3Desc(childJsonObj.optString("CONNECTOR_3_DESC"));

                    receivedLocationDetailsMO.setAddressOne(childJsonObj.optString("ADDRESS_ONE"));
                    receivedLocationDetailsMO.setAddressTwo(childJsonObj.optString("ADDRESS_TWO"));
                    receivedLocationDetailsMO.setAddressThree(childJsonObj.optString("ADDRESS_THREE"));
                    receivedLocationDetailsMO.setCountry(childJsonObj.optString("COUNTRY"));
                    receivedLocationDetailsMO.setCity(childJsonObj.optString("CITY"));
                    receivedLocationDetailsMO.setPostCode(childJsonObj.optString("POSTCODE"));
                    receivedLocationDetailsMO.setUserPostCode(childJsonObj.optString("USER_POSTCODE"));
                    receivedLocationDetailsMO.setRegistrationNumber(childJsonObj.optString("REGISTRATION_NUMBER"));
                    receivedLocationDetailsMO.setRecentMake(childJsonObj.optString("RECENT_MAKE"));
                    receivedLocationDetailsMO.setRecentModel(childJsonObj.optString("RECENT_MODEL"));
                    receivedLocationDetailsMO.setRecentYear(childJsonObj.optString("RECENT_YEAR"));
                    receivedLocationDetailsMO.setRecentBatteryType(childJsonObj.optString("RECENT_BATTERY_TYPE"));
                    receivedLocationDetailsMO.setRecentChargerType(childJsonObj.optString("RECENT_CHARGER_TYPE"));
                    receivedLocationDetailsMO.setRecentMaxAmount(childJsonObj.optString("RECENT_MAX_AMOUNT"));
                    receivedLocationDetailsMO.setRecentMaxTime(childJsonObj.optString("RECENT_MAX_TIME"));
                    receivedLocationDetailsMO.setRecentRate(childJsonObj.optString("RECENT_RATE"));

                    KwikChargeDetailsSingleton.getInstance().setLocationIdMO(receivedLocationDetailsMO);

                    if (childJsonObj.has("REG_NUMS")) {
                        Object object = childJsonObj.get("REG_NUMS");

                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            regNoJsonArray = new JSONArray();
                            regNoJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray) {
                            regNoJsonArray = (JSONArray) object;
                        }

                        if (regNoJsonArray != null && regNoJsonArray.length() > 0) {

                            ArrayList<RegistrationNumbersMO> regNoArrayList = new ArrayList<>(regNoJsonArray.length());

                            for (byte b = 0; b < regNoJsonArray.length(); b++) {

                                RegistrationNumbersMO regNoModel = new RegistrationNumbersMO();
                                regNoModel.setRegistrationNo(regNoJsonArray.getJSONObject(b).optString("REG_NUM"));
                                regNoModel.setMake(regNoJsonArray.getJSONObject(b).optString("MAKE"));
                                regNoModel.setModel(regNoJsonArray.getJSONObject(b).optString("MODEL"));
                                regNoModel.setChargerType(regNoJsonArray.getJSONObject(b).optString("CHARGER_TYPE"));
                                regNoModel.setBatteryType(regNoJsonArray.getJSONObject(b).optString("BATTERY_TYPE"));
                                regNoModel.setYear(regNoJsonArray.getJSONObject(b).optString("YEAR"));
                                regNoModel.setMaxAmount(regNoJsonArray.getJSONObject(b).optString("MAX_AMOUNT"));
                                regNoModel.setMaxTime(regNoJsonArray.getJSONObject(b).optString("MAX_TIME"));
                                regNoModel.setMaxRate(regNoJsonArray.getJSONObject(b).optString("RATE"));

                                regNoArrayList.add(regNoModel);
                            }
                            RegNoDetailsSingleton.getInstance().setRegNoList(regNoArrayList);
                        }
                    }

                    //SETTING VALUE IN PREFERENCE IF(TNC CHECKED BY USER)
                    getSharedPreferences("KP_Preference", MODE_PRIVATE).edit().putBoolean("IS_KC_TNC_CHECKED", true).apply();

                    startActivity(new Intent(KCCompanyDetails.this, KwikChargeOrderScreen.class));
                    this.finish();
                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        startActivity(new Intent(KCCompanyDetails.this, VehicleDetailScreen.class));
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TERM_CONDITION_CODE) {
            if (resultCode == RESULT_OK)
                isTermConditionAccepted = true;
            else
                isTermConditionAccepted = false;
        }
    }

}
