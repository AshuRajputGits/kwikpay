package com.uk.recharge.kwikpay.kwikcharge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.uk.recharge.kwikpay.LoginScreen;
import com.uk.recharge.kwikpay.R;

/**
 * Created by Ashu Rajput on 9/26/2017.
 */

public class ElectricBlueAcNoScreen extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.electric_blue_account_no);

        findViewById(R.id.payAsYouGoButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                redirectToLoginOrOrderScreen(1);
                startActivityForResult(new Intent(ElectricBlueAcNoScreen.this, LoginScreen.class)
                        .putExtra("COMING_FROM_PAY_AS_GO", true), 4);
            }
        });
        findViewById(R.id.signInElectricUserTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                redirectToLoginOrOrderScreen(2);
                startActivityForResult(new Intent(ElectricBlueAcNoScreen.this, LoginScreen.class)
                        .putExtra("IS_COMING_FROM_ELEC_BLUE", true), 4);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4) {
            if (resultCode == Activity.RESULT_OK) {
                startActivity(new Intent(ElectricBlueAcNoScreen.this, KwikChargeOrderScreen.class));
                finish();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                startActivity(new Intent(ElectricBlueAcNoScreen.this, VehicleDetailScreen.class));
                finish();
            }
        }
    }

    /*private void redirectToLoginOrOrderScreen(int type) {

        if (preference.getString("KP_USER_ID", "0").equals("0")) {
            if (type == 1) {
                startActivity(new Intent(ElectricBlueAcNoScreen.this, LoginScreen.class).putExtra("COMING_FROM_PAY_AS_GO", true));
            } else if (type == 2) {
                startActivity(new Intent(ElectricBlueAcNoScreen.this, LoginScreen.class).putExtra("IS_COMING_FROM_ELEC_BLUE", true));
            }
        } else {
            new LoadResponseViaPost(ElectricBlueAcNoScreen.this, formRegNumberJSON(), true).execute("");
        }
    }*/
}
