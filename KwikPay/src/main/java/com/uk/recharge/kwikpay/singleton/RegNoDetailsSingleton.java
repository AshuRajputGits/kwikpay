package com.uk.recharge.kwikpay.singleton;

import com.uk.recharge.kwikpay.kwikcharge.models.RegistrationNumbersMO;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 10/8/2017.
 */

public class RegNoDetailsSingleton {
    private static RegNoDetailsSingleton ourInstance = null;
    private ArrayList<RegistrationNumbersMO> regNoList = null;

    public static RegNoDetailsSingleton getInstance() {
        if (ourInstance == null)
            ourInstance = new RegNoDetailsSingleton();
        return ourInstance;
    }

    private RegNoDetailsSingleton() {
    }

    public ArrayList<RegistrationNumbersMO> getRegNoList() {
        return regNoList;
    }

    public void setRegNoList(ArrayList<RegistrationNumbersMO> regNoList) {
        this.regNoList = regNoList;
    }

    private void clearRegNumberListData() {
        if (regNoList != null && regNoList.size() > 0) {
            regNoList.clear();
        }
    }
}
