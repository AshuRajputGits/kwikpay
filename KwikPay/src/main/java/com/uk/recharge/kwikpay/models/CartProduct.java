package com.uk.recharge.kwikpay.models;

import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;

import java.util.HashMap;
import java.util.List;

public class CartProduct {

    private String id;
    private String category;
    private String sub_category;
    private String brand_name;
    private String sub_brand_name;
    private String category_action;
    private String size;
    private String product_id;
    private String product_description;
    private String image;
    private String core_price;
    private String available_quantity;
    private String unit_price;
    private Integer selected_quantity;
    private Float total_price;
    private List<Ingredient> ingredientList;
    private HashMap<String, List<Integer>> viewCheckedPerIngredientList = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public HashMap<String, List<Integer>> getViewCheckedPerIngredientList() {
        return viewCheckedPerIngredientList;
    }

    public void setViewCheckedPerIngredientList(HashMap<String, List<Integer>> viewCheckedPerIngredientList) {
        this.viewCheckedPerIngredientList = viewCheckedPerIngredientList;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getSub_brand_name() {
        return sub_brand_name;
    }

    public void setSub_brand_name(String sub_brand_name) {
        this.sub_brand_name = sub_brand_name;
    }

    public String getCategory_action() {
        return category_action;
    }

    public void setCategory_action(String category_action) {
        this.category_action = category_action;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCore_price() {
        return core_price;
    }

    public void setCore_price(String core_price) {
        this.core_price = core_price;
    }

    public String getAvailable_quantity() {
        return available_quantity;
    }

    public void setAvailable_quantity(String available_quantity) {
        this.available_quantity = available_quantity;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public Integer getSelected_quantity() {
        return selected_quantity;
    }

    public void setSelected_quantity(Integer selected_quantity) {
        this.selected_quantity = selected_quantity;
    }

    public Float getTotal_price() {
        return total_price;
    }

    public void setTotal_price(Float total_price) {
        this.total_price = total_price;
    }
}
