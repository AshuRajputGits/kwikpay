package com.uk.recharge.kwikpay;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidationWithMessage;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.uk.recharge.kwikpay.utilities.CustomDialogBox;

public class RaiseTicket extends Activity implements OnClickListener, AsyncRequestListenerViaPost
{
	private Spinner chooseProblemSpinner;
//	private TextView backBtnTV;
//	private LinearLayout backBtnTV;
//	private ,headerFavBtn,headerProfileBtn,headerChangePWBtn,headerTicketBtn,cartItemCounter;
//	View separatorLine;
	private TextView labelSubsID,labelOrderId,labelAmount,labelDOT,labelTOP,labelComment,labelOperator;
	private String[] problemsArray ={"Select any One","Payment Deducted/Topup Fail","Payment Deducted/Session Timeout","Confirmed but no Topup received"};
	private String[] problemsArrayId;
	private ArrayAdapter<String> chooseProblemAdapter;
	private SharedPreferences preferences;
	private Button backBtnTV;
	//String raiseTicketName ="";
	private EditText raiseTkt_SubscriberID, raiseTkt_OrderID, raiseTkt_Amount, raiseTkt_Date_Of_Txn, raiseTkt_Comment,
	raiseTkt_Operator;
	private int flag =0;
	private String receivedOrderId="",receivedServiceProvider="",receivedDOT="";
	
	private ImageView problemSpinnerError;
	private boolean controlNetOffExit=false;
//	private String[] subjectsArray={"Failed Recharge","Feedback/Query"};
//	private LinearLayout myTransitionLinearLayout;
//	private Spinner chooseSubjectSpinner;
//	private ArrayAdapter<String> chooseSubjectAdapter;
//	private TextView labelChooseSub,labelName,labelMobNumber,labelEmailId;
//	private EditText raiseTkt_Name, raiseTkt_MobNo,raiseTkt_EmailID;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.raise_ticket);
		settingIDs();

		Bundle receivedBundle=getIntent().getExtras();
		if(receivedBundle!=null)
		{
			if(receivedBundle.containsKey("RAISETICKET_ORDERID") && receivedBundle.containsKey("RAISETICKET_SERVICE_PROVIDER")
					&& receivedBundle.containsKey("RAISETICKET_DOT"))
			{
				receivedOrderId=receivedBundle.getString("RAISETICKET_ORDERID");
				receivedServiceProvider=receivedBundle.getString("RAISETICKET_SERVICE_PROVIDER");
				receivedDOT=receivedBundle.getString("RAISETICKET_DOT");
				
				//SETTING THE VALUE OF OPERATOR AND MAKING IT DISABLED 
				raiseTkt_Operator.setEnabled(false);
				raiseTkt_Operator.setText(receivedServiceProvider);
			}
		}


		// SETTING ADAPTER AND DATA TO THE CHOOSE REASON SPINNER
		/*chooseSubjectAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textsize,subjectsArray);
		chooseSubjectAdapter.setDropDownViewResource(R.layout.single_row_spinner);
		chooseSubjectSpinner.setAdapter(chooseSubjectAdapter);
		chooseSubjectSpinner.setOnItemSelectedListener(this);*/

		flag=1;
		new LoadResponseViaPost(RaiseTicket.this, formJSONParam() , true).execute("");

		//RESTRICTING ' IN COMMENT FIELD
		//		FieldValidationWithMessage.blockMyCharacter(raiseTkt_Comment, "@!#^()/:+;-_");

		//SETTING SCREEN NAMES TO APPS FLYER
		try {
			EventsLoggerSingleton.getInstance().setCommonEventLogs(this,getResources().getString(R.string.SCREEN_NAME_RAISE_TICKET));
	/*		String eventName = getResources().getString(R.string.SCREEN_NAME_RAISE_TICKET);
			AppsFlyerLib.trackEvent(this, eventName, null);
			EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
			((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public String formJSONParam() 
	{
		JSONObject jsonobj = new JSONObject();

		try 
		{
			jsonobj.put("APISERVICE", "KPRAISETICKETAPP");
			jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
			jsonobj.put("DEVICEOS", "Android");
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("ORDERID", receivedOrderId);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();

	}

	private void settingIDs() 
	{
//		chooseSubjectSpinner =(Spinner)findViewById(R.id.raiseticket_ChooseSubSpinner);
//		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);
//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);
		
//		myTransitionLinearLayout=(LinearLayout)findViewById(R.id.myTransitionHeaderLayout);
//		myTransitionLinearLayout.setVisibility(View.VISIBLE);

		preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);	
		chooseProblemSpinner = (Spinner)findViewById(R.id.raiseticket_TypeOfProblemSpinner);
//		backBtnTV=(TextView)findViewById(R.id.raiseTicket_BackButtonTV);
		backBtnTV=(Button)findViewById(R.id.raiseTicket_BackButtonTV);
		labelSubsID=(TextView)findViewById(R.id.raiseticket_SubsIdLabel);
		labelOrderId=(TextView)findViewById(R.id.raiseticket_OrderIdLabel);
		labelAmount=(TextView)findViewById(R.id.raiseticket_AmountLabel);
		labelOperator=(TextView)findViewById(R.id.raiseticket_OperatorLabel);
		labelDOT=(TextView)findViewById(R.id.raiseticket_DOTLabel);
		labelTOP=(TextView)findViewById(R.id.raiseticket_TOPLabel);
		labelComment=(TextView)findViewById(R.id.raiseticket_CommentLabel);

/*		labelChooseSub=(TextView)findViewById(R.id.raiseticket_ChooseSubjectLabel);
		labelName=(TextView)findViewById(R.id.raiseticket_NameLabel);
		labelMobNumber=(TextView)findViewById(R.id.raiseticket_MobNumbLabel);
		labelEmailId=(TextView)findViewById(R.id.raiseticket_EmailIdLabel);

		labelChooseSub.setText(Html.fromHtml("Choose Subject"+"<font color='#ff0000'>*"));
		labelName.setText(Html.fromHtml("Name"+"<font color='#ff0000'>*"));
		labelMobNumber.setText(Html.fromHtml("Mobile Number"+"<font color='#ff0000'>*"));
		labelEmailId.setText(Html.fromHtml("Email Id"+"<font color='#ff0000'>*"));*/
		
//		labelTOP.setText(Html.fromHtml("Type Of Problem"+"<font color='#ff0000'>*"));
		
		labelSubsID.setText(Html.fromHtml("Mobile Number/Subscriber Id"+"<font color='#ff0000'>*"));
		labelOrderId.setText(Html.fromHtml("Order Id"+"<font color='#ff0000'>*"));
		labelAmount.setText(Html.fromHtml("Amount"+"<font color='#ff0000'>*"));
		labelOperator.setText(Html.fromHtml("Operator"+"<font color='#ff0000'>*"));
		labelDOT.setText(Html.fromHtml("Date Of Transaction"+"<font color='#ff0000'>*"));
		labelTOP.setText(Html.fromHtml("Type of issue"+"<font color='#ff0000'>*"));
		labelComment.setText(Html.fromHtml("Comment/Feedback"+"<font color='#ff0000'>*"));

		// IDs of all four buttons
//		headerFavBtn=(TextView)findViewById(R.id.header_FavouriteBtn);
//		headerProfileBtn=(TextView)findViewById(R.id.header_MyProfileBtn);
//		headerChangePWBtn=(TextView)findViewById(R.id.header_ChangePWBtn);
//		headerTicketBtn=(TextView)findViewById(R.id.header_MyTicketBtn);


		// IDs of all Edit Texts
		/*raiseTkt_Name =(EditText)findViewById(R.id.raiseticket_name);
		raiseTkt_MobNo =(EditText)findViewById(R.id.raiseticket_MobNo);
		raiseTkt_EmailID =(EditText)findViewById(R.id.raiseticket_mailID);*/

		raiseTkt_SubscriberID =(EditText)findViewById(R.id.raiseticket_MobNoSubscriberID);
		raiseTkt_OrderID =(EditText)findViewById(R.id.raiseticket_OrderId);
		raiseTkt_Amount =(EditText)findViewById(R.id.raiseticket_Amount);
		raiseTkt_Operator =(EditText)findViewById(R.id.raiseticket_OperatorValue);
		raiseTkt_Date_Of_Txn =(EditText)findViewById(R.id.raiseticket_DOT);
		raiseTkt_Comment =(EditText)findViewById(R.id.raiseticket_Comments);

		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);/
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(RaiseTicket.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();
		
		
		problemSpinnerError =(ImageView)findViewById(R.id.raiseticket_TOPErrorLabel);
		
		
		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		backBtnTV.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RaiseTicket.this.finish();
			}
		});


//		headerFavBtn.setOnClickListener(this);
//		headerProfileBtn.setOnClickListener(this);
//		headerChangePWBtn.setOnClickListener(this);
//		headerTicketBtn.setOnClickListener(this);

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME

		/*DBQueryMethods database=new DBQueryMethods(RaiseTicket.this);
		try
		{
			database.open();
			TextView headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
			headerBelow_ClassTV.setText(database.getSubHeaderTitles("Raise ticket"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}*/
	}

	/*@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) 
	{
		// TODO Auto-generated method stub

		switch(position)
		{
		case 0:
			break;

		case 1:
			Intent raiseTicketFeedback = new Intent(RaiseTicket.this,ContactUs.class);
			raiseTicketFeedback.putExtra("RAISE_TICKET_SCREEN", "RAISE_TICKET_SCREEN");
			raiseTicketFeedback.putExtra("RAISE_TICKET_NAME", raiseTkt_Name.getText().toString());
			raiseTicketFeedback.putExtra("RAISE_TICKET_MOBILE_NUMBER", raiseTkt_MobNo.getText().toString());
			raiseTicketFeedback.putExtra("RAISE_TICKET_EMAIL_ID", raiseTkt_EmailID.getText().toString());
			startActivity(raiseTicketFeedback);

			break;
		default:
			break;

		}

	}*/

	public String formRaiseTicket() 
	{
		JSONObject jsonobj = new JSONObject();
		long queryID=0;
		if(chooseProblemSpinner.getSelectedItemId() != 0)
		{
			queryID = chooseProblemSpinner.getSelectedItemId();
		}
		try 
		{
			String trimmedComment = raiseTkt_Comment.getText().toString().replaceAll(" +"," ").trim();
			jsonobj.put("APISERVICE", "KPSAVERAISETICKET");
			jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
			jsonobj.put("DEVICEOS", "Android");
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("COMMENT",trimmedComment);
			jsonobj.put("QUERYTYPEID", problemsArrayId[((int)queryID)-1]);
			jsonobj.put("MOBILENUMBER",raiseTkt_SubscriberID.getText().toString());
			jsonobj.put("SERVICEPROVIDER",receivedServiceProvider);
			jsonobj.put("DATEOFTRANSACTION", raiseTkt_Date_Of_Txn.getText().toString());
			jsonobj.put("ORDERID", raiseTkt_OrderID.getText().toString());
			jsonobj.put("AMOUNT",raiseTkt_Amount.getText().toString());
			jsonobj.put("TYPEOFPROBLEM", chooseProblemSpinner.getSelectedItem().toString());
			jsonobj.put("EMAIL", "");
			jsonobj.put("NAME", "");
			jsonobj.put("CONTACTMOBILENUMBER","");


			/*jsonobj.put("EMAIL", raiseTkt_EmailID.getText().toString());
			jsonobj.put("NAME", raiseTkt_Name.getText().toString());
			jsonobj.put("CONTACTMOBILENUMBER",raiseTkt_MobNo.getText().toString());*/
			
			//			Log.e("QUERY PROBLEM ID", problemsArrayId[((int)queryID)-1]); 

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();

	}

	public void raiseTicketSubmitButton(View raiseTicketView)
	{
		if(RaiseTicketValidation())
		{
			flag=2;
			controlNetOffExit=true;
			new LoadResponseViaPost(RaiseTicket.this, formRaiseTicket() , true).execute("");
			
		}
	}


	public boolean RaiseTicketValidation()
	{
		/*if(!FieldValidationWithMessage.nameIsValid(raiseTkt_Name.getText().toString(), raiseTkt_Name).equals("SUCCESS"))
		{
			problemSpinnerError.setImageResource(0);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_MobNo);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_SubscriberID);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_EmailID);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Comment);

			ARCustomToast.showToast(RaiseTicket.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}*/

		/*else if(!FieldValidationWithMessage.mobNumberIsValid(raiseTkt_MobNo.getText().toString(), raiseTkt_MobNo).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Name);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_SubscriberID);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_EmailID);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Comment);

			ARCustomToast.showToast(RaiseTicket.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}*/
		/*else if(!FieldValidationWithMessage.mobNumberIsValid(raiseTkt_SubscriberID.getText().toString(), raiseTkt_SubscriberID).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Name);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_MobNo);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_EmailID);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Comment);

			ARCustomToast.showToast(RaiseTicket.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}*/
		/*else if(!FieldValidation.emailAddressIsValid(raiseTkt_EmailID.getText().toString(), raiseTkt_EmailID))
		{
			problemSpinnerError.setImageResource(0);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Name);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_MobNo);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_SubscriberID);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Comment);


			ARCustomToast.showToast(RaiseTicket.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}*/
		
		 if(chooseProblemSpinner.getSelectedItemId() == 0)
		{
			problemSpinnerError.setImageResource(R.drawable.error_img);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_SubscriberID);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Comment);
/*			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Name);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_MobNo);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_EmailID);*/
			ARCustomToast.showToast(RaiseTicket.this, "please select query type", Toast.LENGTH_LONG);
			return false;
		}

		else if(!FieldValidationWithMessage.commentIsValid(raiseTkt_Comment.getText().toString(),raiseTkt_Comment).equals("SUCCESS"))
		{
			problemSpinnerError.setImageResource(0);
/*			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_SubscriberID);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_Name);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_MobNo);
			FieldValidationWithMessage.removeAllErrorIcons(raiseTkt_EmailID);*/

			ARCustomToast.showToast(RaiseTicket.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}
		else
		{
			return true;
		}

	}

	@Override
	public void onClick(View v) 
	{
		// TODO Auto-generated method stub
		/*if(v.getId()==R.id.header_FavouriteBtn)
		{
			startActivity(new Intent(RaiseTicket.this,FavouriteScreen.class));
		}
		if(v.getId()==R.id.header_MyProfileBtn)
		{
			startActivity(new Intent(RaiseTicket.this,EditProfile.class));
		}
		if(v.getId()==R.id.header_ChangePWBtn)
		{
			startActivity(new Intent(RaiseTicket.this,ChangePassword.class));
		}
		if(v.getId()==R.id.header_MyTicketBtn)
		{
			startActivity(new Intent(RaiseTicket.this,MyTicket.class));
		}
*/
	}

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub

//		Log.e("RaiseTicketResponse", loadedString);

		if(loadedString!=null && !loadedString.equals(""))
		{
			if(!loadedString.equals("Exception"))
			{
				if(flag==1)
				{
					try 
					{
						JSONObject raise_ticket_json = new JSONObject(loadedString);
						JSONObject raise_ticket_json_response = raise_ticket_json.getJSONObject(ProjectConstant.RESPONSE_TAG);

						if(raise_ticket_json_response.has(ProjectConstant.API_RESPONSE_CODE))
						{
							if(raise_ticket_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
							{
								/*if(raise_ticket_json_response.has("FNAME") && raise_ticket_json_response.has("MNAME") && raise_ticket_json_response.has("LNAME") )
								{
									if(raise_ticket_json_response.getString("MNAME").equals(""))
									{
										raiseTkt_Name.setText(
												raise_ticket_json_response.getString("FNAME")+ " " +
														raise_ticket_json_response.getString("LNAME"));
									}
									else
									{
										raiseTkt_Name.setText(
												raise_ticket_json_response.getString("FNAME")+ " " +
														raise_ticket_json_response.getString("MNAME")+ " " +
														raise_ticket_json_response.getString("LNAME"));


									}

									if(!raiseTkt_Name.equals(""))
									{
										raiseTkt_Name.setEnabled(false);

									}

								}*/

								/*if(raise_ticket_json_response.has("MOBILENUMBER"))
								{
									raiseTkt_MobNo.setText(raise_ticket_json_response.getString("MOBILENUMBER"));

									if(!raiseTkt_MobNo.equals(""))
									{
										raiseTkt_MobNo.setEnabled(false);
									}

								}*/

								if(raise_ticket_json_response.has("SUBSCRIPTIONNUMBER"))
								{
									raiseTkt_SubscriberID.setText(raise_ticket_json_response.getString("SUBSCRIPTIONNUMBER"));
									if(!raiseTkt_SubscriberID.equals(""))
										raiseTkt_SubscriberID.setEnabled(false);
								}

								if(raise_ticket_json_response.has("ORDERID"))
								{
									raiseTkt_OrderID.setText(raise_ticket_json_response.getString("ORDERID"));
									if(!raiseTkt_OrderID.equals(""))
										raiseTkt_OrderID.setEnabled(false);
								}

								if(raise_ticket_json_response.has("AMOUNT"))
								{
									raiseTkt_Amount.setText(raise_ticket_json_response.getString("AMOUNT"));
									if(!raiseTkt_Amount.equals(""))
									{
										raiseTkt_Amount.setEnabled(false);
									}
								}

								/*if(raise_ticket_json_response.has("EMAIL"))
								{
									raiseTkt_EmailID.setText(raise_ticket_json_response.getString("EMAIL"));
									if(!raiseTkt_EmailID.equals(""))
										raiseTkt_EmailID.setEnabled(false);
								}*/

								if(raise_ticket_json_response.has("DATEOFTRANSACTION"))
								{
									raiseTkt_Date_Of_Txn.setText(receivedDOT);

									if(!raiseTkt_Date_Of_Txn.equals(""))
										raiseTkt_Date_Of_Txn.setEnabled(false);
								}

								if(raise_ticket_json_response.has("CONTACTUSREASON"))
								{
									JSONArray json_arr = raise_ticket_json_response.getJSONArray("CONTACTUSREASON");
									problemsArrayId = new String[json_arr.length()];
									for(int i = 0; i < json_arr.length(); i++)
									{
										if(json_arr.getJSONObject(i).has("REASONNAME") && json_arr.getJSONObject(i).has("REASONID") )
										{
											problemsArray[i+1] = json_arr.getJSONObject(i).getString("REASONNAME");
											problemsArrayId[i] = json_arr.getJSONObject(i).getString("REASONID");
										}
									}

									chooseProblemAdapter = new ArrayAdapter<String>(this, R.layout.spinner_textsize,problemsArray);
									chooseProblemAdapter.setDropDownViewResource(R.layout.single_row_spinner);
									chooseProblemSpinner.setAdapter(chooseProblemAdapter);

								}
							}
							else
							{
								if(raise_ticket_json_response.has("DESCRIPTION"))
								{
									ARCustomToast.showToast(RaiseTicket.this,raise_ticket_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
								}

							}
						}

					}	

					catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}


				if(flag ==2)
				{
					try 
					{
						JSONObject raise_ticket_json = new JSONObject(loadedString);
						JSONObject raise_ticket_json_response = raise_ticket_json.getJSONObject(ProjectConstant.RESPONSE_TAG);

						if(raise_ticket_json_response.has(ProjectConstant.API_RESPONSE_CODE))
						{
							if(raise_ticket_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
							{
								if(raise_ticket_json_response.has(ProjectConstant.TICKET_ID))
								{
									Intent i = new Intent(RaiseTicket.this,CustomDialogBox.class);
									i.putExtra("MSG_KEY", raise_ticket_json_response.getString("DESCRIPTION"));
									startActivity(i);
									RaiseTicket.this.finish();

								}
							}
							else
							{
								if(raise_ticket_json_response.has("DESCRIPTION"))
								{
									ARCustomToast.showToast(RaiseTicket.this,raise_ticket_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
								}
							}
						}
					}
					catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
			else
			{
				if(!controlNetOffExit)
				RaiseTicket.this.finish();
				ARCustomToast.showToast(RaiseTicket.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
			}

		}
		else
		{
			if(!controlNetOffExit)
			RaiseTicket.this.finish();
			ARCustomToast.showToast(RaiseTicket.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}

	}

	/*@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}*/
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preferences!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preferences.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}
		
		}
	}*/
	
}
