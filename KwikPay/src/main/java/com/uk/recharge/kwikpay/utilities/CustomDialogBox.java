package com.uk.recharge.kwikpay.utilities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.R;

public class CustomDialogBox extends Activity
{
	Context context;
	TextView message,backButtonTV;
	String msgToShow;
	Button backButton;
	LinearLayout escalateLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.common_dialog_box);
		
		settingIds();
		
		Bundle bundle=getIntent().getExtras();
		if(bundle!=null)
		{
			if(bundle.containsKey("MSG_KEY"))
			{
				message.setText(bundle.getString("MSG_KEY"));
			}
			
			if(bundle.containsKey("KP_SCREEN_IDENTIFIER"))
			{
				if(bundle.getString("KP_SCREEN_IDENTIFIER").equals("FORGOTPW"))
				{
					backButton.setVisibility(View.VISIBLE);
				}
				else if(bundle.getString("KP_SCREEN_IDENTIFIER").equals("MYTICKET_ESCALATE"))
				{
					escalateLayout.setVisibility(View.VISIBLE);
					backButton.setVisibility(View.GONE);
				}
			}
			
		}
		
		backButtonTV.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CustomDialogBox.this.finish();
			}
		});


	}

	private void settingIds() {
		// TODO Auto-generated method stub
		
		message=(TextView)findViewById(R.id.dialogMsgTV);
		backButton=(Button)findViewById(R.id.dialogBoxBackBtn);
		escalateLayout=(LinearLayout)findViewById(R.id.escalate_Layout);
		backButtonTV=(TextView)findViewById(R.id.escalate_BackButtonTV);
		
//		View separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);
		
		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(CustomDialogBox.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;
	}

	public void backButton(View backBtnView)
	{
//		ProjectConstant.LOGIN_FORGOT_PW_FLAG=1;
		CustomDialogBox.this.finish();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	
}
