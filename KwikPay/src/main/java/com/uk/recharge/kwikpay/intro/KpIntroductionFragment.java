package com.uk.recharge.kwikpay.intro;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.uk.recharge.kwikpay.R;

public class KpIntroductionFragment extends Fragment {

	private static final String PAGE = "page";
	private int mPage;

	public static KpIntroductionFragment newInstance(int page) {
		KpIntroductionFragment frag = new KpIntroductionFragment();
		Bundle b = new Bundle();
		b.putInt(PAGE, page);
		frag.setArguments(b);
		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (!getArguments().containsKey(PAGE))
			throw new RuntimeException("Fragment must contain a \"" + PAGE + "\" argument!");
		mPage = getArguments().getInt(PAGE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = getActivity().getLayoutInflater().inflate(R.layout.introduction_fragment, container, false);
		view.setTag(mPage);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

//		View background = view.findViewById(R.id.introduction_fragment_layout);
		
		ImageView bgIV=(ImageView)view.findViewById(R.id.introduction_screen_bg);
		
		Drawable drawable=bgIV.getDrawable();
		if(drawable!=null)
			((BitmapDrawable) drawable).getBitmap().recycle();
		
		switch (mPage) 
		{
		case 0:
//			background.setBackgroundResource(R.drawable.introscreen1);
			bgIV.setImageBitmap(decode(bgIV.getContext().getResources(), R.drawable.introscreen1));
			break;
		case 1:
//			background.setBackgroundResource(R.drawable.introscreen2);
			bgIV.setImageBitmap(decode(bgIV.getContext().getResources(), R.drawable.introscreen2));
			break;
		case 2:
//			background.setBackgroundResource(R.drawable.introscreen3);
			bgIV.setImageBitmap(decode(bgIV.getContext().getResources(), R.drawable.introscreen3));
			break;
		case 3:
//			background.setBackgroundResource(R.drawable.introscreen4);
			bgIV.setImageBitmap(decode(bgIV.getContext().getResources(), R.drawable.introscreen4));
			break;
		case 4:
//			background.setBackgroundResource(R.drawable.introscreen5);
			bgIV.setImageBitmap(decode(bgIV.getContext().getResources(), R.drawable.introscreen5));
			break;
		default:
		}

	}
	

	    @SuppressWarnings("deprecation")
		public  Bitmap decode(Resources res, int resId){
	        BitmapFactory.Options bfOptions=new BitmapFactory.Options();
	        bfOptions.inDither = false;
	        bfOptions.inPurgeable = true;               
	        bfOptions.inInputShareable = true;
	        bfOptions.inTempStorage = new byte[32 * 1024]; 
	        return BitmapFactory.decodeResource(res, resId, bfOptions);
	}

}