package com.uk.recharge.kwikpay.gamingarcade.models;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 7/6/2018.
 */

public class GameMachineInfoMO {

    private String cost;
    private String bleUDIDNo;
    private String machineNumber;
    private String machineName;
    private String machineLogo;
    private String machineVideoURL;
    private String machineDescURL;
    private String machineBannerDesc;
    private String operatorDesc;
    private String operatorLogo;
    private String operatorID;
    private String providerURLTerms;
    private String providerTermsChecked;
    private String providerImgURL;
    private String amusementAvailableCredit;
    private String amusementAvailableTicket;
    private String operatorCode;
    private String serviceID;
    private String addressOne;
    private String country;
    private String city;
    private String postCode;
    private int selectedPosition;
    private ArrayList<PricingOptionsMO> pricingOptionsMOList;

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getBleUDIDNo() {
        return bleUDIDNo;
    }

    public void setBleUDIDNo(String bleUDIDNo) {
        this.bleUDIDNo = bleUDIDNo;
    }

    public String getMachineNumber() {
        return machineNumber;
    }

    public void setMachineNumber(String machineNumber) {
        this.machineNumber = machineNumber;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getMachineLogo() {
        return machineLogo;
    }

    public void setMachineLogo(String machineLogo) {
        this.machineLogo = machineLogo;
    }

    public String getMachineVideoURL() {
        return machineVideoURL;
    }

    public void setMachineVideoURL(String machineVideoURL) {
        this.machineVideoURL = machineVideoURL;
    }

    public String getMachineDescURL() {
        return machineDescURL;
    }

    public void setMachineDescURL(String machineDescURL) {
        this.machineDescURL = machineDescURL;
    }

    public String getMachineBannerDesc() {
        return machineBannerDesc;
    }

    public void setMachineBannerDesc(String machineBannerDesc) {
        this.machineBannerDesc = machineBannerDesc;
    }

    public String getOperatorDesc() {
        return operatorDesc;
    }

    public void setOperatorDesc(String operatorDesc) {
        this.operatorDesc = operatorDesc;
    }

    public String getOperatorLogo() {
        return operatorLogo;
    }

    public void setOperatorLogo(String operatorLogo) {
        this.operatorLogo = operatorLogo;
    }

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    public String getProviderURLTerms() {
        return providerURLTerms;
    }

    public void setProviderURLTerms(String providerURLTerms) {
        this.providerURLTerms = providerURLTerms;
    }

    public String getProviderTermsChecked() {
        return providerTermsChecked;
    }

    public void setProviderTermsChecked(String providerTermsChecked) {
        this.providerTermsChecked = providerTermsChecked;
    }

    public String getProviderImgURL() {
        return providerImgURL;
    }

    public void setProviderImgURL(String providerImgURL) {
        this.providerImgURL = providerImgURL;
    }

    public String getAmusementAvailableCredit() {
        return amusementAvailableCredit;
    }

    public void setAmusementAvailableCredit(String amusementAvailableCredit) {
        this.amusementAvailableCredit = amusementAvailableCredit;
    }

    public String getAmusementAvailableTicket() {
        return amusementAvailableTicket;
    }

    public void setAmusementAvailableTicket(String amusementAvailableTicket) {
        this.amusementAvailableTicket = amusementAvailableTicket;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getAddressOne() {
        return addressOne;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public ArrayList<PricingOptionsMO> getPricingOptionsMOList() {
        return pricingOptionsMOList;
    }

    public void setPricingOptionsMOList(ArrayList<PricingOptionsMO> pricingOptionsMOList) {
        this.pricingOptionsMOList = pricingOptionsMOList;
    }
}
