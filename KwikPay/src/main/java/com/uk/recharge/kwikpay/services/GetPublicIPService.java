package com.uk.recharge.kwikpay.services;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.ar.library.model.DeviceAppInformation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Ashu Rajput on 22-05-2017.
 */

public class GetPublicIPService extends JobIntentService {

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

        Log.e("IPAddress", "Block....793878");

        String publicIP = "0.0.0.0";
        URL downloadUrl;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        try {
            downloadUrl = new URL("http://myipdoc.com/ip.php");
//            downloadUrl = new URL("https://api.ipify.org/?format=json"); to get response in json
//            downloadUrl = new URL("https://api.ipify.org/");
            connection = (HttpURLConnection) downloadUrl.openConnection();
            inputStream = connection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }

            publicIP = total.toString();

        } catch (Exception exception) {
            try {
                publicIP = new DeviceAppInformation(this).getIpAddress();
            } catch (Exception e) {
            }

        } finally {
            if (connection != null)
                connection.disconnect();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }

            try {
                Log.e("IPAddress", "Finally Block executed...." + publicIP);
                getSharedPreferences("KP_Preference", MODE_PRIVATE).edit().putString("USER_PUBLIC_IP", publicIP).apply();
            } catch (Exception e) {
            }
        }
    }

   /* public GetPublicIPService() {
        super("GetPublicIPService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String publicIP = "0.0.0.0";
        URL downloadUrl;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        try {
            downloadUrl = new URL("http://myipdoc.com/ip.php");
//            downloadUrl = new URL("https://api.ipify.org/?format=json"); to get response in json
//            downloadUrl = new URL("https://api.ipify.org/");
            connection = (HttpURLConnection) downloadUrl.openConnection();
            inputStream = connection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }

            publicIP = total.toString();

        } catch (Exception exception) {
            Log.e("IPAddress", "Exception Area.....2");
            try {
                publicIP = new DeviceAppInformation(this).getIpAddress();
            } catch (Exception e) {
            }

        } finally {
            if (connection != null)
                connection.disconnect();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }

            try {
//                Log.e("IPAddress", "Finally Block executed...." + publicIP);
                getSharedPreferences("KP_Preference", MODE_PRIVATE).edit().putString("USER_PUBLIC_IP", publicIP).apply();
            } catch (Exception e) {
            }
        }
    }*/
}

