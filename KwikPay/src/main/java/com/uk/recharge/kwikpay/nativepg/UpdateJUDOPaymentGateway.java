package com.uk.recharge.kwikpay.nativepg;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.judopay.Judo;
import com.judopay.JudoOptions;
import com.judopay.PaymentActivity;
import com.judopay.model.Currency;
import com.judopay.model.Receipt;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.ReceiptScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.HashMap;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by HP on 20-06-2016.
 */
public class UpdateJUDOPaymentGateway extends Activity implements LoadResponseViaPost.AsyncRequestListenerViaPost
{
    private static final int PAYMENT_REQUEST = 10;
    private HashMap<String, String> receivedNativePGData;
    private SharedPreferences preference;
    private String receivedPGName="",receivedOrderId="",receivedAppTxnId="",receivedTotalAmount="";
    private static  String MY_JUDO_ID="",MY_API_TOKEN = "",MY_API_SECRET = "";
    private long delayTimeInterval=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.payment_holding_screen);

        preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);
        Bundle receivedData=getIntent().getExtras();

        if(receivedData!=null)
        {
            if(receivedData.containsKey("NATIVE_PG_HASHMAP_KEY"))
            {
                try
                {
                    receivedNativePGData = (HashMap<String, String>) receivedData.getSerializable("NATIVE_PG_HASHMAP_KEY");
                    MY_JUDO_ID=receivedNativePGData.get("JUDOID").toString();
                    MY_API_TOKEN=receivedNativePGData.get("TOKEN").toString();
                    MY_API_SECRET=receivedNativePGData.get("SECRETKEY").toString();
//                    MY_OAUTH_TOKEN=receivedNativePGData.get("CHECKSUMKEY").toString();
                    receivedPGName=receivedNativePGData.get("PGNAME").toString();
                    receivedOrderId=receivedNativePGData.get("ORDERID").toString();
                    receivedAppTxnId=receivedNativePGData.get("APPTXNID").toString();
                    receivedTotalAmount =receivedNativePGData.get("TOTALAMOUNTPAYABLE");

                    String mode=getResources().getString(R.string.nativePGMode);
                    if(mode.equals("SANDBOX"))
                        Judo.setup(MY_API_TOKEN,MY_API_SECRET, Judo.SANDBOX);
                    else if(mode.equals("PRODUCTION"))
                        Judo.setup(MY_API_TOKEN,MY_API_SECRET, Judo.LIVE);

                     /*judo = new Judo.Builder()
                            .setApiToken(MY_API_TOKEN)
                            .setApiSecret(MY_API_SECRET)
                            .setEnvironment(Judo.SANDBOX)
                            .setJudoId(MY_JUDO_ID)
                            .setAmount(receivedTotalAmount)
                            .setCurrency(Currency.GBP)
                            .setConsumerRef(receivedNativePGData.get("CONSUMERREFERENCE").toString())
                            .build();*/

                }catch(Exception e)
                {
                }
            }

            else if (receivedData.containsKey("NATIVE_PG_COUPON"))
            {
                receivedNativePGData = (HashMap<String, String>) receivedData.getSerializable("NATIVE_PG_COUPON");
                receivedTotalAmount =receivedNativePGData.get("TOTALAMOUNTPAYABLE");
                receivedPGName=receivedNativePGData.get("PGNAME").toString();
                receivedOrderId=receivedNativePGData.get("ORDERID").toString();
                receivedAppTxnId=receivedNativePGData.get("APPTXNID").toString();
                new LoadResponseViaPost(UpdateJUDOPaymentGateway.this, getTimerValueJson(), true).execute("");
            }
        }

        setConfiguration();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                proceedForPayment();
            }
        }, 1000);

    }

    private void setConfiguration() {
        SettingsPrefs settingsPrefs = new SettingsPrefs(this);
        Judo.setAvsEnabled(settingsPrefs.isAvsEnabled());
        Judo.setMaestroEnabled(settingsPrefs.isMaestroEnabled());
        Judo.setAmexEnabled(settingsPrefs.isAmexEnabled());

       /* SettingsPrefs settingsPrefs = new SettingsPrefs(this);
        this.judo = judo.newBuilder()
                .setAvsEnabled(settingsPrefs.isAvsEnabled())
                .setMaestroEnabled(settingsPrefs.isMaestroEnabled())
                .setAmexEnabled(settingsPrefs.isAmexEnabled())
                .build();*/

    }

    private void proceedForPayment() {

        /*try
        {
            RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.payment_holding_relativelayout);
            relativeLayout.removeAllViews();

        }catch(Exception e)
        {

        }*/

        Intent intent = new Intent(this, PaymentActivity.class);
        JudoOptions judoOptions = new JudoOptions.Builder()
                .setJudoId(MY_JUDO_ID)
                .setAmount(receivedTotalAmount)
                .setCurrency(Currency.GBP)
                .setConsumerRef(receivedNativePGData.get("CONSUMERREFERENCE").toString())
                .build();

        intent.putExtra(Judo.JUDO_OPTIONS, judoOptions);
        startActivityForResult(intent, PAYMENT_REQUEST);

        /*Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(Judo.JUDO_OPTIONS, judo);
        startActivityForResult(intent, PAYMENT_REQUEST);*/

    }

    // FORMING TIMER VALUE JSON PARAMETER IN CASE OF COUPON PAYMENT GATEWAY
    public String getTimerValueJson()
    {
        try
        {
            JSONObject json = new JSONObject();

            json.put("APISERVICE", "KPSAVEPGRESPONSE");
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KP_AND");
            json.put("APPTRANSACTIONID",receivedAppTxnId);
            if(ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID",""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("PGNAME",receivedPGName);
            json.put("AMOUNT",receivedTotalAmount);
            json.put("APPEARSONSTATEMENTAS","");
            json.put("CARDDETAILS","");
            json.put("CARDLASTFOUR","");
            json.put("CARDTOKEN","");
            json.put("CARDTYPE","");
            json.put("CONSUMER","");
            json.put(" n ","");
            json.put("CREATEDAT","");
            json.put("CURRENCY","GBP");
            json.put("ENDDATE","");
            json.put("JUDOID","");
            json.put("MERCHANTNAME","");
            json.put("MESSAGE","");
            json.put("NETAMOUNT","");
            json.put("ORIGINALAMOUNT","");
            json.put("ORIGINALRECEIPTID","");
            json.put("PARTNERSERVICEFEE","");
            json.put("RAWRECEIPT","");
            json.put("RECEIPTID","");
            json.put("REFUNDS","");
            json.put("RESULT","");
            json.put("TYPE","");
            json.put("CONSUMERREFERENCE","");
            json.put("PAYMENTREFERENCE","");
            json.put("STATUS","");
            json.put("ORDERID",receivedOrderId);
            json.put("CHECKSUM ", "");

            return json.toString();
        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    //FORM TIMER VALUE JSON PARAMETERS
    public String getTimerValueJson(Receipt receivedReceipt)
    {
        try
        {
            String formEncryptData = "KPAPP"+ "|" + String.valueOf(receivedReceipt.getAmount())+ "|" + MY_JUDO_ID+ "|" + receivedOrderId +
                    "|" +receivedAppTxnId + "|" + receivedReceipt.getResult() + "|" + "ozj5Fvn3skB51EnNK3lTqoqo+p4JxtHrzjIoyncH57o=";

            String checkSumString=encryptInSHA256(formEncryptData.trim());

            JSONObject json = new JSONObject();

            json.put("APISERVICE", "KPSAVEPGRESPONSE");
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KP_AND");
            json.put("APPTRANSACTIONID",receivedAppTxnId);
            if(ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID",""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("PGNAME",receivedPGName);
            json.put("AMOUNT",String.valueOf(receivedReceipt.getAmount()));
            json.put("APPEARSONSTATEMENTAS",receivedReceipt.getAppearsOnStatementAs().toString().trim());
            json.put("CARDDETAILS","");
            json.put("CARDLASTFOUR",receivedReceipt.getCardDetails().getLastFour());
            json.put("CARDTOKEN",receivedReceipt.getCardDetails().getToken());
            json.put("CARDTYPE",String.valueOf(receivedReceipt.getCardDetails().getType()));
            json.put("CONSUMER","");
            json.put(" n ",receivedReceipt.getConsumer().getConsumerToken());
            json.put("CREATEDAT",receivedReceipt.getCreatedAt());
            json.put("CURRENCY","GBP");
            json.put("ENDDATE",receivedReceipt.getCardDetails().getEndDate());
            json.put("JUDOID",MY_JUDO_ID);
            json.put("MERCHANTNAME",receivedReceipt.getMerchantName());
            json.put("MESSAGE",receivedReceipt.getMessage());
            json.put("NETAMOUNT",String.valueOf(receivedReceipt.getNetAmount()));
            json.put("ORIGINALAMOUNT",String.valueOf(receivedReceipt.getOriginalAmount()));
            json.put("ORIGINALRECEIPTID",String.valueOf(receivedReceipt.getOriginalReceiptId()));
            if(receivedReceipt.getPartnerServiceFee()==null)
                json.put("PARTNERSERVICEFEE","");
            else
                json.put("PARTNERSERVICEFEE",String.valueOf(receivedReceipt.getPartnerServiceFee()));
            json.put("RAWRECEIPT","");
            json.put("RECEIPTID",receivedReceipt.getReceiptId());

            // need to provide the value for the below tag
            json.put("REFUNDS","");

            json.put("RESULT",receivedReceipt.getResult());
            json.put("TYPE",receivedReceipt.getType());
            json.put("CONSUMERREFERENCE",receivedReceipt.getConsumer().getYourConsumerReference());
            json.put("PAYMENTREFERENCE",receivedReceipt.getYourPaymentReference());
            json.put("STATUS",receivedReceipt.getResult());
            json.put("ORDERID",receivedOrderId);
            json.put("CHECKSUM ", checkSumString);

            return json.toString();
        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    private static String encryptInSHA256(String base)
    {
        try
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++)
            {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PAYMENT_REQUEST) {
            switch (resultCode) {
                case Judo.RESULT_SUCCESS:
                    // handle successful payment
                   /* if(pgBanner!=null)
                    {
                        pgBanner.recycle();
                        pgBanner = null;
                    }*/

                    switchLogoWithGIF();

                    Receipt receipt = data.getParcelableExtra(Judo.JUDO_RECEIPT);
//                    Log.e("JUDO","Receipt "+receipt);
                    new LoadResponseViaPost(UpdateJUDOPaymentGateway.this, getTimerValueJson(receipt), false).execute("");
                    break;

                case Judo.RESULT_CANCELED:
                    handlingPaymentProcessing();
                    break;
                case Judo.RESULT_ERROR:
                    handlingPaymentProcessing();
                    break;
            }
        }
    }

    private void handlingPaymentProcessing()
    {
        Intent receiptIntent = new Intent(UpdateJUDOPaymentGateway.this,ReceiptScreen.class);
        receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedAppTxnId);
        startActivity(receiptIntent);
        UpdateJUDOPaymentGateway.this.finish();
    }

    @Override
    public void onRequestComplete(String loadedString) {

        Log.e("JUDO","Response "+loadedString);

        if(loadedString!=null && !loadedString.equals(""))
        {
            if(parseJudoPaymentResponse(loadedString))
            {
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        handlingPaymentProcessing();
                    }
                }, delayTimeInterval);
            }
        }
    }

    //LOGIC FOR PARSING THE DATA RECEIVED FROM WEB RESPONSE
    public boolean parseJudoPaymentResponse(String judoResponse)
    {
        JSONObject jsonObject,parentResponseObject;
        try
        {
            jsonObject=new JSONObject(judoResponse);
            if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
            {
                ARCustomToast.showToast(UpdateJUDOPaymentGateway.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }
            parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
            {
                if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
                {
                    if(parentResponseObject.has("WAITINGTIME"))
                    {
                        delayTimeInterval=Long.parseLong(parentResponseObject.getString("WAITINGTIME"));
                    }
                }

                else
                {
                    if(parentResponseObject.has("DESCRIPTION"))
                    {
                        ARCustomToast.showToast(UpdateJUDOPaymentGateway.this,parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;

    }

    private void switchLogoWithGIF()
    {
        try
        {
            GifImageView gifImageView=(GifImageView)findViewById(R.id.gifImageView);
            GifDrawable gifFromResource = new GifDrawable( getResources(), R.drawable.countdown_timer );
            gifImageView.setImageDrawable(gifFromResource);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
    }
}
