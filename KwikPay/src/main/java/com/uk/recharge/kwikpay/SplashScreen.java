package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.crashlytics.android.Crashlytics;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.uk.recharge.kwikpay.database.DBQueryMethods;
import com.uk.recharge.kwikpay.intro.KPIntroductionScreen;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.uk.recharge.kwikpay.utilities.DataDownloadApiParser;
import com.uk.recharge.kwikpay.utilities.DataDownloadApiParser.DataDownloadListener;
import com.urbanairship.UAirship;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class SplashScreen extends Activity implements AsyncRequestListenerViaPost, DataDownloadListener {
    private SharedPreferences preference;
    private DBQueryMethods database;
    HashMap<String, String> appTableVersionHashMap;
    boolean isComingFromTextFile = false;
    int apiHitPosition = 0;
    String IMEINumber = "NOT_FOUND";
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splashscreen);

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        progressBar = findViewById(R.id.progressBar);

        try {
            AppsFlyerLib.setAppsFlyerKey("oFstLDnEsuKRqdyLN5PzU5");
            AppsFlyerLib.sendTracking(getApplicationContext());
            AppsFlyerLib.setUseHTTPFalback(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        appTableVersionHashMap = new HashMap<>();
        database = new DBQueryMethods(SplashScreen.this);

        if (preference.getString("KP_USER_ID", "").equals("")) {
            SharedPreferences.Editor edit = preference.edit();
            edit.putString("KP_USER_ID", "0").apply();
        }

        // GOOGLE ADWORDS FOR RE MARKETING
        AdWordsConversionReporter.reportWithConversionId(this.getApplicationContext(), "940802495", "1lH-CPjfp2QQv4POwAM", "0", true);

        if (preference.getString("IMEI_NUMBER_KEY", "NOT_FOUND").equals("NOT_FOUND")) {
            executeRunTimPermissionForIMEI();
        }

        //BELOW METHOD IS USED TO GET THE CURRENT AND APP STORE APP VERSION, USED FOR FORCEFUL UPDATION
        getCurrentVersion();

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_SPLASH));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //SETTING DEFAULT VALUE TO PUBLIC IP PREFERENCE
        preference.edit().putString("USER_PUBLIC_IP", "0.0.0.0").apply();
    }

    @SuppressLint("MissingPermission")
    private void executeRunTimPermissionForIMEI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        } else {
            try {
                TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                IMEINumber = tm.getDeviceId();
                SharedPreferences.Editor editor = preference.edit();
                editor.putString("IMEI_NUMBER_KEY", IMEINumber).apply();
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 1) {
                    if (parseToGetSessionID(loadedString)) {
                        //STARTING INTRODUCING CONCEPT TO GET RESPONSE FROM TEXT FILE FIRST THAN CALLING RESPECTIVE DATA DOWNLOAD API
                        if (preference.getBoolean("RESPONSE_FROM_FILE", true)) {
                            String jsonResponseFromFile = loadJSONFromAsset();
                            if (jsonResponseFromFile != null) {
                                isComingFromTextFile = true;

                                SharedPreferences.Editor editor = preference.edit();
                                editor.putBoolean("RESPONSE_FROM_FILE", false).apply();

                                new DataDownloadApiParser(SplashScreen.this, jsonResponseFromFile).execute();
                            }
                        } else {
                            textFileLoadingDone();
                        }
                    }

                } else if (apiHitPosition == 2) {
                    new DataDownloadApiParser(SplashScreen.this, loadedString).execute();
                } else if (apiHitPosition == 3) {
                    parseLatestVersionAPIResponse(loadedString);
                }

            } else {
//				progressBarLoadingLayout.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                if (DeviceNetConnectionDetector.checkDataConnWifiMobile(SplashScreen.this))
                    ARCustomToast.showToast(SplashScreen.this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
                else
                    ARCustomToast.showToast(SplashScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
//			progressBarLoadingLayout.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            if (DeviceNetConnectionDetector.checkDataConnWifiMobile(SplashScreen.this))
                ARCustomToast.showToast(SplashScreen.this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
            else
                ARCustomToast.showToast(SplashScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }

    }

    @Override
    public void parsingResponse(String parseResponse) {
        if (parseResponse.equals("PARSED")) {
            if (isComingFromTextFile) {
                isComingFromTextFile = false;
                textFileLoadingDone();
            } else {
//				progressBarLoadingLayout.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);

                if (preference.getBoolean("IS_INTRODUCTION_COMPLETE", false)) {
                    //CHECKING WHETHER DEVICE IS EMULATOR OR REAL DEVICE
                    if (isEmulator())
                        SplashScreen.this.finish();
                    else
                        startActivity(new Intent(SplashScreen.this, HomeScreen.class));

//					startActivity(new Intent(SplashScreen.this,HomeScreen.class));
                } else {
                    startActivity(new Intent(SplashScreen.this, KPIntroductionScreen.class));
                }

                SplashScreen.this.finish();
            }
        } else
//			progressBarLoadingLayout.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
    }

    private static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    public String formJSONParam() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPDATADOWNLOAD");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", "NOT_FOUND"));
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));

            json.put("HOW_TO_USE_REVISED", "0");
            json.put("OPERATOR", "0");
            json.put("OPERATOR_AREA", "0");
            json.put("AREA_GROUP", "0");
            json.put("OPR_SERVICE", "0");
            json.put("TERMS_OF_USAGE_REVISED", "0");
            json.put("HOW_IT_WORKS_REVISED", "0");
            json.put("ABOUTUS_REVISED", "0");
            json.put("KP_PAGEWISE_HEADER_INFO", "0");
            json.put("APPNAME", "KWIKPAY");

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String formTableVersionJSON() {
        try {
            try {
                if (preference.getString("URBAN_AIRSHIP_CHANNEL_ID", "").equals("")) {
                    preference.edit().putString("URBAN_AIRSHIP_CHANNEL_ID", UAirship.shared().getPushManager().getChannelId()).apply();
                }
            } catch (Exception e) {
            }

            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPDATADOWNLOAD");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", "NOT_FOUND"));
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("APPNAME", "KWIKPAY");

            json.put("HOW_TO_USE_REVISED", appTableVersionHashMap.get(ProjectConstant.DB_HOW_TO_USE_REVISED));
            json.put("OPERATOR", appTableVersionHashMap.get(ProjectConstant.DB_OPERATOR_TABLENAME));
            json.put("OPERATOR_AREA", appTableVersionHashMap.get(ProjectConstant.DB_OP_AREA_TABLENAME));
            json.put("AREA_GROUP", appTableVersionHashMap.get(ProjectConstant.DB_AG_TABLENAME));
            json.put("OPR_SERVICE", appTableVersionHashMap.get(ProjectConstant.DB_OP_SERVICE_TABLENAME));
            json.put("TERMS_OF_USAGE_REVISED", appTableVersionHashMap.get(ProjectConstant.DB_TERMS_OF_USAGE_REVISED));
            json.put("HOW_IT_WORKS_REVISED", appTableVersionHashMap.get(ProjectConstant.DB_HOW_IT_WORKS_REVISED));
            json.put("ABOUTUS_REVISED", appTableVersionHashMap.get(ProjectConstant.DB_ABOUTUS_REVISED));
            json.put("KP_PAGEWISE_HEADER_INFO", appTableVersionHashMap.get(ProjectConstant.DB_KP_PHI_TABLENAME));

            json.put("PUSH_TOKEN", preference.getString("URBAN_AIRSHIP_CHANNEL_ID", ""));

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String formSessionIdJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPGETSESSIONID");
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", "NOT_FOUND"));
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String formLatestVersionJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "APP_VERSION_BY_APP_NAME");
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("SESSIONID", "");
            json.put("APPNAME", "KWIKPAY");
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String loadJSONFromAsset() {
        String jsonResponse = null;
        try {
            database.open();
            InputStream is = SplashScreen.this.getAssets().open("datadownloadresponse.txt");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonResponse = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } finally {
            database.close();
        }
        return jsonResponse;
    }

    public void textFileLoadingDone() {
        // OPENING LOCAL DATABASE AND CHECKING THE STATUS OF APP TABLE VERSION.
        try {
            database.open();

            if (database.isAppVersionTableEmpty()) {
                apiHitPosition = 2;
                new LoadResponseViaPost(SplashScreen.this, formJSONParam(), false).execute("");
            } else {
                apiHitPosition = 2;
                appTableVersionHashMap = database.getAppTableVersionData();
                new LoadResponseViaPost(SplashScreen.this, formTableVersionJSON(), false).execute("");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

    }

    public boolean parseToGetSessionID(String sessionIdResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(sessionIdResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(SplashScreen.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has("SESSIONID")) {
                        ProjectConstant.SESSIONID = parentResponseObject.getString("SESSIONID");
                        preference.edit().putString("SESSION_ID", parentResponseObject.getString("SESSIONID")).apply();
                        ProjectConstant.ISLOGINPAGE_ACTIVE = false;
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(SplashScreen.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void parseLatestVersionAPIResponse(String loadedString) {

        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(loadedString);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(SplashScreen.this, "No Response Tag", Toast.LENGTH_LONG);
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);
            String appForceUpdateFlag = "", latestAppVersion = "";

            if (parentResponseObject.has("APP_VERSION")) {
                latestAppVersion = parentResponseObject.getString("APP_VERSION");
                if (parentResponseObject.has("APP_FORCE_UPDATE"))
                    appForceUpdateFlag = parentResponseObject.getString("APP_FORCE_UPDATE");

                int versionResult = versionComparator(myAppVersion, latestAppVersion);

                // IF BOTH APP AND GOOGLE PLAY APP VERSION ARE SAME
                if (versionResult == 0) {
                    apiHitPosition = 1;
                    new LoadResponseViaPost(SplashScreen.this, formSessionIdJson(), false).execute("");
                } else if (versionResult == 1) {
                    apiHitPosition = 1;
                    new LoadResponseViaPost(SplashScreen.this, formSessionIdJson(), false).execute("");
                } else if (versionResult == -1) {
                    if (appForceUpdateFlag.equalsIgnoreCase("YES")) {
                        progressBar.setVisibility(View.INVISIBLE);
                        showUpdateDialog();
                    } else {
                        apiHitPosition = 1;
                        new LoadResponseViaPost(SplashScreen.this, formSessionIdJson(), false).execute("");
                    }
                }

            } else {
                apiHitPosition = 1;
                new LoadResponseViaPost(SplashScreen.this, formSessionIdJson(), false).execute("");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // INTEGRATING STUFF IN ORDER TO CHECK THE CURRENT VERSION OF APP ON PLAY STORE AND FORCE USER TO INSTALL THE APP
    String myAppVersion;
    Dialog dialog;

    private void getCurrentVersion() {
        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;
        try {
            pInfo = pm.getPackageInfo(this.getPackageName(), 0);
            myAppVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }

        apiHitPosition = 3;
        new LoadResponseViaPost(SplashScreen.this, formLatestVersionJSON(), false).execute("");

//        new GetLatestVersion().execute();
    }

    /*private class GetLatestVersion extends AsyncTask<String, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                //It retrieves the latest version by scraping the content of current version from play store at runtime
                Document doc = Jsoup.connect("https://play.google.com/store/apps/details?id=com.uk.recharge.kwikpay&hl=en").get();

                Log.e("Document", "Value " + doc.getElementsByAttributeValue("itemprop", "softwareVersion"));
//                doc.getElementsByAttributeValue("itemprop", "softwareVersion").first();

                googleplayAppVersion = doc.getElementsByAttributeValue("itemprop", "softwareVersion").first().text();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if (googleplayAppVersion != null) {
                int versionResult = versionComparator(myAppVersion, googleplayAppVersion);

                // IF BOTH APP AND GOOGLE PLAY APP VERSION ARE SAME
                if (versionResult == 0) {
                    apiHitPosition = 1;
                    new LoadResponseViaPost(SplashScreen.this, formSessionIdJson(), false).execute("");
                } else if (versionResult == 1) {
                    apiHitPosition = 1;
                    new LoadResponseViaPost(SplashScreen.this, formSessionIdJson(), false).execute("");
                } else if (versionResult == -1) {
                    progressBar.setVisibility(View.INVISIBLE);
                    showUpdateDialog();
                }
            } else {
                apiHitPosition = 1;
                new LoadResponseViaPost(SplashScreen.this, formSessionIdJson(), false).execute("");
//                progressBar.setVisibility(View.INVISIBLE);
//                ARCustomToast.showToast(SplashScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
            super.onPostExecute(jsonObject);
        }
    }*/

    public Integer versionComparator(String str1, String str2) {
        try {
            String[] vals1 = str1.split("\\.");
            String[] vals2 = str2.split("\\.");
            int i = 0;
            // set index to first non-equal ordinal or length of shortest version string
            while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
                i++;
            }
            // compare first non-equal ordinal number
            if (i < vals1.length && i < vals2.length) {
                int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
                return Integer.signum(diff);
            } else {
                return Integer.signum(vals1.length - vals2.length);
            }
        } catch (Exception e) {
            return 0;
        }
    }

    private void showUpdateDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("We are constantly updating to improve the security & user experience. Update the app to use our services");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.uk.recharge.kwikpay")));
                } catch (Exception e) {
                }
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setCancelable(false);
        dialog = builder.show();
    }

}

// facebook release key : tsot2pc2nIlkk1ZObO1ouclEGHA=
