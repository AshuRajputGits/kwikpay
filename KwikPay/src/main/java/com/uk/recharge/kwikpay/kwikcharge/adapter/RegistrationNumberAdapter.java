package com.uk.recharge.kwikpay.kwikcharge.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.models.RegistrationNumbersMO;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 10/8/2017.
 */

public class RegistrationNumberAdapter extends RecyclerView.Adapter<RegistrationNumberAdapter.RegNumberViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<RegistrationNumbersMO> regNoModelList = null;
    private ChangeRegNumberListener listener = null;

    public RegistrationNumberAdapter(Context context, ArrayList<RegistrationNumbersMO> regNoModelList, ChangeRegNumberListener listener) {
        layoutInflater = LayoutInflater.from(context);
        this.regNoModelList = regNoModelList;
        this.listener = listener;
    }

    @Override
    public RegNumberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.registration_no_single_row, parent, false);
        return new RegNumberViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RegNumberViewHolder holder, int position) {

        try {
            holder.regNumber.setText(regNoModelList.get(position).getRegistrationNo());
            holder.make.setText(regNoModelList.get(position).getMake());
            holder.model.setText(regNoModelList.get(position).getModel());
        } catch (Exception e) {
        }
    }

    @Override
    public int getItemCount() {
        return regNoModelList.size();
    }

    public class RegNumberViewHolder extends RecyclerView.ViewHolder {

        private TextView regNumber, make, model;

        public RegNumberViewHolder(View itemView) {
            super(itemView);
            regNumber = (TextView) itemView.findViewById(R.id.regNumber);
            make = (TextView) itemView.findViewById(R.id.make);
            model = (TextView) itemView.findViewById(R.id.model);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onRegisterNumberSelect(getLayoutPosition());
                }
            });
        }
    }

    public interface ChangeRegNumberListener {
        void onRegisterNumberSelect(int position);
    }

}
