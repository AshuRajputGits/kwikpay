package com.uk.recharge.kwikpay;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.adapters.MyAccountAdapter;
import com.uk.recharge.kwikpay.database.DBQueryMethods;

public class MyAccount extends Activity implements OnClickListener,AsyncRequestListenerViaPost
{
	ListView myAccountListView;
	MyAccountAdapter myAccountAdapter;
	ArrayList<HashMap<String, String>> myAccountArrayList;
	TextView headerBelow_ClassTV,myProfileTV,headerFavBtn,headerProfileBtn,headerChangePWBtn,headerTicketBtn,cartItemCounter;
	HashMap<String, String> myAccountHashMap,selectedTxnHashMap,repeatRechargeHashMap;

//	View separatorLine;
	SharedPreferences preference;
	boolean isRadioOn=false,isRepeatButtonClicked=false;
	JSONArray txnJsonArray;
	String selectedOrderId="", selectedServiceProvider ="",selectedDOT="",selectedDisplayAmount="",selectedRechargeAmount="",
			selectedRechargeStatus="";
	int apiHitPosition=0;
	LinearLayout myAccountLinearLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.myaccount_screen);

		settingIDs();

		apiHitPosition=1;
		new LoadResponseViaPost(MyAccount.this, formMyAccountJSON(), true).execute("");

		myAccountListView.setOnItemClickListener(new AdapterView.OnItemClickListener() 
		{
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) 
			{
				isRadioOn=true;
				selectedTxnHashMap = (HashMap<String, String>) myAccountArrayList.get(position); 
				selectedOrderId=selectedTxnHashMap.get(ProjectConstant.TXN_ORDERID);
				selectedServiceProvider=selectedTxnHashMap.get(ProjectConstant.TXN_OPERATORNAME);
				selectedDOT=selectedTxnHashMap.get(ProjectConstant.TXN_TRANSACTIONDATETIME);
				selectedDisplayAmount=selectedTxnHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT);
				selectedRechargeAmount=selectedTxnHashMap.get(ProjectConstant.TXN_RECHARGEAMOUNT);
				selectedRechargeStatus=selectedTxnHashMap.get(ProjectConstant.TXN_RECHARGESTATUS);
			}
		});

	}

	private void settingIDs()
	{
		// TODO Auto-generated method stub

		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);

		myAccountListView=(ListView)findViewById(R.id.myAccountListView);
		myAccountArrayList = new ArrayList<HashMap<String, String>>();
		//	myProfileTV=(TextView)findViewById(R.id.myAccount_MyProfile);

		headerFavBtn=(TextView)findViewById(R.id.header_FavouriteBtn);
		headerProfileBtn=(TextView)findViewById(R.id.header_MyProfileBtn);
		headerChangePWBtn=(TextView)findViewById(R.id.header_ChangePWBtn);
		headerTicketBtn=(TextView)findViewById(R.id.header_MyTicketBtn);

//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);
		
		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);
		
		myAccountLinearLayout=(LinearLayout)findViewById(R.id.myAccountHeaderLayout);
		myAccountLinearLayout.setVisibility(View.VISIBLE);

		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(MyAccount.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage,cartIconLayout,true);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		headerFavBtn.setOnClickListener(this);
		headerProfileBtn.setOnClickListener(this);
		headerChangePWBtn.setOnClickListener(this);
		headerTicketBtn.setOnClickListener(this);

		
		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME
		DBQueryMethods database=new DBQueryMethods(MyAccount.this);
		try
		{
			database.open();
			headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
			headerBelow_ClassTV.setText(database.getSubHeaderTitles("My Account"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}

	}

	private void showMyTransactionList(ArrayList<HashMap<String, String>> arrayListMap) 
	{
		// TODO Auto-generated method stub
		myAccountAdapter = new MyAccountAdapter(MyAccount.this, arrayListMap) 
		{
			@Override
			public View getView(int position, View convertView, ViewGroup parent) 
			{
				View row = super.getView(position, convertView, parent);
				return row;
			}
		};
	}

	public void myAccountOnClickMethod(View accountView)
	{
		switch(accountView.getId())
		{
		case R.id.myAccount_ViewDetail:

			if(isRadioOn)
			{
				Intent payNowIntent=new Intent(MyAccount.this,ViewDetailScreen.class);
				payNowIntent.putExtra("TRANSACTION_VIEW_DETAIL", selectedTxnHashMap);
				startActivity(payNowIntent);	
			}
			else
			{
				ARCustomToast.showToast(MyAccount.this,"please select a transaction to view details", Toast.LENGTH_LONG);
			}

			break;
		case R.id.myAccount_RaiseTicket:

			if(isRadioOn)
			{
				Intent raiseTicketIntent = new Intent(MyAccount.this,RaiseTicket.class);
				raiseTicketIntent.putExtra("RAISETICKET_ORDERID", selectedOrderId);
				raiseTicketIntent.putExtra("RAISETICKET_SERVICE_PROVIDER", selectedServiceProvider);
				raiseTicketIntent.putExtra("RAISETICKET_DOT", selectedDOT);

				startActivity(raiseTicketIntent);	
			}
			else
			{
				ARCustomToast.showToast(MyAccount.this,"please select a transaction to proceed", Toast.LENGTH_LONG);
			}

			break;
		case R.id.myAccount_Repeat:

			if(isRadioOn)
			{
				isRepeatButtonClicked=true;
				apiHitPosition=2;
				new LoadResponseViaPost(MyAccount.this, formRepeatRechargeJSON(), true).execute("");
			}
			else
			{
				ARCustomToast.showToast(MyAccount.this,"please select a valid transaction to Repeat the same", Toast.LENGTH_LONG);
			}

			break;
		case R.id.myAccount_SetFav:

			if(isRadioOn)
			{
				if(!selectedRechargeStatus.equals("SUCCESSFUL"))
				{
					ARCustomToast.showToast(MyAccount.this,"please select a successful transaction to set as favorite", Toast.LENGTH_LONG);
				}
				else
				{
					Intent setFavIntent=new Intent(MyAccount.this,SetFavourite.class);
					setFavIntent.putExtra("MYAC_FAV_HASHMAP", selectedTxnHashMap);
					startActivity(setFavIntent);
				}
				
			}
			else
			{
				ARCustomToast.showToast(MyAccount.this,"please select a transaction to set favourite", Toast.LENGTH_LONG);
			}

			break;
		}
	}

	// Expensive operation method
	public static void setListViewHeightBasedOnChildren(ListView listView) 
	{
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		int totalHeight=0;
		View view = null;

		for (int i = 0; i < listAdapter.getCount(); i++) 
		{
			view = listAdapter.getView(i, view, listView); 
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.MATCH_PARENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	@Override
	public void onClick(View v) 
	{
		// TODO Auto-generated method stub
		if(v.getId()==R.id.header_FavouriteBtn)
		{
			startActivity(new Intent(MyAccount.this,FavouriteScreen.class));
		}

		if(v.getId()==R.id.header_MyProfileBtn)
		{
			startActivity(new Intent(MyAccount.this,EditProfile.class));
		}

		if(v.getId()==R.id.header_ChangePWBtn)
		{
			startActivity(new Intent(MyAccount.this,ChangePassword.class));
		}

		if(v.getId()==R.id.header_MyTicketBtn)
		{
			startActivity(new Intent(MyAccount.this,MyTicket.class));
		}

	}

	public String formMyAccountJSON()
	{
		try
		{
			JSONObject json = new JSONObject();
			json.put("APISERVICE", "KPTRANSACTIONHISTORY");
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("DEVICEOS", "ANDROID");
			json.put("SOURCENAME", "KPAPP");
			json.put("USERID",  preference.getString("KP_USER_ID", "0"));
			//			json.put("USERID", "93");

			return json.toString();
		}catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public String formRepeatRechargeJSON()
	{
		try
		{
			JSONObject json = new JSONObject();
			json.put("APISERVICE", "KPREPEATRECHARGE");
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("DEVICEOS", "ANDROID");
			json.put("SOURCENAME", "KPAPP");
			json.put("ORDERID", selectedOrderId);
//			json.put("ORDERID", "CT0000012435-002");

			return json.toString();
		}catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub
		if(loadedString!=null && !loadedString.equals(""))
		{
			if(!loadedString.equals("Exception"))
			{
				if(apiHitPosition==1)
				{
					if(parseMyTransactionDetails(loadedString))
					{
						showMyTransactionList(myAccountArrayList);
						myAccountListView.setAdapter(myAccountAdapter);
						setListViewHeightBasedOnChildren(myAccountListView);
					}
				}

				if(apiHitPosition==2)
				{
					if(parseRepeatRechargeDetail(loadedString))
					{
						Intent payNowIntent=new Intent(MyAccount.this,OrderDetails.class);
						payNowIntent.putExtra("PAYNOW_DETAILS_VIA_RR_HASHMAP", repeatRechargeHashMap);
						startActivity(payNowIntent);
					}

				}

			}
			else
			{
				if(!isRepeatButtonClicked)
				{
					MyAccount.this.finish();
				}
				ARCustomToast.showToast(MyAccount.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
			}
		}
		else
		{
			if(!isRepeatButtonClicked)
			{
				MyAccount.this.finish();
			}
			ARCustomToast.showToast(MyAccount.this,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}
	}

	//PARSING TRANSACTION DETAILS DATA
	public boolean parseMyTransactionDetails(String myTransactionResponse)
	{
		JSONObject jsonObject,parentResponseObject,childResponseObject;

		try 
		{
			jsonObject=new JSONObject(myTransactionResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
			{
				if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
				{
					if(parentResponseObject.has(ProjectConstant.TXN_TRANSACTIONDETAIL))
					{
						myAccountArrayList=new ArrayList<HashMap<String,String>>();
						//JSONArray txnJsonArray=parentResponseObject.getJSONArray(ProjectConstant.TXN_TRANSACTIONDETAIL);

						// CHECKING WHETHER THE DATA INSIDE FAVOURITES TAG IS OBJECT OR JSON OBJECT
						Object object = parentResponseObject.get(ProjectConstant.TXN_TRANSACTIONDETAIL);

						if(object instanceof JSONObject)
						{
							final JSONObject jsonOBJECT = (JSONObject)object;
							txnJsonArray = new JSONArray();
							txnJsonArray.put(jsonOBJECT);
						}
						else if (object instanceof JSONArray)
						{
							txnJsonArray = (JSONArray)object;
						}

						for(int i=0;i<txnJsonArray.length();i++)
						{
							childResponseObject=txnJsonArray.getJSONObject(i);

							if (!childResponseObject.isNull(ProjectConstant.TXN_TRANSACTIONDATETIME)
									&& !childResponseObject.isNull(ProjectConstant.TXN_SERVICEID) 
									&& !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTCURRENCYCODE)
									&& !childResponseObject.isNull(ProjectConstant.TXN_RECHARGECURRENCYCODE)
									&& !childResponseObject.isNull(ProjectConstant.TXN_OPERATORNAME)
									&& !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTAMOUNT) 
									&& !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTSTATUS)
									&& !childResponseObject.isNull(ProjectConstant.TXN_SERVICENAME) 
									&& !childResponseObject.isNull(ProjectConstant.TXN_COUNTRYCODE)
									&& !childResponseObject.isNull(ProjectConstant.TXN_DISPLAYAMOUNT)
									&& !childResponseObject.isNull(ProjectConstant.TXN_ORDERID)
									&& !childResponseObject.isNull(ProjectConstant.TXN_OPERATORCODE) 
									&& !childResponseObject.isNull(ProjectConstant.TXN_DISPLAYCURRENCYCODE)
									&& !childResponseObject.isNull(ProjectConstant.TXN_RECHARGESTATUS)
									&& !childResponseObject.isNull(ProjectConstant.TXN_RECHARGEAMOUNT) 
									&& !childResponseObject.isNull(ProjectConstant.TXN_SUBSCRIPTIONNUMBER)) 
							{
								myAccountHashMap=new HashMap<String, String>();
								myAccountHashMap.put(ProjectConstant.TXN_TRANSACTIONDATETIME, childResponseObject.getString(ProjectConstant.TXN_TRANSACTIONDATETIME));
								myAccountHashMap.put(ProjectConstant.TXN_SERVICEID, childResponseObject.getString(ProjectConstant.TXN_SERVICEID));
								myAccountHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_PAYMENTCURRENCYCODE));
								myAccountHashMap.put(ProjectConstant.TXN_RECHARGECURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_RECHARGECURRENCYCODE));
								myAccountHashMap.put(ProjectConstant.TXN_OPERATORNAME, childResponseObject.getString(ProjectConstant.TXN_OPERATORNAME));
								myAccountHashMap.put(ProjectConstant.TXN_PAYMENTAMOUNT, childResponseObject.getString(ProjectConstant.TXN_PAYMENTAMOUNT));
								myAccountHashMap.put(ProjectConstant.TXN_PAYMENTSTATUS, childResponseObject.getString(ProjectConstant.TXN_PAYMENTSTATUS));
								myAccountHashMap.put(ProjectConstant.TXN_SERVICENAME, childResponseObject.getString(ProjectConstant.TXN_SERVICENAME));
								myAccountHashMap.put(ProjectConstant.TXN_COUNTRYCODE, childResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE));
								myAccountHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, childResponseObject.getString(ProjectConstant.TXN_DISPLAYAMOUNT));
								myAccountHashMap.put(ProjectConstant.TXN_ORDERID, childResponseObject.getString(ProjectConstant.TXN_ORDERID));
								myAccountHashMap.put(ProjectConstant.TXN_OPERATORCODE, childResponseObject.getString(ProjectConstant.TXN_OPERATORCODE));
								myAccountHashMap.put(ProjectConstant.TXN_DISPLAYCURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_DISPLAYCURRENCYCODE));
								myAccountHashMap.put(ProjectConstant.TXN_RECHARGESTATUS, childResponseObject.getString(ProjectConstant.TXN_RECHARGESTATUS));
								myAccountHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, childResponseObject.getString(ProjectConstant.TXN_RECHARGEAMOUNT));
								myAccountHashMap.put(ProjectConstant.TXN_SUBSCRIPTIONNUMBER, childResponseObject.getString(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));

								myAccountArrayList.add(myAccountHashMap);
							}
						}

					}

				}

				else
				{
					if(parentResponseObject.has("DESCRIPTION"))
					{
						ARCustomToast.showToast(MyAccount.this,parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						return false;
					}
				}

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	//PARSING REPEAT RECHARGE DETAILS DATA
	public boolean parseRepeatRechargeDetail(String repeatRCResponse)
	{
		JSONObject jsonObject,parentResponseObject;

		try 
		{
			jsonObject=new JSONObject(repeatRCResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
			{
				if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
				{
					repeatRechargeHashMap=new HashMap<String, String>();

					if(parentResponseObject.has(ProjectConstant.TXN_DISPLAYAMOUNT))
						repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, parentResponseObject.getString(ProjectConstant.TXN_DISPLAYAMOUNT).toString());
					else
						repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, selectedDisplayAmount);
						
					if(parentResponseObject.has(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID))
						repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID).toString());
					if(parentResponseObject.has("DISPLAYAMOUNTCURRENCYCODE"))
						repeatRechargeHashMap.put("DISPLAYAMOUNTCURRENCYCODE", parentResponseObject.getString("DISPLAYAMOUNTCURRENCYCODE").toString());
					if(parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGEAMT))
						repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGEAMT, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGEAMT).toString());
					if(parentResponseObject.has(ProjectConstant.TXN_SERVICEID))
						repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICEID, parentResponseObject.getString(ProjectConstant.TXN_SERVICEID).toString());
					if(parentResponseObject.has(ProjectConstant.TXN_OPERATORNAME))
						repeatRechargeHashMap.put(ProjectConstant.TXN_OPERATORNAME, parentResponseObject.getString(ProjectConstant.TXN_OPERATORNAME).toString());
					if(parentResponseObject.has("EMAILID"))
						repeatRechargeHashMap.put("EMAILID", parentResponseObject.getString("EMAILID").toString());
					if(parentResponseObject.has(ProjectConstant.PAYNOW_PROCESSINGFEE))
						repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_PROCESSINGFEE).toString());
					if(parentResponseObject.has("PGAMT"))
						repeatRechargeHashMap.put("PGAMT", parentResponseObject.getString("PGAMT").toString());
					if(parentResponseObject.has(ProjectConstant.TXN_PAYMENTCURRENCYCODE))
						repeatRechargeHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, parentResponseObject.getString(ProjectConstant.TXN_PAYMENTCURRENCYCODE).toString());

					if(parentResponseObject.has(ProjectConstant.TXN_SERVICENAME))
						repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICENAME, parentResponseObject.getString(ProjectConstant.TXN_SERVICENAME).toString());
					if(parentResponseObject.has(ProjectConstant.TXN_COUNTRYCODE))
						repeatRechargeHashMap.put(ProjectConstant.TXN_COUNTRYCODE, parentResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE).toString());
					if(parentResponseObject.has("MOBILENUMBER"))
						repeatRechargeHashMap.put("MOBILENUMBER", parentResponseObject.getString("MOBILENUMBER").toString());
					if(parentResponseObject.has(ProjectConstant.PAYNOW_SERVICEFEE))
						repeatRechargeHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_SERVICEFEE).toString());
					if(parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID))
						repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID).toString());
					if(parentResponseObject.has(ProjectConstant.API_OPCODE))
						repeatRechargeHashMap.put(ProjectConstant.API_OPCODE, parentResponseObject.getString(ProjectConstant.API_OPCODE).toString());
					if(parentResponseObject.has(ProjectConstant.PAYNOW_PLANDESCRIPTION))
						repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, parentResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION).toString());
					if(parentResponseObject.has(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID))
						repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, parentResponseObject.getString(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID).toString());

					repeatRechargeHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, selectedRechargeAmount);
					
				}

				else
				{
					if(parentResponseObject.has("DESCRIPTION"))
					{
						ARCustomToast.showToast(MyAccount.this,parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						return false;
					}
				}

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preference!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preference.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}
		
		}
	}*/


}
