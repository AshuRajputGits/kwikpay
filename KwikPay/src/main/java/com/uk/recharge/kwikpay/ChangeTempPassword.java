package com.uk.recharge.kwikpay;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidationWithMessage;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

public class ChangeTempPassword extends Activity implements AsyncRequestListenerViaPost{

	private TextView labelConfirmPW,labelNEWPW;
//	private View separatorLine;
	private EditText confirmPasswordET, newPasswordET;
//	private boolean screenIdentifierFlag=false;
	private SharedPreferences preferences;
	private String receivedOldPassword="",receivedTempUserId="";
	private HashMap<String,String> receivedPayNowDetailHashMap;
	private boolean isHashMapFull=false;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.change_temp_password);

		settingIds();
		
		Bundle receivedBundle=getIntent().getExtras();
		if(receivedBundle!=null)
		{
			if(receivedBundle.containsKey("OLD_TEMP_PW") && receivedBundle.containsKey("TEMP_USERID"))
			{
				receivedOldPassword=receivedBundle.getString("OLD_TEMP_PW");
				receivedTempUserId=receivedBundle.getString("TEMP_USERID");
			}
			if(receivedBundle.containsKey("PAYNOW_DETAILS_HASHMAP"))
			{
				isHashMapFull=true;
				receivedPayNowDetailHashMap = (HashMap<String, String>) receivedBundle.getSerializable("PAYNOW_DETAILS_HASHMAP");
			}
		}

		//RESTRICTING ' IN PASSWORD FIELD
		FieldValidationWithMessage.blockMyCharacter(confirmPasswordET, "'");
		FieldValidationWithMessage.blockMyCharacter(newPasswordET, "'");

		//SETTING SCREEN NAMES TO APPS FLYER
		try {
			EventsLoggerSingleton.getInstance().setCommonEventLogs(this,getResources().getString(R.string.SCREEN_NAME_TEMP_PW));

			/*String eventName = getResources().getString(R.string.SCREEN_NAME_TEMP_PW);
			AppsFlyerLib.trackEvent(this, eventName, null);
			EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
			((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void settingIds() 
	{
		preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);	

		labelNEWPW=(TextView)findViewById(R.id.changetemppw_newPWLabel);
		labelConfirmPW=(TextView)findViewById(R.id.changetemppw_confirmPWLabel);

		newPasswordET = (EditText)findViewById(R.id.changetemppw_newPWET);
		confirmPasswordET = (EditText)findViewById(R.id.changetemppw_confirmPWET);

		labelNEWPW.setText(Html.fromHtml("New Password"+"<font color='#ff0000'>*"));
		labelConfirmPW.setText(Html.fromHtml("Confirm Password"+"<font color='#ff0000'>*"));

//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(ChangeTempPassword.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;
	}

	public void changeTempPWSubmitButton(View changeTempPwView)
	{
		if(ChangePWValidation())
		{
			new LoadResponseViaPost(ChangeTempPassword.this, formJSONParam() , true).execute("");
		}

	}

	public boolean ChangePWValidation()
	{
		if(!FieldValidationWithMessage.passwordIsValid(newPasswordET.getText().toString(), newPasswordET, 8).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(confirmPasswordET);
			ARCustomToast.showToast(ChangeTempPassword.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}
		else if(!FieldValidationWithMessage.confirmPasswordIsValid(newPasswordET.getText().toString(), confirmPasswordET, confirmPasswordET.getText().toString()).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(newPasswordET);
			
			ARCustomToast.showToast(ChangeTempPassword.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}
		else
		{
			return true;
		}
	}

	public String formJSONParam() 
	{
		JSONObject jsonobj = new JSONObject();

		try 
		{
			String trimmedConfirmPassword = confirmPasswordET.getText().toString().replaceAll(" +"," ").trim();
			
			jsonobj.put("APISERVICE", "KPCHANGETEMPPASSWORD");
			jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
			jsonobj.put("DEVICEOS", "Android");
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("USERID", receivedTempUserId);
			jsonobj.put("OLDPASSWORD", receivedOldPassword);
			jsonobj.put("NEWPASSWORD", trimmedConfirmPassword);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub
		if(loadedString!=null && !loadedString.equals(""))
		{
			try 
			{
				JSONObject jsonObject,parentResponseObject;
				jsonObject = new JSONObject(loadedString);

				if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
				{
					ARCustomToast.showToast(ChangeTempPassword.this, "No Reponse Tag", Toast.LENGTH_LONG);
				}

				else
				{
					parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

					if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
					{
						if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
						{
							if(parentResponseObject.has("USERNAME") && parentResponseObject.has("USERID"))
							{
//								ProjectConstant.LOGIN_FORGOT_PW_FLAG=0;
								
								SharedPreferences.Editor loginPrefEdit=preferences.edit();
								loginPrefEdit.putString("KP_USER_ID", parentResponseObject.getString("USERID")).commit();
								loginPrefEdit.putString("KP_USER_NAME", parentResponseObject.getString("USERNAME")).commit();
								
								if(isHashMapFull)
								{
									isHashMapFull=false;
									Intent logInIntent=new Intent(ChangeTempPassword.this,OrderDetails.class);
									logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", receivedPayNowDetailHashMap);
									startActivity(logInIntent);
									ChangeTempPassword.this.finish();
								}
								else
								{
									ChangeTempPassword.this.finish();
								}
							}
						}
						else
						{
							if(parentResponseObject.has("DESCRIPTION"))
							{
								ARCustomToast.showToast(ChangeTempPassword.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
							}
						}

					}
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
