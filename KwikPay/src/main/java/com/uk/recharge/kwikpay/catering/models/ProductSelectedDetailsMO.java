package com.uk.recharge.kwikpay.catering.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ashu Rajput on 11/5/2018.
 */

public class ProductSelectedDetailsMO implements Serializable {

    private String categoryType;
    private String brandName;
    private String unitPrice;
    private String productPurchaseCount;
    private String productDeliverCount;

    private List<SubProductSelectedMO> subProductSelectedMOList;

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getProductPurchaseCount() {
        return productPurchaseCount;
    }

    public void setProductPurchaseCount(String productPurchaseCount) {
        this.productPurchaseCount = productPurchaseCount;
    }

    public String getProductDeliverCount() {
        return productDeliverCount;
    }

    public void setProductDeliverCount(String productDeliverCount) {
        this.productDeliverCount = productDeliverCount;
    }

    public List<SubProductSelectedMO> getSubProductSelectedMOList() {
        return subProductSelectedMOList;
    }

    public void setSubProductSelectedMOList(List<SubProductSelectedMO> subProductSelectedMOList) {
        this.subProductSelectedMOList = subProductSelectedMOList;
    }
}
