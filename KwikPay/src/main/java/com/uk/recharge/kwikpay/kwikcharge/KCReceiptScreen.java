package com.uk.recharge.kwikpay.kwikcharge;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.models.KCReceiptMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.text.DecimalFormat;

/**
 * Created by Ashu Rajput on 9/28/2017.
 */

public class KCReceiptScreen extends AppCompatActivity {

    private TextView kcReceiptVehicleNo, kcReceiptChargingLoc, kcReceiptChargeDuration,
            kcReceiptTotalPowerUsed, kcCostOfEnergy, kcReceiptTransactionFee, kcReceiptTotalCost;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kwik_charge_receipt_screen);

        settingResourceIds();

        updateReceiptUI();

        findViewById(R.id.headerHomeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KCReceiptScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        ((RatingBar) findViewById(R.id.ratingBarId)).setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                // TODO Auto-generated method stub
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.uk.recharge.kwikpay")));
                } catch (Exception e) {
                    ARCustomToast.showToast(KCReceiptScreen.this, "Google play store application not found or updated", Toast.LENGTH_LONG);
                }
            }
        });
    }

    private void settingResourceIds() {

        kcReceiptVehicleNo = (TextView) findViewById(R.id.kcReceiptVehicleNo);
        kcReceiptChargingLoc = (TextView) findViewById(R.id.kcReceiptChargingLoc);
        kcReceiptChargeDuration = (TextView) findViewById(R.id.kcReceiptChargeDuration);
//        kcReceiptStopTime = (TextView) findViewById(R.id.kcReceiptStopTime);
        kcReceiptTotalPowerUsed = (TextView) findViewById(R.id.kcReceiptTotalPowerUsed);
        kcCostOfEnergy = (TextView) findViewById(R.id.kcCostOfEnergy);
        kcReceiptTransactionFee = (TextView) findViewById(R.id.kcReceiptTransactionFee);
        kcReceiptTotalCost = (TextView) findViewById(R.id.kcReceiptTotalCost);
        ((TextView) findViewById(R.id.kwikChargeHeaderTitle)).setText("Receipt");
    }

    private void updateReceiptUI() {
        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("KC_RECEIPT_DETAILS_MO")) {
                KCReceiptMO kcReceiptMO = (KCReceiptMO) receivedBundle.getSerializable("KC_RECEIPT_DETAILS_MO");

                kcReceiptVehicleNo.setText(kcReceiptMO.getRegistrationNumber());
                kcReceiptChargingLoc.setText(kcReceiptMO.getLocationName());
                double finalCostOfEnergy = 0.0;

                try {
                    double receivedRate = Double.parseDouble(kcReceiptMO.getRate());
                    double receivedTotalEnergy = Double.parseDouble(kcReceiptMO.getTotalEnergy());
                    finalCostOfEnergy = receivedRate * receivedTotalEnergy;
                    kcCostOfEnergy.setText(Utility.fromHtml("\u00a3") + convertToTwoDecimals(finalCostOfEnergy));
                } catch (Exception e) {
                }

                kcReceiptChargeDuration.setText(kcReceiptMO.getDuration());

                String totalPowerUsed = "";
                if (kcReceiptMO.getTotalEnergy() != null && !kcReceiptMO.getTotalEnergy().isEmpty()) {
                    totalPowerUsed = convertToTwoDecimals(Double.parseDouble(kcReceiptMO.getTotalEnergy()));
                }
                kcReceiptTotalPowerUsed.setText(totalPowerUsed + " kWh");

                String serviceFee = kcReceiptMO.getServiceFee();
                kcReceiptTransactionFee.setText(Utility.fromHtml("\u00a3") + serviceFee);

                try {
                    double txnFee = 0.0;
                    if (!serviceFee.equals(""))
                        txnFee = Double.parseDouble(serviceFee);
                    double finalTotalCost = finalCostOfEnergy + txnFee;
                    kcReceiptTotalCost.setText(Utility.fromHtml("\u00a3") + convertToTwoDecimals(finalTotalCost));

                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private String convertToTwoDecimals(double valueToConvert) {

        String convertedValues = "";
        try {
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            convertedValues = decimalFormat.format(valueToConvert);
//            String.valueOf(decimalFormat.format(valueToConvert));

        } catch (Exception e) {
        }
        return convertedValues;
    }

}
