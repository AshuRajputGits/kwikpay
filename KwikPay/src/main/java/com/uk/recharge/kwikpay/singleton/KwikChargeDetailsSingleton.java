package com.uk.recharge.kwikpay.singleton;

import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;

/**
 * Created by Ashu Rajput on 10/8/2017.
 */

public class KwikChargeDetailsSingleton {
    private static KwikChargeDetailsSingleton ourInstance = null;
    private LocationIdMO locationIdMO=null;

    public static KwikChargeDetailsSingleton getInstance() {
        if (ourInstance == null)
            ourInstance = new KwikChargeDetailsSingleton();
        return ourInstance;
    }

    private KwikChargeDetailsSingleton() {
    }

    public LocationIdMO getLocationIdMO() {
        return locationIdMO;
    }

    public void setLocationIdMO(LocationIdMO locationIdMO) {
        this.locationIdMO = locationIdMO;
    }

    public void clearLocationDetails(){
        locationIdMO=null;
    }
}
