package com.uk.recharge.kwikpay.kwikcharge.models;

import java.io.Serializable;

/**
 * Created by Ashu Rajput on 11/27/2017.
 */

public class KCReceiptMO implements Serializable {

    String LocationName;
    String RegistrationNumber;
    String MaxTime;
    String Rate;
    String TotalEnergy;
    String ServiceFee;
    String Duration;
    String Seconds;

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        RegistrationNumber = registrationNumber;
    }

    public String getMaxTime() {
        return MaxTime;
    }

    public void setMaxTime(String maxTime) {
        MaxTime = maxTime;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getTotalEnergy() {
        return TotalEnergy;
    }

    public void setTotalEnergy(String totalEnergy) {
        TotalEnergy = totalEnergy;
    }

    public String getServiceFee() {
        return ServiceFee;
    }

    public void setServiceFee(String serviceFee) {
        ServiceFee = serviceFee;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getSeconds() {
        return Seconds;
    }

    public void setSeconds(String seconds) {
        Seconds = seconds;
    }
}
