package com.uk.recharge.kwikpay.catering.models;

import java.io.Serializable;

/**
 * Created by Ashu Rajput on 11/5/2018.
 */

public class SubProductSelectedMO implements Serializable{
    private String subProdCategoryType;
    private String subProdBrandName;
    private String subProdUnitPrice;
    private String subProdPurchaseCount;
    private String subProdDispenseCount;

    public String getSubProdCategoryType() {
        return subProdCategoryType;
    }

    public void setSubProdCategoryType(String subProdCategoryType) {
        this.subProdCategoryType = subProdCategoryType;
    }

    public String getSubProdBrandName() {
        return subProdBrandName;
    }

    public void setSubProdBrandName(String subProdBrandName) {
        this.subProdBrandName = subProdBrandName;
    }

    public String getSubProdUnitPrice() {
        return subProdUnitPrice;
    }

    public void setSubProdUnitPrice(String subProdUnitPrice) {
        this.subProdUnitPrice = subProdUnitPrice;
    }

    public String getSubProdPurchaseCount() {
        return subProdPurchaseCount;
    }

    public void setSubProdPurchaseCount(String subProdPurchaseCount) {
        this.subProdPurchaseCount = subProdPurchaseCount;
    }

    public String getSubProdDispenseCount() {
        return subProdDispenseCount;
    }

    public void setSubProdDispenseCount(String subProdDispenseCount) {
        this.subProdDispenseCount = subProdDispenseCount;
    }
}
