package com.uk.recharge.kwikpay.nativepg;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

public class PayPalOTPVerification extends Activity implements OnClickListener,AsyncRequestListenerViaPost
{
	Button paypalOTPproceedButton;
	TextView paypalResendOTP,paypalOtpLabel,paypalInfoMsgTV;
	EditText paypalOTPET;
	int apiHitter=0;
	private SharedPreferences preference;
	String receivedOrderId="",receivedPPEmailId="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.paypal_otp_screen);
		
		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);
		paypalOTPproceedButton=(Button)findViewById(R.id.paypalOtpProceedButton);
		paypalResendOTP=(TextView)findViewById(R.id.paypalResendOTPLink);
		paypalOtpLabel=(TextView)findViewById(R.id.paypalOtpLabel);
		paypalInfoMsgTV=(TextView)findViewById(R.id.paypalOtpInfoMsg);
		paypalOTPET=(EditText)findViewById(R.id.paypalOtpET);
		
		paypalOtpLabel.setText(Html.fromHtml("Enter verification code  here"+"<font color='#ff0000'>*"));
		
		Bundle receivedBundle=getIntent().getExtras();
		if(receivedBundle!=null)
		{
			if(receivedBundle.containsKey("PP_EMAIL_ID") && receivedBundle.containsKey("PP_ORDER_ID"))
			{
				receivedPPEmailId=receivedBundle.getString("PP_EMAIL_ID");
				receivedOrderId=receivedBundle.getString("PP_ORDER_ID");
			}
		}
		
		/*String msg="For security reasons we need to verify your email. Hence a one time password has been sent on the email "+
				maskEmail(0,3,receivedPPEmailId) +". Please enter the same in the space provided below";*/
		
		String msg="We need a one time verification and hence have sent an OTP on the email "+
					receivedPPEmailId +". \n\nPls check your junk mail box in case mail has not come through";
		
		paypalInfoMsgTV.setText(msg);
		paypalOTPproceedButton.setOnClickListener(this);
		paypalResendOTP.setOnClickListener(this);
	}

	/*private static String maskEmail(int leadingUnmasketChars, int trailingUnmasketChars, String email) {
		try
		{
			String output = "";
			String regex = "^(.{" + leadingUnmasketChars + "})(.*)(.{" + trailingUnmasketChars + "})(@)";
			Matcher m = Pattern.compile(regex).matcher(email);

			if (m.find()) {
				String stringToReplace = m.group(2);
				char[] chars = new char[stringToReplace.length()];
				Arrays.fill(chars, '*');
				String replacement = new String(chars);
				output = email.replaceAll(stringToReplace, replacement);
			}

			return output;

		}catch(Exception e)
		{
			return email;
		}
	}
	*/
	
	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		
		if(view.getId()==R.id.paypalOtpProceedButton)
		{
			if(paypalOTPET.getText().toString().isEmpty())
			{
				paypalOTPET.requestFocus();
				paypalOTPET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				ARCustomToast.showToast(PayPalOTPVerification.this, "Please enter a valid OTP", Toast.LENGTH_LONG);
			}
			else if(paypalOTPET.getText().toString().length()<6)
			{
				paypalOTPET.requestFocus();
				paypalOTPET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				ARCustomToast.showToast(PayPalOTPVerification.this, "Please enter a valid OTP", Toast.LENGTH_LONG);
			}
			else
			{
				paypalOTPET.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
				apiHitter=1;
				new LoadResponseViaPost(PayPalOTPVerification.this, formVerifyPayPalOTPJson() , true).execute("");
			}
		}
		else if(view.getId()==R.id.paypalResendOTPLink)
		{
			apiHitter=2;
			new LoadResponseViaPost(PayPalOTPVerification.this, formPayPalResendOTPJson() , true).execute("");
		}
		
	}
	
	private String formVerifyPayPalOTPJson()
	{
		JSONObject jsonobj = new JSONObject();
		try 
		{
			jsonobj.put("APISERVICE", "KPSAVEPAYPALOTP");
            if(ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID",""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("ORDERID",receivedOrderId);
			jsonobj.put("PAYPALEMAIL",receivedPPEmailId);
			jsonobj.put("OTPVALUE", paypalOTPET.getText().toString());
			jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();
	}
	
	private String formPayPalResendOTPJson()
	{
		JSONObject jsonobj = new JSONObject();
		try 
		{
			jsonobj.put("APISERVICE", "KPRESENDPAYPALOTP");
            if(ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID",""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("ORDERID", receivedOrderId);
			jsonobj.put("PAYPALEMAIL", receivedPPEmailId);
			jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();
	}
	
	@Override
	public void onRequestComplete(String loadedString) {
		// TODO Auto-generated method stub
		if(loadedString!=null && !loadedString.equals("") && !loadedString.equals("Exception"))
		{
			try
			{
				JSONObject jsonObject = new JSONObject(loadedString);
				JSONObject responseJsonObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);
				
				if(responseJsonObject.has(ProjectConstant.API_RESPONSE_CODE))
				{
					if(responseJsonObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
					{
						if(apiHitter==1)
						{
							apiHitter=0;
							Intent returnIntent = new Intent();
							returnIntent.putExtra("RESPONSE",true);
							setResult(RESULT_OK,returnIntent);
							PayPalOTPVerification.this.finish();
						}
						else if(apiHitter==2)
						{
							apiHitter=0;
						}
					}
					else
					{
						if(responseJsonObject.has("DESCRIPTION"))
							ARCustomToast.showToast(PayPalOTPVerification.this, responseJsonObject.getString("DESCRIPTION"),Toast.LENGTH_LONG);
					}
				}
				
			}catch(Exception e)
			{
				
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}

}
