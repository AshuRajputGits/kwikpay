package com.uk.recharge.kwikpay.kwikcharge.kccharger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.payment.KCChargingTimeScreen;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ashu Rajput on 12/17/2017.
 */

public class KCChargeStepProcessScreen extends AppCompatActivity implements AsyncRequestListenerViaPost {

    private int stepperCounter = 0;
    private int dividedTimer = 0;
    private Handler timerHandler = new Handler();
    private ImageView stepperInitiateTick, stepperProcessingTick, stepperPreparingChargeTick, stepperStartingChargeTick;
    private TextView stepperInitiateTickTV, stepperProcessingTickTV, stepperPreparingChargeTickTV, stepperStartingChargeTickTV;
    private int startStatusTimer = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_charge_steps_process);

        setUpIds();

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("START_STATUS_TIMER_IN_SECONDS")) {
                if (!receivedBundle.getString("START_STATUS_TIMER_IN_SECONDS").isEmpty()) {
                    try {
                        startStatusTimer = Integer.parseInt(receivedBundle.getString("START_STATUS_TIMER_IN_SECONDS"));
                        new LoadResponseViaPost(KCChargeStepProcessScreen.this, formKCChargeRunningStatusJson(), true).execute("");
//                        initiateStepperViaTimer(startStatusTimer);
                    } catch (Exception e) {
                    }
                }
            }
        }

        findViewById(R.id.headerHomeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KCChargeStepProcessScreen.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

    }

    private void setUpIds() {

        stepperInitiateTick = findViewById(R.id.stepperInitiateTick);
        stepperProcessingTick = findViewById(R.id.stepperProcessingTick);
        stepperPreparingChargeTick = findViewById(R.id.stepperPreparingChargeTick);
        stepperStartingChargeTick = findViewById(R.id.stepperStartingChargeTick);

        stepperInitiateTickTV = findViewById(R.id.stepperInitiateTickTV);
        stepperProcessingTickTV = findViewById(R.id.stepperProcessingTickTV);
        stepperPreparingChargeTickTV = findViewById(R.id.stepperPreparingChargeTickTV);
        stepperStartingChargeTickTV = findViewById(R.id.stepperStartingChargeTickTV);
    }

    private void initiateStepperViaTimer(int startStatusTimer) {
        dividedTimer = (startStatusTimer * 1000) / 4;
        startBlinkAnimation(stepperInitiateTickTV);
        timerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stepperCounter = stepperCounter + 1;
                if (stepperCounter == 1) {
                    stopBlinkAnimation(stepperInitiateTickTV);
                    startBlinkAnimation(stepperProcessingTickTV);
                    stepperInitiateTick.setVisibility(View.VISIBLE);
                } else if (stepperCounter == 2) {
                    stopBlinkAnimation(stepperProcessingTickTV);
                    startBlinkAnimation(stepperPreparingChargeTickTV);
                    stepperProcessingTick.setVisibility(View.VISIBLE);
                } else if (stepperCounter == 3) {
                    stopBlinkAnimation(stepperPreparingChargeTickTV);
                    startBlinkAnimation(stepperStartingChargeTickTV);
                    stepperPreparingChargeTick.setVisibility(View.VISIBLE);
                } else if (stepperCounter == 4) {
                    stopBlinkAnimation(stepperStartingChargeTickTV);
                    stepperStartingChargeTick.setVisibility(View.VISIBLE);
                    try {
                        Thread.sleep(1000);
                        startActivity(new Intent(KCChargeStepProcessScreen.this, KCChargingTimeScreen.class));
//                        new LoadResponseViaPost(KCChargeStepProcessScreen.this, formKCChargeRunningStatusJson(), true).execute("");
                    } catch (Exception e) {
                    }
                }
                timerHandler.postDelayed(this, dividedTimer);
            }
        }, dividedTimer);
    }

    private String formKCChargeRunningStatusJson() {

        SharedPreferences preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("APISERVICE", "KC_CHARGE_RUNNING_STATUS");
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
            jsonobj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("LOCATION_NUMBER", preference.getString("TEMP_LOCATION_NUMBER", ""));
            jsonobj.put("DEVICE_DB_ID", preference.getString("TEMP_DEVICE_DB_ID", ""));
            jsonobj.put("LOCAL_CONNECTOR_NUMBER", preference.getString("TEMP_LOCAL_CONNECTOR_NUMBER", ""));
            jsonobj.put("APP_TXN_ID", preference.getString("TEMP_APP_TXN_ID", ""));
//            jsonobj.put("APP_TXN_ID", "123");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                parseChargeRunningStatusResponse(loadedString);
            } else {
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    private void parseChargeRunningStatusResponse(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    initiateStepperViaTimer(startStatusTimer);
//                    startActivity(new Intent(this, KCChargingTimeScreen.class));
                } else {
                    startActivity(new Intent(this, KCChargeRunningStatusScreen.class));
                }
            }
        } catch (Exception e) {
            ARCustomToast.showToast(this, "Parsing exception", Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    Animation anim = new AlphaAnimation(0.0f, 1.0f);

    private void startBlinkAnimation(TextView view) {
        try {
            anim.setDuration(350); //You can manage the blinking time with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            view.startAnimation(anim);
        } catch (Exception e) {
        }
    }

    private void stopBlinkAnimation(TextView view) {
        try {
            view.clearAnimation();
        } catch (Exception e) {
        }
    }
}
