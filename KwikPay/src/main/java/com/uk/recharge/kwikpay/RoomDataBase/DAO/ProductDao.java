package com.uk.recharge.kwikpay.RoomDataBase.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.uk.recharge.kwikpay.RoomDataBase.Models.Product;

import java.util.List;

@Dao
public interface ProductDao {

    @Query("SELECT * FROM Product")
    List<Product> getAll();

    @Insert
    void insert(List<Product> list);

    @Query("delete from Product")
    void clear();

    @Query("SELECT COUNT(*) from Product")
    int count();

}
