package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidation;
import com.ar.library.validation.FieldValidationWithMessage;
import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.database.DBQueryMethods;
import com.uk.recharge.kwikpay.nativepg.PayPalPaymentScreen;
import com.uk.recharge.kwikpay.nativepg.UpdateJUDOPaymentGateway;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

public class OrderDetails extends Activity implements AsyncRequestListenerViaPost {

    private TextView serviceProviderValue, mobNumberValue, topUpOptionValue, transactionFeeValue, totAmtPayableValue, couponDiscountAmount;
    private ImageView reedemCouponGoTV;
    private boolean isSuccessfullCouponCase = false, ispaynowPressed = false;
    private HashMap<String, String> receivedPayNowDetailHashMap, nativePGDataHashMap;
    String transactionFee = "", totalAmountPayable = "", receivedServiceId = "", receivedOPCode = "", PG_APP_TXN_ID = "", PG_TYPE = "",
            receivedCountryCode = "", receivedRCAmount = "", dbReceivedOpABBR = "", couponPaymentAmount = "", receivedOperatorId = "",
            couponCode = "", couponSrNo = "", couponAmount = "0.00", infoIconDisplayFlag = "", infoIconDisplayMsg = "", couponTxnId = "";
    String apiPGName = "", mobileNumber = "";
    int apiHitPosition = 0;
    SharedPreferences preference;
    TextView paypalPGBtn, judoPGBtn, orderDetailCountryOrPinTV;
    LinearLayout withoutCouponLayout, reedemCouponBoxesLL;
    //    LinearLayout withCouponLayout;
    private NestedScrollView orderDetailLayout;
    RelativeLayout pgIntermediateLayout;
    EditText couponBoxET1, couponBoxET2, couponBoxET3;
    DBQueryMethods database;
    private CheckBox couponCheckBox, tncCheckBox, sendGiftCheckBox;
    private static final int MOBILE_OTP_REQUEST_CODE = 100;
    private static final int EMAIL_VERIFICATION_REQUEST_CODE = 200;
    private static final int BILLING_ADDRESS_REQUEST_CODE = 300;
    private static final int TERM_CONDITION_CODE = 40;
    private static final int EDIT_BILLING_ADDRESS_REQUEST_CODE = 45;
    private TextView headerTitle;
    private TextView tncWebViewTV;
    private LinearLayout addressLayout;
    private TextView addressPostalCodeTV;
    private boolean isTermConditionAccepted = false;
    protected boolean isAddressFound = false;
    private boolean isVisaChoose = false;
    private String directPaymentScreenFlag = "";
    private RelativeLayout orderScreenHeaderLayout;
    private EditText sendGiftEmailAddress, sendGiftMobileNumber, sendGiftRecipientName;
    private ImageView txnDetailsOperatorLogo;
    private ImageView txnDetailsRCStatusIcon;
    private TextView orderDetailPlanDescription;
    private String operatorProductCode = "";

    //************************************************************************************//
    // apiHitPosition=1: formPGDetails, to get PGSOID and MOPID          			      //
    // apiHitPosition=2: formTxnId    , to get Txn Id and Native Tag					  //
    // apiHitPosition=3: getNativePGDtails, to get details of Native PG.				  //
    // apiHitPosition=4: getCouponStatus, to check the status of COupon when Go Clicked   //
    // apiHitPosition=5: lockCoupon, to Lock the Coupon									  //
    // apiHitPosition=6: formSaveToCart  , to save the transaction to Cart 				  //
    // apiHitPosition=7: formDeleteAllTxn, to Delete all the transaction from cart		  //
    //************************************************************************************//

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.order_details_card_view);

        settingIds();

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("PAYNOW_DETAILS_HASHMAP")) {
                receivedPayNowDetailHashMap = (HashMap<String, String>) receivedBundle.getSerializable("PAYNOW_DETAILS_HASHMAP");

                mobileNumber = receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER);
                topUpOptionValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE)) + " " +
                        receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_DISPLAYAMT));

                transactionFee = paymentConverter(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_SERVICEFEE),
                        receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PROCESSINGFEE));

                if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.TOPUP_API_PAYMENTAMT)) {
                    couponPaymentAmount = receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_PAYMENTAMT);
                    totalAmountPayable = paymentConverter(transactionFee, receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_PAYMENTAMT));
                }

				/*if(receivedPayNowDetailHashMap.containsKey("PGAMT"))
                    totalAmountPayable=paymentConverter(transactionFee,	receivedPayNowDetailHashMap.get("PGAMT"));*/

                if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE))
                    totAmtPayableValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE)) + " " + totalAmountPayable);
                receivedRCAmount = receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_RECHARGEAMOUNT);

            }

            if (receivedBundle.containsKey("PAYNOW_DETAILS_VIA_RR_HASHMAP")) {
                receivedPayNowDetailHashMap = (HashMap<String, String>) receivedBundle.getSerializable("PAYNOW_DETAILS_VIA_RR_HASHMAP");

                mobileNumber = receivedPayNowDetailHashMap.get("MOBILENUMBER");
                topUpOptionValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get("DISPLAYAMOUNTCURRENCYCODE")) + " " +
                        receivedPayNowDetailHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT));

                transactionFee = paymentConverter(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_SERVICEFEE),
                        receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PROCESSINGFEE));

                if (receivedPayNowDetailHashMap.containsKey("PGAMT")) {
                    couponPaymentAmount = receivedPayNowDetailHashMap.get("PGAMT");
                    totalAmountPayable = paymentConverter(transactionFee, receivedPayNowDetailHashMap.get("PGAMT"));
                }
                totAmtPayableValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE)) + " " + totalAmountPayable);

                if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.TXN_RECHARGEAMOUNT))
                    receivedRCAmount = receivedPayNowDetailHashMap.get(ProjectConstant.TXN_RECHARGEAMOUNT);
                if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.TOPUP_API_RECHARGEAMT))
                    receivedRCAmount = receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_RECHARGEAMT);
            }

            try {
                serviceProviderValue.setText(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_OPERATORNAME));
                mobNumberValue.setText("Mobile " + mobileNumber);

                transactionFeeValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE)) + " " + transactionFee);
                receivedOPCode = receivedPayNowDetailHashMap.get(ProjectConstant.API_OPCODE);
                receivedServiceId = receivedPayNowDetailHashMap.get("SERVICEID");
                receivedCountryCode = receivedPayNowDetailHashMap.get(ProjectConstant.TXN_COUNTRYCODE);

                if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE))
                    couponDiscountAmount.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE)) + " 0.00");
                /*else if (receivedPayNowDetailHashMap.containsKey("DISPLAYAMOUNTCURRENCYCODE"))
                    couponDiscountAmount.setText(Html.fromHtml(receivedPayNowDetailHashMap.get("DISPLAYAMOUNTCURRENCYCODE")) + " 0.00");*/

                if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.API_OPID))
                    receivedOperatorId = receivedPayNowDetailHashMap.get(ProjectConstant.API_OPID);

                if (receivedPayNowDetailHashMap.containsKey("PLANDESCRIPTION"))
                    orderDetailPlanDescription.setText(Html.fromHtml(receivedPayNowDetailHashMap.get("PLANDESCRIPTION")));

                if (receivedPayNowDetailHashMap.containsKey("OPERATORPRODUCTCODE"))
                    operatorProductCode = receivedPayNowDetailHashMap.get("OPERATORPRODUCTCODE");


                Glide.with(this)
                        .load(receivedPayNowDetailHashMap.get(ProjectConstant.API_OPIMGPATH))
                        .placeholder(R.drawable.default_operator_logo)
                        .into(txnDetailsOperatorLogo);

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (receivedServiceId.equals("2")) {
                if (receivedPayNowDetailHashMap.containsKey("COUNTRY_NAME")) {
                    orderDetailCountryOrPinTV.setText(receivedPayNowDetailHashMap.get("COUNTRY_NAME"));
                    orderDetailCountryOrPinTV.setVisibility(View.VISIBLE);
                } else if (receivedPayNowDetailHashMap.containsKey("COUNTRYNAME")) {
                    orderDetailCountryOrPinTV.setText(receivedPayNowDetailHashMap.get("COUNTRYNAME"));
                    orderDetailCountryOrPinTV.setVisibility(View.VISIBLE);
                }
            } else if (receivedServiceId.equals("3")) {
                mobNumberValue.setVisibility(View.GONE);
            }
        }

        // OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME
        DBQueryMethods database = new DBQueryMethods(OrderDetails.this);
        try {
            database.open();
            dbReceivedOpABBR = database.getOpABBR(receivedOPCode);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                database.close();
            } catch (Exception e) {
            }
        }

        paypalPGBtn.setOnClickListener(viewClickListener);
        judoPGBtn.setOnClickListener(viewClickListener);
        reedemCouponGoTV.setOnClickListener(viewClickListener);
        tncWebViewTV.setOnClickListener(viewClickListener);
        tncCheckBox.setOnClickListener(viewClickListener);
        findViewById(R.id.headerBackButton).setOnClickListener(viewClickListener);
        findViewById(R.id.orderDetailCheckOutBtn).setOnClickListener(viewClickListener);

//        couponInfoTV.setOnClickListener(viewClickListener);
        /*tncCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });*/

        //INTRODUCING NEW API, TO GET THE STATUS OF ADDRESS ALREADY EXIST IN CENTRAL D.B. OR NOT
        apiHitPosition = 8;
        LoadResponseViaPost asyncObject8 = new LoadResponseViaPost(OrderDetails.this, formConfirmAddressJSON(), true);
        asyncObject8.execute("");
        asyncObject8 = null;

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_ORDER_SCREEN));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.paypal_PG_Button) {
                apiHitPosition = 2;
                isVisaChoose = false;
                new LoadResponseViaPost(OrderDetails.this, formAppTxnIDJSON(), true).execute("");

            } else if (id == R.id.judo_PG_Button) {
                apiHitPosition = 2;
                isVisaChoose = true;
                new LoadResponseViaPost(OrderDetails.this, formAppTxnIDJSON(), true).execute("");

            } else if (id == R.id.reedemCouponGoTV) {
                final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (!FieldValidation.couponFieldIsValid(couponBoxET1.getText().toString(), couponBoxET1, couponBoxET1.getText().toString().length(), 1)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    ARCustomToast.showToast(OrderDetails.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else if (!FieldValidation.couponFieldIsValid(couponBoxET2.getText().toString(), couponBoxET2, couponBoxET2.getText().toString().length(), 2)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    ARCustomToast.showToast(OrderDetails.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else if (!FieldValidation.couponFieldIsValid(couponBoxET3.getText().toString(), couponBoxET3, couponBoxET3.getText().toString().length(), 3)) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    ARCustomToast.showToast(OrderDetails.this, "Please enter a valid coupon no.", Toast.LENGTH_LONG);
                } else {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    apiHitPosition = 4;
                    new LoadResponseViaPost(OrderDetails.this, formRedeemCouponJSON(), true).execute("");
                }
            } else if (id == R.id.tncCheckBox) {
                if (tncCheckBox.isChecked())
                    isTermConditionAccepted = true;
                else
                    isTermConditionAccepted = false;

            } else if (id == R.id.addressEditButton) {
                startActivityForResult(new Intent(OrderDetails.this, BillingAddressScreen.class), EDIT_BILLING_ADDRESS_REQUEST_CODE);
            } else if (id == R.id.tncMessageWebView) {
                startActivityForResult(new Intent(OrderDetails.this, CommonStaticPages.class)
                        .putExtra("STATIC_PAGE_NAME", "TNC_PAGE"), TERM_CONDITION_CODE);
            } else if (id == R.id.headerBackButton) {
                finish();
            } else if (id == R.id.orderDetailCheckOutBtn) {
                confirmPaymentProcess();
            }

            /*else if (id == R.id.orderDetail_CouponINFO_TV) {
                ARCustomToast.showToast(OrderDetails.this, infoIconDisplayMsg, Toast.LENGTH_LONG);
            }*/
        }
    };

    private void settingIds() {

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        database = new DBQueryMethods(this);
        addressLayout = findViewById(R.id.addressLayout);
        orderDetailLayout = findViewById(R.id.orderDetailId);
        pgIntermediateLayout = findViewById(R.id.pgIntermediateScreenId);
        paypalPGBtn = findViewById(R.id.paypal_PG_Button);
        judoPGBtn = findViewById(R.id.judo_PG_Button);
        headerTitle = findViewById(R.id.mainHeaderTitleName);
        headerTitle.setText("Your Order");
        serviceProviderValue = findViewById(R.id.orderDetail_ServiceProviderTV);
//        serviceDetailsValue = findViewById(R.id.orderDetail_ServiceDetailTV);
        mobNumberValue = findViewById(R.id.orderDetail_MobNumberTV);
        topUpOptionValue = findViewById(R.id.orderDetail_TopUpOptionTV);
        transactionFeeValue = findViewById(R.id.orderDetail_TransFeeTV);
        totAmtPayableValue = findViewById(R.id.orderDetail_TotAmountTV);
        addressPostalCodeTV = findViewById(R.id.addressPostalCodeTV);
        couponDiscountAmount = findViewById(R.id.orderDetail1_CouponAmountTV);
        reedemCouponBoxesLL = findViewById(R.id.reedemCouponBoxesLayouts);
        withoutCouponLayout = findViewById(R.id.orderDetail_WithoutCoupon);
        orderDetailCountryOrPinTV = findViewById(R.id.orderDetailCountryOrPinTV);
//        withCouponLayout = findViewById(R.id.orderDetail_WithCoupon);

        couponBoxET1 = findViewById(R.id.redeemCouponCheckBoxET1);
        couponBoxET2 = findViewById(R.id.redeemCouponCheckBoxET2);
        couponBoxET3 = findViewById(R.id.redeemCouponCheckBoxET3);

//        couponInfoTV = findViewById(R.id.orderDetail_CouponINFO_TV);
        reedemCouponGoTV = findViewById(R.id.reedemCouponGoTV);
        couponCheckBox = findViewById(R.id.redeemCouponCheckBox);
        sendGiftCheckBox = findViewById(R.id.sendGiftCheckBox);
        tncCheckBox = findViewById(R.id.tncCheckBox);
        tncWebViewTV = findViewById(R.id.tncMessageWebView);
        txnDetailsOperatorLogo = findViewById(R.id.txnDetailsOperatorLogo);
        orderScreenHeaderLayout = findViewById(R.id.orderScreenHeaderLayout);
        sendGiftEmailAddress = findViewById(R.id.sendGiftEmailAddress);
        sendGiftRecipientName = findViewById(R.id.sendGiftRecipientName);
        sendGiftMobileNumber = findViewById(R.id.sendGiftMobileNumber);
        txnDetailsRCStatusIcon = findViewById(R.id.txnDetailsRCStatusIcon);
        orderDetailPlanDescription = findViewById(R.id.orderDetailPlanDescription);

        findViewById(R.id.addressEditButton).setOnClickListener(viewClickListener);

        couponCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if (isChecked) {
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET2);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET1);
                    FieldValidationWithMessage.removeAllErrorIcons(couponBoxET3);
                    reedemCouponBoxesLL.setVisibility(View.VISIBLE);
                } else {
                    reedemCouponBoxesLL.setVisibility(View.GONE);
                    couponBoxET1.setText("");
                    couponBoxET2.setText("");
                    couponBoxET3.setText("");
                }
            }
        });

        sendGiftCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if (isChecked) {
                    FieldValidationWithMessage.removeAllErrorIcons(sendGiftEmailAddress);
                    FieldValidationWithMessage.removeAllErrorIcons(sendGiftMobileNumber);
                    sendGiftEmailAddress.setVisibility(View.VISIBLE);
                    sendGiftMobileNumber.setVisibility(View.VISIBLE);
                    sendGiftRecipientName.setVisibility(View.VISIBLE);
                } else {
                    sendGiftEmailAddress.setVisibility(View.GONE);
                    sendGiftMobileNumber.setVisibility(View.GONE);
                    sendGiftRecipientName.setVisibility(View.GONE);
                    sendGiftEmailAddress.setText("");
                    sendGiftMobileNumber.setText("");
                    sendGiftRecipientName.setText("");
                }
            }
        });

        //SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(OrderDetails.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();
        //ENDING SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
    }

    public void confirmPaymentProcess() {

        if (sendGiftCheckBox.isChecked() && sendGiftRecipientName.getText().toString().trim().isEmpty()) {
            ARCustomToast.showToast(this, "please enter a valid name", Toast.LENGTH_LONG);
        } else if (sendGiftCheckBox.isChecked() && sendGiftEmailAddress.getText().toString().trim().isEmpty()) {
            ARCustomToast.showToast(this, "please enter a valid email address", Toast.LENGTH_LONG);
        } else if (sendGiftCheckBox.isChecked() && !FieldValidation.emailAddressIsValid(sendGiftEmailAddress.getText().toString(), sendGiftEmailAddress)) {
            ARCustomToast.showToast(this, "please enter a valid email address", Toast.LENGTH_LONG);
        } else if (sendGiftCheckBox.isChecked() && sendGiftMobileNumber.getText().toString().isEmpty()) {
            ARCustomToast.showToast(this, "please enter a valid mobile number", Toast.LENGTH_LONG);
        } else if (!tncCheckBox.isChecked() && !isTermConditionAccepted) {
            ARCustomToast.showToast(this, "please accept the terms & condition", Toast.LENGTH_LONG);
        } else {
            // IS USER HAD ALREADY VERIFIED THEIR EMAIL ID-- Uncomment once testing is done, comment billing intent above
            if (preference.getBoolean("IS_KP_USER_EMAIL_VERIFIED", false) || preference.getBoolean("EMAIL_IS_VERIFIED", false)) {
                if (directPaymentScreenFlag.equals("1")) {
                    payNowButtonProcessing();
                } else if (directPaymentScreenFlag.equals("0")) {
//                    if (isAddressFound)
                    if (preference.getBoolean("IS_KP_USER_POSTCODE_DEFINED", false))
                        payNowButtonProcessing();
                    else {
                        Intent billingIntent = new Intent(OrderDetails.this, BillingAddressScreen.class);
                        startActivityForResult(billingIntent, BILLING_ADDRESS_REQUEST_CODE);
                    }
                } else
                    payNowButtonProcessing();

            } else
                startActivityForResult(new Intent(OrderDetails.this, EmailVerificationScreen.class), EMAIL_VERIFICATION_REQUEST_CODE);
        }
    }

    //CALLING THIS METHOD IF WE WANT TO PROCESS FURTHER PAYMENT, AFTER USER IS VERIFIED FROM CM SDK
    //CASE 1: IF USER IS ALREADY VERIFIED CALLING THIS METHOD DIRECTLY FROM 'orderDetailOnClickMethod'
    //CASE 2: IF USER IS NOT VERIFIED AS PER CM SDK THEN OPENING NEW INTENT [VERIFYING CM OTP] AND GETTING STATUS IN onActivityResult()
    private void payNowButtonProcessing() {
        ispaynowPressed = true;
        if (isSuccessfullCouponCase) // COMING HERE AFTER ENTERING AND VALIDATING THE CORRECT COUPON FROM API @ GO BUTTON
        {
            isSuccessfullCouponCase = false;
            apiHitPosition = 5;
            new LoadResponseViaPost(OrderDetails.this, formLockCouponJson(), true).execute("");
        } else {
            finalConfirmCallBeforeProcessing();
        }
    }

    private void finalConfirmCallBeforeProcessing() {
        if (ProjectConstant.TOP_UP_IDENTIFIER == 1) {
            apiHitPosition = 2;
            new LoadResponseViaPost(OrderDetails.this, formAppTxnIDJSON(), true).execute("");
        } else {
            orderDetailLayout.setVisibility(View.GONE);
            orderScreenHeaderLayout.setVisibility(View.GONE);
            pgIntermediateLayout.setVisibility(View.VISIBLE);
            headerTitle.setText("Pay with ");

            try {
                EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_PG_OPTION));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == MOBILE_OTP_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    payNowButtonProcessing();
                }
            }
            if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == EMAIL_VERIFICATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {

                    if (directPaymentScreenFlag.equals("1")) {
                        payNowButtonProcessing();
                    } else if (directPaymentScreenFlag.equals("0")) {
//                        if (isAddressFound)
                        if (preference.getBoolean("IS_KP_USER_POSTCODE_DEFINED", false))
                            payNowButtonProcessing();
                        else {
                            Intent billingIntent = new Intent(OrderDetails.this, BillingAddressScreen.class);
                            startActivityForResult(billingIntent, BILLING_ADDRESS_REQUEST_CODE);
                        }
                    } else
                        payNowButtonProcessing();
                }
            }
            if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == BILLING_ADDRESS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("RESPONSE", false)) {
                    isAddressFound = true;
                    preference.edit().putBoolean("IS_KP_USER_POSTCODE_DEFINED", true).apply();
                    payNowButtonProcessing();
                }
            }
            if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == EDIT_BILLING_ADDRESS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("REFRESH_POSTAL_CODE")) {
                    addressPostalCodeTV.setText("Address : " + data.getStringExtra("REFRESH_POSTAL_CODE"));
                    isAddressFound = true;
                    preference.edit().putBoolean("IS_KP_USER_POSTCODE_DEFINED", true).apply();
                }
            }
        } else if (requestCode == TERM_CONDITION_CODE) {
            if (resultCode == RESULT_OK) {
                isTermConditionAccepted = true;
                tncCheckBox.setChecked(true);
            } else
                isTermConditionAccepted = false;
        }
    }

    //FORMING JSON REQUEST PARAMETERS FOR CONFIRM USER IS ENTERS IN ORDER SCREEN[ApiHitPosition==8]
    private String formConfirmAddressJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KP_GETADDRESS_BY_CONFIRMDETAIL");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("OPERATORCODE", receivedOPCode);
            json.put("SERVICEID", receivedServiceId);
            json.put("COUNTRYCODE", receivedCountryCode);
            json.put("PAYMENTAMT", totalAmountPayable);
            json.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", ""));

            if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_MOBILENUMBER)) {
                if (receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER).contains("N/A"))
                    json.put("SUBSCRIBERNUM", "");
                else
                    json.put("SUBSCRIBERNUM", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER));
            } else {
                json.put("SUBSCRIBERNUM", receivedPayNowDetailHashMap.get("MOBILENUMBER"));
            }
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //FORM PG APP TRANSACTION ID JSON PARAMETERS [ApiHitPosition==2]
    private String formAppTxnIDJSON() {
        try {
            JSONObject json = new JSONObject();

            json.put("APISERVICE", "KPPAYMENTGATEWAYAPPTXNID");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);

            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("SOURCE", "KPAPP");
            json.put("OPABBR", dbReceivedOpABBR);
            if (isVisaChoose) {
                json.put("MOPID", preference.getString("PG_MOP_ID", "18"));
                json.put("PGSOID", preference.getString("PG_SO_ID", "160"));
            } else {
                json.put("MOPID", preference.getString("PG_MOP_ID1", "22"));
                json.put("PGSOID", preference.getString("PG_SO_ID1", "164"));
            }

            /*json.put("APISERVICE", "KPPAYMENTGATEWAYAPPTXNID");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("OPERATORCODE", receivedOPCode);
            json.put("SERVICEID", receivedServiceId);
            json.put("USERNAME", preference.getString("KP_USER_NAME", ""));
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            if (isVisaChoose) {
                json.put("MOPID", preference.getString("PG_MOP_ID", "18"));
                json.put("PGSOID", preference.getString("PG_SO_ID", "160"));
            } else {
                json.put("MOPID", preference.getString("PG_MOP_ID1", "22"));
                json.put("PGSOID", preference.getString("PG_SO_ID1", "164"));
            }
            json.put("PLANDESCRIPTION", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PLANDESCRIPTION));
            json.put("OPERATORNAME", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_OPERATORNAME));
            json.put("PROCESSINGFEE", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PROCESSINGFEE));
            json.put("SERVICEFEE", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_SERVICEFEE));
            json.put("DISCOUNTAMOUNT", "0");
            json.put("PGDISCOUNTAMOUNT", "0");
            json.put("SOURCE", "KPAPP");

            if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_MOBILENUMBER)) {
                if (receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER).contains("N/A"))
                    json.put("MOBILENUMBER", "");
                else
                    json.put("MOBILENUMBER", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER));
            } else
                json.put("MOBILENUMBER", receivedPayNowDetailHashMap.get("MOBILENUMBER"));
            json.put("NETPGAMOUNT", totalAmountPayable);
            json.put("COUNTRYCODE", receivedCountryCode);
            json.put("BROWSER", "BROWSER");
            json.put("OS", "Android");
            json.put("CPNTXNID", couponTxnId);
            json.put("CPNSRNO", couponSrNo);
            json.put("CPNAMOUNT", couponAmount);
            json.put("OPABBR", dbReceivedOpABBR);

            if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.TOPUP_API_DISPLAYAMT))
                json.put("DISPLAYAMOUNT", receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_DISPLAYAMT));
            else
                json.put("DISPLAYAMOUNT", receivedPayNowDetailHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT));

            json.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("RECHARGEAMOUNT", receivedRCAmount);
            json.put("RECHARGECURRENCYAGID", receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
            json.put("DISPLAYCURRENCYAGID", receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
            json.put("PAYMENTCURRENCYAGID", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID));

            //ADDING NEW EXTRA TAGS, FOR DEVICE INFORMATION
            try {
                DeviceAppInformation deviceAppInformation = new DeviceAppInformation(OrderDetails.this);
//                json.put("IPADDRESS", deviceAppInformation.getIpAddress());
                json.put("IPADDRESS", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
                json.put("BRAND", deviceAppInformation.getDeviceBrand());
                json.put("MODEL", deviceAppInformation.getDeviceModel());
                json.put("MANUFACTURER", deviceAppInformation.getDeviceManufacturer());
                json.put("DEVICE", deviceAppInformation.getDevice());
                json.put("OSVERSION", deviceAppInformation.getDeviceOSVersion());
                json.put("SCREENSIZE", deviceAppInformation.getDeviceScreenSize());
                json.put("RESOLUTION", deviceAppInformation.getDeviceResolution());
                json.put("PRODUCT", deviceAppInformation.getDeviceProduct());
                json.put("CELLID", "");

                deviceAppInformation = null;
            } catch (Exception e) {
                e.printStackTrace();
            }*/

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    // FORM MAIN PAYMENT GATEWAY JSON PARAMETERS
    // FORMING JSON FOR GETTING PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEBVIEW
    public String formPaymentGatewayJSON(String apiServiceName, boolean isWebView) {
        try {
            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            json.put("iscart", "NO");
            json.put("os", "Android");

            if (isVisaChoose) {
                json.put("mopid", preference.getString("PG_MOP_ID", "18"));
                json.put("pgsoid", preference.getString("PG_SO_ID", "160"));
            } else {
                json.put("mopid", preference.getString("PG_MOP_ID1", "22"));
                json.put("pgsoid", preference.getString("PG_SO_ID1", "164"));
            }

            json.put("apptransactionid", PG_APP_TXN_ID);
            json.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));

            json.put("browser", "BROWSER");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("sessionid", preference.getString("SESSION_ID", ""));
            else
                json.put("sessionid", ProjectConstant.SESSIONID);
            json.put("source", "KPAPP");
            json.put("username", preference.getString("KP_USER_NAME", ""));
            json.put("userid", preference.getString("KP_USER_ID", "0"));
            json.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("appname", "KWIKPAY");

            if (!isWebView)
                json.put("pgType", PG_TYPE);

            for (int i = 0; i < 1; i++) {
                JSONObject childJsonObject = new JSONObject();

                childJsonObject.put("apiservice", apiServiceName);
                String giftReceiver = "";
                if (sendGiftCheckBox.isChecked())
                    giftReceiver = sendGiftRecipientName.getText().toString() + "|" + sendGiftEmailAddress.getText().toString() + "|" + sendGiftMobileNumber.getText().toString();

                childJsonObject.put("giftreceiver", giftReceiver);
                childJsonObject.put("username", preference.getString("KP_USER_NAME", ""));
                childJsonObject.put("userid", preference.getString("KP_USER_ID", "0"));
                childJsonObject.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));

                if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_MOBILENUMBER)) {
                    if (receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER).contains("N/A"))
                        childJsonObject.put("mobilenumber", "");
                    else
                        childJsonObject.put("mobilenumber", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER));
                } else
                    childJsonObject.put("mobilenumber", receivedPayNowDetailHashMap.get("MOBILENUMBER"));

                childJsonObject.put("rechargeamount", receivedRCAmount);

                if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.TOPUP_API_DISPLAYAMT))
                    childJsonObject.put("displayamount", receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                else
                    childJsonObject.put("displayamount", receivedPayNowDetailHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT));

                childJsonObject.put("netpgamount", totalAmountPayable);
                childJsonObject.put("rechargecurrencyagid", receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
                childJsonObject.put("displaycurrencyagid", receivedPayNowDetailHashMap.get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
                childJsonObject.put("paymentcurrencyagid", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID));
                childJsonObject.put("os", "Android");
                childJsonObject.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
                childJsonObject.put("browser", "BROWSER");

                childJsonObject.put("cpntxnid", couponTxnId);
                childJsonObject.put("cpnamount", couponAmount);
                childJsonObject.put("cpnsrno", couponSrNo);
                childJsonObject.put("discountamount", couponAmount);
                childJsonObject.put("pgdiscountamount", "0");

                childJsonObject.put("serviceid", receivedServiceId);
                childJsonObject.put("operatorcode", receivedOPCode);
                childJsonObject.put("countrycode", receivedCountryCode);
                childJsonObject.put("source", "KPAPP");
                if (isVisaChoose) {
                    childJsonObject.put("mopid", preference.getString("PG_MOP_ID", "18"));
                    childJsonObject.put("pgsoid", preference.getString("PG_SO_ID", "160"));
                } else {
                    childJsonObject.put("mopid", preference.getString("PG_MOP_ID1", "22"));
                    childJsonObject.put("pgsoid", preference.getString("PG_SO_ID1", "164"));
                }
                if (ProjectConstant.SESSIONID.equals(""))
                    childJsonObject.put("sessionid", preference.getString("SESSION_ID", ""));
                else
                    childJsonObject.put("sessionid", ProjectConstant.SESSIONID);
                childJsonObject.put("processingfee", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PROCESSINGFEE));
                childJsonObject.put("servicefee", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_SERVICEFEE));
                childJsonObject.put("operatorproductcode", operatorProductCode);

                jsonArray.put(childJsonObject);
            }
            json.put("kprequest", jsonArray);

//            Log.e("OrderScreen", "Final PG Json " + json.toString());

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //FORM REDEEM COUPON JSON PARAM
    private String formRedeemCouponJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPCPNVALIDATION");
            json.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("PARTYCODE", couponBoxET1.getText().toString().toUpperCase(Locale.getDefault()));
            json.put("OPERATORID", receivedOperatorId);
            json.put("CPNCODE", couponBoxET2.getText().toString().toUpperCase(Locale.getDefault()) + couponBoxET3.getText().toString().toUpperCase(Locale.getDefault()));
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("PAYMENTAMT", couponPaymentAmount);
            json.put("PROCESSINGFEE", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PROCESSINGFEE));
            json.put("SERVICEFEE", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_SERVICEFEE));
            json.put("SERVICEID", receivedServiceId);
            json.put("AGID", receivedPayNowDetailHashMap.get("AGID"));
            json.put("COUNTRYCODE", receivedCountryCode);

            if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_MOBILENUMBER)) {
                if (receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER).contains("N/A"))
                    json.put("MOBILENUMBER", "");
                else
                    json.put("MOBILENUMBER", receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER));
            } else
                json.put("MOBILENUMBER", receivedPayNowDetailHashMap.get("MOBILENUMBER"));


            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    //FORM LOCK COUPON JSON REQUEST PARAMETERS [ApiHitPosition==5]
    private String formLockCouponJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPCPNLOCKSTATUS");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("CPNSERIALNUMBER", couponSrNo);
            json.put("UNIQUEID", couponTxnId);
            json.put("ISLOCK", "Y");

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    @SuppressLint("DefaultLocale")
    public String paymentConverter(String amount1, String amount2) {
        if (amount1.equals(""))
            amount1 = "0.00";
        if (amount2.equals(""))
            amount2 = "0.00";

        try {
            Float floatAmt1 = Float.parseFloat(amount1);
            Float floatAmt2 = Float.parseFloat(amount2);
            floatAmt1 = floatAmt1 + floatAmt2;
            return String.format("%.2f", floatAmt1);
        } catch (Exception e) {
        }
        return "0.00";
    }

    @Override
    public void onRequestComplete(String loadedString) {
//        Log.e("Response", "Order Response " + loadedString);

        // TODO Auto-generated method stub
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 8) {
//                    apiHitPosition=0;
                    // NOTHING TO DO WITH THE RESPONSE OF THIS API... IT IS TRIGGERED TO SEND INFO TO SERVER
                    parseAddressConfirmStatus(loadedString);
                }

                // TO GET TRANSACTION ID AND NATIVE TYPE TAG
                else if (apiHitPosition == 2) {
                    if (parseAppTxnIdDetails(loadedString)) {
                        if (!PG_APP_TXN_ID.equals("") && !PG_TYPE.equals("")) {
                            if (PG_TYPE.equals("NATIVE")) {
                                /*if(isJudoClickedForPayment)
                                {
									isJudoClickedForPayment=false;
									Intent pgIntent=(new Intent(OrderDetails.this,PaymentGatewayPage.class));
									pgIntent.putExtra("PG_JSON_PARAM", formPaymentGatewayJSON("WSGetCotrolAndData",true));
									pgIntent.putExtra("PG_APP_TXN_ID_KEY", PG_APP_TXN_ID);
									startActivity(pgIntent);
								}
								else
								{
									apiHitPosition=3;
									new LoadResponseViaPost(OrderDetails.this, formPaymentGatewayJSON("KPGETPGDETAILS",false)).execute("");
								}*/

                                apiHitPosition = 3;
                                new LoadResponseViaPost(OrderDetails.this, formPaymentGatewayJSON("KPGETPGDETAILS", false)).execute("");

                            } else {
                                Intent pgIntent = (new Intent(OrderDetails.this, PaymentGatewayPage.class));
                                pgIntent.putExtra("PG_JSON_PARAM", formPaymentGatewayJSON("WSGetCotrolAndData", true));
                                pgIntent.putExtra("PG_APP_TXN_ID_KEY", PG_APP_TXN_ID);
                                startActivity(pgIntent);
                            }
                        }
                    }
                }

                // MOVING TOWARDS THE NATIVE PAYMENT GATEWAY
                else if (apiHitPosition == 3) {
                    if (parseRecievedPGDetail(loadedString)) {
//						Log.e("","resp "+loadedString);
                        if (apiPGName.equals("JUDOPG")) {
                            Intent pgIntent = (new Intent(OrderDetails.this, UpdateJUDOPaymentGateway.class));
//							Intent pgIntent=(new Intent(OrderDetails.this,PayPalPaymentScreen.class));
                            pgIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                            startActivity(pgIntent);
                        } else if (apiPGName.equalsIgnoreCase("PAYPAL")) {
                            Intent paypalIntent = new Intent(OrderDetails.this, PayPalPaymentScreen.class);
                            paypalIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                            startActivity(paypalIntent);
                        } else if (apiPGName.equals("COUPONPG")) {
                            Intent couponPgIntent = (new Intent(OrderDetails.this, UpdateJUDOPaymentGateway.class));
                            couponPgIntent.putExtra("NATIVE_PG_COUPON", nativePGDataHashMap);
                            couponPgIntent.putExtra("NATIVE_PG_COUPON2", "COUPON_DATA");
                            startActivity(couponPgIntent);
//						 	new LoadResponseViaPost(OrderDetails.this, judoGetTimerValue.getTimerValueJson(receipt), true).execute("");
                        } else {
                            Toast.makeText(OrderDetails.this, "Payment Gateway not defined", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                // HITTING GO BUTTON TO VALIDATE COUPON
                else if (apiHitPosition == 4) {
                    if (parseCouponDetails(loadedString)) {
                        reedemCouponBoxesLL.setVisibility(View.GONE);
                        couponCheckBox.setVisibility(View.GONE);
//                        withoutCouponLayout.setVisibility(View.GONE);
//                        withCouponLayout.setVisibility(View.VISIBLE);

                        // NEED TO CREATE THE IDS AGAIN OF THE VIEWS AS IT IS IN DIFFERENT XML

                        /*serviceProviderValue = findViewById(R.id.orderDetail1_ServiceProviderTV);
                        mobNumberValue = findViewById(R.id.orderDetail1_MobNumberTV);
                        topUpOptionValue = findViewById(R.id.orderDetail1_TopUpOptionTV);
                        transactionFeeValue = findViewById(R.id.orderDetail1_TransFeeTV);
                        totAmtPayableValue = findViewById(R.id.orderDetail1_TotAmountTV);
                        couponDiscountAmount = findViewById(R.id.orderDetail1_CouponAmountTV);

                        serviceProviderValue.setText(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_OPERATORNAME));
//                        serviceDetailsValue.setText(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PLANDESCRIPTION));
                        mobNumberValue.setText(mobileNumber);

                        String displayCurrencyCode = "", displayAmount = "";

                        if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE))
                            displayCurrencyCode = receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE);
                        else if (receivedPayNowDetailHashMap.containsKey("DISPLAYAMOUNTCURRENCYCODE"))
                            displayCurrencyCode = receivedPayNowDetailHashMap.get("DISPLAYAMOUNTCURRENCYCODE");

                        if (receivedPayNowDetailHashMap.containsKey("DISPLAYAMT") && receivedPayNowDetailHashMap.get("DISPLAYAMT") != null)
                            displayAmount = receivedPayNowDetailHashMap.get("DISPLAYAMT");
                        else if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.TXN_DISPLAYAMOUNT))
                            displayAmount = receivedPayNowDetailHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT);

                        topUpOptionValue.setText(Html.fromHtml(displayCurrencyCode) + " " + displayAmount);
                        transactionFeeValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.TXN_PAYMENTCURRENCYCODE)) + " " + transactionFee);
                        totAmtPayableValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.TXN_PAYMENTCURRENCYCODE)) + " " + totalAmountPayable);
                        couponDiscountAmount.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.TXN_PAYMENTCURRENCYCODE)) + " " + couponAmount);
                    */

                        /*  if (infoIconDisplayFlag.equals("Y"))
                            couponInfoTV.setVisibility(View.VISIBLE);*/
                    }
                }

                // TO LOCK THE COUPON, AFTER VALIDATING SUCCESSFULLY
                else if (apiHitPosition == 5) {
                    if (parseLockCouponResoponse(loadedString)) {
                        finalConfirmCallBeforeProcessing();
                    }
                }

            } else {
                if (apiHitPosition != 8) {
                    if (DeviceNetConnectionDetector.checkDataConnWifiMobile(OrderDetails.this))
                        ARCustomToast.showToast(OrderDetails.this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
                    else
                        ARCustomToast.showToast(OrderDetails.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
                }
            }
        } else {
            if (apiHitPosition != 8) {
                if (DeviceNetConnectionDetector.checkDataConnWifiMobile(OrderDetails.this))
                    ARCustomToast.showToast(OrderDetails.this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
                else
                    ARCustomToast.showToast(OrderDetails.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        }
    }

    // LOGIC FOR PARSING PG APP TRANSACTION ID FROM WEBSERVICE [apiHitPosition==2]
    public boolean parseAppTxnIdDetails(String operatorResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(operatorResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(OrderDetails.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.RESPONSE_MSG_TAG)) {
                if (parentResponseObject.getString(ProjectConstant.RESPONSE_MSG_TAG).equals("SUCCESS")) {
                    if (parentResponseObject.has(ProjectConstant.PG_APP_TXN_ID) && parentResponseObject.has("PGTYPE")) {
                        PG_APP_TXN_ID = parentResponseObject.getString(ProjectConstant.PG_APP_TXN_ID);
                        PG_TYPE = parentResponseObject.getString("PGTYPE");
                    }
                } else {
                    ARCustomToast.showToast(OrderDetails.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                    return false;
                }
            }
        } catch (JSONException e) {
            return false;
        }
        return true;
    }

    //LOGIC FOR PARSING THE RECEIVED PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEB VIEW [apiHitPosition==3]
    public boolean parseRecievedPGDetail(String pgDetails) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(pgDetails);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(OrderDetails.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    nativePGDataHashMap = new HashMap<>();

                    if (parentResponseObject.has("PAYMENTREFERENCE"))
                        nativePGDataHashMap.put("PAYMENTREFERENCE", parentResponseObject.getString("PAYMENTREFERENCE"));
                    if (parentResponseObject.has("JUDOID"))
                        nativePGDataHashMap.put("JUDOID", parentResponseObject.getString("JUDOID"));
                    if (parentResponseObject.has("CONSUMERREFERENCE"))
                        nativePGDataHashMap.put("CONSUMERREFERENCE", parentResponseObject.getString("CONSUMERREFERENCE"));
                    if (parentResponseObject.has("CHECKSUMKEY"))
                        nativePGDataHashMap.put("CHECKSUMKEY", parentResponseObject.getString("CHECKSUMKEY"));
                    if (parentResponseObject.has("ORDERID"))
                        nativePGDataHashMap.put("ORDERID", parentResponseObject.getString("ORDERID"));
                    if (parentResponseObject.has("TOKENPAYREFERENCE"))
                        nativePGDataHashMap.put("TOKENPAYREFERENCE", parentResponseObject.getString("TOKENPAYREFERENCE"));
                    if (parentResponseObject.has("SECRETKEY"))
                        nativePGDataHashMap.put("SECRETKEY", parentResponseObject.getString("SECRETKEY"));
                    if (parentResponseObject.has("TOKEN"))
                        nativePGDataHashMap.put("TOKEN", parentResponseObject.getString("TOKEN"));
                    if (parentResponseObject.has("PGNAME"))
                        apiPGName = parentResponseObject.getString("PGNAME");

                    nativePGDataHashMap.put("TOTALAMOUNTPAYABLE", totalAmountPayable);
                    nativePGDataHashMap.put("PGNAME", apiPGName);
                    nativePGDataHashMap.put("APPTXNID", PG_APP_TXN_ID);

                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(OrderDetails.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;

    }

    //LOGIC FOR PARSING THE COUPON RECEIVED DATA [apiHitPosition==4]
    public boolean parseCouponDetails(String couponResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(couponResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(OrderDetails.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has("COUPONCODE"))
                        couponCode = parentResponseObject.getString("COUPONCODE");
                    if (parentResponseObject.has(ProjectConstant.CART_CPNSRNO))
                        couponSrNo = parentResponseObject.getString(ProjectConstant.CART_CPNSRNO);
                    if (parentResponseObject.has("CPNAMT"))
                        couponAmount = parentResponseObject.getString("CPNAMT");
                    if (parentResponseObject.has("PGAMT"))
                        totalAmountPayable = parentResponseObject.getString("PGAMT");
                    if (parentResponseObject.has("INFOICONDISPLAYFLAG"))
                        infoIconDisplayFlag = parentResponseObject.getString("INFOICONDISPLAYFLAG");
                    if (parentResponseObject.has("INFOICONDISPLAYMSG"))
                        infoIconDisplayMsg = parentResponseObject.getString("INFOICONDISPLAYMSG");
                    if (parentResponseObject.has("COUPONTXNID"))
                        couponTxnId = parentResponseObject.getString("COUPONTXNID");

                    isSuccessfullCouponCase = true;
                    if (receivedPayNowDetailHashMap.containsKey(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE))
                        couponDiscountAmount.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE)) + " " + couponAmount);
                    /*if (receivedPayNowDetailHashMap.containsKey("DISPLAYAMOUNTCURRENCYCODE"))
                        couponDiscountAmount.setText(Html.fromHtml(receivedPayNowDetailHashMap.get("DISPLAYAMOUNTCURRENCYCODE")) + " " + couponAmount);*/

                    totalAmountPayable = paymentConverter(transactionFee, totalAmountPayable);
                    totAmtPayableValue.setText(Html.fromHtml(receivedPayNowDetailHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE)) + " " + totalAmountPayable);

                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(OrderDetails.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;

    }

    //*******************************************************************************************//
    // LOGIC FOR PARSING THE RESPONSE OF ADDRESS CONFIRMATION                                    //
    //*******************************************************************************************//
    private void parseAddressConfirmStatus(String response) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(response);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(OrderDetails.this, "No Response Tag", Toast.LENGTH_LONG);
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has("DIRECT_PAYMENT_SCREEN")) {
                        directPaymentScreenFlag = parentResponseObject.optString("DIRECT_PAYMENT_SCREEN");

                        SharedPreferences.Editor prefEditor = preference.edit();
                        if (parentResponseObject.has("IS_POSTCODE_DEFINED") && !parentResponseObject.getString("IS_POSTCODE_DEFINED").isEmpty())
                            prefEditor.putBoolean("IS_KP_USER_POSTCODE_DEFINED", Boolean.parseBoolean(parentResponseObject.optString("IS_POSTCODE_DEFINED")));
                        if (parentResponseObject.has("IS_EMAIL_VERIFIED") && !parentResponseObject.getString("IS_EMAIL_VERIFIED").isEmpty())
                            prefEditor.putBoolean("IS_KP_USER_EMAIL_VERIFIED", Boolean.parseBoolean(parentResponseObject.optString("IS_EMAIL_VERIFIED")));
                        prefEditor.apply();

                        if (directPaymentScreenFlag.equals("0")) {
                            // IF FLAG=1: ADDRESS FOUND IN THE D.B.
                            // IF FLAG=0: ADDRESS NOT FOUND IN THE D.B.
                            if (parentResponseObject.optString("ADDRESSRESPONSECODE").equals("1")) {
                                addressLayout.setVisibility(View.VISIBLE);
                                addressPostalCodeTV.setText("Address : " + parentResponseObject.optString("POSTALCODE"));
                                isAddressFound = true;
                            }
                        }
                    }

                } else {
                    if (parentResponseObject.has("DESCRIPTION"))
                        ARCustomToast.showToast(OrderDetails.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (ispaynowPressed) {
            ispaynowPressed = false;
            orderDetailLayout.setVisibility(View.VISIBLE);
            orderScreenHeaderLayout.setVisibility(View.VISIBLE);
            pgIntermediateLayout.setVisibility(View.GONE);
            headerTitle.setText("Your Order");
        } else {
            super.onBackPressed();
        }
    }

    //LOGIC FOR PARSING THE LOCK COUPON RESPONSE [apiHitPosition==5]
    public boolean parseLockCouponResoponse(String deleteTxnResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(deleteTxnResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(OrderDetails.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    return true;
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(OrderDetails.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
