package com.uk.recharge.kwikpay.kwikcharge.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.uk.recharge.kwikpay.R;

/**
 * Created by Ashu Rajput on 10/4/2017.
 */

public class CustomDialog extends DialogFragment {

    private EditText locationIdET;
    private LocationIdListener listener;
    private boolean showAddressDialog = false;
    private boolean isComingFromGamingScreen = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (LocationIdListener) context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.custom_dialog, null);
        locationIdET = view.findViewById(R.id.locationIdET);
        TextView chargerMessage = view.findViewById(R.id.chargerIdMessage);
        TextView prefixNumber = view.findViewById(R.id.prefixNumber);
        LinearLayout enterChargerIdLayout = view.findViewById(R.id.enterChargerIdLayout);
        Button okButton = view.findViewById(R.id.dialogButtonOK);
        Button cancelButton = view.findViewById(R.id.dialogButtonCancel);

        Bundle bundle = getArguments();
        if (bundle != null) {

            if (bundle.containsKey("IsComingFromGamingScreen")) {
                chargerMessage.setText("Enter Machine Number");
                prefixNumber.setVisibility(View.GONE);
                isComingFromGamingScreen = true;
            }else if (bundle.containsKey("IsComingFromKV")) {
                chargerMessage.setText("Enter Machine Number");
                prefixNumber.setVisibility(View.GONE);
                isComingFromGamingScreen = true;
            } else {
                if (bundle.containsKey("ShowAddressDialog")) {
                    showAddressDialog = bundle.getBoolean("ShowAddressDialog", false);
                    enterChargerIdLayout.setVisibility(View.GONE);
                    if (bundle.containsKey("ButtonsText")) {
                        okButton.setText("Proceed");
                        cancelButton.setText("Cancel");
                        cancelButton.setVisibility(View.GONE);
                    } else {
                        okButton.setText("Yes");
                        cancelButton.setText("No");
                    }
                }
                if (bundle.containsKey("Charger_Address")) {
                    chargerMessage.setText(bundle.getString("Charger_Address"));
                }
            }
        }
        try {
            if (!showAddressDialog) {
                locationIdET.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        } catch (Exception e) {
        }

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isComingFromGamingScreen) {
                    if (locationIdET.getText().toString().isEmpty() || locationIdET.getText().toString().trim().equals("")) {
                        Toast.makeText(getActivity(), "please enter valid machine number", Toast.LENGTH_LONG).show();
                    } else {
                        listener.getLocationId(locationIdET.getText().toString());
                        hideKeyboard(v);
                        dismiss();
                    }
                } else {
                    if (showAddressDialog) {
                        listener.getLocationId("");
                        dismiss();
                    } else {
                        if (locationIdET.getText().toString().isEmpty() || locationIdET.getText().toString().trim().equals("")) {
                            Toast.makeText(getActivity(), "please enter valid location ID", Toast.LENGTH_LONG).show();
                        } else {
                            listener.getLocationId(locationIdET.getText().toString());
                            hideKeyboard(v);
                            dismiss();
                        }
                    }
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                dismiss();
            }
        });

        builder.setView(view);
        return builder.create();
    }

    private void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public interface LocationIdListener {
        void getLocationId(String locationID);
    }
}
