package com.uk.recharge.kwikpay.gamingarcade.payments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.PaymentGatewayPage;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.gamingarcade.models.GameMachineInfoMO;
import com.uk.recharge.kwikpay.nativepg.PayPalPaymentScreen;
import com.uk.recharge.kwikpay.nativepg.UpdateJUDOPaymentGateway;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.GamingInfoSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 7/9/2018.
 */

public class GamingPaymentOptions extends AppCompatActivity {

    private boolean isVisaChoose = false;
    private String pgAppTxnId, pgType;
    private SharedPreferences preference;
    private TextView maximumAuthorizedAmountTV;
    private String apiPGName = "";
    private GameMachineInfoMO gameMachineInfoMO = null;
    private HashMap<String, String> nativePGDataHashMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_payment_options);
        gameMachineInfoMO = GamingInfoSingleton.getInstance().getGameMachineInfoMO();
        setResourceIds();
    }

    private void setResourceIds() {

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        findViewById(R.id.processingFeesMessage).setVisibility(View.VISIBLE);
        maximumAuthorizedAmountTV = findViewById(R.id.maximumAuthorizedAmountTV);
        maximumAuthorizedAmountTV.setVisibility(View.VISIBLE);
//        findViewById(R.id.pgIntermediateScreenId).setVisibility(View.VISIBLE);
//        findViewById(R.id.kcPaymentNote).setVisibility(View.VISIBLE);
        findViewById(R.id.paypal_PG_Button).setOnClickListener(onClickListener);
        findViewById(R.id.judo_PG_Button).setOnClickListener(onClickListener);
        ((TextView) findViewById(R.id.mainHeaderTitleName)).setText("Pay with ");

        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        try {
            maximumAuthorizedAmountTV.setText("Maximum amount to be authorised " + Utility.fromHtml("\u00a3") +
                    gameMachineInfoMO.getPricingOptionsMOList().get(gameMachineInfoMO.getSelectedPosition()).getPlanAmount());
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewID = v.getId();
            if (viewID == R.id.paypal_PG_Button)
                isVisaChoose = false;
            else if (viewID == R.id.judo_PG_Button)
                isVisaChoose = true;
            callGetPaymentTransactionIdAPI();
        }
    };

    private void callGetPaymentTransactionIdAPI() {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "KPPAYMENTGATEWAYAPPTXNID");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);

            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("SOURCE", "KPAPP");
            json.put("OPABBR", gameMachineInfoMO.getOperatorCode());
            if (isVisaChoose) {
                json.put("MOPID", preference.getString("PG_MOP_ID", "18"));
                json.put("PGSOID", preference.getString("PG_SO_ID", "160"));
            } else {
                json.put("MOPID", preference.getString("PG_MOP_ID1", "22"));
                json.put("PGSOID", preference.getString("PG_SO_ID1", "164"));
            }

        } catch (Exception e) {
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has(ProjectConstant.RESPONSE_MSG_TAG)) {
                                if (childJson.getString(ProjectConstant.RESPONSE_MSG_TAG).equals("SUCCESS")) {
                                    if (childJson.has(ProjectConstant.PG_APP_TXN_ID) && childJson.has("PGTYPE")) {
                                        pgAppTxnId = childJson.getString(ProjectConstant.PG_APP_TXN_ID);
                                        pgType = childJson.getString("PGTYPE");

                                        if (!pgAppTxnId.equals("") && !pgType.equals("")) {
                                            if (pgType.equals("NATIVE")) {
                                                callNativePGAPI();
                                            } else {

                                                preference.edit().putString("GAMING_TXN_ID", pgAppTxnId).apply();
                                                /*startActivity(new Intent(GamingPaymentOptions.this, GamingSuccessFailure.class)
                                                        .putExtra("PG_APP_TXN_ID_KEY", pgAppTxnId));*/

                                                Intent pgIntent = (new Intent(GamingPaymentOptions.this, PaymentGatewayPage.class));
                                                pgIntent.putExtra("PG_JSON_PARAM", formPaymentGatewayJSON("WSGetCotrolAndData", true));
                                                pgIntent.putExtra("PG_APP_TXN_ID_KEY", pgAppTxnId);
                                                startActivity(pgIntent);
                                                finish();
                                            }
                                        }
                                    }
                                } else {
                                    ARCustomToast.showToast(GamingPaymentOptions.this, childJson.optString("DESCRIPTION"));
                                }
                            }
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(GamingPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(GamingPaymentOptions.this))
                        ARCustomToast.showToast(GamingPaymentOptions.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(GamingPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                ARCustomToast.showToast(GamingPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void callNativePGAPI() {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(formPaymentGatewayJSON("KPGETPGDETAILS", false)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has(ProjectConstant.API_RESPONSE_CODE)) {
                                if (childJson.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                                    nativePGDataHashMap = new HashMap<>();
                                    nativePGDataHashMap.put("PAYMENTREFERENCE", childJson.optString("PAYMENTREFERENCE"));
                                    nativePGDataHashMap.put("JUDOID", childJson.optString("JUDOID"));
                                    nativePGDataHashMap.put("CONSUMERREFERENCE", childJson.optString("CONSUMERREFERENCE"));
                                    nativePGDataHashMap.put("CHECKSUMKEY", childJson.optString("CHECKSUMKEY"));
                                    nativePGDataHashMap.put("ORDERID", childJson.optString("ORDERID"));
                                    nativePGDataHashMap.put("TOKENPAYREFERENCE", childJson.optString("TOKENPAYREFERENCE"));
                                    nativePGDataHashMap.put("SECRETKEY", childJson.optString("SECRETKEY"));
                                    nativePGDataHashMap.put("TOKEN", childJson.optString("TOKEN"));
                                    apiPGName = childJson.optString("PGNAME");

//                                    NOTE: made changes here to get Max AMOUNT for the gaming
                                   /* if (receivedModel != null)
                                        nativePGDataHashMap.put("TOTALAMOUNTPAYABLE", receivedModel.getRecentMaxAmount());*/

                                    nativePGDataHashMap.put("PGNAME", apiPGName);
                                    nativePGDataHashMap.put("APPTXNID", pgAppTxnId);

                                    if (apiPGName.equals("JUDOPG")) {
                                        Intent pgIntent = (new Intent(GamingPaymentOptions.this, UpdateJUDOPaymentGateway.class));
                                        pgIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                                        startActivity(pgIntent);
                                    } else if (apiPGName.equalsIgnoreCase("PAYPAL")) {
                                        Intent paypalIntent = new Intent(GamingPaymentOptions.this, PayPalPaymentScreen.class);
                                        paypalIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                                        startActivity(paypalIntent);
                                    } else if (apiPGName.equals("COUPONPG")) {
                                        Intent couponPgIntent = (new Intent(GamingPaymentOptions.this, UpdateJUDOPaymentGateway.class));
                                        couponPgIntent.putExtra("NATIVE_PG_COUPON", nativePGDataHashMap);
                                        couponPgIntent.putExtra("NATIVE_PG_COUPON2", "COUPON_DATA");
                                        startActivity(couponPgIntent);
                                    } else {
                                        Toast.makeText(GamingPaymentOptions.this, "Payment Gateway not defined", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    if (childJson.has("DESCRIPTION")) {
                                        ARCustomToast.showToast(GamingPaymentOptions.this, childJson.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(GamingPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(GamingPaymentOptions.this))
                        ARCustomToast.showToast(GamingPaymentOptions.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(GamingPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                ARCustomToast.showToast(GamingPaymentOptions.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });

    }

    // FORMING JSON FOR GETTING PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEBVIEW
    public String formPaymentGatewayJSON(String apiServiceName, boolean isWebView) {
        try {
            int selectedPosition = gameMachineInfoMO.getSelectedPosition();
            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            json.put("iscart", "NO");
            json.put("os", "Android");

            if (isVisaChoose) {
                json.put("mopid", preference.getString("PG_MOP_ID", "18"));
                json.put("pgsoid", preference.getString("PG_SO_ID", "160"));
            } else {
                json.put("mopid", preference.getString("PG_MOP_ID1", "22"));
                json.put("pgsoid", preference.getString("PG_SO_ID1", "164"));
            }
            json.put("apptransactionid", pgAppTxnId);
            json.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
            json.put("browser", "BROWSER");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("sessionid", preference.getString("SESSION_ID", ""));
            else
                json.put("sessionid", ProjectConstant.SESSIONID);
            json.put("source", "KPAPP");
            json.put("username", preference.getString("KP_USER_NAME", ""));
            json.put("userid", preference.getString("KP_USER_ID", "0"));
            json.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("appname", "KWIKPAY");
            json.put("gammingcredit", gameMachineInfoMO.getPricingOptionsMOList().get(selectedPosition).getPlanCredit());

            if (!isWebView)
                json.put("pgType", pgType);

            for (int i = 0; i < 1; i++) {

                JSONObject childJsonObject = new JSONObject();

                childJsonObject.put("apiservice", apiServiceName);
                childJsonObject.put("username", preference.getString("KP_USER_NAME", ""));
                childJsonObject.put("userid", preference.getString("KP_USER_ID", "0"));
                childJsonObject.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));
                childJsonObject.put("os", "Android");
                childJsonObject.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
                childJsonObject.put("browser", "BROWSER");
                childJsonObject.put("cpntxnid", "0");
                childJsonObject.put("cpnamount", "0");
                childJsonObject.put("cpnsrno", "0");
                childJsonObject.put("source", "KPAPP");
                childJsonObject.put("discountamount", "0");
                childJsonObject.put("pgdiscountamount", "0");
                if (isVisaChoose) {
                    childJsonObject.put("mopid", preference.getString("PG_MOP_ID", "18"));
                    childJsonObject.put("pgsoid", preference.getString("PG_SO_ID", "160"));
                } else {
                    childJsonObject.put("mopid", preference.getString("PG_MOP_ID1", "22"));
                    childJsonObject.put("pgsoid", preference.getString("PG_SO_ID1", "164"));
                }
                if (ProjectConstant.SESSIONID.equals(""))
                    childJsonObject.put("sessionid", preference.getString("SESSION_ID", ""));
                else
                    childJsonObject.put("sessionid", ProjectConstant.SESSIONID);

                childJsonObject.put("mobilenumber", "NA");
                childJsonObject.put("rechargeamount", gameMachineInfoMO.getPricingOptionsMOList().get(selectedPosition).getPlanAmount());
                childJsonObject.put("displayamount", gameMachineInfoMO.getPricingOptionsMOList().get(selectedPosition).getPlanAmount());
                childJsonObject.put("netpgamount", calculateNetPGAmount(selectedPosition));
                childJsonObject.put("rechargecurrencyagid", "42");
                childJsonObject.put("displaycurrencyagid", "42");
                childJsonObject.put("paymentcurrencyagid", "42");
                childJsonObject.put("countrycode", "UNITEDKINGDOM");
//                childJsonObject.put("serviceid", gameMachineInfoMO.getServiceID());
                childJsonObject.put("serviceid", "7");
//                childJsonObject.put("operatorcode", gameMachineInfoMO.getOperatorCode());
                childJsonObject.put("operatorcode", "GAMMING");
                childJsonObject.put("processingfee", gameMachineInfoMO.getPricingOptionsMOList().get(selectedPosition).getProcessingFee());
                childJsonObject.put("servicefee", gameMachineInfoMO.getPricingOptionsMOList().get(selectedPosition).getServiceFee());
                childJsonObject.put("machinecode", gameMachineInfoMO.getMachineNumber());
                childJsonObject.put("operatorproductcode", gameMachineInfoMO.getOperatorCode());
//                childJsonObject.put("operatorproductcode", "GAMMING");
                childJsonObject.put("giftreceiver", "||");
                jsonArray.put(childJsonObject);
            }
            json.put("kprequest", jsonArray);

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String calculateNetPGAmount(int position) {
        String convertedValues = "0.00";
        try {
            String rcAmount = gameMachineInfoMO.getPricingOptionsMOList().get(position).getPlanAmount();
            String serviceFeeAmount = gameMachineInfoMO.getPricingOptionsMOList().get(position).getServiceFee();
            if (rcAmount != null && !rcAmount.isEmpty() && serviceFeeAmount != null && !serviceFeeAmount.isEmpty()) {
                Double totalAmount = Double.parseDouble(rcAmount) + Double.parseDouble(serviceFeeAmount);
                DecimalFormat decimalFormat = new DecimalFormat("#.##");
                convertedValues = decimalFormat.format(totalAmount);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }

        return convertedValues;
    }

}
