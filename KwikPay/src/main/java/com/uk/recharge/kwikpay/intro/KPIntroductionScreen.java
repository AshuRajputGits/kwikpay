package com.uk.recharge.kwikpay.intro;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.R;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class KPIntroductionScreen extends FragmentActivity {

    SharedPreferences preference;
    private TextView proceedToHomeScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.kp_introduction_screen);

//		mViewPager = findViewById(R.id.viewpager);
//		dotRadioGroup=findViewById(R.id.dotsRadioGroup);
        proceedToHomeScreen = findViewById(R.id.introScreenSkipButton);
        AutoScrollViewPager mViewPager = findViewById(R.id.viewPager);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        IntroScreenAdapter adsViewPagerAdapter = new IntroScreenAdapter(this);
        mViewPager.setAdapter(adsViewPagerAdapter);

		/*mViewPager.setAdapter(new IntroAdapter(getSupportFragmentManager()));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
//				dotRadioGroup.check(dotRadioGroup.getChildAt(position).getId());
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});*/

        proceedToHomeScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                SharedPreferences.Editor editor = preference.edit();
                editor.putBoolean("IS_INTRODUCTION_COMPLETE", true).apply();

                startActivity(new Intent(KPIntroductionScreen.this, HomeScreen.class));
                KPIntroductionScreen.this.finish();
            }
        });

    }

	/*public class IntroAdapter extends FragmentPagerAdapter {

		public IntroAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			
			*//*Fragment fragment=null;
			if(position==0)
				fragment=new IntroFragmentOne();
			if(position==1)
				fragment=new IntroFragmentTwo();
			if(position==2)
				fragment=new IntroFragmentThree();
			if(position==3)
				fragment=new IntroFragmentFour();
			if(position==4)
				fragment=new IntroFragmentFive();
			
			return fragment;*//*

			return KpIntroductionFragment.newInstance(position);
			
		}

		@Override
		public int getCount() {
			return 5;
		}
	}*/
}
