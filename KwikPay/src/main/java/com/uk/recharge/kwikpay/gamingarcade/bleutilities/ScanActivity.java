package com.uk.recharge.kwikpay.gamingarcade.bleutilities;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;

public class ScanActivity extends AppCompatActivity {

    private static final String TAG = ScanActivity.class.getSimpleName();
    private static final long SCAN_TIMEOUT_MS = 10_000;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_PERMISSION_LOCATION = 1;
    private boolean mScanning;
    private final BluetoothLeScannerCompat mScanner = BluetoothLeScannerCompat.getScanner();
    private boolean isComingAfterPairingFlag = false;
    private String macAddressAfterPairing = "";
    private final Handler mStopScanHandler = new Handler();

    private final Runnable mStopScanRunnable = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getApplicationContext(), "No devices found", Toast.LENGTH_SHORT).show();
            stopLeScan();
        }
    };

    private final ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            // We scan with report delay > 0. This will never be called.
            Log.i(TAG, "onScanResult: " + result.getDevice().getAddress());
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            Log.i(TAG, "onBatchScanResults: " + results.toString());

            if (!results.isEmpty()) {
                parseBLEScanResult(results, "D8:80:39:F8:38:B6");
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.w(TAG, "Scan failed: " + errorCode);
            stopLeScan();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.scan_activity);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.scan, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(R.layout.scan_indeterminate_progress);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                startLeScan();
                break;
            case R.id.menu_stop:
                stopLeScan();
                break;
        }
        return true;
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, "You must turn Bluetooth on, to use this app", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_LOCATION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission accepted");
        } else {
            Toast.makeText(this, "You must grant the location permission.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isComingAfterPairingFlag)
            prepareForScan();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLeScan();
    }

    private BluetoothAdapter btAdapter = null;

    private void prepareForScan() {
        if (isBleSupported()) {
            // Ensures Bluetooth is enabled on the device
            BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            btAdapter = btManager.getAdapter();
            if (btAdapter.isEnabled()) {
                // Prompt for runtime permission
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    startLeScan();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_LOCATION);
                }
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        } else {
            Toast.makeText(this, "BLE is not supported", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private boolean isBleSupported() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    private void startLeScan() {
        mScanning = true;

        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setReportDelay(1000)
                .build();
        List<ScanFilter> filters = new ArrayList<>();
//        filters.add(new ScanFilter.Builder().setServiceUuid(new ParcelUuid(SERVICE_UUID)).build());
        mScanner.startScan(filters, settings, mScanCallback);

        // Stops scanning after a pre-defined scan period.
        mStopScanHandler.postDelayed(mStopScanRunnable, SCAN_TIMEOUT_MS);

//        invalidateOptionsMenu();
    }

    private void stopLeScan() {
        if (mScanning) {
            mScanning = false;

            mScanner.stopScan(mScanCallback);
            mStopScanHandler.removeCallbacks(mStopScanRunnable);

            invalidateOptionsMenu();
        }
    }

    private void parseBLEScanResult(List<ScanResult> scanResultList, String bleMacToSearch) {
        if (scanResultList != null && scanResultList.size() > 0) {
            try {
                boolean isSearchableBLEFound = false;

                for (ScanResult scanResult : scanResultList) {
                    Log.e(TAG, "Coming to search bleFound " + scanResult.getDevice() + " BLE ToSearch " + bleMacToSearch);

                    if (scanResult.getDevice().getAddress().equals(bleMacToSearch)) {
                        Log.e(TAG, "bleFounded UUID " + scanResult.getDevice().getUuids());
                        isSearchableBLEFound = true;
                        break;
                    }
                }
                if (isSearchableBLEFound) {
                    Log.e(TAG, "Great!! scanned BLE is found... Hurrey ");
                    stopLeScan();
//                    automaticallyPairBLE();
                    startConnectingToBLE(bleMacToSearch);
//                    startInteractActivity(bleMacToSearch);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startInteractActivity(String bleMacAddress) {
        Intent intent = new Intent(this, InteractActivity.class);
        intent.putExtra(InteractActivity.EXTRA_DEVICE_ADDRESS, bleMacAddress);
        startActivity(intent);
        finish();
    }

    private FinalBluetoothChatService mService;

    private void startConnectingToBLE(final String macAddress) {
        Log.e("macAddress:", macAddress);

        mService = new FinalBluetoothChatService(this, this.mHandler);
        BLEConnector bleConnector = new BLEConnector(this, macAddress, new BLEInterface() {
            public void onConnected(BluetoothDevice device) {
                Log.e("Connected in SDK:", device.getName());
                isComingAfterPairingFlag = true;
                macAddressAfterPairing = macAddress;
                mService.connect(device);
            }

            @Override
            public void onBLEAlreadyPaired(BluetoothDevice device) {
                Log.e("BLEPaired", "Device already Paired: " + device.getName());
                startInteractActivity(macAddress);
            }
        });
        bleConnector.startScanning();
    }

    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            byte[] send;
            switch (msg.what) {
                case 1:
                    switch (msg.arg1) {
                        case 0:
                        case 1:
//                            ++VendekinSdk.this.bluetoothIssue;
//                            Log.e("BluetoothIssue", String.valueOf(VendekinSdk.this.bluetoothIssue));
                            return;
                        case 2:
                            Log.e("BluetoothIssue", "Connecting");
                            return;
                        case 3:
//                            VendekinSdk.this.bluetoothIssue = 4;
                            Log.e("BluetoothIssue", "connected");

                            if (macAddressAfterPairing != null && !macAddressAfterPairing.isEmpty())
                                startInteractActivity(macAddressAfterPairing);

                            /*VendekinSdk.this.isQrScanned = true;
                            (new Handler()).postDelayed(new Runnable() {
                                public void run() {
                                    Log.e("DEX:", VendekinSdk.DEX_STRING);
                                    VendekinSdk.this.getProductInfo();
                                }
                            }, 4000L);
                            VendekinSdk.this.isConnectedToVendekin = true;
                            if(VendekinSdk.this.mService.getState() != 3) {
                                return;
                            }

                            if(VendekinSdk.this.codeName.length() > 0) {
                                Log.e("CODE:", VendekinSdk.this.codeName);
                                send = VendekinSdk.this.codeName.getBytes();
                                VendekinSdk.this.mService.write(send);
                            }*/

                            return;
                        default:
                            return;
                    }
                case 2:
                    Log.e("AT", "MESSAGE_READ");
                    send = (byte[]) ((byte[]) msg.obj);
                    String string = new String(send, 0, msg.arg1);
                    Log.e("FeedBackString", string);
                case 3:
                case 4:
                case 5:
            }
        }
    };

    public void disconnect() {
        if (btAdapter != null && btAdapter.isEnabled()) {
            this.btAdapter.disable();
        }

        if (Build.VERSION.SDK_INT >= 23) {
            try {
                if (this.mService != null) {
                    this.mService.stop();
                }
            } catch (Exception var2) {
                var2.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disconnect();
    }
}
