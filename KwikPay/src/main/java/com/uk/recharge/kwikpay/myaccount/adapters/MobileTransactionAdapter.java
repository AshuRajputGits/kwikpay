package com.uk.recharge.kwikpay.myaccount.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.myaccount.MyTransactionSingleton;
import com.uk.recharge.kwikpay.myaccount.MyTransactionViewDetails;
import com.uk.recharge.kwikpay.myaccount.MyTransactionsMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 23-12-2017.
 */

public class MobileTransactionAdapter extends RecyclerView.Adapter<MobileTransactionAdapter.MobileTransactionViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<MyTransactionsMO> mobileTransactionList = null;
    private Context context = null;

    public MobileTransactionAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        mobileTransactionList = MyTransactionSingleton.getInstance().getMobileTransactionList();
        this.context = context;
    }

    @Override
    public MobileTransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.my_transactions_row, parent, false);
        return new MobileTransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MobileTransactionViewHolder holder, int position) {
        holder.transactionRCAmount.setText(Utility.fromHtml(mobileTransactionList.get(position).getTransactionDisplayCurrencyCode())
                + mobileTransactionList.get(position).getTransactionDisplayAmount());
        if (mobileTransactionList.get(position).getTransactionPaymentStatus().equalsIgnoreCase("SUCCESSFUL")) {
            holder.transactionRechargeStatus.setText(mobileTransactionList.get(position).getTransactionRechargeStatus());
        } else {
            holder.transactionRechargeStatus.setText(mobileTransactionList.get(position).getTransactionPaymentStatus());
            holder.transactionRechargeStatus.setTextColor(Color.RED);
        }

//        holder.transactionRechargeStatus.setText(mobileTransactionList.get(position).getTransactionRechargeStatus());
        holder.transactionRechargeType.setText(mobileTransactionList.get(position).getTransactionOperatorName());
        holder.transactionDateTime.setText(mobileTransactionList.get(position).getTransactionDateTime());
        holder.transactionOrderID.setText("Order No " + mobileTransactionList.get(position).getTransactionOrderId());

        try {
            Glide.with(context).load(mobileTransactionList.get(position).getImageUrl())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(holder.operatorImages);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (mobileTransactionList != null && mobileTransactionList.size() > 0)
            return mobileTransactionList.size();
        return 0;
    }

    public class MobileTransactionViewHolder extends RecyclerView.ViewHolder {
        TextView transactionRCAmount;
        TextView transactionRechargeStatus;
        TextView transactionRechargeType;
        TextView transactionDateTime;
        TextView transactionOrderID;
        ImageView operatorImages;

        public MobileTransactionViewHolder(View itemView) {
            super(itemView);
            transactionRCAmount = itemView.findViewById(R.id.transactionRCAmount);
            transactionRechargeStatus = itemView.findViewById(R.id.transactionRechargeStatus);
            transactionRechargeType = itemView.findViewById(R.id.transactionRechargeType);
            transactionDateTime = itemView.findViewById(R.id.transactionDateTime);
            transactionOrderID = itemView.findViewById(R.id.transactionOrderID);
            operatorImages = itemView.findViewById(R.id.operatorImages);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, MyTransactionViewDetails.class)
                            .putExtra("SELECTED_TRANSACTION", mobileTransactionList.get(getLayoutPosition())));
                }
            });
        }
    }

}
