package com.uk.recharge.kwikpay.adapters.catering;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.catering.models.CateringMerchantDetailMO;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 11/3/2018.
 */

public class CateringMerchantAdapter extends RecyclerView.Adapter<CateringMerchantAdapter.CateringMerchantViewHolder> {

    private LayoutInflater layoutInflater = null;
    private Context context;
    private CateringMerchantSelectListener listener;
    private ArrayList<CateringMerchantDetailMO> merchantDetailMOArrayList;

    public CateringMerchantAdapter(Context context, ArrayList<CateringMerchantDetailMO> merchantDetailMOArrayList,
                                   CateringMerchantSelectListener listener) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.merchantDetailMOArrayList = merchantDetailMOArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CateringMerchantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.game_service_row, parent, false);
        return new CateringMerchantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CateringMerchantViewHolder holder, int position) {

        try {
            Glide.with(context)
                    .load(merchantDetailMOArrayList.get(position).getMerchantLogoUrls())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(holder.merchantLogo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.merchantName.setText(merchantDetailMOArrayList.get(position).getMerchantName());
    }

    @Override
    public int getItemCount() {
        if (merchantDetailMOArrayList != null && merchantDetailMOArrayList.size() > 0)
            return merchantDetailMOArrayList.size();
        return 0;
    }

    public class CateringMerchantViewHolder extends RecyclerView.ViewHolder {

        private ImageView merchantLogo;
        private TextView merchantName;

        public CateringMerchantViewHolder(View itemView) {
            super(itemView);
            merchantLogo = itemView.findViewById(R.id.gameServiceLogo);
            merchantName = itemView.findViewById(R.id.gameServiceDescription);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onSelectingMerchant(merchantDetailMOArrayList.get(getLayoutPosition()).getLocationNumber());
                }
            });
        }
    }

    public interface CateringMerchantSelectListener {
        void onSelectingMerchant(String merchantLocationNumber);
    }

}
