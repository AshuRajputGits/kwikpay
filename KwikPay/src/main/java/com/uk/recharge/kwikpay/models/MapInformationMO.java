package com.uk.recharge.kwikpay.models;

/**
 * Created by Ashu Rajput on 9/24/2017.
 */

public class MapInformationMO {
    private String mapLatitude;
    private String mapLongitude;

    public String getMapLatitude() {
        return mapLatitude;
    }

    public void setMapLatitude(String mapLatitude) {
        this.mapLatitude = mapLatitude;
    }

    public String getMapLongitude() {
        return mapLongitude;
    }

    public void setMapLongitude(String mapLongitude) {
        this.mapLongitude = mapLongitude;
    }
}
