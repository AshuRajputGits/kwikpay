package com.uk.recharge.kwikpay.services;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.uk.recharge.kwikpay.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Ashu Rajput on 24-04-2017.
 */

public class SendEmailForVerificationService extends JobIntentService {

    private SharedPreferences preferences = null;
    private boolean isComingFromKPLoginScreen = false;

   /* public SendEmailForVerificationService() {
        super("SendEmailForVerificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.e("EmailBG", "AboutTo start service is NOW started");

        URL downloadUrl;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        String finalResponse = "";
        try {
            if (intent != null)
                isComingFromKPLoginScreen = intent.getBooleanExtra("isComingFromKPLoginScreen", false);

            preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);

            String checkEmailStatus = getResources().getString(R.string.email_verification_status_base_url) +
                    preferences.getString("KP_USER_EMAIL_ID", "");

            downloadUrl = new URL(checkEmailStatus);
            connection = (HttpURLConnection) downloadUrl.openConnection();
            inputStream = connection.getInputStream();
//            buffer = new byte[1024];
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }

            finalResponse = total.toString();
//            Log.e("EmailBG", "finalResponse" + finalResponse);
            if (!isComingFromKPLoginScreen)
                parseRequestedResponse(finalResponse);

        } catch (Exception exception) {
//            Log.e("EmailBG", "Exception Area.....1");
        } finally {
            if (connection != null)
                connection.disconnect();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }*/

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
//        Log.e("EmailBG", "AboutTo start service is NOW started");

        URL downloadUrl;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        String finalResponse = "";
        try {
            if (intent != null)
                isComingFromKPLoginScreen = intent.getBooleanExtra("isComingFromKPLoginScreen", false);

            preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);

            String checkEmailStatus = getResources().getString(R.string.email_verification_status_base_url) +
                    preferences.getString("KP_USER_EMAIL_ID", "");

            downloadUrl = new URL(checkEmailStatus);
            connection = (HttpURLConnection) downloadUrl.openConnection();
            inputStream = connection.getInputStream();
//            buffer = new byte[1024];
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }

            finalResponse = total.toString();
//            Log.e("EmailBG", "finalResponse" + finalResponse);
            if (!isComingFromKPLoginScreen)
                parseRequestedResponse(finalResponse);

        } catch (Exception exception) {
//            Log.e("EmailBG", "Exception Area.....1");
        } finally {
            if (connection != null)
                connection.disconnect();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private void parseRequestedResponse(String response) {

//        Log.e("EmailBG", "Response Area...." + response);
        try {
            if (!response.equals("") && !response.equals("Exception")) {
                if (response.contains("Not Found") || response.contains("not found")) {
                } else {
                    Log.e("EmailBG", "Coming for Cool Parsing");
                    SharedPreferences.Editor emailEditor = preferences.edit();
                    emailEditor.putBoolean("EMAIL_IS_VERIFIED", true).apply();
                }
            }
        } catch (Exception e) {
        }
    }

}
