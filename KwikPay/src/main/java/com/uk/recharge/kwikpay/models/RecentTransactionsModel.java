package com.uk.recharge.kwikpay.models;

/**
 * Created by Ashu Rajput on 15-03-2017.
 */

public class RecentTransactionsModel {

    private String mobileNumber;
    private String operatorName;
    private String topUpAmount;
    private String displayCurrencyCode;

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getTopUpAmount() {
        return topUpAmount;
    }

    public void setTopUpAmount(String topUpAmount) {
        this.topUpAmount = topUpAmount;
    }

    public String getDisplayCurrencyCode() {
        return displayCurrencyCode;
    }

    public void setDisplayCurrencyCode(String displayCurrencyCode) {
        this.displayCurrencyCode = displayCurrencyCode;
    }
}
