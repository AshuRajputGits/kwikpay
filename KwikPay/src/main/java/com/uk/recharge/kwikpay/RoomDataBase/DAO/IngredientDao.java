package com.uk.recharge.kwikpay.RoomDataBase.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;

import java.util.List;

@Dao
public interface IngredientDao {

    @Query("SELECT * FROM Ingredient where parent_id=:parent_id")
    List<Ingredient> getAll(String parent_id);

    @Insert
    void insert(List<Ingredient> list);

    @Query("delete from Ingredient")
    void clear();

    @Query("SELECT COUNT(*) from Ingredient")
    int count();
}
