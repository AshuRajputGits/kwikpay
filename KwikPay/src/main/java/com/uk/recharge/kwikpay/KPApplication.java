package com.uk.recharge.kwikpay;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.cm.hybridmessagingsdk.HybridMessaging;
import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.uk.recharge.kwikpay.utilities.CustomSignikaFont;
import com.uk.recharge.kwikpay.utilities.Utility;
import com.urbanairship.UAirship;

import io.fabric.sdk.android.Fabric;

public class KPApplication extends Application {

    private static Context mContext;
    private static final String FIRST_RUN_KEY = "first_run";
    private Utility utility = null;
    private static KPApplication kpApplication = null;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        mContext = getApplicationContext();
        kpApplication = this;

        String signikaFontType = "fonts/signika.ttf";
        CustomSignikaFont.setDefaultFont(this, "DEFAULT", signikaFontType);
        CustomSignikaFont.setDefaultFont(this, "MONOSPACE", signikaFontType);
        CustomSignikaFont.setDefaultFont(this, "SERIF", signikaFontType);
        CustomSignikaFont.setDefaultFont(this, "SANS_SERIF", signikaFontType);

//        FacebookSdk.sdkInitialize(getApplicationContext());
        try {
            AppEventsLogger.activateApp(this);
//            FirebaseApp.initializeApp(this);
            HybridMessaging.initialize(this);
        } catch (Exception e) {
        }

        //INITIALIZING URBAN AIR SHIP SDK...
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final boolean isFirstRun = preferences.getBoolean(FIRST_RUN_KEY, true);
        if (isFirstRun) {
            preferences.edit().putBoolean(FIRST_RUN_KEY, false).apply();
        }

        UAirship.takeOff(this, new UAirship.OnReadyCallback() {
            @Override
            public void onAirshipReady(UAirship airship) {

                /*CustomNotificationFactory notificationFactory;
                notificationFactory = new CustomNotificationFactory(UAirship.getApplicationContext());
                airship.getPushManager().setNotificationFactory(notificationFactory);*/

                // Enable user notifications
                if (isFirstRun) {
                    airship.getPushManager().setUserNotificationsEnabled(true);
                }
            }
        });

        //Adding below to activate : Crashlytics
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)  // Enables Crashlytics debugger
                .build();
        Fabric.with(fabric);

        /*Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());*/

    }

    public static Context getContext() {
        return mContext;
    }

    synchronized public Tracker getDefaultTracker() {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        return analytics.newTracker(R.xml.global_tracker);
    }

    public void setGoogleAnalyticsScreenName(String eventName) {
        try {
            Tracker mTracker = getDefaultTracker();
            if (mTracker != null) {
                mTracker.setScreenName(eventName);
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static KPApplication getInstance() {
        return kpApplication;
    }

    public Utility getUtilityInstance() {
        if (utility == null)
            utility = new Utility();
        return utility;
    }

}
