package com.uk.recharge.kwikpay.kwikcharge;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;

/**
 * Created by Ashu Rajput on 10/7/2017.
 */

public class ElectricChargeMiniDetails extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.electric_charge_mini_details);

        ImageView evRateBannerIV = findViewById(R.id.evRateBannerIV);
        TextView rateSummaryTV = findViewById(R.id.rateSummaryTV);

        findViewById(R.id.headerHomeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ElectricChargeMiniDetails.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.kcRateProceedButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ElectricChargeMiniDetails.this, KwikChargeOrderScreen.class));
            }
        });

        String rate = "";
        try {
            rate = KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getRecentRate();

            Double rateInDouble = Double.parseDouble(rate);
            int finalRateCalculated = (int) (rateInDouble * 100);

            StringBuilder rateBuilder = new StringBuilder();
            rateBuilder.append(Html.fromHtml("\u00a3"));
            rateBuilder.append(" ");
            rateBuilder.append(rate);
            rateBuilder.append(" (");
            rateBuilder.append("" + finalRateCalculated + " pence");
            rateBuilder.append(")");
            rateBuilder.append(" per kWh");
            rateSummaryTV.setText(rateBuilder.toString());
        } catch (Exception e) {
        }

        try {
            Glide.with(this).load(KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getEvRateScreenBanner())
                    .placeholder(R.drawable.kc_banner2)
                    .into(evRateBannerIV);
        } catch (Exception e) {
        }
    }
}
