package com.uk.recharge.kwikpay.kwikcharge;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.dialog.MapInfoWindow;
import com.uk.recharge.kwikpay.kwikcharge.models.ConnectorListMO;
import com.uk.recharge.kwikpay.kwikcharge.models.MapChargerLocationsMO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;

/**
 * Created by Ashu Rajput on 9/21/2017.
 */

public class KCMapsActivity extends FragmentActivity implements OnMapReadyCallback, OnMarkerClickListener, AsyncRequestListenerViaPost {

    private GoogleMap mMap;
    private ArrayList<MapChargerLocationsMO> mapChargerMOArrayList = null;
    private Polyline polyline = null;
    private ArrayList<LatLng> latLongList = null;
    public double latitude = 0.0, longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_maps_activity);

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("LATITUDE"))
                latitude = receivedBundle.getDouble("LATITUDE", 0.0);
            if (receivedBundle.containsKey("LONGITUDE"))
                longitude = receivedBundle.getDouble("LONGITUDE", 0.0);
        }

        if (latitude == 0.0 && longitude == 0.0)
            startLocationUpdates();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new LoadResponseViaPost(KCMapsActivity.this, formKCMapChargerLocations(), true).execute("");
            }
        }, 500);
    }

    private String formKCMapChargerLocations() {

        SharedPreferences preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("APISERVICE", "KC_DEVICES");
            jsonobj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("DEVICEOS", "ANDROID");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonobj.toString();

    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //------------------------------------------------------------------------------------------------------------//
            // If you just want to get a single location (not periodic) you can just use the oneFix modifier  eg: .oneFix()
            //------------------------------------------------------------------------------------------------------------//
            LocationParams params = LocationParams.NAVIGATION;
            SmartLocation.with(this)
                    .location()
                    .config(params)
                    .start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("com.ar.draw.route.path"));
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("google.navigation:q=an+" +
                            mapChargerMOArrayList.get((Integer) marker.getTag()).getLocationAddress()));
                    startActivity(intent);
                } catch (Exception e) {
                }

                /*try {
                    Intent intent = new Intent();
                    intent.setData(Uri.parse("geo:" + marker.getPosition() + "?q=" + mapChargerMOArrayList.get((Integer) marker.getTag()).getLocationAddress()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
        });
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        clearRouteFromMap();
        if ((int) marker.getTag() == -1)
            return true;

        marker.showInfoWindow();

        /*LatLng markerLatLng = marker.getPosition();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startForegroundService(new Intent(this, DrawRoutePathService.class));
        else
            startService(new Intent(this, DrawRoutePathService.class));
                .putExtra("SourceLatitude", "12.9221")
                .putExtra("SourceLongitude", "77.6210")
                .putExtra("DestinationLatitude", "" + markerLatLng.latitude)
                .putExtra("DestinationLongitude", "" + markerLatLng.longitude));*/

        return true;
    }

    @Override
    protected void onDestroy() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    /*private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList<LatLng> pathResponse = intent.getParcelableArrayListExtra("PathResponse");
            drawRoutePath(pathResponse);
        }
    };*/

    /*public void drawRoutePath(ArrayList<LatLng> latLongList) {

        try {
            if (mMap != null) {

                polyline = mMap.addPolyline(new PolylineOptions()
                        .addAll(latLongList)
                        .width(4)
                        .color(Color.parseColor("#00581e"))//Google maps dark green color
                        .geodesic(true)
                );
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLongList.get(0), 17.0f));
            }
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }
    }*/

    private void clearRouteFromMap() {
        if (polyline != null)
            polyline.remove();
        polyline = null;
    }

    @Override
    public void onRequestComplete(String loadedString) {

        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception"))
                parseMapChargerLocationsResponse(loadedString);
            else
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        } else
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
    }

    /*private class PlotMarkersOnMap extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {

            try {
                if (latLongList != null && latLongList.size() > 0) {
                    {
//                        LatLng source_location = new LatLng(Double.parseDouble("12.9279"), foundLocation.getLongitude());
                        LatLng source_location = new LatLng(Double.parseDouble("12.9221"), Double.parseDouble("77.6210"));
                        MarkerOptions markerOptions;
                        markerOptions = new MarkerOptions().position(source_location).title("Your Current Location");
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                        mMap.addMarker(markerOptions);
                    }
                    for (int i = 0; i < latLongList.size(); i++) {
                        MarkerOptions markerOptions;
                        markerOptions = new MarkerOptions().position(latLongList.get(i)).title(mapChargerMOArrayList.get(i).getLocationName());
                        mMap.addMarker(markerOptions);
                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLongList.get(0), 14.0f));
                }
            } catch (Exception e) {
                e.printStackTrace();
                FirebaseCrash.report(e);
            }
            return null;
        }

    }*/

    private void parseMapChargerLocationsResponse(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);
            ArrayList<ConnectorListMO> connectorListMOArrayList;
            latLongList = new ArrayList<>();

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    mapChargerMOArrayList = new ArrayList<>();

                    JSONArray mapLocationJsonArray = childJsonObj.getJSONArray("DEVICE_LIST");
                    if (null != mapLocationJsonArray && mapLocationJsonArray.length() > 0) {
                        for (int i = 0; i < mapLocationJsonArray.length(); i++) {
                            MapChargerLocationsMO modelObject = new MapChargerLocationsMO();

                            JSONObject childJsonObject = mapLocationJsonArray.getJSONObject(i);

                            modelObject.setLocationName(childJsonObject.optString("LOCATIONNAME"));
                            modelObject.setLocationAddress(childJsonObject.optString("LocationAddress"));
                            modelObject.setLatitude(childJsonObject.optString("Latitude"));
                            modelObject.setLongitude(childJsonObject.optString("Longitude"));
                            modelObject.setDeviceStatus(childJsonObject.optString("DEVICE_STATUS"));

                            try {
                                latLongList.add(new LatLng(Double.parseDouble(childJsonObject.optString("Latitude")),
                                        Double.parseDouble(childJsonObject.optString("Longitude"))));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            connectorListMOArrayList = new ArrayList<>();

                            JSONArray connectorListArray = childJsonObject.getJSONArray("CONNECTOR_LIST");
                            if (connectorListArray != null && connectorListArray.length() > 0) {
                                ConnectorListMO connectorListMO;
                                for (byte b = 0; b < connectorListArray.length(); b++) {
                                    connectorListMO = new ConnectorListMO();
                                    connectorListMO.setConnectorType(connectorListArray.getJSONObject(b).optString("ConnectorType"));
                                    connectorListMO.setConnectorStatus(connectorListArray.getJSONObject(b).optString("ConnectorStatus"));
                                    connectorListMOArrayList.add(connectorListMO);
                                }
                            }
                            modelObject.setConnectorListMOArrayList(connectorListMOArrayList);
                            mapChargerMOArrayList.add(modelObject);
                        }
                        plotMarkersOnMap();
                    }
                } else {
                    if (childJsonObj.has("DESCRIPTION"))
                        ARCustomToast.showToast(this, childJsonObj.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ARCustomToast.showToast(this, "Parsing exception", Toast.LENGTH_LONG);
        }
    }

    private void plotMarkersOnMap() {
        try {
            LatLng currentLatLong;
            if (latitude != 0.0 && longitude != 0.0) {
                currentLatLong = new LatLng(latitude, longitude);
                mMap.addMarker(new MarkerOptions().position(currentLatLong).title("Your Current Location")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))).setTag(-1);
            }
            if (latLongList != null && latLongList.size() > 0) {
                for (int i = 0; i < latLongList.size(); i++) {
                    MarkerOptions markerOptions;
                    markerOptions = new MarkerOptions().position(latLongList.get(i))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

                    mMap.addMarker(markerOptions).setTag(i);
                }

                mMap.setInfoWindowAdapter(new MapInfoWindow(KCMapsActivity.this, mapChargerMOArrayList));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLongList.get(0)));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLongList.get(0), 8.5f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
