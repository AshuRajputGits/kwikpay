package com.uk.recharge.kwikpay.kwikvend;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.KCMapsActivity;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog.LocationIdListener;
import com.uk.recharge.kwikpay.kwikcharge.qrscanner.BarcodeScannerActivity;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductOthersDetailsMO;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;

import static com.uk.recharge.kwikpay.kwikcharge.ScanAndLocationActivity.isGPSEnabled;

/**
 * Created by Ashu Rajput on 7/10/2018.
 */

public class KVHomeScreen extends KPSuperClass implements LocationIdListener {

    private static final int REQUEST_SCANNER = 9;
    private static final int REQUEST_CAMERA_RESULT = 10;
    private static final int REQUEST_LOCATION_RESULT = 11;
    private double latitude = 0.0, longitude = 0.0;
    private int serviceCallIdentifierFlag = 0; // {if flag=1[Need to call locationId Service], if flag=2[Need to call Map Service] }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        initiateAppsBanner("KwikVend");

        findViewById(R.id.scanQRCodeTV).setOnClickListener(onClickListener);
        findViewById(R.id.enterLocationId).setOnClickListener(onClickListener);
        findViewById(R.id.kcGeoLocationTV).setOnClickListener(onClickListener);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.scan_location_activity;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.scanQRCodeTV) {
                checkRTPForCamera();
            } else if (v.getId() == R.id.enterLocationId) {
                serviceCallIdentifierFlag = 1;
                grantLocationPermission();
            } else if (v.getId() == R.id.kcGeoLocationTV) {
                serviceCallIdentifierFlag = 2;
//                grantLocationPermission();
                ARCustomToast.showToast(KVHomeScreen.this, "Map is not available at the moment");
            }
        }
    };

    private void checkRTPForCamera() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                    startQRScanActivity();
                else {
                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA))
                        Toast.makeText(this, "No Permission to use the Camera services", Toast.LENGTH_SHORT).show();
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, REQUEST_CAMERA_RESULT);
                }
            } else
                startQRScanActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void grantLocationPermission() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION))
                        Toast.makeText(this, "Please accept location permission, to use this service", Toast.LENGTH_SHORT).show();

                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_RESULT);
                } else {
                    if (serviceCallIdentifierFlag == 1)
                        startLocationIdService();
                    else if (serviceCallIdentifierFlag == 2)
                        startMapScreen();
                }
            } else {
                if (serviceCallIdentifierFlag == 1)
                    startLocationIdService();
                else if (serviceCallIdentifierFlag == 2)
                    startMapScreen();
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void startQRScanActivity() {
        startActivityForResult(new Intent(this, BarcodeScannerActivity.class), REQUEST_SCANNER);
    }

    private void startLocationIdService() {
        if (isGPSEnabled(this)) {
            DialogFragment dialog = new CustomDialog();
            dialog.show(getSupportFragmentManager(), "LocationIdDialog");
            Bundle bundle = new Bundle();
            bundle.putBoolean("IsComingFromKV", true);
            dialog.setArguments(bundle);
            dialog.setCancelable(false);
        } else
            gpsDialogBox();
    }

    private void startMapScreen() {

        if (isGPSEnabled(this)) {
            startLocationUpdates();
            startActivity(new Intent(this, KCMapsActivity.class)
                    .putExtra("LATITUDE", latitude)
                    .putExtra("LONGITUDE", longitude));
        } else
            gpsDialogBox();
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationParams params = LocationParams.NAVIGATION;
            SmartLocation.with(this).location()
                    .config(params)
                    .start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Cannot run application because camera service permission have not been granted", Toast.LENGTH_SHORT).show();
                else
                    startQRScanActivity();
                break;

            case REQUEST_LOCATION_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Cannot run application because location permission have not been granted", Toast.LENGTH_SHORT).show();
                else {
                    if (serviceCallIdentifierFlag == 1)
                        startLocationIdService();
                    else if (serviceCallIdentifierFlag == 2)
                        startMapScreen();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SCANNER) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String locationNumber = data.getStringExtra("QR_RESULT");
                    if (locationNumber != null && !locationNumber.trim().equals("") && locationNumber.contains("-")) {
                        callKVGetProductListScreen(locationNumber.split("-")[1]);
                    } else
                        ARCustomToast.showToast(KVHomeScreen.this, "Please scan valid QR Code");
                }
            }
        }
    }

    private void gpsDialogBox() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("Please enable your GPS from setting");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    dialog.dismiss();
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(KVHomeScreen.this, "Setting activity not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.show();
    }

    @Override
    public void getLocationId(String locationID) {
        if (locationID != null && !locationID.isEmpty()) {
            callKVGetProductListScreen(locationID);
        } else {
            /*if (apiReceivedLocationChargerDetailsModel != null) {
                if (preference.getBoolean("IS_KC_TNC_CHECKED", false)) {
                    apiHitPosition = 2;
                    KwikChargeDetailsSingleton.getInstance().setLocationIdMO(apiReceivedLocationChargerDetailsModel);
                    new LoadResponseViaPost(this, formRegNumberJSON(), true).execute("");
                } else {
                    KwikChargeDetailsSingleton.getInstance().setLocationIdMO(apiReceivedLocationChargerDetailsModel);
                    startActivity(new Intent(this, KCCompanyDetails.class));
                    addressBuilder = null;
                }
            }*/
        }
    }

    private void callKVGetProductListScreen(String qrCode) {
        KVProductOthersDetailsMO model = new KVProductOthersDetailsMO();
        model.setMerchantId("123456");
        model.setQrCode(qrCode);
        KVProductDetailsSingleton.getInstance().setKvProductOthersDetailsMO(model);

        startActivity(new Intent(KVHomeScreen.this, KVProductSelection.class));
    }

}
