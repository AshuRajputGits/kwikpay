package com.uk.recharge.kwikpay.catering;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.models.CartProduct;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.uk.recharge.kwikpay.catering.CateringCart.cartProductId;
import static com.uk.recharge.kwikpay.catering.CateringCart.cartProducts;
import static com.uk.recharge.kwikpay.catering.CateringCart.updateCartTotal;
import static com.uk.recharge.kwikpay.catering.CateringCart.updateCartVisibility;
import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;
import static com.uk.recharge.kwikpay.utilities.Utility.ParseFloat;

public class CustomizeOrder extends Activity {

    SharedPreferences preference;
    Context context;
    String parent_id;
    String product_type;
    String id;
    AppDatabase appDatabase;
    List<Ingredient> ingredientList = new ArrayList<>();
    GridLayout ingredient_gridLayout;
    public TextView cartCountTextView;
    public LinearLayout cartLayout;
    public TextView cartTotalTextView;
    float total_price = 0;
    CartProduct selectedCartProduct;
    boolean killScreen = false;
    TextView productTypeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cateringcustomize_order);
        context = this;
        cartLayout = findViewById(R.id.cart_layout);
        cartCountTextView = findViewById(R.id.cart_count);
        cartTotalTextView = findViewById(R.id.cart_total);

        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        setTotal_price();
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ingredient_gridLayout = findViewById(R.id.ingredient_gridLayout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(CustomizeOrder.this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        findViewById(R.id.backbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        parent_id = getIntent().getStringExtra("parent_id");
        product_type = getIntent().getStringExtra("product_type");
        id = getIntent().getStringExtra("id");
        killScreen = getIntent().getBooleanExtra("killScreen", false);


        cartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartProducts.isEmpty()) {
                    Toast.makeText(context, "Cart is Empty!", Toast.LENGTH_SHORT).show();
                } else {
                    finish();
                    if (!killScreen) {
                        Intent intent = new Intent(context, MyOrders.class);
                        startActivity(intent);
                    }
                }
            }
        });
        productTypeTextView = findViewById(R.id.brandname);
        updateCartVisibility(cartLayout);
        renderDynamicControls();
    }

    private void renderDynamicControls() {
        ingredientList = appDatabase.ingredientDao().getAll(parent_id);
        for (CartProduct cartProduct : cartProducts) {
            if (cartProduct.getId().equals(id)) {
                Log.e("CartProduct", "Cart Product Selected : " + cartProduct.getBrand_name());
                selectedCartProduct = cartProduct;
                break;
            }
        }

        productTypeTextView.setText(String.format("%s £ %s", selectedCartProduct.getBrand_name(), selectedCartProduct.getUnit_price()));

        HashMap<String, List<Integer>> viewCheckedPerIngredientList = selectedCartProduct.getViewCheckedPerIngredientList();
        for (final Ingredient ingredient : ingredientList) {
            View mainView;
            final List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());
            if (ingredient.getCategory_action().equals("MULTIPLE_SELECTION")) {
                mainView = getLayoutInflater().inflate(R.layout.dynamic_group_checkbox, null, false);

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                layoutParams.setMargins(4, 4, 4, 20);
                mainView.setLayoutParams(layoutParams);

                TextView field_label = mainView.findViewById(R.id.field_label);
                GridLayout ingredient_type_gridLayout = mainView.findViewById(R.id.ingredient_type_gridLayout);

                field_label.setText(String.format(" %s", ingredient.getName()));

                List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());


                int index = 0;

                for (IngredientType ingredientType : ingredientTypeList) {

                    final CheckBox checkbox_view = (CheckBox) getLayoutInflater().inflate(R.layout.dynamic_checkbox, null, false);
                    LinearLayout.LayoutParams subLayoutParams = new LinearLayout.LayoutParams(-1, -2);
                    subLayoutParams.setMargins(4, 4, 4, 20);
                    checkbox_view.setLayoutParams(subLayoutParams);

                    String label = ingredientType.getName() + " £ " + ingredientType.getUnit_price();
                    checkbox_view.setText(label);
                    if (checkedList.get(index) == 1)
                        checkbox_view.setChecked(true);
                    else
                        checkbox_view.setChecked(false);
                    final int finalIndex = index;
                    checkbox_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (checkbox_view.isChecked()) {
                                checkedList.set(finalIndex, 1);
                            } else {
                                checkedList.set(finalIndex, 0);
                            }
                            updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
                        }
                    });
                    ingredient_type_gridLayout.addView(checkbox_view);
                    index++;
                }
            } else {
                mainView = getLayoutInflater().inflate(R.layout.dynamic_group_radio, null, false);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                layoutParams.setMargins(4, 4, 4, 20);
                mainView.setLayoutParams(layoutParams);

                TextView field_label = mainView.findViewById(R.id.field_label);
                final RadioGroup ingredient_type_radioGroup = mainView.findViewById(R.id.ingredient_type_radioGroup);

                field_label.setText(String.format("* %s", ingredient.getName()));

                List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
                int index = 0;

                for (IngredientType ingredientType : ingredientTypeList) {

                    final RadioButton radio_view = (RadioButton) getLayoutInflater().inflate(R.layout.dynamic_radio, null, false);
                    LinearLayout.LayoutParams subLayoutParams = new LinearLayout.LayoutParams(-1, -2);
                    subLayoutParams.setMargins(4, 4, 4, 20);
                    radio_view.setLayoutParams(subLayoutParams);

                    String label = ingredientType.getName() + " £ " + ingredientType.getUnit_price();

                    radio_view.setText(label);
                    ingredient_type_radioGroup.addView(radio_view);


                    if (checkedList.get(index) == 1) {
                        ingredient_type_radioGroup.check(radio_view.getId());
                    }


                    final int finalIndex = index;
                    radio_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (radio_view.isChecked()) {

                                for (Integer checked : checkedList) {
                                    checkedList.set(checkedList.indexOf(checked), 0);
                                }

                                checkedList.set(finalIndex, 1);
                            }

                            updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);

                        }
                    });


                    index++;
                }

            }


            ingredient_gridLayout.addView(mainView);


        }
    }

    private void setTotal_price() {

        total_price = 0;
        if (!cartProducts.isEmpty()) {

            for (CartProduct cartProduct : cartProducts) {

                float val = ParseFloat(cartProduct.getUnit_price()) * cartProduct.getSelected_quantity();
                total_price += val;


                List<Ingredient> ingredientList = cartProduct.getIngredientList();

                HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();

                for (final Ingredient ingredient : ingredientList) {

                    List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
                    List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());

                    for (IngredientType ingredientType : ingredientTypeList) {
                        int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                        if (checked == 1) {
                            total_price += ParseFloat(ingredientType.getUnit_price());
                        }
                    }


                }


            }
        }

        cartCountTextView.setText(String.valueOf(cartProductId.size()));
        cartTotalTextView.setText(String.format("£ %s", new DecimalFormat("##.00").format(total_price)));

    }

}
