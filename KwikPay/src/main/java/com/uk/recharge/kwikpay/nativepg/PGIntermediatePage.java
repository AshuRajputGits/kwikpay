package com.uk.recharge.kwikpay.nativepg;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.PaymentGatewayPage;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;

public class PGIntermediatePage extends Activity implements AsyncRequestListenerViaPost
{
	private TextView paypalPGButton,judoPGButton;
	private SharedPreferences preference;
	private String apiPGName="",PG_APP_TXN_ID="",PG_TYPE="",PG_SO_ID_VALUE="",PG_MOP_ID_VALUE="",PG_APP_ORDER_ID="",PG_APP_AMT="";
	private Context classContext=null;
	private HashMap<String, String> nativePGDataHashMap,receivedPGDetailsHashMap=null;
	private int apiHitPosition=0;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pgintermediate_page);

		classContext=PGIntermediatePage.this;

		receivedPGDetailsHashMap=new HashMap<String, String>();

		paypalPGButton=(TextView)findViewById(R.id.paypal_PG_Button);
		judoPGButton=(TextView)findViewById(R.id.judo_PG_Button);
		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);

		Bundle receivedBundle=getIntent().getExtras();
		if(receivedBundle!=null)
		{
			if(receivedBundle.containsKey("JUDOPGDETAIL_JSON_KEY"))
				receivedPGDetailsHashMap = (HashMap<String, String>) receivedBundle.getSerializable("JUDOPGDETAIL_JSON_KEY");
		}

		paypalPGButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				apiHitPosition=1;
				PG_SO_ID_VALUE="164";
				PG_MOP_ID_VALUE="21";

				//CALLING API FOR GETTING APP TRANSACTION ID FOR PAYPAL PG 
				new LoadResponseViaPost(classContext, formAppTxnIDJSON(), true).execute("");


//				new LoadResponseViaPost(classContext, formPaymentGatewayJSON("164","21")).execute("");
			}
		});

		judoPGButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//CALLING API FOR GETTING APP TRANSACTION ID FOR PAYPAL PG 
				apiHitPosition=1;

				PG_SO_ID_VALUE=preference.getString("PG_SO_ID","160");
				PG_MOP_ID_VALUE=preference.getString("PG_MOP_ID1","18");
				new LoadResponseViaPost(classContext, formAppTxnIDJSON(), true).execute("");

				/*apiHitPosition=2;
				new LoadResponseViaPost(classContext, formPaymentGatewayJSON() ).execute("");*/
			}
		});

	}

	public String formPaymentGatewayJSON(String SERVICE_NAME,boolean isWebView)
	{
		try
		{
			JSONObject json = new JSONObject();
			JSONArray jsonArray=new JSONArray();

			json.put("iscart", "NO");
			json.put("os", "TEST OS");
			json.put("pgsoid",PG_SO_ID_VALUE);
			json.put("mopid",PG_MOP_ID_VALUE);
			json.put("apptransactionid", PG_APP_TXN_ID );
			json.put("ipaddress","122.176.42.107");
			json.put("browser","TEST BROWSER");
			json.put("sessionid",  ProjectConstant.SESSIONID);
			json.put("source","KPAPP");
			json.put("username",preference.getString("KP_USER_NAME", ""));
			json.put("userid",preference.getString("KP_USER_ID", "0"));
			json.put("emailid",preference.getString("KP_USER_EMAIL_ID", ""));
			json.put("appname", "KWIKPAY");
			
			if(!isWebView)
				json.put("pgType", PG_TYPE);

			for(int i=0;i<1;i++)
			{
				JSONObject childJsonObject = new JSONObject();

				childJsonObject.put("apiservice", SERVICE_NAME);
				childJsonObject.put("username",preference.getString("KP_USER_NAME", ""));
				childJsonObject.put("userid",preference.getString("KP_USER_ID", "0"));
				childJsonObject.put("emailid",preference.getString("KP_USER_EMAIL_ID", ""));

				if(receivedPGDetailsHashMap.containsKey(ProjectConstant.PAYNOW_MOBILENUMBER))
				{
					if(receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER).contains("N/A"))
						childJsonObject.put("mobilenumber","");
					else
						childJsonObject.put("mobilenumber",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER));
				}
				else
					childJsonObject.put("mobilenumber",receivedPGDetailsHashMap.get("MOBILENUMBER"));

				childJsonObject.put("rechargeamount",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_RECHARGEAMOUNT));

				if(receivedPGDetailsHashMap.containsKey(ProjectConstant.TOPUP_API_DISPLAYAMT))
					childJsonObject.put("displayamount",receivedPGDetailsHashMap.get(ProjectConstant.TOPUP_API_DISPLAYAMT));
				else
					childJsonObject.put("displayamount",receivedPGDetailsHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT));

				childJsonObject.put("netpgamount",receivedPGDetailsHashMap.get("TOTAL_AMT_PAYABLE_KEY"));
				childJsonObject.put("rechargecurrencyagid",receivedPGDetailsHashMap.get(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
				childJsonObject.put("displaycurrencyagid",receivedPGDetailsHashMap.get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
				childJsonObject.put("paymentcurrencyagid",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID));
				childJsonObject.put("os", "TEST OS");
				childJsonObject.put("ipaddress","122.176.42.107");
				childJsonObject.put("browser","TEST BROWSER");
				childJsonObject.put("cpntxnid","0");
				childJsonObject.put("cpnamount","0");
				childJsonObject.put("cpnsrno","0");
				childJsonObject.put("serviceid",receivedPGDetailsHashMap.get("SERVICEID"));
				childJsonObject.put("operatorcode", receivedPGDetailsHashMap.get(ProjectConstant.API_OPCODE));
				childJsonObject.put("countrycode",receivedPGDetailsHashMap.get(ProjectConstant.TXN_COUNTRYCODE));
				childJsonObject.put("source","KPAPP");
				childJsonObject.put("discountamount","0");
				childJsonObject.put("pgdiscountamount","0");
				childJsonObject.put("pgsoid",PG_SO_ID_VALUE);
				childJsonObject.put("mopid",PG_MOP_ID_VALUE);
				childJsonObject.put("sessionid", ProjectConstant.SESSIONID);
				childJsonObject.put("processingfee",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_PROCESSINGFEE));
				childJsonObject.put("servicefee",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_SERVICEFEE));

				jsonArray.put(childJsonObject);
			}
			json.put("kprequest",jsonArray);

			return json.toString();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public String formAppTxnIDJSON()
	{
		try
		{
			JSONObject json = new JSONObject();
			json.put("APISERVICE", "KPPAYMENTGATEWAYAPPTXNID");
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("DEVICEOS", "ANDROID");
			json.put("SOURCENAME", "KPAPP");
			json.put("OPERATORCODE",  receivedPGDetailsHashMap.get(ProjectConstant.API_OPCODE));
			json.put("SERVICEID",receivedPGDetailsHashMap.get(ProjectConstant.TXN_SERVICEID));
			json.put("USERNAME",preference.getString("KP_USER_NAME", ""));
			json.put("USERID",preference.getString("KP_USER_ID", "0"));
			json.put("PGSOID",PG_SO_ID_VALUE); 
			json.put("MOPID",PG_MOP_ID_VALUE);
			json.put("PLANDESCRIPTION",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_PLANDESCRIPTION));
			json.put("OPERATORNAME",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_OPERATORNAME));
			json.put("PROCESSINGFEE",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_PROCESSINGFEE));
			json.put("SERVICEFEE",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_SERVICEFEE));
			json.put("DISCOUNTAMOUNT","0");
			json.put("PGDISCOUNTAMOUNT","0");
			json.put("SOURCE","KPAPP");

			if(receivedPGDetailsHashMap.containsKey(ProjectConstant.PAYNOW_MOBILENUMBER))
			{
				if(receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER).contains("N/A"))
					json.put("MOBILENUMBER","");
				else
					json.put("MOBILENUMBER",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_MOBILENUMBER));
			}
			else
				json.put("MOBILENUMBER",receivedPGDetailsHashMap.get("MOBILENUMBER"));
			json.put("NETPGAMOUNT",receivedPGDetailsHashMap.get("TOTAL_AMT_PAYABLE_KEY"));
			json.put("COUNTRYCODE",receivedPGDetailsHashMap.get(ProjectConstant.TXN_COUNTRYCODE));
			json.put("BROWSER","TEST BROWSER");
			json.put("OS","TEST OS");
			json.put("CPNTXNID","0");
			json.put("CPNSRNO","0");
			json.put("CPNAMOUNT","0");
			json.put("OPABBR",receivedPGDetailsHashMap.get("OP_ABBR_KEY"));

			if(receivedPGDetailsHashMap.containsKey(ProjectConstant.TOPUP_API_DISPLAYAMT))
				json.put("DISPLAYAMOUNT",receivedPGDetailsHashMap.get(ProjectConstant.TOPUP_API_DISPLAYAMT));
			else
				json.put("DISPLAYAMOUNT",receivedPGDetailsHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT));

			json.put("IPADDRESS","122.176.42.107");
			json.put("EMAILID",preference.getString("KP_USER_EMAIL_ID", ""));

			if(receivedPGDetailsHashMap.containsKey(ProjectConstant.TXN_RECHARGEAMOUNT))
				json.put("RECHARGEAMOUNT",receivedPGDetailsHashMap.get(ProjectConstant.TXN_RECHARGEAMOUNT));
			else if(receivedPGDetailsHashMap.containsKey(ProjectConstant.TOPUP_API_RECHARGEAMT))
				json.put("RECHARGEAMOUNT",receivedPGDetailsHashMap.get(ProjectConstant.TOPUP_API_RECHARGEAMT));

			json.put("RECHARGECURRENCYAGID",receivedPGDetailsHashMap.get(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
			json.put("DISPLAYCURRENCYAGID",receivedPGDetailsHashMap.get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
			json.put("PAYMENTCURRENCYAGID",receivedPGDetailsHashMap.get(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID));

			return json.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub
		Log.e("", "Pg Response "+loadedString);

		if(loadedString!=null && !loadedString.equals(""))
		{
			if(!loadedString.equals("Exception"))
			{
				if(apiHitPosition==1) // VISITING HERE, IF REQUESTING FOR RESPONSE OF TXN ID EITHER FORM PAYPAL OR JUDO
				{
					if(parseAppTxnIdDetails(loadedString))
					{
						if(!PG_APP_TXN_ID.equals("") && !PG_TYPE.equals(""))
						{
							if(PG_TYPE.equals("NATIVE"))
							{
								apiHitPosition=2;
								new LoadResponseViaPost(classContext, formPaymentGatewayJSON("KPGETPGDETAILS",false)).execute("");
							}
							else
							{
								Intent pgIntent=(new Intent(classContext,PaymentGatewayPage.class));
								pgIntent.putExtra("PG_JSON_PARAM", formPaymentGatewayJSON("WSGetCotrolAndData",true));
								pgIntent.putExtra("PG_APP_TXN_ID_KEY", PG_APP_TXN_ID);
								startActivity(pgIntent);
							}
						}
					}
				}
				else if(apiHitPosition==2)
				{
					if(parseRecievedPGDetail(loadedString))
					{
						if(apiPGName.equals("JUDOPG"))
						{
							Intent pgIntent=(new Intent(classContext,JUDOPaymentGateway.class));
							pgIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
							startActivity(pgIntent);
						}
						else if(apiPGName.equalsIgnoreCase("PAYPAL"))
						{
							Intent paypalIntent=new Intent(PGIntermediatePage.this,PayPalPaymentScreen.class);
							paypalIntent.putExtra("PG_APP_TXN_ID", PG_APP_TXN_ID);
							paypalIntent.putExtra("PG_APP_ORDER_ID", PG_APP_ORDER_ID);
							paypalIntent.putExtra("PG_APP_AMOUNT", PG_APP_AMT);
							startActivity(paypalIntent);
						}
						else if(apiPGName.equals("COUPONPG"))
						{
							Intent couponPgIntent = (new Intent(classContext,JUDOPaymentGateway.class));
							couponPgIntent.putExtra("NATIVE_PG_COUPON", nativePGDataHashMap);
							couponPgIntent.putExtra("NATIVE_PG_COUPON2", "COUPON_DATA");
							startActivity(couponPgIntent);
						}
					}
				}
			}
			else
			{
				if(DeviceNetConnectionDetector.checkDataConnWifiMobile(PGIntermediatePage.this))
					ARCustomToast.showToast(classContext,getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
				else
					ARCustomToast.showToast(classContext,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
			}
		}
		else
		{
			if(DeviceNetConnectionDetector.checkDataConnWifiMobile(PGIntermediatePage.this))
				ARCustomToast.showToast(classContext,getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
			else
				ARCustomToast.showToast(classContext,getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}
	}

	//LOGIC FOR PARSING THE RECEIVED PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEB VIEW []
	public boolean parseRecievedPGDetail(String pgDetails)
	{
		JSONObject jsonObject,parentResponseObject;
		try 
		{
			jsonObject=new JSONObject(pgDetails);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				ARCustomToast.showToast(classContext,"No Reponse Tag", Toast.LENGTH_LONG);
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
			{
				if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
				{
					nativePGDataHashMap=new HashMap<String, String>();

					if(parentResponseObject.has("PAYMENTREFERENCE"))
						nativePGDataHashMap.put("PAYMENTREFERENCE", parentResponseObject.getString("PAYMENTREFERENCE"));
					if(parentResponseObject.has("JUDOID"))
						nativePGDataHashMap.put("JUDOID", parentResponseObject.getString("JUDOID"));
					if(parentResponseObject.has("CONSUMERREFERENCE"))
						nativePGDataHashMap.put("CONSUMERREFERENCE",parentResponseObject.getString("CONSUMERREFERENCE"));
					if(parentResponseObject.has("CHECKSUMKEY"))
						nativePGDataHashMap.put("CHECKSUMKEY", parentResponseObject.getString("CHECKSUMKEY"));
					if(parentResponseObject.has("ORDERID"))
						nativePGDataHashMap.put("ORDERID", parentResponseObject.getString("ORDERID"));
					if(parentResponseObject.has("TOKENPAYREFERENCE"))
						nativePGDataHashMap.put("TOKENPAYREFERENCE", parentResponseObject.getString("TOKENPAYREFERENCE"));
					if(parentResponseObject.has("SECRETKEY"))
						nativePGDataHashMap.put("SECRETKEY", parentResponseObject.getString("SECRETKEY"));
					if(parentResponseObject.has("TOKEN"))
						nativePGDataHashMap.put("TOKEN", parentResponseObject.getString("TOKEN"));
					if(parentResponseObject.has("PGNAME"))
						apiPGName=parentResponseObject.getString("PGNAME");

					nativePGDataHashMap.put("TOTALAMOUNTPAYABLE", receivedPGDetailsHashMap.get("TOTAL_AMT_PAYABLE_KEY"));
					nativePGDataHashMap.put("PGNAME", apiPGName);
					nativePGDataHashMap.put("APPTXNID", PG_APP_TXN_ID);
					
					PG_APP_AMT = receivedPGDetailsHashMap.get("TOTAL_AMT_PAYABLE_KEY");
					PG_APP_ORDER_ID = parentResponseObject.getString("ORDERID");

				}

				else
				{
					if(parentResponseObject.has("DESCRIPTION"))
					{
						ARCustomToast.showToast(classContext,parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						return false;
					}
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;

	}

	// LOGIC FOR PARSING PG APP TRANSACTION ID FROM WEBSERVICE [apiHitPosition==2]
	public boolean parseAppTxnIdDetails(String operatorResponse)
	{
		JSONObject jsonObject,parentResponseObject;
		try 
		{
			jsonObject=new JSONObject(operatorResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				ARCustomToast.showToast(classContext,"No Reponse Tag", Toast.LENGTH_LONG);
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.RESPONSE_MSG_TAG))
			{
				if(parentResponseObject.getString(ProjectConstant.RESPONSE_MSG_TAG).equals("SUCCESS"))
				{
					if(parentResponseObject.has(ProjectConstant.PG_APP_TXN_ID) && parentResponseObject.has("PGTYPE"))
					{
						PG_APP_TXN_ID=parentResponseObject.getString(ProjectConstant.PG_APP_TXN_ID);
						PG_TYPE=parentResponseObject.getString("PGTYPE");
					}
				}

				else
				{
					ARCustomToast.showToast(classContext,parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
					return false;
				}

			}

		} catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}
	
	/*@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
	}*/

}
