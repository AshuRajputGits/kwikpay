package com.uk.recharge.kwikpay.catering;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.CommonStaticPages;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.R;

/**
 * Created by Ashu Rajput on 11/2/2018.
 */

public class CateringTnCScreen extends KPSuperClass {

    private CheckBox tncCheckBox;
    private boolean isTermConditionAccepted = false;
    private static final int TERM_CONDITION_CODE = 1;
    private SharedPreferences sharedPreferences = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        turnHomeButtonToBackButton();
        setUpResourceIds();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.catering_tnc;
    }

    private void setUpResourceIds() {
        sharedPreferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        ImageView singleBannerIV = findViewById(R.id.singleBannerIV);
        setDynamicWidthHeightToImageView(singleBannerIV);

        tncCheckBox = findViewById(R.id.tncCheckBox);
        tncCheckBox.setOnClickListener(onClickListener);
        findViewById(R.id.tncMessageWebView).setOnClickListener(onClickListener);
        findViewById(R.id.cateringTnCButton).setOnClickListener(onClickListener);
        ImageView cateringCompanyLogo = findViewById(R.id.cateringCompanyLogo);
        TextView cateringCompanyAddress = findViewById(R.id.cateringCompanyAddress);
        cateringCompanyAddress.setText(sharedPreferences.getString("DISTRIBUTOR_ADDRESS", ""));

        try {
            Glide.with(this)
                    .load(sharedPreferences.getString("DISTRIBUTOR_IMAGE", "image"))
                    .placeholder(R.drawable.default_operator_logo)
                    .into(cateringCompanyLogo);

            Glide.with(this)
                    .load(sharedPreferences.getString("MERCHANT_TERM_BANNER", ""))
                    .into(singleBannerIV);
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.tncCheckBox) {
                if (tncCheckBox.isChecked())
                    isTermConditionAccepted = true;
                else
                    isTermConditionAccepted = false;
            } else if (v.getId() == R.id.tncMessageWebView) {
                startActivityForResult(new Intent(CateringTnCScreen.this, CommonStaticPages.class)
                        .putExtra("STATIC_PAGE_NAME", "TNC_PAGE"), TERM_CONDITION_CODE);
            } else if (v.getId() == R.id.cateringTnCButton) {
                if (!tncCheckBox.isChecked() && !isTermConditionAccepted) {
                    ARCustomToast.showToast(CateringTnCScreen.this, "please accept the terms & condition", Toast.LENGTH_LONG);
                } else {
                    sharedPreferences.edit().putBoolean("IS_CATERING_TNC_ACCEPTED", true).apply();
                    startActivity(new Intent(CateringTnCScreen.this, RestoMenuList.class));
                    finish();
                }
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TERM_CONDITION_CODE) {
            if (resultCode == RESULT_OK) {
                isTermConditionAccepted = true;
                tncCheckBox.setChecked(true);
            } else
                isTermConditionAccepted = false;
        }
    }
}
