package com.uk.recharge.kwikpay.myaccount;

import java.io.Serializable;

/**
 * Created by Ashu Rajput on 12/22/2017.
 */

public class MyTransactionsMO implements Serializable {

    private String transactionDateTime;
    private String transactionServiceId;
    private String transactionPaymentCurrencyCode;
    private String transactionRechargeCurrencyCode;
    private String transactionOperatorName;
    private String transactionPaymentAmount;
    private String transactionPaymentStatus;
    private String transactionServiceName;
    private String transactionCountryCode;
    private String transactionDisplayAmount;
    private String transactionOrderId;
    private String transactionOperatorCode;
    private String transactionDisplayCurrencyCode;
    private String transactionRechargeStatus;
    private String transactionRechargeAmount;
    private String transactionSubscriptionNumber;
    private String transactionFee;

    private String transactionRCPin;
    private String transactionRCPinExpiryDate;
    private String transactionCouponSrNumber;
    private String transactionCouponValue;
    private String imageUrl;
    private String transactionDiscountAmount;
    private String transactionPlanDesc;

    //KC RELATED STRINGS
    private String transactionTotalEnergy;
    private String transactionCostOfEnergy;
    private String transactionRegNumber;
    private String transactionLocationName;
    private String transactionChargerId;

    public String getTransactionTotalEnergy() {
        return transactionTotalEnergy;
    }

    public void setTransactionTotalEnergy(String transactionTotalEnergy) {
        this.transactionTotalEnergy = transactionTotalEnergy;
    }

    public String getTransactionCostOfEnergy() {
        return transactionCostOfEnergy;
    }

    public void setTransactionCostOfEnergy(String transactionCostOfEnergy) {
        this.transactionCostOfEnergy = transactionCostOfEnergy;
    }

    public String getTransactionRegNumber() {
        return transactionRegNumber;
    }

    public void setTransactionRegNumber(String transactionRegNumber) {
        this.transactionRegNumber = transactionRegNumber;
    }

    public String getTransactionLocationName() {
        return transactionLocationName;
    }

    public void setTransactionLocationName(String transactionLocationName) {
        this.transactionLocationName = transactionLocationName;
    }

    public String getTransactionChargerId() {
        return transactionChargerId;
    }

    public void setTransactionChargerId(String transactionChargerId) {
        this.transactionChargerId = transactionChargerId;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getTransactionServiceId() {
        return transactionServiceId;
    }

    public void setTransactionServiceId(String transactionServiceId) {
        this.transactionServiceId = transactionServiceId;
    }

    public String getTransactionPaymentCurrencyCode() {
        return transactionPaymentCurrencyCode;
    }

    public void setTransactionPaymentCurrencyCode(String transactionPaymentCurrencyCode) {
        this.transactionPaymentCurrencyCode = transactionPaymentCurrencyCode;
    }

    public String getTransactionRechargeCurrencyCode() {
        return transactionRechargeCurrencyCode;
    }

    public void setTransactionRechargeCurrencyCode(String transactionRechargeCurrencyCode) {
        this.transactionRechargeCurrencyCode = transactionRechargeCurrencyCode;
    }

    public String getTransactionOperatorName() {
        return transactionOperatorName;
    }

    public void setTransactionOperatorName(String transactionOperatorName) {
        this.transactionOperatorName = transactionOperatorName;
    }

    public String getTransactionPaymentAmount() {
        return transactionPaymentAmount;
    }

    public void setTransactionPaymentAmount(String transactionPaymentAmount) {
        this.transactionPaymentAmount = transactionPaymentAmount;
    }

    public String getTransactionPaymentStatus() {
        return transactionPaymentStatus;
    }

    public void setTransactionPaymentStatus(String transactionPaymentStatus) {
        this.transactionPaymentStatus = transactionPaymentStatus;
    }

    public String getTransactionServiceName() {
        return transactionServiceName;
    }

    public void setTransactionServiceName(String transactionServiceName) {
        this.transactionServiceName = transactionServiceName;
    }

    public String getTransactionCountryCode() {
        return transactionCountryCode;
    }

    public void setTransactionCountryCode(String transactionCountryCode) {
        this.transactionCountryCode = transactionCountryCode;
    }

    public String getTransactionDisplayAmount() {
        return transactionDisplayAmount;
    }

    public void setTransactionDisplayAmount(String transactionDisplayAmount) {
        this.transactionDisplayAmount = transactionDisplayAmount;
    }

    public String getTransactionOrderId() {
        return transactionOrderId;
    }

    public void setTransactionOrderId(String transactionOrderId) {
        this.transactionOrderId = transactionOrderId;
    }

    public String getTransactionOperatorCode() {
        return transactionOperatorCode;
    }

    public void setTransactionOperatorCode(String transactionOperatorCode) {
        this.transactionOperatorCode = transactionOperatorCode;
    }

    public String getTransactionDisplayCurrencyCode() {
        return transactionDisplayCurrencyCode;
    }

    public void setTransactionDisplayCurrencyCode(String transactionDisplayCurrencyCode) {
        this.transactionDisplayCurrencyCode = transactionDisplayCurrencyCode;
    }

    public String getTransactionRechargeStatus() {
        return transactionRechargeStatus;
    }

    public void setTransactionRechargeStatus(String transactionRechargeStatus) {
        this.transactionRechargeStatus = transactionRechargeStatus;
    }

    public String getTransactionRechargeAmount() {
        return transactionRechargeAmount;
    }

    public void setTransactionRechargeAmount(String transactionRechargeAmount) {
        this.transactionRechargeAmount = transactionRechargeAmount;
    }

    public String getTransactionSubscriptionNumber() {
        return transactionSubscriptionNumber;
    }

    public void setTransactionSubscriptionNumber(String transactionSubscriptionNumber) {
        this.transactionSubscriptionNumber = transactionSubscriptionNumber;
    }

    public String getTransactionRCPin() {
        return transactionRCPin;
    }

    public void setTransactionRCPin(String transactionRCPin) {
        this.transactionRCPin = transactionRCPin;
    }

    public String getTransactionRCPinExpiryDate() {
        return transactionRCPinExpiryDate;
    }

    public void setTransactionRCPinExpiryDate(String transactionRCPinExpiryDate) {
        this.transactionRCPinExpiryDate = transactionRCPinExpiryDate;
    }

    public String getTransactionCouponSrNumber() {
        return transactionCouponSrNumber;
    }

    public void setTransactionCouponSrNumber(String transactionCouponSrNumber) {
        this.transactionCouponSrNumber = transactionCouponSrNumber;
    }

    public String getTransactionCouponValue() {
        return transactionCouponValue;
    }

    public void setTransactionCouponValue(String transactionCouponValue) {
        this.transactionCouponValue = transactionCouponValue;
    }

    public String getTransactionFee() {
        return transactionFee;
    }

    public void setTransactionFee(String transactionFee) {
        this.transactionFee = transactionFee;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTransactionDiscountAmount() {
        return transactionDiscountAmount;
    }

    public void setTransactionDiscountAmount(String transactionDiscountAmount) {
        this.transactionDiscountAmount = transactionDiscountAmount;
    }

    public String getTransactionPlanDesc() {
        return transactionPlanDesc;
    }

    public void setTransactionPlanDesc(String transactionPlanDesc) {
        this.transactionPlanDesc = transactionPlanDesc;
    }
}
