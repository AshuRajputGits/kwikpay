package com.uk.recharge.kwikpay.singleton;

import android.content.Context;
import android.os.Bundle;

import com.appsflyer.AppsFlyerLib;
import com.facebook.appevents.AppEventsLogger;
import com.uk.recharge.kwikpay.KPApplication;

/**
 * Created by Ashu Rajput on 19-03-2017.
 */

public class EventsLoggerSingleton {

    private static EventsLoggerSingleton eventsLoggerSingleton=null;
    private static AppEventsLogger logger=null;

    private EventsLoggerSingleton()
    {
    }

    public static EventsLoggerSingleton getInstance()
    {
        if(eventsLoggerSingleton==null)
            eventsLoggerSingleton=new EventsLoggerSingleton();
        if(logger==null)
            logger= AppEventsLogger.newLogger(KPApplication.getContext());

        return eventsLoggerSingleton;
    }

    public void setFBLogEvent(String eventName)
    {
        if(logger!=null)
            logger.logEvent(eventName);
    }

    public void setCommonEventLogs(Context context, String eventName)
    {
        try {
            AppsFlyerLib.trackEvent(context, eventName, null);

            if(logger!=null)
                logger.logEvent(eventName);

            KPApplication application= (KPApplication) KPApplication.getContext();
            application.setGoogleAnalyticsScreenName(eventName);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setFBLogBundleEvent(String eventName,Bundle eventBundle)
    {
        if(logger!=null)
            logger.logEvent(eventName);
    }

}
