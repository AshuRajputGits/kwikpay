package com.uk.recharge.kwikpay.gamingarcade;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.CommonStaticPages;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.gamingarcade.models.GameMachineInfoMO;
import com.uk.recharge.kwikpay.singleton.GamingInfoSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

/**
 * Created by Ashu Rajput on 7/7/2018.
 */

public class GamingTnCScreen extends AppCompatActivity {

    private GameMachineInfoMO gameMachineInfoMO = null;
    private CheckBox gamingTNCCheckBox;
    private boolean isTermConditionAccepted = false;
    private static final int TERM_CONDITION_CODE = 3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gaming_tnc);

        gameMachineInfoMO = GamingInfoSingleton.getInstance().getGameMachineInfoMO();
        updateTnCUI();
    }

    private void updateTnCUI() {

        gamingTNCCheckBox = findViewById(R.id.gamingTNCCheckBox);
        String tncMessage = "I accept the <i><u>terms &amp; conditions</u></i>";
        gamingTNCCheckBox.setText(Utility.fromHtml(tncMessage));
        findViewById(R.id.headerBackButton).setOnClickListener(onClickListener);
        findViewById(R.id.gamingTnCProceedButton).setOnClickListener(onClickListener);

        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        ImageView gamingOperatorLogoIV = findViewById(R.id.gamingOperatorLogoIV);

        try {
            Glide.with(this).load(gameMachineInfoMO.getOperatorLogo())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(gamingOperatorLogoIV);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((TextView) (findViewById(R.id.gamingOperatorLogoTV))).setText(gameMachineInfoMO.getOperatorDesc());
        ImageView gamingProviderLogoIV = findViewById(R.id.gamingProviderLogoIV);

        try {
            Glide.with(this).load(gameMachineInfoMO.getProviderImgURL())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(gamingProviderLogoIV);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.headerBackButton)
                GamingTnCScreen.this.finish();
            else if (v.getId() == R.id.gamingTNCCheckBox) {
                if (gamingTNCCheckBox.isChecked()) {
                    startActivityForResult(new Intent(GamingTnCScreen.this, CommonStaticPages.class)
                            .putExtra("STATIC_PAGE_NAME", "TNC_PAGE"), TERM_CONDITION_CODE);
                } else
                    isTermConditionAccepted = false;
            } else if (v.getId() == R.id.gamingTnCProceedButton) {
                if (!gamingTNCCheckBox.isChecked() && !isTermConditionAccepted) {
                    ARCustomToast.showToast(GamingTnCScreen.this, "please accept the terms & condition", Toast.LENGTH_LONG);
                } else {
                    getSharedPreferences("KP_Preference", MODE_PRIVATE).edit().putBoolean("IS_GAMING_TNC_ACCEPTED", true).apply();
                    startActivity(new Intent(GamingTnCScreen.this, GamingProductPricing.class));
                }
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TERM_CONDITION_CODE) {
            if (resultCode == RESULT_OK)
                isTermConditionAccepted = true;
            else
                isTermConditionAccepted = false;
        }
    }


}
