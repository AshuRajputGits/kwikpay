package com.uk.recharge.kwikpay.catering;

import android.app.Dialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Product;
import com.uk.recharge.kwikpay.RoomDataBase.Models.ProductType;
import com.uk.recharge.kwikpay.kwikcharge.qrscanner.BarcodeScannerActivity;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.utilities.PreferenceHelper;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;

public class CateringLaunchScreen extends KPSuperClass {

    private static final int REQUEST_CAMERA_RESULT = 10;
    private static final int REQUEST_SCANNER = 9;
    private static final String TAG = "CateringScreen";
    private SharedPreferences preference;
    Context context;
    private PreferenceHelper mPreferenceHelper;
    private String qrCODE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        initiateAppsBanner("HomeScreen");
        setUpResourceIDs();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_catering_launch_screen;
    }

    private void setUpResourceIDs() {
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        mPreferenceHelper = new PreferenceHelper(this);

        context = CateringLaunchScreen.this;

        findViewById(R.id.scanQRCodeTV).setOnClickListener(onClickListener);
        findViewById(R.id.enterrestrono).setOnClickListener(onClickListener);

        if (getIntent().hasExtra("QR_RESULT")) {
            qrCODE = getIntent().getStringExtra("QR_RESULT");
            if (qrCODE != null && !qrCODE.isEmpty())
                callCateringLocationIsGroupedAPI();
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.scanQRCodeTV)
                checkRTPForCamera();
            else if (v.getId() == R.id.enterrestrono)
                openRetroIdDialogBox();
        }
    };

    private void checkRTPForCamera() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                    startQRScanActivity();
                else {
                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA))
                        Toast.makeText(this, "No Permission enabled to use the Camera services", Toast.LENGTH_SHORT).show();
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, REQUEST_CAMERA_RESULT);
                }
            } else
                startQRScanActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Cannot run application because camera service permission have not been granted", Toast.LENGTH_SHORT).show();
                else
                    startQRScanActivity();
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void startQRScanActivity() {
        startActivityForResult(new Intent(context, BarcodeScannerActivity.class), REQUEST_SCANNER);
    }

    private void openRetroIdDialogBox() {
        final Dialog dialogPopup = new Dialog(context);
        dialogPopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogPopup.setContentView(R.layout.custom_catering_location_no);

        final EditText locationIdET = dialogPopup.findViewById(R.id.locationIdET);
        Button okButton = dialogPopup.findViewById(R.id.dialogButtonOK);
        Button cancelButton = dialogPopup.findViewById(R.id.dialogButtonCancel);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (locationIdET.getText().toString().isEmpty() || locationIdET.getText().toString().trim().equals("")) {
                    Toast.makeText(context, "Please enter valid location Id", Toast.LENGTH_LONG).show();
                } else {
                    dialogPopup.dismiss();
                    qrCODE = locationIdET.getText().toString();
                    callCateringLocationIsGroupedAPI();
                }
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPopup.dismiss();
            }
        });
        dialogPopup.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SCANNER) {
            if (resultCode == RESULT_OK) {
                qrCODE = data.getStringExtra("QR_RESULT");
                if (qrCODE != null && !qrCODE.isEmpty()) {
                    try {
                        qrCODE = qrCODE.split("-")[1];
                        callCateringLocationIsGroupedAPI();
                    } catch (Exception e) {
                        Crashlytics.logException(e);
                    }
                }
            }
        }
    }

    private void callCateringLocationIsGroupedAPI() {
        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "CATERING_LOCATION_IS_GROUPED");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("DEVICEOS", "ANDROID");
            json.put("LOCATION_NUMBER", qrCODE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {

                                    String locationGroupStatus = childJson.getString("LOCATION_GROUP_STATUS");
                                    if (locationGroupStatus != null && !locationGroupStatus.isEmpty()) {
                                        if (locationGroupStatus.equalsIgnoreCase("YES"))
                                            startActivity(new Intent(context, CateringGroupMerchants.class).putExtra("LOC_NUMBER", qrCODE));
                                        else
                                            callCateringProductByLocationNumAPI();
                                    }
                                } else
                                    ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                            } else
                                ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(context))
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(context))
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void callCateringProductByLocationNumAPI() {
        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "CATERING_PRODUCTS_BY_LOCATION_NUM");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("DEVICEOS", "ANDROID");
            json.put("LOCATION_NUMBER", qrCODE);
            json.put("USERID", sharedPreferences.getString("KP_USER_ID", "0"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {

                                    JSONObject distributor_details = childJson.getJSONObject("distributor_details");
                                    preference.edit().putString("DISTRIBUTOR_NAME", distributor_details.getString("distributor_name")).apply();
                                    preference.edit().putString("DISTRIBUTOR_ADDRESS", distributor_details.optString("ADDRESS_ONE") + " " + distributor_details.optString("CITY")
                                            + " " + distributor_details.optString("COUNTRY") + " " + distributor_details.optString("POSTCODE")).apply();
                                    preference.edit().putString("DISTRIBUTOR_IMAGE", distributor_details.getString("logo_img")).apply();
                                    ProjectConstant.tncURL = distributor_details.getString("URLTERMS");
                                    saveProductData(childJson);
                                    mPreferenceHelper.addString("locationid", qrCODE);
                                    mPreferenceHelper.addString("servicefee", childJson.getString("SERVICE_FEE"));
                                } else
                                    ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                            } else
                                ARCustomToast.showToast(context, childJson.optString("DESCRIPTION"));
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(context))
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(context))
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(context, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    void saveProductData(JSONObject productResponse) {
        AppDatabase appDatabase;
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        appDatabase.productDao().clear();
        appDatabase.productTypeDao().clear();
        appDatabase.ingredientDao().clear();
        appDatabase.ingredientTypeDao().clear();
        try {
            JSONArray productData = productResponse.getJSONArray("products");
            JSONArray IngredientData = null;
            if (productResponse.has("sub_products"))
                IngredientData = productResponse.getJSONArray("sub_products");
            List<Product> productList = new ArrayList<>();
            List<String> stringArrayList = new ArrayList<>();
            List<ProductType> productTypeList = new ArrayList<>();
            List<Ingredient> ingredientList = new ArrayList<>();
            List<IngredientType> ingredientTypeList = new ArrayList<>();

            for (int i = 0; i < productData.length(); i++) {
                String newProduct = productData.getJSONObject(i).getString("category");

                if (!stringArrayList.contains(newProduct)) {
                    Product product = new Product();
                    product.setName(newProduct);
                    productList.add(product);
                    stringArrayList.add(newProduct);
                    Log.d("ProductInMerchant", "Add Product " + product.getName());
                }

                ProductType productType = new ProductType();

                productType.setParent_id(newProduct);
                productType.setCategory_action(productData.getJSONObject(i).getString("category_action"));
                productType.setSize(productData.getJSONObject(i).getString("size"));
                productType.setProduct_id(productData.getJSONObject(i).getString("product_id"));
                productType.setProduct_description(productData.getJSONObject(i).getString("product_description"));
                productType.setImage(productData.getJSONObject(i).getString("image"));
                productType.setCore_price(productData.getJSONObject(i).getString("core_price"));
                productType.setAvailable_quantity(productData.getJSONObject(i).getString("available_quantity"));
                productType.setName(productData.getJSONObject(i).getString("brand_name"));
                productType.setUnit_price(productData.getJSONObject(i).getString("unit_price"));

                Log.d("ProductInMerchant", "Add productType " + productType.getName());

                productTypeList.add(productType);
            }
            stringArrayList.clear();

            if (IngredientData != null && IngredientData.length() > 0) {
                for (int i = 0; i < IngredientData.length(); i++) {
                    String newIngredient = IngredientData.getJSONObject(i).getString("sub_category");

                    if (!stringArrayList.contains(newIngredient)) {
                        Ingredient ingredient = new Ingredient();
                        ingredient.setName(newIngredient);
                        ingredient.setCategory_action(IngredientData.getJSONObject(i).getString("category_action"));
                        ingredient.setParent_id(IngredientData.getJSONObject(i).getString("category"));
                        ingredientList.add(ingredient);
                        stringArrayList.add(newIngredient);

                        Log.d("ProductInMerchant", "Add ingredient " + ingredient.getName());
                    }

                    IngredientType ingredientType = new IngredientType();

                    ingredientType.setParent_id(newIngredient);
                    ingredientType.setCategory_action(IngredientData.getJSONObject(i).getString("category_action"));
                    ingredientType.setSize(IngredientData.getJSONObject(i).getString("size"));
                    ingredientType.setProduct_id(IngredientData.getJSONObject(i).getString("product_id"));
                    ingredientType.setProduct_description(IngredientData.getJSONObject(i).getString("product_description"));
                    ingredientType.setImage(IngredientData.getJSONObject(i).getString("image"));
                    ingredientType.setCore_price(IngredientData.getJSONObject(i).getString("core_price"));
                    ingredientType.setAvailable_quantity(IngredientData.getJSONObject(i).getString("available_quantity"));
                    ingredientType.setName(IngredientData.getJSONObject(i).getString("brand_name"));
                    ingredientType.setUnit_price(IngredientData.getJSONObject(i).getString("unit_price"));
                    ingredientTypeList.add(ingredientType);
                    Log.d("ProductInMerchant", "Add ingredientType " + ingredientType.getName());
                }
            }
            appDatabase.productDao().insert(productList);
            appDatabase.productTypeDao().insert(productTypeList);
            if (ingredientList != null && ingredientList.size() > 0)
                appDatabase.ingredientDao().insert(ingredientList);
            if (ingredientTypeList != null && ingredientTypeList.size() > 0)
                appDatabase.ingredientTypeDao().insert(ingredientTypeList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (preference.getBoolean("IS_CATERING_TNC_ACCEPTED", false))
            startActivity(new Intent(context, RestoMenuList.class));
        else
            startActivity(new Intent(context, CateringTnCScreen.class));
    }

    /*void saveProductData(JSONObject productResponse) {
        AppDatabase appDatabase;
        appDatabase = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        appDatabase.productDao().clear();
        appDatabase.productTypeDao().clear();
        appDatabase.ingredientDao().clear();
        appDatabase.ingredientTypeDao().clear();
        try {
            JSONArray productData = productResponse.getJSONArray("products");
            JSONArray IngredientData = productResponse.getJSONArray("sub_products");
            List<Product> productList = new ArrayList<>();
            List<String> stringArrayList = new ArrayList<>();
            List<ProductType> productTypeList = new ArrayList<>();
            List<Ingredient> ingredientList = new ArrayList<>();
            List<IngredientType> ingredientTypeList = new ArrayList<>();

            for (int i = 0; i < productData.length(); i++) {
                String newProduct = productData.getJSONObject(i).getString("category");

                if (!stringArrayList.contains(newProduct)) {
                    Product product = new Product();
                    product.setName(newProduct);
                    productList.add(product);
                    stringArrayList.add(newProduct);
                    Log.d(TAG, "Add Product " + product.getName());
                }

                ProductType productType = new ProductType();

                productType.setParent_id(newProduct);
                productType.setCategory_action(productData.getJSONObject(i).getString("category_action"));
                productType.setSize(productData.getJSONObject(i).getString("size"));
                productType.setProduct_id(productData.getJSONObject(i).getString("product_id"));
                productType.setProduct_description(productData.getJSONObject(i).getString("product_description"));
                productType.setImage(productData.getJSONObject(i).getString("image"));
                productType.setCore_price(productData.getJSONObject(i).getString("core_price"));
                productType.setAvailable_quantity(productData.getJSONObject(i).getString("available_quantity"));
                productType.setName(productData.getJSONObject(i).getString("brand_name"));
                productType.setUnit_price(productData.getJSONObject(i).getString("unit_price"));

                Log.d(TAG, "Add productType " + productType.getName());

                productTypeList.add(productType);
            }
            stringArrayList.clear();

            for (int i = 0; i < IngredientData.length(); i++) {
                String newIngredient = IngredientData.getJSONObject(i).getString("sub_category");

                if (!stringArrayList.contains(newIngredient)) {
                    Ingredient ingredient = new Ingredient();
                    ingredient.setName(newIngredient);
                    ingredient.setCategory_action(IngredientData.getJSONObject(i).getString("category_action"));
                    ingredient.setParent_id(IngredientData.getJSONObject(i).getString("category"));
                    ingredientList.add(ingredient);
                    stringArrayList.add(newIngredient);

                    Log.d(TAG, "Add ingredient " + ingredient.getName());
                }

                IngredientType ingredientType = new IngredientType();

                ingredientType.setParent_id(newIngredient);
                ingredientType.setCategory_action(IngredientData.getJSONObject(i).getString("category_action"));
                ingredientType.setSize(IngredientData.getJSONObject(i).getString("size"));
                ingredientType.setProduct_id(IngredientData.getJSONObject(i).getString("product_id"));
                ingredientType.setProduct_description(IngredientData.getJSONObject(i).getString("product_description"));
                ingredientType.setImage(IngredientData.getJSONObject(i).getString("image"));
                ingredientType.setCore_price(IngredientData.getJSONObject(i).getString("core_price"));
                ingredientType.setAvailable_quantity(IngredientData.getJSONObject(i).getString("available_quantity"));
                ingredientType.setName(IngredientData.getJSONObject(i).getString("brand_name"));
                ingredientType.setUnit_price(IngredientData.getJSONObject(i).getString("unit_price"));

                ingredientTypeList.add(ingredientType);

                Log.d(TAG, "Add ingredientType " + ingredientType.getName());
            }
            appDatabase.productDao().insert(productList);
            appDatabase.productTypeDao().insert(productTypeList);
            appDatabase.ingredientDao().insert(ingredientList);
            appDatabase.ingredientTypeDao().insert(ingredientTypeList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (preference.getBoolean("IS_CATERING_TNC_ACCEPTED", false))
            startActivity(new Intent(context, RestoMenuList.class));
        else
            startActivity(new Intent(context, CateringTnCScreen.class));
    }*/

}
