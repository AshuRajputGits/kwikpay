package com.uk.recharge.kwikpay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidationWithMessage;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HP on 04-10-2016.
 */
public class BillingAddressScreen extends Activity implements AsyncRequestListenerViaPost {

    //    private EditText firstNameET, lastNameET;
    private EditText postcodeET;
    private Spinner fullAddressSpinner;
    private Button proceedButton;
    //    private TextView address1TV,address2TV,address3TV,townTV,cityTV;
    private EditText address1TV, address2TV, address3TV, townTV, cityTV;
    //    private TextView editAddress,completeAddress;
    private SharedPreferences preference;
    private int apiHitPosition = 0;
    private LinearLayout billingEntryAddressLayout, onlyBillingAddressLayout;
    private boolean isAddressInEditMode = false, noAddressFound = false;
    private ArrayList<String> addressList;
    private Button billingGoButton;
    private boolean receivedAddressStatus = false;
    private String firstNameValue = "", lastNameValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.billing_address);

        //SETTING ID'S VIA findViewById()
        setUpIDs();

        receivedAddressStatus = getIntent().getBooleanExtra("ADDRESS_FOUND_STATUS", false);

        apiHitPosition = 1;
        new LoadResponseViaPost(this, formGetAddressByID(), true).execute("");

        proceedButton.setOnClickListener(clickListener);
//        editAddress.setOnClickListener(clickListener);
        billingGoButton.setOnClickListener(clickListener);

        fullAddressSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    // commenting as per new requirement
//                    proceedButton.setVisibility(View.VISIBLE);
//                    onlyBillingAddressLayout.setVisibility(View.VISIBLE);

                    try {
                        String selectedAddressFromSpinner = fullAddressSpinner.getSelectedItem().toString();
                        String[] subAddressArray = selectedAddressFromSpinner.split(",");
                        address1TV.setText(subAddressArray[0]);
                        address2TV.setText(subAddressArray[1]);
                        address3TV.setText(subAddressArray[2]);
                        townTV.setText(subAddressArray[5]);
                        cityTV.setText(subAddressArray[6]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // commenting as per new requirement
//                    proceedButton.setVisibility(View.GONE);
//                    onlyBillingAddressLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        postcodeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int finalLength, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                runCleanMaster();
                /*if (s.length() == 7 && count > 0) {
                    runCleanMaster();
                }  */
            }

            @Override
            public void afterTextChanged(Editable mobNumber) {
                // TODO Auto-generated method stub
               /* if (mobNumber.length() == 7) {
                    hideKeyboard(postcodeET);
                }*/
            }
        });

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_ADDRESS_SCREEN));
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }

    }

    private void runCleanMaster() {
        fullAddressSpinner.setVisibility(View.GONE);
        address1TV.setText("");
        address2TV.setText("");
        address3TV.setText("");
        townTV.setText("");
        cityTV.setText("");
    }

    private void setUpIDs() {
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        postcodeET = findViewById(R.id.billingPostCode);
        fullAddressSpinner = findViewById(R.id.billingFullAddress);
        address1TV = findViewById(R.id.billingAddress1TV);
        address2TV = findViewById(R.id.billingAddress2TV);
        address3TV = findViewById(R.id.billingAddress3TV);
        townTV = findViewById(R.id.billingTownTV);
        cityTV = findViewById(R.id.billingCityTV);

        billingEntryAddressLayout = findViewById(R.id.billingEntryAddressLayout);
        onlyBillingAddressLayout = findViewById(R.id.billingAddressLL);
        proceedButton = findViewById(R.id.billingProceedButton);
        billingGoButton = findViewById(R.id.billingGoButton);

        TextView headerTitle = findViewById(R.id.mainHeaderTitleName);
        headerTitle.setText("Billing address");

        //SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);

        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int ID = v.getId();

            if (ID == R.id.billingProceedButton) {
                validateIfNoAddressFound();

               /* if (isAddressInEditMode) {
                    //Call Save API
                    apiHitPosition = 2;
                    new LoadResponseViaPost(BillingAddressScreen.this, formSaveAddressJSON(), true).execute("");
                }
               *//* else if(noAddressFound)
                {
                    validateIfNoAddressFound();
                }*//*
                else {
                    //normal exit by sending RESULT_OK
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("RESPONSE", true);
                    setResult(RESULT_OK, returnIntent);
                    BillingAddressScreen.this.finish();
                }*/


            }
            /*else if(ID==R.id.billingCompleteAddressEditTV)
            {
                isAddressInEditMode=true;
                editAddress.setVisibility(View.GONE);
                completeAddress.setVisibility(View.GONE);
                billingEntryAddressLayout.setVisibility(View.VISIBLE);
                billingGoButton.setVisibility(View.VISIBLE);
                onlyBillingAddressLayout.setVisibility(View.GONE);
                proceedButton.setVisibility(View.GONE);
            }*/
            else if (ID == R.id.billingGoButton) {
//                validateIfNoAddressFound();
                validatePostCodeField();
            }
        }
    };

    private String formGetAddressByID() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("APISERVICE", "KP_GETADDRESS_BY_USERID");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonObject.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonObject.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonObject.put("SOURCENAME", "KPAPP");
            jsonObject.put("USERID", preference.getString("KP_USER_ID", "0"));
            jsonObject.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", "0"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private String formPostCodeGetAddressJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("APISERVICE", "KP_GETADDRESS_BY_POSTCODE");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonObject.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonObject.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonObject.put("SOURCENAME", "KPAPP");
            jsonObject.put("INPUT_POSTCODE", postcodeET.getText().toString());
            jsonObject.put("USERID", preference.getString("KP_USER_ID", "0"));
            jsonObject.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", "0"));
//            jsonObject.put("EMAILID", "prashant@kwikpay.co.uk");
//            jsonObject.put("USERID","44");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private String formSaveAddressJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("APISERVICE", "KP_SAVEADDRESS_BY_USERID");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonObject.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonObject.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonObject.put("SOURCENAME", "KPAPP");
            jsonObject.put("POSTALCODE", postcodeET.getText().toString().toUpperCase());
            jsonObject.put("USERID", preference.getString("KP_USER_ID", "0"));
            jsonObject.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", "0"));
            jsonObject.put("COUNTRY", "");
            jsonObject.put("STATE", "");
            jsonObject.put("CITY", cityTV.getText().toString());
            jsonObject.put("POSTALTOWN", townTV.getText().toString());
            jsonObject.put("FIRSTNAME", firstNameValue);
            jsonObject.put("LASTNAME", lastNameValue);
            jsonObject.put("ADDRESSONE", address1TV.getText().toString());
            jsonObject.put("ADDRESSTWO", address2TV.getText().toString());
            jsonObject.put("ADDRESSTHREE", address3TV.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private void validatePostCodeField() {
        if (postcodeET.getText().toString().isEmpty()) {
//            FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
//            FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
            ARCustomToast.showToast(this, "please enter postal code", Toast.LENGTH_LONG);
        } else {
//            FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
//            FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
            FieldValidationWithMessage.removeAllErrorIcons(postcodeET);
            // CALLING API TO GET DETAILS OF THE ADDRESS WITH RESPECT TO POSTAL CODE, ENTER BY USER
            apiHitPosition = 3;
            new LoadResponseViaPost(this, formPostCodeGetAddressJSON(), true).execute("");
        }
    }

    private void validateIfNoAddressFound() {
        /*if (!FieldValidationWithMessage.firstNameIsValid(firstNameET.getText().toString(), firstNameET).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
            FieldValidationWithMessage.removeAllErrorIcons(postcodeET);
            FieldValidationWithMessage.removeAllErrorIcons(address1TV);
            FieldValidationWithMessage.removeAllErrorIcons(address2TV);

            ARCustomToast.showToast(this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
        } else if (!FieldValidationWithMessage.lastNameIsValid(lastNameET.getText().toString(), lastNameET).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
            FieldValidationWithMessage.removeAllErrorIcons(postcodeET);
            FieldValidationWithMessage.removeAllErrorIcons(address1TV);
            FieldValidationWithMessage.removeAllErrorIcons(address2TV);

            ARCustomToast.showToast(this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
        }*/

        if (postcodeET.getText().toString().isEmpty()) {

            postcodeET.setCompoundDrawablesWithIntrinsicBounds(0, 0, com.ar.library.R.drawable.error_img, 0);
//            FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
//            FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
            FieldValidationWithMessage.removeAllErrorIcons(address1TV);
            FieldValidationWithMessage.removeAllErrorIcons(address2TV);
            ARCustomToast.showToast(this, "please enter valid postal code", Toast.LENGTH_LONG);
        }
       /* else if(fullAddressSpinner.isShown() && fullAddressSpinner.getSelectedItemPosition()==0)
        {
            ARCustomToast.showToast(this, "please select address", Toast.LENGTH_LONG);
        }*/
        else if (address1TV.getText().toString().isEmpty()) {

            address1TV.setCompoundDrawablesWithIntrinsicBounds(0, 0, com.ar.library.R.drawable.error_img, 0);
//            FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
//            FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
            FieldValidationWithMessage.removeAllErrorIcons(postcodeET);
            FieldValidationWithMessage.removeAllErrorIcons(address2TV);
            ARCustomToast.showToast(this, "please enter valid address", Toast.LENGTH_LONG);
        } else if (address2TV.getText().toString().isEmpty()) {
            address2TV.setCompoundDrawablesWithIntrinsicBounds(0, 0, com.ar.library.R.drawable.error_img, 0);
//            FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
//            FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
            FieldValidationWithMessage.removeAllErrorIcons(postcodeET);
            FieldValidationWithMessage.removeAllErrorIcons(address1TV);
            ARCustomToast.showToast(this, "please enter valid address", Toast.LENGTH_LONG);
        } else if (townTV.getText().toString().isEmpty()) {
            townTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, com.ar.library.R.drawable.error_img, 0);
//            FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
//            FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
            FieldValidationWithMessage.removeAllErrorIcons(postcodeET);
            FieldValidationWithMessage.removeAllErrorIcons(address1TV);
            FieldValidationWithMessage.removeAllErrorIcons(address2TV);
            FieldValidationWithMessage.removeAllErrorIcons(cityTV);
            ARCustomToast.showToast(this, "please enter valid town", Toast.LENGTH_LONG);
        } else if (cityTV.getText().toString().isEmpty()) {
            cityTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, com.ar.library.R.drawable.error_img, 0);
//            FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
//            FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
            FieldValidationWithMessage.removeAllErrorIcons(postcodeET);
            FieldValidationWithMessage.removeAllErrorIcons(address1TV);
            FieldValidationWithMessage.removeAllErrorIcons(address2TV);
            FieldValidationWithMessage.removeAllErrorIcons(townTV);
            ARCustomToast.showToast(this, "please enter valid city", Toast.LENGTH_LONG);
        } else {
            cleanAllErrorIcons();
            apiHitPosition = 2;
            new LoadResponseViaPost(BillingAddressScreen.this, formSaveAddressJSON(), true).execute("");
        }
    }

    private void cleanAllErrorIcons() {
//        FieldValidationWithMessage.removeAllErrorIcons(firstNameET);
//        FieldValidationWithMessage.removeAllErrorIcons(lastNameET);
        FieldValidationWithMessage.removeAllErrorIcons(postcodeET);
        FieldValidationWithMessage.removeAllErrorIcons(address1TV);
        FieldValidationWithMessage.removeAllErrorIcons(address2TV);
        FieldValidationWithMessage.removeAllErrorIcons(townTV);
        FieldValidationWithMessage.removeAllErrorIcons(cityTV);
    }

    @Override
    public void onRequestComplete(String loadedString) {
        Log.e("Response", "Bill Response " + loadedString);

        if (!loadedString.equals("Exception") && !loadedString.equals("")) {
            if (apiHitPosition == 1)
                parseAddressByUserID(loadedString);
            else if (apiHitPosition == 2)
                parseSaveAddressResponse(loadedString);
            else if (apiHitPosition == 3)
                parseAddressByPostalCode(loadedString);

        } else {
            if (apiHitPosition == 1) {
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
                BillingAddressScreen.this.finish();
            } else {
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        }
    }

    private void parseSaveAddressResponse(String loadedString) {
        try {
            JSONObject jsonObject, parentResponseObject;
            jsonObject = new JSONObject(loadedString);
            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    Log.e("Response", "PostCode " + postcodeET.getText().toString());

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("RESPONSE", true);
                    returnIntent.putExtra("REFRESH_POSTAL_CODE", postcodeET.getText().toString());
                    setResult(RESULT_OK, returnIntent);
                    BillingAddressScreen.this.finish();
                } else {
                    if (parentResponseObject.has("DESCRIPTION"))
                        ARCustomToast.showToast(this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                }
            } else {
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void parseAddressByUserID(String loadedString) {
        try {
            JSONObject jsonObject, parentResponseObject;
            jsonObject = new JSONObject(loadedString);
            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    postcodeET.setText(parentResponseObject.optString("POSTALCODE"));
                    firstNameValue = parentResponseObject.optString("FIRSTNAME");
                    lastNameValue = parentResponseObject.optString("LASTNAME");
//                    firstNameET.setText(parentResponseObject.optString("FIRSTNAME"));
//                    lastNameET.setText(parentResponseObject.optString("LASTNAME"));
//                    address1TV.setText(parentResponseObject.optString("ADDRESSONE"));
//                    address2TV.setText(parentResponseObject.optString("ADDRESSTWO"));
//                    address3TV.setText(parentResponseObject.optString("ADDRESSTHREE"));
//                    townTV.setText(parentResponseObject.optString("POSTALTOWN"));
//                    cityTV.setText(parentResponseObject.optString("CITY"));
//                    firstNameET.setKeyListener(null);
//                    lastNameET.setKeyListener(null);
//                    postcodeET.setKeyListener(null);

                    /*StringBuffer stringBuffer=new StringBuffer();
                    stringBuffer.append(parentResponseObject.optString("FIRSTNAME") +" ");
                    stringBuffer.append(parentResponseObject.optString("LASTNAME") +"\n");
                    stringBuffer.append(parentResponseObject.optString("ADDRESSONE") +" ");
                    stringBuffer.append(parentResponseObject.optString("ADDRESSTWO") +" ");
                    stringBuffer.append(parentResponseObject.optString("ADDRESSTHREE") +" \n");
                    stringBuffer.append(parentResponseObject.optString("CITY") +" ");
                    stringBuffer.append(parentResponseObject.optString("POSTALCODE") );
                    completeAddress.setText(stringBuffer);*/

//                    proceedButton.setVisibility(View.VISIBLE);
//                    billingGoButton.setVisibility(View.GONE);


                    //commenting as per new requirement
                    /*billingEntryAddressLayout.setVisibility(View.VISIBLE);
                    billingGoButton.setVisibility(View.VISIBLE);
                    onlyBillingAddressLayout.setVisibility(View.GONE);
                    proceedButton.setVisibility(View.GONE);*/

                } else {
//                    noAddressFound=true;
//                    editAddress.setVisibility(View.GONE);
//                    completeAddress.setVisibility(View.GONE);
                    billingEntryAddressLayout.setVisibility(View.VISIBLE);
                }
            } else {
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
                BillingAddressScreen.this.finish();
            }

        } catch (Exception e) {

        }
    }

    private void parseAddressByPostalCode(String loadedString) {
        try {
            JSONObject jsonObject, parentResponseObject;
            jsonObject = new JSONObject(loadedString);
            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    cleanAllErrorIcons();
                    fullAddressSpinner.setVisibility(View.VISIBLE);
//                    noAddressFound=false;
                    isAddressInEditMode = true;

                    if (parentResponseObject.has("ADDRESSES")) {
                        JSONArray addressArray = parentResponseObject.getJSONArray("ADDRESSES");
                        addressList = new ArrayList<>(addressArray.length() + 1);
                        for (int i = 0; i < addressArray.length(); i++) {
                            JSONObject addressJsonObject = addressArray.getJSONObject(i);
                            addressList.add(addressJsonObject.getString("ADDRESS"));
                        }

                        addressList.add(0, "Select Address");
                        ArrayAdapter<String> addressAdapter = new ArrayAdapter<>(this, R.layout.spinner_textsize, addressList);
                        addressAdapter.setDropDownViewResource(R.layout.single_row_spinner);
                        fullAddressSpinner.setAdapter(addressAdapter);

                        //commenting as per new requirement
//                        billingGoButton.setVisibility(View.GONE);
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION"))
                        ARCustomToast.showToast(this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                }
            } else {
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }

        } catch (Exception e) {

        }
    }

    private void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
