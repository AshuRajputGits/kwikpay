package com.uk.recharge.kwikpay.singleton;

import com.uk.recharge.kwikpay.kwikvend.models.KVProductDetailsMO;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductOthersDetailsMO;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 7/11/2018.
 */

public class KVProductDetailsSingleton {

    private static KVProductDetailsSingleton ourInstance = null;
    private ArrayList<KVProductDetailsMO> kvProductDetailsMOList = null;
    private KVProductOthersDetailsMO kvProductOthersDetailsMO=null;

    public static KVProductDetailsSingleton getInstance() {
        if (ourInstance == null)
            ourInstance = new KVProductDetailsSingleton();
        return ourInstance;
    }

    private KVProductDetailsSingleton() {
    }

    public ArrayList<KVProductDetailsMO> getKvProductDetailsMOList() {
        return kvProductDetailsMOList;
    }

    public void setKvProductDetailsMOList(ArrayList<KVProductDetailsMO> kvProductDetailsMOList) {
        this.kvProductDetailsMOList = kvProductDetailsMOList;
    }

    public void clearKVProductList() {
        if (kvProductDetailsMOList != null && kvProductDetailsMOList.size() > 0)
            kvProductDetailsMOList.clear();
    }

    public KVProductOthersDetailsMO getKvProductOthersDetailsMO() {
        return kvProductOthersDetailsMO;
    }

    public void setKvProductOthersDetailsMO(KVProductOthersDetailsMO kvProductOthersDetailsMO) {
        this.kvProductOthersDetailsMO = kvProductOthersDetailsMO;
    }
}
