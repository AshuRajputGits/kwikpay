package com.uk.recharge.kwikpay.services;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResultFromGetMethod;
import com.ar.library.asynctaskclasses.LoadResultFromGetMethod.AsyncTaskCompleteListenerForGetMethod;
import com.google.android.gms.maps.model.LatLng;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.utilities.MapUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HP on 22-12-2016.
 */
public class DrawRoutePathService extends IntentService implements AsyncTaskCompleteListenerForGetMethod {

    public DrawRoutePathService() {
        super("DrawRoutePathService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(1,new Notification());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String sourceLatitude = intent.getStringExtra("SourceLatitude");
            String sourceLongitude = intent.getStringExtra("SourceLongitude");
            String destinationLatitude = intent.getStringExtra("DestinationLatitude");
            String destinationLongitude = intent.getStringExtra("DestinationLongitude");

            String createdPathURL = createURL(sourceLatitude, sourceLongitude, destinationLatitude, destinationLongitude);

//            Log.e("MapPath", "PathCreated:  " + createdPathURL);
            executeRoutePathURL(createdPathURL);
        }
    }

    private void executeRoutePathURL(String createdPathURL) {
        new LoadResultFromGetMethod(this, true).execute(createdPathURL);
    }

    public String createURL(String sourceLatitude, String sourceLongitude, String destinationLatitude, String destinationLongitude) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(sourceLatitude);
        urlString.append(",");
        urlString.append(sourceLongitude);
        urlString.append("&destination=");// to
        urlString.append(destinationLatitude);
        urlString.append(",");
        urlString.append(destinationLongitude);
        urlString.append("&sensor=false&mode=driving&alternatives=true");
//        urlString.append("&key=AIzaSyCmfBUWxfzc-N_pdXkbkWstaAzo-e2Enbc"); // prev
        urlString.append("&key=AIzaSyD4R6TzcT9mRnB8rRZJM71hCUHsnqTOe64");
//        urlString.append("&key=AIzaSyBkh-L6wXAqC01lqGfbgS62gwQ20pA06vo");
        return urlString.toString();
    }

    @Override
    public void onLoadComplete(String loadedString) {
        if (loadedString.equals("") || loadedString == null || loadedString.equals("Exception")) {
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        } else {
            try {
                generateLatLongList(loadedString);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void generateLatLongList(String result) {
        try {
            Log.e("Response", "result--> " + result);

            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            if (routeArray != null && routeArray.length() > 0) {

                JSONObject routes = routeArray.getJSONObject(0);
                JSONObject overviewPolyLines = routes.getJSONObject("overview_polyline");
                String encodedString = overviewPolyLines.getString("points");
                ArrayList<LatLng> list = MapUtility.decodePolyLines(encodedString);

                Intent intent = new Intent("com.ar.draw.route.path");
                intent.putExtra("PathResponse", list);
                LocalBroadcastManager.getInstance(DrawRoutePathService.this).sendBroadcast(intent);
            }

        } catch (JSONException e) {
            e.printStackTrace();
//            FirebaseCrash.report(e);
        } catch (Exception e) {
            e.printStackTrace();
//            FirebaseCrash.report(e);
        }
    }
}
