package com.uk.recharge.kwikpay.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.uk.recharge.kwikpay.ProjectConstant;

public class DatabaseTables extends SQLiteOpenHelper
{
	private static String dbname="KwikPay0.db";
	private static int version=3;
	
	// during upload on google play store : change version=2

	private static final String CREATE_AREA_GROUP_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_AG_TABLENAME + "(" 
			+ ProjectConstant.AG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," 
			+ ProjectConstant.AG_ENABLE + " TEXT," 
			+ ProjectConstant.AG_GROUP_CODE + " TEXT,"
			+ ProjectConstant.AG_NAME + " TEXT," 
			+ ProjectConstant.AG_CURRENCY + " TEXT," 
			+ ProjectConstant.AG_PREFIX_ALLOW + " TEXT,"
			+ ProjectConstant.AG_COUNTRY_CODE + " TEXT," 
			+ ProjectConstant.AG_SUBS_TERM_USED + " TEXT," 
			+ ProjectConstant.AG_SUBS_DATA_TYPE + " TEXT,"
			+ ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO + " TEXT," 
			+ ProjectConstant.AG_SUBS_MIN_LENGTH + " TEXT," 
			+ ProjectConstant.AG_SUBS_MAX_LENGTH + " TEXT,"
			+ ProjectConstant.AG_PREFIX_LENGTH + " TEXT," 
			+ ProjectConstant.AG_SECOND_VALIDATION + " TEXT,"
			+ ProjectConstant.AG_CF_ACTIVE_FLAG+ " TEXT" 
			+ ")";

	private static final String CREATE_OPERATOR_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_OPERATOR_TABLENAME + "(" 
			+ ProjectConstant.OPERATOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," 
			+ ProjectConstant.OP_OPERATOR_ID + " TEXT," 
			+ ProjectConstant.OPERATOR_ABBR + " TEXT," 
			+ ProjectConstant.OPERATOR_HLR_CODE + " TEXT," 
			+ ProjectConstant.OPERATOR_NAME + " TEXT," 
			+ ProjectConstant.OPERATOR_ENABLE + " TEXT," 
			+ ProjectConstant.OPERATOR_CF_ACTIVE_FLAG + " TEXT" 
			+ ")";

	private static final String CREATE_OPR_AREA_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_OP_AREA_TABLENAME + "(" 
			+ ProjectConstant.OA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," 
			+ ProjectConstant.OA_AG_ID + " TEXT," 
			+ ProjectConstant.OA_OS_ID + " TEXT," 
			+ ProjectConstant.OA_CONFIGURED + " TEXT," 
			+ ProjectConstant.OA_HLR_CODE + " TEXT," 
			+ ProjectConstant.OA_ENABLE + " TEXT," 
			+ ProjectConstant.OA_REQ_TYPE + " TEXT," 
			+ ProjectConstant.OA_LOGO + " TEXT,"
			+ ProjectConstant.OA_CF_ACTIVE_FLAG + " TEXT" 
			+ ")";

	/*private static final String CREATE_OPR_SERVICE_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_OP_SERVICE_TABLENAME + "(" 
			+ ProjectConstant.OS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," 
			+ ProjectConstant.OS_OP_ID + " TEXT," 
			+ ProjectConstant.OS_SUB_TERM_USED + " TEXT," 
			+ ProjectConstant.OS_SUBCR_DATATYPE + " TEXT," 
			+ ProjectConstant.OS_SUBCR_ALLOW_LEADING + " TEXT," 
			+ ProjectConstant.OS_SUBCR_MIN_LEN + " TEXT," 
			+ ProjectConstant.OS_SUBCR_MAX_LEN + " TEXT," 
			+ ProjectConstant.OS_PREFIX_LEN + " TEXT," 
			+ ProjectConstant.OS_CF_ACTION_STATUS + " TEXT,"
			+ ProjectConstant.OS_SERVICE_START_FLAG + " TEXT," 
			+ ProjectConstant.OS_SERVICE_START_TIME + " TEXT," 
			+ ProjectConstant.OS_SERVICE_STOP_TIME + " TEXT," 
			+ ProjectConstant.OS_SECOND_VALIDATION + " TEXT," 
			+ ProjectConstant.OS_IS_PLAN_RANGE + " TEXT,"
			+ ProjectConstant.OS_ENABLE + " TEXT," 
			+ ProjectConstant.OS_GUIDELINE + " TEXT," 
			+ ProjectConstant.OS_GUIDELINE1 + " TEXT," 
			+ ProjectConstant.OS_CF_ACTIVE_FLAG + " TEXT" 
			+ ")";*/

	/*private static final String CREATE_FAV_TRANSACTION_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_FAV_TRANS_TABLENAME + "(" 
			+ ProjectConstant.FAV_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," 
			+ ProjectConstant.FAV_ORDER_ID + " TEXT," 
			+ ProjectConstant.FAV_NICK_NAME + " TEXT," 
			+ ProjectConstant.FAV_OPERATOR_ID + " TEXT," 
			+ ProjectConstant.FAV_SERVICE_ID + " TEXT," 
			+ ProjectConstant.FAV_AG_ID + " TEXT," 
			+ ProjectConstant.FAV_SUBSCRIPTION_NUM + " TEXT," 
			+ ProjectConstant.FAV_AMT + " TEXT," 
			+ ProjectConstant.FAV_RU_ID + " TEXT," 
			+ ProjectConstant.FAV_ENABLE + " TEXT," 
			+ ProjectConstant.FAV_REGISTRATION_SOURCE + " TEXT," 
			+ ProjectConstant.FAV_DEVICE_OS + " TEXT,"
			+ ProjectConstant.FAV_CTS + " TEXT"
			+ ")";*/

	/*private static final String CREATE_TRANSACTION_HISTORY_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_TRANSACTION_HISTORY + "(" 
			+ ProjectConstant.TXN_TRANSACTIONDATETIME + " TEXT," 
			+ ProjectConstant.TXN_ORDERID + " TEXT," 
			+ ProjectConstant.TXN_SERVICENAME + " TEXT," 
			+ ProjectConstant.TXN_OPERATORNAME + " TEXT," 
			+ ProjectConstant.TXN_SUBSCRIPTIONNUMBER + " TEXT," 
			+ ProjectConstant.TXN_RECHARGEAMOUNT + " TEXT," 
			+ ProjectConstant.TXN_PAYMENTAMOUNT + " TEXT," 
			+ ProjectConstant.TXN_PAYMENTSTATUS + " TEXT," 
			+ ProjectConstant.TXN_RECHARGESTATUS + " TEXT," 
			+ ProjectConstant.TXN_SERVICEID + " TEXT," 
			+ ProjectConstant.TXN_OPERATORCODE + " TEXT," 
			+ ProjectConstant.TXN_COUNTRYCODE + " TEXT,"
			+ ProjectConstant.TXN_DISPLAYAMOUNT + " TEXT," 
			+ ProjectConstant.TXN_DISPLAYCURRENCYCODE + " TEXT," 
			+ ProjectConstant.TXN_RECHARGECURRENCYCODE + " TEXT," 
			+ ProjectConstant.TXN_PAYMENTCURRENCYCODE + " TEXT," 
			+ ProjectConstant.TXN_PARTYCODE + " TEXT," 
			+ ProjectConstant.TXN_CPNSRNUM + " TEXT," 
			+ ProjectConstant.TXN_CPNVALUE + " TEXT," 
			+ ProjectConstant.TXN_CPNUNIQUETRANSID + " TEXT," 
			+ ProjectConstant.PAYNOW_SERVICEFEE + " TEXT," 
			+ ProjectConstant.PAYNOW_PROCESSINGFEE + " TEXT,"
			+ ProjectConstant.RECEIPT_RECHARGEPIN + " TEXT,"
			+ ProjectConstant.TXN_RECHARGEPINEXPIRYDATE + " TEXT"
			+ ")";*/

	private static final String CREATE_SAVE_CART_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_SAVE_CART + "(" 
			
			+ ProjectConstant.CART_CARTID + " INTEGER PRIMARY KEY NOT NULL," 
			+ ProjectConstant.CART_MOBILENUMBER + " TEXT," 
			+ ProjectConstant.TXN_RECHARGEAMOUNT + " TEXT," 
			+ ProjectConstant.TXN_DISPLAYAMOUNT + " TEXT," 
			+ ProjectConstant.CART_NETPGAMOUNT + " TEXT," 
			+ ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID + " TEXT," 
			+ ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID + " TEXT," 
			+ ProjectConstant.TOPUP_API_PAYMENTCURRENCYAGID + " TEXT," 
			+ ProjectConstant.CART_CPNTXNID + " TEXT," 
			+ ProjectConstant.CART_CPNSRNO + " TEXT," 
			+ ProjectConstant.CART_CPNAMOUNT + " TEXT," 
			+ ProjectConstant.CART_CPNMESSAGE + " TEXT," 
			+ ProjectConstant.TXN_SERVICEID + " TEXT," 
			+ ProjectConstant.TXN_OPERATORCODE + " TEXT," 
			+ ProjectConstant.TXN_COUNTRYCODE + " TEXT,"
			+ ProjectConstant.CART_DISCOUNTAMOUNT + " TEXT," 
			+ ProjectConstant.CART_PGDISCOUNTAMOUNT + " TEXT," 
			+ ProjectConstant.PG_PGSOID + " TEXT," 
			+ ProjectConstant.PG_MOPID + " TEXT,"
			+ ProjectConstant.APP_SESSIONID + " TEXT," 
			+ ProjectConstant.PAYNOW_PROCESSINGFEE + " TEXT,"
			+ ProjectConstant.PAYNOW_SERVICEFEE + " TEXT," 
			+ ProjectConstant.PAYNOW_PLANDESCRIPTION + " TEXT," 
			+ ProjectConstant.TXN_OPERATORNAME + " TEXT," 
			+ ProjectConstant.CART_INSERTIONDATE + " TEXT," 
			+ ProjectConstant.CART_UPDATEDDATE + " TEXT," 
			+ ProjectConstant.TXN_DISPLAYCURRENCYCODE + " TEXT," 
			+ ProjectConstant.TXN_RECHARGECURRENCYCODE + " TEXT," 
			+ ProjectConstant.TXN_PAYMENTCURRENCYCODE + " TEXT" 
			+ ")";

	private static final String CREATE_OPR_SERVICE_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_OP_SERVICE_TABLENAME + "(" 
			+ ProjectConstant.OS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," 
			+ ProjectConstant.OS_OP_ID + " INTEGER," 
			+ ProjectConstant.OS_SERVICE_ID + " TEXT," 
			+ ProjectConstant.OS_OP_LOGO + " TEXT," 
			+ ProjectConstant.OS_ENABLE + " TEXT," 
			+ ProjectConstant.OS_IS_PLAN_RANGE + " INTEGER DEFAULT 0,"
			+ ProjectConstant.OS_CF_ACTIVE_FLAG + " TEXT,"
			+ " FOREIGN KEY ("+ProjectConstant.OS_OP_ID+") REFERENCES "+ProjectConstant.DB_OPERATOR_TABLENAME+
			" ("+ProjectConstant.OPERATOR_ID +")"
			+ ")";

	private static final String CREATE_PHI_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_KP_PHI_TABLENAME + "(" 
			+ ProjectConstant.KP_PHI + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," 
			+ ProjectConstant.KP_PHI_HEADER_CONTENT + " TEXT," 
			+ ProjectConstant.KP_PHI_SCREEN_NAME + " TEXT" 
			+ ")";

	private static final String CREATE_APPTABLE_VERSION_TABLE = "CREATE TABLE "
			+ ProjectConstant.DB_APP_TABLE_VERSION_TABLENAME + "(" 
			+ ProjectConstant.DB_HOW_TO_USE_REVISED + " TEXT," 
			+ ProjectConstant.DB_OPERATOR_TABLENAME + " TEXT," 
			+ ProjectConstant.DB_OP_AREA_TABLENAME + " TEXT," 
			+ ProjectConstant.DB_AG_TABLENAME + " TEXT," 
			+ ProjectConstant.DB_OP_SERVICE_TABLENAME + " TEXT," 
			+ ProjectConstant.DB_TERMS_OF_USAGE_REVISED + " TEXT," 
			+ ProjectConstant.DB_HOW_IT_WORKS_REVISED + " TEXT," 
			+ ProjectConstant.DB_ABOUTUS_REVISED + " TEXT," 
			+ ProjectConstant.DB_KP_PHI_TABLENAME + " TEXT" 
			+ ")";

	public DatabaseTables(Context context) 
	{
		super(context, dbname, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		db.execSQL(CREATE_AREA_GROUP_TABLE);      
		db.execSQL(CREATE_OPERATOR_TABLE);
		db.execSQL(CREATE_OPR_AREA_TABLE);
		db.execSQL(CREATE_OPR_SERVICE_TABLE);
		db.execSQL(CREATE_PHI_TABLE);
		db.execSQL(CREATE_APPTABLE_VERSION_TABLE);
		db.execSQL(CREATE_SAVE_CART_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		db.execSQL("DROP TABLE IF EXISTS " +ProjectConstant.DB_AG_TABLENAME);      
		db.execSQL("DROP TABLE IF EXISTS " +ProjectConstant.DB_OPERATOR_TABLENAME); 
		db.execSQL("DROP TABLE IF EXISTS " +ProjectConstant.DB_OP_AREA_TABLENAME); 
		db.execSQL("DROP TABLE IF EXISTS " +ProjectConstant.DB_OP_SERVICE_TABLENAME);
		db.execSQL("DROP TABLE IF EXISTS " +ProjectConstant.DB_KP_PHI_TABLENAME);
		db.execSQL("DROP TABLE IF EXISTS " +ProjectConstant.DB_APP_TABLE_VERSION_TABLENAME);
        db.execSQL("DROP TABLE IF EXISTS " +ProjectConstant.DB_SAVE_CART);

		onCreate(db);
	}


}