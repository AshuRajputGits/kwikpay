package com.uk.recharge.kwikpay;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.asynctaskclasses.LoadResultFromGetMethod;
import com.ar.library.asynctaskclasses.LoadResultFromGetMethod.AsyncTaskCompleteListenerForGetMethod;
import com.ar.library.model.DeviceAppInformation;
import com.ar.library.validation.FieldValidation;
import com.ar.library.validation.FieldValidationWithMessage;
import com.cm.hybridmessagingsdk.HybridMessaging;
import com.cm.hybridmessagingsdk.listener.OnRegistrationListener;
import com.cm.hybridmessagingsdk.listener.OnVerificationStatus;
import com.cm.hybridmessagingsdk.util.Registration;
import com.cm.hybridmessagingsdk.util.VerificationStatus;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpScreen extends Activity implements AsyncRequestListenerViaPost, AsyncTaskCompleteListenerForGetMethod {
    private TextView firstNameLabel, lastNameLabel, mobnoLabel, mailAddressLabel, confirmMailLabel, pwLabel, confirmPwLabel,
            mobnoOtpLabel, resendOTPButton;
    private EditText signupFirstName, signupLastName, signupMobNo, signupMailAddress, signupConfirmMail, signupPassword,
            signupConfirmPassword, signupMobNoOtp;
    private SharedPreferences preferences;
    private boolean isOtpVerifiedSuccessfully = false;
    private String formatedMobileNumberForCMSdk = "";
    private Button proceedButtonWithOTP, proceedButtonWithoutOTP;
    private ProgressBar mProgressBar;
    private ProgressDialog progressDialog = null;
    private String receivedOTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        settingIds();

        //RESTRICTING ' IN PASSWORD FIELD
        FieldValidationWithMessage.blockMyCharacter(signupPassword, "'");
        FieldValidationWithMessage.blockMyCharacter(signupConfirmPassword, "'");

        resendOTPButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                /*if (isInternetAvailable()) {
                    showProgressDialog("Please wait, resending you an OTP");
                    resendOTP(formatedMobileNumberForCMSdk);
                } else
                    ARCustomToast.showToast(SignUpScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);*/

                if (signupValidation()) {
                    callingGetOTPApi();
                }
            }
        });

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_SIGN_UP));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void settingIds() {
        firstNameLabel = findViewById(R.id.signUp_FirstNameLabel);
        lastNameLabel = findViewById(R.id.signUp_LastNameLabel);
        mobnoLabel = findViewById(R.id.signUp_MobNoLabel);
        mobnoOtpLabel = findViewById(R.id.signUp_MobNoOtpLabel);
        mailAddressLabel = findViewById(R.id.signUp_MailAddressLabel);
        confirmMailLabel = findViewById(R.id.signUp_ConfirmMailAddressLabel);
        pwLabel = findViewById(R.id.signUp_PasswordLabel);
        confirmPwLabel = findViewById(R.id.signUp_ConfirmPasswordLabel);
        resendOTPButton = findViewById(R.id.signUp_ResendOTP);
        proceedButtonWithOTP = findViewById(R.id.signUpProceedButtonWithOTP);
        proceedButtonWithoutOTP = findViewById(R.id.SignUpProceedButtonWithoutOTP);

        firstNameLabel.setText(Html.fromHtml("First Name" + "<font color='#ff0000'>*"));
        lastNameLabel.setText(Html.fromHtml("Last Name" + "<font color='#ff0000'>*"));
        mobnoLabel.setText(Html.fromHtml("Mobile Number" + "<font color='#ff0000'>*"));
        mailAddressLabel.setText(Html.fromHtml("Email Address" + "<font color='#ff0000'>*"));
        confirmMailLabel.setText(Html.fromHtml("Confirm Email Address" + "<font color='#ff0000'>*"));
        pwLabel.setText(Html.fromHtml("Password" + "<font color='#ff0000'>*"));
        confirmPwLabel.setText(Html.fromHtml("Confirm Password" + "<font color='#ff0000'>*"));
        mobnoOtpLabel.setText(Html.fromHtml("Enter verification code  here" + "<font color='#ff0000'>*"));

        preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        signupFirstName = findViewById(R.id.signUpFirstname);
        signupLastName = findViewById(R.id.signUpLastname);
        signupMobNo = findViewById(R.id.signUpMobNumb);
        signupMobNoOtp = findViewById(R.id.signUpMobNumbOtp);
        signupMailAddress = findViewById(R.id.signUpEID);
        signupConfirmMail = findViewById(R.id.signUpConfirmEID);
        signupPassword = findViewById(R.id.signUpPW);
        signupConfirmPassword = findViewById(R.id.signUpConfirmPW);
        mProgressBar = findViewById(R.id.signUpProgressBar);
    }

    // BELOW METHODS CALL'S THE SIGN UP API, AFTER VALIDATING FIELDS IN THE FORM
    // Case 1: If user enter's UK no. that is 07**********, than we need to validate that no. from CM SDK
    // Case 2: If user is not of UK, than we directly call the sign up API
    public void signUpSignInBtn(View view) {
        if (signupValidation()) {
            try {
                if (isInternetAvailable()) {
                    /*showProgressDialog("Please wait...");
                    formatedMobileNumberForCMSdk = "0044" + signupMobNo.getText().toString().substring(1);
                    registerNewUserFromCMSdk(formatedMobileNumberForCMSdk);*/

                    callingGetOTPApi();

                } else
                    ARCustomToast.showToast(SignUpScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void callingGetOTPApi() {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "KP_GENERATE_OTP");
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("MOBILE", signupMobNo.getText().toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {
                                    if (childJson.has("OTP")) {
                                        receivedOTP = childJson.getString("OTP");
                                        showOTPLayout();
                                    }
                                } else {
                                    ARCustomToast.showToast(SignUpScreen.this, childJson.optString("DESCRIPTION"));
                                }
                            } else {
                                ARCustomToast.showToast(SignUpScreen.this, childJson.optString("DESCRIPTION"));
                            }
                        }

                    } catch (Exception e) {
                        ARCustomToast.showToast(SignUpScreen.this, "Invalid response, parsing error!!!");
                    }
                } else {
                    if (Utility.isInternetAvailable(SignUpScreen.this))
                        ARCustomToast.showToast(SignUpScreen.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(SignUpScreen.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(SignUpScreen.this))
                    ARCustomToast.showToast(SignUpScreen.this, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(SignUpScreen.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void registerNewUserFromCMSdk(String msisdn) {
        //USE THIS METHOD IF YOU WANT TO REGISTER NEW USER[FIRST TIME]
        HybridMessaging.registerNewUser(msisdn, new OnRegistrationListener() {
            @Override
            public void onReceivedRegistration(Registration registrationResponse) {
                hideProgressDialog();
                try {
                    if (registrationResponse.getStatus().toString().equalsIgnoreCase("WaitingForPin")) {
                        showOTPLayout();
                    } else if (registrationResponse.getStatus().toString().equalsIgnoreCase("Verified")) {
//                        hideProgressDialog();
//                        mProgressBar.setVisibility(View.GONE);
                        //If number is already verified
                        new LoadResponseViaPost(SignUpScreen.this, formJSONParam(), true).execute("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable arg0) {
                hideProgressDialog();
                try {
                    ARCustomToast.showToast(SignUpScreen.this, "Something went wrong! Please try again", Toast.LENGTH_LONG);
                } catch (Exception e) {
                }
            }
        });
    }

    private String formJSONParam() {
        JSONObject jsonobj = new JSONObject();
        String trimmedPassword = signupPassword.getText().toString().replaceAll(" +", " ").trim();
        try {
            jsonobj.put("APISERVICE", "KPUSERREGISTRATION");
            jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
            jsonobj.put("DEVICEOS", "Android");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("EMAILID", signupMailAddress.getText().toString());
            jsonobj.put("PASSWORD", trimmedPassword);
            jsonobj.put("FIRSTNAME", signupFirstName.getText().toString());
            jsonobj.put("LASTNAME", signupLastName.getText().toString());
            jsonobj.put("MOBILENUMBER", signupMobNo.getText().toString());
            jsonobj.put("LOCATION", "UnitedKingdom");
            jsonobj.put("REGISTRATIONSOURCE", "KPAPP");

            jsonobj.put("PUSH_TOKEN", preferences.getString("URBAN_AIRSHIP_CHANNEL_ID", ""));
            jsonobj.put("APPNAME", "KWIKPAY");

            //ADDING NEW EXTRA TAGS, FOR DEVICE INFORMATION
            try {
                DeviceAppInformation deviceAppInformation = new DeviceAppInformation(SignUpScreen.this);
                jsonobj.put("TOKAN", "");
                jsonobj.put("MOBILE", preferences.getString("VerifiedMobNoViaCM", ""));
                jsonobj.put("BRAND", deviceAppInformation.getDeviceBrand());
                jsonobj.put("MODEL", deviceAppInformation.getDeviceModel());
                jsonobj.put("MANUFACTURER", deviceAppInformation.getDeviceManufacturer());
                jsonobj.put("DEVICE", deviceAppInformation.getDevice());
                jsonobj.put("OSVERSION", deviceAppInformation.getDeviceOSVersion());
                jsonobj.put("SCREENSIZE", deviceAppInformation.getDeviceScreenSize());
                jsonobj.put("RESOLUTION", deviceAppInformation.getDeviceResolution());
                jsonobj.put("PRODUCT", deviceAppInformation.getDeviceProduct());
                jsonobj.put("IPADDRESS", deviceAppInformation.getIpAddress());
                jsonobj.put("CELLID", "");

                deviceAppInformation = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    // CALLING THIS METHOD IF WE WANT TO CONFIRM OTP RECEIVED FROM THE SDK
    public void signUpOTPConfirmMethod(View otpView) {
        if (signupValidation()) {
            if (signupMobNoOtp.getText().toString().isEmpty()) {
                signupMobNoOtp.requestFocus();
                signupMobNoOtp.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                ARCustomToast.showToast(SignUpScreen.this, "Please enter a valid OTP", Toast.LENGTH_LONG);
            } else if (signupMobNoOtp.getText().toString().length() < 4) {
                signupMobNoOtp.requestFocus();
                signupMobNoOtp.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                ARCustomToast.showToast(SignUpScreen.this, "Please enter a valid OTP", Toast.LENGTH_LONG);
            } else if (!signupMobNoOtp.getText().toString().equals(receivedOTP)) {
                signupMobNoOtp.requestFocus();
                signupMobNoOtp.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                ARCustomToast.showToast(SignUpScreen.this, "Entered OTP does not match", Toast.LENGTH_LONG);
            } else {
                 /*signupMobNoOtp.requestFocus();
                signupMobNoOtp.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (!isInternetAvailable())
                    ARCustomToast.showToast(SignUpScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
                else if (isOtpVerifiedSuccessfully)
                    new LoadResponseViaPost(SignUpScreen.this, formJSONParam(), true).execute("");
                else {
                    showProgressDialog("Please wait verifying your OTP");
                    proceedButtonWithOTP.setEnabled(false);
                    confirmReceivedOTP(signupMobNoOtp.getText().toString());
                }*/

                new LoadResponseViaPost(SignUpScreen.this, formJSONParam(), true).execute("");
            }
        }
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString.equals("") || loadedString == null || loadedString.equals("Exception")) {
            ARCustomToast.showToast(SignUpScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        } else {
            try {
                JSONObject signup_json = new JSONObject(loadedString);
                JSONObject signup_json_response = signup_json.getJSONObject(ProjectConstant.RESPONSE_TAG);

                if (signup_json_response.has(ProjectConstant.API_RESPONSE_CODE)) {
                    if (signup_json_response.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                        if (signup_json_response.has("USERNAME") && signup_json_response.has("USERID")) {
                            SharedPreferences.Editor loginPrefEdit = preferences.edit();
                            loginPrefEdit.putString("KP_USER_NAME", signup_json_response.getString("USERNAME"));
                            loginPrefEdit.putString("KP_USER_ID", signup_json_response.getString("USERID"));
                            loginPrefEdit.putString("KP_USER_MOBILE_NUMBER", signupMobNo.getText().toString());
                            loginPrefEdit.putString("KP_USER_EMAIL_ID", signupMailAddress.getText().toString());
                            loginPrefEdit.putString("VerifiedMobNoViaCM", signupMobNo.getText().toString());
                            loginPrefEdit.putBoolean("IS_USER_SIGNED_UP", true);
                            loginPrefEdit.putBoolean("isUserVerifiedFromCM", true);
                            loginPrefEdit.apply();

                            // CALLING API, FOR EMAIL VERIFICATION [WITH USER ID AND EMAIL ID]
                            String emailVerificationURL = getResources().getString(R.string.email_verification_send_base_url) + signupMailAddress.getText().toString();
                            new LoadResultFromGetMethod(SignUpScreen.this).execute(emailVerificationURL);
                        }
                    } else {
                        if (signup_json_response.has("DESCRIPTION")) {
                            ARCustomToast.showToast(SignUpScreen.this, signup_json_response.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean signupValidation() {
        if (!FieldValidationWithMessage.firstNameIsValid(signupFirstName.getText().toString(), signupFirstName).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(signupLastName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMobNo);
            FieldValidationWithMessage.removeAllErrorIcons(signupMailAddress);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmMail);
            FieldValidationWithMessage.removeAllErrorIcons(signupPassword);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmPassword);

            ARCustomToast.showToast(SignUpScreen.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
            return false;
        } else if (!FieldValidationWithMessage.lastNameIsValid(signupLastName.getText().toString(), signupLastName).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(signupFirstName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMobNo);
            FieldValidationWithMessage.removeAllErrorIcons(signupMailAddress);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmMail);
            FieldValidationWithMessage.removeAllErrorIcons(signupPassword);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmPassword);

            ARCustomToast.showToast(SignUpScreen.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
            return false;
        } else if (!FieldValidationWithMessage.mobNumberIsValidWithoutLeadingZero(signupMobNo.getText().toString(), signupMobNo).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(signupFirstName);
            FieldValidationWithMessage.removeAllErrorIcons(signupLastName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMailAddress);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmMail);
            FieldValidationWithMessage.removeAllErrorIcons(signupPassword);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmPassword);

            ARCustomToast.showToast(SignUpScreen.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
            return false;
        }

//		else if(!(signupMobNo.getText().toString().charAt(0)=='0' && signupMobNo.getText().toString().charAt(1)=='7'))
        else if (!(signupMobNo.getText().toString().startsWith("07"))) {
            signupMobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
            FieldValidationWithMessage.removeAllErrorIcons(signupFirstName);
            FieldValidationWithMessage.removeAllErrorIcons(signupLastName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMailAddress);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmMail);
            FieldValidationWithMessage.removeAllErrorIcons(signupPassword);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmPassword);

            ARCustomToast.showToast(SignUpScreen.this, "please enter a valid 11 digit mobile no. starting with 07", Toast.LENGTH_LONG);
            return false;
        } else if (signupMobNo.getText().toString().length() < 11) {
            signupMobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
            FieldValidationWithMessage.removeAllErrorIcons(signupFirstName);
            FieldValidationWithMessage.removeAllErrorIcons(signupLastName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMailAddress);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmMail);
            FieldValidationWithMessage.removeAllErrorIcons(signupPassword);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmPassword);

            ARCustomToast.showToast(SignUpScreen.this, "please enter a valid 11 digit mobile no. starting with 07", Toast.LENGTH_LONG);
            return false;
        } else if (!FieldValidation.emailAddressIsValid(signupMailAddress.getText().toString(), signupMailAddress)) {
            FieldValidationWithMessage.removeAllErrorIcons(signupFirstName);
            FieldValidationWithMessage.removeAllErrorIcons(signupLastName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMobNo);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmMail);
            FieldValidationWithMessage.removeAllErrorIcons(signupPassword);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmPassword);

            ARCustomToast.showToast(SignUpScreen.this, "please enter a valid email id", Toast.LENGTH_LONG);
            return false;
        } else if (!FieldValidationWithMessage.confirmEmailAddressIsValid(signupMailAddress.getText().toString(), signupConfirmMail, signupConfirmMail.getText().toString()).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(signupFirstName);
            FieldValidationWithMessage.removeAllErrorIcons(signupLastName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMobNo);
            FieldValidationWithMessage.removeAllErrorIcons(signupMailAddress);
            FieldValidationWithMessage.removeAllErrorIcons(signupPassword);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmPassword);

            ARCustomToast.showToast(SignUpScreen.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
            return false;
        } else if (!FieldValidationWithMessage.passwordIsValid(signupPassword.getText().toString(), signupPassword, 8).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(signupFirstName);
            FieldValidationWithMessage.removeAllErrorIcons(signupLastName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMobNo);
            FieldValidationWithMessage.removeAllErrorIcons(signupMailAddress);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmMail);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmPassword);

            ARCustomToast.showToast(SignUpScreen.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
            return false;
        } else if (!FieldValidationWithMessage.confirmPasswordIsValid(signupPassword.getText().toString(), signupConfirmPassword, signupConfirmPassword.getText().toString()).equals("SUCCESS")) {
            FieldValidationWithMessage.removeAllErrorIcons(signupFirstName);
            FieldValidationWithMessage.removeAllErrorIcons(signupLastName);
            FieldValidationWithMessage.removeAllErrorIcons(signupMobNo);
            FieldValidationWithMessage.removeAllErrorIcons(signupMailAddress);
            FieldValidationWithMessage.removeAllErrorIcons(signupConfirmMail);
            FieldValidationWithMessage.removeAllErrorIcons(signupPassword);
            ARCustomToast.showToast(SignUpScreen.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
            return false;
        } else {
            return true;
        }
    }

	/*private void hideOTPLayout()
    {
		proceedButtonWithoutOTP.setVisibility(View.VISIBLE);
		mobnoOtpLabel.setVisibility(View.GONE);
		signupMobNoOtp.setVisibility(View.GONE);
		resendOTPButton.setVisibility(View.GONE);
		proceedButtonWithOTP.setVisibility(View.INVISIBLE);
	}*/

    private void showOTPLayout() {
//		mobnoOtpLabel.setVisibility(View.VISIBLE);  // COMMENTING AS PER NEW CHANGE, REVERT BACK IF REQUIRED
        signupMobNoOtp.setVisibility(View.VISIBLE);
        resendOTPButton.setVisibility(View.VISIBLE);
        proceedButtonWithOTP.setVisibility(View.VISIBLE);
        proceedButtonWithoutOTP.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
    }

    private void confirmReceivedOTP(String verificationCode) {
        HybridMessaging.registerUserByPincode(verificationCode, new OnVerificationStatus() {
            @Override
            public void onVerificationStatus(VerificationStatus verificationStatus) {
                hideProgressDialog();
                if (verificationStatus.toString().equalsIgnoreCase("Verified")) {
                    isOtpVerifiedSuccessfully = true;
                    resendOTPButton.setVisibility(View.INVISIBLE);
                    signupMobNoOtp.setEnabled(false);
                    proceedButtonWithOTP.setEnabled(true);

                    SharedPreferences.Editor otpEditor = preferences.edit();
                    otpEditor.putBoolean("isUserVerifiedFromCM", true).apply();

                    new LoadResponseViaPost(SignUpScreen.this, formJSONParam(), true).execute("");
                } else {
                    proceedButtonWithOTP.setEnabled(true);
                    ARCustomToast.showToast(SignUpScreen.this, "Last pin verification failed", Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onError(Throwable verificationError) {
                hideProgressDialog();
                proceedButtonWithOTP.setEnabled(true);
                try {
                    ARCustomToast.showToast(SignUpScreen.this, "Something went wrong! Please try again", Toast.LENGTH_LONG);
                } catch (Exception e) {
                }
            }
        });
    }

    private void resendOTP(String enteredMsisdn) {
        HybridMessaging.requestNewVerificationPin(enteredMsisdn, new OnRegistrationListener() {
            @Override
            public void onReceivedRegistration(Registration resendRegResponse) {
                hideProgressDialog();
            }

            @Override
            public void onError(Throwable error) {
                hideProgressDialog();
                try {
                    ARCustomToast.showToast(SignUpScreen.this, "Something went wrong! Please try again", Toast.LENGTH_LONG);
                } catch (Exception e) {
                }
            }
        });
    }

    @Override
    public void onLoadComplete(String loadedString) {
        Intent intent = new Intent(SignUpScreen.this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(SignUpScreen.this);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public boolean isInternetAvailable() {
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        } catch (Exception e) {
        }
        return true;
    }
}
