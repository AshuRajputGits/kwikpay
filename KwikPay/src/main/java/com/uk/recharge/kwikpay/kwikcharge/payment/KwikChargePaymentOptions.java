package com.uk.recharge.kwikpay.kwikcharge.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.PaymentGatewayPage;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.nativepg.PayPalPaymentScreen;
import com.uk.recharge.kwikpay.nativepg.UpdateJUDOPaymentGateway;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * Created by Ashu Rajput on 10/9/2017.
 */

public class KwikChargePaymentOptions extends KPSuperClass implements AsyncRequestListenerViaPost {

    private boolean isVisaChoose = false;
    private int apiHitPosition = 0;
    private LocationIdMO receivedModel;
    private String pgAppTxnId, pgType;
    private SharedPreferences preference;
    private String apiPGName = "";
    private HashMap<String, String> nativePGDataHashMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        setHeaderScreenName("Pay with ");
        setResourceIds();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.kc_payment_options;
    }

    private void setResourceIds() {

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        TextView processingFeesMessage = findViewById(R.id.processingFeesMessage);
        processingFeesMessage.setVisibility(View.VISIBLE);

        TextView maximumAuthorizedAmountTV = findViewById(R.id.maximumAuthorizedAmountTV);
        maximumAuthorizedAmountTV.setVisibility(View.VISIBLE);

        findViewById(R.id.pgIntermediateScreenId).setVisibility(View.VISIBLE);
        findViewById(R.id.kcPaymentNote).setVisibility(View.VISIBLE);
        findViewById(R.id.paypal_PG_Button).setOnClickListener(onClickListener);
        findViewById(R.id.judo_PG_Button).setOnClickListener(onClickListener);

        receivedModel = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();

        try {
            if (receivedModel != null) {
                processingFeesMessage.setText("* A processing fee of " + Utility.fromHtml("\u00a3") + receivedModel.getServiceFee() + " will be charged");
                maximumAuthorizedAmountTV.setText("Maximum amount to be authorised " + Utility.fromHtml("\u00a3") + receivedModel.getAuthorisingAmount());
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewID = v.getId();
            if (viewID == R.id.paypal_PG_Button) {
                isVisaChoose = false;
                callPaymentRelatedApis();

            } else if (viewID == R.id.judo_PG_Button) {
                isVisaChoose = true;
                callPaymentRelatedApis();
            }
        }
    };

    private void callPaymentRelatedApis() {
        apiHitPosition = 1;
        new LoadResponseViaPost(KwikChargePaymentOptions.this, formTransactionIdJSON(), true).execute("");
    }

    private String formTransactionIdJSON() {
        try {
            JSONObject json = new JSONObject();

            json.put("APISERVICE", "KPPAYMENTGATEWAYAPPTXNID");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);

            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("SOURCE", "KPAPP");
            json.put("OPABBR", receivedModel.getOperatorCode());
            if (isVisaChoose) {
                json.put("MOPID", preference.getString("PG_MOP_ID", "18"));
                json.put("PGSOID", preference.getString("PG_SO_ID", "160"));
            } else {
                json.put("MOPID", preference.getString("PG_MOP_ID1", "22"));
                json.put("PGSOID", preference.getString("PG_SO_ID1", "164"));
            }

            /*json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("SERVICEID", "");
            json.put("USERNAME", preference.getString("KP_USER_NAME", ""));
            json.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("DISCOUNTAMOUNT", "0");
            json.put("PGDISCOUNTAMOUNT", "0");
            json.put("SOURCE", "KPAPP");
            json.put("MOPID", "18");
            json.put("PGSOID", "160");
            json.put("PLANDESCRIPTION", "");
            json.put("OPERATORNAME", "");
            json.put("PROCESSINGFEE", "");
            json.put("MOBILENUMBER", "");
            json.put("NETPGAMOUNT", "");
            json.put("COUNTRYCODE", "");
            json.put("BROWSER", "BROWSER");
            json.put("OS", "Android");
            json.put("CPNTXNID", "0");
            json.put("CPNSRNO", "0");
            json.put("CPNAMOUNT", "0");
            json.put("DISPLAYAMOUNT", "");
            json.put("DISPLAYCURRENCYAGID", "");
            json.put("RECHARGEAMOUNT", "");
            json.put("RECHARGECURRENCYAGID", "");
            json.put("PAYMENTCURRENCYAGID", "");
            json.put("OPERATORCODE", receivedModel.getOperatorCode());
            json.put("OPABBR", receivedModel.getOperatorCode());
            json.put("SERVICEFEE", "");

            //ADDING NEW EXTRA TAGS, FOR DEVICE INFORMATION
            try {
                DeviceAppInformation deviceAppInformation = new DeviceAppInformation(KwikChargePaymentOptions.this);
                json.put("IPADDRESS", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
                json.put("BRAND", deviceAppInformation.getDeviceBrand());
                json.put("MODEL", deviceAppInformation.getDeviceModel());
                json.put("MANUFACTURER", deviceAppInformation.getDeviceManufacturer());
                json.put("DEVICE", deviceAppInformation.getDevice());
                json.put("OSVERSION", deviceAppInformation.getDeviceOSVersion());
                json.put("SCREENSIZE", deviceAppInformation.getDeviceScreenSize());
                json.put("RESOLUTION", deviceAppInformation.getDeviceResolution());
                json.put("PRODUCT", deviceAppInformation.getDeviceProduct());
                json.put("CELLID", "");
                deviceAppInformation = null;
            } catch (Exception e) {
                e.printStackTrace();
            }*/

            return json.toString();
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {

//        Log.e("Response", "Order Response " + loadedString);

        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {

                if (apiHitPosition == 1) {

                    if (parseAppTxnIdDetails(loadedString)) {
                        if (!pgAppTxnId.equals("") && !pgType.equals("")) {
                            if (pgType.equals("NATIVE")) {
                                apiHitPosition = 2;
                                new LoadResponseViaPost(KwikChargePaymentOptions.this, formPaymentGatewayJSON("KPGETPGDETAILS", false)).execute("");
                            } else {
                                Intent pgIntent = (new Intent(KwikChargePaymentOptions.this, PaymentGatewayPage.class));
                                pgIntent.putExtra("PG_JSON_PARAM", formPaymentGatewayJSON("WSGetCotrolAndData", true));
                                pgIntent.putExtra("PG_APP_TXN_ID_KEY", pgAppTxnId);
                                startActivity(pgIntent);
                                finish();
                            }
                        }
                    }
                } else if (apiHitPosition == 2) {
                    if (parseReceivedPGDetail(loadedString)) {
                        if (apiPGName.equals("JUDOPG")) {
                            Intent pgIntent = (new Intent(KwikChargePaymentOptions.this, UpdateJUDOPaymentGateway.class));
                            pgIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                            startActivity(pgIntent);
                        } else if (apiPGName.equalsIgnoreCase("PAYPAL")) {
                            Intent paypalIntent = new Intent(KwikChargePaymentOptions.this, PayPalPaymentScreen.class);
                            paypalIntent.putExtra("NATIVE_PG_HASHMAP_KEY", nativePGDataHashMap);
                            startActivity(paypalIntent);
                        } else if (apiPGName.equals("COUPONPG")) {
                            Intent couponPgIntent = (new Intent(KwikChargePaymentOptions.this, UpdateJUDOPaymentGateway.class));
                            couponPgIntent.putExtra("NATIVE_PG_COUPON", nativePGDataHashMap);
                            couponPgIntent.putExtra("NATIVE_PG_COUPON2", "COUPON_DATA");
                            startActivity(couponPgIntent);
                        } else {
                            Toast.makeText(KwikChargePaymentOptions.this, "Payment Gateway not defined", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        }
    }

    // LOGIC FOR PARSING PG APP TRANSACTION ID FROM WEBSERVICE [apiHitPosition==1]
    public boolean parseAppTxnIdDetails(String operatorResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(operatorResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(KwikChargePaymentOptions.this, "No Response Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.RESPONSE_MSG_TAG)) {
                if (parentResponseObject.getString(ProjectConstant.RESPONSE_MSG_TAG).equals("SUCCESS")) {
                    if (parentResponseObject.has(ProjectConstant.PG_APP_TXN_ID) && parentResponseObject.has("PGTYPE")) {
                        pgAppTxnId = parentResponseObject.getString(ProjectConstant.PG_APP_TXN_ID);
                        pgType = parentResponseObject.getString("PGTYPE");
                    }
                } else {
                    ARCustomToast.showToast(KwikChargePaymentOptions.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                    return false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // FORMING JSON FOR GETTING PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEBVIEW
    public String formPaymentGatewayJSON(String apiServiceName, boolean isWebView) {
        try {
            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            json.put("iscart", "NO");
            json.put("os", "Android");

            if (isVisaChoose) {
                json.put("mopid", preference.getString("PG_MOP_ID", "18"));
                json.put("pgsoid", preference.getString("PG_SO_ID", "160"));
            } else {
                json.put("mopid", preference.getString("PG_MOP_ID1", "22"));
                json.put("pgsoid", preference.getString("PG_SO_ID1", "164"));
            }
            json.put("apptransactionid", pgAppTxnId);
            json.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
            json.put("browser", "BROWSER");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("sessionid", preference.getString("SESSION_ID", ""));
            else
                json.put("sessionid", ProjectConstant.SESSIONID);
            json.put("source", "KPAPP");
            json.put("username", preference.getString("KP_USER_NAME", ""));
            json.put("userid", preference.getString("KP_USER_ID", "0"));
            json.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));
            json.put("appname", "KWIKPAY");

            if (!isWebView)
                json.put("pgType", pgType);

            for (int i = 0; i < 1; i++) {
                JSONObject childJsonObject = new JSONObject();

                childJsonObject.put("apiservice", apiServiceName);
                childJsonObject.put("username", preference.getString("KP_USER_NAME", ""));
                childJsonObject.put("userid", preference.getString("KP_USER_ID", "0"));
                childJsonObject.put("emailid", preference.getString("KP_USER_EMAIL_ID", ""));
                childJsonObject.put("os", "Android");
                childJsonObject.put("ipaddress", preference.getString("USER_PUBLIC_IP", "0.0.0.0"));
                childJsonObject.put("browser", "BROWSER");
                childJsonObject.put("cpntxnid", "0");
                childJsonObject.put("cpnamount", "0");
                childJsonObject.put("cpnsrno", "0");
                childJsonObject.put("source", "KPAPP");
                childJsonObject.put("discountamount", "0");
                childJsonObject.put("pgdiscountamount", "0");
                if (isVisaChoose) {
                    childJsonObject.put("mopid", preference.getString("PG_MOP_ID", "18"));
                    childJsonObject.put("pgsoid", preference.getString("PG_SO_ID", "160"));
                } else {
                    childJsonObject.put("mopid", preference.getString("PG_MOP_ID1", "22"));
                    childJsonObject.put("pgsoid", preference.getString("PG_SO_ID1", "164"));
                }
                if (ProjectConstant.SESSIONID.equals(""))
                    childJsonObject.put("sessionid", preference.getString("SESSION_ID", ""));
                else
                    childJsonObject.put("sessionid", ProjectConstant.SESSIONID);

                childJsonObject.put("mobilenumber", "NA");
                childJsonObject.put("rechargeamount", "0.00");
                childJsonObject.put("displayamount", "0.00");
                childJsonObject.put("rechargecurrencyagid", "42");
                childJsonObject.put("displaycurrencyagid", "42");
                childJsonObject.put("paymentcurrencyagid", "42");
                childJsonObject.put("countrycode", "UNITEDKINGDOM");
                childJsonObject.put("serviceid", "4");
                childJsonObject.put("operatorcode", "ELECTRICBLUE");
                childJsonObject.put("processingfee", "0.00");
                childJsonObject.put("servicefee", receivedModel.getServiceFee());

                /*childJsonObject.put("netpgamount", receivedModel.getRecentMaxAmount());
                childJsonObject.put("maxamount", receivedModel.getRecentMaxAmount());*/
                childJsonObject.put("netpgamount", convertToTwoDecimals(receivedModel.getAuthorisingAmount()));
                childJsonObject.put("maxamount", receivedModel.getAuthorisingAmount());
                childJsonObject.put("locationnumber", receivedModel.getLocationNumber());
                childJsonObject.put("registrationnumber", receivedModel.getRegistrationNumber());
                childJsonObject.put("make", receivedModel.getRecentMake());
                childJsonObject.put("model", receivedModel.getRecentModel());
                childJsonObject.put("batterytype", receivedModel.getRecentBatteryType());
                childJsonObject.put("conectortype", receivedModel.getConnectorType());
                childJsonObject.put("chargertype", "High");
                childJsonObject.put("rate", receivedModel.getRecentRate());
                childJsonObject.put("maxtime", receivedModel.getRecentMaxTime());
                jsonArray.put(childJsonObject);
            }
            json.put("kprequest", jsonArray);

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        return null;
    }

    //LOGIC FOR PARSING THE RECEIVED PAYMENT GATEWAY DETAILS EITHER FOR NATIVE OR WEB VIEW [apiHitPosition==3]
    public boolean parseReceivedPGDetail(String pgDetails) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(pgDetails);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(KwikChargePaymentOptions.this, "No Response Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    nativePGDataHashMap = new HashMap<>();
                    nativePGDataHashMap.put("PAYMENTREFERENCE", parentResponseObject.optString("PAYMENTREFERENCE"));
                    nativePGDataHashMap.put("JUDOID", parentResponseObject.optString("JUDOID"));
                    nativePGDataHashMap.put("CONSUMERREFERENCE", parentResponseObject.optString("CONSUMERREFERENCE"));
                    nativePGDataHashMap.put("CHECKSUMKEY", parentResponseObject.optString("CHECKSUMKEY"));
                    nativePGDataHashMap.put("ORDERID", parentResponseObject.optString("ORDERID"));
                    nativePGDataHashMap.put("TOKENPAYREFERENCE", parentResponseObject.optString("TOKENPAYREFERENCE"));
                    nativePGDataHashMap.put("SECRETKEY", parentResponseObject.optString("SECRETKEY"));
                    nativePGDataHashMap.put("TOKEN", parentResponseObject.optString("TOKEN"));
                    apiPGName = parentResponseObject.optString("PGNAME");
                    if (receivedModel != null)
                        nativePGDataHashMap.put("TOTALAMOUNTPAYABLE", receivedModel.getRecentMaxAmount());
                    nativePGDataHashMap.put("PGNAME", apiPGName);
                    nativePGDataHashMap.put("APPTXNID", pgAppTxnId);

                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(KwikChargePaymentOptions.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String convertToTwoDecimals(String totalPayableAmount) {

        String convertedValues = "";
        double totalPaymentAmountDouble = 0.00;
        double finalTotalPaymentAmount;
        try {
            if (totalPayableAmount != null && !totalPayableAmount.isEmpty()) {
                totalPaymentAmountDouble = Double.parseDouble(totalPayableAmount);
            }
            finalTotalPaymentAmount = 0.20 + totalPaymentAmountDouble;
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            convertedValues = decimalFormat.format(finalTotalPaymentAmount);

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return convertedValues;
    }

}
