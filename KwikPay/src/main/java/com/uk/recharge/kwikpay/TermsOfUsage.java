package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.appsflyer.AppsFlyerLib;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

public class TermsOfUsage extends Activity 
{
	private ProgressDialog mProgressDialog;
	private WebView webView;
	ImageView headerBackImage,homeButtonImage;
//	TextView headerBelowTV,cartItemCounter;
	SharedPreferences preference;

	@SuppressLint("SetJavaScriptEnabled") 
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.terms_of_usage);

		setUpViewsId();

		webView.getSettings().setJavaScriptEnabled(true);
		MyWebClient webClient =new MyWebClient(TermsOfUsage.this);
		webView.setWebViewClient(webClient);

		//SETTING SCREEN NAMES TO APPS FLYER
		try {
			EventsLoggerSingleton.getInstance().setCommonEventLogs(this,getResources().getString(R.string.SCREEN_NAME_TOU));
			/*String eventName = getResources().getString(R.string.SCREEN_NAME_TOU);
			AppsFlyerLib.trackEvent(this, eventName, null);
			EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
			((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String touURL="https://kwikpay.co.uk/kwikpay/kwikpay/apphtml/"+preference.getString(ProjectConstant.DB_TERMS_OF_USAGE_REVISED, "terms_of_usage")+".html";
//		String touURL="http://46.37.168.7:8080/kwikpay/kwikpay/apphtml/"+preference.getString(ProjectConstant.DB_TERMS_OF_USAGE_REVISED, "terms_of_usage")+".html";
		
		webView.loadUrl(touURL);

	}

	public void setUpViewsId()
	{
		webView = (WebView) findViewById(R.id.tou_WebView);
		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);
		
		TextView headerTitle=(TextView)findViewById(R.id.mainHeaderTitleName);
		headerTitle.setText("Terms");
		
//		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);
		
//		View separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(TermsOfUsage.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();
		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME
		/*DBQueryMethods database=new DBQueryMethods(TermsOfUsage.this);
		try
		{
			database.open();
			TextView headerBelowTV=(TextView)findViewById(R.id.headerBelowTextPart2);
			headerBelowTV.setText(database.getSubHeaderTitles("Terms of usage"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}*/

	}

	public class MyWebClient extends WebViewClient 
	{
		public Context touContext;

		public MyWebClient(Context touContext) 
		{
			// TODO Auto-generated constructor stub
			this.touContext = touContext;
			mProgressDialog = new ProgressDialog(touContext);
			mProgressDialog.setMessage("loading please wait...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.setCancelable(false);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) 
		{
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);

			try
			{
				if(!isFinishing())
				{
					mProgressDialog.show();
				}
			}catch(Exception exception){
			}

		}

		@Override
		public void onPageFinished(WebView view, String url) 
		{
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);

			try 
			{
				if ((mProgressDialog != null) && mProgressDialog.isShowing()) 
				{
					mProgressDialog.dismiss();
				}
			} catch (final IllegalArgumentException ae) {
			} catch (final Exception excep) {
			} finally {
				mProgressDialog = null;
			}
		}
	}
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preference!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preference.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}
		
		}
	}*/

}