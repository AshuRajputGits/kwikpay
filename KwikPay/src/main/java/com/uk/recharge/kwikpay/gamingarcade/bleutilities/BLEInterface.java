package com.uk.recharge.kwikpay.gamingarcade.bleutilities;

import android.bluetooth.BluetoothDevice;

/**
 * Created by Ashu Rajput on 11/19/2018.
 */

public interface BLEInterface {

    void onConnected(BluetoothDevice var1);
    void onBLEAlreadyPaired(BluetoothDevice device);

}
