package com.uk.recharge.kwikpay.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class SignikLightTextView extends TextView {


    public SignikLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SignikLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SignikLightTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/signika_light.ttf");
            setTypeface(tf);
        }
    }
}
