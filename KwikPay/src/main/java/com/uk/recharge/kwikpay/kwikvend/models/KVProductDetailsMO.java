package com.uk.recharge.kwikpay.kwikvend.models;

/**
 * Created by Ashu Rajput on 7/11/2018.
 */

public class KVProductDetailsMO {

    private String productId;
    private String productName;
    private float productPrice;
    private Boolean isProductSelected;
    private String productDescription;
    private String productSize;
    private String productSubBrand;
    private String productURL;
    private String productCorePrice;
    private int productAvailableQuantity;
    private String productPurchaseCount;
    private String productDispensedCount;
    private String paymentCurrencyCode;
    private String modelType;

    //Below variable is used to identify : counts of each product of same category
    private int productQuantity;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public Boolean getProductSelected() {
        return isProductSelected;
    }

    public void setProductSelected(Boolean productSelected) {
        isProductSelected = productSelected;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductSubBrand() {
        return productSubBrand;
    }

    public void setProductSubBrand(String productSubBrand) {
        this.productSubBrand = productSubBrand;
    }

    public String getProductURL() {
        return productURL;
    }

    public void setProductURL(String productURL) {
        this.productURL = productURL;
    }

    public String getProductCorePrice() {
        return productCorePrice;
    }

    public void setProductCorePrice(String productCorePrice) {
        this.productCorePrice = productCorePrice;
    }

    public int getProductAvailableQuantity() {
        return productAvailableQuantity;
    }

    public void setProductAvailableQuantity(int productAvailableQuantity) {
        this.productAvailableQuantity = productAvailableQuantity;
    }

    public String getProductPurchaseCount() {
        return productPurchaseCount;
    }

    public void setProductPurchaseCount(String productPurchaseCount) {
        this.productPurchaseCount = productPurchaseCount;
    }

    public String getProductDispensedCount() {
        return productDispensedCount;
    }

    public void setProductDispensedCount(String productDispensedCount) {
        this.productDispensedCount = productDispensedCount;
    }

    public String getPaymentCurrencyCode() {
        return paymentCurrencyCode;
    }

    public void setPaymentCurrencyCode(String paymentCurrencyCode) {
        this.paymentCurrencyCode = paymentCurrencyCode;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }
}
