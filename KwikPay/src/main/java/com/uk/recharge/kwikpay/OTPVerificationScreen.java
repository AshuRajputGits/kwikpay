package com.uk.recharge.kwikpay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidationWithMessage;
import com.cm.hybridmessagingsdk.HybridMessaging;
import com.cm.hybridmessagingsdk.listener.OnRegistrationListener;
import com.cm.hybridmessagingsdk.listener.OnVerificationStatus;
import com.cm.hybridmessagingsdk.util.Registration;
import com.cm.hybridmessagingsdk.util.VerificationStatus;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPVerificationScreen extends Activity implements AsyncRequestListenerViaPost {
    private TextView otpMobileNoLabel, otpNumberLabel, otpResendButton;
    private EditText otpMobileNoETField, otpNumberETField;
    private Button mobileNumberProceedButton, otpNumberProceedButton;
    private SharedPreferences preference;
    private String formatedMobileNumberForCMSdk = "";
    private boolean isNumberVerifiedFlag = false;
    private boolean isComingFromLoginScreen = false;
    private ProgressBar otpVerifyProgressBar;
    private boolean isComingFromFBLogin = false;
    private String receivedOTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.otp_screen);

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        otpMobileNoLabel = findViewById(R.id.otpScreen_MobileNoTVLabel);
        otpNumberLabel = findViewById(R.id.otp_screen_verificationOtpLabel);
        otpMobileNoETField = findViewById(R.id.otpScreen_MobileNoET);
        otpNumberETField = findViewById(R.id.otp_screen_verificationOtpET);
        mobileNumberProceedButton = findViewById(R.id.otp_screen_MobileNumberproceedbutton);
        otpNumberProceedButton = findViewById(R.id.otp_screen_OtpNumberproceedbutton);
        otpResendButton = findViewById(R.id.otp_screen_ResendOTP);
        otpVerifyProgressBar = findViewById(R.id.otpVerificationProgressBar);

        otpMobileNoLabel.setText(Html.fromHtml("Mobile Number" + "<font color='#ff0000'>*"));
        otpNumberLabel.setText(Html.fromHtml("Enter verification code  here" + "<font color='#ff0000'>*"));
        otpMobileNoETField.setText(preference.getString("KP_USER_MOBILE_NUMBER", ""));

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("LOGIN_OTP_CALLER"))
                isComingFromLoginScreen = true;
            if (receivedBundle.containsKey("LOGIN_OTP_COMING_FROM_FB"))
                isComingFromFBLogin = true;
        }

        otpMobileNoETField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int finalLength, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.length() == 11 && count > 0) {
                    //hide opt layout
                    mobileNumberProceedButton.setVisibility(View.VISIBLE);
                    otpNumberLabel.setVisibility(View.INVISIBLE);
                    otpNumberETField.setVisibility(View.INVISIBLE);
                    otpNumberProceedButton.setVisibility(View.INVISIBLE);
                    otpResendButton.setVisibility(View.GONE);
                    otpMobileNoETField.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable mobNumber) {
                if (mobNumber.length() == 11) {
                    final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(otpMobileNoETField.getWindowToken(), 0);
                }
            }
        });

        otpResendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                /*otpVerifyProgressBar.setVisibility(View.VISIBLE);
                disableMyField();
                resendOTP(formatedMobileNumberForCMSdk);*/

                callingGetOTPApi();
            }
        });

        mobileNumberProceedButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String mobNumber = otpMobileNoETField.getText().toString();

                if (TextUtils.isEmpty(mobNumber)) {
                    otpMobileNoETField.requestFocus();
                    otpMobileNoETField.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                    ARCustomToast.showToast(OTPVerificationScreen.this, "please enter valid mobile no.", Toast.LENGTH_LONG);
                } else if (mobNumber.length() < 11) {
                    otpMobileNoETField.requestFocus();
                    otpMobileNoETField.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                    ARCustomToast.showToast(OTPVerificationScreen.this, "please enter valid 11 digit no.", Toast.LENGTH_LONG);
                } else if (!(Long.valueOf(mobNumber) > 0)) {
                    otpMobileNoETField.requestFocus();
                    otpMobileNoETField.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                    ARCustomToast.showToast(OTPVerificationScreen.this, "please enter valid 11 digit no.", Toast.LENGTH_LONG);
                } else if (!(mobNumber.charAt(0) == '0' && mobNumber.charAt(1) == '7')) {
                    otpMobileNoETField.requestFocus();
                    otpMobileNoETField.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                    ARCustomToast.showToast(OTPVerificationScreen.this, "please enter valid mobile no. starting with 07", Toast.LENGTH_LONG);
                } else {
                    FieldValidationWithMessage.removeAllErrorIcons(otpMobileNoETField);

                    /*if (isNumberVerifiedFlag) {
                        //CALLING EDIT PROFILE API, IN ORDER TO SAVE THE VERIFIED MOBILE NUMBER
                        new LoadResponseViaPost(OTPVerificationScreen.this, formEditProfileJson(), true).execute("");
                    } else {
                        try {
                            HybridMessaging.resetLogin();
                            formatedMobileNumberForCMSdk = "0044" + mobNumber.substring(1);

                            registerNewUserFromCMSdk(formatedMobileNumberForCMSdk);
                            Thread.sleep(500);
                            showOTPLayout();

                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }*/

                    callingGetOTPApi();
                }
            }
        });

        otpNumberProceedButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (otpNumberETField.getText().toString().isEmpty()) {
                    otpNumberETField.requestFocus();
                    otpNumberETField.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                    ARCustomToast.showToast(OTPVerificationScreen.this, "Please enter a valid OTP", Toast.LENGTH_LONG);
                } else if (otpNumberETField.getText().toString().length() < 4) {
                    otpNumberETField.requestFocus();
                    otpNumberETField.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                    ARCustomToast.showToast(OTPVerificationScreen.this, "Please enter a valid OTP", Toast.LENGTH_LONG);
                } else if (!otpNumberETField.getText().toString().equals(receivedOTP)) {
                    ARCustomToast.showToast(OTPVerificationScreen.this, "Last pin verification failed", Toast.LENGTH_LONG);
                } else {
                    otpVerifyProgressBar.setVisibility(View.VISIBLE);
                    otpNumberETField.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    /*disableMyField();
                    confirmReceivedOTP(otpNumberETField.getText().toString());*/

                    isNumberVerifiedFlag = true;
                    otpVerifyProgressBar.setVisibility(View.GONE);
                    enableMyField();

                    //CALLING EDIT PROFILE API, IN ORDER TO SAVE THE VERIFIED MOBILE NUMBER
                    SharedPreferences.Editor otpEditor = preference.edit();
                    otpEditor.putBoolean("isUserVerifiedFromCM", true);
                    otpEditor.putString("VerifiedMobNoViaCM", otpMobileNoETField.getText().toString());
                    otpEditor.apply();

                    if (isComingFromLoginScreen) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("RESPONSE", true);
                        setResult(RESULT_OK, returnIntent);
                        OTPVerificationScreen.this.finish();
                    } else {
                        new LoadResponseViaPost(OTPVerificationScreen.this, formEditProfileJson(), true).execute("");
                    }
                }
            }
        });

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_MOBILE_OTP));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void callingGetOTPApi() {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "KP_GENERATE_OTP");
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("MOBILE", otpMobileNoETField.getText().toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {
                                    if (childJson.has("OTP")) {
                                        receivedOTP = childJson.getString("OTP");
                                        showOTPLayout();
                                    }
                                } else {
                                    ARCustomToast.showToast(OTPVerificationScreen.this, childJson.optString("DESCRIPTION"));
                                }
                            } else {
                                ARCustomToast.showToast(OTPVerificationScreen.this, childJson.optString("DESCRIPTION"));
                            }
                        }

                    } catch (Exception e) {
                        ARCustomToast.showToast(OTPVerificationScreen.this, "Invalid response, parsing error!!!");
                    }
                } else {
                    if (Utility.isInternetAvailable(OTPVerificationScreen.this))
                        ARCustomToast.showToast(OTPVerificationScreen.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(OTPVerificationScreen.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(OTPVerificationScreen.this))
                    ARCustomToast.showToast(OTPVerificationScreen.this, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(OTPVerificationScreen.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void registerNewUserFromCMSdk(String msisdn) {
        try {
            //USE THIS METHOD IF YOU WANT TO REGISTER NEW USER[FIRST TIME]
            HybridMessaging.registerNewUser(msisdn, new OnRegistrationListener() {
                @Override
                public void onReceivedRegistration(Registration registrationResponse) {
                    // TODO Auto-generated method stub
                    try {
//						Log.i("", "CM SDK value of registrations Status: "+registrationResponse.getStatus());

                        if (registrationResponse.getStatus().toString().equalsIgnoreCase("WaitingForPin")) {
//							showOTPLayout();
                        } else if (registrationResponse.getStatus().toString().equalsIgnoreCase("Verified")) {
                            //CALLING EDIT PROFILE API, IN ORDER TO SAVE THE VERIFIED MOBILE NUMBER
                            new LoadResponseViaPost(OTPVerificationScreen.this, formEditProfileJson(), true).execute("");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable arg0) {
                    // TODO Auto-generated method stub
                }
            });
        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    private void showOTPLayout() {
        otpNumberETField.requestFocus();
        otpNumberLabel.setVisibility(View.VISIBLE);
        otpNumberETField.setVisibility(View.VISIBLE);
        otpNumberProceedButton.setVisibility(View.VISIBLE);
        otpResendButton.setVisibility(View.VISIBLE);
        mobileNumberProceedButton.setVisibility(View.GONE);

        otpMobileNoETField.setEnabled(false);
    }

    private void confirmReceivedOTP(String verificationCode) {
        HybridMessaging.registerUserByPincode(verificationCode, new OnVerificationStatus() {

            @Override
            public void onVerificationStatus(VerificationStatus verificationStatus) {
                // TODO Auto-generated method stub
                if (verificationStatus.toString().equalsIgnoreCase("Verified")) {
                    isNumberVerifiedFlag = true;
                    otpVerifyProgressBar.setVisibility(View.GONE);
                    enableMyField();

                    //CALLING EDIT PROFILE API, IN ORDER TO SAVE THE VERIFIED MOBILE NUMBER
                    SharedPreferences.Editor otpEditor = preference.edit();
                    otpEditor.putBoolean("isUserVerifiedFromCM", true);
                    otpEditor.putString("VerifiedMobNoViaCM", otpMobileNoETField.getText().toString());
                    otpEditor.apply();

                    if (isComingFromLoginScreen) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("RESPONSE", true);
                        setResult(RESULT_OK, returnIntent);
                        OTPVerificationScreen.this.finish();
                    } else {
                        new LoadResponseViaPost(OTPVerificationScreen.this, formEditProfileJson(), true).execute("");
                    }
                } else {
                    otpVerifyProgressBar.setVisibility(View.GONE);
                    enableMyField();
                    ARCustomToast.showToast(OTPVerificationScreen.this, "Last pin verification failed", Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onError(Throwable verificationError) {
                // TODO Auto-generated method stub
                otpVerifyProgressBar.setVisibility(View.GONE);
                enableMyField();
            }
        });
    }

    private void resendOTP(String enteredMsisdn) {
        HybridMessaging.requestNewVerificationPin(enteredMsisdn, new OnRegistrationListener() {
            @Override
            public void onReceivedRegistration(Registration resendRegResponse) {
                // TODO Auto-generated method stub
                otpVerifyProgressBar.setVisibility(View.GONE);
                enableMyField();
            }

            @Override
            public void onError(Throwable error) {
                // TODO Auto-generated method stub
                otpVerifyProgressBar.setVisibility(View.GONE);
                enableMyField();
            }
        });
    }

    private void enableMyField() {
        otpNumberETField.setEnabled(true);
        otpResendButton.setEnabled(true);
        otpNumberProceedButton.setEnabled(true);
    }

    private void disableMyField() {
        otpNumberETField.setEnabled(false);
        otpResendButton.setEnabled(false);
        otpNumberProceedButton.setEnabled(false);
    }

    private String formEditProfileJson() {
        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("APISERVICE", "KPEDITPROFILE");
            jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            jsonobj.put("DEVICEOS", "Android");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("EMAILID", preference.getString("KP_USER_EMAIL_ID", ""));
            jsonobj.put("GENDER", "");
            jsonobj.put("FIRSTNAME", preference.getString("KP_USER_NAME", ""));
            jsonobj.put("MIDNAME", "");
            jsonobj.put("LASTNAME", "");
            jsonobj.put("MOBILENUMBER", otpMobileNoETField.getText().toString());
            jsonobj.put("TITLE", "");
            jsonobj.put("DOB", "");
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
            jsonobj.put("ADDRESSONE", "UNITEDKINGDOM");
            jsonobj.put("ADDRESSTWO", "");
            jsonobj.put("ADDRESSTHREE", "");
            jsonobj.put("POSTALTOWN", "");
            jsonobj.put("POSTALCODE", "");
            jsonobj.put("COUNTRY", "");
            jsonobj.put("RECEIVEPROMOTIONAL", "0");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("") && !loadedString.equals("Exception")) {
            try {
                JSONObject jsonObject = new JSONObject(loadedString);
                JSONObject responseJsonObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

                if (responseJsonObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                    if (responseJsonObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("RESPONSE", true);
                        setResult(RESULT_OK, returnIntent);

                        OTPVerificationScreen.this.finish();
                    } else {
                        if (responseJsonObject.has("DESCRIPTION")) {
                            ARCustomToast.showToast(OTPVerificationScreen.this, responseJsonObject.getString("DESCRIPTION"),
                                    Toast.LENGTH_LONG);
                        }
                    }
                }

            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!isComingFromFBLogin)
            super.onBackPressed();
    }
}
