package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.adapters.FavouriteScreenAdapter;
import com.uk.recharge.kwikpay.myaccount.MyTransactionActivity;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressLint("NewApi")
public class FavouriteScreen extends Activity implements OnClickListener, AsyncRequestListenerViaPost {
    private ListView favouriteListView;
    private FavouriteScreenAdapter favouriteAdapter;
    private ArrayList<HashMap<String, String>> favouriteArrayList;
    private JSONArray favJsonArray;
    private int apiHitPosition = 0, favTransPosition;
    private HashMap<String, String> favouriteHashMap, selectedFavTxnHashMap, repeatRechargeHashMap;
    private SharedPreferences preferences;
    boolean isRadioOn = false;
    private String selectedOrderId = "";
    private TextView footerMyProfileBtn, footerMyTxn;
    private TextView noFavouriteMessageTV;

//	TextView headerBelow_ClassTV,headerTicketBtn,selectedDisplayAmount="";
//	TextView headerProfileBtn,headerChangePWBtn,cartItemCounter;
//	View separatorLine;
//	TextView favButton;
//	LinearLayout myTransitionLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.favourite_screen);

        settingIDs();

        apiHitPosition = 1;
        new LoadResponseViaPost(FavouriteScreen.this, formFavouriteJSON(), true).execute("");

        favouriteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isRadioOn = true;
                selectedFavTxnHashMap = (HashMap<String, String>) favouriteArrayList.get(position);
                favTransPosition = position;
                selectedOrderId = selectedFavTxnHashMap.get(ProjectConstant.FAV_ORDERID);
//				selectedDisplayAmount=selectedFavTxnHashMap.get(ProjectConstant.FAV_AMOUNT);

                showPopUpMenu();

            }
        });

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_FAVOURITE));
        /*	String eventName=getResources().getString(R.string.SCREEN_NAME_FAVOURITE);
            AppsFlyerLib.trackEvent(this, eventName, null);
			EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
			((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPopUpMenu() {
        final CharSequence[] items = {"Repeat", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {

                if (position == 0) {
                    apiHitPosition = 3;
                    new LoadResponseViaPost(FavouriteScreen.this, formRepeatRechargeJSON(), true).execute("");
                } else if (position == 1) {
                    apiHitPosition = 2;
                    new LoadResponseViaPost(FavouriteScreen.this, formFavDeleteJSON(), true).execute("");
                }

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void settingIDs() {
        preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        favouriteListView = (ListView) findViewById(R.id.favouriteListView);

        footerMyProfileBtn = (TextView) findViewById(R.id.footerEditProfileBtn);
        footerMyTxn = (TextView) findViewById(R.id.footerMyTxnBtn);

        footerMyProfileBtn.setOnClickListener(this);
        footerMyTxn.setOnClickListener(this);

        TextView headerTitle = (TextView) findViewById(R.id.mainHeaderTitleName);
        headerTitle.setText("My Favourites ");

        noFavouriteMessageTV = (TextView) findViewById(R.id.noFavouriteMessage);

        TextView myFavouriteButton = (TextView) findViewById(R.id.footerFavouriteBtn);
        myFavouriteButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.favorite_icon, 0, 0);
        myFavouriteButton.setTextColor(getResources().getColor(R.color.darkGreen));


//		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);
//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

//		headerProfileBtn=(TextView)findViewById(R.id.header_MyProfileBtn);
//		headerChangePWBtn=(TextView)findViewById(R.id.header_ChangePWBtn);

//		headerTicketBtn=(TextView)findViewById(R.id.header_MyTicketBtn);
//		myTransitionLinearLayout=(LinearLayout)findViewById(R.id.myTransitionHeaderLayout);
//		myTransitionLinearLayout.setVisibility(View.VISIBLE);

        //SETTING THE IMAGE AND COLOR ON TEXT ON BUTTON PROGRAMMATICALLY

		/*favButton=(TextView)findViewById(R.id.header_FavouriteBtn);
//		favButton.setBackground(getResources().getDrawable(R.drawable.menu_header_btn_active));
		favButton.setBackgroundResource(R.drawable.menu_header_btn_active);
		favButton.setTextColor(getResources().getColor(R.color.darkGreen));*/

        //SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

        ImageView homeButtonImage = ((ImageView) findViewById(R.id.headerHomeBtn));
        ImageView headerMenuImage = (ImageView) findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);
        LinearLayout cartIconLayout = (LinearLayout) findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

        CustomSlidingDrawer csd = new CustomSlidingDrawer(FavouriteScreen.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage, cartIconLayout);
        csd.createMenuDrawer();

        //END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

//		headerProfileBtn.setOnClickListener(this);
//		headerChangePWBtn.setOnClickListener(this);
//		headerTicketBtn.setOnClickListener(this);

        // OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME
		/*DBQueryMethods database=new DBQueryMethods(FavouriteScreen.this);
		try
		{
			database.open();
			headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
			headerBelow_ClassTV.setText(database.getSubHeaderTitles("My favourites"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}*/
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        if (v.getId() == R.id.footerEditProfileBtn) {
            startActivity(new Intent(FavouriteScreen.this, EditProfile.class));
        } else if (v.getId() == R.id.footerMyTxnBtn) {
//			startActivity(new Intent(FavouriteScreen.this,MyTransaction.class));
            startActivity(new Intent(FavouriteScreen.this, MyTransactionActivity.class));
        }

		/*if(v.getId()==R.id.header_MyProfileBtn)
		{
			startActivity(new Intent(FavouriteScreen.this,EditProfile.class));
		}

		if(v.getId()==R.id.header_ChangePWBtn)
		{
			startActivity(new Intent(FavouriteScreen.this,ChangePassword.class));
		}

		if(v.getId()==R.id.header_MyTicketBtn)
		{
			startActivity(new Intent(FavouriteScreen.this,MyTicket.class));
		}*/

    }

    public void favouriteOnClickMethod(View favView) {
		/*switch (favView.getId()) 
		{
		case R.id.favDeleteBtn:
			if(isRadioOn)
			{
				apiHitPosition=2;
				new LoadResponseViaPost(FavouriteScreen.this, formFavDeleteJSON(), true).execute("");
			}
			else
			{
				ARCustomToast.showToast(FavouriteScreen.this, "please select a transaction to delete favourite", Toast.LENGTH_LONG);
			}
			break;

		case R.id.favRepeatBtn:
			if(isRadioOn)
			{
				apiHitPosition=3;
				new LoadResponseViaPost(FavouriteScreen.this, formRepeatRechargeJSON(), true).execute("");
			}
			else
			{
				ARCustomToast.showToast(FavouriteScreen.this, "please select a valid transaction to Repeat the same", Toast.LENGTH_LONG);
			}
			break;
		default:
			break;
		}*/

    }

    // Expensive operation method
	/*public static void setListViewHeightBasedOnChildren(ListView listView) 
	{
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		int totalHeight=0;
		View view = null;

		for (int i = 0; i < listAdapter.getCount(); i++) 
		{
			view = listAdapter.getView(i, view, listView); 
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.MATCH_PARENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}*/

    // FORMING JSON TO SHOW ALL LIST OF FAVOURITES
    public String formFavouriteJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPSHOWFAVOURITE");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("USERID", preferences.getString("KP_USER_ID", "0"));
//			json.put("USERID", "62");

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    // FORMING JSON TO DELETE THE FAVOURITE
    public String formFavDeleteJSON() {
        JSONObject jsonobj = new JSONObject();

        try {

            jsonobj.put("APISERVICE", "KPSAVEFAVOURITE");
            jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
            jsonobj.put("DEVICEOS", "Android");
            jsonobj.put("SOURCENAME", "KPAPP");
            jsonobj.put("USERID", preferences.getString("KP_USER_ID", "0"));
            jsonobj.put("NICKNAME", selectedFavTxnHashMap.get(ProjectConstant.FAV_NICKNAME));
            jsonobj.put("AMOUNT", selectedFavTxnHashMap.get(ProjectConstant.FAV_AMOUNT));
            jsonobj.put("ORDERID", selectedFavTxnHashMap.get(ProjectConstant.FAV_ORDERID));
            jsonobj.put("SUBSCRIBERNUM", selectedFavTxnHashMap.get(ProjectConstant.FAV_SUBSCRIBERNUM));
            jsonobj.put("OPERATORCODE", selectedFavTxnHashMap.get(ProjectConstant.FAV_OPCODE));
            //			jsonobj.put("SERVICEID", selectedFavTxnHashMap.get(ProjectConstant.TXN_SERVICEID));
            jsonobj.put("SERVICEID", selectedFavTxnHashMap.get(ProjectConstant.TXN_SERVICEID));
            jsonobj.put("COUNTRYCODE", selectedFavTxnHashMap.get(ProjectConstant.FAV_COUNTRYCODE));
            jsonobj.put("FAVOURITEENABLEFLAG", "0");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();

    }

    //FORMING JSON FOR REPEAT RECHARGE
    public String formRepeatRechargeJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPREPEATRECHARGE");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("ORDERID", selectedOrderId);

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {
        // TODO Auto-generated method stub

        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 1) {
                    if (parseMyFavouriteDetails(loadedString)) {
//						showFavouriteList(favouriteArrayList);
                        favouriteAdapter = new FavouriteScreenAdapter(FavouriteScreen.this, favouriteArrayList);
                        favouriteListView.setAdapter(favouriteAdapter);
//						setListViewHeightBasedOnChildren(favouriteListView);	
                    } else {
                        // FINISHING THE CURRENT ACTIVITY, IF THERE IS NO FAVOURITE FOUND OR GOT SOME EXECEPTION
//						FavouriteScreen.this.finish();
                        noFavouriteMessageTV.setText("You have no favourites, please select transaction from My Top-ups & set as favourite.");
                    }
                }

                if (apiHitPosition == 2) {
                    if (parseFavDelete(loadedString)) {
                        if (favouriteArrayList.size() > 0) {
                            isRadioOn = false;

                            favouriteArrayList.remove(favTransPosition);
                            favouriteAdapter.notifyDataSetChanged();

//							showFavouriteList(favouriteArrayList);
                            favouriteAdapter = new FavouriteScreenAdapter(FavouriteScreen.this, favouriteArrayList);
                            favouriteListView.setAdapter(favouriteAdapter);
//							setListViewHeightBasedOnChildren(favouriteListView);	
                        }

                    }
                }

                if (apiHitPosition == 3) {
                    if (parseFavRepeatRecharge(loadedString)) {
                        Intent payNowIntent = new Intent(FavouriteScreen.this, OrderDetails.class);
                        payNowIntent.putExtra("PAYNOW_DETAILS_VIA_RR_HASHMAP", repeatRechargeHashMap);
                        startActivity(payNowIntent);
                    }
                }

            } else {
                FavouriteScreen.this.finish();
                ARCustomToast.showToast(FavouriteScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
            FavouriteScreen.this.finish();
            ARCustomToast.showToast(FavouriteScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }

    }

    // PARSING THE FAVOURITE DATA COMING FROM API

    public boolean parseMyFavouriteDetails(String myTransactionResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(myTransactionResponse);
            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(FavouriteScreen.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has(ProjectConstant.FAV_FAVOURITES)) {
                        favouriteArrayList = new ArrayList<HashMap<String, String>>();
                        Object object = parentResponseObject.get(ProjectConstant.FAV_FAVOURITES);

                        // CHECKING WHETHER THE DATA INSIDE FAVOURITES TAG IS OBJECT OR JSON OBJECT

                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            favJsonArray = new JSONArray();
                            favJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray) {
                            favJsonArray = (JSONArray) object;
                        }


                        for (int i = 0; i < favJsonArray.length(); i++) {
                            childResponseObject = favJsonArray.getJSONObject(i);

                            if (!childResponseObject.isNull(ProjectConstant.FAV_COUNTRYCODE)
                                    && !childResponseObject.isNull(ProjectConstant.FAV_SUBSCRIBERNUM)
                                    && !childResponseObject.isNull(ProjectConstant.FAV_ORDERID)
                                    && !childResponseObject.isNull(ProjectConstant.FAV_OPCODE)
                                    && !childResponseObject.isNull(ProjectConstant.FAV_AMOUNT)
                                    && !childResponseObject.isNull(ProjectConstant.FAV_NICKNAME)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_SERVICEID)) {
                                favouriteHashMap = new HashMap<String, String>();
                                favouriteHashMap.put(ProjectConstant.FAV_COUNTRYCODE, childResponseObject.getString(ProjectConstant.FAV_COUNTRYCODE));
                                favouriteHashMap.put(ProjectConstant.FAV_SUBSCRIBERNUM, childResponseObject.getString(ProjectConstant.FAV_SUBSCRIBERNUM));
                                favouriteHashMap.put(ProjectConstant.FAV_ORDERID, childResponseObject.getString(ProjectConstant.FAV_ORDERID));
                                favouriteHashMap.put(ProjectConstant.FAV_OPCODE, childResponseObject.getString(ProjectConstant.FAV_OPCODE));
                                favouriteHashMap.put(ProjectConstant.FAV_AMOUNT, childResponseObject.getString(ProjectConstant.FAV_AMOUNT));
                                favouriteHashMap.put(ProjectConstant.FAV_NICKNAME, childResponseObject.getString(ProjectConstant.FAV_NICKNAME));
                                favouriteHashMap.put(ProjectConstant.TXN_SERVICEID, childResponseObject.getString(ProjectConstant.TXN_SERVICEID));

                                favouriteArrayList.add(favouriteHashMap);

                            }
                        }
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
//						if(!parentResponseObject.getString("DESCRIPTION").equals(""))
//							ARCustomToast.showToast(FavouriteScreen.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);

                        return false;
                    }

                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

        return true;
    }

    // PARSING THE FAVOURITE DATA COMING FROM API

    public boolean parseFavDelete(String favDeleteResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(favDeleteResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(FavouriteScreen.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(FavouriteScreen.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }

                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

        return true;
    }

    // PARSING THE REPEAT RECHARGE DATA FROM API

    public boolean parseFavRepeatRecharge(String repeatRCResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(repeatRCResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    repeatRechargeHashMap = new HashMap<String, String>();

                    if (parentResponseObject.has(ProjectConstant.TXN_DISPLAYAMOUNT))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, parentResponseObject.getString(ProjectConstant.TXN_DISPLAYAMOUNT).toString());
                    else
                        repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, parentResponseObject.getString("DISPLAYAMT").toString());


                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID).toString());
                    if (parentResponseObject.has("DISPLAYAMOUNTCURRENCYCODE"))
                        repeatRechargeHashMap.put("DISPLAYAMOUNTCURRENCYCODE", parentResponseObject.getString("DISPLAYAMOUNTCURRENCYCODE").toString());

                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGEAMT))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGEAMT, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGEAMT).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_RECHARGEAMOUNT))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, parentResponseObject.getString(ProjectConstant.TXN_RECHARGEAMOUNT).toString());

                    if (parentResponseObject.has(ProjectConstant.TXN_SERVICEID))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICEID, parentResponseObject.getString(ProjectConstant.TXN_SERVICEID).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_OPERATORNAME))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_OPERATORNAME, parentResponseObject.getString(ProjectConstant.TXN_OPERATORNAME).toString());
                    if (parentResponseObject.has("EMAILID"))
                        repeatRechargeHashMap.put("EMAILID", parentResponseObject.getString("EMAILID").toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PROCESSINGFEE))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_PROCESSINGFEE).toString());
                    if (parentResponseObject.has("PGAMT"))
                        repeatRechargeHashMap.put("PGAMT", parentResponseObject.getString("PGAMT").toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_PAYMENTCURRENCYCODE))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, parentResponseObject.getString(ProjectConstant.TXN_PAYMENTCURRENCYCODE).toString());

                    if (parentResponseObject.has(ProjectConstant.TXN_SERVICENAME))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICENAME, parentResponseObject.getString(ProjectConstant.TXN_SERVICENAME).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_COUNTRYCODE))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_COUNTRYCODE, parentResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE).toString());
                    if (parentResponseObject.has("MOBILENUMBER"))
                        repeatRechargeHashMap.put("MOBILENUMBER", parentResponseObject.getString("MOBILENUMBER").toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_SERVICEFEE))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_SERVICEFEE).toString());
                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID).toString());
                    if (parentResponseObject.has(ProjectConstant.API_OPCODE))
                        repeatRechargeHashMap.put(ProjectConstant.API_OPCODE, parentResponseObject.getString(ProjectConstant.API_OPCODE).toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PLANDESCRIPTION))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, parentResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION).toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, parentResponseObject.getString(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID).toString());

                    repeatRechargeHashMap.put("OPERATORPRODUCTCODE", parentResponseObject.optString("OPERATORPRODUCTCODE"));

                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(FavouriteScreen.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preferences!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preferences.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);
		}
		}
	}
*/
}
