package com.uk.recharge.kwikpay.singleton;

import com.uk.recharge.kwikpay.gamingarcade.models.GameMachineInfoMO;

/**
 * Created by Ashu Rajput on 7/6/2018.
 */

public class GamingInfoSingleton {

    private static GamingInfoSingleton ourInstance = null;
    private GameMachineInfoMO gameMachineInfoMO = null;

    public static GamingInfoSingleton getInstance() {
        if (ourInstance == null)
            ourInstance = new GamingInfoSingleton();
        return ourInstance;
    }

    private GamingInfoSingleton() {
    }

    public GameMachineInfoMO getGameMachineInfoMO() {
        return gameMachineInfoMO;
    }

    public void setGameMachineInfoMO(GameMachineInfoMO gameMachineInfoMO) {
        this.gameMachineInfoMO = gameMachineInfoMO;
    }

    public void clearGameMachineMO() {
        gameMachineInfoMO = null;
    }
}
