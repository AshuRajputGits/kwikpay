package com.uk.recharge.kwikpay.kwikcharge.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 1/19/2018.
 */

public class MapChargerLocationsMO {

    private String locationName;
    private String locationAddress;
    private String latitude;
    private String longitude;
    private String deviceStatus;
    private String connectorType;
    private String connectorStatus;
    private ArrayList<ConnectorListMO> connectorListMOArrayList;
    private ArrayList<LatLng> latlongArrayList;
    private LatLng latLng;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public ArrayList<LatLng> getLatlongArrayList() {
        return latlongArrayList;
    }

    public void setLatlongArrayList(ArrayList<LatLng> latlongArrayList) {
        this.latlongArrayList = latlongArrayList;
    }

    public ArrayList<ConnectorListMO> getConnectorListMOArrayList() {
        return connectorListMOArrayList;
    }

    public void setConnectorListMOArrayList(ArrayList<ConnectorListMO> connectorListMOArrayList) {
        this.connectorListMOArrayList = connectorListMOArrayList;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getConnectorType() {
        return connectorType;
    }

    public void setConnectorType(String connectorType) {
        this.connectorType = connectorType;
    }

    public String getConnectorStatus() {
        return connectorStatus;
    }

    public void setConnectorStatus(String connectorStatus) {
        this.connectorStatus = connectorStatus;
    }
}
