package com.uk.recharge.kwikpay.services;

import android.app.Notification;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.uk.recharge.kwikpay.R;
import com.urbanairship.push.PushMessage;
import com.urbanairship.push.notifications.ActionsNotificationExtender;
import com.urbanairship.push.notifications.NotificationFactory;
import com.urbanairship.util.UAStringUtil;

/**
 * Created by Ashu Rajput on 24-05-2017.
 */

public class CustomNotificationFactory extends NotificationFactory {

    private Context context;

    public CustomNotificationFactory(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public Notification createNotification(PushMessage message, int notificationId) {
        // do not display a notification if there is not an alert
        if (UAStringUtil.isEmpty(message.getAlert())) {
            return null;
        }

        // Build the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext())
                .setContentTitle("Kwikpay")
                .setContentText(message.getAlert())
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.kp_notification_icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.kwikpay_logo));

        builder.extend(new ActionsNotificationExtender(getContext(), message, notificationId));
        return builder.build();
    }

    /*@Override
    public int getNextId(PushMessage pushMessage) {
//        return NotificationIDGenerator.nextID();
    }*/
}
