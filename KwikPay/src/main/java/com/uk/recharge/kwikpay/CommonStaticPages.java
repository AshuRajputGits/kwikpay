package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

//*********************************************************//
// HowItWork---Page
// Product Description--Page
//********************************************************//

public class CommonStaticPages extends Activity {
    private ProgressDialog mProgressDialog;
    private WebView webView;
    private SharedPreferences preference;
    private TextView headerTitle, iAcceptButton;
    private boolean isTnCPage = false;
    private boolean isComingFromEBScreen = false;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.how_it_works);

        settingIDs();
        String pageName = "", viewProductDescURL = "";
        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("STATIC_PAGE_NAME")) {
                pageName = receivedBundle.getString("STATIC_PAGE_NAME");
                viewProductDescURL = receivedBundle.getString("VIEW_PDT_URL");
            }
        }

        //********************SANDBOX BASE URL**************************//
//        String baseURL="http://46.37.168.7:8080/kwikpay/kwikpay/apphtml/";
        //****************LIVE PRODUCTION BASE URL*********************//
//        String baseURL = "https://kwikpay.co.uk/kwikpay/kwikpay/apphtml/";

        String baseURL = getResources().getString(R.string.staticPageURL);

//        https://kwikpay.co.uk/kwikpay/kwikpay/apphtml/privacy_policy.html
//        http://46.37.168.7:8080/kwikpaybeta/kwikpay/apphtml/privacy_policy.html

        webView.getSettings().setJavaScriptEnabled(true);
        MyWebClient webClient = new MyWebClient(CommonStaticPages.this);
        webView.setWebViewClient(webClient);

        String callingURL = "", titleName = "";
        if (pageName != null && pageName.equalsIgnoreCase("HOWITWORK_PAGE")) {
            titleName = "FAQs";
            //SETTING SCREEN NAMES TO APPS FLYER
            try {
                EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_HOW_IT_WORKS));
            } catch (Exception e) {
                e.printStackTrace();
            }

            callingURL = baseURL + preference.getString(ProjectConstant.DB_HOW_IT_WORKS_REVISED, "how_it_works") + ".html";

        } else if (pageName != null && pageName.equalsIgnoreCase("PRIVACY_POLICY")) {
            titleName = "Privacy Policy";
            callingURL = baseURL + "privacy_policy.html";
        } else if (pageName != null && pageName.equalsIgnoreCase("PDT_DESCRIPTION_PAGE")) {
            titleName = "Product Description";
            callingURL = viewProductDescURL;
        } else if (pageName != null && pageName.equalsIgnoreCase("TNC_PAGE")) {
            titleName = "Terms & Condition";
            callingURL = ProjectConstant.tncURL;
            isTnCPage = true;
            iAcceptButton.setVisibility(View.VISIBLE);
        } else if (pageName != null && pageName.equalsIgnoreCase("EB_TNC_PAGE")) {
            titleName = "Terms & Condition";
            callingURL = ProjectConstant.tncURL;
            isTnCPage = true;
            isComingFromEBScreen = true;
            iAcceptButton.setVisibility(View.VISIBLE);
        }

        headerTitle.setText(titleName);
        webView.loadUrl(callingURL);

        iAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isComingFromEBScreen)
                    getSharedPreferences("KP_Preference", MODE_PRIVATE).edit().putBoolean("IS_KC_TNC_CHECKED", true).apply();

                setResult(Activity.RESULT_OK, new Intent());
                CommonStaticPages.this.finish();
            }
        });

    }

    private void settingIDs() {
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        headerTitle = findViewById(R.id.mainHeaderTitleName);
        iAcceptButton = findViewById(R.id.tncIAcceptButton);
        webView = findViewById(R.id.howitworks_WebView);

        //SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = (findViewById(R.id.headerHomeBtn));
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);

        CustomSlidingDrawer csd = new CustomSlidingDrawer(CommonStaticPages.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage);   // EARLIER IT IS TRUE TO MAKE ICON UN CLICKABLE
        csd.createMenuDrawer();
    }

    public class MyWebClient extends WebViewClient {
        public MyWebClient(Context howitworksContext) {
            mProgressDialog = new ProgressDialog(howitworksContext);
            mProgressDialog.setMessage("loading please wait...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            try {
                if (!isFinishing()) {
                    mProgressDialog.show();
                }
            } catch (Exception exception) {
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            try {
                if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            } catch (final IllegalArgumentException ae) {
            } catch (final Exception excep) {
            } finally {
                mProgressDialog = null;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isTnCPage) {
            //stop back pressed
        } else
            super.onBackPressed();
    }
}
