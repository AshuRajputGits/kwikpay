package com.uk.recharge.kwikpay;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidationWithMessage;
import com.uk.recharge.kwikpay.myaccount.MyTransactionActivity;
import com.uk.recharge.kwikpay.utilities.CustomDialogBox;

public class ChangePassword extends Activity implements OnClickListener, AsyncRequestListenerViaPost
{
	private TextView labelCurrentPaaword,labelNewPW,labelConfirmPW;
	private boolean screenIdentifierFlag=false;
	private EditText currentPassword, newPassword, confirmPassword;
//	private TextView changePWBtn,headerFavBtn,headerProfileBtn,headerTicketBtn;
	private SharedPreferences preferences;
	private TextView footerFavouriteTxnBtn,footerMyTxnBtn,footerMyProfileBtn;
//	private TextView cartItemCounter;
//	private LinearLayout myAccountLinearLayout;
//	private View lineSeparator2;
//	private LinearLayout cartIconLayout;
//	private View separatorLine;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.changepassword);

		settingIds();

		//RESTRICTING ' IN PASSWORD FIELD
		FieldValidationWithMessage.blockMyCharacter(currentPassword, "'");
		FieldValidationWithMessage.blockMyCharacter(newPassword, "'");
		FieldValidationWithMessage.blockMyCharacter(confirmPassword, "'");

	}

	@SuppressLint("NewApi")
	private void settingIds() 
	{
		// TODO Auto-generated method stub

		preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);
		
		/*myAccountLinearLayout=(LinearLayout)findViewById(R.id.myAccountHeaderLayout);
		myAccountLinearLayout.setVisibility(View.VISIBLE);*/

		labelCurrentPaaword=(TextView)findViewById(R.id.changepw_CurrentPWTV);
		labelNewPW=(TextView)findViewById(R.id.changepw_NewPWTV);
		labelConfirmPW=(TextView)findViewById(R.id.changepw_ConfirmPWTV);
		currentPassword=(EditText)findViewById(R.id.changepw_CurrentPW);
		newPassword=(EditText)findViewById(R.id.changepw_NewPW);
		confirmPassword=(EditText)findViewById(R.id.changepw_ConfirmPW);

		labelCurrentPaaword.setText(Html.fromHtml("Current Password"+"<font color='#ff0000'>*"));
		labelNewPW.setText(Html.fromHtml("New Password"+"<font color='#ff0000'>*"));
		labelConfirmPW.setText(Html.fromHtml("Confirm Password"+"<font color='#ff0000'>*"));
		
		footerFavouriteTxnBtn=(TextView)findViewById(R.id.footerFavouriteBtn);
		footerMyTxnBtn=(TextView)findViewById(R.id.footerMyTxnBtn);
		footerMyProfileBtn=(TextView)findViewById(R.id.footerEditProfileBtn);

//		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);

		/*headerFavBtn=(TextView)findViewById(R.id.header_FavouriteBtn);
		headerProfileBtn=(TextView)findViewById(R.id.header_MyProfileBtn);
		headerTicketBtn=(TextView)findViewById(R.id.header_MyTicketBtn);*/

		//SETTING THE IMAGE AND COLOR ON TEXT ON BUTTON PROGRAMMATICALLY 

		/*changePWBtn=(TextView)findViewById(R.id.header_ChangePWBtn);
//		changePWBtn.setBackgroundResource(R.drawable.menu_header_btn_active);
		changePWBtn.setTextColor(getResources().getColor(R.color.darkGreen));*/
		
		TextView headerTitle=(TextView)findViewById(R.id.mainHeaderTitleName);
		headerTitle.setText("Change Password");

//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(ChangePassword.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		/*headerFavBtn.setOnClickListener(this);
		headerProfileBtn.setOnClickListener(this);
		headerTicketBtn.setOnClickListener(this);*/

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME

		/*DBQueryMethods database=new DBQueryMethods(ChangePassword.this);
		try
		{
			database.open();
			TextView headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
			headerBelow_ClassTV.setText(database.getSubHeaderTitles("Change password"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}*/
		
		
		footerFavouriteTxnBtn.setOnClickListener(this);
		footerMyTxnBtn.setOnClickListener(this);
		footerMyProfileBtn.setOnClickListener(this);
		
	}

	public void changePWSubmitButton(View changePwView)
	{
		if(ChangePWValidation())
		{
			final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(changePwView.getWindowToken(), 0);

			new LoadResponseViaPost(ChangePassword.this, formJSONParam() , true).execute("");
		}
	}

	public String formJSONParam() 
	{
		JSONObject jsonobj = new JSONObject();
		try 
		{
			String trimmedCurrentPassword = currentPassword.getText().toString().replaceAll(" +"," ").trim();
			String trimmedNewPassword = newPassword.getText().toString().replaceAll(" +"," ").trim();

			jsonobj.put("APISERVICE", "KPCHANGEPASSWORD");
			jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
			jsonobj.put("DEVICEOS", "Android");
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("USERID", preferences.getString("KP_USER_ID", "0"));
			jsonobj.put("OLDPASSWORD", trimmedCurrentPassword);
			jsonobj.put("NEWPASSWORD", trimmedNewPassword);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();

	}

	public boolean ChangePWValidation()
	{
		if(!FieldValidationWithMessage.passwordIsValid(currentPassword.getText().toString(), currentPassword, 8).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(newPassword);
			FieldValidationWithMessage.removeAllErrorIcons(confirmPassword);

			ARCustomToast.showToast(ChangePassword.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}
		else if(!FieldValidationWithMessage.passwordIsValid(newPassword.getText().toString(), newPassword, 8).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(currentPassword);
			FieldValidationWithMessage.removeAllErrorIcons(confirmPassword);

			ARCustomToast.showToast(ChangePassword.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}
		else if(!FieldValidationWithMessage.confirmPasswordIsValid(newPassword.getText().toString(), confirmPassword, confirmPassword.getText().toString()).equals("SUCCESS"))
		{
			FieldValidationWithMessage.removeAllErrorIcons(currentPassword);
			FieldValidationWithMessage.removeAllErrorIcons(newPassword);


			ARCustomToast.showToast(ChangePassword.this, FieldValidationWithMessage.getErrorMessage(), Toast.LENGTH_LONG);
			return false;
		}
		else
		{
			return true;
		}
	}

	@Override
	public void onBackPressed() 
	{
		// TODO Auto-generated method stub
		if(screenIdentifierFlag)
		{

		}
		else
		{
			super.onBackPressed();
		}

	}

	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.footerFavouriteBtn)
		{
			startActivity(new Intent(ChangePassword.this,FavouriteScreen.class));
		}
		else if(v.getId()==R.id.footerMyTxnBtn)
		{
//			startActivity(new Intent(ChangePassword.this,MyTransaction.class));
			startActivity(new Intent(ChangePassword.this,MyTransactionActivity.class));
		}
		else if(v.getId()==R.id.footerEditProfileBtn)
		{
			startActivity(new Intent(ChangePassword.this,EditProfile.class));
		}

		// TODO Auto-generated method stub
		/*if(v.getId()==R.id.header_MyProfileBtn)
		{
			startActivity(new Intent(ChangePassword.this,EditProfile.class));
		}*/

		/*if(v.getId()==R.id.header_FavouriteBtn)
		{
			startActivity(new Intent(ChangePassword.this,FavouriteScreen.class));
		}
		if(v.getId()==R.id.header_MyTicketBtn)
		{
			startActivity(new Intent(ChangePassword.this,MyTicket.class));
		}*/

	}

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub
//		Log.e("ChangePassowordResponse", loadedString);

		if(!loadedString.equals("Exception") && !loadedString.equals(""))
		{
			try 
			{
				JSONObject jsonObject,parentResponseObject;

				jsonObject = new JSONObject(loadedString);
				parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

				if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
				{
					if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
					{
						Intent i = new Intent(ChangePassword.this,CustomDialogBox.class);
						i.putExtra("MSG_KEY", parentResponseObject.getString("DESCRIPTION"));
						startActivity(i);
						ChangePassword.this.finish();
					}
					else
					{
						if(parentResponseObject.has("DESCRIPTION"))
						{
							ARCustomToast.showToast(ChangePassword.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						}
					}

				}

				else
				{
					ARCustomToast.showToast(ChangePassword.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
				}


			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		else
		{
			ARCustomToast.showToast(ChangePassword.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}
	}
	
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preferences!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preferences.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);
		}
		
		}
	}*/

}
