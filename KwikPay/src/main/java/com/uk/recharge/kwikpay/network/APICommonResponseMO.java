package com.uk.recharge.kwikpay.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashu Rajput on 04-02-2018.
 */

public class APICommonResponseMO {

    @SerializedName("APISERVICE")
    private String apiService;

    @SerializedName("SOURCENAME")
    private String sourceName;

    @SerializedName("SESSIONID")
    private String sessionID;

    @SerializedName("RESPONSECODE")
    private String responseCode;

    @SerializedName("DESCRIPTION")
    private String description;

    public String getApiService() {
        return apiService;
    }

    public void setApiService(String apiService) {
        this.apiService = apiService;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
