package com.uk.recharge.kwikpay;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.adapters.RecentTransactionsAdapter;
import com.uk.recharge.kwikpay.adapters.RecentTransactionsAdapter.RecentTransactionListener;
import com.uk.recharge.kwikpay.database.DBQueryMethods;
import com.uk.recharge.kwikpay.models.RecentTransactionsModel;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.urbanairship.UAirship;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TopUpYourPhone extends Activity implements AsyncRequestListenerViaPost, RecentTransactionListener {
    private Spinner operatorSpinner;
    private Button greenButton1, greenButton2, greenButton3, greenButton4, greenButton5, greenButton6;
    private TextView greenTextView1, greenTextView2, greenTextView3, greenTextView4, greenTextView5, greenTextView6;
    private LinearLayout sixGreenBtnLayout, secondHalfSixBtnLayout, recentTransactionLayout;
    private TextView labelOperator;
    private EditText mobileNumber;
    private ArrayAdapter<String> adapter_operator;
    //	private TextView headerBelowTV;
    //	private View separatorLine;
    private int mobileNoMaxLength, apiHitPosition = 0;
    private ArrayList<HashMap<String, String>> operatorsArrayList, topUpPlansArrayList;
    private HashMap<String, String> operatorsHashMap, topUpHashMap, payNowHashMap, getDetailFromDBHashMap;

    private String[] operatorsNameArray;
    private String noOfProducts = "", selectedOperatorFromSpinner = "", selectedOperatorIdFromSpinner = "";
    private Bitmap operatorImageBitmap;
    private ImageView chooseProductOnFlyIV;

    private LinearLayout greenBtnLayout1, greenBtnLayout2, greenBtnLayout3, greenBtnLayout4, greenBtnLayout5, greenBtnLayout6, topUpProductLayout;
    private JSONArray operatorsJsonArray;
    private SharedPreferences preference;
    private DBQueryMethods database;
    private String isLeadingAllowZero = "", ag_CountryCode = "", subscriberMobileNumber = "", cartDeleteMsg = "";
    private String viewProductDetailURL;

    private JSONArray txnJsonArray;
    private ArrayList<HashMap<String, String>> myTransitionArrayList;
    private ArrayList<RecentTransactionsModel> recentTxnModelList;
    private HashMap<String, String> repeatRechargeHashMap;
    private String selectedRechargeAmount = "";

    //private TextView productDescriptionLink;
    // apiHitPosition=1: for MSID
    // apiHitPosition=2: for Operator
    //	private Button paynowButton;
    //	private ImageView  selectedOperatorOnFly
    //	private LinearLayout selectedTopUpLayout
    //	private TextView choosedAboveTV,choosedBelowTV;

    private Spinner topupSpinner;
    private ArrayAdapter<String> adapter_topup;
    private LinearLayout contactPickerLayout;
    private ImageView contactPickerIcon;
    private String[] topUpPlansArray;

    public static int PERMISSION_REQUEST_CONTACT = 01;
    public static int REQUEST_PHONE_CODE = 02;
    public static int REQUEST_APP_SETTINGS = 03;
    private String productLogoURL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.top_up_phone);// OLD by ASHU
        settingIdsNew();

        try {
            if (!preference.getString("KP_USER_ID", "").equals("0")) {
                apiHitPosition = 3;
                new LoadResponseViaPost(this, userTransactionsJSON(), true).execute("");
            }

            database.open();
            getDetailFromDBHashMap = database.getDetailsForTopUpAndGameService();
            mobileNoMaxLength = Integer.parseInt(getDetailFromDBHashMap.get(ProjectConstant.AG_SUBS_MAX_LENGTH));
            isLeadingAllowZero = getDetailFromDBHashMap.get(ProjectConstant.AG_SUBS_ALLOW_LEADING_ZERO);
            ag_CountryCode = getDetailFromDBHashMap.get(ProjectConstant.AG_COUNTRY_CODE);

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        setMaxLengthToET(mobileNumber, mobileNoMaxLength);
        //		mobileNoMaxLength=10;

        mobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int finalLength, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
                if (s.length() == mobileNoMaxLength && count > 0) {
                    hideAllLayouts();
                    setDefaultValues();
                    mobileNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    contactPickerLayout.setVisibility(View.VISIBLE);
                    if (myTransitionArrayList != null && myTransitionArrayList.size() > 0)
                        recentTransactionLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable mobNumber) {
                // TODO Auto-generated method stub
                if (mobNumber.length() == mobileNoMaxLength) {

                    contactPickerLayout.setVisibility(View.GONE);
                    recentTransactionLayout.setVisibility(View.GONE);

                    final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mobileNumber.getWindowToken(), 0);

                    if (isLeadingAllowZero.equals("1")) {
                        if (mobileNumber.getText().toString().charAt(0) == '0' && mobileNumber.getText().toString().charAt(1) == '7') {
                            apiHitPosition = 1;
                            new LoadResponseViaPost(TopUpYourPhone.this, formMSIDNJson(), true).execute("");
                        } else {
                            if (mobileNumber.getText().toString().charAt(0) != '0') {
                                mobileNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                                ARCustomToast.showToast(TopUpYourPhone.this, "please enter valid mobile no. starting with 07", Toast.LENGTH_LONG);
                            } else {
                                mobileNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                                ARCustomToast.showToast(TopUpYourPhone.this, "please enter a valid mobile no. starting with 07", Toast.LENGTH_LONG);
                            }

                        }
                    } else {
                        if (mobileNumber.getText().toString().charAt(0) == '0') {
                            mobileNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
                            ARCustomToast.showToast(TopUpYourPhone.this, "please enter valid mobile no.  starting with 07", Toast.LENGTH_LONG);
                        } else {
                            apiHitPosition = 1;
                            new LoadResponseViaPost(TopUpYourPhone.this, formMSIDNJson(), true).execute("");
                        }
                    }
                }
            }
        });

        operatorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                hideChooseTopUpLayout();
                hideSelectedTopUP();
                setDefaultValues();
                if (position > 0) {
//					noOfProducts="0";
                    position--;
                    apiHitPosition = 2;
                    selectedOperatorFromSpinner = operatorsArrayList.get(position).get(ProjectConstant.API_OPCODE);
                    selectedOperatorIdFromSpinner = operatorsArrayList.get(position).get(ProjectConstant.API_OPID);

//					 new LoadResponseViaPost(TopUpYourPhone.this, formTopUpProductJson() , true).execute("");

                    // CHECKING OPERATOR ACTIVE FLAG
                    if (operatorsArrayList.get(position).get(ProjectConstant.API_OPACTIVEFLAG).equals("0")
                            || operatorsArrayList.get(position).get(ProjectConstant.API_OPCIRCLEACTIVEFLAG).equals("0")
                            || operatorsArrayList.get(position).get(ProjectConstant.API_OPTEMPACTIVEFLAG).equals("0")) {
                        ARCustomToast.showToast(TopUpYourPhone.this, "this operator is temporarily unavailable. please try after sometime.", Toast.LENGTH_LONG);
                    } else {
                        new LoadResponseViaPost(TopUpYourPhone.this, formTopUpProductJson(), true).execute("");
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // COMMENTING AS PER NEW REQ
        topupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub

                hideSelectedTopUP();

                if (position > 0) {
                    position--;
                    createHashMapForOrderDetail(position);
                    /*String selectedTopUpAbove=Html.fromHtml(topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) +" "+
                            topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYAMT);
					String selectedTopUpBelow=Html.fromHtml(topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) +" "+
							topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_PAYMENTAMT);
					showSelectedTopUP(selectedTopUpAbove,selectedTopUpBelow,position);*/
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_MOBILE_TOP_UP));
            /*String eventName=getResources().getString(R.string.SCREEN_NAME_MOBILE_TOP_UP);
            AppsFlyerLib.trackEvent(this, eventName, null);
            EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
            ((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/

        } catch (Exception e) {
            e.printStackTrace();
        }

//        productDescriptionLink.setOnClickListener(viewClickListener);
        contactPickerIcon.setOnClickListener(viewClickListener);

        try {
            if (preference.getString("URBAN_AIRSHIP_CHANNEL_ID", "").equals("")) {
                preference.edit().putString("URBAN_AIRSHIP_CHANNEL_ID", UAirship.shared().getPushManager().getChannelId()).apply();
            }
        } catch (Exception e) {
        }
    }

    private void settingIdsNew() {
        // TODO Auto-generated method stub
        database = new DBQueryMethods(TopUpYourPhone.this);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        mobileNumber = (EditText) findViewById(R.id.mobileNumb);
        labelOperator = (TextView) findViewById(R.id.topUpPhone_OperatorTV);
        topUpProductLayout = (LinearLayout) findViewById(R.id.topUpPhone_ChooseTopUPLabelLayout);
//		labelTopUp=(TextView)findViewById(R.id.topUpPhone_TopUpTV);
//		labelSelectedTopUp=(TextView)findViewById(R.id.topupPhone_SelectedTopUpLabel);

        chooseProductOnFlyIV = (ImageView) findViewById(R.id.topupPhone_onFlyOperatorImage);
        operatorSpinner = (Spinner) findViewById(R.id.selectTopUpOperatorSpinner);
        sixGreenBtnLayout = (LinearLayout) findViewById(R.id.sixbuttonLayout);
        secondHalfSixBtnLayout = (LinearLayout) findViewById(R.id.customTopUpSecondLayout);
        recentTransactionLayout = (LinearLayout) findViewById(R.id.recentTransactionMainLayout);
        topupSpinner = (Spinner) findViewById(R.id.selectTopupTopUpSpinner);
        contactPickerLayout = (LinearLayout) findViewById(R.id.contact_picker_layout);
        contactPickerIcon = (ImageView) findViewById(R.id.contact_picker_icon);


//        productDescriptionLink=(TextView)findViewById(R.id.topup_ProductDescriptionLink);

        //COMMENTING AS PER NEW REQUIREMENT
//		verifiedPaymentBanners=(ImageView)findViewById(R.id.topUpPhone_VerifiedImage);
        /*selectedOperatorOnFly=(ImageView)findViewById(R.id.topupPhone_onFlySelectedOprImage);
        selectedTopUpLayout=(LinearLayout)findViewById(R.id.topUpPhone_SelectedTopUPLayout);
		paynowButton=(Button)findViewById(R.id.topUpPhone_PayNowBtn);

		// SETTING IDs FOR CHOOSED TOPUP EITHER FROM 6 BUTTONS OR DROP DOWN
		choosedAboveTV=(TextView)findViewById(R.id.topUpPhone_SelectedAboveValue0);
		choosedBelowTV=(TextView)findViewById(R.id.topUpPhone_SelectedBelowValue0);*/

        //SETTING IDs FOR SIX GREEN BUTTONS AND LINEAR LAYOUT
        greenBtnLayout1 = (LinearLayout) findViewById(R.id.greenBox1);
        greenBtnLayout2 = (LinearLayout) findViewById(R.id.greenBox2);
        greenBtnLayout3 = (LinearLayout) findViewById(R.id.greenBox3);
        greenBtnLayout4 = (LinearLayout) findViewById(R.id.greenBox4);
        greenBtnLayout5 = (LinearLayout) findViewById(R.id.greenBox5);
        greenBtnLayout6 = (LinearLayout) findViewById(R.id.greenBox6);

        // SETTING THE IDs FOR SIX GREEN BUTTONS
        greenButton1 = (Button) findViewById(R.id.topupAboveValue1);
        greenButton2 = (Button) findViewById(R.id.topupAboveValue2);
        greenButton3 = (Button) findViewById(R.id.topupAboveValue3);
        greenButton4 = (Button) findViewById(R.id.topupAboveValue4);
        greenButton5 = (Button) findViewById(R.id.topupAboveValue5);
        greenButton6 = (Button) findViewById(R.id.topupAboveValue6);

        // SETTING THE IDs FOR SIX GREEN TEXT VIEWS
        greenTextView1 = (TextView) findViewById(R.id.topupBelowValue1);
        greenTextView2 = (TextView) findViewById(R.id.topupBelowValue2);
        greenTextView3 = (TextView) findViewById(R.id.topupBelowValue3);
        greenTextView4 = (TextView) findViewById(R.id.topupBelowValue4);
        greenTextView5 = (TextView) findViewById(R.id.topupBelowValue5);
        greenTextView6 = (TextView) findViewById(R.id.topupBelowValue6);

        //SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = ((ImageView) findViewById(R.id.headerHomeBtn));
        ImageView headerMenuImage = (ImageView) findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);
        LinearLayout cartIconLayout = (LinearLayout) findViewById(R.id.cartImageLayout);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(TopUpYourPhone.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage, cartIconLayout);
        csd.createMenuDrawer();
    }

    private String userTransactionsJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPTRANSACTIONHISTORY");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int viewId = v.getId();
            if (viewId == R.id.contact_picker_icon) {
                askContactPickerPermission();
            }
//            startActivity(new Intent(TopUpYourPhone.this, CommonStaticPages.class).putExtra("STATIC_PAGE_NAME", "PDT_DESCRIPTION_PAGE").putExtra("VIEW_PDT_URL", viewProductDetailURL));
        }
    };

    private void askContactPickerPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS)) {
                    showUserWhyWeNeedPermissionDialog();
                } else {
                    ActivityCompat.requestPermissions(TopUpYourPhone.this, new String[]{android.Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CONTACT);
                }
            } else {
                initiatingContactPickFromBook();
            }
        } else {
            initiatingContactPickFromBook();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CONTACT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initiatingContactPickFromBook();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(TopUpYourPhone.this, android.Manifest.permission.READ_CONTACTS)) {
                    showUserWhyWeNeedPermissionDialog();
                } else {
//                    Toast.makeText(getBaseContext(), "Grant permission from setting, to use this feature", Toast.LENGTH_LONG).show();
                    openNeverAskedSelectedDialog();
                }
            }
        }
    }

    private void initiatingContactPickFromBook() {
        mobileNumber.setText("");
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(intent, REQUEST_PHONE_CODE);
        } catch (Exception e) {
        }
    }

    private void showUserWhyWeNeedPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(TopUpYourPhone.this);
        builder.setMessage(getResources().getString(R.string.CONTACT_ACCESS_USER_MSG));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                ActivityCompat.requestPermissions(TopUpYourPhone.this, new String[]{android.Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CONTACT);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void openNeverAskedSelectedDialog() {
        new AlertDialog.Builder(TopUpYourPhone.this)
                .setMessage(getResources().getString(R.string.CONTACT_ACCESS_USER_MSG))
                .setPositiveButton("open setting", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        try {
                            Intent appSettingIntent = new Intent();
                            appSettingIntent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            appSettingIntent.setData(Uri.fromParts("package:", getPackageName(), null));
                            startActivityForResult(appSettingIntent, REQUEST_APP_SETTINGS);
                        } catch (Exception e) {
                            Toast.makeText(TopUpYourPhone.this, "Cannot access your setting page, grant permission manually from setting", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setCancelable(false)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PHONE_CODE) {
            Uri phoneNumberURI = data.getData();
            Cursor cursor = getContentResolver().query(phoneNumberURI, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    String number = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    if (number != null && !number.isEmpty()) {
//                        Log.d("PhoneNumber", "No: " + number);
                        validateContactPickerNumber(number);
                    }
                }
                cursor.close();
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_APP_SETTINGS) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                initiatingContactPickFromBook();
            }
        }
    }

    private void validateContactPickerNumber(String number) {
        number = number.replaceAll("[^0-9]+", "");
        if (number.startsWith("44"))
            number = "0" + number.substring(2);
        else if (number.startsWith("0044"))
            number = "0" + number.substring(4);

        if (number.length() < mobileNoMaxLength || number.length() > mobileNoMaxLength)
            ARCustomToast.showToast(TopUpYourPhone.this, "please enter a valid mobile no.", Toast.LENGTH_LONG);
        else if (!number.startsWith("07"))
            ARCustomToast.showToast(TopUpYourPhone.this, "please enter a valid mobile no. starting with 07", Toast.LENGTH_LONG);
        else {
            mobileNumber.setText(number);
            try {
                mobileNumber.setSelection(number.length());
            } catch (Exception e) {
            }
        }
    }

    public void topUpOnClickMethod(View topUpPayNowView) {
        // COMMENTING AS PER NEW REQUIREMENT

		/*if(topUpPayNowView.getId()==R.id.topUpPhone_PayNowBtn)
        {
			if(preference.getString("KP_USER_ID", "").equals("0"))
			{
				Intent logInIntent=new Intent(TopUpYourPhone.this,LoginScreen.class);
				logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
				startActivity(logInIntent);
			}
			else
			{
				// Create the Values
		        Map<String,Object> event = new HashMap<String,Object>();
		        event.put(AFInAppEventParameterName.PRICE,9.99);
		        event.put(AFInAppEventParameterName.CONTENT_TYPE,"TopUpYourPhone");		        
		        event.put(AFInAppEventParameterName.CURRENCY,"GBP");
		        event.put(AFInAppEventParameterName.QUANTITY,1);

		        AppsFlyerLib.trackEvent(TopUpYourPhone.this, AFInAppEventType.ADD_PAYMENT_INFO,event);
				
				Intent payNowIntent=new Intent(TopUpYourPhone.this,OrderDetails.class);
				payNowIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
				startActivity(payNowIntent);
			}
		}
*/
    }

    //HIDING AND SHOWING CHOOSE OPERATOR LAYOUT VIEWS
    public void showChooseOperatorLayout() {
        labelOperator.setVisibility(View.VISIBLE);
        operatorSpinner.setVisibility(View.VISIBLE);
    }

    public void hideChooseOperatorLayout() {
        labelOperator.setVisibility(View.GONE);
        operatorSpinner.setVisibility(View.GONE);
    }

    //HIDING AND SHOWING CHOOSE YOUR TOP UP LAYOUT VIEWS
    public void showChooseTopUpLayout(String numberOfProduct) {
        topUpProductLayout.setVisibility(View.VISIBLE);
        // ENABLING OR SHOWING "PRODUCT DESCRIPTION LINK"
//        productDescriptionLink.setVisibility(View.VISIBLE);

        if (Integer.parseInt(numberOfProduct) > 6) {
//            sixGreenBtnLayout.setVisibility(View.VISIBLE);
            sixGreenBtnLayout.setVisibility(View.GONE);
            // COMMENTING AS PER NEW REQ
            topupSpinner.setVisibility(View.VISIBLE);
        } else {
            sixGreenBtnLayout.setVisibility(View.VISIBLE);
            // COMMENTING AS PER NEW REQ
            topupSpinner.setVisibility(View.GONE);
        }

    }

    public void hideChooseTopUpLayout() {
        topUpProductLayout.setVisibility(View.GONE);
        sixGreenBtnLayout.setVisibility(View.GONE);
//        productDescriptionLink.setVisibility(View.GONE);

        // COMMENTING AS PER NEW REQ
        topupSpinner.setVisibility(View.GONE);
    }

    //	HIDING AND SHOWING CHOOSED OR SELECTED TOP UP LAYOUT VIEWS // Removing
    public void hideSelectedTopUP() {
//		verifiedPaymentBanners.setVisibility(View.GONE);

        // COMMENTING AS PER NEW REQUIREMENT
//		paynowButton.setVisibility(View.GONE);
//		selectedTopUpLayout.setVisibility(View.GONE);
    }

    public void hideAllLayouts() {
        labelOperator.setVisibility(View.GONE);
        operatorSpinner.setVisibility(View.GONE);
        topUpProductLayout.setVisibility(View.GONE);
        sixGreenBtnLayout.setVisibility(View.GONE);
//        productDescriptionLink.setVisibility(View.GONE);

        // COMMENTING AS PER NEW REQUIREMENT
//		verifiedPaymentBanners.setVisibility(View.GONE);
//		paynowButton.setVisibility(View.GONE);
        topupSpinner.setVisibility(View.GONE);
//		selectedTopUpLayout.setVisibility(View.GONE);
    }

    // SETTING DEFAULT VALUES OF SIX GREEN BUTTONS
    public void setDefaultValues() {
        greenBtnLayout1.setVisibility(View.VISIBLE);
        greenBtnLayout2.setVisibility(View.VISIBLE);
        greenBtnLayout3.setVisibility(View.VISIBLE);
        greenBtnLayout4.setVisibility(View.VISIBLE);
        greenBtnLayout5.setVisibility(View.VISIBLE);
        greenBtnLayout6.setVisibility(View.VISIBLE);
        secondHalfSixBtnLayout.setVisibility(View.VISIBLE);

        LinearLayout layout = (LinearLayout) findViewById(R.id.customTopUpFirstLayout);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
        params.setMargins(0, 0, 0, 0);
        layout.setLayoutParams(params);

        greenButton1.setBackgroundResource(R.drawable.topup_button);
        greenButton1.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton2.setBackgroundResource(R.drawable.topup_button);
        greenButton2.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton3.setBackgroundResource(R.drawable.topup_button);
        greenButton3.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton4.setBackgroundResource(R.drawable.topup_button);
        greenButton4.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton5.setBackgroundResource(R.drawable.topup_button);
        greenButton5.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton6.setBackgroundResource(R.drawable.topup_button);
        greenButton6.setTextColor(getResources().getColor(R.color.whiteColor));
    }

    //SETTING MAXIMUM LENGTH FOR EDIT TEXT
    public void setMaxLengthToET(EditText editText, int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        editText.setFilters(FilterArray);
    }

    //FORM MSIDN JSON PARAMETERS
    public String formMSIDNJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPMSIDNVALIDITYAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("SUBSCRIBERNUM", mobileNumber.getText().toString());
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("MAXLENGTH", String.valueOf(mobileNoMaxLength));
            json.put("COUNTRYCODE", "UNITEDKINGDOM");
            json.put("AGCOUNTRYCODE", ag_CountryCode);
            json.put("SERVICEID", "1");
            json.put("PUSH_TOKEN", preference.getString("URBAN_AIRSHIP_CHANNEL_ID", ""));

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    //FORM PLAN PRODUCT JSON PARAMETERS
    public String formTopUpProductJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPPRODUCTDETAIL");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            json.put("SUBSCRIBERNUM", mobileNumber.getText().toString());
            json.put("OPERATORCODE", selectedOperatorFromSpinner);
            json.put("COUNTRYCODE", "UNITEDKINGDOM");
            json.put("SERVICEID", "1");

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    //FORM PAY NOW JSON PARAMETERS // removing comment
    @Override
    public void onRequestComplete(String loadedString) {
        // TODO Auto-generated method stub
//        Log.e("Response", "My Response afterRequest" + loadedString);

        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 1) {
                    if (parseMyMSIDNData(loadedString)) {
                        try {
                            operatorsNameArray = new String[operatorsArrayList.size() + 1];
                            operatorsNameArray[0] = "Select";

                            for (int i = 0; i < operatorsArrayList.size(); i++) {
                                operatorsNameArray[i + 1] = operatorsArrayList.get(i).get(ProjectConstant.API_OPNAME);
                            }

                            showChooseOperatorLayout();

                            adapter_operator = new ArrayAdapter<>(this, R.layout.spinner_textsize, operatorsNameArray);
                            adapter_operator.setDropDownViewResource(R.layout.single_row_spinner);
                            operatorSpinner.setAdapter(adapter_operator);

                        } catch (Exception e) {

                        }
                    }
                } else if (apiHitPosition == 2) {
                    if (parseMyTopUpData(loadedString)) {
                        try {
                            showChooseTopUpLayout(noOfProducts);

                            String imageUrl = "https://kwikpay.co.uk/kwikpay/kwikpay/Image/appimages/" + selectedOperatorFromSpinner + "APP.png";
                            new ShowOperatorImageAsync().execute(imageUrl);

                            if (Integer.parseInt(noOfProducts) > 6) {

                                // COMMENTING AS PER NEW REQ
//                                handleSixGreenButton(6);

                                topUpPlansArray = new String[topUpPlansArrayList.size() + 1];
                                topUpPlansArray[0] = "Select";

                                for (int i = 0; i < topUpPlansArrayList.size(); i++) {
                                    topUpPlansArray[i + 1] = Html.fromHtml(topUpPlansArrayList.get(i).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " "
                                            + topUpPlansArrayList.get(i).get(ProjectConstant.TOPUP_API_DISPLAYAMT) + " - "
                                            + Html.fromHtml(topUpPlansArrayList.get(i).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " "
                                            + topUpPlansArrayList.get(i).get(ProjectConstant.TOPUP_API_PAYMENTAMT);
                                }

                                adapter_topup = new ArrayAdapter<>(this, R.layout.spinner_textsize, topUpPlansArray);
                                adapter_topup.setDropDownViewResource(R.layout.single_row_spinner);
                                topupSpinner.setAdapter(adapter_topup);

                            } else {
                                //Logic for Handling six buttons
                                handleSixGreenButton(Integer.parseInt(noOfProducts));
                            }
                        } catch (Exception e) {
                        }
                    }
                } else if (apiHitPosition == 3) {
                    if (parseRecentTransactionsData(loadedString)) {
                        if (myTransitionArrayList != null && myTransitionArrayList.size() > 0)
                            recentTransactionLayout.setVisibility(View.VISIBLE);
                        RecyclerView recentTxtRV = (RecyclerView) findViewById(R.id.recentTransactionsRV);
                        recentTxtRV.setLayoutManager(new LinearLayoutManager(TopUpYourPhone.this));
                        RecentTransactionsAdapter recentAdapter = new RecentTransactionsAdapter(TopUpYourPhone.this, recentTxnModelList, this);
                        recentTxtRV.setAdapter(recentAdapter);
                    }
                } else if (apiHitPosition == 4) {
                    if (parseRepeatRechargeDetail(loadedString)) {
                        Intent payNowIntent = new Intent(TopUpYourPhone.this, OrderDetails.class);
                        payNowIntent.putExtra("PAYNOW_DETAILS_VIA_RR_HASHMAP", repeatRechargeHashMap);
                        startActivity(payNowIntent);
                    }
                }

            } else {
                if (DeviceNetConnectionDetector.checkDataConnWifiMobile(TopUpYourPhone.this))
                    ARCustomToast.showToast(TopUpYourPhone.this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
                else
                    ARCustomToast.showToast(TopUpYourPhone.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }

        } else {
            if (DeviceNetConnectionDetector.checkDataConnWifiMobile(TopUpYourPhone.this))
                ARCustomToast.showToast(TopUpYourPhone.this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
            else
                ARCustomToast.showToast(TopUpYourPhone.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    // HANDLING SIX GREEN BUTTON AND THEIR VISIBILITY
    public void handleSixGreenButton(int productsNumber) {
        if (productsNumber <= 3) {
            LinearLayout layout = (LinearLayout) findViewById(R.id.customTopUpFirstLayout);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
            params.setMargins(0, 0, 0, 50);
            layout.setLayoutParams(params);
        } else {
            LinearLayout layout = (LinearLayout) findViewById(R.id.customTopUpSecondLayout);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
            params.setMargins(0, 15, 0, 50);
            layout.setLayoutParams(params);
        }

        switch (productsNumber) {
            case 1:
                greenBtnLayout2.setVisibility(View.INVISIBLE);
                greenBtnLayout3.setVisibility(View.INVISIBLE);

                secondHalfSixBtnLayout.setVisibility(View.GONE);

                greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                break;

            case 2:
                greenBtnLayout3.setVisibility(View.INVISIBLE);

                secondHalfSixBtnLayout.setVisibility(View.GONE);

                greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                break;
            case 3:

                secondHalfSixBtnLayout.setVisibility(View.GONE);

                greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTAMT));


                break;
            case 4:

                greenBtnLayout5.setVisibility(View.INVISIBLE);
                greenBtnLayout6.setVisibility(View.INVISIBLE);

                greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                break;

            case 5:

                greenBtnLayout6.setVisibility(View.INVISIBLE);

                greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton5.setText(Html.fromHtml(topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView5.setText(Html.fromHtml(topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                break;

            case 6:

                greenButton1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView1.setText(Html.fromHtml(topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(0).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView2.setText(Html.fromHtml(topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(1).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView3.setText(Html.fromHtml(topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(2).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView4.setText(Html.fromHtml(topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(3).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton5.setText(Html.fromHtml(topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView5.setText(Html.fromHtml(topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(4).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                greenButton6.setText(Html.fromHtml(topUpPlansArrayList.get(5).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(5).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
                greenTextView6.setText(Html.fromHtml(topUpPlansArrayList.get(5).get(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)) + " " +
                        topUpPlansArrayList.get(5).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

                break;
        }
    }

    public void sixGreenButtonClick(View sixBtnView) {
        greenButton1.setBackgroundResource(R.drawable.topup_button);
        greenButton1.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton2.setBackgroundResource(R.drawable.topup_button);
        greenButton2.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton3.setBackgroundResource(R.drawable.topup_button);
        greenButton3.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton4.setBackgroundResource(R.drawable.topup_button);
        greenButton4.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton5.setBackgroundResource(R.drawable.topup_button);
        greenButton5.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton6.setBackgroundResource(R.drawable.topup_button);
        greenButton6.setTextColor(getResources().getColor(R.color.whiteColor));

        if (Integer.parseInt(noOfProducts) <= 3) {
            LinearLayout secondGreenBtnLayout = (LinearLayout) findViewById(R.id.customTopUpFirstLayout);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) secondGreenBtnLayout.getLayoutParams();
            params.setMargins(0, 0, 0, 0);
            secondGreenBtnLayout.setLayoutParams(params);
        } else {
            LinearLayout secondGreenBtnLayout = (LinearLayout) findViewById(R.id.customTopUpSecondLayout);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) secondGreenBtnLayout.getLayoutParams();
            params.setMargins(0, 15, 0, 0);
            secondGreenBtnLayout.setLayoutParams(params);
        }

        switch (sixBtnView.getId()) {
            case R.id.topupAboveValue1:
//			showSelectedTopUP(greenButton1.getText().toString(),greenTextView1.getText().toString(),0);  // COMMENTING AS PER NEW REQ

                greenButton1.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton1.setTextColor(getResources().getColor(R.color.darkGreen));

                createHashMapForOrderDetail(0);

                break;
            case R.id.topupAboveValue2:
//			showSelectedTopUP(greenButton2.getText().toString(),greenTextView2.getText().toString(),1); // COMMENTING AS PER NEW REQ

                greenButton2.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton2.setTextColor(getResources().getColor(R.color.darkGreen));

                createHashMapForOrderDetail(1);

                break;
            case R.id.topupAboveValue3:
//			showSelectedTopUP(greenButton3.getText().toString(),greenTextView3.getText().toString(),2); // COMMENTING AS PER NEW REQ

                greenButton3.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton3.setTextColor(getResources().getColor(R.color.darkGreen));

                createHashMapForOrderDetail(2);


                break;
            case R.id.topupAboveValue4:
//			showSelectedTopUP(greenButton4.getText().toString(),greenTextView4.getText().toString(),3); // COMMENTING AS PER NEW REQ

                greenButton4.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton4.setTextColor(getResources().getColor(R.color.darkGreen));

                createHashMapForOrderDetail(3);


                break;
            case R.id.topupAboveValue5:
//			showSelectedTopUP(greenButton5.getText().toString(),greenTextView5.getText().toString(),4); // COMMENTING AS PER NEW REQ

                greenButton5.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton5.setTextColor(getResources().getColor(R.color.darkGreen));

                createHashMapForOrderDetail(4);


                break;
            case R.id.topupAboveValue6:
//			showSelectedTopUP(greenButton6.getText().toString(),greenTextView6.getText().toString(),5); // COMMENTING AS PER NEW REQ

                greenButton6.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton6.setTextColor(getResources().getColor(R.color.darkGreen));

                createHashMapForOrderDetail(5);


                break;

            default:
                break;
        }
    }

    // LOGIC FOR PARSING MSIDN DATA FROM WEBSERVICE
    public boolean parseMyMSIDNData(String msidResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(msidResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(TopUpYourPhone.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has(ProjectConstant.API_OPERATORS)) {
                        operatorsArrayList = new ArrayList<>();

                        Object object = parentResponseObject.get(ProjectConstant.API_OPERATORS);
                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            operatorsJsonArray = new JSONArray();
                            operatorsJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray) {
                            operatorsJsonArray = (JSONArray) object;
                        }

                        for (int i = 0; i < operatorsJsonArray.length(); i++) {
                            childResponseObject = operatorsJsonArray.getJSONObject(i);

                            if (!childResponseObject.isNull(ProjectConstant.API_OPNAME)
                                    && !childResponseObject.isNull(ProjectConstant.API_OPTEMPACTIVEFLAG)
                                    && !childResponseObject.isNull(ProjectConstant.API_SECONDLEVELVALIDATIONE)
                                    && !childResponseObject.isNull(ProjectConstant.API_OPID)
                                    && !childResponseObject.isNull(ProjectConstant.API_OPIMGPATH)
                                    && !childResponseObject.isNull(ProjectConstant.API_OPCODE)
                                    && !childResponseObject.isNull(ProjectConstant.API_OPACTIVEFLAG)
                                    && !childResponseObject.isNull(ProjectConstant.API_OPCIRCLEACTIVEFLAG)) {
                                operatorsHashMap = new HashMap<>();
                                operatorsHashMap.put(ProjectConstant.API_OPNAME, childResponseObject.getString(ProjectConstant.API_OPNAME));
                                operatorsHashMap.put(ProjectConstant.API_OPTEMPACTIVEFLAG, childResponseObject.getString(ProjectConstant.API_OPTEMPACTIVEFLAG));
                                operatorsHashMap.put(ProjectConstant.API_SECONDLEVELVALIDATIONE, childResponseObject.getString(ProjectConstant.API_SECONDLEVELVALIDATIONE));
                                operatorsHashMap.put(ProjectConstant.API_OPID, childResponseObject.getString(ProjectConstant.API_OPID));
                                operatorsHashMap.put(ProjectConstant.API_OPIMGPATH, childResponseObject.getString(ProjectConstant.API_OPIMGPATH));
                                operatorsHashMap.put(ProjectConstant.API_OPCODE, childResponseObject.getString(ProjectConstant.API_OPCODE));
                                operatorsHashMap.put(ProjectConstant.API_OPACTIVEFLAG, childResponseObject.getString(ProjectConstant.API_OPACTIVEFLAG));
                                operatorsHashMap.put(ProjectConstant.API_OPCIRCLEACTIVEFLAG, childResponseObject.getString(ProjectConstant.API_OPCIRCLEACTIVEFLAG));

                                operatorsArrayList.add(operatorsHashMap);
                            }

                        }

                    }
                }

                if (parentResponseObject.has("SUBSNUMBER")) {
                    subscriberMobileNumber = parentResponseObject.getString("SUBSNUMBER");
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(TopUpYourPhone.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // LOGIC FOR PARSING TOPUP PLANS FROM WEBSERVICE
    public boolean parseMyTopUpData(String operatorResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(operatorResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(TopUpYourPhone.this, "No Reponse Tag", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    productLogoURL = parentResponseObject.optString("IMG_URL");

                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_PRODUCTS)) {
                        JSONArray topupJsonArray = parentResponseObject.getJSONArray(ProjectConstant.TOPUP_API_PRODUCTS);
                        topUpPlansArrayList = new ArrayList<>();

                        for (int i = 0; i < topupJsonArray.length(); i++) {
                            childResponseObject = topupJsonArray.getJSONObject(i);

                            if (!childResponseObject.isNull(ProjectConstant.TOPUP_API_DISPLAYAMT)
                                    && !childResponseObject.isNull(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID)
                                    && !childResponseObject.isNull(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH)
                                    && !childResponseObject.isNull(ProjectConstant.TOPUP_API_RECHARGEAMT)
                                    && !childResponseObject.isNull(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH)
                                    && !childResponseObject.isNull(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID)
                                    && !childResponseObject.isNull(ProjectConstant.TOPUP_API_PAYMENTAMT)
                                    && !childResponseObject.isNull(ProjectConstant.TOPUP_API_PAYMENTCURRENCYAGID)
                                    && !childResponseObject.isNull(ProjectConstant.TOPUP_API_RECHARGECURRENCYIMGPATH)) {
                                topUpHashMap = new HashMap<>();
                                topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYAMT, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYAMT));
                                topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));

                                topUpHashMap.put(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH));
                                topUpHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH));

                                // ADDING THESE TWO EXTRA AS IT CONTAINING AT SO MANY PLACES IN CLASS [KEEPING IT]
                                topUpHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH, childResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYIMGPATH));
                                topUpHashMap.put(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYIMGPATH));

                                topUpHashMap.put(ProjectConstant.PAYNOW_RECHARGEAMOUNT, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGEAMT));
                                topUpHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
                                topUpHashMap.put(ProjectConstant.TOPUP_API_PAYMENTAMT, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTAMT));
                                topUpHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, childResponseObject.getString(ProjectConstant.TOPUP_API_PAYMENTCURRENCYAGID));
                                topUpHashMap.put(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE, childResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYIMGPATH));

                                topUpHashMap.put(ProjectConstant.PAYNOW_OPERATORNAME, childResponseObject.getString(ProjectConstant.PAYNOW_OPERATORNAME));
                                topUpHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, childResponseObject.getString("PAYMENTPROCESSINGFEE"));
                                topUpHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, childResponseObject.getString("PAYMENTSERVICEFEE"));
                                topUpHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, childResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION));
                                topUpHashMap.put("OPERATORPRODUCTCODE", childResponseObject.optString("OPERATORPRODUCTCODE"));

                                topUpPlansArrayList.add(topUpHashMap);

                            }
                        }
                    }

                    if (parentResponseObject.has("URLTERMS")) {
                        ProjectConstant.tncURL = parentResponseObject.getString("URLTERMS");
                    }
                    if (parentResponseObject.has("URLVIEWPRODUCTDETAIL")) {
                        viewProductDetailURL = parentResponseObject.getString("URLVIEWPRODUCTDETAIL");
                    }
                    if (parentResponseObject.has("URLPROVIDERHELP")) {
                        ProjectConstant.providerDetailsURL = parentResponseObject.getString("URLPROVIDERHELP");
                    }
                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_NO_OF_PRODUCTS)) {
                        noOfProducts = parentResponseObject.getString(ProjectConstant.TOPUP_API_NO_OF_PRODUCTS);
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(TopUpYourPhone.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }

            }

            if (parentResponseObject.has("CARTMSG")) {
                cartDeleteMsg = parentResponseObject.getString("CARTMSG");
                ProjectConstant.CART_INFO_MESSAGE = parentResponseObject.getString("CARTMSG");
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

        return true;

    }

    //PARSING REPEAT RECHARGE DETAILS DATA
    public boolean parseRepeatRechargeDetail(String repeatRCResponse) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(repeatRCResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    repeatRechargeHashMap = new HashMap<>();

                    if (parentResponseObject.has("DISPLAYAMT"))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, parentResponseObject.getString("DISPLAYAMT").toString());
//                    else
//                        repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, selectedDisplayAmount);

                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID).toString());
                    if (parentResponseObject.has("DISPLAYAMOUNTCURRENCYCODE"))
                        repeatRechargeHashMap.put("DISPLAYAMOUNTCURRENCYCODE", parentResponseObject.getString("DISPLAYAMOUNTCURRENCYCODE").toString());
                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGEAMT))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGEAMT, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGEAMT).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_SERVICEID))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICEID, parentResponseObject.getString(ProjectConstant.TXN_SERVICEID).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_OPERATORNAME))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_OPERATORNAME, parentResponseObject.getString(ProjectConstant.TXN_OPERATORNAME).toString());
                    if (parentResponseObject.has("EMAILID"))
                        repeatRechargeHashMap.put("EMAILID", parentResponseObject.getString("EMAILID").toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PROCESSINGFEE))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_PROCESSINGFEE).toString());
                    if (parentResponseObject.has("PGAMT"))
                        repeatRechargeHashMap.put("PGAMT", parentResponseObject.getString("PGAMT").toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_PAYMENTCURRENCYCODE))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, parentResponseObject.getString(ProjectConstant.TXN_PAYMENTCURRENCYCODE).toString());

                    if (parentResponseObject.has(ProjectConstant.TXN_SERVICENAME))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICENAME, parentResponseObject.getString(ProjectConstant.TXN_SERVICENAME).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_COUNTRYCODE))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_COUNTRYCODE, parentResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE).toString());
                    if (parentResponseObject.has("MOBILENUMBER"))
                        repeatRechargeHashMap.put("MOBILENUMBER", parentResponseObject.getString("MOBILENUMBER").toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_SERVICEFEE))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_SERVICEFEE).toString());
                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID).toString());
                    if (parentResponseObject.has(ProjectConstant.API_OPCODE))
                        repeatRechargeHashMap.put(ProjectConstant.API_OPCODE, parentResponseObject.getString(ProjectConstant.API_OPCODE).toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PLANDESCRIPTION))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, parentResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION).toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, parentResponseObject.getString(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID).toString());

                    if (parentResponseObject.has("URLTERMS"))
                        ProjectConstant.tncURL = parentResponseObject.getString("URLTERMS");
                    if (parentResponseObject.has("URLVIEWPRODUCTDETAIL"))
                        viewProductDetailURL = parentResponseObject.getString("URLVIEWPRODUCTDETAIL");
                    if (parentResponseObject.has("URLPROVIDERHELP"))
                        ProjectConstant.providerDetailsURL = parentResponseObject.getString("URLPROVIDERHELP");

                    if (parentResponseObject.getString(ProjectConstant.TXN_SERVICEID).equals("3")) {
                        if (parentResponseObject.has("PRODUCT_IMG"))
                            repeatRechargeHashMap.put(ProjectConstant.API_OPIMGPATH, parentResponseObject.getString("PRODUCT_IMG").toString());
                    } else {
                        if (parentResponseObject.has("IMG_URL"))
                            repeatRechargeHashMap.put(ProjectConstant.API_OPIMGPATH, parentResponseObject.getString("IMG_URL").toString());
                    }

                    repeatRechargeHashMap.put(ProjectConstant.API_OPID, "");
                    repeatRechargeHashMap.put("AGID", "");
                    repeatRechargeHashMap.put("OPERATORPRODUCTCODE", parentResponseObject.optString("OPERATORPRODUCTCODE"));

                    repeatRechargeHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, selectedRechargeAmount);
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(TopUpYourPhone.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean parseRecentTransactionsData(String recentTransactions) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(recentTransactions);
            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
                return false;

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);
            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has(ProjectConstant.TXN_TRANSACTIONDETAIL)) {
                        HashMap<String, String> myTransitionHashMap;
                        myTransitionArrayList = new ArrayList<>();
                        recentTxnModelList = new ArrayList<>();
                        Object object = parentResponseObject.get(ProjectConstant.TXN_TRANSACTIONDETAIL);

                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            txnJsonArray = new JSONArray();
                            txnJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray)
                            txnJsonArray = (JSONArray) object;

                        /*int recentTxnLength;
                        if (txnJsonArray.length() > 3)
                            recentTxnLength = 3;
                        else
                            recentTxnLength = txnJsonArray.length();*/

                        for (int i = 0; i < txnJsonArray.length(); i++) {

                            childResponseObject = txnJsonArray.getJSONObject(i);

                            if (childResponseObject.getString(ProjectConstant.TXN_SERVICEID).equals("1")) {
                                myTransitionHashMap = new HashMap<>();
                                myTransitionHashMap.put(ProjectConstant.TXN_TRANSACTIONDATETIME, childResponseObject.getString(ProjectConstant.TXN_TRANSACTIONDATETIME));
                                myTransitionHashMap.put(ProjectConstant.TXN_SERVICEID, childResponseObject.getString(ProjectConstant.TXN_SERVICEID));
                                myTransitionHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_PAYMENTCURRENCYCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_RECHARGECURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_RECHARGECURRENCYCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_OPERATORNAME, childResponseObject.getString(ProjectConstant.TXN_OPERATORNAME));
                                myTransitionHashMap.put(ProjectConstant.TXN_PAYMENTAMOUNT, childResponseObject.getString(ProjectConstant.TXN_PAYMENTAMOUNT));
                                myTransitionHashMap.put(ProjectConstant.TXN_PAYMENTSTATUS, childResponseObject.getString(ProjectConstant.TXN_PAYMENTSTATUS));
                                myTransitionHashMap.put(ProjectConstant.TXN_SERVICENAME, childResponseObject.getString(ProjectConstant.TXN_SERVICENAME));
                                myTransitionHashMap.put(ProjectConstant.TXN_COUNTRYCODE, childResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, childResponseObject.getString(ProjectConstant.TXN_DISPLAYAMOUNT));
                                myTransitionHashMap.put(ProjectConstant.TXN_ORDERID, childResponseObject.getString(ProjectConstant.TXN_ORDERID));
                                myTransitionHashMap.put(ProjectConstant.TXN_OPERATORCODE, childResponseObject.getString(ProjectConstant.TXN_OPERATORCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_DISPLAYCURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_DISPLAYCURRENCYCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_RECHARGESTATUS, childResponseObject.getString(ProjectConstant.TXN_RECHARGESTATUS));
                                myTransitionHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, childResponseObject.getString(ProjectConstant.TXN_RECHARGEAMOUNT));
                                myTransitionHashMap.put(ProjectConstant.TXN_SUBSCRIPTIONNUMBER, childResponseObject.getString(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));

                                if (childResponseObject.has("RECHARGEPIN"))
                                    myTransitionHashMap.put("RECHARGEPIN", childResponseObject.getString("RECHARGEPIN").toString());
                                if (childResponseObject.has("RECHARGEPINEXPIRYDATE"))
                                    myTransitionHashMap.put("RECHARGEPINEXPIRYDATE", childResponseObject.getString("RECHARGEPINEXPIRYDATE").toString());
                                if (childResponseObject.has("CPNSRNUM"))
                                    myTransitionHashMap.put("CPNSRNUM", childResponseObject.getString("CPNSRNUM").toString());
                                if (childResponseObject.has("CPNVALUE"))
                                    myTransitionHashMap.put("CPNVALUE", childResponseObject.getString("CPNVALUE").toString());

                                myTransitionArrayList.add(myTransitionHashMap);

                                RecentTransactionsModel model = new RecentTransactionsModel();
                                model.setMobileNumber(childResponseObject.optString(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));
                                model.setDisplayCurrencyCode(childResponseObject.optString(ProjectConstant.TXN_DISPLAYCURRENCYCODE));
                                model.setTopUpAmount(childResponseObject.optString(ProjectConstant.TXN_DISPLAYAMOUNT));
                                model.setOperatorName(childResponseObject.optString(ProjectConstant.TXN_OPERATORNAME));

                                recentTxnModelList.add(model);
                            }
                        }
                    } else
                        return false;

                } else {
                    return false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //************************************************************************************************************//
    // AS PER NEW REQUIREMENT, BELOW METHOD IS RESPONSIBLE FOR TAKING USER TO ORDER SCREEN AFTER SELECTING TOPUP  //
    // EARLIER THIS FUNCTIONALITY IS BUILT IN PROCEED BUTTON (IMP METHOD) 										  //
    //************************************************************************************************************//
    public void createHashMapForOrderDetail(int position) {
        try {
            payNowHashMap = new HashMap<>();
            payNowHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PAYMENTCURRENCYCODE));
            payNowHashMap.put(ProjectConstant.PAYNOW_OPERATORNAME, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_OPERATORNAME));
            payNowHashMap.put(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGECURRENCYCODE));
            payNowHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PROCESSINGFEE));
            payNowHashMap.put(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_DISPLAYCURRENCYCODE));
            payNowHashMap.put(ProjectConstant.PAYNOW_RECHARGEAMOUNT, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGEAMOUNT));
            payNowHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_SERVICEFEE));
            payNowHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PLANDESCRIPTION));
            payNowHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID));

            payNowHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID));
            payNowHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID));
            payNowHashMap.put(ProjectConstant.TOPUP_API_DISPLAYAMT, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYAMT));
            payNowHashMap.put(ProjectConstant.TOPUP_API_PAYMENTAMT, topUpPlansArrayList.get(position).get(ProjectConstant.TOPUP_API_PAYMENTAMT));

            payNowHashMap.put(ProjectConstant.API_OPIMGPATH, productLogoURL);

            payNowHashMap.put(ProjectConstant.PAYNOW_MOBILENUMBER, subscriberMobileNumber);
            payNowHashMap.put(ProjectConstant.API_OPCODE, selectedOperatorFromSpinner);
            payNowHashMap.put("SERVICEID", "1");
            payNowHashMap.put(ProjectConstant.TXN_COUNTRYCODE, "UNITEDKINGDOM");
            payNowHashMap.put(ProjectConstant.API_OPID, selectedOperatorIdFromSpinner);
            payNowHashMap.put("AGID", "44");
            payNowHashMap.put("CARTMSG", cartDeleteMsg);
            payNowHashMap.put("OPERATORPRODUCTCODE", topUpPlansArrayList.get(position).get("OPERATORPRODUCTCODE"));


            //CHECKING WHETHER USER ALREADY LOGED IN OR NOT
            if (preference.getString("KP_USER_ID", "").equals("0")) {
                Intent logInIntent = new Intent(TopUpYourPhone.this, LoginScreen.class);
                logInIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
                startActivity(logInIntent);
            } else {
                // Create the Values
                Map<String, Object> event = new HashMap<>();
                event.put(AFInAppEventParameterName.PRICE, topUpPlansArrayList.get(position).get(ProjectConstant.PAYNOW_RECHARGEAMOUNT));
                event.put(AFInAppEventParameterName.CONTENT_TYPE, "TopUpYourPhone");
                event.put(AFInAppEventParameterName.CURRENCY, "GBP");
                event.put(AFInAppEventParameterName.QUANTITY, 1);

                AppsFlyerLib.trackEvent(TopUpYourPhone.this, AFInAppEventType.ADD_PAYMENT_INFO, event);
                AppsFlyerLib.trackEvent(TopUpYourPhone.this, "TopUp", null);

                Intent payNowIntent = new Intent(TopUpYourPhone.this, OrderDetails.class);
                payNowIntent.putExtra("PAYNOW_DETAILS_HASHMAP", payNowHashMap);
                startActivity(payNowIntent);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void recentTxnSelected(int position) {
        apiHitPosition = 4;
        selectedRechargeAmount = myTransitionArrayList.get(position).get(ProjectConstant.TXN_RECHARGEAMOUNT);
        new LoadResponseViaPost(TopUpYourPhone.this, formRepeatRechargeJSON(myTransitionArrayList.get(position).get(ProjectConstant.TXN_ORDERID)), true).execute("");
    }

    public String formRepeatRechargeJSON(String selectedOrderId) {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPREPEATRECHARGE");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("ORDERID", selectedOrderId);
            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    private class ShowOperatorImageAsync extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urlString) {
            // TODO Auto-generated method stub
            try {
                URL url = new URL(urlString[0]);

                HttpURLConnection imageConnection = (HttpURLConnection) url.openConnection();
                imageConnection.setConnectTimeout(5000);
                imageConnection.setReadTimeout(5000);
                imageConnection.setInstanceFollowRedirects(true);
                operatorImageBitmap = BitmapFactory.decodeStream(imageConnection.getInputStream());

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                operatorImageBitmap = null;
            }
            return operatorImageBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // TODO Auto-generated method stub

            // COMMENTING AS PER NEW REQ
            if (result != null)
                chooseProductOnFlyIV.setImageBitmap(result);
            else
                chooseProductOnFlyIV.setImageDrawable(getResources().getDrawable(R.drawable.default_operator));
        }
    }

}