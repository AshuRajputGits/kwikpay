package com.uk.recharge.kwikpay.kwikcharge.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.models.ConnectorListMO;
import com.uk.recharge.kwikpay.kwikcharge.models.MapChargerLocationsMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 02-02-2018.
 */

public class MapInfoWindow implements GoogleMap.InfoWindowAdapter {

    private final View mymarkerview;
    private ArrayList<MapChargerLocationsMO> mapChargerMOArrayList = null;

    public MapInfoWindow(Context context, ArrayList<MapChargerLocationsMO> mapChargerMOArrayList) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        mymarkerview = layoutInflater.inflate(R.layout.map_info_window, null);
        this.mapChargerMOArrayList = mapChargerMOArrayList;
    }

    public View getInfoWindow(Marker marker) {
        render(marker, mymarkerview);
        return mymarkerview;
    }

    public View getInfoContents(Marker marker) {
        return null;
    }

    private void render(Marker marker, View view) {
        try {
            int position = (int) marker.getTag();

            if (position != -1) {

                if (mapChargerMOArrayList != null) {

                    TextView address = view.findViewById(R.id.markerAddress);
                    TextView mapType2Charger = view.findViewById(R.id.mapType2Charger);
                    TextView mapCCSCharger = view.findViewById(R.id.mapCCSCharger);
                    TextView mapChademoCharger = view.findViewById(R.id.mapChademoCharger);

                    address.setText(Utility.fromHtml("<b><font color='#262626'>" + mapChargerMOArrayList.get(position).getLocationName() + "</b> <br />" + mapChargerMOArrayList.get(position).getLocationAddress()));

                    ArrayList<ConnectorListMO> connectorListMOS = mapChargerMOArrayList.get(position).getConnectorListMOArrayList();

                    if (connectorListMOS.get(0).getConnectorType().contains("62196-2")) {
                        if (connectorListMOS.get(0).getConnectorStatus().equalsIgnoreCase("Available"))
                            mapType2Charger.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.green_dot, 0);
                        else
                            mapType2Charger.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.red_dot, 0);
                    }
                    if (connectorListMOS.get(1).getConnectorType().contains("ChaDeMo") || connectorListMOS.get(1).getConnectorType().contains("CCS")) {
                        if (connectorListMOS.get(1).getConnectorStatus().equalsIgnoreCase("Available")) {
                            mapCCSCharger.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.green_dot, 0);
                            mapChademoCharger.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.green_dot, 0);
                        } else {
                            mapCCSCharger.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.red_dot, 0);
                            mapChademoCharger.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.red_dot, 0);
                        }
                    } else {
                        mapCCSCharger.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.red_dot, 0);
                        mapChademoCharger.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.red_dot, 0);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
