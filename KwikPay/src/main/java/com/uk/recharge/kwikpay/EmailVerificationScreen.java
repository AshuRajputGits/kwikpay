package com.uk.recharge.kwikpay;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResultFromGetMethod;
import com.ar.library.asynctaskclasses.LoadResultFromGetMethod.AsyncTaskCompleteListenerForGetMethod;
import com.crashlytics.android.Crashlytics;

public class EmailVerificationScreen extends Activity implements AsyncTaskCompleteListenerForGetMethod {
    TextView emailVerificationMsg, resendEmailLink;
    private SharedPreferences preference;
    private int WHICH_API_HIT = 0;
    //*******************************************************************************//
    //IF WHICH_API_HIT=1: CALLING THE API TO CHECK THE STATUS OF EMAIL VERIFICATION  //
    //IF WHICH_API_HIT=2: CALLING THE API TO RESEND THE MAIL FOR VERIFICATION        //
    //*******************************************************************************//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.email_verification_screen);

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        emailVerificationMsg = findViewById(R.id.email_verification_infomsg);
        resendEmailLink = findViewById(R.id.email_verification_ResendLink);

		/*String emailMessage="We need to do a one time verification of your email  "+
                preference.getString("KP_USER_EMAIL_ID", "")+
		 ". We have sent a link on this email please click on the same to confirm. Once confirmed please select proceed for your fast & easy top up";
		emailVerificationMsg.setText(emailMessage);*/

        resendEmailLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WHICH_API_HIT = 2;

                // CALLING AND SENDING REQUEST API, FOR EMAIL VERIFICATION [ EMAIL ID]
                String emailVerificationURL = getResources().getString(R.string.email_verification_send_base_url) +
                        preference.getString("KP_USER_EMAIL_ID", "");
                new LoadResultFromGetMethod(EmailVerificationScreen.this).execute(emailVerificationURL);
            }
        });
    }

    public void emailVerificationProceedButton(View v) {
        WHICH_API_HIT = 1;
        // CALLING API, FOR CHECKING THE STATUS WHETHER USER VERIFIED THEIR EMAIL ADDRESS OR NOT [WITH USER ID]
        String checkEmailStatus = getResources().getString(R.string.email_verification_status_base_url) +
                preference.getString("KP_USER_EMAIL_ID", "");
        new LoadResultFromGetMethod(EmailVerificationScreen.this).execute(checkEmailStatus);
    }

    @Override
    public void onLoadComplete(String loadedString) {
//		Log.e("", "Received Result in MainClass - "+loadedString);

        try {
            if (!loadedString.equals("") && !loadedString.equals("Exception")) {
                if (WHICH_API_HIT == 1) {
                    WHICH_API_HIT = 0;
                    if (loadedString.contains("Not Found") || loadedString.contains("not found")) {
                        ARCustomToast.showToast(EmailVerificationScreen.this, "Please verify your email by clicking on link we've send you");
                    } else {
                        SharedPreferences.Editor emailEditor = preference.edit();
                        emailEditor.putBoolean("EMAIL_IS_VERIFIED", true);
                        emailEditor.putBoolean("IS_KP_USER_EMAIL_VERIFIED", true).apply();

                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("RESPONSE", true);
                        setResult(RESULT_OK, returnIntent);
                        EmailVerificationScreen.this.finish();
                    }
                } else if (WHICH_API_HIT == 2) {
                    ARCustomToast.showToast(EmailVerificationScreen.this, "We have sent an email for verification, please verify");
                }
            } else {
                if (DeviceNetConnectionDetector.checkDataConnWifiMobile(this))
                    ARCustomToast.showToast(this, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection));
            }

        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }
}
