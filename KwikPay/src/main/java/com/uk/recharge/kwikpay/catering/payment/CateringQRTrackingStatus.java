package com.uk.recharge.kwikpay.catering.payment;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spanned;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ar.library.ARCustomToast;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.catering.models.CateringCompleteOrdersMO;
import com.uk.recharge.kwikpay.catering.models.ProductSelectedDetailsMO;
import com.uk.recharge.kwikpay.catering.models.SubProductSelectedMO;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 11/9/2018.
 */

public class CateringQRTrackingStatus extends KPSuperClass {

    private CateringCompleteOrdersMO cateringCompleteOrdersMO = null;
    private LinearLayout catProductDynamicLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        turnHomeButtonToBackButton();
        setUpResourcesAndUpdateUI();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.catering_qr_tracking_status;
    }

    private void setUpResourcesAndUpdateUI() {
        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("SELECTED_ORDER_TXN"))
                cateringCompleteOrdersMO = (CateringCompleteOrdersMO) receivedBundle.getSerializable("SELECTED_ORDER_TXN");
        }
        catProductDynamicLayout = findViewById(R.id.cateringProductSubProdDetailsLayout);

        if (cateringCompleteOrdersMO != null) {
            createDynamicQRCode();
            updateOrderStatusTrackerUI();
            createAndUpdateDynamicProducts();

            //Calling below API to update the status of the order
            callCateringOrderedInfoAPI();
        }
    }

    private void createDynamicQRCode() {

        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(cateringCompleteOrdersMO.getCateringOrderId(),
                    BarcodeFormat.QR_CODE, 400, 400);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            ((ImageView) findViewById(R.id.img_result_qr)).setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private void updateOrderStatusTrackerUI() {

        if (cateringCompleteOrdersMO != null) {
            ImageView catOrderPlacedIV = findViewById(R.id.catOrderPlacedIV);
            ImageView catUnderPreparationIV = findViewById(R.id.catUnderPreparationIV);
            ImageView catReadyForCollectionIV = findViewById(R.id.catReadyForCollectionIV);

            if (cateringCompleteOrdersMO.getCateringAcceptedStatus().equals("1")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    catOrderPlacedIV.setImageDrawable(getResources().getDrawable(R.drawable.stepper_tick, getApplicationContext().getTheme()));
                else
                    catOrderPlacedIV.setImageDrawable(getResources().getDrawable(R.drawable.stepper_tick));
            }

            if (cateringCompleteOrdersMO.getCateringReadyStatus().equals("1")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    catUnderPreparationIV.setImageDrawable(getResources().getDrawable(R.drawable.stepper_tick, getApplicationContext().getTheme()));
                else
                    catUnderPreparationIV.setImageDrawable(getResources().getDrawable(R.drawable.stepper_tick));
            }

            if (cateringCompleteOrdersMO.getCateringDeliveryStatus().equals("1")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    catReadyForCollectionIV.setImageDrawable(getResources().getDrawable(R.drawable.stepper_tick, getApplicationContext().getTheme()));
                else
                    catReadyForCollectionIV.setImageDrawable(getResources().getDrawable(R.drawable.stepper_tick));
            }
        }
    }

    private void createAndUpdateDynamicProducts() {

        List<ProductSelectedDetailsMO> productSelectedDetailsMOList = cateringCompleteOrdersMO.getProductSelectedDetailsMOList();

        Spanned[] productAndSubProductArray = new Spanned[productSelectedDetailsMOList.size()];
//        String[] productQtyArray = new String[productSelectedDetailsMOList.size()];
        String[] productAmountArray = new String[productSelectedDetailsMOList.size()];

        for (int i = 0; i < productSelectedDetailsMOList.size(); i++) {

            List<SubProductSelectedMO> subProductSelectedMOList = productSelectedDetailsMOList.get(i).getSubProductSelectedMOList();
            if (subProductSelectedMOList != null && subProductSelectedMOList.size() > 0) {
                String subProductCollection = "";
                for (int j = 0; j < subProductSelectedMOList.size(); j++) {
                    if (j == 0)
                        subProductCollection = subProductSelectedMOList.get(j).getSubProdBrandName();
                    else
                        subProductCollection = subProductCollection + "," + subProductSelectedMOList.get(j).getSubProdBrandName();
                }
                String formatedSubProduct = productSelectedDetailsMOList.get(i).getBrandName() + "<br/><small>" + subProductCollection + "</small>";
                productAndSubProductArray[i] = Utility.fromHtml(formatedSubProduct);
            } else
                productAndSubProductArray[i] = Utility.fromHtml(productSelectedDetailsMOList.get(i).getBrandName());

            productAmountArray[i] = productSelectedDetailsMOList.get(i).getUnitPrice();
        }

        String currencySymbol = cateringCompleteOrdersMO.getCateringDisplayCurrencyCode();
        int indexLength = productSelectedDetailsMOList.size();

        TextView productNameAndDescription[] = new TextView[indexLength];
        TextView productQty[] = new TextView[indexLength];
        TextView productAmount[] = new TextView[indexLength];

        LinearLayout.LayoutParams productLayoutParam = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.8f);
        LinearLayout.LayoutParams productQtyLayoutParam = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.25f);
        LinearLayout.LayoutParams productAmountLayoutParam = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);

        for (int b = 0; b < productAndSubProductArray.length && b < productAmountArray.length; b++) {

            LinearLayout parent = new LinearLayout(this);
            parent.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams innerLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            innerLayout.setMargins(10, 10, 10, 30);
            parent.setLayoutParams(innerLayout);

            // CREATING PRODUCT NAME AND SUB PRODUCT TEXT VIEW
            productNameAndDescription[b] = new TextView(this);
            productNameAndDescription[b].setTextSize(13f);
            productNameAndDescription[b].setLayoutParams(productLayoutParam);
            productNameAndDescription[b].setText(productAndSubProductArray[b]);

            // CREATING PRODUCT QUANTITY TEXT VIEW
            productQty[b] = new TextView(this);
            productQty[b].setTextSize(13.5f);
            productQty[b].setGravity(Gravity.CENTER);
            productQty[b].setLayoutParams(productQtyLayoutParam);
            productQty[b].setText("1");

            // CREATING PRODUCT AMOUNT TEXT VIEW
            productAmount[b] = new TextView(this);
            productAmount[b].setTextSize(13.5f);
            productAmount[b].setGravity(Gravity.CENTER);
            productAmount[b].setLayoutParams(productAmountLayoutParam);
            productAmount[b].setText(Utility.fromHtml(currencySymbol) + productAmountArray[b]);

            parent.addView(productNameAndDescription[b]);
            parent.addView(productQty[b]);
            parent.addView(productAmount[b]);
            catProductDynamicLayout.addView(parent);
        }
    }

    private void callCateringOrderedInfoAPI() {

        if (cateringCompleteOrdersMO == null)
            return;

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "CATERING_USER_ORDERED_INFO");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("LOCATION_NUMBER", cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorLocationNumber());
            json.put("ORDER_ID", cateringCompleteOrdersMO.getCateringOrderId());
            json.put("DEVICEOS", "ANDROID");
            json.put("MERCHANT_ID", cateringCompleteOrdersMO.getProductDistributorDetails().getDistributorHeadId());

        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {
                                    if (cateringCompleteOrdersMO != null) {
                                        cateringCompleteOrdersMO.setCateringReadyStatus(childJson.optString("catering_ready_status"));
                                        cateringCompleteOrdersMO.setCateringAcceptedStatus(childJson.optString("catering_accepted_status"));
                                        cateringCompleteOrdersMO.setCateringDeliveryStatus(childJson.optString("catering_delivery_status"));
                                        cateringCompleteOrdersMO.setCateringCancelStatus(childJson.optString("catering_cancel_status"));
                                        updateOrderStatusTrackerUI();
                                    }
                                } else {
                                    ARCustomToast.showToast(CateringQRTrackingStatus.this, childJson.optString("DESCRIPTION"));
                                }
                            } else {
                                ARCustomToast.showToast(CateringQRTrackingStatus.this, childJson.optString("DESCRIPTION"));
                            }
                        }

                    } catch (Exception e) {
                        ARCustomToast.showToast(CateringQRTrackingStatus.this, "Invalid response, parsing error!!!");
                    }
                } else {
                    if (Utility.isInternetAvailable(CateringQRTrackingStatus.this))
                        ARCustomToast.showToast(CateringQRTrackingStatus.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(CateringQRTrackingStatus.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(CateringQRTrackingStatus.this))
                    ARCustomToast.showToast(CateringQRTrackingStatus.this, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(CateringQRTrackingStatus.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

}
