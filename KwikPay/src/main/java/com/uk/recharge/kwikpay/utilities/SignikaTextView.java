package com.uk.recharge.kwikpay.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by HP on 27-08-2016.
 */
public class SignikaTextView extends TextView {

    public SignikaTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SignikaTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SignikaTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/signika.ttf");
            setTypeface(tf);
        }
    }
}