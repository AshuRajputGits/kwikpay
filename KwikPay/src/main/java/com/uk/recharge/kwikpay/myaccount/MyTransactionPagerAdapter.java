package com.uk.recharge.kwikpay.myaccount;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.uk.recharge.kwikpay.myaccount.fragments.CateringTransactionFragment;
import com.uk.recharge.kwikpay.myaccount.fragments.DigitalTransactionsFragment;
import com.uk.recharge.kwikpay.myaccount.fragments.EVChargeTransactionsFragment;
import com.uk.recharge.kwikpay.myaccount.fragments.MobileTransactionsFragment;

/**
 * Created by Ashu Rajput on 12/20/2017.
 */

public class MyTransactionPagerAdapter extends FragmentStatePagerAdapter {

    public MyTransactionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        /*if (position == 0) {
            AllTransactionsFragment allFragment = new AllTransactionsFragment();
            return allFragment;
        } else if (position == 1) {
            MobileTransactionsFragment mobileFragment = new MobileTransactionsFragment();
            return mobileFragment;
        } else if (position == 2) {
            DigitalTransactionsFragment digitalFragment = new DigitalTransactionsFragment();
            return digitalFragment;
        } else if (position == 3) {
            EVChargeTransactionsFragment kvFragment = new EVChargeTransactionsFragment();
            return kvFragment;
        }*/

        if (position == 0) {
            MobileTransactionsFragment mobileFragment = new MobileTransactionsFragment();
            return mobileFragment;
        } else if (position == 1) {
            DigitalTransactionsFragment digitalFragment = new DigitalTransactionsFragment();
            return digitalFragment;
        } else if (position == 2) {
            EVChargeTransactionsFragment kvFragment = new EVChargeTransactionsFragment();
            return kvFragment;
        } else if (position == 3) {
            CateringTransactionFragment cateringTransactionFragment = new CateringTransactionFragment();
            return cateringTransactionFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
