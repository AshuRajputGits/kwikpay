package com.uk.recharge.kwikpay;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.ar.library.validation.FieldValidationWithMessage;
import com.uk.recharge.kwikpay.database.DBQueryMethods;
import com.uk.recharge.kwikpay.utilities.CustomDialogBox;

public class ForgetPasswordScreen extends Activity implements AsyncRequestListenerViaPost
{
	TextView emailAddressLabel,cartItemCounter;
	EditText forgotEmailID;
	SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.forgot_password);

		setUpIds();
	}


	private void setUpIds() 
	{
		// TODO Auto-generated method stub

		preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);	
		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);

		emailAddressLabel=(TextView)findViewById(R.id.forgetPW_mailAddressLabel); 
		emailAddressLabel.setText(Html.fromHtml("Email Address"+"<font color='#ff0000'>*"));

		forgotEmailID = (EditText)findViewById(R.id.loginId);

//		View separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);
		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(ForgetPasswordScreen.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage,cartIconLayout);
		csd.createMenuDrawer();

		//END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;	

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME
		/*DBQueryMethods database=new DBQueryMethods(ForgetPasswordScreen.this);
		try
		{
			database.open();
			TextView headerBelowTV=(TextView)findViewById(R.id.headerBelowTextPart2);
			headerBelowTV.setText(database.getSubHeaderTitles("Forgot password"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}*/

	}


	public void loginsignupProceedMethod(View v)
	{
		final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

		if(!FieldValidationWithMessage.emailAddressIsValid(forgotEmailID.getText().toString(), forgotEmailID))
		{
			ARCustomToast.showToast(ForgetPasswordScreen.this, "please enter a valid email id", Toast.LENGTH_LONG);
		}
		else
		{
			new LoadResponseViaPost(ForgetPasswordScreen.this, formJSONParam() , true).execute("");
		}


	}


	public String formJSONParam() 
	{
		JSONObject jsonobj = new JSONObject();
		try 
		{
			jsonobj.put("APISERVICE", "KPFORGOTPASSWORD");
			jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
			jsonobj.put("IMEINUMBER", preferences.getString("IMEI_NUMBER_KEY", ""));
			jsonobj.put("DEVICEOS", "Android");
			jsonobj.put("SOURCENAME", "KPAPP");
			jsonobj.put("EMAILID", forgotEmailID.getText().toString());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonobj.toString();

	}


	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub

		Log.e("ForgetPassowordResponse", loadedString);

		if(!loadedString.equals("Exception") && !loadedString.equals(""))
		{
			try 
			{
				JSONObject jsonObject,parentResponseObject;

				jsonObject = new JSONObject(loadedString);
				parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

				if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
				{
					if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
					{
						if(parentResponseObject.has("DESCRIPTION") && parentResponseObject.has("USERID"))
						{
							Intent i = new Intent(ForgetPasswordScreen.this,CustomDialogBox.class);
							i.putExtra("MSG_KEY", parentResponseObject.getString("DESCRIPTION"));
							i.putExtra("KP_SCREEN_IDENTIFIER", "FORGOTPW");
							startActivity(i);
							ForgetPasswordScreen.this.finish();
						}
					}
					else
					{
						if(parentResponseObject.has("DESCRIPTION"))
						{
							ARCustomToast.showToast(ForgetPasswordScreen.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						}
					}

				}


			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		else
		{
			ARCustomToast.showToast(ForgetPasswordScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
		}

	}

	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preferences!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preferences.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}
		
		}
	}*/

}
