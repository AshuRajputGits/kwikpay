package com.uk.recharge.kwikpay;

public class ProjectConstant {
    //DATABASE TABLE NAMES
    public static final String DB_AG_TABLENAME = "AREA_GROUP";
    public static final String DB_OPERATOR_TABLENAME = "OPERATOR";
    public static final String DB_OP_AREA_TABLENAME = "OPERATOR_AREA";
    public static final String DB_OP_SERVICE_TABLENAME = "OPR_SERVICE";
    public static final String DB_KP_PHI_TABLENAME = "KP_PAGEWISE_HEADER_INFO";
    public static final String DB_APP_TABLE_VERSION_TABLENAME = "APP_TABLE_VERSION";
    public static final String DB_TERMS_OF_USAGE_REVISED = "TERMS_OF_USAGE_REVISED";
    public static final String DB_HOW_IT_WORKS_REVISED = "HOW_IT_WORKS_REVISED";
    public static final String DB_ABOUTUS_REVISED = "ABOUTUS_REVISED";
    public static final String DB_HOW_TO_USE_REVISED = "HOW_TO_USE_REVISED";
    public static final String DB_SAVE_CART = "CART_DETAIL_HOLDING";

    //DATABASE TABLE : STRING TAGS(AREA GROUP)
    public static final String AG_ID = "AG_ID";
    public static final String AG_ENABLE = "AG_ENABLE";
    public static final String AG_GROUP_CODE = "AG_GROUP_CODE";
    public static final String AG_NAME = "AG_NAME";
    public static final String AG_CURRENCY = "AG_CURRENCY";
    public static final String AG_PREFIX_ALLOW = "AG_PREFIX_ALLOW";
    public static final String AG_COUNTRY_CODE = "AG_COUNTRY_CODE";
    public static final String AG_SUBS_TERM_USED = "AG_SUBCRIBER_TERM_USED";
    public static final String AG_SUBS_DATA_TYPE = "AG_SUBCR_DATA_TYPE";
    public static final String AG_SUBS_ALLOW_LEADING_ZERO = "AG_SUBCR_ALLOW_LEADING_ZERO";
    public static final String AG_SUBS_MIN_LENGTH = "AG_SUBCR_MIN_LEN";
    public static final String AG_SUBS_MAX_LENGTH = "AG_SUBCR_MAX_LEN";
    public static final String AG_PREFIX_LENGTH = "AG_PREFIX_LEN";
    public static final String AG_SECOND_VALIDATION = "AG_SECOND_VALIDATION";
    public static final String AG_CF_ACTIVE_FLAG = "CF_ACTIVE_FLAG";

    //DATABASE TABLE : STRING TAGS(OPERATOR)
    public static final String OPERATOR_ID = "OP_ID";
    public static final String OP_OPERATOR_ID = "OP_CODE";
    public static final String OPERATOR_ABBR = "OP_ABBR";
    public static final String OPERATOR_NAME = "OP_NAME";
    public static final String OPERATOR_ENABLE = "OP_ENABLE";
    public static final String OPERATOR_HLR_CODE = "OP_HLR_CODE";
    public static final String OPERATOR_CF_ACTIVE_FLAG = "CF_ACTIVE_FLAG";

    //DATABASE TABLE : STRING TAGS(OPERATOR AREA)
    public static final String OA_ID = "OA_ID";
    public static final String OA_AG_ID = "AG_ID";
    public static final String OA_OS_ID = "OS_ID";
    public static final String OA_CONFIGURED = "OA_CONFIGURED";
    public static final String OA_HLR_CODE = "OA_HLR_CODE";
    public static final String OA_ENABLE = "OA_ENABLE";
    public static final String OA_REQ_TYPE = "OA_REQ_TYPE";
    public static final String OA_LOGO = "OA_LOGO";
    public static final String OA_CF_ACTIVE_FLAG = "CF_ACTIVE_FLAG";

    //DATABASE TABLE : STRING TAGS(OPR SERVICE)
    public static final String OS_ID = "OS_ID";
    public static final String OS_OP_ID = "OP_ID";
    public static final String OS_SERVICE_ID = "SERVICE_ID";
    public static final String OS_OP_LOGO = "OP_LOGO";
    public static final String OS_ENABLE = "OS_ENABLE";
    public static final String OS_IS_PLAN_RANGE = "OS_IS_PLAN_RANGE";
    public static final String OS_CF_ACTIVE_FLAG = "CF_ACTIVE_FLAG";

    // TRANSACTION HISTORY DATA OR MY ACCOUNT DATABASE AND API STRINGS

    public static final String TXN_TRANSACTIONDETAIL = "TRANSACTIONDETAIL";
    public static final String TXN_TRANSACTIONDATETIME = "TRANSACTIONDATETIME";
    public static final String TXN_SERVICEID = "SERVICEID";
    public static final String TXN_PAYMENTCURRENCYCODE = "PAYMENTCURRENCYCODE";
    public static final String TXN_RECHARGECURRENCYCODE = "RECHARGECURRENCYCODE";
    public static final String TXN_OPERATORNAME = "OPERATORNAME";
    public static final String TXN_PAYMENTAMOUNT = "PAYMENTAMOUNT";
    public static final String TXN_PAYMENTSTATUS = "PAYMENTSTATUS";
    public static final String TXN_SERVICENAME = "SERVICENAME";
    public static final String TXN_COUNTRYCODE = "COUNTRYCODE";
    public static final String TXN_DISPLAYAMOUNT = "DISPLAYAMOUNT";
    public static final String TXN_ORDERID = "ORDERID";
    public static final String TXN_OPERATORCODE = "OPERATORCODE";
    public static final String TXN_DISPLAYCURRENCYCODE = "DISPLAYCURRENCYCODE";
    public static final String TXN_RECHARGESTATUS = "RECHARGESTATUS";
    public static final String TXN_RECHARGEAMOUNT = "RECHARGEAMOUNT";
    public static final String TXN_SUBSCRIPTIONNUMBER = "SUBSCRIPTIONNUMBER";
    public static final String TXN_QUERYTYPEID = "QUERYTYPEID";

    //MY CART TABLE TAGS
    public static final String CART_CARTID = "CARTID";
    public static final String CART_MOBILENUMBER = "MOBILENUMBER";
    public static final String CART_NETPGAMOUNT = "NETPGAMOUNT";
    public static final String CART_CPNTXNID = "CPNTXNID";
    public static final String CART_CPNAMOUNT = "CPNAMOUNT";
    public static final String CART_CPNMESSAGE = "CPNMESSAGE";
    public static final String CART_DISCOUNTAMOUNT = "DISCOUNTAMOUNT";
    public static final String CART_PGDISCOUNTAMOUNT = "PGDISCOUNTAMOUNT";
    public static final String APP_SESSIONID = "SESSIONID";
    public static final String CART_INSERTIONDATE = "INSERTIONDATE";
    public static final String CART_UPDATEDDATE = "UPDATEDDATE";
    public static final String CART_CPNSRNO = "CPNSRNO";

    //MY FAVOURITE DETAILS API TAGS
    public static final String FAV_FAVOURITES = "FAVOURITES";
    public static final String FAV_COUNTRYCODE = "COUNTRYCODE";
    public static final String FAV_SUBSCRIBERNUM = "SUBSCRIBERNUM";
    public static final String FAV_ORDERID = "ORDERID";
    public static final String FAV_OPCODE = "OPCODE";
    public static final String FAV_AMOUNT = "AMOUNT";
    public static final String FAV_NICKNAME = "NICKNAME";

    // MY TICKET DETAILS API STRING TAGS
    public static final String TICKET_RAISETICKETDETAIL = "RAISETICKETDETAIL";
    public static final String TICKET_TICKETSTATUS = "TICKETSTATUS";
    public static final String TICKET_TICKETNO = "TICKETNO";
    public static final String TICKET_QUERYTYPE = "QUERYTYPE";
    public static final String TICKET_TICKETDESC = "TICKETDESC";
    public static final String TICKET_TYPEOFPROBLEM = "TYPEOFPROBLEM";
    public static final String ESCALATE_TICKETDATETIME = "TICKETDATETIME";
    public static final String ESCALATE_TICKETID = "TICKETID";

    // RECEIPT OR TRANSACTION STATUS DETAILS API STRING TAGS
    public static final String RECEIPT_TRANSACTIONLIST = "TRANSACTIONLIST";
    public static final String RECEIPT_USEREMAIL = "USEREMAIL";
    public static final String RECEIPT_TRANSACTIONDATETIME = "TRANSACTIONDATETIME";
    public static final String RECEIPT_RECHARGEPIN = "RECHARGEPIN";
    public static final String RECEIPT_USERNAME = "USERNAME";
    public static final String RECEIPT_RECHARGEREQUESTTYPE = "RECHARGEREQUESTTYPE";
    public static final String RECEIPT_VENDORDESC = "VENDORDESC";
    public static final String RECEIPT_USERID = "USERID";

    // DATABASE TABLE : STRING TAGS FOR(KP_PAGEWISE_HEADER_INFO)
    public static final String KP_PHI = "KP_PHI";
    public static final String KP_PHI_HEADER_CONTENT = "KP_PHI_HEADER_CONTENT";
    public static final String KP_PHI_SCREEN_NAME = "KP_PHI_SCREEN_NAME";

    // API RESPONSE, TAGS
    public static final String RESPONSE_TAG = "KPRESPONSE";
    public static final String RESPONSE_MSG_TAG = "RESPONSEMESSAGE";

    //API RESPONSE TAGS FOR OPERATOR TABLE
    public static final String OP_OPERATOR_ID_TAG = "OP_OPERATOR_ID";

    // API TAGS FOR DATA DOWNLOAD API
    public static final String OPERATOR_AREA_TAG = "OPERATOR_AREA";
    public static final String AREA_GROUP_TAG = "AREA_GROUP";

    // TAGS UNDER AREA GROUP TABLE
    /*public static final String AG_GROUP_CODE_TAG="AG_GROUP_CODE";
    public static final String AG_NAME_TAG="AG_NAME";
    public static final String OPR_SERVICE_TAG="OPR_SERVICE";*/

    //TAGS UNDER APP TABLE VERSION TABLE
    public static final String OPERATOR_TAG = "OPERATOR";

    // COMMON TAGS FOR API GETTING IN RESPONSE FROM MSDIN API
    public static final String API_RESPONSE_CODE = "RESPONSECODE";
    public static final String API_OPERATORS = "OPERATORS";
    public static final String API_OPNAME = "OPNAME";
    public static final String API_OPTEMPACTIVEFLAG = "OPTEMPACTIVEFLAG";
    public static final String API_SECONDLEVELVALIDATIONE = "SECONDLEVELVALIDATION";
    public static final String API_OPID = "OPID";
    public static final String API_OPIMGPATH = "OPIMGPATH";
    public static final String API_OPCODE = "OPCODE";
    public static final String API_OPACTIVEFLAG = "OPACTIVEFLAG";
    public static final String API_OPCIRCLEACTIVEFLAG = "OPCIRCLEACTIVEFLAG";

    // COMMON TAGS FOR API GETTING IN RESPONSE FROM TOPUP PRODUCTS API
    public static final String TOPUP_API_PRODUCTS = "PRODUCTS";
    public static final String TOPUP_API_DISPLAYAMT = "DISPLAYAMT";
    public static final String TOPUP_API_DISPLAYCURRENCYAGID = "DISPLAYCURRENCYAGID";
    public static final String TOPUP_API_DISPLAYCURRENCYIMGPATH = "DISPLAYCURRENCYIMGPATH";
    public static final String TOPUP_API_RECHARGEAMT = "RECHARGEAMT";
    public static final String TOPUP_API_PAYMENTCURRENCYIMGPATH = "PAYMENTCURRENCYIMGPATH";
    public static final String TOPUP_API_RECHARGECURRENCYAGID = "RECHARGECURRENCYAGID";
    public static final String TOPUP_API_PAYMENTAMT = "PAYMENTAMT";
    public static final String TOPUP_API_PAYMENTCURRENCYAGID = "PAYMENTCURRENCYAGID";
    public static final String TOPUP_API_RECHARGECURRENCYIMGPATH = "RECHARGECURRENCYIMGPATH";
    public static final String TOPUP_API_NO_OF_PRODUCTS = "NOOFPRODUCTS";
    public static final String GAMESERVICE_KPDIGITALSERVICE = "KPDIGITALSERVICE";

    // COMMON TAGS FOR API GETTING IN RESPONSE FROM PAY NOW API
    public static final String PAYNOW_MOBILENUMBER = "MOBILE_NUMBER";
    public static final String PAYNOW_PAYMENTCURRENCYCODE = "PAYMENTCURRENCYCODE";
    public static final String PAYNOW_OPERATORNAME = "OPERATORNAME";
    public static final String PAYNOW_RECHARGECURRENCYCODE = "RECHARGECURRENCYCODE";
    public static final String PAYNOW_PROCESSINGFEE = "PROCESSINGFEE";
    public static final String PAYNOW_DISPLAYCURRENCYCODE = "DISPLAYCURRENCYCODE";
    public static final String PAYNOW_RECHARGEAMOUNT = "RECHARGEAMOUNT";
    public static final String PAYNOW_SERVICEFEE = "SERVICEFEE";
    public static final String PAYNOW_PLANDESCRIPTION = "PLANDESCRIPTION";
    public static final String PAYNOW_PAYMENTCURRENCYAGID = "PAYMENTCURRENCYAGID";
    public static final String TICKET_ID = "TICKETID";

    // COMMON TAGS FOR PAYMENT GATEWAY
    public static final String PG_PGSOID = "PGSOID";
    public static final String PG_MOPID = "MOPID";

    // COMMON TAGS FOR PAYMENT GATEWAY APP TRANSACTION ID
    public static final String PG_APP_TXN_ID = "TXNID";
    public static boolean ISITCOMINGFROM_MYACCOUNT = false;
    public static boolean ISLOGINPAGE_ACTIVE = false;
    public static String SESSIONID = "";

    // INTRODUCING TO NEGLECT CONSECUTIVES OPENING OF SAME PAGE FROM SLIDER
    public static int MY_TRANSACTION_COUNTER = 0, MY_ACCOUNT_COUNTER = 0, HOWITWORKS_COUNTER = 0, CONTACTUS_COUNTER = 0, TOU_COUNTER = 0, ABOUTUS = 0;
    //	public static String PG_SO_ID_VALUE="",PG_MOP_ID_VALUE="";
//	public static String PG_SO_ID_VALUE1="",PG_MOP_ID_VALUE1="";
    public static String CART_INFO_MESSAGE = "";

    public static int TOP_UP_IDENTIFIER = 0;   // 1 : INTERNATIONAL TOPUP, 2: TOPUP YOUR PHONE, 3: GAME & SERVICES
    public static String tncURL = null;
    public static String providerDetailsURL = null;

//	public static String receivedPublicIP="0.0.0.0";
}
