package com.uk.recharge.kwikpay.nativepg;

import java.io.InputStream;
import java.security.MessageDigest;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.ReceiptScreen;

public class JUDOPaymentGateway extends Activity
//        implements AsyncRequestListenerViaPost
{
/*	private HashMap<String, String> receivedNativePGData;
	private SharedPreferences preference;
	private String receivedPGName="",receivedOrderId="",receivedAppTxnId="",receivedTotalAmount="";
	private static  String MY_JUDO_ID="",MY_API_TOKEN = "",MY_API_SECRET = "";

	// Set your OAUTH token if instead of API_TOKEN AND API_SECRET above if using OAUTH
	static  String MY_OAUTH_TOKEN = "";

	private static final int ACTION_CARD_PAYMENT = 101,ACTION_TOKEN_PAYMENT = 102,ACTION_PREAUTH =201;
	private long delayTimeInterval=0;
//	private int nativePGCase = 0;
	private static Bitmap pgBanner;
	private static Bitmap kpLogo;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState); 
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
//		setContentView(new GIFView(this));
		setContentView(R.layout.payment_holding_screen);

		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);

		Bundle receivedData=getIntent().getExtras();

		if(receivedData!=null)
		{
			if(receivedData.containsKey("NATIVE_PG_HASHMAP_KEY"))
			{
				try
				{
					receivedNativePGData = (HashMap<String, String>) receivedData.getSerializable("NATIVE_PG_HASHMAP_KEY");
					MY_JUDO_ID=receivedNativePGData.get("JUDOID").toString();
					MY_API_TOKEN=receivedNativePGData.get("TOKEN").toString();
					MY_API_SECRET=receivedNativePGData.get("SECRETKEY").toString();
					MY_OAUTH_TOKEN=receivedNativePGData.get("CHECKSUMKEY").toString();
					receivedPGName=receivedNativePGData.get("PGNAME").toString();
					receivedOrderId=receivedNativePGData.get("ORDERID").toString();
					receivedAppTxnId=receivedNativePGData.get("APPTXNID").toString();

				}catch(Exception e)
				{
				}
			}

			else if (receivedData.containsKey("NATIVE_PG_COUPON"))
			{
				receivedNativePGData = (HashMap<String, String>) receivedData.getSerializable("NATIVE_PG_COUPON");
				receivedTotalAmount =receivedNativePGData.get("TOTALAMOUNTPAYABLE");
				receivedPGName=receivedNativePGData.get("PGNAME").toString();
				receivedOrderId=receivedNativePGData.get("ORDERID").toString();
				receivedAppTxnId=receivedNativePGData.get("APPTXNID").toString();
				new LoadResponseViaPost(JUDOPaymentGateway.this, getTimerValueJson(), true).execute("");
			}
		}

		nativePGCallBacks();
		
		new Handler().postDelayed(new Runnable() 
		{
            @Override
            public void run() 
            {
            	//ENABLE IF WANTS TO LAUNCH NORAML JUDO PAGE FOR PAYMENT PROCESSING
            	makePayment();

            	//ENABLE IF WANTS TO LAUNCH PRE-AUTHENTICATION JUDO PAGE FOR PAYMENT PROCESSING
            	//   makePreAuth();
            }
        }, 1000);

	}

	public void nativePGCallBacks()
	{
		// If you are using OAuth to connect to Judo, use the following method:
		// JudoAPIManager.setOAuthAccessToken(this, MY_OAUTH_TOKEN);

		// Otherwise use the following:
		// Set the API TOKEN and API SECRET
		JudoSDKManager.setKeyAndSecret(this, MY_API_TOKEN, MY_API_SECRET);
		//		JudoSDKManager.setKeyAndSecret(this, "4Aj96owzTjIdcQ71", "38c83d3b6239926087adf43c2bb653d0c60dc61d279195b1dca53f3c3151a2a5");

		//3Dsecure defaults to false
		JudoSDKManager.set3DsecureEnabled(true);

		//AVS defaults to false
		JudoSDKManager.setAVSEnabled(true);

		// Uncomment the following to use Production servers (defaults to sandbox)
//		JudoSDKManager.setProductionMode(this);

		JudoAPIServiceBase.setSSLPinning(false);
		JudoSDKManager.setMaestroEnabled(true);

		//not supported yet
		JudoSDKManager.setAmexEnabled(true);
	}

	public void makePayment()
	{
		try
		{
			String currency = "GBP";
			String payableAmount = receivedNativePGData.get("TOTALAMOUNTPAYABLE").toString();
			String paymentReference = receivedNativePGData.get("PAYMENTREFERENCE").toString();
			String consumerReference = receivedNativePGData.get("CONSUMERREFERENCE").toString();

			// Optional: Supply meta data about this transaction, pass as last argument instead of null.
			//			 Bundle metaData = new Bundle();
			//			 metaData.putString("myName", "myValue");
			//			 metaData.putString("myOtherName", "myOtherValue");

			//			Log.e("", "details "+MY_JUDO_ID+"  "+currency+"  "+payableAmount+ "  "+paymentReference+"  "+consumerReference);

			Intent intent = JudoSDKManager.makeAPayment(JUDOPaymentGateway.this, MY_JUDO_ID, currency, payableAmount, 
					paymentReference, consumerReference, null);

			startActivityForResult(intent, ACTION_TOKEN_PAYMENT);

		}catch(Exception ee)
		{
		}
	}

	// METHOD FOR REQUESTING PRE-AUTHENTICATION
	public void makePreAuth()
	{
		try {
			JudoSDKManager.setKeyAndSecret(getApplicationContext(), MY_API_TOKEN, MY_API_SECRET);

			Intent intent = JudoSDKManager.makeAPreAuth(getApplicationContext(), MY_JUDO_ID, "GBP",
					receivedNativePGData.get("TOTALAMOUNTPAYABLE").toString(), 
					receivedNativePGData.get("PAYMENTREFERENCE").toString(), Settings.Secure.ANDROID_ID, null);

			startActivityForResult(intent, ACTION_PREAUTH);
		} catch(Exception e){}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		switch (requestCode)
		{
		case ACTION_CARD_PAYMENT:
		case ACTION_TOKEN_PAYMENT:
			processPaymentResult(resultCode, data);
			break;
		case ACTION_PREAUTH:
			processPreAuthResult(resultCode, data, getApplicationContext());

		}
	}

	//METHOD DEFINING NORMAL PAYMENT FLOW RESULT
	public void processPaymentResult(int resultCode, Intent data)
	{
//		Log.e("", "JUDO PG  response "+resultCode + "  Data: "+data);
		switch (resultCode)
		{
		case JudoSDKManager.JUDO_SUCCESS:
			// STUB: Handle successful payment
			if(pgBanner!=null)
			{
				pgBanner.recycle();
				pgBanner = null;
			}
			
			Receipt receipt = data.getParcelableExtra(JudoSDKManager.JUDO_RECEIPT);
			//    			Consumer consumer = receipt.getConsumer();
			//    			CardToken cardToken = receipt.getCardToken();

//			Log.e("","receipt "+ receipt.toString());

			new LoadResponseViaPost(JUDOPaymentGateway.this, getTimerValueJson(receipt), false).execute("");

			break;

		case JudoSDKManager.JUDO_CANCELLED:
			// STUB: Handle Payment Cancelled
			Intent receiptIntent = new Intent(JUDOPaymentGateway.this,ReceiptScreen.class);
			receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedAppTxnId);
			startActivity(receiptIntent);
			JUDOPaymentGateway.this.finish();
			break;

		case JudoSDKManager.JUDO_ERROR:
			// STUB: Handle Payment Cancelled
			// JudoSDKManager.JudoError judoError = JudoSDKManager.parseErrorIntent(data);

			Intent receiptIntent2 = new Intent(JUDOPaymentGateway.this,ReceiptScreen.class);
			receiptIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			receiptIntent2.putExtra("PG_APP_TXN_ID_KEY", receivedAppTxnId);
			startActivity(receiptIntent2);
			break;
		}
	}

	*//**
	 * Function to process the PreAuth result
	 *//*
	public void processPreAuthResult(int resultCode, Intent data, Context context){
		switch (resultCode){
		case JudoSDKManager.JUDO_SUCCESS:
			Receipt receipt = data.getParcelableExtra(JudoSDKManager.JUDO_RECEIPT);
			//Consumer consumer = receipt.getConsumer();
			//CardToken cardToken = receipt.getCardToken();

			// Save the receipt for making a collection later.
			// Log.d(TAG, "PreAuth Successful: "+ receipt.getReceiptId());

//			Toast.makeText(context, "PreAuth Successful: "+ receipt.getReceiptId(), Toast.LENGTH_SHORT).show();
			
			collectFunds(receipt,receivedNativePGData.get("PAYMENTREFERENCE").toString(),JUDOPaymentGateway.this);
			
			break;
		case JudoSDKManager.JUDO_CANCELLED:
			Toast.makeText(context, "PreAuth Cancelled", Toast.LENGTH_SHORT).show();
			
			break;
		case JudoSDKManager.JUDO_ERROR:
			Toast.makeText(context, "PreAuth Error", Toast.LENGTH_SHORT).show();
			
			break;
		}
	}

	//FORM TIMER VALUE JSON PARAMETERS
	public String getTimerValueJson(Receipt receivedReceipt)
	{
		//		Log.e("", "Judo Receipt Response "+receivedReceipt);
		try
		{
			String formEncryptData = "KPAPP"+ "|" + String.valueOf(receivedReceipt.getAmount())+ "|" + MY_JUDO_ID+ "|" + receivedOrderId +
					"|" +receivedAppTxnId + "|" + receivedReceipt.getResult() + "|" + "ozj5Fvn3skB51EnNK3lTqoqo+p4JxtHrzjIoyncH57o=";

			String checkSumString=encryptInSHA256(formEncryptData.trim());

			JSONObject json = new JSONObject();

			json.put("APISERVICE", "KPSAVEPGRESPONSE");
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("DEVICEOS", "ANDROID");
			json.put("SOURCENAME", "KP_AND");
			json.put("APPTRANSACTIONID",receivedAppTxnId);
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("PGNAME",receivedPGName);
			json.put("AMOUNT",String.valueOf(receivedReceipt.getAmount()));
			json.put("APPEARSONSTATEMENTAS",receivedReceipt.getAppearsOnStatementAs());
			json.put("CARDDETAILS","");
			json.put("CARDLASTFOUR",receivedReceipt.getCardToken().getCardLastFour());
			json.put("CARDTOKEN",receivedReceipt.getCardToken().getCardToken());
			json.put("CARDTYPE",String.valueOf(receivedReceipt.getCardToken().getCardType()));
			json.put("CONSUMER","");
			json.put(" n ",receivedReceipt.getConsumer().getConsumerToken());
			json.put("CREATEDAT",receivedReceipt.getCreatedAt());
			json.put("CURRENCY","GBP");
			json.put("ENDDATE",receivedReceipt.getCardToken().getEndDate());
			json.put("JUDOID",MY_JUDO_ID);
			json.put("MERCHANTNAME",receivedReceipt.getMerchantName());
			json.put("MESSAGE",receivedReceipt.getMessage());
			json.put("NETAMOUNT",String.valueOf(receivedReceipt.getNetAmount()));
			json.put("ORIGINALAMOUNT",String.valueOf(receivedReceipt.getOriginalAmount()));
			json.put("ORIGINALRECEIPTID",String.valueOf(receivedReceipt.getOriginalReceiptId()));
			json.put("PARTNERSERVICEFEE",String.valueOf(receivedReceipt.getPartnerServiceFee()));
			json.put("RAWRECEIPT","");
			json.put("RECEIPTID",receivedReceipt.getReceiptId());
			json.put("REFUNDS",String.valueOf(receivedReceipt.getRefundAmount()));
			json.put("RESULT",receivedReceipt.getResult());
			json.put("TYPE",receivedReceipt.getType());
			json.put("CONSUMERREFERENCE",receivedReceipt.getConsumer().getYourConsumerReference());
			json.put("PAYMENTREFERENCE",receivedReceipt.getPaymentReference());
			json.put("STATUS",receivedReceipt.getResult());
			json.put("ORDERID",receivedOrderId);
			json.put("CHECKSUM ", checkSumString);

			return json.toString();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	// FORMING TIMER VALUE JSON PARAMETER IN CASE OF COUPON PAYMENT GATEWAY
	public String getTimerValueJson()
	{
		try
		{
			*//*String formEncryptData = "KPAPP"+ "|" + String.valueOf(receivedReceipt.getAmount())+ "|" + MY_JUDO_ID+ "|" + receivedOrderId +
									  "|" +receivedAppTxnId + "|" + receivedReceipt.getResult() + "|" + "ozj5Fvn3skB51EnNK3lTqoqo+p4JxtHrzjIoyncH57o=";*//*
			*//*Log.e("", "sALT DATA "+formEncryptData);
			String checkSumString=encryptInSHA256(formEncryptData.trim());*//*

			JSONObject json = new JSONObject();

			json.put("APISERVICE", "KPSAVEPGRESPONSE");
			json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
			json.put("DEVICEOS", "ANDROID");
			json.put("SOURCENAME", "KP_AND");
			json.put("APPTRANSACTIONID",receivedAppTxnId);
			json.put("SESSIONID", ProjectConstant.SESSIONID);
			json.put("PGNAME",receivedPGName);
			json.put("AMOUNT",receivedTotalAmount);
			json.put("APPEARSONSTATEMENTAS","");
			json.put("CARDDETAILS","");
			json.put("CARDLASTFOUR","");
			json.put("CARDTOKEN","");
			json.put("CARDTYPE","");
			json.put("CONSUMER","");
			json.put(" n ","");
			json.put("CREATEDAT","");
			json.put("CURRENCY","GBP");
			json.put("ENDDATE","");
			json.put("JUDOID","");
			json.put("MERCHANTNAME","");
			json.put("MESSAGE","");
			json.put("NETAMOUNT","");
			json.put("ORIGINALAMOUNT","");
			json.put("ORIGINALRECEIPTID","");
			json.put("PARTNERSERVICEFEE","");
			json.put("RAWRECEIPT","");
			json.put("RECEIPTID","");
			json.put("REFUNDS","");
			json.put("RESULT","");
			json.put("TYPE","");
			json.put("CONSUMERREFERENCE","");
			json.put("PAYMENTREFERENCE","");
			json.put("STATUS","");
			json.put("ORDERID",receivedOrderId);
			json.put("CHECKSUM ", "");

			return json.toString();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onRequestComplete(String loadedString) 
	{
		// TODO Auto-generated method stub
		if(loadedString!=null && !loadedString.equals(""))
		{
			if(parseJudoPaymentResponse(loadedString))
			{
//				setContentView(new GIFView(JUDOPaymentGateway.this));
				setContentView(new GIFTimerView(JUDOPaymentGateway.this));
				
				new Handler().postDelayed(new Runnable() 
				{
					@Override
					public void run() 
					{
						Intent receiptIntent = new Intent(JUDOPaymentGateway.this,ReceiptScreen.class);
						receiptIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						receiptIntent.putExtra("PG_APP_TXN_ID_KEY", receivedAppTxnId);
						startActivity(receiptIntent);

						JUDOPaymentGateway.this.finish();
					}
				}, delayTimeInterval);
			}
		}
	}

	//LOGIC FOR PARSING THE COUPON RECEIVED DATA 
	public boolean parseJudoPaymentResponse(String judoResponse)
	{
		JSONObject jsonObject,parentResponseObject;
		try 
		{
			jsonObject=new JSONObject(judoResponse);

			if(jsonObject.isNull(ProjectConstant.RESPONSE_TAG))
			{
				ARCustomToast.showToast(JUDOPaymentGateway.this,"No Reponse Tag", Toast.LENGTH_LONG);
				return false;
			}

			parentResponseObject=jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

			if(parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE))
			{
				if(parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0"))
				{
					if(parentResponseObject.has("WAITINGTIME"))
					{
						delayTimeInterval=Long.parseLong(parentResponseObject.getString("WAITINGTIME"));
					}
				}

				else
				{
					if(parentResponseObject.has("DESCRIPTION"))
					{
						ARCustomToast.showToast(JUDOPaymentGateway.this,parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
						return false;
					}
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;

	}

	private static String encryptInSHA256(String base) 
	{
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) 
			{
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}

			return hexString.toString();
		} catch(Exception ex){
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	}

	private static class GIFView extends View
	{
//		Movie movie;
//		InputStream is=null;
//		long moviestart;
		public GIFView(Context context) {
			super(context);
//			is=context.getResources().openRawResource(R.drawable.pg_arrow);
			kpLogo=BitmapFactory.decodeResource(getResources(), R.drawable.splash_logo);
			pgBanner=BitmapFactory.decodeResource(getResources(), R.drawable.verified_payment_banner);
//			movie=Movie.decodeStream(is);
		}

		@Override
		protected void onDraw(Canvas canvas) 
		{
			try
			{
				canvas.drawColor(getResources().getColor(R.color.whiteColor));
				super.onDraw(canvas);
//				long now=android.os.SystemClock.uptimeMillis();
//				if (moviestart == 0) 
//					moviestart = now;

//				int relTime = (int)((now - moviestart) % movie.duration()) ;
//				movie.setTime(relTime);
				
				canvas.drawBitmap(kpLogo, (canvas.getWidth()/2)-(kpLogo.getWidth()/2),(canvas.getHeight()/2)-(kpLogo.getHeight()), null);
				
//				movie.draw(canvas,(getWidth()/2)-(movie.width()/2),(getHeight()/2)-(movie.height()/2));
//				movie.draw(canvas,(canvas.getWidth()/2)-(movie.width()/2),( (canvas.getHeight()/2) + movie.height()));

				if(pgBanner!=null)
					canvas.drawBitmap(pgBanner, 0,(canvas.getHeight()-(pgBanner.getHeight()*2)), null);
				
				this.invalidate();
			}catch(Exception e)
			{
				
			}
		}
	}
	
	private static class GIFTimerView extends View
	{
		Movie movie;
		InputStream is=null;
		long moviestart;
		
		public GIFTimerView(Context context) {
			super(context);
			is=context.getResources().openRawResource(R.drawable.countdown_timer);
//			pgBanner2=BitmapFactory.decodeResource(getResources(), R.drawable.verified_payment_banner);
			movie=Movie.decodeStream(is);
		}

		@Override
		protected void onDraw(Canvas canvas) 
		{
			try
			{
				canvas.drawColor(getResources().getColor(R.color.whiteColor));
				super.onDraw(canvas);
				long now=android.os.SystemClock.uptimeMillis();
				if (moviestart == 0) 
					moviestart = now;

				int relTime = (int)((now - moviestart) % movie.duration()) ;
				movie.setTime(relTime);
				movie.draw(canvas,(canvas.getWidth()/2)-(movie.width()/2),(canvas.getHeight()/2)-((movie.width()/2)+(movie.width()/6) ));
				if(pgBanner!=null)
				{
					canvas.drawBitmap(pgBanner, 0,canvas.getHeight()-(pgBanner.getHeight()*2), null);
				}
				
				this.invalidate();
			}catch(Exception e)
			{
			}
		}
	}

	// INTRODUCING THE METHOD TO REFUND THE TRANSACTION AMOUNT OR COLLECTING THE TXN AMOUNT FROM JUDO
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void collectFunds(Receipt receipt, String PayRef, Activity act)
    {
        try {
            PaymentAction pa = new PaymentAction();
            CollectionRequest cr = new CollectionRequest(JUDOPaymentGateway.this, receipt.getReceiptId(), receipt.getAmount(),PayRef);
            pa.collection(JUDOPaymentGateway.this, cr, new Callback(act) {
                @Override
                protected void onSuccess(Object o) {

                	 Toast.makeText(getApplicationContext(), "Funds Collected", Toast.LENGTH_SHORT).show();
                }

            });
            
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
	
	*/
}