package com.uk.recharge.kwikpay.kwikcharge.qrscanner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.R;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by Ashu Rajput on 9/19/2017.
 */

public class BarcodeScannerActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.barcode_scanner_activity);
        initiateScannerView();

        findViewById(R.id.headerHomeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BarcodeScannerActivity.this, HomeScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

    }

    private void initiateScannerView() {
        ViewGroup contentFrame = findViewById(R.id.content_frame);
        mScannerView = new ZBarScannerView(this);
        contentFrame.addView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {

        if (rawResult != null) {

            String qrScannedCode = rawResult.getContents();
            if (qrScannedCode != null && !qrScannedCode.isEmpty()) {
                if (qrScannedCode.contains("/")) {
                    String prefixWithQRCode = qrScannedCode.substring(qrScannedCode.lastIndexOf("/") + 1);
//                    Log.e("BarCodeScan", "Code : " + prefixWithQRCode);

                    broadcastQRCodeToCaller(prefixWithQRCode);

                    /*if (prefixWithQRCode.contains("-")) {
                        String prefixCode = prefixWithQRCode.split("-")[0];
                        String locationID = prefixWithQRCode.split("-")[1];

                        Log.d("BarCodeScan", "Prefix and QR  : " + prefixCode + " And " + locationID);

                    } else
                        ARCustomToast.showToast(BarcodeScannerActivity.this, getString(R.string.qr_error_msg), Toast.LENGTH_LONG);*/

                } else if (qrScannedCode.contains("-")) {
                    broadcastQRCodeToCaller(qrScannedCode);
                } else
                    ARCustomToast.showToast(BarcodeScannerActivity.this, getString(R.string.qr_error_msg));
            }
            finish();
        }
    }

    private void broadcastQRCodeToCaller(String qrWithPrefix) {
        setResult(Activity.RESULT_OK, new Intent().putExtra("QR_RESULT", qrWithPrefix));
    }
}