package com.uk.recharge.kwikpay;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uk.recharge.kwikpay.adapters.AdsViewPagerAdapter;
import com.uk.recharge.kwikpay.kwikcharge.ScanAndLocationActivity;
import com.uk.recharge.kwikpay.utilities.Utility;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by Ashu Rajput on 11/19/2017.
 */

public class TopUpIntermediateScreen extends AppCompatActivity {

    private AutoScrollViewPager mViewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top_up_intermediate_screen);
        settingResourceIds();
    }

    private void settingResourceIds() {

        TextView localMobileTopUpOption = findViewById(R.id.localMobileTopUpOption);
        TextView internationalMobileTopUpOption = findViewById(R.id.internationalMobileTopUpOption);
        localMobileTopUpOption.setOnClickListener(onClickListener);
        internationalMobileTopUpOption.setOnClickListener(onClickListener);

        localMobileTopUpOption.setText(Utility.fromHtml("<b><font color='#262626'>Mobile top up</b>" + "<br />" +
                "<small><small><font color='#262626'>Top up your and your family's Mobiles on any UK operator</small></small>"));

        internationalMobileTopUpOption.setText(Utility.fromHtml("<b><font color='#262626'>International top up</b>" + "<br />" +
                "<small><small><font color='#262626'>Top up your family's or friends mobiles in 100+ countries</small></small>"));

        ImageView homeButtonImage =  findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage =  findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout =  findViewById(R.id.drawer_layout);
        ListView mDrawerList =  findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        AdsViewPagerAdapter adsViewPagerAdapter = new AdsViewPagerAdapter(this, "TopUpCharge");
        mViewPager =  findViewById(R.id.viewPager);
        mViewPager.startAutoScroll(4500);
        mViewPager.setInterval(4500);
        mViewPager.setAdapter(adsViewPagerAdapter);

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.localMobileTopUpOption) {
                ProjectConstant.TOP_UP_IDENTIFIER = 0;
                startActivity(new Intent(TopUpIntermediateScreen.this, TopUpYourPhone.class));

            } else if (v.getId() == R.id.internationalMobileTopUpOption) {
                ProjectConstant.TOP_UP_IDENTIFIER = 0;
                startActivity(new Intent(TopUpIntermediateScreen.this, InternationalTopUp.class));
            }
        }
    };

}
