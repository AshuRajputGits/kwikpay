package com.uk.recharge.kwikpay.kwikvend.payments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.RatingBar;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikvend.adapters.KVReceiptAdapter;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductDetailsMO;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 10/24/2018.
 */

public class KVReceiptScreen extends KPSuperClass {

    private RecyclerView kvReceiptRecyclerView;
    private String transactionFee = "";
    private Double totalAmount = 0.0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createMenuDrawer(this);
        setHeaderScreenName("Your Receipt");
        settingResourceIds();

        ArrayList<String> receivedStatusList = null;
        String kvOrderTransactionId = "";

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("DISPENSE_STATUS_LIST"))
                receivedStatusList = bundle.getStringArrayList("DISPENSE_STATUS_LIST");
            if (bundle.containsKey("KV_ORDER_TXN_ID"))
                kvOrderTransactionId = bundle.getString("KV_ORDER_TXN_ID");
        }
        callKVReceiptAPI(receivedStatusList, kvOrderTransactionId);
    }

    private void settingResourceIds() {

        kvReceiptRecyclerView = findViewById(R.id.kvReceiptRV);
        ((RatingBar) findViewById(R.id.ratingBarId)).setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.uk.recharge.kwikpay")));
                } catch (Exception e) {
                    ARCustomToast.showToast(KVReceiptScreen.this, "Google play store application not found or updated", Toast.LENGTH_LONG);
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.kv_receipt_screen;
    }

    private void callKVReceiptAPI(ArrayList<String> receivedStatusList, String kvOrderTransactionId) {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            json.put("APISERVICE", "KV_RECEIPT");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("USERID", sharedPreferences.getString("KP_USER_ID", "0"));
            json.put("DISPENSED_DATA", TextUtils.join(",", receivedStatusList));
            json.put("ORDERID", kvOrderTransactionId);

        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {

                                    Object genericObject = childJson.get("TRANSACTIONLIST");
                                    JSONArray jsonArray = null;

                                    if (genericObject instanceof JSONObject) {
                                        final JSONObject jsonOBJECT = (JSONObject) genericObject;
                                        jsonArray = new JSONArray();
                                        jsonArray.put(jsonOBJECT);
                                    } else if (genericObject instanceof JSONArray)
                                        jsonArray = (JSONArray) genericObject;

                                    transactionFee = childJson.optString("SERVICEFEE");

                                    //Clearing previous selected product list with list receiving from API
                                    KVProductDetailsSingleton.getInstance().clearKVProductList();
                                    ArrayList<KVProductDetailsMO> kvProductDetailsMOList = new ArrayList<>();

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject innerJObject = jsonArray.getJSONObject(i);
                                            KVProductDetailsMO transactionMO = new KVProductDetailsMO();

                                            transactionMO.setProductName(innerJObject.getString("PRODUCT_NAME"));
                                            transactionMO.setProductId(innerJObject.getString("PRODUCT_CODE"));
                                            transactionMO.setProductCorePrice(innerJObject.getString("DISPLAYAMOUNT"));
                                            transactionMO.setProductPurchaseCount(innerJObject.optString("PRODUCT_PURCHASE_COUNT"));
                                            transactionMO.setProductDispensedCount(innerJObject.optString("PRODUCT_DISPENSED_COUNT"));
                                            transactionMO.setPaymentCurrencyCode(innerJObject.optString("PAYMENTCURRENCYCODE"));
                                            kvProductDetailsMOList.add(transactionMO);

                                            calculateTotalAmount(innerJObject.getString("DISPLAYAMOUNT"));
                                        }
                                        //Creating RV and updating UI w.r.t Adapter
                                        updateRVAdapterForReceipt(kvProductDetailsMOList);
                                    }
                                } else {
                                    ARCustomToast.showToast(KVReceiptScreen.this, childJson.optString("DESCRIPTION"));
                                }
                            } else {
                                ARCustomToast.showToast(KVReceiptScreen.this, childJson.optString("DESCRIPTION"));
                            }
                        }

                    } catch (Exception e) {
                        ARCustomToast.showToast(KVReceiptScreen.this, "Invalid response, parsing error!!!");
                    }
                } else {
                    if (Utility.isInternetAvailable(KVReceiptScreen.this))
                        ARCustomToast.showToast(KVReceiptScreen.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(KVReceiptScreen.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (Utility.isInternetAvailable(KVReceiptScreen.this))
                    ARCustomToast.showToast(KVReceiptScreen.this, getResources().getString(R.string.common_ServerConnection));
                else
                    ARCustomToast.showToast(KVReceiptScreen.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    private void updateRVAdapterForReceipt(ArrayList<KVProductDetailsMO> kvProductDetailsMOList) {
        if (kvProductDetailsMOList != null && kvProductDetailsMOList.size() > 0) {

            //Adding additional model objects w.r.t "Transaction Fee"
            KVProductDetailsMO transactionFeeMO = new KVProductDetailsMO();
            transactionFeeMO.setProductName("Transaction Fee");
            transactionFeeMO.setProductCorePrice(Utility.fromHtml(kvProductDetailsMOList.get(0).getPaymentCurrencyCode()) + transactionFee);
            transactionFeeMO.setModelType("KVReceiptPlainMO");
            kvProductDetailsMOList.add(transactionFeeMO);

            //Adding additional model objects w.r.t "Total Amount Charged"
            Double finalTotalAmount = Double.parseDouble(transactionFee) + totalAmount;
            KVProductDetailsMO totalAmountMO = new KVProductDetailsMO();
            totalAmountMO.setProductName("Total Amount Charged");
            totalAmountMO.setProductCorePrice(Utility.fromHtml(kvProductDetailsMOList.get(0).getPaymentCurrencyCode()) + "" + finalTotalAmount);
            totalAmountMO.setModelType("KVReceiptPlainMO");
            kvProductDetailsMOList.add(totalAmountMO);

            //Adding additional model objects w.r.t "Total Refunded"
            KVProductDetailsMO totalRefundedAmountMO = new KVProductDetailsMO();
            totalRefundedAmountMO.setProductName("Total Refunded");
            totalRefundedAmountMO.setProductCorePrice(Utility.fromHtml(kvProductDetailsMOList.get(0).getPaymentCurrencyCode()) + "0.0");
            totalRefundedAmountMO.setModelType("KVReceiptPlainMO");
            kvProductDetailsMOList.add(totalRefundedAmountMO);

            //--------------End--------------//

            kvReceiptRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            kvReceiptRecyclerView.setAdapter(new KVReceiptAdapter(this, kvProductDetailsMOList));
        }
    }

    @Override
    public void onBackPressed() {
    }

    private void calculateTotalAmount(String amount) {
        try {
            if (amount != null && !amount.trim().equals("")) {
                Double convertedAmount = Double.parseDouble(amount);
                totalAmount = totalAmount + convertedAmount;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }
}
