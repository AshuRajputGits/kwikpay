package com.uk.recharge.kwikpay.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Html;
import android.text.Spanned;

import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.KPApplication;

import java.util.UUID;

/**
 * Created by Ashu Rajput on 11/3/2017.
 */

public class Utility {

    private ProgressDialog progressDialog = null;

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static boolean isInternetAvailable(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        } catch (Exception e) {
        }
        return true;
    }

    public void showProgressDialog(Context context, String message) {
        try {
            if (context != null)
                progressDialog = new ProgressDialog(context);
            else
                progressDialog = new ProgressDialog(KPApplication.getContext());

            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showProgressDialog(Context context) {
        try {
            if (context != null)
                progressDialog = new ProgressDialog(context);
            else
                progressDialog = new ProgressDialog(KPApplication.getContext());

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    public static float ParseFloat(String value) {
        float floatVal = 0;
        String number;
        if (value != null && !value.isEmpty()) {
            number = value.replace(",", "").trim();

            try {
                floatVal = Float.valueOf(number);
            } catch (NumberFormatException | NullPointerException e) {
                floatVal = 0;
            }
        }
        return floatVal;
    }

    public static int ParseInteger(String value) {
        int intVal = 0;
        String number;
        if (value != null && !value.isEmpty()) {
            number = value.replace(",", "").trim();

            try {
                intVal = Integer.valueOf(number);
            } catch (NumberFormatException | NullPointerException e) {
                intVal = 0;
            }
        }
        return intVal;
    }

    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }
}
