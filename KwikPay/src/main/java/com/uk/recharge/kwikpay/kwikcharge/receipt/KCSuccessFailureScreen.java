package com.uk.recharge.kwikpay.kwikcharge.receipt;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog.LocationIdListener;
import com.uk.recharge.kwikpay.kwikcharge.kccharger.EBInterimPage;
import com.uk.recharge.kwikpay.kwikcharge.kccharger.KCChargeStepProcessScreen;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ashu Rajput on 10/9/2017.
 */

public class KCSuccessFailureScreen extends AppCompatActivity implements AsyncRequestListenerViaPost, LocationIdListener {

    private String receivedOrderId = "", timerReceivedFromAPI = "0";
    private SharedPreferences preference;
    private TextView successConnectChargerMsg;
    private ImageView receiptConnectorImages;
    private LocationIdMO detailsMO = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kc_success_failure_screen);

        successConnectChargerMsg = findViewById(R.id.successConnectChargerMsg);
        receiptConnectorImages = findViewById(R.id.receiptConnectorImages);

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if (bundle.containsKey("PG_APP_TXN_ID_KEY")) {
                receivedOrderId = bundle.getString("PG_APP_TXN_ID_KEY");
//                new LoadResponseViaPost(KCSuccessFailureScreen.this, formReceiptJSON(), true).execute("");
            }
            if (bundle.containsKey("IS_PAYMENT_SUCCESS")) {
                boolean isPaymentSuccess = bundle.getBoolean("IS_PAYMENT_SUCCESS");
                if (isPaymentSuccess) {
                    findViewById(R.id.kcReceiptSuccessLayout).setVisibility(View.VISIBLE);

                    updateReceiptUI();
                    //GETTING DETAILS FROM THE MODEL CLASS AND SETTING THAT ON PREFERENCE TEMPORARY
                    setInformationDetailsInPrefs();
                } else
                    findViewById(R.id.kcReceiptFailureLayout).setVisibility(View.VISIBLE);
            }
        }

        findViewById(R.id.kcReceiptSuccessButton).setOnClickListener(onClickListener);
        findViewById(R.id.kcReceiptRetryButton).setOnClickListener(onClickListener);
        findViewById(R.id.headerHomeBtn).setOnClickListener(onClickListener);
    }

    private void setInformationDetailsInPrefs() {
        try {
            detailsMO = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();
            SharedPreferences.Editor editor = preference.edit();
            editor.putString("TEMP_LOCATION_NUMBER", detailsMO.getLocationNumber());
            editor.putString("TEMP_DEVICE_DB_ID", detailsMO.getTempDeviceDBID());
            editor.putString("TEMP_LOCAL_CONNECTOR_NUMBER", detailsMO.getTempLocalConnectorNumber());
            editor.putString("TEMP_APP_TXN_ID", receivedOrderId);
            editor.apply();
        } catch (Exception e) {
        }
    }

    private void updateReceiptUI() {
        try {
            LocationIdMO receivedModel = KwikChargeDetailsSingleton.getInstance().getLocationIdMO();
            if (receivedModel != null) {
                StringBuilder messageBuilder = new StringBuilder();
                messageBuilder.append("Please connect ");
                messageBuilder.append(receivedModel.getSelectedConnectorName());

                try {
                    Glide.with(this).load(receivedModel.getSelectedConnectorURL()).into(receiptConnectorImages);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*if (receivedModel.getConnectorImageType().equals("TYPE2")) {
                    messageBuilder.append("Type 2");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        receiptConnectorImages.setImageDrawable(getResources().getDrawable(R.drawable.type2_icon_big, getApplicationContext().getTheme()));
                    else
                        receiptConnectorImages.setImageDrawable(getResources().getDrawable(R.drawable.type2_icon_big));
                } else if (receivedModel.getConnectorImageType().equals("CCS")) {
                    messageBuilder.append("CCS");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        receiptConnectorImages.setImageDrawable(getResources().getDrawable(R.drawable.ccs_icon_big, getApplicationContext().getTheme()));
                    else
                        receiptConnectorImages.setImageDrawable(getResources().getDrawable(R.drawable.ccs_icon_big));
                } else if (receivedModel.getConnectorImageType().equals("CHADEMO")) {
                    messageBuilder.append("CHAdeMO");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        receiptConnectorImages.setImageDrawable(getResources().getDrawable(R.drawable.chademo_icon_big, getApplicationContext().getTheme()));
                    else
                        receiptConnectorImages.setImageDrawable(getResources().getDrawable(R.drawable.chademo_icon_big));
                }*/

                messageBuilder.append(" to vehicle and press \"start charge\" to begin the charging process");
                successConnectChargerMsg.setText(messageBuilder.toString());
            }
        } catch (Exception e) {
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if (viewId == R.id.kcReceiptSuccessButton) {
                if (detailsMO != null) {
                    if (detailsMO.getInterimScreenFlow() != null && !detailsMO.getInterimScreenFlow().isEmpty() && detailsMO.getInterimScreenFlow().equals("1"))
                        startActivity(new Intent(KCSuccessFailureScreen.this, EBInterimPage.class));
                    else
                        callKCStartChargingAPI();
                } else
                    callKCStartChargingAPI();
            } else if (viewId == R.id.kcReceiptRetryButton)
                KCSuccessFailureScreen.this.finish();
            else if (viewId == R.id.headerHomeBtn)
                redirectToHomeScreen();
        }
    };

    private void callKCStartChargingAPI() {
        new LoadResponseViaPost(KCSuccessFailureScreen.this, formKCStartChargingJson(), true).execute("");
    }

    public String formKCStartChargingJson() {
        JSONObject jsonobj = new JSONObject();

        try {
            jsonobj.put("APISERVICE", "KC_START_CHARGE");
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
            jsonobj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("LOCATION_NUMBER", preference.getString("TEMP_LOCATION_NUMBER", ""));
            jsonobj.put("DEVICE_DB_ID", preference.getString("TEMP_DEVICE_DB_ID", ""));
            jsonobj.put("LOCAL_CONNECTOR_NUMBER", preference.getString("TEMP_LOCAL_CONNECTOR_NUMBER", ""));
            jsonobj.put("APP_TXN_ID", preference.getString("TEMP_APP_TXN_ID", ""));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    private void redirectToHomeScreen() {
        Intent intent = new Intent(KCSuccessFailureScreen.this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                parseStartChargingDetails(loadedString);
            } else {
                ARCustomToast.showToast(KCSuccessFailureScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
            ARCustomToast.showToast(KCSuccessFailureScreen.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    private void parseStartChargingDetails(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    timerReceivedFromAPI = childJsonObj.optString("START_STATUS_TIMER_IN_SECONDS");

                    DialogFragment dialog = new CustomDialog();
                    dialog.show(getSupportFragmentManager(), "LocationIdDialog");
                    Bundle bundle = new Bundle();
//                    bundle.putString("Charger_Address", "Please ensure the charger cable is connected to your car now and press \"Proceed  /  Cancel\"");
                    bundle.putString("Charger_Address", "Please ensure the charger cable is connected to your car now");
                    bundle.putBoolean("ShowAddressDialog", true);
                    bundle.putBoolean("ButtonsText", true);
                    dialog.setArguments(bundle);
                    dialog.setCancelable(false);

                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        ARCustomToast.showToast(KCSuccessFailureScreen.this, childJsonObj.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (Exception e) {
            ARCustomToast.showToast(KCSuccessFailureScreen.this, "Parsing exception", Toast.LENGTH_LONG);
        }
    }

    @Override
    public void getLocationId(String locationID) {
        startActivity(new Intent(KCSuccessFailureScreen.this, KCChargeStepProcessScreen.class)
                .putExtra("START_STATUS_TIMER_IN_SECONDS", timerReceivedFromAPI));

//        new LoadResponseViaPost(this, formKCStopChargingJson(), true).execute("");

    }


   /* public String formKCStopChargingJson() {
        JSONObject jsonobj = new JSONObject();
        try {
            jsonobj.put("APISERVICE", "KC_STOP_CHARGE");
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
            jsonobj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("LOCATION_NUMBER", preference.getString("TEMP_LOCATION_NUMBER", ""));
            jsonobj.put("DEVICE_DB_ID", preference.getString("TEMP_DEVICE_DB_ID", ""));
            jsonobj.put("LOCAL_CONNECTOR_NUMBER", preference.getString("TEMP_LOCAL_CONNECTOR_NUMBER", ""));
            jsonobj.put("APP_TXN_ID", preference.getString("TEMP_APP_TXN_ID", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    private void parseStopChargingDetails(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, childJsonObj.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (Exception e) {
            ARCustomToast.showToast(this, "Parsing Exception", Toast.LENGTH_LONG);
        }
    }*/

}
