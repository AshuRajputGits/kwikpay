package com.uk.recharge.kwikpay.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uk.recharge.kwikpay.R;

public class HeaderMenuAdapter extends BaseAdapter {
    Activity context;

    String[] menuClasArray = {"Home", "My Account", "How it works", "Contact Us", "Terms Of usage", "About us", "Privacy Policy"};
    int[] menuIcons = {R.drawable.home_icon, R.drawable.my_account_icon, R.drawable.how_it_work_icon,
            R.drawable.contact_us_icon, R.drawable.tou_icon, R.drawable.about_us_icon, R.drawable.privacy_policy_icon};

    public HeaderMenuAdapter(Activity context) {
        this.context = context;
    }

    public int getCount() {
        return menuClasArray.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView menuNameClassTV;
        ImageView menuClassIV;

        ViewHolder(View view) {
            menuNameClassTV = view.findViewById(R.id.menuClassName);
            menuClassIV = view.findViewById(R.id.menuClassImage);
        }
    }

    @SuppressWarnings("static-access")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_menu_row, parent, false);
            holder = new ViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        holder.menuNameClassTV.setText(menuClasArray[position]);
        holder.menuClassIV.setImageResource(menuIcons[position]);
        return row;
    }
}
