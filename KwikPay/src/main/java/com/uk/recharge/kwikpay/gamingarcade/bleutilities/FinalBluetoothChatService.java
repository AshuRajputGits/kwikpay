package com.uk.recharge.kwikpay.gamingarcade.bleutilities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;

public class FinalBluetoothChatService {
    private UUID MY_UUID;
    private final BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
    private final Handler mHandler;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState = 0;

    public FinalBluetoothChatService(Context context, Handler handler) {
        this.mHandler = handler;
//        this.MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        this.MY_UUID = AwesomenessProfile.SERVICE_UUID;
    }

    private synchronized void setState(int state) {
        Log.d("FinalBluetooth", "setState() " + this.mState + " -> " + state);
        this.mState = state;
        this.mHandler.obtainMessage(1, state, -1).sendToTarget();
    }

    public synchronized int getState() {
        return this.mState;
    }

    public synchronized void start() {
        Log.d("FinalBluetooth", "start(): to cancel mConnectThread");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        this.setState(1);
    }

    public synchronized void connect(BluetoothDevice device) {
        Log.d("FinalBluetooth", "connect to: " + device);
        if (this.mState == 2 && this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }

        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }

        this.mConnectThread = new ConnectThread(device);
        this.mConnectThread.start();
        this.setState(2);
    }

    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        Log.d("FinalBluetooth", "BLE connected, send back this information to UI");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }

//        this.mConnectedThread = new ConnectedThread(socket);
//        this.mConnectedThread.start();
        Message msg = this.mHandler.obtainMessage(4);
        Bundle bundle = new Bundle();
        bundle.putString("device_name", device.getName());
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
        this.setState(3);
    }

    public synchronized void stop() {
        Log.d("FinalBluetooth", "stop");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }

        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }

        this.setState(0);
    }

    public void write(byte[] out) {
        ConnectedThread r;
        synchronized (this) {
            if (this.mState != 3) {
                return;
            }
            r = this.mConnectedThread;
        }
        r.write(out);
    }

    private void connectionFailed() {
        this.setState(1);
        Message msg = this.mHandler.obtainMessage(5);
        Bundle bundle = new Bundle();
        bundle.putString("toast", "Unable to connect device");
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
    }

    private void connectionLost() {
//        VendekinSdk.bledisconnectd = true;
        this.setState(1);
        Message msg = this.mHandler.obtainMessage(5);
        Bundle bundle = new Bundle();
        bundle.putString("toast", "Device connection was lost");
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d("FinalBluetooth", "create ConnectedThread");
            this.mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException var6) {
                Log.e("FinalBluetooth", "temp sockets not created", var6);
            }

            this.mmInStream = tmpIn;
            this.mmOutStream = tmpOut;
        }

        public void run() {
            StringBuilder total = new StringBuilder();
            Log.i("FinalBluetooth", "BEGIN mConnectedThread");
            byte[] buffer = new byte[2048];

            while (true) {
                try {
                    int bytes = this.mmInStream.read(buffer);
                    String string = new String(buffer, 0, bytes);
                    total.append(string);
//                    VendekinSdk.DEX_STRING = total.toString();
                    FinalBluetoothChatService.this.mHandler.obtainMessage(2, bytes, -1, buffer).sendToTarget();
                } catch (IOException var5) {
                    Log.e("FinalBluetooth", "disconnected", var5);
                    FinalBluetoothChatService.this.connectionLost();
                    return;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                Log.e("AT", "Write");
                this.mmOutStream.write(buffer);
                FinalBluetoothChatService.this.mHandler.obtainMessage(3, -1, -1, buffer).sendToTarget();
            } catch (IOException var3) {
                Log.e("FinalBluetooth", "Exception during write", var3);
            }

        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (IOException var2) {
                Log.e("FinalBluetooth", "close() of connect socket failed", var2);
            }
        }
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            this.mmDevice = device;
            BluetoothSocket tmp = null;
            try {
                tmp = device.createRfcommSocketToServiceRecord(FinalBluetoothChatService.this.MY_UUID);
                Method m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
                tmp = (BluetoothSocket) m.invoke(device, 1);
            } catch (Exception var5) {
                Log.e("FinalBluetooth", "create() failed", var5);
            }

            this.mmSocket = tmp;
        }

        public void run() {
            Log.i("FinalBluetooth", "BEGIN mConnectThread");
            this.setName("ConnectThread");
            FinalBluetoothChatService.this.mAdapter.cancelDiscovery();

            try {
                this.mmSocket.connect();
            } catch (IOException var6) {

                Log.i("FinalBluetooth", "Exception aa gaya boss!!");
                var6.printStackTrace();
                FinalBluetoothChatService.this.connectionFailed();

                try {
                    this.mmSocket.close();
                } catch (IOException var4) {
                    var4.printStackTrace();
                    Log.e("FinalBluetooth", "unable to close() socket during connection failure", var4);
                }

                FinalBluetoothChatService.this.start();
                return;
            }

            FinalBluetoothChatService var1 = FinalBluetoothChatService.this;
            synchronized (FinalBluetoothChatService.this) {
                FinalBluetoothChatService.this.mConnectThread = null;
            }

            FinalBluetoothChatService.this.connected(this.mmSocket, this.mmDevice);
        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (IOException var2) {
                Log.e("FinalBluetooth", "close() of connect socket failed", var2);
            }
        }
    }
}
