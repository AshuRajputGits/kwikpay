package com.uk.recharge.kwikpay.myaccount.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.myaccount.MyTransactionSingleton;
import com.uk.recharge.kwikpay.myaccount.MyTransactionViewDetails;
import com.uk.recharge.kwikpay.myaccount.MyTransactionsMO;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 22-12-2017.
 */

public class AllTransactionsAdapter extends RecyclerView.Adapter<AllTransactionsAdapter.AllTransactionViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<MyTransactionsMO> allTransactionList = null;
    private Context context;

    public AllTransactionsAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        allTransactionList = MyTransactionSingleton.getInstance().getAllTransactionList();
        this.context = context;
    }

    @Override
    public AllTransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.my_transactions_row, parent, false);
        return new AllTransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AllTransactionViewHolder holder, int position) {
        holder.transactionRCAmount.setText(Utility.fromHtml(allTransactionList.get(position).getTransactionDisplayCurrencyCode())
                + allTransactionList.get(position).getTransactionDisplayAmount());
        if (allTransactionList.get(position).getTransactionPaymentStatus().equalsIgnoreCase("SUCCESSFUL")) {
            holder.transactionRechargeStatus.setText(allTransactionList.get(position).getTransactionRechargeStatus());
        } else {
            holder.transactionRechargeStatus.setText(allTransactionList.get(position).getTransactionPaymentStatus());
            holder.transactionRechargeStatus.setTextColor(Color.RED);
        }


//        if (allTransactionList.get(position).getTransactionRechargeStatus().contains("Failed"))
//        holder.transactionRechargeStatus.setTextColor(Color.RED);
        holder.transactionRechargeType.setText(allTransactionList.get(position).getTransactionServiceName());
        holder.transactionDateTime.setText(allTransactionList.get(position).getTransactionDateTime());
        holder.transactionOrderID.setText("Order No " + allTransactionList.get(position).getTransactionOrderId());

        try {
            Glide.with(context).load(allTransactionList.get(position).getImageUrl())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(holder.operatorImages);
//            Glide.with(context).load("http://46.37.168.7:8080/kwikpaybeta/kwikpay/apphtml/myaccount/vodafone.png").into(holder.operatorImages);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (allTransactionList != null && allTransactionList.size() > 0)
            return allTransactionList.size();
        return 0;
    }

    public class AllTransactionViewHolder extends RecyclerView.ViewHolder {

        TextView transactionRCAmount;
        TextView transactionRechargeStatus;
        TextView transactionRechargeType;
        TextView transactionDateTime;
        TextView transactionOrderID;
        ImageView operatorImages;

        public AllTransactionViewHolder(View itemView) {
            super(itemView);
            transactionRCAmount = itemView.findViewById(R.id.transactionRCAmount);
            transactionRechargeStatus = itemView.findViewById(R.id.transactionRechargeStatus);
            transactionRechargeType = itemView.findViewById(R.id.transactionRechargeType);
            transactionDateTime = itemView.findViewById(R.id.transactionDateTime);
            transactionOrderID = itemView.findViewById(R.id.transactionOrderID);
            operatorImages = itemView.findViewById(R.id.operatorImages);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, MyTransactionViewDetails.class)
                            .putExtra("SELECTED_TRANSACTION", allTransactionList.get(getLayoutPosition())));
                }
            });
        }
    }

}
