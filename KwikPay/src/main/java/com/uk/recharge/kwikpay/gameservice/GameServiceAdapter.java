package com.uk.recharge.kwikpay.gameservice;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ashu Rajput on 1/16/2018.
 */

public class GameServiceAdapter extends RecyclerView.Adapter<GameServiceAdapter.GameServiceViewHolder> {

    private ArrayList<HashMap<String, String>> serviceArrayList = null;
    private LayoutInflater layoutInflater = null;
    private Context context;
    private GameServiceItemListener listener;
    private boolean isGameProductScreen = false;

    public GameServiceAdapter(Context context, ArrayList<HashMap<String, String>> serviceArrayList) {
        layoutInflater = LayoutInflater.from(context);
        this.serviceArrayList = serviceArrayList;
        this.context = context;
        this.listener = (GameServiceItemListener) context;
    }

    public GameServiceAdapter(Context context, ArrayList<HashMap<String, String>> serviceArrayList, boolean isGameProductScreen) {
        layoutInflater = LayoutInflater.from(context);
        this.serviceArrayList = serviceArrayList;
        this.context = context;
        this.listener = (GameServiceItemListener) context;
        this.isGameProductScreen = isGameProductScreen;
    }

    @Override
    public GameServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.game_service_row, parent, false);
        return new GameServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GameServiceViewHolder holder, int position) {

        try {
            Glide.with(context)
                    .load(serviceArrayList.get(position).get("OPIMGPATH"))
                    .placeholder(R.drawable.default_operator_logo)
                    .into(holder.gameServiceLogo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isGameProductScreen) {

            String productAmount = serviceArrayList.get(position).get(ProjectConstant.TXN_DISPLAYCURRENCYCODE) +
                    serviceArrayList.get(position).get(ProjectConstant.TOPUP_API_DISPLAYAMT);
            holder.gameServiceDescription.setText(Utility.fromHtml("<b><font color='#262626'>" + productAmount + "</b>" + "<br />" +
                    "<small><small><font color='#262626'>" + serviceArrayList.get(position).get(ProjectConstant.PAYNOW_PLANDESCRIPTION) + "</small></small>"));

        } else {
            holder.gameServiceDescription.setText(Utility.fromHtml("<b><font color='#262626'>" +
                    serviceArrayList.get(position).get(ProjectConstant.API_OPNAME) + "</b>" + "<br />" +
                    "<small><small><font color='#262626'>" + serviceArrayList.get(position).get("OP_DESC") + "</small></small>"));
        }

    }

    @Override
    public int getItemCount() {
        return serviceArrayList.size();
    }

    public class GameServiceViewHolder extends RecyclerView.ViewHolder {

        private ImageView gameServiceLogo;
        private TextView gameServiceDescription;

        public GameServiceViewHolder(View itemView) {
            super(itemView);

            gameServiceLogo = itemView.findViewById(R.id.gameServiceLogo);
            gameServiceDescription = itemView.findViewById(R.id.gameServiceDescription);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRowItemSelection(getLayoutPosition(), 1);
                }
            });
        }
    }

    public interface GameServiceItemListener {
        void onRowItemSelection(int position, int type);
    }

}
