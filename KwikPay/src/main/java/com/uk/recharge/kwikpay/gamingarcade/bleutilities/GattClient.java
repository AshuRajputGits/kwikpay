package com.uk.recharge.kwikpay.gamingarcade.bleutilities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import static android.content.Context.BLUETOOTH_SERVICE;
import static com.uk.recharge.kwikpay.gamingarcade.bleutilities.AwesomenessProfile.CHARACTERISTIC_COUNTER_UUID;
import static com.uk.recharge.kwikpay.gamingarcade.bleutilities.AwesomenessProfile.CHARACTERISTIC_INTERACTOR_UUID;
import static com.uk.recharge.kwikpay.gamingarcade.bleutilities.AwesomenessProfile.DESCRIPTOR_CONFIG;
import static com.uk.recharge.kwikpay.gamingarcade.bleutilities.AwesomenessProfile.SERVICE_UUID;

public class GattClient {

    private static final String TAG = GattClient.class.getSimpleName();

    public interface OnCounterReadListener {
        void onCounterRead(int value);

        void onConnected(boolean success);

        void onCharacterResponseRead(String response);
    }

    private Context mContext;
    private OnCounterReadListener mListener;
    private String mDeviceAddress;

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;

    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "Connected to GATT client. Attempting to start service discovery");
                gatt.discoverServices();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "Disconnected from GATT client");
                mListener.onConnected(false);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                boolean connected = false;

                BluetoothGattService service = gatt.getService(SERVICE_UUID);
                if (service != null) {

                    Log.e(TAG, "Connected onServicesDiscovered() 1111111 ");

                    BluetoothGattCharacteristic characteristic = service.getCharacteristic(CHARACTERISTIC_COUNTER_UUID);
                    if (characteristic != null) {
                        Log.e(TAG, "Connected onServicesDiscovered() 22222 ");

                        gatt.setCharacteristicNotification(characteristic, true);
                        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(DESCRIPTOR_CONFIG);
                        if (descriptor != null) {
                            Log.e(TAG, "Connected onServicesDiscovered() 33333 ");

                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            connected = gatt.writeDescriptor(descriptor);
                        }
                    }
                }
                mListener.onConnected(connected);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            readCounterCharacteristic(characteristic);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            readCounterCharacteristic(characteristic);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {

            Log.w(TAG, "onDescriptorWrite Area('') ");

            if (DESCRIPTOR_CONFIG.equals(descriptor.getUuid())) {
                BluetoothGattCharacteristic characteristic = gatt.getService(SERVICE_UUID).getCharacteristic(CHARACTERISTIC_COUNTER_UUID);
                gatt.readCharacteristic(characteristic);
            }
        }

        private void readCounterCharacteristic(BluetoothGattCharacteristic characteristic) {

            Log.e(TAG, "readCounterCharacteristic Area('') " + characteristic.getUuid());

            if (CHARACTERISTIC_COUNTER_UUID.equals(characteristic.getUuid())) {
                byte[] data = characteristic.getValue();

                String returnedValue = new String(data);
                Log.e(TAG, "Reader" + returnedValue);

//                int value = fromByteArray(data);
//                mListener.onCounterRead(value);
                mListener.onCharacterResponseRead(returnedValue);
            }
        }
    };

    private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF);

            switch (state) {
                case BluetoothAdapter.STATE_ON:
                    startClient();
                    break;
                case BluetoothAdapter.STATE_OFF:
                    stopClient();
                    break;
                default:
                    // Do nothing
                    break;
            }
        }
    };

    public void onCreate(Context context, String deviceAddress, OnCounterReadListener listener) throws RuntimeException {
        mContext = context;
        mListener = listener;
        mDeviceAddress = deviceAddress;

        mBluetoothManager = (BluetoothManager) context.getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (!checkBluetoothSupport(mBluetoothAdapter)) {
            throw new RuntimeException("GATT client requires Bluetooth support");
        }

        // Register for system Bluetooth events
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        mContext.registerReceiver(mBluetoothReceiver, filter);
        if (!mBluetoothAdapter.isEnabled()) {
            Log.w(TAG, "Bluetooth is currently disabled... enabling");
            mBluetoothAdapter.enable();
        } else {
            Log.i(TAG, "Bluetooth enabled... starting client");
            startClient();
        }


        //Register for automatic pairing
        // Actually set it in response to ACTION_PAIRING_REQUEST.
        final IntentFilter pairingRequestFilter = new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST);
        pairingRequestFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY - 1);
        mContext.getApplicationContext().registerReceiver(mPairingRequestRecevier, pairingRequestFilter);
    }


    private final BroadcastReceiver mPairingRequestRecevier = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent == null) {
                Log.w("Unexpected ", "Coming with intent as nulll");
                return;
            }

            if (BluetoothDevice.ACTION_PAIRING_REQUEST.equals(intent.getAction())) {

                Log.w("Unexpected ", "Coming for something...mPairingRequestRecevier");

                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                int type = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_VARIANT, BluetoothDevice.ERROR);

                if (type == BluetoothDevice.PAIRING_VARIANT_PIN) {
                    //IMP Lines below
                    Log.w("Unexpected ", " pairing for coming");
                    device.setPin("0104".getBytes());
                    abortBroadcast();
                } else {
                    Log.w("Unexpected ", " pairing type:" + type);
                }
            }
        }
    };

    public void onDestroy() {
        mListener = null;
        BluetoothAdapter bluetoothAdapter = mBluetoothManager.getAdapter();
        if (bluetoothAdapter.isEnabled()) {
            stopClient();
        }

        try {
            mContext.unregisterReceiver(mBluetoothReceiver);
            if (mPairingRequestRecevier != null)
                mContext.unregisterReceiver(mPairingRequestRecevier);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeInteractor(String writableDataToBle) {
        BluetoothGattCharacteristic interactor = mBluetoothGatt
                .getService(SERVICE_UUID)
                .getCharacteristic(CHARACTERISTIC_INTERACTOR_UUID);
        interactor.setValue(writableDataToBle);
        mBluetoothGatt.writeCharacteristic(interactor);
    }

    public String readData() {
        BluetoothGattCharacteristic reader = mBluetoothGatt
                .getService(SERVICE_UUID)
                .getCharacteristic(CHARACTERISTIC_INTERACTOR_UUID);
        byte[] readedValue = reader.getValue();
        String returnedValue = new String(readedValue);
        Log.e(TAG, "Reader" + returnedValue);
        return returnedValue;
    }

    private boolean checkBluetoothSupport(BluetoothAdapter bluetoothAdapter) {
        if (bluetoothAdapter == null) {
            Log.w(TAG, "Bluetooth is not supported");
            return false;
        }

        if (!mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.w(TAG, "Bluetooth LE is not supported");
            return false;
        }

        return true;
    }

    private void startClient() {
        if (mBluetoothAdapter != null) {
            Log.w(TAG, "About to <----> create GATT client: MacAddress" + mDeviceAddress);
            BluetoothDevice bluetoothDevice = mBluetoothAdapter.getRemoteDevice(mDeviceAddress);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mBluetoothGatt = bluetoothDevice.connectGatt(mContext, true, mGattCallback, 2);
//                defineCharAndDescrUUIDs();
            } else {
                mBluetoothGatt = bluetoothDevice.connectGatt(mContext, false, mGattCallback);
            }
        }

        if (mBluetoothGatt == null) {
            Log.w(TAG, "Unable to create GATT client");
            return;
        }
    }

    private void stopClient() {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }

        if (mBluetoothAdapter != null) {
            mBluetoothAdapter = null;
        }
    }

    public static int fromByteArray(byte[] bytes) {
        return fromBytes(bytes[0], bytes[1], bytes[2], bytes[3]);
    }

    private static int fromBytes(byte b1, byte b2, byte b3, byte b4) {
        return b1 << 24 | (b2 & 0xFF) << 16 | (b3 & 0xFF) << 8 | (b4 & 0xFF);
    }

    // Extra logical values to identify characteristic and descriptors
    /*List<UUID> serviceUUIDsList = new ArrayList<>();
    List<UUID> characteristicUUIDsList = new ArrayList<>();
    List<UUID> descriptorUUIDsList = new ArrayList<>();

    private void defineCharAndDescrUUIDs() {
        List<BluetoothGattService> servicesList = mBluetoothGatt.getServices();

        for (int i = 0; i < servicesList.size(); i++) {
            BluetoothGattService bluetoothGattService = servicesList.get(i);

            if (serviceUUIDsList.contains(bluetoothGattService.getUuid())) {
                List<BluetoothGattCharacteristic> bluetoothGattCharacteristicList = bluetoothGattService.getCharacteristics();

                for (BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattCharacteristicList) {
                    characteristicUUIDsList.add(bluetoothGattCharacteristic.getUuid());
                    List<BluetoothGattDescriptor> bluetoothGattDescriptorsList = bluetoothGattCharacteristic.getDescriptors();

                    for (BluetoothGattDescriptor bluetoothGattDescriptor : bluetoothGattDescriptorsList) {
                        descriptorUUIDsList.add(bluetoothGattDescriptor.getUuid());
                    }
                }
            }
        }
        Log.e(TAG, "defineArea: *** " + serviceUUIDsList.toString());
        Log.e(TAG, "defineArea: ^^^" + characteristicUUIDsList.toString());
        Log.e(TAG, "defineArea: $$$" + descriptorUUIDsList.toString());
    }*/


}
