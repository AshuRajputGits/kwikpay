package com.uk.recharge.kwikpay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.adapters.MyAccountAdapter;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyTransaction extends Activity implements OnClickListener, AsyncRequestListenerViaPost {
    private ListView myTransitionListView;
    private MyAccountAdapter myTransitionAdapter;
    private ArrayList<HashMap<String, String>> myTransitionArrayList;
    private HashMap<String, String> myTransitionHashMap, selectedTxnHashMap, repeatRechargeHashMap;
    private SharedPreferences preference;
    private boolean isRepeatButtonClicked = false;
    private JSONArray txnJsonArray;
    private String selectedOrderId = "", selectedDisplayAmount = "", selectedRechargeAmount = "";
    //	private String  selectedServiceProvider ="",selectedDOT="",selectedRechargeStatus="";
    private String selectedRechargeStatus = "";
    private int apiHitPosition = 0;
    private TextView footerMyProfile, footerMyFavorites;

//	private boolean isRadioOn=false
//	private TextView viewPrintDetailTV;
//	private LinearLayout myTransitionLinearLayout;
//	TextView headerProfileBtn,headerChangePWBtn;
//	private View separatorLine;
//	private TextView headerBelow_ClassTV,headerFavBtn,headerTicketBtn,viewPrintDetailTV,cartItemCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.mytransitions);

        settingIDs();

        apiHitPosition = 1;
        new LoadResponseViaPost(MyTransaction.this, formmyTransitionJSON(), true).execute("");

        myTransitionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedTxnHashMap = (HashMap<String, String>) myTransitionArrayList.get(position);
                selectedOrderId = selectedTxnHashMap.get(ProjectConstant.TXN_ORDERID);
                selectedDisplayAmount = selectedTxnHashMap.get(ProjectConstant.TXN_DISPLAYAMOUNT);
                selectedRechargeAmount = selectedTxnHashMap.get(ProjectConstant.TXN_RECHARGEAMOUNT);
                selectedRechargeStatus = selectedTxnHashMap.get(ProjectConstant.TXN_RECHARGESTATUS);
//				selectedServiceProvider=selectedTxnHashMap.get(ProjectConstant.TXN_OPERATORNAME);
//				selectedDOT=selectedTxnHashMap.get(ProjectConstant.TXN_TRANSACTIONDATETIME);

                showPopUpMenu();
            }
        });

		/*viewPrintDetailTV.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				if(isRadioOn)
				{
					Intent payNowIntent=new Intent(MyTransaction.this,ViewDetailScreen.class);
					payNowIntent.putExtra("TRANSACTION_VIEW_DETAIL", selectedTxnHashMap);
					startActivity(payNowIntent);	
				}
				else
				{
					ARCustomToast.showToast(MyTransaction.this,"please select a transaction to view details", Toast.LENGTH_LONG);
				}
			}
		});*/

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_MY_ACCOUNT));
            /*String eventName = getResources().getString(R.string.SCREEN_NAME_MY_ACCOUNT);
            AppsFlyerLib.trackEvent(this, eventName, null);
            EventsLoggerSingleton.getInstance().setFBLogEvent(eventName);
            ((KPApplication) getApplication()).setGoogleAnalyticsScreenName(eventName);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPopUpMenu() {
        final CharSequence[] items = {"View Details", "Repeat", "Set as Favorites", "Raise Issue", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {

                Intent menuIntent = null;

                if (position == 0) {
                    menuIntent = new Intent(MyTransaction.this, ViewDetailScreen.class);
                    menuIntent.putExtra("TRANSACTION_VIEW_DETAIL", selectedTxnHashMap);
                    menuIntent.putExtra("RECEIPT_TYPE", 0);
                    startActivity(menuIntent);
                } else if (position == 1) {
                    isRepeatButtonClicked = true;
                    apiHitPosition = 2;
                    new LoadResponseViaPost(MyTransaction.this, formRepeatRechargeJSON(), true).execute("");
                } else if (position == 2) {
                    if (!selectedRechargeStatus.equalsIgnoreCase("SUCCESSFUL"))
                        ARCustomToast.showToast(MyTransaction.this, "please select a successful transaction to set as favorite", Toast.LENGTH_LONG);
                    else {
                        menuIntent = new Intent(MyTransaction.this, SetFavourite.class);
                        menuIntent.putExtra("MYAC_FAV_HASHMAP", selectedTxnHashMap);
                        startActivity(menuIntent);
                    }
                } else if (position == 3) {
                    Intent raiseTicketIntent = new Intent(MyTransaction.this, RaiseTicket.class);
                    raiseTicketIntent.putExtra("RAISETICKET_ORDERID", selectedTxnHashMap.get(ProjectConstant.TXN_ORDERID));
                    raiseTicketIntent.putExtra("RAISETICKET_SERVICE_PROVIDER", selectedTxnHashMap.get(ProjectConstant.TXN_OPERATORNAME));
                    raiseTicketIntent.putExtra("RAISETICKET_DOT", selectedTxnHashMap.get(ProjectConstant.TXN_TRANSACTIONDATETIME));

                    startActivity(raiseTicketIntent);
                } else if (position == 4) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

    private void settingIDs() {
        // TODO Auto-generated method stub

        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        myTransitionListView = (ListView) findViewById(R.id.myTransitionListView);
        myTransitionArrayList = new ArrayList<HashMap<String, String>>();
        footerMyProfile = (TextView) findViewById(R.id.footerEditProfileBtn);
        footerMyFavorites = (TextView) findViewById(R.id.footerFavoriteBtn);

        footerMyProfile.setOnClickListener(this);
        footerMyFavorites.setOnClickListener(this);

        TextView headerTitle = (TextView) findViewById(R.id.mainHeaderTitleName);
        headerTitle.setText("My top-ups");

        TextView myTransactionButton = (TextView) findViewById(R.id.footerMyTxnBtn);
        myTransactionButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_transaction_icon, 0, 0);
        myTransactionButton.setTextColor(getResources().getColor(R.color.darkGreen));

        //	myProfileTV=(TextView)findViewById(R.id.myAccount_MyProfile);

//		headerFavBtn=(TextView)findViewById(R.id.header_FavouriteBtn);
//		headerProfileBtn=(TextView)findViewById(R.id.header_MyProfileBtn);
//		headerChangePWBtn=(TextView)findViewById(R.id.header_ChangePWBtn);
//		headerTicketBtn=(TextView)findViewById(R.id.header_MyTicketBtn);

//		separatorLine=(View)findViewById(R.id.headerHomeMenuSeparator);
//		separatorLine.setVisibility(View.GONE);

//		cartItemCounter=(TextView)findViewById(R.id.cartItemCounter);

//		myTransitionLinearLayout=(LinearLayout)findViewById(R.id.myTransitionHeaderLayout);
//		myTransitionLinearLayout.setVisibility(View.VISIBLE);

//		viewPrintDetailTV=(TextView)findViewById(R.id.myTransition_ViewDetail);

        //SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

        ImageView homeButtonImage = ((ImageView) findViewById(R.id.headerHomeBtn));
        ImageView headerMenuImage = (ImageView) findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);
//		LinearLayout cartIconLayout=(LinearLayout)findViewById(R.id.cartImageLayout);
//		cartIconLayout.setVisibility(View.GONE);

        CustomSlidingDrawer csd = new CustomSlidingDrawer(MyTransaction.this, mDrawerLayout,
                mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        //END OF SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

//		headerFavBtn.setOnClickListener(this);
//		headerProfileBtn.setOnClickListener(this);
//		headerChangePWBtn.setOnClickListener(this);
//		headerTicketBtn.setOnClickListener(this);


        // OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME
        /*DBQueryMethods database=new DBQueryMethods(MyTransaction.this);
        try
		{
			database.open();
			headerBelow_ClassTV=(TextView)findViewById(R.id.headerBelow_ClassNameTV);
			headerBelow_ClassTV.setText(database.getSubHeaderTitles("My Transaction"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}*/

    }

    public void myTransitionOnClickMethod(View accountView) {
        switch (accountView.getId()) {

		/*
		case R.id.myTransition_ViewDetail:

			if(isRadioOn)
			{
				Intent payNowIntent=new Intent(MyTransitions.this,ViewDetailScreen.class);
				payNowIntent.putExtra("TRANSACTION_VIEW_DETAIL", selectedTxnHashMap);
				startActivity(payNowIntent);	
			}
			else
			{
				ARCustomToast.showToast(MyTransitions.this,"please select a transaction to view details", Toast.LENGTH_LONG);
			}

			break;
		case R.id.myTransition_RaiseTicket:

			if(isRadioOn)
			{
				Intent raiseTicketIntent = new Intent(MyTransitions.this,RaiseTicket.class);
				raiseTicketIntent.putExtra("RAISETICKET_ORDERID", selectedOrderId);
				raiseTicketIntent.putExtra("RAISETICKET_SERVICE_PROVIDER", selectedServiceProvider);
				raiseTicketIntent.putExtra("RAISETICKET_DOT", selectedDOT);

				startActivity(raiseTicketIntent);	
			}
			else
			{
				ARCustomToast.showToast(MyTransitions.this,"please select a transaction to proceed", Toast.LENGTH_LONG);
			}

			break;*/
			
		/*case R.id.myTransition_Repeat:

			if(isRadioOn)
			{
				isRepeatButtonClicked=true;
				apiHitPosition=2;
				new LoadResponseViaPost(MyTransaction.this, formRepeatRechargeJSON(), true).execute("");
			}
			else
				ARCustomToast.showToast(MyTransaction.this,"please select a valid transaction to Repeat the same", Toast.LENGTH_LONG);

			break;
		case R.id.myTransition_SetFav:

			if(isRadioOn)
			{
				if(!selectedRechargeStatus.equals("SUCCESSFUL"))
					ARCustomToast.showToast(MyTransaction.this,"please select a successful transaction to set as favorite", Toast.LENGTH_LONG);
				else
				{
					Intent setFavIntent=new Intent(MyTransaction.this,SetFavourite.class);
					setFavIntent.putExtra("MYAC_FAV_HASHMAP", selectedTxnHashMap);
					startActivity(setFavIntent);
				}
			}
			else
				ARCustomToast.showToast(MyTransaction.this,"please select a transaction to set favourite", Toast.LENGTH_LONG);
			break;*/
        }
    }

    // Expensive operation method
	/*private static void setListViewHeightBasedOnChildren(ListView listView) 
	{
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		int totalHeight=0;
		View view = null;

		for (int i = 0; i < listAdapter.getCount(); i++) 
		{
			view = listAdapter.getView(i, view, listView); 
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.MATCH_PARENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}*/

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
		/*if(v.getId()==R.id.header_FavouriteBtn)
		{
			startActivity(new Intent(MyTransaction.this,FavouriteScreen.class));
		}
		if(v.getId()==R.id.header_MyTicketBtn)
		{
			startActivity(new Intent(MyTransaction.this,MyTicket.class));
		}*/

		/*if(v.getId()==R.id.header_MyProfileBtn)
		{
			startActivity(new Intent(MyTransitions.this,EditProfile.class));
		}

		if(v.getId()==R.id.header_ChangePWBtn)
		{
			startActivity(new Intent(MyTransitions.this,ChangePassword.class));
		}*/

        if (v.getId() == R.id.footerEditProfileBtn)
            startActivity(new Intent(MyTransaction.this, EditProfile.class));
        else if (v.getId() == R.id.footerFavoriteBtn)
            startActivity(new Intent(MyTransaction.this, FavouriteScreen.class));

    }

    public String formmyTransitionJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPTRANSACTIONHISTORY");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));
            //			json.put("USERID", "93");

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    public String formRepeatRechargeJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put("APISERVICE", "KPREPEATRECHARGE");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("IMEINUMBER", preference.getString("IMEI_NUMBER_KEY", ""));
            json.put("DEVICEOS", "ANDROID");
            json.put("SOURCENAME", "KPAPP");
            json.put("ORDERID", selectedOrderId);
//			json.put("ORDERID", "CT0000012435-002");

            return json.toString();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {
        // TODO Auto-generated method stub
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 1) {
                    if (parseMyTransactionDetails(loadedString)) {
//						showMyTransactionList(myTransitionArrayList);
                        myTransitionAdapter = new MyAccountAdapter(MyTransaction.this, myTransitionArrayList);
                        myTransitionListView.setAdapter(myTransitionAdapter);
//						setListViewHeightBasedOnChildren(myTransitionListView);
                    }
                }

                if (apiHitPosition == 2) {
                    if (parseRepeatRechargeDetail(loadedString)) {
                        Intent payNowIntent = new Intent(MyTransaction.this, OrderDetails.class);
                        payNowIntent.putExtra("PAYNOW_DETAILS_VIA_RR_HASHMAP", repeatRechargeHashMap);
                        startActivity(payNowIntent);
                    }
                }
            } else {
                if (!isRepeatButtonClicked) {
                    MyTransaction.this.finish();
                }
                ARCustomToast.showToast(MyTransaction.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
            if (!isRepeatButtonClicked) {
                MyTransaction.this.finish();
            }
            ARCustomToast.showToast(MyTransaction.this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    //PARSING TRANSACTION DETAILS DATA
    public boolean parseMyTransactionDetails(String myTransactionResponse) {
        JSONObject jsonObject, parentResponseObject, childResponseObject;
        try {
            jsonObject = new JSONObject(myTransactionResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    if (parentResponseObject.has(ProjectConstant.TXN_TRANSACTIONDETAIL)) {
                        myTransitionArrayList = new ArrayList<>();
                        //JSONArray txnJsonArray=parentResponseObject.getJSONArray(ProjectConstant.TXN_TRANSACTIONDETAIL);

                        // CHECKING WHETHER THE DATA INSIDE FAVOURITES TAG IS OBJECT OR JSON OBJECT
                        Object object = parentResponseObject.get(ProjectConstant.TXN_TRANSACTIONDETAIL);

                        if (object instanceof JSONObject) {
                            final JSONObject jsonOBJECT = (JSONObject) object;
                            txnJsonArray = new JSONArray();
                            txnJsonArray.put(jsonOBJECT);
                        } else if (object instanceof JSONArray) {
                            txnJsonArray = (JSONArray) object;
                        }

                        for (int i = 0; i < txnJsonArray.length(); i++) {
                            childResponseObject = txnJsonArray.getJSONObject(i);

                            if (!childResponseObject.isNull(ProjectConstant.TXN_TRANSACTIONDATETIME)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_SERVICEID)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTCURRENCYCODE)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_RECHARGECURRENCYCODE)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_OPERATORNAME)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTAMOUNT)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_PAYMENTSTATUS)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_SERVICENAME)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_COUNTRYCODE)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_DISPLAYAMOUNT)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_ORDERID)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_OPERATORCODE)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_DISPLAYCURRENCYCODE)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_RECHARGESTATUS)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_RECHARGEAMOUNT)
                                    && !childResponseObject.isNull(ProjectConstant.TXN_SUBSCRIPTIONNUMBER)) {
                                myTransitionHashMap = new HashMap<>();
                                myTransitionHashMap.put(ProjectConstant.TXN_TRANSACTIONDATETIME, childResponseObject.getString(ProjectConstant.TXN_TRANSACTIONDATETIME));
                                myTransitionHashMap.put(ProjectConstant.TXN_SERVICEID, childResponseObject.getString(ProjectConstant.TXN_SERVICEID));
                                myTransitionHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_PAYMENTCURRENCYCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_RECHARGECURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_RECHARGECURRENCYCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_OPERATORNAME, childResponseObject.getString(ProjectConstant.TXN_OPERATORNAME));
                                myTransitionHashMap.put(ProjectConstant.TXN_PAYMENTAMOUNT, childResponseObject.getString(ProjectConstant.TXN_PAYMENTAMOUNT));
                                myTransitionHashMap.put(ProjectConstant.TXN_PAYMENTSTATUS, childResponseObject.getString(ProjectConstant.TXN_PAYMENTSTATUS));
                                myTransitionHashMap.put(ProjectConstant.TXN_SERVICENAME, childResponseObject.getString(ProjectConstant.TXN_SERVICENAME));
                                myTransitionHashMap.put(ProjectConstant.TXN_COUNTRYCODE, childResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, childResponseObject.getString(ProjectConstant.TXN_DISPLAYAMOUNT));
                                myTransitionHashMap.put(ProjectConstant.TXN_ORDERID, childResponseObject.getString(ProjectConstant.TXN_ORDERID));
                                myTransitionHashMap.put(ProjectConstant.TXN_OPERATORCODE, childResponseObject.getString(ProjectConstant.TXN_OPERATORCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_DISPLAYCURRENCYCODE, childResponseObject.getString(ProjectConstant.TXN_DISPLAYCURRENCYCODE));
                                myTransitionHashMap.put(ProjectConstant.TXN_RECHARGESTATUS, childResponseObject.getString(ProjectConstant.TXN_RECHARGESTATUS));
                                myTransitionHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, childResponseObject.getString(ProjectConstant.TXN_RECHARGEAMOUNT));
                                myTransitionHashMap.put(ProjectConstant.TXN_SUBSCRIPTIONNUMBER, childResponseObject.getString(ProjectConstant.TXN_SUBSCRIPTIONNUMBER));

                                if (childResponseObject.has("RECHARGEPIN"))
                                    myTransitionHashMap.put("RECHARGEPIN", childResponseObject.getString("RECHARGEPIN").toString());
                                if (childResponseObject.has("RECHARGEPINEXPIRYDATE"))
                                    myTransitionHashMap.put("RECHARGEPINEXPIRYDATE", childResponseObject.getString("RECHARGEPINEXPIRYDATE").toString());
                                if (childResponseObject.has("CPNSRNUM"))
                                    myTransitionHashMap.put("CPNSRNUM", childResponseObject.getString("CPNSRNUM").toString());
                                if (childResponseObject.has("CPNVALUE"))
                                    myTransitionHashMap.put("CPNVALUE", childResponseObject.getString("CPNVALUE").toString());

                                myTransitionArrayList.add(myTransitionHashMap);
                            }
                        }
                    }
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(MyTransaction.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //PARSING REPEAT RECHARGE DETAILS DATA
    public boolean parseRepeatRechargeDetail(String repeatRCResponse) {
        JSONObject jsonObject, parentResponseObject;

        try {
            jsonObject = new JSONObject(repeatRCResponse);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (parentResponseObject.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (parentResponseObject.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    repeatRechargeHashMap = new HashMap<>();

                    if (parentResponseObject.has(ProjectConstant.TXN_DISPLAYAMOUNT))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, parentResponseObject.getString(ProjectConstant.TXN_DISPLAYAMOUNT).toString());
                    else
                        repeatRechargeHashMap.put(ProjectConstant.TXN_DISPLAYAMOUNT, selectedDisplayAmount);

                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_DISPLAYCURRENCYAGID).toString());
                    if (parentResponseObject.has("DISPLAYAMOUNTCURRENCYCODE"))
                        repeatRechargeHashMap.put("DISPLAYAMOUNTCURRENCYCODE", parentResponseObject.getString("DISPLAYAMOUNTCURRENCYCODE").toString());
                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGEAMT))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGEAMT, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGEAMT).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_SERVICEID))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICEID, parentResponseObject.getString(ProjectConstant.TXN_SERVICEID).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_OPERATORNAME))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_OPERATORNAME, parentResponseObject.getString(ProjectConstant.TXN_OPERATORNAME).toString());
                    if (parentResponseObject.has("EMAILID"))
                        repeatRechargeHashMap.put("EMAILID", parentResponseObject.getString("EMAILID").toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PROCESSINGFEE))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PROCESSINGFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_PROCESSINGFEE).toString());
                    if (parentResponseObject.has("PGAMT"))
                        repeatRechargeHashMap.put("PGAMT", parentResponseObject.getString("PGAMT").toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_PAYMENTCURRENCYCODE))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_PAYMENTCURRENCYCODE, parentResponseObject.getString(ProjectConstant.TXN_PAYMENTCURRENCYCODE).toString());

                    if (parentResponseObject.has(ProjectConstant.TXN_SERVICENAME))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_SERVICENAME, parentResponseObject.getString(ProjectConstant.TXN_SERVICENAME).toString());
                    if (parentResponseObject.has(ProjectConstant.TXN_COUNTRYCODE))
                        repeatRechargeHashMap.put(ProjectConstant.TXN_COUNTRYCODE, parentResponseObject.getString(ProjectConstant.TXN_COUNTRYCODE).toString());
                    if (parentResponseObject.has("MOBILENUMBER"))
                        repeatRechargeHashMap.put("MOBILENUMBER", parentResponseObject.getString("MOBILENUMBER").toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_SERVICEFEE))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_SERVICEFEE, parentResponseObject.getString(ProjectConstant.PAYNOW_SERVICEFEE).toString());
                    if (parentResponseObject.has(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID, parentResponseObject.getString(ProjectConstant.TOPUP_API_RECHARGECURRENCYAGID).toString());
                    if (parentResponseObject.has(ProjectConstant.API_OPCODE))
                        repeatRechargeHashMap.put(ProjectConstant.API_OPCODE, parentResponseObject.getString(ProjectConstant.API_OPCODE).toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PLANDESCRIPTION))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PLANDESCRIPTION, parentResponseObject.getString(ProjectConstant.PAYNOW_PLANDESCRIPTION).toString());
                    if (parentResponseObject.has(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID))
                        repeatRechargeHashMap.put(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID, parentResponseObject.getString(ProjectConstant.PAYNOW_PAYMENTCURRENCYAGID).toString());

                    if (parentResponseObject.has("URLTERMS"))
                        ProjectConstant.tncURL = parentResponseObject.getString("URLTERMS");
                    if (parentResponseObject.has("URLPROVIDERHELP"))
                        ProjectConstant.providerDetailsURL = parentResponseObject.getString("URLPROVIDERHELP");

                    repeatRechargeHashMap.put(ProjectConstant.API_OPID, "");
                    repeatRechargeHashMap.put("AGID", "");

                    repeatRechargeHashMap.put(ProjectConstant.TXN_RECHARGEAMOUNT, selectedRechargeAmount);
                } else {
                    if (parentResponseObject.has("DESCRIPTION")) {
                        ARCustomToast.showToast(MyTransaction.this, parentResponseObject.getString("DESCRIPTION"), Toast.LENGTH_LONG);
                        return false;
                    }
                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

        return true;

    }
	
	/*@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		if(preference!=null && cartItemCounter!=null)
		{
//			cartItemCounter.setText(preference.getString("CART_ITEM_COUNTER", "0"));
		if((preference.getString("CART_ITEM_COUNTER", "0").equals("0")))
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage0);	
		}
		else
		{
			cartItemCounter.setBackgroundResource(R.drawable.cartimage1);	
		}
		
		}
	}*/


}