package com.uk.recharge.kwikpay.myaccount;

import com.uk.recharge.kwikpay.catering.models.CateringCompleteOrdersMO;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 22-12-2017.
 */

public class MyTransactionSingleton {

    private static MyTransactionSingleton myTransactionSingleton = null;
    private ArrayList<MyTransactionsMO> allTransactionList = null;
    private ArrayList<MyTransactionsMO> mobileTransactionList = null;
    private ArrayList<MyTransactionsMO> digitalTransactionList = null;
    private ArrayList<MyTransactionsMO> kvChargeTransactionList = null;
    private ArrayList<CateringCompleteOrdersMO> cateringOnGoingCompleteOrderList = null;
    private ArrayList<CateringCompleteOrdersMO> cateringPastCompleteOrderList = null;

    private MyTransactionSingleton() {
    }

    public static MyTransactionSingleton getInstance() {
        if (myTransactionSingleton == null) {
            myTransactionSingleton = new MyTransactionSingleton();
        }
        return myTransactionSingleton;
    }

    public ArrayList<MyTransactionsMO> getAllTransactionList() {
        return allTransactionList;
    }

    public void setAllTransactionList(ArrayList<MyTransactionsMO> allTransactionList) {
        this.allTransactionList = allTransactionList;
    }

    public ArrayList<MyTransactionsMO> getMobileTransactionList() {
        return mobileTransactionList;
    }

    public void setMobileTransactionList(ArrayList<MyTransactionsMO> mobileTransactionList) {
        this.mobileTransactionList = mobileTransactionList;
    }

    public ArrayList<MyTransactionsMO> getDigitalTransactionList() {
        return digitalTransactionList;
    }

    public void setDigitalTransactionList(ArrayList<MyTransactionsMO> digitalTransactionList) {
        this.digitalTransactionList = digitalTransactionList;
    }

    public ArrayList<MyTransactionsMO> getKvChargeTransactionList() {
        return kvChargeTransactionList;
    }

    public void setKvChargeTransactionList(ArrayList<MyTransactionsMO> kvChargeTransactionList) {
        this.kvChargeTransactionList = kvChargeTransactionList;
    }

    public ArrayList<CateringCompleteOrdersMO> getCateringOnGoingCompleteOrderList() {
        return cateringOnGoingCompleteOrderList;
    }

    public void setCateringOnGoingCompleteOrderList(ArrayList<CateringCompleteOrdersMO> cateringOnGoingCompleteOrderList) {
        this.cateringOnGoingCompleteOrderList = cateringOnGoingCompleteOrderList;
    }

    public ArrayList<CateringCompleteOrdersMO> getCateringPastCompleteOrderList() {
        return cateringPastCompleteOrderList;
    }

    public void setCateringPastCompleteOrderList(ArrayList<CateringCompleteOrdersMO> cateringPastCompleteOrderList) {
        this.cateringPastCompleteOrderList = cateringPastCompleteOrderList;
    }

    public void clearAllArrayLists() {
        if (allTransactionList != null && allTransactionList.size() > 0)
            allTransactionList.clear();
        if (mobileTransactionList != null && mobileTransactionList.size() > 0)
            mobileTransactionList.clear();
        if (digitalTransactionList != null && digitalTransactionList.size() > 0)
            digitalTransactionList.clear();
        if (kvChargeTransactionList != null && kvChargeTransactionList.size() > 0)
            kvChargeTransactionList.clear();
        if (cateringOnGoingCompleteOrderList != null && cateringOnGoingCompleteOrderList.size() > 0)
            cateringOnGoingCompleteOrderList.clear();
        if (cateringPastCompleteOrderList != null && cateringPastCompleteOrderList.size() > 0)
            cateringPastCompleteOrderList.clear();
    }

}
