package com.uk.recharge.kwikpay.RoomDataBase.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;

import java.util.List;

@Dao
public interface IngredientTypeDao {

    @Query("SELECT * FROM IngredientType where parent_id=:parent_id")
    List<IngredientType> getAll(String parent_id);

    @Insert
    void insert(List<IngredientType> list);

    @Query("delete from IngredientType")
    void clear();

    @Query("SELECT COUNT(*) from IngredientType")
    int count();

}
