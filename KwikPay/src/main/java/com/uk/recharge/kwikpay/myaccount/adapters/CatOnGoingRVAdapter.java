package com.uk.recharge.kwikpay.myaccount.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.catering.models.CateringCompleteOrdersMO;
import com.uk.recharge.kwikpay.catering.payment.CateringQRTrackingStatus;
import com.uk.recharge.kwikpay.myaccount.MyTransactionSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 11/7/2018.
 */

public class CatOnGoingRVAdapter extends RecyclerView.Adapter<CatOnGoingRVAdapter.CatBothOrdersViewHolder> {

    private LayoutInflater layoutInflater = null;
    //1: OnGoing Order , 2: Past Order
    ArrayList<CateringCompleteOrdersMO> cateringOnGoingCompleteOrderList = null;
    private Context context = null;

    public CatOnGoingRVAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        cateringOnGoingCompleteOrderList = MyTransactionSingleton.getInstance().getCateringOnGoingCompleteOrderList();
    }

    @NonNull
    @Override
    public CatBothOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cat_ongoing_post_order_row, parent, false);
        return new CatBothOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CatBothOrdersViewHolder holder, int position) {

        try {
            holder.transactionDateTime.setText(cateringOnGoingCompleteOrderList.get(position).getCateringTxnDateTime());
            holder.transactionOrderID.setText(cateringOnGoingCompleteOrderList.get(position).getCateringOrderId());
            holder.transactionRCAmount.setText(Utility.fromHtml(cateringOnGoingCompleteOrderList.get(position).getCateringDisplayCurrencyCode()) +
                    cateringOnGoingCompleteOrderList.get(position).getCateringPaymentAmount());
            holder.transactionOrderItemsCount.setText("Items : " + cateringOnGoingCompleteOrderList.get(position).getCateringTotalItems());
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        try {
            if (cateringOnGoingCompleteOrderList.get(position).getCateringCancelStatus().equals("1")) {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderCancelled));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusCancel));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_red));
            } else if (cateringOnGoingCompleteOrderList.get(position).getCateringDeliveryStatus().equals("1")) {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderDelivered));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusReadyOrPrep));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_green));
            } else if (cateringOnGoingCompleteOrderList.get(position).getCateringReadyStatus().equals("1")) {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderReadyForCollect));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusReadyOrPrep));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_green));
            } else if (cateringOnGoingCompleteOrderList.get(position).getCateringAcceptedStatus().equals("1")) {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderUnderPrep));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusUnderPrep));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_blue));
            } else {
                holder.transactionOrderStatus.setText(context.getResources().getString(R.string.catOrderPlaced));
                holder.transactionOrderStatus.setTextColor(context.getResources().getColor(R.color.orderStatusPlaced));
                holder.transactionOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.order_status_orange));

            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        try {
            Glide.with(context)
                    .load(cateringOnGoingCompleteOrderList.get(position).getProductDistributorDetails().getDistributorLogo())
                    .placeholder(R.drawable.default_operator_logo)
                    .into(holder.operatorImages);
        } catch (Exception e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (cateringOnGoingCompleteOrderList != null && cateringOnGoingCompleteOrderList.size() > 0) {
            return cateringOnGoingCompleteOrderList.size();
        }
        return 0;
    }

    public class CatBothOrdersViewHolder extends RecyclerView.ViewHolder {

        private TextView transactionOrderID, transactionDateTime, transactionRCAmount,
                transactionOrderItemsCount, transactionOrderStatus;
        private ImageView operatorImages;

        public CatBothOrdersViewHolder(View itemView) {
            super(itemView);
            transactionOrderID = itemView.findViewById(R.id.transactionOrderID);
            transactionDateTime = itemView.findViewById(R.id.transactionDateTime);
            transactionRCAmount = itemView.findViewById(R.id.transactionRCAmount);
            transactionOrderItemsCount = itemView.findViewById(R.id.transactionOrderItemsCount);
            transactionOrderStatus = itemView.findViewById(R.id.transactionOrderStatus);
            operatorImages = itemView.findViewById(R.id.operatorImages);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, CateringQRTrackingStatus.class)
                            .putExtra("SELECTED_ORDER_TXN", cateringOnGoingCompleteOrderList.get(getLayoutPosition())));
                }
            });

        }
    }
}

