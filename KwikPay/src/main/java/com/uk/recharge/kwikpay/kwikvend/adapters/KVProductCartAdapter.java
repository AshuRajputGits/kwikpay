package com.uk.recharge.kwikpay.kwikvend.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductDetailsMO;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Ashu Rajput on 7/11/2018.
 */

public class KVProductCartAdapter extends RecyclerView.Adapter<KVProductCartAdapter.KVProductCartViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<KVProductDetailsMO> kvProductDetailsMOList = null;
    private int productTotalQuantity = 0;
    private float productTotalPrice = 0;
    private UpdateTotalsListener listener = null;
    //    private Context context;
    //    private String moduleName = "";
    private String currencySign = "";

    public KVProductCartAdapter(Context context, String currencySign) {
        layoutInflater = LayoutInflater.from(context);
        this.kvProductDetailsMOList = KVProductDetailsSingleton.getInstance().getKvProductDetailsMOList();
        this.listener = (UpdateTotalsListener) context;
//        this.context = context;
        this.currencySign = currencySign;
    }

    @NonNull
    @Override
    public KVProductCartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.kv_product_cart_row, parent, false);
        return new KVProductCartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KVProductCartViewHolder holder, int pos) {

        try {
            /*if (moduleName.equalsIgnoreCase("KVReceipt")) {
                holder.kvCartProductMinus.setVisibility(View.INVISIBLE);
                holder.kvCartProductPlus.setVisibility(View.INVISIBLE);

                if (pos % 2 == 0)
                    holder.kvProductRowLayout.setBackgroundColor(context.getResources().getColor(R.color.greyColor));
            }*/

            holder.kvCartProductQuantity.setText(String.valueOf(kvProductDetailsMOList.get(pos).getProductQuantity()));
            holder.kvCartProductName.setText(kvProductDetailsMOList.get(pos).getProductName());
//            holder.kvCartProductPrice.setText(Utility.fromHtml(currencySign) + String.valueOf(kvProductDetailsMOList.get(pos).getProductPrice()));
            holder.kvCartProductPrice.setText(calculateIndividualProductAmount(pos));

            productTotalQuantity = productTotalQuantity + kvProductDetailsMOList.get(pos).getProductQuantity();
            productTotalPrice = productTotalPrice + (kvProductDetailsMOList.get(pos).getProductQuantity() *
                    kvProductDetailsMOList.get(pos).getProductPrice());

            if (pos == kvProductDetailsMOList.size() - 1) {
                listener.getTotalQuantityAmount(productTotalQuantity, productTotalPrice);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    @Override
    public int getItemCount() {
        return kvProductDetailsMOList.size();
    }

    public class KVProductCartViewHolder extends RecyclerView.ViewHolder {

        private TextView kvCartProductName, kvCartProductQuantity, kvCartProductPrice;
        private TextView kvCartProductMinus, kvCartProductPlus;

        public KVProductCartViewHolder(View itemView) {
            super(itemView);
            kvCartProductName = itemView.findViewById(R.id.kvCartProductName);
            kvCartProductQuantity = itemView.findViewById(R.id.kvCartProductQuantity);
            kvCartProductPrice = itemView.findViewById(R.id.kvCartProductPrice);

            kvCartProductMinus = itemView.findViewById(R.id.kvCartProductMinus);
            kvCartProductMinus.setOnClickListener(onClickListener);

            kvCartProductPlus = itemView.findViewById(R.id.kvCartProductPlus);
            kvCartProductPlus.setOnClickListener(onClickListener);
        }

        private View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.kvCartProductMinus) {
                    if (kvProductDetailsMOList.get(getLayoutPosition()).getProductQuantity() > 1) {
                        int minusQuantity = kvProductDetailsMOList.get(getLayoutPosition()).getProductQuantity() - 1;
                        kvProductDetailsMOList.get(getLayoutPosition()).setProductQuantity(minusQuantity);
                    }

                } else if (v.getId() == R.id.kvCartProductPlus) {

                    int plusQuantity = kvProductDetailsMOList.get(getLayoutPosition()).getProductQuantity() + 1;

                    if (plusQuantity <= kvProductDetailsMOList.get(getLayoutPosition()).getProductAvailableQuantity())
                        kvProductDetailsMOList.get(getLayoutPosition()).setProductQuantity(plusQuantity);
                    else
                        listener.showMessageToUI("you have selected the maximum allowed quantity");
                }
                productTotalPrice = productTotalQuantity = 0;
                notifyDataSetChanged();
            }
        };
    }

    public interface UpdateTotalsListener {
        void getTotalQuantityAmount(int productQty, float productTotalAmount);

        void showMessageToUI(String message);
    }

    public void setIfModifiedProductsFromCartToSingleton() {
        KVProductDetailsSingleton.getInstance().setKvProductDetailsMOList(kvProductDetailsMOList);
    }

    private String calculateIndividualProductAmount(int position) {
//        double productAmount = 0.0;
        float productAmount = 0f;
        try {
            productAmount = kvProductDetailsMOList.get(position).getProductQuantity() *
                    kvProductDetailsMOList.get(position).getProductPrice();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Utility.fromHtml(currencySign) + String.valueOf(productAmount);
    }
}
