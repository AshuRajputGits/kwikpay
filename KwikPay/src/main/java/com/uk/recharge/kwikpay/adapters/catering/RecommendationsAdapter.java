package com.uk.recharge.kwikpay.adapters.catering;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.RoomDataBase.Models.ProductType;
import com.uk.recharge.kwikpay.catering.CustomizeOrder;
import com.uk.recharge.kwikpay.models.CartProduct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.uk.recharge.kwikpay.catering.CateringCart.cartProductId;
import static com.uk.recharge.kwikpay.catering.CateringCart.cartProducts;
import static com.uk.recharge.kwikpay.catering.CateringCart.setPaymentTotal;
import static com.uk.recharge.kwikpay.catering.CateringCart.updateCartTotal;
import static com.uk.recharge.kwikpay.catering.MyOrders.billamount;
import static com.uk.recharge.kwikpay.catering.MyOrders.cart_items_recycler_view;
import static com.uk.recharge.kwikpay.catering.MyOrders.itemtotal;
import static com.uk.recharge.kwikpay.catering.MyOrders.recomdation_recycler_view;
import static com.uk.recharge.kwikpay.catering.RestoMenuList.cartCountTextView;
import static com.uk.recharge.kwikpay.catering.RestoMenuList.cartTotalTextView;
import static com.uk.recharge.kwikpay.utilities.AppConstants.DATABASE_NAME;
import static com.uk.recharge.kwikpay.utilities.Utility.randomUUID;

public class RecommendationsAdapter extends RecyclerView.Adapter<RecommendationsAdapter.MyViewHolder> {

    private List<ProductType> recommendationsProductList;
    private Context context;
    private AppDatabase appDatabase;

    public RecommendationsAdapter(List<ProductType> recommendationsProductList, Context context) {
        this.recommendationsProductList = recommendationsProductList;
        this.context = context;
        appDatabase = Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recommendations_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ProductType productType = recommendationsProductList.get(position);

        holder.product_name.setText(productType.getName());
        holder.product_quantity.setText(String.format("£ %s", productType.getUnit_price()));

        Glide.with(context)
                .load(productType.getImage())
                .placeholder(R.drawable.you_may_like_this_box)
                .into(holder.product_image);

        final List<Ingredient> ingredientList = appDatabase.ingredientDao().getAll(productType.getParent_id());


        holder.product_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartProduct cartProduct = new CartProduct();

                cartProduct.setId(randomUUID());
                cartProduct.setCategory(productType.getParent_id());
                cartProduct.setBrand_name(productType.getName());
                cartProduct.setAvailable_quantity(productType.getAvailable_quantity());
                cartProduct.setCategory_action(productType.getCategory_action());
                cartProduct.setCore_price(productType.getCore_price());
                cartProduct.setImage(productType.getImage());
                cartProduct.setProduct_description(productType.getProduct_description());
                cartProduct.setProduct_id(productType.getProduct_id());
                cartProduct.setSelected_quantity(1);
                cartProduct.setUnit_price(productType.getUnit_price());
                cartProduct.setSize(productType.getSize());
                cartProduct.setIngredientList(ingredientList);
                cartProduct.setViewCheckedPerIngredientList(getDefaultMapping(ingredientList));

                cartProductId.add(productType.getProduct_id());
                cartProducts.add(cartProduct);


                updateCartTotal(cartCountTextView, cartTotalTextView, appDatabase);
                setPaymentTotal(itemtotal, billamount);

                recommendationsProductList.remove(position);

                recomdation_recycler_view.setAdapter(new RecommendationsAdapter(recommendationsProductList, context));
                cart_items_recycler_view.setAdapter(new CartProductsAdapter(context));

                Log.e("Recommendation PRODUCT", "Added Product ID : " + productType.getProduct_id());
                Toast.makeText(context, "Added To Cart", Toast.LENGTH_SHORT).show();


                if (!ingredientList.isEmpty()) {

                    Intent intent = new Intent(context, CustomizeOrder.class);
                    intent.putExtra("parent_id", productType.getParent_id());
                    intent.putExtra("product_type", productType.getName());
                    intent.putExtra("id", cartProduct.getId());
                    context.startActivity(intent);
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return recommendationsProductList.size();
    }

    private HashMap<String, List<Integer>> getDefaultMapping(List<Ingredient> ingredientList) {
        HashMap<String, List<Integer>> viewCheckedPerIngredientList = new HashMap<>();

        for (final Ingredient ingredient : ingredientList) {


            List<Integer> idList = new ArrayList<>();

            if (ingredient.getCategory_action().equals("MULTIPLE_SELECTION")) {


                List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());


                for (int index = 0; index < ingredientTypeList.size(); index++)
                    idList.add(0);

            } else {


                List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());

                for (int index = 0; index < ingredientTypeList.size(); index++) {

                    if (index == 0)
                        idList.add(1);

                    idList.add(0);


                }
            }

            viewCheckedPerIngredientList.put(ingredient.getName(), idList);


        }

        return viewCheckedPerIngredientList;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView product_name, product_quantity;
        ImageView product_image;
        LinearLayout product_layout;

        MyViewHolder(View view) {
            super(view);
            product_layout = view.findViewById(R.id.product_layout);
            product_name = view.findViewById(R.id.product_name);
            product_quantity = view.findViewById(R.id.product_quantity);
            product_image = view.findViewById(R.id.product_image);
        }
    }
}
