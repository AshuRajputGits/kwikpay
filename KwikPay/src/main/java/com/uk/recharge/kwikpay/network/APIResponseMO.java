package com.uk.recharge.kwikpay.network;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

/**
 * Created by Ashu Rajput on 04-02-2018.
 */

public class APIResponseMO {

    @SerializedName("KPRESPONSE")
    private JSONObject kpResponse;

    public JSONObject getKpResponse() {
        return kpResponse;
    }

}
