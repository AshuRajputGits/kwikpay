package com.uk.recharge.kwikpay.kwikvend;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uk.recharge.kwikpay.R;

/**
 * Created by Ashu Rajput on 7/11/2018.
 */

public class KVProductDetailDialog extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.kv_product_details_dialog, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView infoProductImage = view.findViewById(R.id.infoProductImage);
        TextView infoProductName = view.findViewById(R.id.infoProductName);
        TextView infoProductPrice = view.findViewById(R.id.infoProductPrice);
        TextView infoProductDescription = view.findViewById(R.id.infoProductDescription);
        LinearLayout dialogParentLayoutID = view.findViewById(R.id.dialogParentLayoutID);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        dialogParentLayoutID.setMinimumWidth((6 * width) / 8);

        Bundle receivedBundle = getArguments();
        if (receivedBundle != null) {
            infoProductName.setText(receivedBundle.getString("ProdName", ""));
            infoProductPrice.setText("" + receivedBundle.getFloat("ProdPrice"));

            try {
                Glide.with(this).load(receivedBundle.getString("ProdImageURL")).into(infoProductImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return view;
    }

}
