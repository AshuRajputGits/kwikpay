package com.uk.recharge.kwikpay;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.JobIntentService;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.ar.library.ARCustomToast;
import com.ar.library.DeviceNetConnectionDetector;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.crashlytics.android.Crashlytics;
import com.uk.recharge.kwikpay.catering.CateringLaunchScreen;
import com.uk.recharge.kwikpay.gameservice.GameServicesScreen;
import com.uk.recharge.kwikpay.gamingarcade.GameArcadeHomeScreen;
import com.uk.recharge.kwikpay.kwikcharge.KCReceiptScreen;
import com.uk.recharge.kwikpay.kwikcharge.ScanAndLocationActivity;
import com.uk.recharge.kwikpay.kwikcharge.models.KCReceiptMO;
import com.uk.recharge.kwikpay.kwikcharge.models.LocationIdMO;
import com.uk.recharge.kwikpay.kwikcharge.payment.KCChargingTimeScreen;
import com.uk.recharge.kwikpay.kwikcharge.qrscanner.BarcodeScannerActivity;
import com.uk.recharge.kwikpay.kwikvend.KVHomeScreen;
import com.uk.recharge.kwikpay.kwikvend.KVProductSelection;
import com.uk.recharge.kwikpay.kwikvend.models.KVProductOthersDetailsMO;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.services.GetPublicIPService;
import com.uk.recharge.kwikpay.services.SendEmailForVerificationService;
import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;
import com.uk.recharge.kwikpay.singleton.KVProductDetailsSingleton;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;
import com.urbanairship.UAirship;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeScreen extends KPSuperClass implements OnClickListener, AsyncRequestListenerViaPost {

    private SharedPreferences preferences;
    private int apiHitPosition = 0;
    private static final int REQUEST_CAMERA_RESULT = 10;
    private static final int REQUEST_SCANNER = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createMenuDrawer(this);
        initiateHomeAppsBanner("HomeScreen");
        setDynamicWidthHeightToHomeView();
        settingIds();

        if (!preferences.getString("KP_USER_ID", "").equals("0")) {
            //CHECKING THE CONDITION IF USER'S EMAIL ID IS NOT VERIFIED
            if (!preferences.getBoolean("EMAIL_IS_VERIFIED", false) && preferences.getBoolean("IS_USER_SIGNED_UP", false)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Intent sendEmailFromBG = new Intent();
                    sendEmailFromBG.putExtra("isComingFromKPLoginScreen", false);
                    JobIntentService.enqueueWork(HomeScreen.this, SendEmailForVerificationService.class, 1002, sendEmailFromBG);
                } else {
                    Intent sendEmailFromBG = new Intent(HomeScreen.this, SendEmailForVerificationService.class);
                    sendEmailFromBG.putExtra("isComingFromKPLoginScreen", false);
                    startService(sendEmailFromBG);
                }
            }
        }

        //STARTING SERVICE TO GET PUBLIC IP FROM URL
        try {
            if (preferences.getString("USER_PUBLIC_IP", "0.0.0.0").equals("0.0.0.0")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Intent intent = new Intent();
                    JobIntentService.enqueueWork(HomeScreen.this, GetPublicIPService.class, 1001, intent);
                } else
                    startService(new Intent(HomeScreen.this, GetPublicIPService.class));
            }

        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        //-------CALLING BELOW TO REMOVE THE TEMP KEY-VALUE FROM PREF SET DURING KC_CHARGING_TIME_SCREEN-------
        removeTempInformationFromPrefs();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.home_screen;
    }

    private void settingIds() {

        // Add this line if we want to track some extra stuff apart from installations, sessions, and updates.
        AppsFlyerLib.sendTracking(getApplicationContext());

        preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);

        findViewById(R.id.topupImgBtn).setOnClickListener(this);
        findViewById(R.id.internationalMobileTopUp).setOnClickListener(this);
        findViewById(R.id.gameserviceImgBtn).setOnClickListener(this);
        findViewById(R.id.kwikChargeImageButton).setOnClickListener(this);
        findViewById(R.id.kwikvendButton).setOnClickListener(this);
        findViewById(R.id.gamingButton).setOnClickListener(this);
        findViewById(R.id.catering).setOnClickListener(this);
        findViewById(R.id.scanbutton).setOnClickListener(this);

        //SETTING SCREEN NAMES TO APPS FLYER
        try {
            EventsLoggerSingleton.getInstance().setCommonEventLogs(this, getResources().getString(R.string.SCREEN_NAME_HOME_PAGE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        // INITIALIZING/ENABLE SDK OF URBAN AIR SHIP FOR PUSH NOTIFICATION
        try {
            UAirship.shared().getPushManager().setUserNotificationsEnabled(true);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topupImgBtn:
                apiHitPosition = 1;
                new LoadResponseViaPost(HomeScreen.this, formBannerImagesJson(1), true).execute("");
                break;
            case R.id.internationalMobileTopUp:
                startActivity(new Intent(HomeScreen.this, InternationalTopUp.class));
                break;
            case R.id.gameserviceImgBtn:
                ProjectConstant.TOP_UP_IDENTIFIER = 0;
                startActivity(new Intent(HomeScreen.this, GameServicesScreen.class));
                break;
            case R.id.kwikChargeImageButton:
                apiHitPosition = 3;
                new LoadResponseViaPost(HomeScreen.this, formBannerImagesJson(2), true).execute("");
                break;

            //Use Type=3 (Gaming), Type=4 (KwikVend)
            case R.id.kwikvendButton:
                callGameArcadeBannerAPI(4);
                break;
            case R.id.gamingButton:
                callGameArcadeBannerAPI(3);
//                Toast.makeText(HomeScreen.this, "Please wait while WIP...", Toast.LENGTH_LONG).show();
                break;
            case R.id.catering:
                startActivity(new Intent(this, CateringLaunchScreen.class));
                break;

            case R.id.scanbutton:
                checkRTPForCamera();
                break;
        }
    }

    private void checkRTPForCamera() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                    startQRScanActivity();
                else {
                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA))
                        Toast.makeText(this, "No Permission to use the Camera services", Toast.LENGTH_SHORT).show();
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, REQUEST_CAMERA_RESULT);
                }
            } else
                startQRScanActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Cannot run application because camera service permission have not been granted", Toast.LENGTH_SHORT).show();
                else
                    startQRScanActivity();
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SCANNER) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String qrCodeWithPrefix = data.getStringExtra("QR_RESULT");
                    if (qrCodeWithPrefix.contains("-")) {
                        String prefixCode = qrCodeWithPrefix.split("-")[0];
                        String locationID = qrCodeWithPrefix.split("-")[1];

//                        Log.e("BarCodeScan", "Prefix and QR  : " + prefixCode + " And " + locationID);

                        if (prefixCode != null && !prefixCode.trim().equals("")) {
                            if (prefixCode.equals("101")) {
                                //EVCharge
                                startActivity(new Intent(this, ScanAndLocationActivity.class).putExtra("HOME_SCANNED_QR", locationID));
                            } else if (prefixCode.equals("201")) {
                                //KwikVend
                                callKVGetProductListScreen(locationID);

                            } else if (prefixCode.equals("301")) {
                                //Game And Services

                            } else if (prefixCode.equals("401")) {
                                //Catering
                                startActivity(new Intent(this, CateringLaunchScreen.class).putExtra("QR_RESULT", locationID));
                            } else if (prefixCode.equals("501")) {
                                //Amusement
                            }
                        } else
                            ARCustomToast.showToast(this, getString(R.string.qr_error_msg));
                    } else
                        ARCustomToast.showToast(this, getString(R.string.qr_error_msg));
                }
            }
        }
    }

    private void startQRScanActivity() {
        startActivityForResult(new Intent(HomeScreen.this, BarcodeScannerActivity.class), REQUEST_SCANNER);
    }

    private void removeTempInformationFromPrefs() {
        try {
            SharedPreferences.Editor editor = preferences.edit();
            if (preferences.contains("TEMP_DEVICE_DB_ID"))
                editor.remove("TEMP_DEVICE_DB_ID");
            if (preferences.contains("TEMP_LOCAL_CONNECTOR_NUMBER"))
                editor.remove("TEMP_LOCAL_CONNECTOR_NUMBER");
            if (preferences.contains("TEMP_APP_TXN_ID"))
                editor.remove("TEMP_APP_TXN_ID");

            //Adding below SP to remove respective key-value after assigning value in Gaming PG Option Screen [Remove if required]
            if (preferences.contains("GAMING_TXN_ID"))
                editor.remove("GAMING_TXN_ID");

            //Removing temp keys w.r.t Catering Modules
            if (preferences.contains("DISTRIBUTOR_NAME"))
                editor.remove("DISTRIBUTOR_NAME");
            if (preferences.contains("DISTRIBUTOR_ADDRESS"))
                editor.remove("DISTRIBUTOR_ADDRESS");
            if (preferences.contains("DISTRIBUTOR_IMAGE"))
                editor.remove("DISTRIBUTOR_IMAGE");
            if (preferences.contains("MERCHANT_TERM_BANNER"))
                editor.remove("MERCHANT_TERM_BANNER");
            editor.apply();
        } catch (Exception e) {
        }
    }

    public String formBannerImagesJson(int requestType) {
        try {
            JSONObject json = new JSONObject();
            if (requestType == 1)
                json.put("APISERVICE", "MOBILE_TOPUP_HOME");
            else if (requestType == 2) {
                json.put("APISERVICE", "KWIKCHARGE_HOME");
                json.put("USERID", preferences.getString("KP_USER_ID", "0"));
            } else if (requestType == 3) {
                json.put("APISERVICE", "AMUSEMENT_HOME");
            } else if (requestType == 4) {
                json.put("APISERVICE", "KWIKVEND_HOME");
            }
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("APPNAME", "KWIKPAY");

            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                if (apiHitPosition == 1)
                    parseTopUpBannerResponse(loadedString);
                else if (apiHitPosition == 3)
                    parseKCBannerResponse(loadedString);
                else if (apiHitPosition == 2)
                    parseReceiptDetailsJson(loadedString);
            } else {
                if (DeviceNetConnectionDetector.checkDataConnWifiMobile(this))
                    ARCustomToast.showToast(this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
                else
                    ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }

        } else {
            if (DeviceNetConnectionDetector.checkDataConnWifiMobile(this))
                ARCustomToast.showToast(this, getResources().getString(R.string.common_ServerConnection), Toast.LENGTH_LONG);
            else
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    public void parseTopUpBannerResponse(String response) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(response);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(this, "No Response Tag Found", Toast.LENGTH_LONG);
                return;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);
            if (parentResponseObject.has("TOPUP_BANNERS")) {
                preferences.edit().putString("TOPUP_BANNERS", parentResponseObject.optString("TOPUP_BANNERS")).apply();
            }
            ProjectConstant.TOP_UP_IDENTIFIER = 0;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        startActivity(new Intent(HomeScreen.this, TopUpIntermediateScreen.class));

    }

    public boolean parseKCBannerResponse(String response) {
        JSONObject jsonObject, parentResponseObject;
        try {
            jsonObject = new JSONObject(response);

            if (jsonObject.isNull(ProjectConstant.RESPONSE_TAG)) {
                ARCustomToast.showToast(this, "No Response Tag Found", Toast.LENGTH_LONG);
                return false;
            }

            parentResponseObject = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);
            if (parentResponseObject.has("KWIKCHARGE_BANNERS")) {
                preferences.edit().putString("KWIKCHARGE_BANNERS", parentResponseObject.optString("KWIKCHARGE_BANNERS")).apply();
            }

            ProjectConstant.TOP_UP_IDENTIFIER = 0;
            if (parentResponseObject.has("APP_TXN_ID")) {
                if (parentResponseObject.getString("APP_TXN_ID").isEmpty()) {
                    startActivity(new Intent(HomeScreen.this, ScanAndLocationActivity.class));
                } else {
                    if (parentResponseObject.optString("EV_LOADER_IN_MIN").isEmpty() || parentResponseObject.optString("EV_LOADER_IN_MIN").equals("0")) {
                        apiHitPosition = 2;
                        new LoadResponseViaPost(this, formKCReceiptJson(parentResponseObject.getString("APP_TXN_ID")), true).execute("");
                    } else {
                        preferences.edit()
                                .putString("TEMP_LOCATION_NUMBER", parentResponseObject.optString("LOCATION_NUMBER"))
                                .putString("TEMP_DEVICE_DB_ID", parentResponseObject.optString("DEVICE_DB_ID"))
                                .putString("TEMP_LOCAL_CONNECTOR_NUMBER", parentResponseObject.optString("LOCAL_CONNECTOR_NUMBER"))
                                .putString("TEMP_APP_TXN_ID", parentResponseObject.optString("APP_TXN_ID")).apply();

                        LocationIdMO modelObj = new LocationIdMO();
                        modelObj.setRecentMaxTime(parentResponseObject.optString("EV_LOADER_IN_MIN"));
                        KwikChargeDetailsSingleton.getInstance().setLocationIdMO(modelObj);
                        startActivity(new Intent(HomeScreen.this, KCChargingTimeScreen.class));
                    }
                }
            } else
                startActivity(new Intent(HomeScreen.this, ScanAndLocationActivity.class));
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String formKCReceiptJson(String receivedAppTxnId) {

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("APISERVICE", "KC_RECEIPT");
            jsonObj.put("USERID", preferences.getString("KP_USER_ID", ""));
            jsonObj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonObj.put("SESSIONID", preferences.getString("SESSION_ID", ""));
            else
                jsonObj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonObj.put("APP_TXN_ID", receivedAppTxnId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObj.toString();
    }

    private void parseReceiptDetailsJson(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {

                    KCReceiptMO kcReceiptMO = new KCReceiptMO();
                    kcReceiptMO.setRegistrationNumber(childJsonObj.optString("REGISTRATION_NUMBER"));
                    kcReceiptMO.setLocationName(childJsonObj.optString("LOCATIONNAME"));
                    kcReceiptMO.setDuration(childJsonObj.optString("DURATION"));
                    kcReceiptMO.setTotalEnergy(childJsonObj.optString("TOTAL_ENERGY"));
                    kcReceiptMO.setServiceFee(childJsonObj.optString("SERVICEFEE"));
                    kcReceiptMO.setMaxTime(childJsonObj.optString("MAX_TIME"));
                    kcReceiptMO.setRate(childJsonObj.optString("RATE"));

                    Intent intent = new Intent(HomeScreen.this, KCReceiptScreen.class);
                    intent.putExtra("KC_RECEIPT_DETAILS_MO", kcReceiptMO);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, childJsonObj.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (Exception e) {
            ARCustomToast.showToast(this, "Parsing exception", Toast.LENGTH_LONG);
        }
    }

    //Adding Gaming arcade, KwikVend HOME banner API
    private void callGameArcadeBannerAPI(final int requestType) {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(formBannerImagesJson(requestType)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                String receivedBanner = "";
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJsonObject = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJsonObject.has("BANNERS"))
                                receivedBanner = childJsonObject.getString("BANNERS");
                            else if (childJsonObject.has("KWIKVEND_BANNERS"))
                                receivedBanner = childJsonObject.getString("KWIKVEND_BANNERS");
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(HomeScreen.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                }

                if (requestType == 3) {
                    if (!receivedBanner.isEmpty())
                        preferences.edit().putString("GAMING_HOME_BANNERS", receivedBanner).apply();
                    startActivity(new Intent(HomeScreen.this, GameArcadeHomeScreen.class));
                } else if (requestType == 4) {
                    if (!receivedBanner.isEmpty())
                        preferences.edit().putString("KV_HOME_BANNERS", receivedBanner).apply();
                    startActivity(new Intent(HomeScreen.this, KVHomeScreen.class));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                if (requestType == 3)
                    startActivity(new Intent(HomeScreen.this, GameArcadeHomeScreen.class));
                else if (requestType == 4)
                    startActivity(new Intent(HomeScreen.this, KVHomeScreen.class));
            }
        });
    }

    //-------------------------HANDLING PREFIX BASED CALLING APIS OR SCREENS-----------------------------//
    //CALLING KWIKVEND PRODUCT LIST SCREEN
    private void callKVGetProductListScreen(String locationID) {
        KVProductOthersDetailsMO model = new KVProductOthersDetailsMO();
        model.setMerchantId("123456");
        model.setQrCode(locationID);
        KVProductDetailsSingleton.getInstance().setKvProductOthersDetailsMO(model);
        startActivity(new Intent(this, KVProductSelection.class));
    }

}
