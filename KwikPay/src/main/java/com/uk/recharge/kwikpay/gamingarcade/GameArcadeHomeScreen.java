package com.uk.recharge.kwikpay.gamingarcade;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.adapters.AdsViewPagerAdapter;
import com.uk.recharge.kwikpay.gamingarcade.bleutilities.BLEConnector;
import com.uk.recharge.kwikpay.gamingarcade.bleutilities.BLEInterface;
import com.uk.recharge.kwikpay.gamingarcade.bleutilities.FinalBluetoothChatService;
import com.uk.recharge.kwikpay.gamingarcade.bleutilities.GattClient;
import com.uk.recharge.kwikpay.gamingarcade.models.GameMachineInfoMO;
import com.uk.recharge.kwikpay.gamingarcade.models.PricingOptionsMO;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog.LocationIdListener;
import com.uk.recharge.kwikpay.kwikcharge.qrscanner.BarcodeScannerActivity;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.GamingInfoSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 7/4/2018.
 */

public class GameArcadeHomeScreen extends AppCompatActivity implements LocationIdListener {

    private static final int REQUEST_CAMERA_RESULT = 10;
    private static final int REQUEST_SCANNER = 9;
    private SharedPreferences preferences;

    //BLE scan other BLE related variables:
    private BluetoothAdapter btAdapter = null;
    private static final String TAG = GameArcadeHomeScreen.class.getSimpleName();
    private static final long SCAN_TIMEOUT_MS = 10_000;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_PERMISSION_LOCATION = 1;
    private boolean mScanning;
    private final BluetoothLeScannerCompat mScanner = BluetoothLeScannerCompat.getScanner();
    private final Handler mStopScanHandler = new Handler();
    private FinalBluetoothChatService mService;
    private String macAddressAfterPairing = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gaming_arcade_screen);
        settingIds();
    }

    private void settingIds() {

        preferences = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        findViewById(R.id.scanQRCodeTV).setOnClickListener(onClickListener);
        TextView enterMachineNoTV = findViewById(R.id.enterMachineNoTV);
        enterMachineNoTV.setText("Enter Machine No.");
        enterMachineNoTV.setOnClickListener(onClickListener);

        try {
            AdsViewPagerAdapter adsViewPagerAdapter = new AdsViewPagerAdapter(this, "GamingArcade");
            AutoScrollViewPager mViewPager = findViewById(R.id.viewPagerBanner);
            mViewPager.startAutoScroll(5000);
            mViewPager.setInterval(5000);
            mViewPager.setAdapter(adsViewPagerAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.scanQRCodeTV) {
                checkRTPForCamera();
            } else if (v.getId() == R.id.enterMachineNoTV) {
                DialogFragment dialog = new CustomDialog();
                dialog.show(getSupportFragmentManager(), "LocationIdDialog");
                Bundle bundle = new Bundle();
                bundle.putBoolean("IsComingFromGamingScreen", true);
                dialog.setArguments(bundle);
                dialog.setCancelable(false);
            }
        }
    };

    private void checkRTPForCamera() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                    startQRScanActivity();
                else {
                    if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA))
                        Toast.makeText(this, "Camera permission not granted, to use the Camera services", Toast.LENGTH_SHORT).show();
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, REQUEST_CAMERA_RESULT);
                }
            } else
                startQRScanActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Cannot run application because camera service permission have not been granted", Toast.LENGTH_SHORT).show();
                } else {
                    startQRScanActivity();
                }
                break;

            case REQUEST_PERMISSION_LOCATION:
                if (requestCode == REQUEST_PERMISSION_LOCATION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this, "You must grant the location permission.", Toast.LENGTH_SHORT).show();
                    finish();
                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void startQRScanActivity() {
        startActivityForResult(new Intent(this, BarcodeScannerActivity.class), REQUEST_SCANNER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SCANNER) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String scannedNumber = data.getStringExtra("QR_RESULT");
                    callChargerIdDetailsAPI(scannedNumber);
                }
            }
        } else if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK) {
            Log.e(TAG, "Coming to start the things again!!!... onActivityResult");
            prepareForScan();
        }
//        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void getLocationId(String locationID) {
        if (locationID != null && !locationID.isEmpty()) {
            callChargerIdDetailsAPI(locationID);
        }
    }

    private void callChargerIdDetailsAPI(String machineNumber) {

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);

        JSONObject json = null;
        try {
            json = new JSONObject();
            json.put("APISERVICE", "GAME_MACHINE_INFO_BY_LOCATION_NUM");
            json.put("SOURCENAME", "KPAPP");
            json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("DEVICEOS", "ANDROID");
            json.put("MACHINE_NUMBER", machineNumber);
            json.put("USERID", preferences.getString("KP_USER_ID", "0"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {

                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");

                            if (childJson.optString("RESPONSECODE").equals("0")) {

                                GameMachineInfoMO gameMachineInfoMO = new GameMachineInfoMO();
                                gameMachineInfoMO.setCost(childJson.optString("COST"));
                                gameMachineInfoMO.setBleUDIDNo(childJson.optString("BLE_UDID_NO"));
                                gameMachineInfoMO.setMachineNumber(childJson.optString("MACHINE_NUMBER"));
                                gameMachineInfoMO.setMachineName(childJson.optString("MACHINE_NAME"));
                                gameMachineInfoMO.setMachineLogo(childJson.optString("MACHINE_LOGO"));
                                gameMachineInfoMO.setMachineVideoURL(childJson.optString("MACHINE_VIDEO_URL"));
                                gameMachineInfoMO.setMachineDescURL(childJson.optString("MACHINE_DESC_URL"));
                                gameMachineInfoMO.setMachineBannerDesc(childJson.optString("MACHINE_BANNER_DESC"));
                                gameMachineInfoMO.setOperatorDesc(childJson.optString("OP_DESC"));
                                gameMachineInfoMO.setOperatorLogo(childJson.optString("OP_LOGO"));
                                gameMachineInfoMO.setOperatorID(childJson.optString("OP_ID"));
                                gameMachineInfoMO.setProviderURLTerms(childJson.optString("PROVIDER_URL_TERMS"));
                                gameMachineInfoMO.setProviderTermsChecked(childJson.optString("PROVIDER_TERMS_CHECKED"));
                                gameMachineInfoMO.setProviderImgURL(childJson.optString("PROVIDER_IMG_URL"));
                                gameMachineInfoMO.setAmusementAvailableCredit(childJson.optString("AMUSEMENT_AVAILABLE_CREDIT"));
                                gameMachineInfoMO.setAmusementAvailableTicket(childJson.optString("AMUSEMENT_AVAILABLE_TICKET"));
                                gameMachineInfoMO.setOperatorCode(childJson.optString("OP_CODE"));
                                gameMachineInfoMO.setServiceID(childJson.optString("SERVICEID"));
                                gameMachineInfoMO.setAddressOne(childJson.optString("ADDRESS_ONE"));
                                gameMachineInfoMO.setCountry(childJson.optString("COUNTRY"));
                                gameMachineInfoMO.setCity(childJson.optString("CITY"));
                                gameMachineInfoMO.setPostCode(childJson.optString("POSTCODE"));

                                if (childJson.has("PRICING_OPTIONS")) {
                                    JSONArray jsonArray = childJson.getJSONArray("PRICING_OPTIONS");
                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        ArrayList<PricingOptionsMO> pricingOptionsMOArrayList = new ArrayList<>();
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObj = jsonArray.getJSONObject(i);
                                            PricingOptionsMO pricingMo = new PricingOptionsMO();
                                            pricingMo.setPlanID(jsonObj.optString("plan_id"));
                                            pricingMo.setPlanAmount(jsonObj.optString("plan_amount"));
                                            pricingMo.setPlanDesc(jsonObj.optString("plan_desc"));
                                            pricingMo.setPlanCredit(jsonObj.optString("plan_credit"));
                                            pricingMo.setServiceFee(jsonObj.optString("service_fee"));
                                            pricingMo.setProcessingFee(jsonObj.optString("processing_fee"));
                                            pricingOptionsMOArrayList.add(pricingMo);
                                        }
                                        gameMachineInfoMO.setPricingOptionsMOList(pricingOptionsMOArrayList);
                                    }
                                }

                                //Move to next Screen
                                GamingInfoSingleton.getInstance().setGameMachineInfoMO(gameMachineInfoMO);
                                /*if (preferences.getString("KP_USER_ID", "").equals("0")) {
                                    startActivity(new Intent(GameArcadeHomeScreen.this, LoginScreen.class)
                                            .putExtra("IS_COMING_FROM_GAMING_SCREEN", true));
                                } else if (preferences.getBoolean("IS_GAMING_TNC_ACCEPTED", false))
                                    startActivity(new Intent(GameArcadeHomeScreen.this, GamingProductPricing.class));
                                else
                                    startActivity(new Intent(GameArcadeHomeScreen.this, GamingTnCScreen.class));*/

                                prepareForScan();

                            } else {
                                ARCustomToast.showToast(GameArcadeHomeScreen.this, childJson.optString("DESCRIPTION"));
                            }
                        }

                    } catch (Exception e) {
                        ARCustomToast.showToast(GameArcadeHomeScreen.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(GameArcadeHomeScreen.this))
                        ARCustomToast.showToast(GameArcadeHomeScreen.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(GameArcadeHomeScreen.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                ARCustomToast.showToast(GameArcadeHomeScreen.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }

    //---------------------Below methods and functions are used for BLE connection and detection------------------------//
    private void prepareForScan() {
        if (isBleSupported()) {
            // Ensures Bluetooth is enabled on the device
            BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            btAdapter = btManager.getAdapter();
            if (btAdapter.isEnabled()) {
                // Prompt for runtime permission
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    startLeScan();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_LOCATION);
                }
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        } else {
            Toast.makeText(this, "BLE is not supported", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private boolean isBleSupported() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    private void startLeScan() {
        mScanning = true;
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setReportDelay(1000)
                .build();
        List<ScanFilter> filters = new ArrayList<>();
//        filters.add(new ScanFilter.Builder().setServiceUuid(new ParcelUuid(SERVICE_UUID)).build());
        mScanner.startScan(filters, settings, mScanCallback);

        // Stops scanning after a pre-defined scan period.
        mStopScanHandler.postDelayed(mStopScanRunnable, SCAN_TIMEOUT_MS);

//        invalidateOptionsMenu();
    }

    private void stopLeScan() {
        if (mScanning) {
            mScanning = false;
            mScanner.stopScan(mScanCallback);
            mStopScanHandler.removeCallbacks(mStopScanRunnable);
        }
    }

    private final Runnable mStopScanRunnable = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getApplicationContext(), "No devices found", Toast.LENGTH_SHORT).show();
            stopLeScan();
        }
    };

    private final ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            // We scan with report delay > 0. This will never be called.
            Log.i(TAG, "onScanResult: " + result.getDevice().getAddress());
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            Log.i(TAG, "onBatchScanResults: " + results.toString());

            if (!results.isEmpty()) {
                parseBLEScanResult(results, "D8:80:39:F8:38:B6");
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.w(TAG, "Scan failed: " + errorCode);
            stopLeScan();
        }
    };


    private void parseBLEScanResult(List<ScanResult> scanResultList, String bleMacToSearch) {
        if (scanResultList != null && scanResultList.size() > 0) {
            try {
                boolean isSearchableBLEFound = false;

                for (ScanResult scanResult : scanResultList) {
                    Log.e(TAG, "Coming to search bleFound " + scanResult.getDevice() + " BLE ToSearch " + bleMacToSearch);

                    if (scanResult.getDevice().getAddress().equals(bleMacToSearch)) {
                        Log.e(TAG, "bleFounded UUID " + scanResult.getDevice().getUuids());
                        isSearchableBLEFound = true;
                        break;
                    }
                }
                if (isSearchableBLEFound) {
                    Log.e(TAG, "Great!! scanned BLE is found... Hurrey ");
                    stopLeScan();
                    startConnectingToBLE(bleMacToSearch);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startConnectingToBLE(final String macAddress) {
        Log.e("macAddress:", macAddress);

        mService = new FinalBluetoothChatService(this, this.mHandler);
        BLEConnector vendekinBLE = new BLEConnector(this, macAddress, new BLEInterface() {
            public void onConnected(BluetoothDevice device) {
                Log.e("Connected in SDK:", device.getName());
//                isComingAfterPairingFlag = true;
                macAddressAfterPairing = macAddress;
                mService.connect(device);
            }

            @Override
            public void onBLEAlreadyPaired(BluetoothDevice device) {
                Log.e("BLEPaired", "Device already Paired: " + device.getName());
                startInteractActivity(macAddress);
            }
        });
        vendekinBLE.startScanning();
    }

    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            byte[] send;
            switch (msg.what) {
                case 1:
                    switch (msg.arg1) {
                        case 0:
                        case 1:
//                            ++VendekinSdk.this.bluetoothIssue;
//                            Log.e("BluetoothIssue", String.valueOf(VendekinSdk.this.bluetoothIssue));
                            return;
                        case 2:
                            Log.e("BluetoothIssue", "Connecting");
                            return;
                        case 3:
//                            VendekinSdk.this.bluetoothIssue = 4;
                            Log.e("BluetoothIssue", "connected");

                            if (macAddressAfterPairing != null && !macAddressAfterPairing.isEmpty())
                                startInteractActivity(macAddressAfterPairing);
                            return;
                        default:
                            return;
                    }
                case 2:
                    Log.e("AT", "MESSAGE_READ");
                    send = (byte[]) ((byte[]) msg.obj);
                    String string = new String(send, 0, msg.arg1);
                    Log.e("FeedBackString", string);

            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        disconnect();

        if (mGattClient != null)
            mGattClient.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLeScan();
    }

    public void disconnect() {
        if (btAdapter != null && btAdapter.isEnabled()) {
            this.btAdapter.disable();
        }

        if (Build.VERSION.SDK_INT >= 23) {
            try {
                if (this.mService != null) {
                    this.mService.stop();
                }
            } catch (Exception var2) {
                var2.printStackTrace();
            }
        }
    }

    private final GattClient mGattClient = new GattClient();

    private void startInteractActivity(String bleMacAddress) {

        /*Intent intent = new Intent(this, InteractActivity.class);
        intent.putExtra(InteractActivity.EXTRA_DEVICE_ADDRESS, bleMacAddress);
        startActivity(intent);*/

        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this, "Building connection, please wait!!");
        mGattClient.onCreate(this, bleMacAddress, new GattClient.OnCounterReadListener() {
            @Override
            public void onCounterRead(final int value) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
//                        bleDataPreviewer.setText("BLE Data value size " + value);
                        Log.e("BLELogs", "BLE ready.. Can write");
                    }
                });
            }

            @Override
            public void onConnected(final boolean success) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (!success) {
                            Log.e("BLELogs", "BLE error!! unable to connect");

                            KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                            Toast.makeText(GameArcadeHomeScreen.this, "Unable to create connection with GATT",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Log.e("BLELogs", "BLE is ready to write data");
                            Toast.makeText(GameArcadeHomeScreen.this, "Connection established successfully",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }

            @Override
            public void onCharacterResponseRead(String response) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                Log.e("BLELogs", "BLE ready.. Can write " + response);
            }
        });

    }

    //-----------------------END of methods or functions used for BLE connection and detection--------------------------//


}
