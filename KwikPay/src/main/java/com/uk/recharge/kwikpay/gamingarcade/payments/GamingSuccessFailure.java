package com.uk.recharge.kwikpay.gamingarcade.payments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ar.library.ARCustomToast;
import com.uk.recharge.kwikpay.CustomSlidingDrawer;
import com.uk.recharge.kwikpay.KPApplication;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.gamingarcade.models.GameMachineInfoMO;
import com.uk.recharge.kwikpay.network.NetworkClient;
import com.uk.recharge.kwikpay.singleton.GamingInfoSingleton;
import com.uk.recharge.kwikpay.utilities.Utility;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashu Rajput on 7/9/2018.
 */

public class GamingSuccessFailure extends AppCompatActivity {

    private TextView availableCreditTV;
    private TextView creditAddedSuccessMsg;
    private TextView pressStartMachineMsgTV;
    private TextView creditRequireMsgTV;
    private Button gamingStartGameButton;
    private String receivedAppTxnId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gaming_success_failure);

        settingResourceIDs();

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("IS_DIRECT_PLAY_NOW")) {
                if (receivedBundle.getBoolean("IS_DIRECT_PLAY_NOW", false)) {
                    availableCreditTV.setVisibility(View.INVISIBLE);
                    creditAddedSuccessMsg.setText("");
                    creditRequireMsgTV.setVisibility(View.GONE);
                    pressStartMachineMsgTV.setText("Press start to enjoy you game");
                    pressStartMachineMsgTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
                    gamingStartGameButton.setText("Start");
                }
            }
            if (receivedBundle.containsKey("PG_APP_TXN_ID_KEY"))
                receivedAppTxnId = receivedBundle.getString("PG_APP_TXN_ID_KEY");

        }
    }

    private void settingResourceIDs() {

        //SLIDING DRAWER VARIABLES,IDS,METHODS AND VALUES;
        ImageView homeButtonImage = findViewById(R.id.headerHomeBtn);
        ImageView headerMenuImage = findViewById(R.id.headerMenuBtn);
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        ListView mDrawerList = findViewById(R.id.left_drawer);
        CustomSlidingDrawer csd = new CustomSlidingDrawer(this, mDrawerLayout, mDrawerList, headerMenuImage, homeButtonImage);
        csd.createMenuDrawer();

        availableCreditTV = findViewById(R.id.availableCreditTV);
        creditAddedSuccessMsg = findViewById(R.id.creditAddedSuccessMsg);
        pressStartMachineMsgTV = findViewById(R.id.pressStartMachineMsgTV);
        creditRequireMsgTV = findViewById(R.id.creditRequireMsgTV);
        gamingStartGameButton = findViewById(R.id.gamingStartGameButton);
        gamingStartGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callStartGameAPI();
            }
        });

        findViewById(R.id.headerBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        GameMachineInfoMO gameMachineInfoMO = GamingInfoSingleton.getInstance().getGameMachineInfoMO();
        if (gameMachineInfoMO != null) {
            availableCreditTV.setText("Available Credit - " + gameMachineInfoMO.getAmusementAvailableCredit());
            creditRequireMsgTV.setText("This game play requires " + gameMachineInfoMO.getCost() + " credits");
        }
    }

    private void callStartGameAPI() {
        KPApplication.getInstance().getUtilityInstance().showProgressDialog(this);
        JSONObject json = new JSONObject();
        try {
            SharedPreferences preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
            GameMachineInfoMO gameMachineInfoMO = GamingInfoSingleton.getInstance().getGameMachineInfoMO();
//            int productPosition = gameMachineInfoMO.getSelectedPosition();

            json.put("APISERVICE", "GAME_MANAGE_BALANCE_AND_TICKETS");
            if (ProjectConstant.SESSIONID.equals(""))
                json.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                json.put("SESSIONID", ProjectConstant.SESSIONID);
            json.put("SOURCENAME", "KPAPP");
            json.put("DEVICEOS", "ANDROID");
            json.put("USERID", preference.getString("KP_USER_ID", "0"));

            json.put("MACHINE_NUMBER", gameMachineInfoMO.getMachineNumber());
            json.put("OP_CODE", gameMachineInfoMO.getOperatorCode());
            json.put("OP_ID", gameMachineInfoMO.getOperatorID());
            json.put("APP_TXN_ID", receivedAppTxnId);
            json.put("GAME_COST", gameMachineInfoMO.getCost());
            json.put("TICKET_EARNED", "0"); // Kept always blank 'While starting the game'
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkClient.APIClient apiClient = NetworkClient.getNetworkClientInstance().getApiClient();
        apiClient.callCommonAPIExecutor(json.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();

                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.has("KPRESPONSE")) {
                            JSONObject childJson = jsonObject.getJSONObject("KPRESPONSE");
                            if (childJson.has("RESPONSECODE")) {
                                if (childJson.getString("RESPONSECODE").equalsIgnoreCase("0")) {
                                    String availableCredit = "", availableTicket = "";
                                    if (childJson.has("AMUSEMENT_AVAILABLE_CREDIT"))
                                        availableCredit = childJson.getString("AMUSEMENT_AVAILABLE_CREDIT");
                                    if (childJson.has("AMUSEMENT_AVAILABLE_TICKET"))
                                        availableTicket = childJson.getString("AMUSEMENT_AVAILABLE_TICKET");
                                    startActivity(new Intent(GamingSuccessFailure.this, GamingReceipt.class)
                                            .putExtra("AMUSEMENT_AVAILABLE_CREDIT", availableCredit)
                                            .putExtra("AMUSEMENT_AVAILABLE_TICKET", availableTicket));
                                } else {
                                    ARCustomToast.showToast(GamingSuccessFailure.this, childJson.optString("DESCRIPTION"));
                                }
                            } else {
                                ARCustomToast.showToast(GamingSuccessFailure.this, childJson.optString("DESCRIPTION"));
                            }
                        }
                    } catch (Exception e) {
                        ARCustomToast.showToast(GamingSuccessFailure.this, getResources().getString(R.string.common_checkNetConnection));
                    }
                } else {
                    if (Utility.isInternetAvailable(GamingSuccessFailure.this))
                        ARCustomToast.showToast(GamingSuccessFailure.this, getResources().getString(R.string.common_ServerConnection));
                    else
                        ARCustomToast.showToast(GamingSuccessFailure.this, getResources().getString(R.string.common_checkNetConnection));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                KPApplication.getInstance().getUtilityInstance().hideProgressDialog();
                ARCustomToast.showToast(GamingSuccessFailure.this, getResources().getString(R.string.common_checkNetConnection));
            }
        });
    }
}
