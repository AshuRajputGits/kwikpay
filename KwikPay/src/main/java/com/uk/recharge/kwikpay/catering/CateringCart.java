package com.uk.recharge.kwikpay.catering;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uk.recharge.kwikpay.RoomDataBase.AppDatabase;
import com.uk.recharge.kwikpay.RoomDataBase.Models.Ingredient;
import com.uk.recharge.kwikpay.RoomDataBase.Models.IngredientType;
import com.uk.recharge.kwikpay.models.CartProduct;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import static com.uk.recharge.kwikpay.catering.RestoMenuList.imgbase;
import static com.uk.recharge.kwikpay.utilities.Utility.ParseFloat;

public class CateringCart {

    public static String cartTotal;
    public static List<String> cartProductId;
    public static List<CartProduct> cartProducts;

    public static void logCart() {
        Log.e("CART", "Cart Product");
        for (String id : cartProductId) {
            Log.e("CART", id);
        }
    }

    public static void updateCartTotal(TextView cartCountTextView, TextView cartTotalTextView, AppDatabase appDatabase) {
        float total_price = 0;
        if (!cartProducts.isEmpty()) {
            for (CartProduct cartProduct : cartProducts) {
                float val = ParseFloat(cartProduct.getUnit_price()) * cartProduct.getSelected_quantity();
                total_price += val;
                List<Ingredient> ingredientList = cartProduct.getIngredientList();
                HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();
                for (final Ingredient ingredient : ingredientList) {
                    List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
                    List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());
                    for (IngredientType ingredientType : ingredientTypeList) {
                        int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                        if (checked == 1) {
                            total_price += ParseFloat(ingredientType.getUnit_price()) * cartProduct.getSelected_quantity();
                        }
                    }
                }
            }
        }
        cartTotal = new DecimalFormat("##.00").format(total_price);
        cartCountTextView.setText(String.valueOf(cartProductId.size()));
        cartTotalTextView.setText(String.format("£ %s", cartTotal));
    }

    public static void setPaymentTotal(TextView itemTotalTextView, TextView totalTextView) {
        itemTotalTextView.setText(String.format("£ %s", cartTotal));
        totalTextView.setText(String.format("£ %s", cartTotal));
    }

    public static void updateCartVisibility(LinearLayout cartLayout) {
        if (cartProductId.isEmpty()) {
            cartLayout.setVisibility(View.GONE);
            imgbase.setVisibility(View.VISIBLE);
        } else {
            cartLayout.setVisibility(View.VISIBLE);
            imgbase.setVisibility(View.GONE);
        }
    }

    public static String setQTY(String product_id) {
        int count = 0;
        for (String id : cartProductId) {
            if (id.equals(product_id))
                count++;
        }
        return String.valueOf(count);
    }

    public static void resolveConflicts(AppDatabase appDatabase) {
        if (!cartProducts.isEmpty()) {
            boolean flag = false;
            int index = 0;
            for (CartProduct product1 : cartProducts) {
                for (CartProduct product2 : cartProducts) {
                    if (product1.getId().equals(product2.getId()))
                        continue;

                    if (product1.getBrand_name().equals(product2.getBrand_name())) {
                        if (getDescription(product1, appDatabase).equals(getDescription(product2, appDatabase))) {
                            product1.setSelected_quantity(product1.getSelected_quantity() + product2.getSelected_quantity());
                            index = cartProducts.indexOf(product2);
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag)
                    break;
            }

            if (flag) {
                cartProducts.remove(index);
                cartProductId.remove(index);
                resolveConflicts(appDatabase);
            }
        }
    }

    private static String getDescription(CartProduct cartProduct, AppDatabase appDatabase) {
        StringBuilder description = new StringBuilder();
        List<Ingredient> ingredientList = cartProduct.getIngredientList();
        HashMap<String, List<Integer>> viewCheckedPerIngredientList = cartProduct.getViewCheckedPerIngredientList();

        for (final Ingredient ingredient : ingredientList) {

            List<IngredientType> ingredientTypeList = appDatabase.ingredientTypeDao().getAll(ingredient.getName());
            List<Integer> checkedList = viewCheckedPerIngredientList.get(ingredient.getName());

            for (IngredientType ingredientType : ingredientTypeList) {
                int checked = checkedList.get(ingredientTypeList.indexOf(ingredientType));
                if (checked == 1) {
                    if (description.length() != 0)
                        description.append(",");
                    description.append(ingredientType.getName());
                }
            }
        }
        return description.toString();
    }

}
