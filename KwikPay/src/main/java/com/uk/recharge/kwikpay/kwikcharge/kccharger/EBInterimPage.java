package com.uk.recharge.kwikpay.kwikcharge.kccharger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ar.library.ARCustomToast;
import com.ar.library.asynctaskclasses.LoadResponseViaPost;
import com.ar.library.asynctaskclasses.LoadResponseViaPost.AsyncRequestListenerViaPost;
import com.uk.recharge.kwikpay.HomeScreen;
import com.uk.recharge.kwikpay.ProjectConstant;
import com.uk.recharge.kwikpay.R;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog;
import com.uk.recharge.kwikpay.kwikcharge.dialog.CustomDialog.LocationIdListener;
import com.uk.recharge.kwikpay.singleton.KwikChargeDetailsSingleton;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ashu Rajput on 10/2/2018.
 */

public class EBInterimPage extends AppCompatActivity implements AsyncRequestListenerViaPost, LocationIdListener {

    private SharedPreferences preference;
    private String timerReceivedFromAPI = "0";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.eb_interim_page);
        preference = getSharedPreferences("KP_Preference", MODE_PRIVATE);
        updateStepperUI();

        findViewById(R.id.headerHomeBtn).setOnClickListener(onClickListener);
        findViewById(R.id.startChargeProceedButton).setOnClickListener(onClickListener);
    }

    private void updateStepperUI() {

        TextView step1TV = findViewById(R.id.step1TV);
        TextView step2TV = findViewById(R.id.step2TV);
        TextView step3TV = findViewById(R.id.step3TV);

        if (KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getInterimScreenText() != null &&
                !KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getInterimScreenText().isEmpty()) {
            try {
                String[] stepperArray = KwikChargeDetailsSingleton.getInstance().getLocationIdMO().getInterimScreenText().split("\\|");
                step1TV.setText(stepperArray[0]);
                step2TV.setText(stepperArray[1]);
                step3TV.setText(stepperArray[2]);
            } catch (Exception e) {
            }
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.headerHomeBtn) {
                redirectToHomeScreen();
            } else if (v.getId() == R.id.startChargeProceedButton) {
                new LoadResponseViaPost(EBInterimPage.this, formKCStartChargingJson(), true).execute("");
            }
        }
    };

    private void redirectToHomeScreen() {
        Intent intent = new Intent(this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public String formKCStartChargingJson() {
        JSONObject jsonobj = new JSONObject();

        try {
            jsonobj.put("APISERVICE", "KC_START_CHARGE");
            jsonobj.put("USERID", preference.getString("KP_USER_ID", ""));
            jsonobj.put("SOURCENAME", "KPAPP");
            if (ProjectConstant.SESSIONID.equals(""))
                jsonobj.put("SESSIONID", preference.getString("SESSION_ID", ""));
            else
                jsonobj.put("SESSIONID", ProjectConstant.SESSIONID);
            jsonobj.put("LOCATION_NUMBER", preference.getString("TEMP_LOCATION_NUMBER", ""));
            jsonobj.put("DEVICE_DB_ID", preference.getString("TEMP_DEVICE_DB_ID", ""));
            jsonobj.put("LOCAL_CONNECTOR_NUMBER", preference.getString("TEMP_LOCAL_CONNECTOR_NUMBER", ""));
            jsonobj.put("APP_TXN_ID", preference.getString("TEMP_APP_TXN_ID", ""));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonobj.toString();
    }

    @Override
    public void getLocationId(String locationID) {
        startActivity(new Intent(this, KCChargeStepProcessScreen.class)
                .putExtra("START_STATUS_TIMER_IN_SECONDS", timerReceivedFromAPI));
    }

    @Override
    public void onRequestComplete(String loadedString) {
        if (loadedString != null && !loadedString.equals("")) {
            if (!loadedString.equals("Exception")) {
                parseStartChargingDetails(loadedString);
            } else {
                ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
            }
        } else {
            ARCustomToast.showToast(this, getResources().getString(R.string.common_checkNetConnection), Toast.LENGTH_LONG);
        }
    }

    private void parseStartChargingDetails(String loadedString) {
        try {
            JSONObject jsonObject = new JSONObject(loadedString);
            JSONObject childJsonObj = jsonObject.getJSONObject(ProjectConstant.RESPONSE_TAG);

            if (childJsonObj.has(ProjectConstant.API_RESPONSE_CODE)) {
                if (childJsonObj.getString(ProjectConstant.API_RESPONSE_CODE).equals("0")) {
                    timerReceivedFromAPI = childJsonObj.optString("START_STATUS_TIMER_IN_SECONDS");

                    DialogFragment dialog = new CustomDialog();
                    dialog.show(getSupportFragmentManager(), "LocationIdDialog");
                    Bundle bundle = new Bundle();
//                    bundle.putString("Charger_Address", "Please ensure the charger cable is connected to your car now and press \"Proceed  /  Cancel\"");
                    bundle.putString("Charger_Address", "Please ensure the charger cable is connected to your car now");
                    bundle.putBoolean("ShowAddressDialog", true);
                    bundle.putBoolean("ButtonsText", true);
                    dialog.setArguments(bundle);
                    dialog.setCancelable(false);

                } else {
                    if (childJsonObj.has("DESCRIPTION")) {
                        ARCustomToast.showToast(this, childJsonObj.optString("DESCRIPTION"), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (Exception e) {
            ARCustomToast.showToast(this, "Parsing exception", Toast.LENGTH_LONG);
        }
    }
}
