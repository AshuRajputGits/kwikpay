package com.uk.recharge.kwikpay.kwikcharge;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uk.recharge.kwikpay.R;

/**
 * Created by Ashu Rajput on 9/26/2017.
 */

public class KwikChargingPlanScreen extends AppCompatActivity {

    private Button greenButton1, greenButton2, greenButton3, greenButton4, greenButton5, greenButton6;
    private TextView greenTextView1, greenTextView2, greenTextView3, greenTextView4, greenTextView5, greenTextView6;
    private LinearLayout greenBtnLayout1, greenBtnLayout2, greenBtnLayout3, greenBtnLayout4, greenBtnLayout5, greenBtnLayout6;
    private String noOfProducts = "4";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kwik_charging_plan_screen);
        setResourceId();
    }

    private void setResourceId() {

        //SETTING IDs FOR SIX GREEN BUTTONS AND LINEAR LAYOUT
        greenBtnLayout1 = (LinearLayout) findViewById(R.id.greenBox1);
        greenBtnLayout2 = (LinearLayout) findViewById(R.id.greenBox2);
        greenBtnLayout3 = (LinearLayout) findViewById(R.id.greenBox3);
        greenBtnLayout4 = (LinearLayout) findViewById(R.id.greenBox4);
        greenBtnLayout5 = (LinearLayout) findViewById(R.id.greenBox5);
        greenBtnLayout6 = (LinearLayout) findViewById(R.id.greenBox6);

        // SETTING THE IDs FOR SIX GREEN BUTTONS
        greenButton1 = (Button) findViewById(R.id.topupAboveValue1);
        greenButton2 = (Button) findViewById(R.id.topupAboveValue2);
        greenButton3 = (Button) findViewById(R.id.topupAboveValue3);
        greenButton4 = (Button) findViewById(R.id.topupAboveValue4);
        greenButton5 = (Button) findViewById(R.id.topupAboveValue5);
        greenButton6 = (Button) findViewById(R.id.topupAboveValue6);

        // SETTING THE IDs FOR SIX GREEN TEXT VIEWS
        greenTextView1 = (TextView) findViewById(R.id.topupBelowValue1);
        greenTextView2 = (TextView) findViewById(R.id.topupBelowValue2);
        greenTextView3 = (TextView) findViewById(R.id.topupBelowValue3);
        greenTextView4 = (TextView) findViewById(R.id.topupBelowValue4);
        greenTextView5 = (TextView) findViewById(R.id.topupBelowValue5);
        greenTextView6 = (TextView) findViewById(R.id.topupBelowValue6);
    }

    public void sixGreenButtonClick(View sixBtnView) {
        greenButton1.setBackgroundResource(R.drawable.topup_button);
        greenButton1.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton2.setBackgroundResource(R.drawable.topup_button);
        greenButton2.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton3.setBackgroundResource(R.drawable.topup_button);
        greenButton3.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton4.setBackgroundResource(R.drawable.topup_button);
        greenButton4.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton5.setBackgroundResource(R.drawable.topup_button);
        greenButton5.setTextColor(getResources().getColor(R.color.whiteColor));
        greenButton6.setBackgroundResource(R.drawable.topup_button);
        greenButton6.setTextColor(getResources().getColor(R.color.whiteColor));

        if (Integer.parseInt(noOfProducts) <= 3) {
            LinearLayout secondGreenBtnLayout = (LinearLayout) findViewById(R.id.customTopUpFirstLayout);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) secondGreenBtnLayout.getLayoutParams();
            params.setMargins(0, 0, 0, 0);
            secondGreenBtnLayout.setLayoutParams(params);
        } else {
            LinearLayout secondGreenBtnLayout = (LinearLayout) findViewById(R.id.customTopUpSecondLayout);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) secondGreenBtnLayout.getLayoutParams();
            params.setMargins(0, 15, 0, 0);
            secondGreenBtnLayout.setLayoutParams(params);
        }

        switch (sixBtnView.getId()) {
            case R.id.topupAboveValue1:
                greenButton1.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton1.setTextColor(getResources().getColor(R.color.darkGreen));
                openOrderScreen();
                break;
            case R.id.topupAboveValue2:
                greenButton2.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton2.setTextColor(getResources().getColor(R.color.darkGreen));
                break;
            case R.id.topupAboveValue3:
                greenButton3.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton3.setTextColor(getResources().getColor(R.color.darkGreen));
                break;
            case R.id.topupAboveValue4:
                greenButton4.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton4.setTextColor(getResources().getColor(R.color.darkGreen));
                break;
            case R.id.topupAboveValue5:
                greenButton5.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton5.setTextColor(getResources().getColor(R.color.darkGreen));
                break;
            case R.id.topupAboveValue6:
                greenButton6.setBackgroundResource(R.drawable.topup_button_hover);
                greenButton6.setTextColor(getResources().getColor(R.color.darkGreen));
                break;
            default:
                break;
        }
    }

    private void openOrderScreen(){
        startActivity(new Intent(this,KwikChargeOrderScreen.class));
    }

}
