package com.uk.recharge.kwikpay.catering.cart;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.uk.recharge.kwikpay.KPSuperClass;
import com.uk.recharge.kwikpay.R;

/**
 * Created by Ashu Rajput on 11/14/2018.
 */

public class RestaurantMenuCartScreen extends KPSuperClass {

    @Override
    protected int getLayoutResourceId() {
        return R.layout.restaurant_menu_cartscreen;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMenuDrawer(this);
        initiateAppsBanner("CateringMerchantDetails");
        setDynamicWidthHeightToView();
    }
}
