package com.uk.recharge.kwikpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uk.recharge.kwikpay.singleton.EventsLoggerSingleton;

@SuppressLint("SetJavaScriptEnabled") 
public class AboutUs extends Activity 
{
	private ProgressDialog mProgressDialog;
	private WebView webView;
	private SharedPreferences preference;
	private Button detailButton;
	private TextView headerTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.about_us);
		setUpViewsId();

		webView.getSettings().setJavaScriptEnabled(true);
		MyWebClient webClient =new MyWebClient(AboutUs.this);
		webView.setWebViewClient(webClient);
		
		Bundle bundle=getIntent().getExtras();
		if(bundle!=null)
		{
			if(bundle.containsKey("SCREEN_NAME"))
			{
				String aboutusURL="";
				if(bundle.getString("SCREEN_NAME").equals("RECEIPT_DETAILS"))
				{
					headerTitle.setText("");
                    if(ProjectConstant.providerDetailsURL!=null)
					    aboutusURL=ProjectConstant.providerDetailsURL;
                    else
					    aboutusURL="https://kwikpay.co.uk/kwikpay/kwikpay/apphtml/operators.html";

					detailButton.setVisibility(View.VISIBLE);
				}
				else
				{
					try {
						EventsLoggerSingleton.getInstance().setCommonEventLogs(this,getResources().getString(R.string.SCREEN_NAME_ABOUT_US));
					} catch (Exception e) {
						e.printStackTrace();
					}
					headerTitle.setText("About us");
					aboutusURL="https://kwikpay.co.uk/kwikpay/kwikpay/apphtml/"+preference.getString(ProjectConstant.DB_ABOUTUS_REVISED, "aboutus")+".html";
//					aboutusURL="http://46.37.168.7:8080/kwikpay/kwikpay/apphtml/"+preference.getString(ProjectConstant.DB_ABOUTUS_REVISED, "aboutus")+".html";
				
				}
				webView.loadUrl(aboutusURL);
			}
		}
		
		detailButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AboutUs.this.finish();
			}
		});
	}

	public void setUpViewsId()
	{
		preference=getSharedPreferences("KP_Preference", MODE_PRIVATE);
		webView = (WebView) findViewById(R.id.aboutUs_WebView);
		
		headerTitle=(TextView)findViewById(R.id.mainHeaderTitleName);
		
		detailButton=(Button)findViewById(R.id.aboutUsDetailBackButton);
		
		//SLIDIND DRAWER VARIABLES,IDS,METHODS AND VALUES;

		ImageView homeButtonImage= ((ImageView) findViewById(R.id.headerHomeBtn));
		ImageView headerMenuImage=(ImageView)findViewById(R.id.headerMenuBtn);
		DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		ListView mDrawerList = (ListView)findViewById(R.id.left_drawer);

		CustomSlidingDrawer csd=new CustomSlidingDrawer(AboutUs.this, mDrawerLayout,
				mDrawerList, headerMenuImage, homeButtonImage);
		csd.createMenuDrawer();

		// OPENING,HITING QUERY AND CLOSING DB TO GET SCREEN SUBHEADER NAME
		/*DBQueryMethods database=new DBQueryMethods(AboutUs.this);
		try
		{
			database.open();
			TextView headerBelowTV=(TextView)findViewById(R.id.headerBelowTextPart2);
			headerBelowTV.setText(database.getSubHeaderTitles("About us"));
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}
*/

	}

	public class MyWebClient extends WebViewClient 
	{
		public Context aboutUsContext;

		public MyWebClient(Context aboutUsContext) 
		{
			// TODO Auto-generated constructor stub
			this.aboutUsContext = aboutUsContext;
			mProgressDialog = new ProgressDialog(aboutUsContext);
			mProgressDialog.setMessage("loading please wait...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.setCancelable(false);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) 
		{
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);

			try
			{
				if(!isFinishing())
				{
					mProgressDialog.show();
				}
			}catch(Exception exception){
			}

		}

		@Override
		public void onPageFinished(WebView view, String url) 
		{
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			try 
			{
				if ((mProgressDialog != null) && mProgressDialog.isShowing()) 
				{
					mProgressDialog.dismiss();
				}
			} catch (final IllegalArgumentException ae) {
			} catch (final Exception excep) {
			} finally {
				mProgressDialog = null;
			}
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) 
		{
			// TODO Auto-generated method stub
			super.onReceivedError(view, errorCode, description, failingUrl);
		}
	}

}