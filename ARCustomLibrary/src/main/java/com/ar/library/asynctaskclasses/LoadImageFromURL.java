package com.ar.library.asynctaskclasses;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class LoadImageFromURL extends AsyncTask<String, Void, Bitmap>
{
	private ProgressDialog mProgressDialog;
	Context activityContext;
	private AsyncTaskCompleteListener asyncTaskListener;

	public LoadImageFromURL(Context activityContext)
	{
		this.activityContext=activityContext;
		mProgressDialog = new ProgressDialog(activityContext);
		mProgressDialog.setMessage(" Loading...");
		mProgressDialog.setIndeterminate(false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setCancelable(false);
		asyncTaskListener=(AsyncTaskCompleteListener) activityContext;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if(mProgressDialog!=null)
			mProgressDialog.show();
	}


	@Override
	protected Bitmap doInBackground(String... urls) 
	{
		// TODO Auto-generated method stub
		Bitmap receivedBitmap = null;
		for (String url : urls) 
		{
			receivedBitmap = downloadImage(url);
		}
		return receivedBitmap;
	}

	// Sets the Bitmap returned by doInBackground
	@Override
	protected void onPostExecute(Bitmap receivedBitmap) 
	{
		if(mProgressDialog!=null && mProgressDialog.isShowing())
			mProgressDialog.cancel();
		
		asyncTaskListener.onLoadComplete(receivedBitmap);
		
	}

	// Creates Bitmap from InputStream and returns it
	private Bitmap downloadImage(String url) {
		Bitmap bitmap = null;
		InputStream stream = null;
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inSampleSize = 1;

		try {
			stream = getHttpConnection(url);
			bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
			stream.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return bitmap;
	}

	// Makes HttpURLConnection and returns InputStream
	private InputStream getHttpConnection(String urlString)	throws IOException 
	{
		InputStream stream = null;
		URL url = new URL(urlString);
		URLConnection connection = url.openConnection();

		try 
		{
			HttpURLConnection httpConnection = (HttpURLConnection) connection;
			httpConnection.setRequestMethod("GET");
			httpConnection.connect();

			if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) 
			{
				stream = httpConnection.getInputStream();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return stream;
	}
	
	// Introducing inner Interface to avoid "Principle of least privilege":
	
	public interface AsyncTaskCompleteListener
	{
		public void onLoadComplete(Bitmap loadedBitmap);
	}

}
