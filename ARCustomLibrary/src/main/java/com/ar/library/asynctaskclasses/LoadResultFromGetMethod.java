package com.ar.library.asynctaskclasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class LoadResultFromGetMethod extends AsyncTask<String, Void, String>
{
	private ProgressDialog mProgressDialog;
	String finalResult="";
	Context activityContext;
	private AsyncTaskCompleteListenerForGetMethod asyncTaskListener;
	URL downloadUrl=null;
	HttpURLConnection connection=null;
	InputStream inputStream=null;
	int read=-1;
	byte[] buffer;
	boolean showProgressBar=true;

	public LoadResultFromGetMethod(Context activityContext)
	{
		this.activityContext=activityContext;
		mProgressDialog = new ProgressDialog(activityContext);
		mProgressDialog.setMessage(" Loading...");
		mProgressDialog.setIndeterminate(false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setCancelable(false);
		asyncTaskListener=(AsyncTaskCompleteListenerForGetMethod) activityContext;
	}
	
	public LoadResultFromGetMethod(Context activityContext,boolean showProgressBar)
	{
		this.activityContext=activityContext;
		asyncTaskListener=(AsyncTaskCompleteListenerForGetMethod) activityContext;
		this.showProgressBar=showProgressBar;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if(mProgressDialog!=null && showProgressBar)
			mProgressDialog.show();
	}

	@Override
	protected String doInBackground(String... url) 
	{
		// TODO Auto-generated method stub
		try 
		{
			downloadUrl=new URL(url[0]);
			connection=(HttpURLConnection)downloadUrl.openConnection();

			inputStream=connection.getInputStream();
			buffer=new byte[1024];

			BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) 
			{
				total.append(line);
			}
			finalResult=total.toString();

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			return "Exception";
		}catch(IOException ioe){
			return "Exception";
		}
		catch(Exception exception)
		{
			return "Exception";
		}
		finally{
			if(connection!=null)
				connection.disconnect();
			if(inputStream!=null)
			{
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
				}
			}
		}

		return finalResult;
	}


	// Sets the Bitmap returned by doInBackground
	@Override
	protected void onPostExecute(String receivedString) 
	{
		if(mProgressDialog!=null && mProgressDialog.isShowing())
			mProgressDialog.cancel();

		asyncTaskListener.onLoadComplete(receivedString);
	}

	// Introducing inner Interface to avoid "Principle of least privilege":

	public interface AsyncTaskCompleteListenerForGetMethod
	{
		public void onLoadComplete(String loadedString);
	}

}

