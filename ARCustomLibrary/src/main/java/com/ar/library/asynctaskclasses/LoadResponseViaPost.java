package com.ar.library.asynctaskclasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.ar.library.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;

@SuppressWarnings("deprecation")
public class LoadResponseViaPost extends AsyncTask<String, Void, String> {
    private ProgressDialog mProgressDialog;
    String webResponse = "", apiParams = "";
    Context activityContext;
    private AsyncRequestListenerViaPost asyncTaskListener;

//	String baseUrl="http://46.37.168.7:8080/kwikpayapi/jsonrequest?param="; // TEST SERVER URL
//	String baseUrl="https://www.kwikpay.uk/kwikpayapi/jsonrequest?param="; // PRODUCTION URL

    String baseUrl = "";

    public LoadResponseViaPost(Context activityContext, String apiParams, boolean showProgressBar) {
        this.activityContext = activityContext;
        this.apiParams = apiParams;
        asyncTaskListener = (AsyncRequestListenerViaPost) activityContext;
        baseUrl = activityContext.getResources().getString(R.string.baseURL);

        if (showProgressBar) {
            mProgressDialog = new ProgressDialog(activityContext);
            mProgressDialog.setMessage(" Loading... ");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
        }
    }

    public LoadResponseViaPost(Context activityContext, String apiParams) {
        this.activityContext = activityContext;
        this.apiParams = apiParams;
        asyncTaskListener = (AsyncRequestListenerViaPost) activityContext;

//		baseUrl="http://46.37.168.7:8080/kwikpayapi/jsonpgrequest?param=";     // SANDBOX URL
//		baseUrl="https://www.kwikpay.uk/kwikpayapi/jsonpgrequest?param=";   // PRODUCTION URL

        baseUrl = activityContext.getResources().getString(R.string.webBaseURL);

        mProgressDialog = new ProgressDialog(activityContext);
        mProgressDialog.setMessage(" Loading... ");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);

    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        if (mProgressDialog != null)
            mProgressDialog.show();
    }

    @Override
    protected String doInBackground(String... url) {
        // TODO Auto-generated method stub
        try {
            String URL = baseUrl + Uri.encode(apiParams);

            Log.e("URL", "URL formed  " + URL);

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 7000); //Timeout Limit
            HttpResponse httpResponse = null;
            HttpPost httpPost = new HttpPost(URL);
            httpPost.setHeader("Content-Type", "text/plain");
            httpResponse = httpClient.execute(httpPost);
            if (httpResponse != null) {
                HttpEntity httpEntity = httpResponse.getEntity();
                webResponse = EntityUtils.toString(httpEntity);
            }

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            webResponse = "Exception";
        } catch (IOException ioe) {
            webResponse = "Exception";
        } catch (Exception e) {
            // TODO: handle exception
            webResponse = "Exception";
        }

        return webResponse;
    }


    // Sets the Bitmap returned by doInBackground
    @Override
    protected void onPostExecute(String receivedString) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.cancel();

        asyncTaskListener.onRequestComplete(receivedString);
    }

    // Introducing inner Interface to avoid "Principle of least privilege":

    public interface AsyncRequestListenerViaPost {
        void onRequestComplete(String loadedString);
    }

}