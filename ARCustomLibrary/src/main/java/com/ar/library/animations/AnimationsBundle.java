package com.ar.library.animations;

import com.ar.library.R;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class AnimationsBundle {
	
	Context animContext;
	
	public AnimationsBundle(Context animContext)
	{
		this.animContext=animContext;
	}
	
	/** This animation lets you shake your views */
	
	public void shakeMyView(View animOnViews)
    {
    	 final Animation shake = AnimationUtils.loadAnimation(animContext, R.anim.shake);
    	 animOnViews.startAnimation(shake);
    }
	
	/** This animation lets you fade in and out your views @ same time */
	
    public void fadeInAndOutMyView(View animOnViews)
    {
    	final Animation animAlpha = AnimationUtils.loadAnimation(animContext, R.anim.anim_alpha);
    	animOnViews.startAnimation(animAlpha);
    }
    
    /** This animation lets you rotate your Views @360 degree */
    
    public void rotateMyView(View animOnViews)
    {
    	final Animation animRotate = AnimationUtils.loadAnimation(animContext, R.anim.anim_rotate);
    	animOnViews.startAnimation(animRotate);
    }
    
    /** This animation lets you Zoom in and out @ the same same time */
    
    public void zoomInOutMyView(View animOnViews)
    {
    	 final Animation animScale = AnimationUtils.loadAnimation(animContext, R.anim.anim_scale);
    	 animOnViews.startAnimation(animScale);
    }
    
    /** This animation lets you Translate your view from left to right and vice versa @ same time */
    
    public void translateTooAndFroMyView(View animOnViews)
    {
    	final Animation animTranslate = AnimationUtils.loadAnimation(animContext, R.anim.anim_translate);
    	animOnViews.startAnimation(animTranslate);
    }
    
    /** This animation lets you Vanish your Views */
    
    public void vanishMyViews(View animOnViews)
    {
    	final Animation animVanish = AnimationUtils.loadAnimation(animContext, R.anim.vanish);
    	animOnViews.startAnimation(animVanish);
    }
    
    /** This animation lets you Zoom In animation Only */
    
    public void zoomInMyView(View animOnViews)
    {
    	final Animation zoomIn = AnimationUtils.loadAnimation(animContext, R.anim.zoom_in);
    	animOnViews.startAnimation(zoomIn);
    }
    
    /** This animation lets you Zoom Out animation Only */
    
    public void zoomOutMyView(View animOnViews)
    {
    	final Animation zoomOut = AnimationUtils.loadAnimation(animContext, R.anim.zoom_out);
    	animOnViews.startAnimation(zoomOut);
    }
    
    /** This animation lets you Continuous Blinking animation on Views */
    
    public void continuousBlinkMyView(View animOnViews)
    {
    	final Animation blink = AnimationUtils.loadAnimation(animContext, R.anim.blink);
    	animOnViews.startAnimation(blink);
    }
    
    /** This animation lets you FADE IN animation Only */
    
    public void fadeInMyView(View animOnViews)
    {
    	final Animation fadeIn = AnimationUtils.loadAnimation(animContext, R.anim.fade_in);
    	animOnViews.startAnimation(fadeIn);
    }
    
    /** This animation lets you FADE OUT animation Only */
    
    public void fadeOutMyView(View animOnViews)
    {
    	final Animation fadeOut = AnimationUtils.loadAnimation(animContext, R.anim.fade_out);
    	animOnViews.startAnimation(fadeOut);
    }
    
    /** This animation lets you BOUNCE animation on your views */
    
    public void bounceMyView(View animOnViews)
    {
    	final Animation bounce = AnimationUtils.loadAnimation(animContext, R.anim.bounce);
    	animOnViews.startAnimation(bounce);
    }
    
    /** This animation lets you create sequel animation on your views ie 
     * 1) Left to Right
     * 2) Top To Bottom
     * 3) Bottom To Top
     * 4) Continuous Rotation */
    
    public void sequelAnimMyView(View animOnViews)
    {
    	final Animation sequel = AnimationUtils.loadAnimation(animContext, R.anim.sequal_anim);
    	animOnViews.startAnimation(sequel);
    }

}
