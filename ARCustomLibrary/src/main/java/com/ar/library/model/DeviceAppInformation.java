package com.ar.library.model;

import android.content.Context;
import android.util.DisplayMetrics;
import org.apache.http.conn.util.InetAddressUtils;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by HP on 05-10-2016.
 */
public class DeviceAppInformation {

    private String deviceResolution=null;
    private String deviceScreenSize=null;
    private String deviceBrand=null;
    private String deviceModel=null;
    private String deviceManufacturer=null;
    private String deviceProduct=null;
    private String device=null;
    private String deviceOSVersion=null;
    private String ipAddress=null;
    private String deviceCellID=null;

    private String screenResolution="";

    public DeviceAppInformation(Context context)
    {
        try
        {
            int density= context.getResources().getDisplayMetrics().densityDpi;
            switch(density)
            {
                case DisplayMetrics.DENSITY_MEDIUM:
                    screenResolution="mdpi";
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    screenResolution="hdpi";
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    screenResolution="xhdpi";
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    screenResolution="xxhdpi";
                    break;
                default:
                    screenResolution="hdpi";
                    break;
            }

            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;
            int height = displayMetrics.heightPixels;

            setDeviceResolution(screenResolution);
            setDeviceScreenSize("" + width + "x" + height);
            setDevice(android.os.Build.DEVICE);
            setDeviceModel(android.os.Build.MODEL);
            setDeviceProduct(android.os.Build.PRODUCT);
            setDeviceBrand(android.os.Build.BRAND);
            setDeviceManufacturer(android.os.Build.MANUFACTURER);
            setDeviceOSVersion(android.os.Build.VERSION.RELEASE);
            setIpAddress(getDeviceIPAddress(true));

            displayMetrics=null;

        }catch(Exception exc)
        {
            exc.printStackTrace();
        }

    }

    public String getDeviceIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : networkInterfaces) {
                List<InetAddress> inetAddresses = Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : inetAddresses) {
                    if (!inetAddress.isLoopbackAddress()) {
                        String sAddr = inetAddress.getHostAddress().toUpperCase(Locale.getDefault());
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                // drop ip6 port suffix
                                int delim = sAddr.indexOf('%');
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            return "0.0.0.0";
        }
        return "0.0.0.0";
    }

    public String getDeviceBrand() {
        return deviceBrand;
    }

    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceManufacturer() {
        return deviceManufacturer;
    }

    public void setDeviceManufacturer(String deviceManufacturer) {
        this.deviceManufacturer = deviceManufacturer;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDeviceOSVersion() {
        return deviceOSVersion;
    }

    public void setDeviceOSVersion(String deviceOSVersion) {
        this.deviceOSVersion = deviceOSVersion;
    }

    public String getDeviceScreenSize() {
        return deviceScreenSize;
    }

    public void setDeviceScreenSize(String deviceScreenSize) {
        this.deviceScreenSize = deviceScreenSize;
    }

    public String getDeviceResolution() {
        return deviceResolution;
    }

    public void setDeviceResolution(String deviceResolution) {
        this.deviceResolution = deviceResolution;
    }

    public String getDeviceProduct() {
        return deviceProduct;
    }

    public void setDeviceProduct(String deviceProduct) {
        this.deviceProduct = deviceProduct;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDeviceCellID() {
        return deviceCellID;
    }

    public void setDeviceCellID(String deviceCellID) {
        this.deviceCellID = deviceCellID;
    }

}
