package com.ar.library.validation;

import java.util.regex.Pattern;

import android.text.InputFilter;
import android.text.TextUtils;
import android.widget.EditText;

import com.ar.library.R;

public class FieldValidationWithMessage 
{
	EditText fieldEditText;
	static String errorMessage;

	public static void removeAllErrorIcons(EditText fieldEditText)
	{
		fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
	}

	// VALIDATING THE USER NAME

	public static String nameIsValid(String name, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter name");
			return "";
		} 
		else if (!Pattern.matches("^[a-zA-Z. ]{1,50}$", name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid name");
			return "";
		} 
		else if (name.replaceAll(" ", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid name");
			return "";
		}
		else if (name.replaceAll("\\.", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid name");
			return "";
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}

	}


	// VALIDATING THE USER NAME

	public static String nameIsValidNickName(String name, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter nick name");
			return "";
		} 
		else if (!Pattern.matches("^[a-zA-Z. ]{1,50}$", name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid nick name");
			return "";
		} 
		else if (name.replaceAll(" ", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid nick name");
			return "";
		}
		else if (name.replaceAll("\\.", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid nick name");
			return "";
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}

	}

	// VALIDATING THE USER NAME, FIRST NAME

	public static String firstNameIsValid(String name, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter first name");
			return "";
		} 
		else if (!Pattern.matches("^[a-zA-Z. ]{1,50}$", name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid first name");
			return "";
		} 
		else if (name.replaceAll(" ", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid first name");
			return "";
		}
		else if (name.replaceAll("\\.", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid first name");
			return "";
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}

	}

	// VALIDATING THE USER NAME LAST NAME

	public static String lastNameIsValid(String name, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter last name");
			return "";
		} 
		else if (!Pattern.matches("^[a-zA-Z. ]{1,50}$", name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid last name");
			return "";
		} 
		else if (name.replaceAll(" ", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid last name");
			return "";
		}
		else if (name.replaceAll("\\.", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid last name");
			return "";
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}
	}

	// VALIDATING THE USER NAME LAST NAME

	public static String middleNameIsValid(String name, EditText fieldEditText)
	{
		if (!Pattern.matches("^[a-zA-Z. ]{1,50}$", name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid middle name");
			return "";
		} 
		else if (name.replaceAll(" ", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid middle name");
			return "";
		}
		else if (name.replaceAll("\\.", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid middle name");
			return "";
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}

	}


	// VALIDATING THE MOBILE NUMBER

	public static String mobNumberIsValid(String mobNumber, EditText fieldEditText)
	{
		
		if(mobNumber.contains("N/A"))
        {
               return "SUCCESS";
        }
		else
		{
			if (TextUtils.isEmpty(mobNumber))  
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				setErrorMessage("please enter a valid mobile no.");
				return "";
			} 
			else if (Pattern.matches("^[0-9]{1,9}$",mobNumber))  
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				setErrorMessage("please enter a valid 10 digit mobile no.");
				return "";
			} 
			else if (!(Long.valueOf(mobNumber) > 0))
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				setErrorMessage("please enter a valid mobile no.");
				return "";
			}
			else if (Pattern.matches("^[0][0-9]{1,18}$", mobNumber))
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				setErrorMessage("please enter a valid mobile no. without leading zero");
				return "";
			}
			else
			{
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
				return "SUCCESS";
			}
		}
		
	}

	// VALIDATING THE EMAIL ADDRESS

	public static boolean emailAddressIsValid(String emailAddress, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(emailAddress))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (!Pattern.matches("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+", emailAddress))   
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (!Pattern.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",emailAddress)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;

		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE CONFIRM EMAIL ADDRESS

	public static String confirmEmailAddressIsValid(String previousEmailaddress, EditText fieldEditText, String confirmEmailaddress)
	{
		if (TextUtils.isEmpty(confirmEmailaddress))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid email id");
			return "";
		} 
		else if(!previousEmailaddress.equals(confirmEmailaddress))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("your email id doesn't match. please re-enter confirm email id.");
			return "";
		} 
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}
	}

	// VALIDATING THE AGE

	public static boolean ageIsValid(String age, EditText fieldEditText, int minAge)
	{
		if (TextUtils.isEmpty(age))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if(Integer.valueOf(age)< minAge )  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE PASSWORD

	public static String passwordIsValid(String password, EditText fieldEditText, int minLengthOfPW)
	{
		if (TextUtils.isEmpty(password))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter password");
			return "";
		} 
		else if(password.length() < minLengthOfPW  || password.length() > 12)
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter password between 8 to 12 character");
			return "";  
		} 
		/*else if(!isNumberAndCharacter(password))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter password between 8 to 12 character");
			return "";
		}*/
		else if(password.contains("'"))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter password between 8 to 12 character");
			return "";
		} 
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}
	}

	// VALIDATING THE CONFIRM PASSWORD

	public static String confirmPasswordIsValid(String previousPassword, EditText fieldEditText, String confirmPassword)
	{
		if (TextUtils.isEmpty(confirmPassword))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please re-enter your password");
			return "";
		} 
		/*else if(confirmPassword.length() < 8  || confirmPassword.length() > 12)
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter password between 8 to 12 character");
			return "";  
		} */
		else if(!previousPassword.equals(confirmPassword))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("your password doesn't match. please re-enter confirm password.");
			return "";
		} 
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}
	}

	// VALIDATING THE ADDRESS

	public static boolean addressIsValid(String address, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(address))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (address.replaceAll(" ", "").trim().equals(""))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}

		else if(address.contains("'"))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			//setErrorMessage("please enter password between 8 to 12 character");
			return false;
		} 
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE COMMENTS

	public static String commentIsValid(String comment, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(comment))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter comment");
			return "";
		} 
		else if (comment.replaceAll(" ", "").trim().equals(""))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid comment");
			return "";
		}
		/*else if ((comment.contains("@")) || (comment.contains("!")) || (comment.contains("#"))
				|| (comment.contains("^")) || (comment.contains("(")) || (comment.contains(")")) 
				|| (comment.contains("/")) || (comment.contains(";")) || (comment.contains(":")) 
				|| (comment.contains("-")) || (comment.contains("_")) || (comment.contains("+")))

		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			setErrorMessage("please enter a valid comment");
			return "";
		}*/
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return "SUCCESS";
		}
	}

	// SETTING AND GETTING ERROR MESSAGE

	public static void setErrorMessage(String errorMsg)
	{
		errorMessage=errorMsg;
	}

	public static String getErrorMessage()
	{
		return errorMessage;
	}


	// CHECKING ONE ALPHA AND NUMBER IN PASSWORD

	public static boolean isNumberAndCharacter(String string) {

		int numCount =0, charCount = 0;
		for (int i = 0; i < string.length(); i++) {
			if (Character.isDigit(string.charAt(i)))  {
				numCount ++ ;
			}
			if(Character.isLetter(string.charAt(i)))
			{
				charCount++;
			}
		}
		if(numCount > 0 && charCount >0)
		{
			return true;
		}

		return false;


	}

	// METHOD FOR BLOCKING CHARACTER SETS IN KEYBOARD

	public static void blockMyCharacter(EditText et,final String receivedBlockCharacterSet)
	{
		InputFilter filter = new InputFilter() 
		{
			@Override
			public CharSequence filter(CharSequence source, int start, int end, android.text.Spanned dest, int dstart, int dend) {

				if (source != null && receivedBlockCharacterSet.contains(("" + source))) 
				{
					return "";
				}
				return null;
			}
		};

		et.setFilters(new InputFilter[] { filter });

	}


	// VALIDATING THE ADDRESS With EMPTY FIELD

	public static boolean addressIsValidEmptyField(String address, EditText fieldEditText)
	{

		if (!address.equals("") && address.replaceAll(" ", "").trim().equals(""))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}

		else if(address.contains("'"))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			//setErrorMessage("please enter password between 8 to 12 character");
			return false;
		} 
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE MOBILE NUMBER

	public static String mobNumberIsValidWithoutLeadingZero(String mobNumber, EditText fieldEditText)
	{
		if(mobNumber.contains("N/A"))
        {
               return "SUCCESS";
        }
		
		/*if(!mobNumber.matches("\\d+"))
		{
			return "SUCCESS";
		}*/
		
		else
		{
			if (TextUtils.isEmpty(mobNumber))  
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				setErrorMessage("please enter a valid mobile no.");
				return "";
			} 
			else if (Pattern.matches("^[0-9]{1,9}$",mobNumber))  
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				setErrorMessage("please enter a valid 10 digit mobile no.");
				return "";
			} 
			else if (!(Long.valueOf(mobNumber) > 0))
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
				setErrorMessage("please enter a valid mobile no.");
				return "";
			}
			else
			{
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
				return "SUCCESS";
			}
		}
		
	}

}
