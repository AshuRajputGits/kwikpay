package com.ar.library.validation;

import java.util.regex.Pattern;
import android.text.TextUtils;
import android.widget.EditText;
import com.ar.library.R;

public class FieldValidation 
{
	EditText fieldEditText;

	// VALIDATING THE USER NAME, FIRST NAME OR LAST NAME

	public static boolean nameIsValid(String name, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (!Pattern.matches("^[a-zA-Z. ]{1,50}$", name)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (name.replaceAll(" ", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}
		else if (name.replaceAll("\\.", "").trim().equals("")) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE MOBILE NUMBER

	public static boolean mobNumberIsValid(String mobNumber, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(mobNumber))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (Pattern.matches("^[0-9]{1,9}$",mobNumber))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (!(Long.valueOf(mobNumber) > 0))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}
		else if (Pattern.matches("^[0][0-9]{1,9}$", mobNumber))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE EMAIL ADDRESS

	public static boolean emailAddressIsValid(String emailAddress, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(emailAddress))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (!Pattern.matches("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+", emailAddress))   
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (!Pattern.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",emailAddress)) 
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE AGE

	public static boolean ageIsValid(String age, EditText fieldEditText, int minAge)
	{
		if (TextUtils.isEmpty(age))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if(Integer.valueOf(age)< minAge )  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE PASSWORD

	public static boolean passwordIsValid(String password, EditText fieldEditText, int minLengthOfPW)
	{
		if (TextUtils.isEmpty(password))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if(password.length() < minLengthOfPW )
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE ADDRESS

	public static boolean addressIsValid(String address, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(address))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (address.replaceAll(" ", "").trim().equals(""))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE COMMENTS

	public static boolean commentIsValid(String comment, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(comment))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (comment.replaceAll(" ", "").trim().equals(""))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}

	// VALIDATING THE NORMAL FIELD

	public static boolean normalFieldIsValid(String comment, EditText fieldEditText)
	{
		if (TextUtils.isEmpty(comment))  
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		} 
		else if (comment.replaceAll(" ", "").trim().equals(""))
		{
			fieldEditText.requestFocus();
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img, 0);
			return false;
		}
		else
		{
			fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			return true;
		}
	}
	
	// VALIDATING THE COUPON FIELD

		public static boolean couponFieldIsValid(String comment, EditText fieldEditText, int length, int position)
		{
			if (TextUtils.isEmpty(comment))  
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img1, 0);
				return false;
			} 
			else if (comment.replaceAll(" ", "").trim().equals(""))
			{
				fieldEditText.requestFocus();
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img1, 0);
				return false;
			}
			else if (position == 1)
			{
				if(!(length == 2))
				{
					fieldEditText.requestFocus();
					fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img1, 0);
					return false;
				}
				
				return true;
			}
			else if ((position == 2) || (position == 3))
			{
				if(!(length == 4))
				{
					fieldEditText.requestFocus();
					fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.error_img1, 0);
					return false;
				}
				
					return true;
			}
			else
			{
				fieldEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
				return true;
			}
		}


}
