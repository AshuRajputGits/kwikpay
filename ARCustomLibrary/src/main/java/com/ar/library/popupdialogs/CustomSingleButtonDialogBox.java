package com.ar.library.popupdialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.ar.library.R;

public class CustomSingleButtonDialogBox extends Dialog
{
	public Activity c;
	public Button yes, no;
	public String title,info,colorOfButton;
	public int numberOfButton;
	Button ok_btn,cancel_btn; 
	TextView title_TV,msg_TV;
	boolean stayOnSamePage;
	View lineView;

	public CustomSingleButtonDialogBox(Activity a,String info,
			boolean stayOnSamePage,String colorOfButton) 
	{
		// TODO Auto-generated constructor stub
		super(a);
		this.c = a;
		this.info=info;
		this.colorOfButton=colorOfButton;
		this.stayOnSamePage=stayOnSamePage;
	}

	public CustomSingleButtonDialogBox(Activity a,String title,String info,
			boolean stayOnSamePage,String colorOfButton) 
	{
		// TODO Auto-generated constructor stub
		super(a);
		this.c = a;
		this.info=info;
		this.title=title;
		this.colorOfButton=colorOfButton;
		this.stayOnSamePage=stayOnSamePage;
	}

	@SuppressLint("NewApi") 
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.customdialogbox_singlebutton);

		title_TV= (TextView) findViewById(R.id.fragmentDialog_HeaderTitle);
		msg_TV= (TextView)findViewById(R.id.ticketContent);
		ok_btn = (Button) findViewById(R.id.ok_popup_Btn);
		lineView=(View)findViewById(R.id.headerLine);
		if(title!=null)
		{
			if(!title.equals(""))
			{
				title_TV.setVisibility(View.VISIBLE);
				lineView.setVisibility(View.VISIBLE);
				title_TV.setText(title);
			}
		}
		
		msg_TV.setText(info);

		// Available colors : Orange, Green, Blue and Black

		if(colorOfButton.equalsIgnoreCase("ORANGE"))
		{
			ok_btn.setBackground(c.getResources().getDrawable(R.drawable.orange_dialog_btn));
			ok_btn.setText("Ok");
			ok_btn.setTextColor(Color.parseColor("#ffffff"));			
		}
		else if(colorOfButton.equalsIgnoreCase("GREEN"))
		{
			ok_btn.setBackground(c.getResources().getDrawable(R.drawable.green_dialog_btn));
			ok_btn.setText("Ok");
			ok_btn.setTextColor(Color.parseColor("#000000"));
		}
		else if(colorOfButton.equalsIgnoreCase("BLUE"))
		{
			ok_btn.setBackground(c.getResources().getDrawable(R.drawable.blue_dialog_btn));
			ok_btn.setText("Ok");
			ok_btn.setTextColor(Color.parseColor("#ffffff"));	
		}
		else if(colorOfButton.equalsIgnoreCase("BLACK"))
		{
			ok_btn.setBackground(c.getResources().getDrawable(R.drawable.black_dialog_btn));
			ok_btn.setText("Ok");
			ok_btn.setTextColor(Color.parseColor("#ffffff"));
		}
		else
		{
			ok_btn.setBackground(c.getResources().getDrawable(R.drawable.white_dialog_btn));
			ok_btn.setText("Ok");
			ok_btn.setTextColor(Color.parseColor("#000000"));
		}


		ok_btn.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub

				if(stayOnSamePage)
				{
					try
					{
						dismiss();
					}catch(Exception e)
					{
					}
				}
				
				else if(!stayOnSamePage)
				{
					try
					{
						dismiss();
						c.finish();
					}catch(Exception e)
					{
					}
				}
			}
		});

	}


	@Override
	public void onBackPressed()
	{
		// TODO Auto-generated method stub
		setCancelable(false);
		super.onBackPressed();

	}



}
