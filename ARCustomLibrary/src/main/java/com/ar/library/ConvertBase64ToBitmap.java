package com.ar.library;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

public class ConvertBase64ToBitmap {
 
	public static Bitmap convert(String base64String)
	{
		try 
		{
			byte [] encodeByte=Base64.decode(base64String,Base64.DEFAULT);
			Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

			return bitmap;
		} catch(Exception e) {
			e.getMessage();
			return null;
		}
	}
}
