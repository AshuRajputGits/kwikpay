package com.ar.library.webview;

import com.ar.library.ARCustomToast;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

@SuppressLint("SetJavaScriptEnabled") 
public class ARWebView
{
	private ProgressDialog mProgressDialog;
	private WebView webView;
	private myWebClient mywevcliClient;
	Context activtityContext;
	private String urlString="";
//	private static String pageFinished="PAGE_FINISHED";
	private static String authError="AUTH_ERROR";
	private static String netError="NETWORK_ERROR";
//	private webViewInterface webViewResponse;
	
	public interface webViewInterface
	{
		public void webViewResponse(String response);
	}

	public ARWebView(Context activtityContext,String urlString,WebView webView)
	{
		this.activtityContext=activtityContext;
		this.urlString=urlString;
		this.webView=webView;
		
	}
	
	public void createMyWebView()
	{
		webView.getSettings().setJavaScriptEnabled(true);
		mywevcliClient =new myWebClient(activtityContext);
		webView.setWebViewClient(mywevcliClient);
		webView.loadUrl(urlString);
//		webViewResponse=(webViewInterface) activtityContext;
	}

	public class myWebClient extends WebViewClient 
	{
		Context receivedContext;

		public myWebClient(Context receivedContext) 
		{
			// TODO Auto-generated constructor stub
			this.receivedContext = receivedContext;
			mProgressDialog = new ProgressDialog(receivedContext);
			mProgressDialog.setMessage("loading please wait...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.setCancelable(false);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) 
		{
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			mProgressDialog.show();
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);

			if (mProgressDialog != null && mProgressDialog.isShowing()) 
			{
				mProgressDialog.dismiss();
//				webViewResponse.webViewResponse(pageFinished);
			}

		}

		@Override
		public void onReceivedHttpAuthRequest(WebView view,
				HttpAuthHandler handler, String host, String realm) 
		{
			// TODO Auto-generated method stub
			super.onReceivedHttpAuthRequest(view, handler, host, realm);
			ARCustomToast.showToast(receivedContext, authError, Toast.LENGTH_LONG);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) 
		{
			// TODO Auto-generated method stub
			super.onReceivedError(view, errorCode, description, failingUrl);
			ARCustomToast.showToast(receivedContext, netError, Toast.LENGTH_LONG);
		}
		
	}
		
}
